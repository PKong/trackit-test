﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace TrackIT2.Templates
{
    public partial class Tower : System.Web.UI.MasterPage
    {
        public Controls.StatusBox StatusBox
        {
            get { return statusBox; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            int AppTimeout;
            int WarnTimeout;
            string AppURL;

            if (!Request.Path.Contains("Login.aspx"))
            {
                // Sometimes the session is lost, but the cookie still remains, 
                // which can cause issues with the site. If we can't find the
                // username displayed on the top navbar (stored in the session),
                // force a session abandon and redirect to the login page.
                if (Session["userId"] == null || String.IsNullOrEmpty(Session["userId"].ToString()))
                {
                    Session.Clear();
                    Session.Abandon();
                    Response.Redirect("~/Account/Login.aspx");
                }
                else
                {
                    AppTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["AppTimeoutMinutes"]);
                    WarnTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["AppWarnMinutes"]);
                    AppURL = Convert.ToString(ConfigurationManager.AppSettings["AppLogoutURL"]);

                    lblTimeoutCount.Text = (AppTimeout - WarnTimeout).ToString();

                    string timeout_script = "StartTimeout(" + AppTimeout * 60000 + ", " + WarnTimeout * 60000 + ", '" + AppURL + "'); ";

                    Page.ClientScript.RegisterStartupScript(this.GetType(), "InitTimeout", timeout_script, true);
                }
            }
        }
    }
}