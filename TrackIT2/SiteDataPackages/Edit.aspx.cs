﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.BLL;
using TrackIT2.DAL;
using TrackIT2.Objects;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Globalization;

namespace TrackIT2.SiteDataPackage
{
    public partial class Edit : System.Web.UI.Page
    {
       private bool siteDataPackageFound = true; 
       private const string ALL_COMMENTS_URL = "/Comments/AllComment.aspx?ct=sdp&id=";

       public enum eTabName
        {
            General,
            DocumentStatuses,
            Abstract,
            Other
        }

        public site_data_packages CurrentSiteDataPackage
        {
            get
            {
                return (site_data_packages)this.ViewState["CurrentSiteDataPackage"];
            }
            set
            {
                this.ViewState["CurrentSiteDataPackage"] = value;
            }
        }

        public eTabName CurrentTab
        {
            get
            {
                eTabName ret = (eTabName)this.Session["cur_tab"];
                if (ret == eTabName.Other) CurrentTab = eTabName.General;
                //--> End Temporary
                return ret;
            }
            set
            {
                this.Session["cur_tab"] = value;
                this.Session["cur_tab_index"] = (int)value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Security.UserIsInRole("Administrator"))
            {
                btnSdpDelete.Visible = false;
            }
            if (!Page.IsPostBack)
            {
                Utility.ClearSubmitValidator();
                int iSDPID;
                bool isValidId = Int32.TryParse(Request.QueryString["Id"],
                                                out iSDPID);
                divNoRelatedLeaseApps.Visible = true;

                if (isValidId)
                {
                   if (BLL.SiteDataPackage.Search(iSDPID) != null)
                   {
                      Page.Title = "Edit Site Data Package - " + iSDPID.ToString() + " | TrackiT2";
                      this.CurrentTab = eTabName.General;
                      BindApplication(iSDPID);
                      BindLookupLists();
                      BindingSummary();
                      BindingGeneralData();
                      CommentBinding();
                      BindingDocumentStatusData();
                      BindingAbstractData();
                      pnlNoID.Visible = false;
                      pnlSectionContent.Visible = true;
                      siteDataPackageFound = true;

                   }
                   else
                   {
                      // Page render event will write the 404 response code. This part
                      // of the code will display the No Id panel and hide the edit
                      // panel.
                      pnlSectionContent.Visible = false;
                      pnlNoID.Visible = true;
                      siteDataPackageFound = false;
                   }
                }
                else
                {
                   // Page render event will write the 404 response code. This part
                   // of the code will display the No Id panel and hide the edit
                   // panel.
                   pnlSectionContent.Visible = false;
                   pnlNoID.Visible = true;                   
                   siteDataPackageFound = false;
                }

                GetSessionFilter();
            }
            else
            {
                // Initialise Download from Amazon
                if (Request.Params["__EVENTARGUMENT"] == "DownloadDocument")
                {
                    string keyName = hidDocumentDownload.Value;
                    if (!string.IsNullOrEmpty(keyName))
                    {
                        try
                        {
                            AmazonDMS.DownloadObject(Server.UrlDecode(keyName));
                        }
                        catch (Amazon.S3.AmazonS3Exception ex)
                        {
                            Master.StatusBox.showErrorMessage(ex.Message);
                        }
                    }
                }

                if (!string.IsNullOrEmpty(Request.Params["id"]))
                {
                    int sdpID = int.Parse(Request.Params["id"]);
                    GetLinkDocument(sdpID);
                    this.CurrentTab = eTabName.DocumentStatuses;
                }

                //>
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
           base.Render(writer);

           if (!siteDataPackageFound)
           {
              Response.StatusCode = 404;
           }
        }

        #region Binding
        protected void BindApplication(int iSDPID)
        {
            using (CPTTEntities ce = new CPTTEntities())
            {
                site_data_packages objSDP;

                objSDP = ce.site_data_packages
                                .Where(la => la.id.Equals(iSDPID))
                                .OrderBy("lock_version")
                                .First();
                ce.Detach(objSDP);

                this.CurrentSiteDataPackage = objSDP;

                lblSiteID.Text = Utility.PrepareString(objSDP.site_uid);
                if (!string.IsNullOrEmpty(objSDP.site_uid))
                {
                    lblSiteID.NavigateUrl = Globals.SITE_LINK_URL + objSDP.site_uid;
                }

                BindTowerImages(objSDP.site_uid);

                //bind Publish document status
                //disabled publish document checnkbok when no TMO_AppId
                if (objSDP.lease_application_id.HasValue)
                {
                    lease_applications objLeaseApp = LeaseApplication.Search(objSDP.lease_application_id.Value);
                    if (((objLeaseApp != null) && (!String.IsNullOrEmpty(objLeaseApp.TMO_AppID))) || objSDP.publish_document)
                    {
                        this.chkShowDocument.Enabled = true;
                    }
                }
                this.chkShowDocument.Checked = objSDP.publish_document;

                //Site informations
                var querySite = from s in ce.sites
                                where s.site_uid == objSDP.site_uid
                                select s;
                if (querySite.ToList().Count > 0)
                {
                    site objSite = querySite.ToList()[0];
                    Utility.ControlValueSetter(lblSiteLatitude, objSite.site_latitude, (0.0).ToString());
                    Utility.ControlValueSetter(lblDegreeLatitude, Utility.ConvertLatLongCoorToDegree(objSite.site_latitude), (0.0).ToString());
                    Utility.ControlValueSetter(lblSiteLongitude, objSite.site_longitude, (0.0).ToString());
                    Utility.ControlValueSetter(lblDegreeLongtitude, Utility.ConvertLatLongCoorToDegree(objSite.site_longitude), (0.0).ToString());

                    // Add Google Maps link to Latitude and Logitude
                    lblSiteLatLong.NavigateUrl = Utility.GetGooleMapLatLong(lblSiteLatitude.Text, lblSiteLongitude.Text).ToString();
                    Utility.ControlValueSetter(lblType, objSite.site_class_desc);
                    Utility.ControlValueSetter(lblHeight, objSite.structure_ht, (0).ToString());
                    Utility.ControlValueSetter(lblRogueEquip, objSite.rogue_equipment_yn, "None");
                    Utility.ControlValueSetter(lblStatus, objSite.site_status_desc);
                    if (BLL.Site.IsNotOnAir(objSite.site_on_air_date, objSite.site_status_desc))
                    {
                        imgStructureDetails_Construction.ImageUrl = "/images/icons/Construction.png";
                    }
                    else
                    {
                        imgStructureDetails_Construction.Visible = false;
                    }

                    if (objSite.address != null && objSite.city != null && objSite.state != null && objSite.zip != null && objSite.county != null)
                    {
                        //Link 0 = address,1 = city,2 = state,3 = zip,4 = country
                        string sAddressFormat = "{0}<br />{1}, {2}, {3}<br />{4}";
                        mybtn.Text = string.Format(sAddressFormat, Utility.PrepareString(objSite.address.ToString())
                            , Utility.PrepareString(objSite.city)
                            , Utility.PrepareString(objSite.state.ToString())
                            , Utility.PrepareString(objSite.zip)
                            , Utility.PrepareString(objSite.county.ToString()));
                        lblSiteName.Text = Utility.PrepareString(objSite.site_name);

                        // Google Maps Link
                        StringBuilder sbGoogleSearch = new StringBuilder();
                        sbGoogleSearch.Append(objSite.address.ToString());
                        if (!String.IsNullOrWhiteSpace(objSite.city)) sbGoogleSearch.Append(", " + objSite.city);
                        if (!String.IsNullOrWhiteSpace(objSite.state.ToString())) sbGoogleSearch.Append(", " + objSite.state.ToString());
                        if (!String.IsNullOrWhiteSpace(objSite.zip)) sbGoogleSearch.Append(", " + objSite.zip);
                        if (!String.IsNullOrWhiteSpace(objSite.county.ToString())) sbGoogleSearch.Append(", " + objSite.county.ToString());
                        mybtn.NavigateUrl = Utility.GetGoogleMapLink(sbGoogleSearch.ToString());
                    }
                    else
                    {
                        lblAddressNA.Text = "Site Address is not Available.";
                        lblAddressNA.Visible = true;
                        mybtn.Visible = false;
                    }

                }
                else
                {
                    //Need default text or error raise.
                }
                customer objCus = Customer.Search(objSDP.customer_id);
                if (objCus != null)
                {
                    Utility.ControlValueSetter(lblCustomerName, objCus.customer_name, "(no customer found)");
                }

                divNoRelatedLeaseApps.Visible = true;
                if (objSDP.lease_application_id.HasValue)
                {
                    //Prepare data to bind with grid.
                    List<SDPRelatedLeaseApplication> lstResult = BLL.SiteDataPackage.GetRelatedLeaseApplication(objSDP.lease_application_id.Value,null);
                    if (lstResult != null && lstResult.Count > 0)
                    {
                        //Binding data to grid.
                        grdMain.DataSource = lstResult;
                        grdMain.DataBind();
                        divNoRelatedLeaseApps.Visible = false;
                    }
                }

                btnAllComment.HRef = ALL_COMMENTS_URL + objSDP.id;

            }

        }
        protected void BindLookupLists()
        {
            using (CPTTEntities ce = new CPTTEntities())
            {
                //General
                List<UserInfo> sort_UserInfo = BLL.User.GetUserInfo();
                var dynamicSort_UserInfo = new DynamicComparer<UserInfo>();
                dynamicSort_UserInfo.SortOrder(x => x.FullName);
                sort_UserInfo.Sort(dynamicSort_UserInfo);
                Utility.BindDDLDataSource(this.ddlSDP_EscalatedByEmp, sort_UserInfo, "ID", "FullName", "- Select -");
                Utility.BindDDLDataSource(this.ddlSDP_EscalationCompletedByEmp, sort_UserInfo, "ID", "FullName", "- Select -");
                
                //Document Statuses
                var sort_sdp_tower_drawing_doc_types = ce.sdp_tower_drawing_doc_types.ToList();
                var dynamicSort_sdp_tower_drawing_doc_types = new DynamicComparer<sdp_tower_drawing_doc_types>();
                dynamicSort_sdp_tower_drawing_doc_types.SortOrder(x => x.sdp_tower_drawing_doc_type);
                sort_sdp_tower_drawing_doc_types.Sort(dynamicSort_sdp_tower_drawing_doc_types);
                Utility.BindDDLDataSource(this.lstTowerDrawing_DocTypes,
                    sort_sdp_tower_drawing_doc_types, "id", "sdp_tower_drawing_doc_type",null);

                var sort_sdp_doc_statuses = ce.sdp_doc_statuses.ToList();
                var dynamicSort_sdp_doc_statuses = new DynamicComparer<sdp_doc_statuses>();
                dynamicSort_sdp_doc_statuses.SortOrder(x => x.sdp_doc_status);
                sort_sdp_doc_statuses.Sort(dynamicSort_sdp_doc_statuses);
                Utility.BindDDLDataSource(this.ddlTowerDrawing_DocStatus, sort_sdp_doc_statuses, "id", "sdp_doc_status", "- Select -");
                Utility.BindDDLDataSource(this.ddlTowerTagPhoto_DocStatus, sort_sdp_doc_statuses, "id", "sdp_doc_status", "- Select -");
                Utility.BindDDLDataSource(this.ddlStructuralCalcs_DocStatus, sort_sdp_doc_statuses, "id", "sdp_doc_status", "- Select -");
                Utility.BindDDLDataSource(this.ddlStructuralAnalysis_DocStatus, sort_sdp_doc_statuses, "id", "sdp_doc_status", "- Select -");
                Utility.BindDDLDataSource(this.ddlTowerErectionDetail_DocStatus, sort_sdp_doc_statuses, "id", "sdp_doc_status", "- Select -");
                Utility.BindDDLDataSource(this.ddlFoundationDesign_DocStatus, sort_sdp_doc_statuses, "id", "sdp_doc_status", "- Select -");
                Utility.BindDDLDataSource(this.ddlConstructionDrawings_DocStatus, sort_sdp_doc_statuses, "id", "sdp_doc_status", "- Select -");
                Utility.BindDDLDataSource(this.ddlGeotechReport_DocStatus, sort_sdp_doc_statuses, "id", "sdp_doc_status", "- Select -");
                Utility.BindDDLDataSource(this.ddlBuildingPermit_DocStatus, sort_sdp_doc_statuses, "id", "sdp_doc_status", "- Select -");
                Utility.BindDDLDataSource(this.ddlZoningApproval_DocStatus, sort_sdp_doc_statuses, "id", "sdp_doc_status", "- Select -");
                Utility.BindDDLDataSource(this.ddlPhaseI_DocStatus, sort_sdp_doc_statuses, "id", "sdp_doc_status", "- Select -");
                Utility.BindDDLDataSource(this.ddlPhaseII_DocStatus, sort_sdp_doc_statuses, "id", "sdp_doc_status", "- Select -");
                Utility.BindDDLDataSource(this.ddlNEPA_DocStatus, sort_sdp_doc_statuses, "id", "sdp_doc_status", "- Select -");
                Utility.BindDDLDataSource(this.ddlSHPO_DocStatus, sort_sdp_doc_statuses, "id", "sdp_doc_status", "- Select -");
                Utility.BindDDLDataSource(this.ddlGPS1a2c_DocStatus, sort_sdp_doc_statuses, "id", "sdp_doc_status", "- Select -");
                Utility.BindDDLDataSource(this.ddlAirspace_DocStatus, sort_sdp_doc_statuses, "id", "sdp_doc_status", "- Select -");
                Utility.BindDDLDataSource(this.ddlFAAStudyDet_DocStatus, sort_sdp_doc_statuses, "id", "sdp_doc_status", "- Select -");
                Utility.BindDDLDataSource(this.ddlFCC_DocStatus, sort_sdp_doc_statuses, "id", "sdp_doc_status", "- Select -");
                Utility.BindDDLDataSource(this.ddlAMCertification_DocStatus, sort_sdp_doc_statuses, "id", "sdp_doc_status", "- Select -");
                Utility.BindDDLDataSource(this.ddlTowair_DocStatus, sort_sdp_doc_statuses, "id", "sdp_doc_status", "- Select -");
                Utility.BindDDLDataSource(this.ddlPrimeLease_DocStatus, sort_sdp_doc_statuses, "id", "sdp_doc_status", "- Select -");
                Utility.BindDDLDataSource(this.ddlMemorandum_DocStatus, sort_sdp_doc_statuses, "id", "sdp_doc_status", "- Select -");
                Utility.BindDDLDataSource(this.ddlTitle_DocStatus, sort_sdp_doc_statuses, "id", "sdp_doc_status", "- Select -");
                //Abstract
                Utility.BindDDLDataSource(this.ddl_EmployeeAssigned, sort_UserInfo, "ID", "FullName", "- Select -");
                var sort_abstract_vendors = ce.abstract_vendors.ToList();
                var dynamicSort_abstract_vendors = new DynamicComparer<abstract_vendors>();
                dynamicSort_abstract_vendors.SortOrder(x => x.abstract_vendor);
                sort_abstract_vendors.Sort(dynamicSort_abstract_vendors);
                Utility.BindDDLDataSource(this.ddl_AbstractVendor, sort_abstract_vendors, "id","abstract_vendor","- Select -");
            }
        }

        protected void BindingSummary()
        {
            using(CPTTEntities ce = new CPTTEntities())
            {
                site_data_packages sdp = this.CurrentSiteDataPackage;
                if (sdp != null)
                {
                    ce.Attach(sdp);
                    //Summary
                    Utility.ControlValueSetter(this.lblSummarySiteID, sdp.site_uid);                                       

                    List<UserInfo> lstUser;
                    //Lease app depedent
                    if (sdp.lease_application_id.HasValue)
                    {
                        lblSpecialProject.Visible = false;
                        lblSpecialProjectFront.Visible = false;
                        lblSummaryCustomer.Visible = false;
                        lblSummaryCustomerFront.Visible = false;

                        lblLeaseAppActivity.Visible = true;
                        lblLeaseAppActivityFront.Visible = true;
                        lblLeaseAppType.Visible = true;
                        lblLeaseAppTypeFront.Visible = true;

                        lease_applications objLA = BLL.LeaseApplication.Search(sdp.lease_application_id.Value);
                        //TODO: Refactor later.
                        if (objLA == null)
                        {
                            Utility.ControlValueSetter(this.lblSDPTMOPMEmp, null, "N/A");
                            Utility.ControlValueSetter(this.lblSDPTMOSpecialistCurrentEmp, null, "N/A");
                            Utility.ControlValueSetter(this.lblSDPTMOAdminEmp, null, "N/A");
                            Utility.ControlValueSetter(this.lblLeaseAppActivity, null, "N/A");
                            Utility.ControlValueSetter(this.lblLeaseAppType, null, "N/A");
                        }
                        else
                        {
                            ce.Attach(objLA);
                            lstUser = BLL.User.GetUserInfo(objLA.tmo_pm_emp_id);
                            if (lstUser.Count > 0)
                            {
                                Utility.ControlValueSetter(this.lblSDPTMOPMEmp, lstUser[0].FullName, "N/A");
                            }
                            else
                            {
                                Utility.ControlValueSetter(this.lblSDPTMOPMEmp, null, "N/A");
                            }
                            
                            lstUser = BLL.User.GetUserInfo(objLA.tmo_specialist_current_emp_id);
                            if (lstUser.Count > 0)
                            {
                                Utility.ControlValueSetter(this.lblSDPTMOSpecialistCurrentEmp, lstUser[0].FullName, "N/A");
                            }
                            else
                            {
                                Utility.ControlValueSetter(this.lblSDPTMOSpecialistCurrentEmp, null, "N/A");
                            }
                            
                            lstUser = BLL.User.GetUserInfo(objLA.tmo_specialist_initial_emp_id);
                            if (lstUser.Count > 0)
                            {
                                Utility.ControlValueSetter(this.lblSDPTMOAdminEmp, lstUser[0].FullName, "N/A");
                            }
                            else
                            {
                                Utility.ControlValueSetter(this.lblSDPTMOAdminEmp, null, "N/A");
                            }

                            if (objLA.leaseapp_activities != null)
                            {
                                Utility.ControlValueSetter(this.lblLeaseAppActivity, objLA.leaseapp_activities.lease_app_activity,"N/A");
                            }
                            else
                            {
                                Utility.ControlValueSetter(this.lblLeaseAppActivity, null, "N/A");
                            }
                            if (objLA.leaseapp_types != null)
                            {
                                Utility.ControlValueSetter(this.lblLeaseAppType, objLA.leaseapp_types.lease_application_type, "N/A");
                            }
                            else
                            {
                                Utility.ControlValueSetter(this.lblLeaseAppType, null, "N/A");
                            }
                            ce.Detach(objLA);
                        }                                                
                    }
                    else
                    {
                        //TODO: use until entity context has relationship with special project id.
                        var querySP = from sp in ce.sdp_special_projects
                                      where sp.id == sdp.sdp_special_project_id
                                      select sp;
                        if (querySP.ToList().Count > 0)
                        {
                            sdp_special_projects objSP = querySP.ToList<sdp_special_projects>()[0];
                            Utility.ControlValueSetter(this.lblSpecialProject, objSP.sdp_special_project, "N/A");
                        }
                        else
                        {
                            Utility.ControlValueSetter(this.lblSpecialProject, null, "N/A");
                        }
                        
                        if (sdp.customer != null && !String.IsNullOrEmpty(sdp.customer.customer_name))
                        {
                           Utility.ControlValueSetter(this.lblSummaryCustomer, sdp.customer.customer_name, "N/A");
                        }
                        
                        lstUser = BLL.User.GetUserInfo(sdp.tmo_pm_emp_id);
                        if (lstUser.Count > 0 && sdp.tmo_pm_emp_id.HasValue)
                        {
                            Utility.ControlValueSetter(this.lblSDPTMOPMEmp, lstUser[0].FullName, "N/A");
                        }
                        else
                        {
                            Utility.ControlValueSetter(this.lblSDPTMOPMEmp, null, "N/A");
                        }
                        lstUser = BLL.User.GetUserInfo(sdp.tmo_specialist_current_emp_id);
                        if (lstUser.Count > 0 && sdp.tmo_specialist_current_emp_id.HasValue)
                        {
                            Utility.ControlValueSetter(this.lblSDPTMOSpecialistCurrentEmp, lstUser[0].FullName, "N/A");
                        }
                        else
                        {
                            Utility.ControlValueSetter(this.lblSDPTMOSpecialistCurrentEmp, null, "N/A");
                        }
                        lstUser = BLL.User.GetUserInfo(sdp.tmo_admin_emp_id);
                        if (lstUser.Count > 0 && sdp.tmo_admin_emp_id.HasValue)
                        {
                            Utility.ControlValueSetter(this.lblSDPTMOAdminEmp, lstUser[0].FullName, "N/A");
                        }
                        else
                        {
                            Utility.ControlValueSetter(this.lblSDPTMOAdminEmp, null, "N/A");
                        }
                    }
                    
                    // Lease application independent data.
                    lstUser = BLL.User.GetUserInfo(sdp.sdp_assigned_to_emp_id);
                    if (lstUser.Count > 0 && sdp.sdp_assigned_to_emp_id.HasValue)
                    {
                        Utility.ControlValueSetter(this.lblSDPAssignedToEmp, lstUser[0].FullName, "N/A");
                    }
                    else
                    {
                        Utility.ControlValueSetter(this.lblSDPAssignedToEmp, null, "N/A");
                    }
                    if (sdp.sdp_priorities != null)
                    {
                        Utility.ControlValueSetter(this.lblSummarySDPPriority, sdp.sdp_priorities.sdp_priority, "N/A");
                    }
                    else
                    {
                        Utility.ControlValueSetter(this.lblSummarySDPPriority, null, "N/A");
                    }
                     
                    ce.Detach(sdp);
                }
            }
        }
        protected void BindingGeneralData()
        {
            site_data_packages sdp = this.CurrentSiteDataPackage;

            if (sdp != null)
            {
                Utility.ControlValueSetter(this.txtSDP_RequestDate, sdp.sdp_request_date);
                Utility.ControlValueSetter(this.txtSDP_DueDate, sdp.sdp_due_by_date);
                Utility.ControlValueSetter(this.txtSDP_DueDateNotes, sdp.sdp_due_date_notes);
                Utility.ControlValueSetter(this.txtFirstSweepDate, sdp.first_sweep_completed_date);
                Utility.ControlValueSetter(this.txtSDP_NoticeOfCompletionDate, sdp.sdp_notice_of_completion_date);
                Utility.ControlValueSetter(this.txtSDP_CompletedDate, sdp.sdp_completed_date);
                Utility.ControlValueSetter(this.txtFirstMarketDate, sdp.first_notice_to_market_date);
                Utility.ControlValueSetter(this.txtSecondMarketDate, sdp.second_notice_to_market_date);
                Utility.ControlValueSetter(this.txtThirdMarketDate, sdp.third_notice_to_market_date);
                Utility.ControlValueSetter(this.ddlSDP_EscalatedByEmp, sdp.escalated_by_emp_id);
                Utility.ControlValueSetter(this.txtEscalationDate, sdp.escalation_date);
                Utility.ControlValueSetter(this.ddlSDP_EscalationCompletedByEmp, sdp.escalation_completed_by_emp_id);
                Utility.ControlValueSetter(this.txtEscalationCompletedDate, sdp.escalation_completed_date);
                Utility.ControlValueSetter(this.txtVendorOrderReceived, sdp.po_for_sa_ordered_date);
                Utility.ControlValueSetter(this.txtVendorOrderPOReceived, sdp.po_for_sa_rcvd_date);
                Utility.ControlValueSetter(this.txtCostApprovalDate, sdp.hard_cost_approval_date);
                Utility.ControlValueSetter(this.txtPrimeLeaseExecDate, sdp.prime_lease_exec_date);
                Utility.ControlValueSetter(this.txtPermitAppliedDate, sdp.permit_applied_date);
                Utility.ControlValueSetter(this.txtConstructionStartDate, sdp.construction_start_date);
            }
        }
        protected void BindingDocumentStatusData()
        {
            site_data_packages sdp = this.CurrentSiteDataPackage;
            if (sdp != null)
            {
                using (CPTTEntities ce = new CPTTEntities())
                {
                    ce.Attach(sdp);
                    List<sdp_tower_drawings> towerDrawings = SDPDrawingTypes.GetAllTowerDrawing(sdp.id) ;
                    if (towerDrawings.Count > 0)
                    {
                        foreach (sdp_tower_drawings currDrawing in towerDrawings)
                        {
                            lstTowerDrawing_DocTypes.Items.FindByValue(currDrawing.sdp_tower_drawing_doc_type_id.ToString()).Selected = true;
                        }
                    }
                    Utility.ControlValueSetter(this.ddlTowerDrawing_DocStatus, sdp.tower_drawings_doc_status_id);
                    Utility.ControlValueSetter(this.ddlTowerTagPhoto_DocStatus, sdp.tower_tag_photo_doc_status_id);
                    Utility.ControlValueSetter(this.ddlStructuralCalcs_DocStatus, sdp.structural_calcs_doc_status_id);
                    Utility.ControlValueSetter(this.ddlStructuralAnalysis_DocStatus, sdp.structural_analysis_current_doc_status_id);
                    Utility.ControlValueSetter(this.ddlTowerErectionDetail_DocStatus, sdp.tower_erection_detail_doc_status_id);
                    Utility.ControlValueSetter(this.ddlFoundationDesign_DocStatus, sdp.foundation_design_doc_status_id);
                    Utility.ControlValueSetter(this.ddlConstructionDrawings_DocStatus, sdp.construction_drawings_doc_status_id);
                    Utility.ControlValueSetter(this.ddlGeotechReport_DocStatus, sdp.geotech_report_doc_status_id);
                    Utility.ControlValueSetter(this.ddlBuildingPermit_DocStatus, sdp.building_permit_doc_status_id);
                    Utility.ControlValueSetter(this.ddlZoningApproval_DocStatus, sdp.zoning_approval_doc_status_id);
                    Utility.ControlValueSetter(this.ddlPhaseI_DocStatus, sdp.phase_i_doc_status_id);
                    Utility.ControlValueSetter(this.ddlPhaseII_DocStatus, sdp.phase_ii_doc_status_id);
                    Utility.ControlValueSetter(this.ddlNEPA_DocStatus, sdp.nepa_doc_status_id);
                    Utility.ControlValueSetter(this.ddlSHPO_DocStatus, sdp.shpo_doc_status_id);
                    Utility.ControlValueSetter(this.ddlGPS1a2c_DocStatus, sdp.gps_1a2c_doc_status_id);
                    Utility.ControlValueSetter(this.ddlAirspace_DocStatus, sdp.airspace_doc_status_id);
                    Utility.ControlValueSetter(this.ddlFAAStudyDet_DocStatus, sdp.faa_study_determination_doc_status_id);
                    Utility.ControlValueSetter(this.ddlFCC_DocStatus, sdp.fcc_doc_status_id);
                    Utility.ControlValueSetter(this.ddlAMCertification_DocStatus, sdp.am_certification_doc_status_id);
                    Utility.ControlValueSetter(this.ddlTowair_DocStatus, sdp.towair_doc_status_id);
                    Utility.ControlValueSetter(this.ddlPrimeLease_DocStatus, sdp.prime_lease_doc_status_id);
                    Utility.ControlValueSetter(this.ddlMemorandum_DocStatus, sdp.memorandum_of_lease_doc_status_id);
                    Utility.ControlValueSetter(this.ddlTitle_DocStatus, sdp.title_doc_status_id);

                    //create sdp document
                    GetLinkDocument(sdp.id);

                    ce.Detach(sdp);
                }
            }
        }
        protected void BindingAbstractData()
        {
            site_data_packages sdp = this.CurrentSiteDataPackage;
            if (sdp != null)
            {
                Utility.ControlValueSetter(this.ddl_EmployeeAssigned, sdp.abstract_assigned_to_emp_id);
                Utility.ControlValueSetter(this.txt_Assigned, sdp.abstract_assigned_date);
                Utility.ControlValueSetter(this.txt_Completed, sdp.abstract_completed_date);
                Utility.ControlValueSetter(this.txt_AbstractNotes, sdp.abstract_notes);
                Utility.ControlValueSetter(this.ddl_AbstractVendor, sdp.abstract_vendor_id);
            }
        }

        protected void CommentBinding(Boolean isGetAllReleate = false)
        {
            site_data_packages sdp = this.CurrentSiteDataPackage;
            if (sdp != null)
            {
                divComment1.Visible = false;
                divComment2.Visible = false;
                divComment3.Visible = false;
                divComment4.Visible = false;
                btnAllComment.Visible = false;
                List<AllComments> listComments = AllCommentsHelper.GetAllReleateSdpComment(sdp.id, false, true);
                AllComments currentComment;

                //Temp use before finding the way to iterate web UI control.
                System.Web.UI.HtmlControls.HtmlGenericControl pCommenter;
                System.Web.UI.HtmlControls.HtmlGenericControl divComment;
                System.Web.UI.HtmlControls.HtmlGenericControl pComment;
                System.Web.UI.HtmlControls.HtmlGenericControl pDocument;
                Image imgComment;
                ImageButton btnDeleteComment;
                List<System.Web.UI.HtmlControls.HtmlGenericControl> lstPCommenter = new List<System.Web.UI.HtmlControls.HtmlGenericControl>();
                List<System.Web.UI.HtmlControls.HtmlGenericControl> lstPComments = new List<System.Web.UI.HtmlControls.HtmlGenericControl>();
                List<System.Web.UI.HtmlControls.HtmlGenericControl> lstDivComment = new List<System.Web.UI.HtmlControls.HtmlGenericControl>();
                List<System.Web.UI.HtmlControls.HtmlGenericControl> lstDivDocument = new List<System.Web.UI.HtmlControls.HtmlGenericControl>();
                List<ImageButton> lstBtnDelete = new List<ImageButton>();
                List<Image> lstImageAvatar = new List<Image>();
                lstPCommenter.Add(pCommenter1);
                lstPCommenter.Add(pCommenter2);
                lstPCommenter.Add(pCommenter3);
                lstPCommenter.Add(pCommenter4);
                lstPComments.Add(pComment1);
                lstPComments.Add(pComment2);
                lstPComments.Add(pComment3);
                lstPComments.Add(pComment4);
                lstDivComment.Add(divComment1);
                lstDivComment.Add(divComment2);
                lstDivComment.Add(divComment3);
                lstDivComment.Add(divComment4);
                lstBtnDelete.Add(btnDeleteComment1);
                lstBtnDelete.Add(btnDeleteComment2);
                lstBtnDelete.Add(btnDeleteComment3);
                lstBtnDelete.Add(btnDeleteComment4);
                lstImageAvatar.Add(imgUserComment1);
                lstImageAvatar.Add(imgUserComment2);
                lstImageAvatar.Add(imgUserComment3);
                lstImageAvatar.Add(imgUserComment4);
                lstDivDocument.Add(pnlDocument1);
                lstDivDocument.Add(pnlDocument2);
                lstDivDocument.Add(pnlDocument3);
                lstDivDocument.Add(pnlDocument4);
                user objUser = null;

                hidden_count_main_comment.Value = listComments.Count.ToString();
                if (listComments.Count > 0)
                {
                    btnAllComment.Visible = true;
                    btnAllComment.InnerText = String.Format("see all {0} comments", listComments.Count);
                    if (!btnAllComment.HRef.Contains("showAllReleate"))
                    {
                        btnAllRelateComment.Visible = true;
                    }
                }
                btnAllRelateComment.Visible = true;

                if (!isGetAllReleate)
                {
                    var focusComment = from listComment in listComments
                                       where (listComment.Source.Equals(AllComments.SDP) || listComment.SdpID == sdp.id)
                                       select listComment;
                    listComments = focusComment.AsQueryable().OrderByDescending(a => a.CreateAt).ToList();
                }
                else
                {
                    var focusComment = from listComment in listComments
                                       where (listComment.Source.Equals(AllComments.SDP) || listComment.SdpID == sdp.id)
                                       || listComment.Source.Equals(AllComments.LEASE_ADMIN)
                                       || listComment.Source.Equals(AllComments.LEASE_APPLICATION)
                                       || listComment.Source.Equals(AllComments.STRUCTURAL_ANALYSIS)
                                       select listComment;
                    listComments = focusComment.AsQueryable().OrderByDescending(a => a.CreateAt).ToList();
                }

                string commenter;
                string sCommenterFormat = "{0}, {1}";//name , date

                //Add link document for all comments
                GetAllCommentLinkDocument(listComments);

                string sImageUrl = "";
                for (int i = 0; i < listComments.Count && i < 4; i++)
                {
                    currentComment = listComments[i];
                    pCommenter = lstPCommenter[i];
                    pComment = lstPComments[i];
                    divComment = lstDivComment[i];
                    btnDeleteComment = lstBtnDelete[i];
                    imgComment = lstImageAvatar[i];
                    pDocument = lstDivDocument[i];

                    if (!string.IsNullOrEmpty(currentComment.CreatorName))
                    {
                        commenter = string.Format(CultureInfo.CurrentCulture, sCommenterFormat, currentComment.CreatorName,
                                                  Utility.DateToString(currentComment.CreateAt));
                    }
                    else
                    {
                        commenter = string.Format(CultureInfo.CurrentCulture, sCommenterFormat, "No employee recorded",
                                                  Utility.DateToString(currentComment.CreateAt));
                    }
                    

                    sImageUrl = BLL.User.GetUserAvatarImage(currentComment.CreatorUsername);

                    if (!Security.UserCanRemoveComment(User.Identity.Name, (int)Session["userId"], currentComment.CommentID, currentComment.Source))
                    {
                        btnDeleteComment.Visible = false;
                    }

                    imgComment.ImageUrl = sImageUrl;
                    pCommenter.InnerText = commenter;
                    divComment.Visible = true;
                    if (currentComment.Mode == "eSDP" && currentComment.SdpID == sdp.id)
                    {
                        pComment.InnerHtml = currentComment.Comments;
                    }
                    else
                    {
                        pComment.InnerHtml = currentComment.CheckTMAndSdp;
                    }
                    btnDeleteComment.AlternateText = currentComment.Source + "/" + currentComment.CommentID.ToString();

                    if (currentComment.Mode != "eLE")
                    {
                        pDocument.Attributes.Add("class", "document-comment-" + currentComment.CommentID);
                    }

                    int commentID = listComments[i].CommentID;

                    if (divComment.Attributes["saCommentId"] != null)
                    {
                        divComment.Attributes.Remove("saCommentId");
                    }

                    if (divComment.Attributes["leaseAdminCommentId"] != null)
                    {
                        divComment.Attributes.Remove("leaseAdminCommentId");
                    }

                    if (listComments[i].Mode == "eSA")
                    {
                        divComment.Attributes.Add("saCommentId", "SA_" + commentID);
                    }
                    else if (listComments[i].Mode == "eLE")
                    {
                        divComment.Attributes.Add("leaseAdminCommentId", "LeaseAdmin_" + commentID);
                    }
                }
            }
        }

        protected void BindTowerImages(string siteId)
        {
            DMSHelper imageHelper = new DMSHelper();
            List<PhotoInfo> siteImages = new List<PhotoInfo>();

            siteImages = imageHelper.GetSitePhotos(siteId, false, string.Empty);
            Image2.ImageUrl = siteImages[0].photoURL;
            Image2.AlternateText = siteImages[0].photoID;
        }
        #endregion

        protected void SaveSDP()
        {
            site_data_packages sdp = this.CurrentSiteDataPackage;
            List<sdp_tower_drawings> lstTD = new List<sdp_tower_drawings>();
            string status;

            if (sdp != null)
            {
                //General
                sdp.sdp_request_date = Utility.PrepareDate(this.txtSDP_RequestDate.Text);
                sdp.sdp_due_by_date = Utility.PrepareDate(this.txtSDP_DueDate.Text);
                sdp.sdp_due_date_notes = Utility.PrepareString(this.txtSDP_DueDateNotes.Text);
                sdp.first_sweep_completed_date = Utility.PrepareDate(this.txtFirstSweepDate.Text);
                sdp.sdp_notice_of_completion_date = Utility.PrepareDate(this.txtSDP_NoticeOfCompletionDate.Text);
                sdp.sdp_completed_date = Utility.PrepareDate(this.txtSDP_CompletedDate.Text);
                sdp.first_notice_to_market_date = Utility.PrepareDate(this.txtFirstMarketDate.Text);
                sdp.second_notice_to_market_date = Utility.PrepareDate(this.txtSecondMarketDate.Text);
                sdp.third_notice_to_market_date = Utility.PrepareDate(this.txtThirdMarketDate.Text);
                sdp.escalated_by_emp_id = Utility.PrepareInt(this.ddlSDP_EscalatedByEmp.SelectedValue);
                sdp.escalation_date = Utility.PrepareDate(this.txtEscalationDate.Text);
                sdp.escalation_completed_by_emp_id = Utility.PrepareInt(this.ddlSDP_EscalationCompletedByEmp.SelectedValue);
                sdp.escalation_completed_date = Utility.PrepareDate(this.txtEscalationCompletedDate.Text);
                sdp.po_for_sa_ordered_date = Utility.PrepareDate(this.txtVendorOrderReceived.Text);
                sdp.po_for_sa_rcvd_date = Utility.PrepareDate(this.txtVendorOrderPOReceived.Text);
                sdp.hard_cost_approval_date = Utility.PrepareDate(this.txtCostApprovalDate.Text);
                sdp.prime_lease_exec_date = Utility.PrepareDate(this.txtPrimeLeaseExecDate.Text);
                sdp.permit_applied_date = Utility.PrepareDate(this.txtPermitAppliedDate.Text);
                sdp.construction_start_date = Utility.PrepareDate(this.txtConstructionStartDate.Text);

                //Doc status
                sdp.tower_drawings_doc_status_id = Utility.PrepareInt(this.ddlTowerDrawing_DocStatus.SelectedValue);
                sdp.tower_tag_photo_doc_status_id = Utility.PrepareInt(this.ddlTowerTagPhoto_DocStatus.SelectedValue);
                sdp.structural_calcs_doc_status_id = Utility.PrepareInt(this.ddlStructuralCalcs_DocStatus.SelectedValue);
                sdp.structural_analysis_current_doc_status_id = Utility.PrepareInt(this.ddlStructuralAnalysis_DocStatus.SelectedValue);
                sdp.tower_erection_detail_doc_status_id = Utility.PrepareInt(this.ddlTowerErectionDetail_DocStatus.SelectedValue);
                sdp.foundation_design_doc_status_id = Utility.PrepareInt(this.ddlFoundationDesign_DocStatus.SelectedValue);
                sdp.construction_drawings_doc_status_id = Utility.PrepareInt(this.ddlConstructionDrawings_DocStatus.SelectedValue);
                sdp.geotech_report_doc_status_id = Utility.PrepareInt(this.ddlGeotechReport_DocStatus.SelectedValue);
                sdp.building_permit_doc_status_id = Utility.PrepareInt(this.ddlBuildingPermit_DocStatus.SelectedValue);
                sdp.zoning_approval_doc_status_id = Utility.PrepareInt(this.ddlZoningApproval_DocStatus.SelectedValue);
                sdp.phase_i_doc_status_id = Utility.PrepareInt(this.ddlPhaseI_DocStatus.SelectedValue);
                sdp.phase_ii_doc_status_id = Utility.PrepareInt(this.ddlPhaseII_DocStatus.SelectedValue);
                sdp.nepa_doc_status_id = Utility.PrepareInt(this.ddlNEPA_DocStatus.SelectedValue);
                sdp.shpo_doc_status_id = Utility.PrepareInt(this.ddlSHPO_DocStatus.SelectedValue);
                sdp.gps_1a2c_doc_status_id = Utility.PrepareInt(this.ddlGPS1a2c_DocStatus.SelectedValue);
                sdp.airspace_doc_status_id = Utility.PrepareInt(this.ddlAirspace_DocStatus.SelectedValue);
                sdp.faa_study_determination_doc_status_id = Utility.PrepareInt(this.ddlFAAStudyDet_DocStatus.SelectedValue);
                sdp.fcc_doc_status_id = Utility.PrepareInt(this.ddlFCC_DocStatus.SelectedValue);
                sdp.am_certification_doc_status_id = Utility.PrepareInt(this.ddlAMCertification_DocStatus.SelectedValue);
                sdp.towair_doc_status_id = Utility.PrepareInt(this.ddlTowair_DocStatus.SelectedValue);
                sdp.prime_lease_doc_status_id = Utility.PrepareInt(this.ddlPrimeLease_DocStatus.SelectedValue);
                sdp.memorandum_of_lease_doc_status_id = Utility.PrepareInt(this.ddlMemorandum_DocStatus.SelectedValue);
                sdp.title_doc_status_id = Utility.PrepareInt(this.ddlTitle_DocStatus.SelectedValue);

                //Abstract
                sdp.abstract_assigned_to_emp_id = Utility.PrepareInt(this.ddl_EmployeeAssigned.SelectedValue);
                sdp.abstract_assigned_date = Utility.PrepareDate(this.txt_Assigned.Text);
                sdp.abstract_completed_date = Utility.PrepareDate(this.txt_Completed.Text);
                sdp.abstract_notes = Utility.PrepareString(this.txt_AbstractNotes.Text);
                sdp.abstract_vendor_id = Utility.PrepareInt(this.ddl_AbstractVendor.SelectedValue);

                // Publish Document
                sdp.publish_document = this.chkShowDocument.Checked;

                try
                {                                      
                   if (lstTowerDrawing_DocTypes.Items.Count > 0)
                    {                       
                        sdp_tower_drawings obj;
                        foreach (ListItem item in lstTowerDrawing_DocTypes.Items)
                        {
                            if (item.Selected)
                            {
                                obj = new sdp_tower_drawings();
                                obj.site_data_package_id = sdp.id;
                                obj.sdp_tower_drawing_doc_type_id = int.Parse(item.Value);
                                lstTD.Add(obj);
                            }
                        }                        
                    }

                    status = BLL.SiteDataPackage.Update(sdp, lstTD);

                    // No status message indicates success.
                    if (String.IsNullOrEmpty(status))
                    {
                       BindApplication(sdp.id);
                       BindingSummary();
                       Master.StatusBox.showStatusMessage("Site Data Package Updated.");
                    }
                    else
                    {
                       Master.StatusBox.showErrorMessage(status);
                    }
                }
                catch (Exception)
                {
                   Master.StatusBox.showErrorMessage("An error occurred updating site data packages. Please try again.");
                }                
            }
        }

        protected void btnSubmitSDP_General_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                SaveSDP();
                this.CurrentTab = eTabName.General;
            }
        }

        protected void btnSubmitDocStatus_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                SaveSDP();
                this.CurrentTab = eTabName.DocumentStatuses;
               
            }
        }

        protected void btnSubmitAbstract_Click(object sender, EventArgs e)
        {
             if (Page.IsValid)
            {
                SaveSDP();
                this.CurrentTab = eTabName.Abstract;
            }
        }

        protected void btnSdpDelete_Click(object sender, EventArgs e)
        {
            String sError;
            site_data_packages sdp = CurrentSiteDataPackage;
            if (sdp != null)
            {
                sError = BLL.SiteDataPackage.Delete(sdp.id);

                // An empty or null sError indicates success.
                if (String.IsNullOrEmpty(sError))
                {
                   //Successful delete.
                   Master.StatusBox.showStatusMessageWithRedirect("Record deleted.", "Default.aspx");
                }
                else
                {
                    //Error occur.
                    Master.StatusBox.showStatusMessage("Error: some related records cannot be deleted; operation aborted.");
                }
            }
        }

        protected void chkShowDocument_CheckedChanged(object sender, EventArgs e)
        {
            site_data_packages sdp = this.CurrentSiteDataPackage;
            BLL.SiteDataPackage.UpdatePublishDocumentStatus(sdp.id, this.chkShowDocument.Checked);
            //check case uncheck not linked document
            if (!this.chkShowDocument.Checked)
            {
                lease_applications objLeaseApp = LeaseApplication.Search(sdp.lease_application_id.Value);
                if ((objLeaseApp != null) && (!String.IsNullOrEmpty(objLeaseApp.TMO_AppID)))
                {
                    this.chkShowDocument.Enabled = true;
                }
                else
                {
                    this.chkShowDocument.Enabled = false;
                }
            }
            ScriptManager.RegisterStartupScript(this.updateShowDocument, updateShowDocument.GetType(), "UpadateShowDocument", "upadateShowDocument()", true);
        }

        #region Comment

        protected void btnPost_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                site_data_packages sdp = this.CurrentSiteDataPackage;
                if (sdp != null)
                {
                    sdp_comments objComment = new sdp_comments();
                    objComment.comments = Server.HtmlEncode(txtComments.Text);
                    objComment.site_data_package_id = sdp.id;
                    SdpComment.Add(objComment);
                    BindApplication(sdp.id);
                    CommentBinding();
                }
            }
        }

        protected void btnDeleteComment_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                ImageButton objRef = sender as ImageButton;
                int id;
                string[] sTmp;
                string source;

                if (objRef != null && objRef.AlternateText != "")
                {
                    sTmp = objRef.AlternateText.Split('/');
                    if (sTmp.Length == 2)
                    {
                        source = sTmp[0];
                        id = int.Parse(sTmp[1]);
                        if (source == AllComments.LEASE_APPLICATION)
                        {
                            LeaseAppComment.Delete(id);
                        }
                        else if (source == AllComments.LEASE_ADMIN)
                        {
                            LeaseAdminComments.Delete(id);
                            // remove another leaseAdmin comment dialog
                        }
                        else if (source == AllComments.SDP)
                        {
                            SdpComment.Delete(id);
                            // remove another sdp comment dialog
                        }
                        else if (source == AllComments.TOWER_MOD)
                        {
                            TowerModComment.Delete(id);
                            // remove another towerMod comment dialog
                        }
                        else
                        {
                            LeaseAppSAComment.Delete(id);
                            // remove another sa comment dialog
                        }
                    }

                    BindApplication(CurrentSiteDataPackage.id);
                    CommentBinding();
                    Master.StatusBox.showStatusMessage("Comment Deleted.");
                }
            }
        }

        protected void btnAllRelateComment_Click(object sender, EventArgs e)
        {
            if (btnAllRelateComment.Text == "Hide related comments")
            {
                btnAllRelateComment.Text = "Show related comments";
                CommentBinding();
            }
            else
            {
                btnAllRelateComment.Text = "Hide related comments";
                CommentBinding(true);
            }
        }

        #endregion Comment

        #region Document Link

        private void GetAllCommentLinkDocument(List<AllComments> lstComment)
        {
            this.hidden_document_all_document_comments.Value = String.Empty;
            List<List<Dms_Document>> allComments = new List<List<Dms_Document>>();
            List<AllComments> allcomments = lstComment;
            foreach (AllComments comment in allcomments)
            {
                DMSDocumentLinkHelper.eDocumentField efield;
                if (comment.Mode == "eSA")
                {
                    efield = DMSDocumentLinkHelper.eDocumentField.SA_Comments;
                }
                else if (comment.Mode == "eLE")
                {
                    //Lease admin set field null because not document link
                    efield = DMSDocumentLinkHelper.eDocumentField.LA_Comments;
                }
                else if (comment.Mode == "eTM")
                {
                    efield = DMSDocumentLinkHelper.eDocumentField.TMO_Comments;
                }
                else if (comment.Mode == "eSDP")
                {
                    efield = DMSDocumentLinkHelper.eDocumentField.SDP_Comments;
                }
                else
                {
                    efield = DMSDocumentLinkHelper.eDocumentField.LE_Comments;
                }

                if (efield != DMSDocumentLinkHelper.eDocumentField.LA_Comments && efield != DMSDocumentLinkHelper.eDocumentField.SDP_Comments)
                {
                    List<Dms_Document> docData = DMSDocumentLinkHelper.GetDocumentLink(comment.CommentID, DMSDocumentLinkHelper.DocumentField(efield.ToString()));
                    if (docData.Count == 0)
                    {
                        Dms_Document doc = new Dms_Document();
                        doc.ID = comment.CommentID;
                        doc.ref_field = efield.ToString();
                        docData.Add(doc);
                    }
                    allComments.Add(docData);
                }
            }
            this.hidden_document_all_document_comments.Value = Newtonsoft.Json.JsonConvert.SerializeObject(allComments);
        }
        private void GetLinkDocument(int sdpID)
        {
            // SDP Document
            List<Dms_Document> sdpDoc = new List<Dms_Document>();
            
            //General
            //SDPCheckList
            sdpDoc = DMSDocumentLinkHelper.GetDocumentLink(sdpID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.SDPCheckList.ToString()));
            SetLinkDocument(sdpID, DMSDocumentLinkHelper.eDocumentField.SDPCheckList, sdpDoc);

            //AdditionalPrimeDocuments
            sdpDoc = DMSDocumentLinkHelper.GetDocumentLink(sdpID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.AdditionalPrimeDocuments.ToString()));
            SetLinkDocument(sdpID, DMSDocumentLinkHelper.eDocumentField.AdditionalPrimeDocuments, sdpDoc);

            //SDPFirstNoticeToMarket
            sdpDoc = DMSDocumentLinkHelper.GetDocumentLink(sdpID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.SDPFirstNoticeToMarket.ToString()));
            SetLinkDocument(sdpID, DMSDocumentLinkHelper.eDocumentField.SDPFirstNoticeToMarket, sdpDoc);

            //SDPSecondNoticeToMarket
            sdpDoc = DMSDocumentLinkHelper.GetDocumentLink(sdpID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.SDPSecondNoticeToMarket.ToString()));
            SetLinkDocument(sdpID, DMSDocumentLinkHelper.eDocumentField.SDPSecondNoticeToMarket, sdpDoc);

            //SDPThirdNoticeToMarket
            sdpDoc = DMSDocumentLinkHelper.GetDocumentLink(sdpID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.SDPThirdNoticeToMarket.ToString()));
            SetLinkDocument(sdpID, DMSDocumentLinkHelper.eDocumentField.SDPThirdNoticeToMarket, sdpDoc);

            //Document Status
            //TowerDrawing_DocTypes
            sdpDoc = DMSDocumentLinkHelper.GetDocumentLink(sdpID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.TowerDrawing_DocTypes.ToString()));
            SetLinkDocument(sdpID, DMSDocumentLinkHelper.eDocumentField.TowerDrawing_DocTypes, sdpDoc);

            //TowerDrawing_DocStatus
            sdpDoc = DMSDocumentLinkHelper.GetDocumentLink(sdpID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.TowerDrawing_DocStatus.ToString()));
            SetLinkDocument(sdpID,DMSDocumentLinkHelper.eDocumentField.TowerDrawing_DocStatus, sdpDoc);

            //TowerTagPhoto_DocStatus
            sdpDoc = DMSDocumentLinkHelper.GetDocumentLink(sdpID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.TowerTagPhoto_DocStatus.ToString()));
            SetLinkDocument(sdpID,DMSDocumentLinkHelper.eDocumentField.TowerTagPhoto_DocStatus, sdpDoc);

            //StructuralCalcs_DocStatus
            sdpDoc = DMSDocumentLinkHelper.GetDocumentLink(sdpID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.StructuralCalcs_DocStatus.ToString()));
            SetLinkDocument(sdpID, DMSDocumentLinkHelper.eDocumentField.StructuralCalcs_DocStatus, sdpDoc);

            //StructuralAnalysis_DocStatus
            sdpDoc = DMSDocumentLinkHelper.GetDocumentLink(sdpID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.StructuralAnalysis_DocStatus.ToString()));
            SetLinkDocument(sdpID, DMSDocumentLinkHelper.eDocumentField.StructuralAnalysis_DocStatus, sdpDoc);

            //TowerErectionDetail_DocStatus
            sdpDoc = DMSDocumentLinkHelper.GetDocumentLink(sdpID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.TowerErectionDetail_DocStatus.ToString()));
            SetLinkDocument(sdpID, DMSDocumentLinkHelper.eDocumentField.TowerErectionDetail_DocStatus, sdpDoc);

            //FoundationDesign_DocStatus
            sdpDoc = DMSDocumentLinkHelper.GetDocumentLink(sdpID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.FoundationDesign_DocStatus.ToString()));
            SetLinkDocument(sdpID, DMSDocumentLinkHelper.eDocumentField.FoundationDesign_DocStatus, sdpDoc);

            //ConstructionDrawings_DocStatus
            sdpDoc = DMSDocumentLinkHelper.GetDocumentLink(sdpID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.ConstructionDrawings_DocStatus.ToString()));
            SetLinkDocument(sdpID, DMSDocumentLinkHelper.eDocumentField.ConstructionDrawings_DocStatus, sdpDoc);

            //GeotechReport_DocStatus
            sdpDoc = DMSDocumentLinkHelper.GetDocumentLink(sdpID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.GeotechReport_DocStatus.ToString()));
            SetLinkDocument(sdpID, DMSDocumentLinkHelper.eDocumentField.GeotechReport_DocStatus, sdpDoc);

            //BuildingPermit_DocStatus
            sdpDoc = DMSDocumentLinkHelper.GetDocumentLink(sdpID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.BuildingPermit_DocStatus.ToString()));
            SetLinkDocument(sdpID, DMSDocumentLinkHelper.eDocumentField.BuildingPermit_DocStatus, sdpDoc);

            //ZoningApproval_DocStatus
            sdpDoc = DMSDocumentLinkHelper.GetDocumentLink(sdpID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.ZoningApproval_DocStatus.ToString()));
            SetLinkDocument(sdpID, DMSDocumentLinkHelper.eDocumentField.ZoningApproval_DocStatus, sdpDoc);

            //PhaseI_DocStatus
            sdpDoc = DMSDocumentLinkHelper.GetDocumentLink(sdpID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.PhaseI_DocStatus.ToString()));
            SetLinkDocument(sdpID, DMSDocumentLinkHelper.eDocumentField.PhaseI_DocStatus, sdpDoc);

            //PhaseII_DocStatus
            sdpDoc = DMSDocumentLinkHelper.GetDocumentLink(sdpID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.PhaseII_DocStatus.ToString()));
            SetLinkDocument(sdpID, DMSDocumentLinkHelper.eDocumentField.PhaseII_DocStatus, sdpDoc);

            //NEPA_DocStatus
            sdpDoc = DMSDocumentLinkHelper.GetDocumentLink(sdpID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.NEPA_DocStatus.ToString()));
            SetLinkDocument(sdpID, DMSDocumentLinkHelper.eDocumentField.NEPA_DocStatus, sdpDoc);

            //SHPO_DocStatus
            sdpDoc = DMSDocumentLinkHelper.GetDocumentLink(sdpID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.SHPO_DocStatus.ToString()));
            SetLinkDocument(sdpID, DMSDocumentLinkHelper.eDocumentField.SHPO_DocStatus, sdpDoc);

            //GPS1a2c_DocStatus
            sdpDoc = DMSDocumentLinkHelper.GetDocumentLink(sdpID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.GPS1a2c_DocStatus.ToString()));
            SetLinkDocument(sdpID, DMSDocumentLinkHelper.eDocumentField.GPS1a2c_DocStatus, sdpDoc);

            //Airspace_DocStatus
            sdpDoc = DMSDocumentLinkHelper.GetDocumentLink(sdpID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.Airspace_DocStatus.ToString()));
            SetLinkDocument(sdpID, DMSDocumentLinkHelper.eDocumentField.Airspace_DocStatus, sdpDoc);

            //FAAStudyDet_DocStatus
            sdpDoc = DMSDocumentLinkHelper.GetDocumentLink(sdpID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.FAAStudyDet_DocStatus.ToString()));
            SetLinkDocument(sdpID, DMSDocumentLinkHelper.eDocumentField.FAAStudyDet_DocStatus, sdpDoc);

            //FCC_DocStatus
            sdpDoc = DMSDocumentLinkHelper.GetDocumentLink(sdpID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.FCC_DocStatus.ToString()));
            SetLinkDocument(sdpID, DMSDocumentLinkHelper.eDocumentField.FCC_DocStatus, sdpDoc);

            //AMCertification_DocStatus
            sdpDoc = DMSDocumentLinkHelper.GetDocumentLink(sdpID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.AMCertification_DocStatus.ToString()));
            SetLinkDocument(sdpID, DMSDocumentLinkHelper.eDocumentField.AMCertification_DocStatus, sdpDoc);

            //Towair_DocStatus
            sdpDoc = DMSDocumentLinkHelper.GetDocumentLink(sdpID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.Towair_DocStatus.ToString()));
            SetLinkDocument(sdpID, DMSDocumentLinkHelper.eDocumentField.Towair_DocStatus, sdpDoc);

            //PrimeLease_DocStatus
            sdpDoc = DMSDocumentLinkHelper.GetDocumentLink(sdpID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.PrimeLease_DocStatus.ToString()));
            SetLinkDocument(sdpID, DMSDocumentLinkHelper.eDocumentField.PrimeLease_DocStatus, sdpDoc);

            //Memorandum_DocStatus
            sdpDoc = DMSDocumentLinkHelper.GetDocumentLink(sdpID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.Memorandum_DocStatus.ToString()));
            SetLinkDocument(sdpID, DMSDocumentLinkHelper.eDocumentField.Memorandum_DocStatus, sdpDoc);

            //Title_DocStatus
            sdpDoc = DMSDocumentLinkHelper.GetDocumentLink(sdpID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.Title_DocStatus.ToString()));
            SetLinkDocument(sdpID, DMSDocumentLinkHelper.eDocumentField.Title_DocStatus, sdpDoc);

            //Abstract
            //AbstractCompleted
            sdpDoc = DMSDocumentLinkHelper.GetDocumentLink(sdpID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.AbstractCompleted.ToString()));
            SetLinkDocument(sdpID, DMSDocumentLinkHelper.eDocumentField.AbstractCompleted, sdpDoc);
        }

        private void SetLinkDocument(int ID, DMSDocumentLinkHelper.eDocumentField field, List<Dms_Document> docData)
        {
            //insert template data in case no documents link
            if (docData.Count == 0)
            {
                Dms_Document doc = new Dms_Document();
                doc.ID = ID;
                doc.ref_field = field.ToString();
                docData.Add(doc);
            }

            switch (field)
            {
                //General
                case DMSDocumentLinkHelper.eDocumentField.SDPCheckList:
                    this.hidden_document_SDPCheckList.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
                case DMSDocumentLinkHelper.eDocumentField.AdditionalPrimeDocuments:
                    this.hidden_document_AdditionalPrimeDocuments.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
                case DMSDocumentLinkHelper.eDocumentField.SDPFirstNoticeToMarket:
                    this.hidden_document_SDPFirstNoticeToMarket.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
                case DMSDocumentLinkHelper.eDocumentField.SDPSecondNoticeToMarket:
                    this.hidden_document_SDPSecondNoticeToMarket.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
                case DMSDocumentLinkHelper.eDocumentField.SDPThirdNoticeToMarket:
                    this.hidden_document_SDPThirdNoticeToMarket.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;

                //Document Status
                case DMSDocumentLinkHelper.eDocumentField.TowerDrawing_DocTypes:
                    this.hidden_document_TowerDrawing_DocTypes.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
                case  DMSDocumentLinkHelper.eDocumentField.TowerDrawing_DocStatus :
                    this.hidden_document_TowerDrawing_DocStatus.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
                case DMSDocumentLinkHelper.eDocumentField.TowerTagPhoto_DocStatus:
                    this.hidden_document_TowerTagPhoto_DocStatus.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
                case DMSDocumentLinkHelper.eDocumentField.StructuralCalcs_DocStatus:
                    this.hidden_document_StructuralCalcs_DocStatus.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
                case DMSDocumentLinkHelper.eDocumentField.StructuralAnalysis_DocStatus:
                    this.hidden_document_StructuralAnalysis_DocStatus.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
                case DMSDocumentLinkHelper.eDocumentField.TowerErectionDetail_DocStatus:
                    this.hidden_document_TowerErectionDetail_DocStatus.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
                case DMSDocumentLinkHelper.eDocumentField.FoundationDesign_DocStatus:
                    this.hidden_document_FoundationDesign_DocStatus.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
                case DMSDocumentLinkHelper.eDocumentField.ConstructionDrawings_DocStatus:
                    this.hidden_document_ConstructionDrawings_DocStatus.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
                case DMSDocumentLinkHelper.eDocumentField.GeotechReport_DocStatus:
                    this.hidden_document_GeotechReport_DocStatus.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
                case DMSDocumentLinkHelper.eDocumentField.BuildingPermit_DocStatus:
                    this.hidden_document_BuildingPermit_DocStatus.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;

                case DMSDocumentLinkHelper.eDocumentField.ZoningApproval_DocStatus:
                    this.hidden_document_ZoningApproval_DocStatus.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
                case DMSDocumentLinkHelper.eDocumentField.PhaseI_DocStatus:
                    this.hidden_document_PhaseI_DocStatus.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
                case DMSDocumentLinkHelper.eDocumentField.PhaseII_DocStatus:
                    this.hidden_document_PhaseII_DocStatus.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
                case DMSDocumentLinkHelper.eDocumentField.NEPA_DocStatus:
                    this.hidden_document_NEPA_DocStatus.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
                case DMSDocumentLinkHelper.eDocumentField.SHPO_DocStatus:
                    this.hidden_document_SHPO_DocStatus.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
                case DMSDocumentLinkHelper.eDocumentField.GPS1a2c_DocStatus:
                    this.hidden_document_GPS1a2c_DocStatus.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
                case DMSDocumentLinkHelper.eDocumentField.Airspace_DocStatus:
                    this.hidden_document_Airspace_DocStatus.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
                case DMSDocumentLinkHelper.eDocumentField.FAAStudyDet_DocStatus:
                    this.hidden_document_FAAStudyDet_DocStatus.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
                case DMSDocumentLinkHelper.eDocumentField.FCC_DocStatus:
                    this.hidden_document_FCC_DocStatus.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
                case DMSDocumentLinkHelper.eDocumentField.AMCertification_DocStatus:
                    this.hidden_document_AMCertification_DocStatus.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
                case DMSDocumentLinkHelper.eDocumentField.Towair_DocStatus:
                    this.hidden_document_Towair_DocStatus.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
                case DMSDocumentLinkHelper.eDocumentField.PrimeLease_DocStatus:
                    this.hidden_document_PrimeLease_DocStatus.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
                case DMSDocumentLinkHelper.eDocumentField.Memorandum_DocStatus:
                    this.hidden_document_Memorandum_DocStatus.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
                case DMSDocumentLinkHelper.eDocumentField.Title_DocStatus:
                    this.hidden_document_Title_DocStatus.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
                //Abstract
                //AbstractCompleted
                case DMSDocumentLinkHelper.eDocumentField.AbstractCompleted:
                    this.hidden_document_AbstractCompleted.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
                default:
                    // do the defalut action
                    break;
                //TODO: big check warrning msg later.
            }
        }

        protected void SaveSDPLinkDocument(int sdpID)
        {
            List<Dms_Document> jsonDocData = new List<Dms_Document>();

            #region General
            //SDPCheckList
            if (!string.IsNullOrEmpty(this.hidden_document_SDPCheckList.Value))
            {
                String SDPCheckList = hidden_document_SDPCheckList.Value;
                jsonDocData.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(SDPCheckList));
            }

            //AdditionalPrimeDocuments
            if (!string.IsNullOrEmpty(this.hidden_document_AdditionalPrimeDocuments.Value))
            {
                String AdditionalPrimeDocuments = hidden_document_AdditionalPrimeDocuments.Value;
                jsonDocData.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(AdditionalPrimeDocuments));
            }

            //SDPFirstNoticeToMarket
            if (!string.IsNullOrEmpty(this.hidden_document_SDPFirstNoticeToMarket.Value))
            {
                String SDPFirstNoticeToMarket = hidden_document_SDPFirstNoticeToMarket.Value;
                jsonDocData.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(SDPFirstNoticeToMarket));
            }

            //SDPSecondNoticeToMarket
            if (!string.IsNullOrEmpty(this.hidden_document_SDPSecondNoticeToMarket.Value))
            {
                String sdpJson = hidden_document_SDPSecondNoticeToMarket.Value;
                jsonDocData.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(sdpJson));
            }
            
            //SDPThirdNoticeToMarket
            if (!string.IsNullOrEmpty(this.hidden_document_SDPThirdNoticeToMarket.Value))
            {
                String sdpJson = hidden_document_SDPThirdNoticeToMarket.Value;
                jsonDocData.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(sdpJson));
            }
            #endregion General

            #region Document Status
            //TowerDrawing_DocTypes
            if (!string.IsNullOrEmpty(this.hidden_document_TowerDrawing_DocTypes.Value))
            {
                String sdpJson = hidden_document_TowerDrawing_DocTypes.Value;
                jsonDocData.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(sdpJson));
            }

            //TowerDrawing_DocStatus
            if (!string.IsNullOrEmpty(this.hidden_document_TowerDrawing_DocStatus.Value))
            {
                String sdpJson = hidden_document_TowerDrawing_DocStatus.Value;
                jsonDocData.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(sdpJson));
            }

            //TowerTagPhoto_DocStatus
            if (!string.IsNullOrEmpty(this.hidden_document_TowerTagPhoto_DocStatus.Value))
            {
                String sdpJson = hidden_document_TowerTagPhoto_DocStatus.Value;
                jsonDocData.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(sdpJson));
            }

            //StructuralCalcs_DocStatus
            if (!string.IsNullOrEmpty(this.hidden_document_StructuralCalcs_DocStatus.Value))
            {
                String sdpJson = hidden_document_StructuralCalcs_DocStatus.Value;
                jsonDocData.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(sdpJson));
            }

            //StructuralAnalysis_DocStatus
            if (!string.IsNullOrEmpty(this.hidden_document_StructuralAnalysis_DocStatus.Value))
            {
                String sdpJson = hidden_document_StructuralAnalysis_DocStatus.Value;
                jsonDocData.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(sdpJson));
            }

            //TowerErectionDetail_DocStatus
            if (!string.IsNullOrEmpty(this.hidden_document_TowerErectionDetail_DocStatus.Value))
            {
                String sdpJson = hidden_document_TowerErectionDetail_DocStatus.Value;
                jsonDocData.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(sdpJson));
            }

            //FoundationDesign_DocStatus
            if (!string.IsNullOrEmpty(this.hidden_document_FoundationDesign_DocStatus.Value))
            {
                String sdpJson = hidden_document_FoundationDesign_DocStatus.Value;
                jsonDocData.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(sdpJson));
            }

            //ConstructionDrawings_DocStatus
            if (!string.IsNullOrEmpty(this.hidden_document_ConstructionDrawings_DocStatus.Value))
            {
                String sdpJson = hidden_document_ConstructionDrawings_DocStatus.Value;
                jsonDocData.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(sdpJson));
            }

            //GeotechReport_DocStatus
            if (!string.IsNullOrEmpty(this.hidden_document_GeotechReport_DocStatus.Value))
            {
                String sdpJson = hidden_document_GeotechReport_DocStatus.Value;
                jsonDocData.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(sdpJson));
            }

            //BuildingPermit_DocStatus
            if (!string.IsNullOrEmpty(this.hidden_document_BuildingPermit_DocStatus.Value))
            {
                String sdpJson = hidden_document_BuildingPermit_DocStatus.Value;
                jsonDocData.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(sdpJson));
            }

            //ZoningApproval_DocStatus
            if (!string.IsNullOrEmpty(this.hidden_document_ZoningApproval_DocStatus.Value))
            {
                String sdpJson = hidden_document_ZoningApproval_DocStatus.Value;
                jsonDocData.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(sdpJson));
            }

            //PhaseI_DocStatus
            if (!string.IsNullOrEmpty(this.hidden_document_PhaseI_DocStatus.Value))
            {
                String sdpJson = hidden_document_PhaseI_DocStatus.Value;
                jsonDocData.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(sdpJson));
            }

            //PhaseII_DocStatus
            if (!string.IsNullOrEmpty(this.hidden_document_PhaseII_DocStatus.Value))
            {
                String sdpJson = hidden_document_PhaseII_DocStatus.Value;
                jsonDocData.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(sdpJson));
            }

            //NEPA_DocStatus
            if (!string.IsNullOrEmpty(this.hidden_document_NEPA_DocStatus.Value))
            {
                String sdpJson = hidden_document_NEPA_DocStatus.Value;
                jsonDocData.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(sdpJson));
            }

            //SHPO_DocStatus
            if (!string.IsNullOrEmpty(this.hidden_document_SHPO_DocStatus.Value))
            {
                String sdpJson = hidden_document_SHPO_DocStatus.Value;
                jsonDocData.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(sdpJson));
            }

            //GPS1a2c_DocStatus
            if (!string.IsNullOrEmpty(this.hidden_document_GPS1a2c_DocStatus.Value))
            {
                String sdpJson = hidden_document_GPS1a2c_DocStatus.Value;
                jsonDocData.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(sdpJson));
            }

            //Airspace_DocStatus
            if (!string.IsNullOrEmpty(this.hidden_document_Airspace_DocStatus.Value))
            {
                String sdpJson = hidden_document_Airspace_DocStatus.Value;
                jsonDocData.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(sdpJson));
            }

            //FAAStudyDet_DocStatus
            if (!string.IsNullOrEmpty(this.hidden_document_FAAStudyDet_DocStatus.Value))
            {
                String sdpJson = hidden_document_FAAStudyDet_DocStatus.Value;
                jsonDocData.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(sdpJson));
            }

            //FCC_DocStatus
            if (!string.IsNullOrEmpty(this.hidden_document_FCC_DocStatus.Value))
            {
                String sdpJson = hidden_document_FCC_DocStatus.Value;
                jsonDocData.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(sdpJson));
            }

            //AMCertification_DocStatus
            if (!string.IsNullOrEmpty(this.hidden_document_AMCertification_DocStatus.Value))
            {
                String sdpJson = hidden_document_AMCertification_DocStatus.Value;
                jsonDocData.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(sdpJson));
            }

            //Towair_DocStatus
            if (!string.IsNullOrEmpty(this.hidden_document_Towair_DocStatus.Value))
            {
                String sdpJson = hidden_document_Towair_DocStatus.Value;
                jsonDocData.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(sdpJson));
            }

            //PrimeLease_DocStatus
            if (!string.IsNullOrEmpty(this.hidden_document_PrimeLease_DocStatus.Value))
            {
                String sdpJson = hidden_document_PrimeLease_DocStatus.Value;
                jsonDocData.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(sdpJson));
            }

            //Memorandum_DocStatus
            if (!string.IsNullOrEmpty(this.hidden_document_Memorandum_DocStatus.Value))
            {
                String sdpJson = hidden_document_Memorandum_DocStatus.Value;
                jsonDocData.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(sdpJson));
            }

            //Title_DocStatus
            if (!string.IsNullOrEmpty(this.hidden_document_Title_DocStatus.Value))
            {
                String sdpJson = hidden_document_Title_DocStatus.Value;
                jsonDocData.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(sdpJson));
            }

            #endregion Document Status

            #region Abstract
            //AbstractCompleted
            if (!string.IsNullOrEmpty(this.hidden_document_AbstractCompleted.Value))
            {
                String sdpJson = hidden_document_AbstractCompleted.Value;
                jsonDocData.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(sdpJson));
            }

            #endregion Abstract

            //Insert link document to DB
            foreach (Dms_Document doc in jsonDocData)
            {
                //Check if not template data will insert to DB
                if ((doc.ref_field != "") && (doc.ref_key != ""))
                {
                    List<Dms_Document> tmpList = DMSDocumentLinkHelper.GetDocumentLink(doc.ID, DMSDocumentLinkHelper.DocumentField(doc.ref_field), doc.ref_key);
                    if (tmpList.Count == 0)
                    {
                        string refkey = Server.UrlDecode(doc.ref_key);
                        DMSDocumentLinkHelper.AddDocumentLink(sdpID, sdpID, DMSDocumentLinkHelper.DocumentField(doc.ref_field), Server.UrlEncode(doc.ref_key), true);
                        DocumentsChangeLog.AddLog(lblSiteID.Text, doc.documentName, doc.ref_key, DocumentsChangeLog.eDocumentLogAction.Upload, Request, true);
                    }
                    //else { Already in db do nothing. }
                }
            }
        }
        #endregion

        private void GetSessionFilter()
        {
            // Check session filter
            DefaultFilterSession filterSession = new DefaultFilterSession();

            if (HttpContext.Current.Session != null && HttpContext.Current.Session[DefaultFilterSession.SessionName] != null)
            {
                filterSession = (DefaultFilterSession)HttpContext.Current.Session[DefaultFilterSession.SessionName];

                string hiddenFilter = DefaultFilterSession.GetFilterSession(filterSession, DefaultFilterSession.FilterType.SdpFilter);
                backToList.NavigateUrl += "?filter=" + hiddenFilter;
            }
        }

    }
}
