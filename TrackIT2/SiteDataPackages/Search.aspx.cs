﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.BLL;

namespace TrackIT2.SiteDataPackages
{
    public partial class Search : System.Web.UI.Page
    {
        private AdvanceSearch.SiteDatePackageFilter objADVSearch = new AdvanceSearch.SiteDatePackageFilter();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["result"] != null && Request["result"] != "")
                {
                    Response.Clear();
                    string ret = AdvanceSearch.GetDDLDBJSON(objADVSearch, Request["result"]);
                    if (ret != "")
                    {
                        Response.ContentType = "text/plain";
                        Response.Write(ret);
                        Response.End();
                    }
                }
            }
            else
            {
                //clears the response written into the buffer and end the response.
                Response.Clear();
                Response.End();
            }
        }
    }
}