﻿using System;
using System.Web;
using TrackIT2.BLL;
using TrackIT2.DAL;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Linq;

namespace TrackIT2.Handlers
{
    public class SiteIDHandler : IHttpHandler
    {
        #region IHttpHandler Members

        /// <summary>
        /// IsReusable
        /// </summary>
        public bool IsReusable
        {
            get { return true; }
        }
        //>

        /// <summary>
        /// ProcessRequest
        /// </summary>
        /// <param name="context"></param>
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                CPTTEntities ce = new CPTTEntities();
                string[] siteID = {};
                
                List<string> result = new List<string>();

                if (context.Request.Form["term"] != null)
                {
                    siteID = context.Request.Form["term"].Split('|'); ;
                }

                foreach (string s in siteID)
                {
                    if (!string.IsNullOrEmpty(s))
                    {
                        string site = (from data in ce.sites
                                   where data.site_uid.Equals(s)
                                   select data.site_uid).FirstOrDefault();

                        if (string.IsNullOrEmpty(site))
                        {
                            result.Add(s);
                        }
                    }
                }

                //// Convert Result

                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                string ids = javaScriptSerializer.Serialize(result);
                context.Response.ContentType = "text/html";
                context.Response.Write(ids);
            }
            catch
            {
                context.Response.Write("");
            }
        }
        //--

        #endregion
    }
}