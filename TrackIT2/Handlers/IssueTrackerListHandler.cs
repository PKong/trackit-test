﻿using System;
using System.Data.Objects;
using System.Globalization;
using System.Web;
using System.Collections.Generic;
using FlexigridASPNET;
using TrackIT2.BLL;
using TrackIT2.DAL;
using System.Linq;
using System.Collections.Specialized;
using System.Text;

namespace TrackIT2.Handlers
{
    #region Main Handler

    /// <summary>
    /// IssueTrackerListHandler - returns a list of Issue Tracker items for binding to a Flexigrid
    /// </summary>
    public class IssueTrackerListHandler : BaseFlexiHandler
    {
        // Var
        public CPTTEntities Ce = new CPTTEntities();
        private static bool _subFilterHasNoResults;

        /// <summary>
        /// Gets the filtered data.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<KeyValuePair<string, object[]>> GetFilteredData()
        {
           // Variables
           var result = new List<KeyValuePair<string, object[]>>();

           const int startIndex = 6;
           const int summaryLengthToDisplay = 200;
           
           List<issue_tracker> issues;
           var ce = new CPTTEntities();
           var skipItems = (Page - 1) * ItemsPerPage;
           var form = HttpContext.Current.Request.Form;
           var parameters = new NameValueCollection();
           DefaultFilterSession filterSession = new DefaultFilterSession();
           string filterString = string.Empty;

           // Set sort name if it exists.
           var sortName = !String.IsNullOrWhiteSpace(this.SortName) ?
                          this.SortName : "id";

           // If there are form values, extract them to build the object query.
           if (form.Count > startIndex)
           {
              for (var i = startIndex; i < form.Count; i++)
              {
                 var itemKey = form.GetKey(i);
                 var itemValue = form.Get(i);

                 // If the search came from the global search bar, a generic 
                 // "gs" item key is specified and needs to be changed.
                 if (itemKey == "gs")
                 {
                     itemKey = "General";
                 }

                 parameters.Add(itemKey, itemValue);

                 if (itemKey == DefaultFilterSession.SessionName)
                 {
                     filterString = itemValue;
                 }
              }
           }

           // Check session filter
           parameters.Remove(DefaultFilterSession.ApplyClick);
           parameters.Remove(DefaultFilterSession.SessionName);
           DefaultFilterSession.GetOrSetFilterSession(HttpContext.Current, ref filterSession, filterString, DefaultFilterSession.FilterType.IssueFilter);

           // Build ObjectQuery
           var searchCriteria = BLL.IssueTracker.GetSearchCriteria();
           ObjectQuery<issue_tracker> issueTrackerQuery = ce.issue_tracker;
           issueTrackerQuery = Utility.BuildObjectQuery(issueTrackerQuery, parameters, searchCriteria);

           VitualItemsCount = issueTrackerQuery.Count();

           // Sort Order
           if (VitualItemsCount > 0)
           {
              if (this.SortDirection == GridSortOrder.Asc)
              {
                 var a = Utility.OrderBy(issueTrackerQuery, sortName);
                 issues = a.Skip(skipItems).Take(this.ItemsPerPage).ToList();
              }
              else
              {
                 var b = Utility.OrderByDescending(issueTrackerQuery, sortName);
                 issues = b.Skip(skipItems).Take(this.ItemsPerPage).ToList();
              }
              //>           

              // Do final massaging of results before returning it to the grid.
              foreach (var issue in issues)
              {
                 var siteSiteUid = "";
                 var siteName = "";
                 var issueSummary = "";
                 var issuePriority = "";
                 var issueStatus = "";

                 if (issue.site != null)
                 {
                    siteSiteUid = issue.site.site_uid;
                    siteName = issue.site.site_name;
                 }

                 if (!String.IsNullOrEmpty(issue.issue_summary))
                 {
                    if (issue.issue_summary.Length > summaryLengthToDisplay)
                    {
                       issueSummary = issue.issue_summary.Substring(0, summaryLengthToDisplay);
                       issueSummary = issueSummary.Substring(0, issueSummary.LastIndexOf(" ", StringComparison.Ordinal)) + "...";
                    }
                    else
                    {
                       issueSummary = issue.issue_summary;
                    }
                 }

                 if (issue.issue_priority_types != null)
                 {
                    if (issue.issue_priority_types.issue_priority_number != null)
                    {
                       issuePriority = issue.issue_priority_types.issue_priority_number.Value.ToString();
                    }

                 }

                 if (issue.issue_statuses != null)
                 {
                    issueStatus = issue.issue_statuses.issue_status;
                 }

                 // Build Result
                 result.Add(new KeyValuePair<string, object[]>
                                (issue.id.ToString(CultureInfo.InvariantCulture),
                                 new object[] 
                                 { 
                                   issue.id.ToString(CultureInfo.InvariantCulture), 
                                   siteSiteUid,
                                   siteName, 
                                   issueSummary, 
                                   issuePriority,
                                   Utility.DateToString(issue.created_at), 
                                   issueStatus, 
                                   issue.id.ToString(CultureInfo.InvariantCulture) 
                                 }
                                )
                           );
              }
           }           

           return result;
        }
        //-->        
    }
    #endregion
}
