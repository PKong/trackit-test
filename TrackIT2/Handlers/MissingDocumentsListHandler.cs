﻿using System;
using System.Web;
using System.Collections.Generic;
using FlexigridASPNET;
using TrackIT2.BLL;
using TrackIT2.DAL;
using System.Linq;
using System.Collections.Specialized;
using System.Text;
using System.Configuration;

namespace TrackIT2.Handlers
{
    public class MissingDocumentsListHandler : BaseFlexiHandler
    {
        public override IEnumerable<KeyValuePair<string, object[]>> GetFilteredData()
        {
            CPTTEntities ce = new CPTTEntities();
            IQueryable<DmsLinkDocumentStructure> iqApplications;
            List<DmsLinkDocumentStructure> returnObj = new List<DmsLinkDocumentStructure>();

            int skipItems = (Page - 1) * this.ItemsPerPage;
            string sortName = !String.IsNullOrWhiteSpace(this.SortName) ? this.SortName : "Update_AT";
            string globalSearch = null;
            GridSortOrder? customSorting = null;
            // Search Filter Values
            NameValueCollection form = HttpContext.Current.Request.Form;
            int startIndex = 6;
            StringBuilder sb = new StringBuilder();
            //
            // Get Filters
            if (form.Count > startIndex)
            {
                for (int i = startIndex; i < form.Count; i++)
                {
                    // Check NOT Global Search param
                    if (form.GetKey(i) == Globals.QS_PROJECT_CATEGORY)
                    {
                        //Use for backward compatibility.
                        //Do nothing. 
                    }
                    else if (form.GetKey(i) == Globals.QS_GLOBAL_SEARCH)
                    {
                        // Global Search - get val
                        //globalSearch = form.Get(i);
                        globalSearch = Utility.PrepareSqlSearchString(form.Get(i));
                    }
                    else if (form.GetKey(i) == Globals.QS_SORT_DIRECTION)
                    {
                        //Using custom sort order from widget.
                        if (form.Get(i) == Globals.SORT_DIRECTION_VALUE_ASC)
                        {
                            customSorting = GridSortOrder.Asc;
                        }
                        else
                        {
                            customSorting = GridSortOrder.Desc;
                        }
                    }
                }
            }
            //>
            
            // Get Working Set
            var listDoc = DMSDocumentLinkHelper.widgetList;

                    switch (form["type"].ToString())
                    {
                        #region TowerMod
                        case "TowerModificationMissingDocument":

                            returnObj = GetTowerMod(listDoc);
                            
                            break;
                        #endregion TowerMod
                        #region LeaseApp

                        case "LeaseApplicationsMissingDocument":

                            returnObj = GetLeaseApp(listDoc);
                            
                            break;
                        #endregion LeaseApp
                        #region SDP

                        case "SiteDataPackageMissingDocument":
                           
                            returnObj = GetSdp(listDoc);
                            
                            break;
                        #endregion SDP
                        #region all
                        default:
                            returnObj.AddRange(GetLeaseApp(listDoc));
                            returnObj.AddRange(GetTowerMod(listDoc));
                            returnObj.AddRange(GetSdp(listDoc));
                            break;
                        #endregion all
                    }

            //>

            IEnumerable<DmsLinkDocumentStructure> noduplicates = returnObj.Distinct();

            // Items Count
            this.VitualItemsCount = returnObj.Count();

            // Sort Order
                var b = Utility.OrderByDescending(noduplicates.AsQueryable().Distinct(), sortName);
                iqApplications = b.Skip(skipItems).Take(this.ItemsPerPage);

                List<KeyValuePair<string, object[]>> result = new List<KeyValuePair<string, object[]>>();
            foreach (var application in iqApplications)
            {
                result.Add(new KeyValuePair<string, object[]>(application.Field + "_" + application.LabelType + "_" + application.siteID + "_" + application.RecordId, new object[] { application.siteID, application.LabelType, application.RecordId, application.LabelField, application.Update_AT }));

            }

            return result;
        }

        public List<DmsLinkDocumentStructure> GetTowerMod(DmsWidgetDocument[] listDoc)
        {
            List<DmsLinkDocumentStructure> returnObj = new List<DmsLinkDocumentStructure>();
            using (CPTTEntities ce = new CPTTEntities())
            {
               var cutoffDate = DateTime.Parse(ConfigurationManager.AppSettings["MissingDocumentCutoffDate"]); 
               var towerModRecord = ce.tower_modifications
                                      .Where(x => x.building_permit_rcvd_date.HasValue
                                                  && x.building_permit_rcvd_date >= cutoffDate
                                                  && x.building_permit_rcvd_date_status_id.HasValue 
                                                  && x.building_permit_rcvd_date_status_id.Value.Equals(1));

                foreach (DmsWidgetDocument obj in listDoc.Where(x => x.document.TypeCode == (int) DMSDocumentLinkHelper.eDocumentType.tower_modification_general
                                                                     || x.document.TypeCode == (int) DMSDocumentLinkHelper.eDocumentType.tower_modification))
                {
                    string tmpField = obj.document.Field.ToString();
                    var towerDocRecord = from towerDoc in ce.documents_links
                                         where towerDoc.linked_field == tmpField
                                         select towerDoc;

                    var towerMod = from mod in towerModRecord
                                   where !(from doc in towerDocRecord
                                           select doc.record_id).Contains(mod.id)
                                   select new DmsLinkDocumentStructure
                                   {
                                       siteID = mod.site_uid,
                                       TypeCode = (int) obj.document.Type,
                                       RecordId = mod.id,
                                       FieldCode = (int) obj.document.Field,
                                       Update_AT = mod.updated_at
                                   };
                    returnObj.AddRange(towerMod.ToList());
                }
                return returnObj;
            }
        }

        public List<DmsLinkDocumentStructure> GetLeaseApp(DmsWidgetDocument[] listDoc)
        {            
            List<DmsLinkDocumentStructure> returnObj = new List<DmsLinkDocumentStructure>();
            var cutoffDate = DateTime.Parse(ConfigurationManager.AppSettings["MissingDocumentCutoffDate"]);

            using (CPTTEntities ce = new CPTTEntities())
            {
                var leaseAppRecord = from leaseApp in ce.lease_applications
                                     where leaseApp.updated_at >= cutoffDate
                                     select leaseApp;               

                //Process Dates Received
                var saRecord = ce.structural_analyses.
                                  Where(x => x.sa_received_date.HasValue
                                             && x.sa_received_date >= cutoffDate
                                             && x.sa_received_date_status_id.HasValue
                                             && x.sa_received_date_status_id.Value.Equals(1));

                foreach (DmsWidgetDocument obj in listDoc.Where(x => x.document.TypeCode == (int) DMSDocumentLinkHelper.eDocumentType.lease_applications
                    || x.document.TypeCode == (int) DMSDocumentLinkHelper.eDocumentType.lease_admin
                    || x.document.TypeCode == (int) DMSDocumentLinkHelper.eDocumentType.structural_analyses))
                {
                    string tmpField = obj.document.Field.ToString();
                    var leaseDocRecord = from towerDoc in ce.documents_links
                                         where towerDoc.linked_field == tmpField
                                         select towerDoc;

                    IQueryable<DmsLinkDocumentStructure> leaseApp;

                    switch (obj.document.Field)
                    {
                        case DMSDocumentLinkHelper.eDocumentField.PreliminaryDecision:
                            //Preliminary Decision
                            leaseApp = from lease in leaseAppRecord
                                       where !(from doc in leaseDocRecord
                                               select doc.record_id).Contains(lease.id)
                                               && lease.prelim_approval_date.HasValue && lease.prelim_approval_date >= cutoffDate
                                               && lease.prelim_decision_id.HasValue && lease.prelim_decision_id.Value.Equals(1)
                                       select new DmsLinkDocumentStructure
                                       {
                                           siteID = lease.site_uid,
                                           TypeCode = (int)obj.document.Type,
                                           RecordId = lease.id,
                                           FieldCode = (int)obj.document.Field,
                                           Update_AT = lease.updated_at
                                       };
                            obj.requireFiled = true;
                            returnObj.AddRange(leaseApp.Distinct().ToList());
                            break;

                        case DMSDocumentLinkHelper.eDocumentField.PreliminarySitewalk:
                            //Preliminary Sitewalk
                            leaseApp = from lease in leaseAppRecord
                                       where !(from doc in leaseDocRecord
                                               select doc.record_id).Contains(lease.id)
                                               && lease.prelim_sitewalk_date.HasValue && lease.prelim_sitewalk_date >= cutoffDate
                                               && lease.prelim_sitewalk_date_status_id.HasValue && lease.prelim_sitewalk_date_status_id.Value.Equals(1)
                                       select new DmsLinkDocumentStructure
                                       {
                                           siteID = lease.site_uid,
                                           TypeCode = (int) obj.document.Type,
                                           RecordId = lease.id,
                                           FieldCode = (int) obj.document.Field,
                                           Update_AT = lease.updated_at
                                       };
                            obj.requireFiled = true;
                            returnObj.AddRange(leaseApp.Distinct().ToList());
                            break;

                        case DMSDocumentLinkHelper.eDocumentField.ExecutablesSenttoCustomer:
                            //Executables Sent to Customer
                            leaseApp = from lease in leaseAppRecord
                                       where !(from doc in leaseDocRecord
                                               select doc.record_id).Contains(lease.id)
                                               && lease.leasedoc_sent_to_cust_date.HasValue && lease.leasedoc_sent_to_cust_date >= cutoffDate
                                               && lease.leasedoc_sent_to_cust_date_status_id.HasValue && lease.leasedoc_sent_to_cust_date_status_id.Value.Equals(1)
                                       select new DmsLinkDocumentStructure
                                       {
                                           siteID = lease.site_uid,
                                           TypeCode = (int) obj.document.Type,
                                           RecordId = lease.id,
                                           FieldCode = (int) obj.document.Field,
                                           Update_AT = lease.updated_at
                                       };

                            returnObj.AddRange(leaseApp.Distinct().ToList());
                            break;

                        case DMSDocumentLinkHelper.eDocumentField.ExecutablesToCrown:
                            //Executables to Crown
                            leaseApp = from lease in leaseAppRecord
                                       where !(from doc in leaseDocRecord
                                               select doc.record_id).Contains(lease.id)
                                               && lease.executables_to_crown_date.HasValue && lease.executables_to_crown_date >= cutoffDate
                                               && lease.executables_to_crown_date_status_id.HasValue && lease.executables_to_crown_date_status_id.Value.Equals(1)
                                       select new DmsLinkDocumentStructure
                                       {
                                           siteID = lease.site_uid,
                                           TypeCode = (int) obj.document.Type,
                                           RecordId = lease.id,
                                           FieldCode = (int) obj.document.Field,
                                           Update_AT = lease.updated_at
                                       };

                            returnObj.AddRange(leaseApp.Distinct().ToList());
                            break;

                        case DMSDocumentLinkHelper.eDocumentField.LeaseExecution:
                            //Lease Execution
                            leaseApp = from lease in leaseAppRecord
                                       where !(from doc in leaseDocRecord
                                               select doc.record_id).Contains(lease.id)
                                               && lease.lease_execution_date.HasValue && lease.lease_execution_date >= cutoffDate
                                               && lease.lease_execution_date_status_id.HasValue && lease.lease_execution_date_status_id.Value.Equals(1)
                                       select new DmsLinkDocumentStructure
                                       {
                                           siteID = lease.site_uid,
                                           TypeCode = (int) obj.document.Type,
                                           RecordId = lease.id,
                                           FieldCode = (int) obj.document.Field,
                                           Update_AT = lease.updated_at
                                       };

                            returnObj.AddRange(leaseApp.Distinct().ToList());
                            break;

                        case DMSDocumentLinkHelper.eDocumentField.Preconstruction_PreconstructionSitewalk:
                            //Preconstruction Sitewalk
                            leaseApp = from lease in leaseAppRecord
                                       where !(from doc in leaseDocRecord
                                               select doc.record_id).Contains(lease.id)
                                               && lease.preconstr_sitewalk_date.HasValue && lease.preconstr_sitewalk_date >= cutoffDate
                                               && lease.preconstr_sitewalk_date_status_id.HasValue && lease.preconstr_sitewalk_date_status_id.Value.Equals(1)
                                       select new DmsLinkDocumentStructure
                                       {
                                           siteID = lease.site_uid,
                                           TypeCode = (int) obj.document.Type,
                                           RecordId = lease.id,
                                           FieldCode = (int) obj.document.Field,
                                           Update_AT = lease.updated_at
                                       };

                            returnObj.AddRange(leaseApp.Distinct().ToList());
                            break;

                        case DMSDocumentLinkHelper.eDocumentField.NTPIssuedDateStatus:
                            //NTP Issued Date
                            leaseApp = from lease in leaseAppRecord
                                       where !(from doc in leaseDocRecord
                                               select doc.record_id).Contains(lease.id)
                                               && lease.ntp_issued_date.HasValue && lease.ntp_issued_date >= cutoffDate
                                               && lease.ntp_issued_date_status_id.HasValue && lease.ntp_issued_date_status_id.Value.Equals(1)
                                       select new DmsLinkDocumentStructure
                                       {
                                           siteID = lease.site_uid,
                                           TypeCode = (int) obj.document.Type,
                                           RecordId = lease.id,
                                           FieldCode = (int) obj.document.Field,
                                           Update_AT = lease.updated_at
                                       };

                            returnObj.AddRange(leaseApp.Distinct().ToList());
                            break;

                        case DMSDocumentLinkHelper.eDocumentField.PostConstruction_Sitewalk:
                            //Sitewalk
                            leaseApp = from lease in leaseAppRecord
                                       where !(from doc in leaseDocRecord
                                               select doc.record_id).Contains(lease.id)
                                               && lease.postconstr_sitewalk_date.HasValue && lease.postconstr_sitewalk_date >= cutoffDate
                                               && lease.postconstr_sitewalk_date_status_id.HasValue && lease.postconstr_sitewalk_date_status_id.Value.Equals(1)
                                       select new DmsLinkDocumentStructure
                                       {
                                           siteID = lease.site_uid,
                                           TypeCode = (int) obj.document.Type,
                                           RecordId = lease.id,
                                           FieldCode = (int) obj.document.Field,
                                           Update_AT = lease.updated_at
                                       };
                            returnObj.AddRange(leaseApp.Distinct().ToList());
                            break;

                        case DMSDocumentLinkHelper.eDocumentField.CloseOut_FinalSiteApproval:
                            //Final Site Approval
                            leaseApp = from lease in leaseAppRecord
                                       where !(from doc in leaseDocRecord
                                               select doc.record_id).Contains(lease.id)
                                               && lease.final_site_apprvl_date.HasValue && lease.final_site_apprvl_date >= cutoffDate
                                               && lease.final_site_apprvl_date_status_id.HasValue && lease.final_site_apprvl_date_status_id.Value.Equals(1)
                                       select new DmsLinkDocumentStructure
                                       {
                                           siteID = lease.site_uid,
                                           TypeCode = (int) obj.document.Type,
                                           RecordId = lease.id,
                                           FieldCode = (int) obj.document.Field,
                                           Update_AT = lease.updated_at
                                       };

                            returnObj.AddRange(leaseApp.Distinct().ToList());
                            break;

                        case DMSDocumentLinkHelper.eDocumentField.MarketHandoff_Status:
                            //Market Handoff
                            leaseApp = from lease in leaseAppRecord
                                       where !(from doc in leaseDocRecord
                                               select doc.record_id).Contains(lease.id)
                                               && lease.market_handoff_date.HasValue && lease.market_handoff_date >= cutoffDate
                                               && lease.market_handoff_date_status_id.HasValue && lease.market_handoff_date_status_id.Value.Equals(1)
                                       select new DmsLinkDocumentStructure
                                       {
                                           siteID = lease.site_uid,
                                           TypeCode = (int) obj.document.Type,
                                           RecordId = lease.id,
                                           FieldCode = (int) obj.document.Field,
                                           Update_AT = lease.updated_at
                                       };

                            returnObj.AddRange(leaseApp.Distinct().ToList());
                            break;

                        case DMSDocumentLinkHelper.eDocumentField.SA_Received:
                            leaseApp = from lease in saRecord
                                       where !(from doc in leaseDocRecord
                                               select doc.record_id).Contains(lease.id)
                                               && lease.updated_at >= cutoffDate
                                       select new DmsLinkDocumentStructure
                                       {
                                           siteID = lease.lease_applications.site_uid,
                                           TypeCode = (int) obj.document.Type,
                                           RecordId = lease.lease_application_id,
                                           FieldCode = (int) obj.document.Field,
                                           Update_AT = lease.updated_at
                                       };

                            returnObj.AddRange(leaseApp.Distinct().ToList());
                            break;
                        
                        //Docs Back from Customer
                        case DMSDocumentLinkHelper.eDocumentField.LOEHoldupReasonTypes_DocsBackfromCustomer:
                            leaseApp = from lease in leaseAppRecord
                                       where !(from doc in leaseDocRecord
                                               select doc.record_id).Contains(lease.id)
                                               && lease.lease_back_from_cust_date.HasValue && lease.lease_back_from_cust_date >= cutoffDate
                                               && lease.lease_back_from_cust_date_status_id.HasValue && lease.lease_back_from_cust_date_status_id.Value.Equals(1)
                                       select new DmsLinkDocumentStructure
                                       {
                                           siteID = lease.site_uid,
                                           TypeCode = (int) obj.document.Type,
                                           RecordId = lease.id,
                                           FieldCode = (int) obj.document.Field,
                                           Update_AT = lease.updated_at
                                       };

                            returnObj.AddRange(leaseApp.Distinct().ToList());
                            break;
                    }
                }
                return returnObj;
            }
        }

        public List<DmsLinkDocumentStructure> GetSdp(DmsWidgetDocument[] listDoc)
        {
            List<DmsLinkDocumentStructure> returnObj = new List<DmsLinkDocumentStructure>();
            var cutoffDate = DateTime.Parse(ConfigurationManager.AppSettings["MissingDocumentCutoffDate"]);

            using (CPTTEntities ce = new CPTTEntities())
            {
                var sdpRecord = from sdp in ce.site_data_packages
                                where sdp.updated_at >= cutoffDate
                                select sdp;                

                foreach (DmsWidgetDocument obj in listDoc.Where(x => x.document.TypeCode == (int) DMSDocumentLinkHelper.eDocumentType.site_data_packages))
                {
                    string tmpField = obj.document.Field.ToString();
                    var sdpDocRecord = from towerDoc in ce.documents_links
                                       where towerDoc.linked_field == tmpField
                                       select towerDoc;

                    IQueryable<DmsLinkDocumentStructure> leaseApp;

                    switch (obj.document.Field)
                    {
                        case DMSDocumentLinkHelper.eDocumentField.TowerDrawing_DocStatus:
                            //Tower Drawing Doc Status
                            var sdpList = from sdp in sdpRecord
                                          where !(from doc in sdpDocRecord
                                                  select doc.record_id).Contains(sdp.id)
                                                  && sdp.updated_at >= cutoffDate
                                                  && sdp.tower_drawings_doc_status_id.HasValue && sdp.tower_drawings_doc_status_id.Value.Equals(1)
                                          select new DmsLinkDocumentStructure
                                          {
                                              siteID = sdp.site_uid,
                                              TypeCode = (int) obj.document.Type,
                                              RecordId = sdp.id,
                                              FieldCode = (int) obj.document.Field,
                                              Update_AT = sdp.updated_at
                                          };
                            returnObj.AddRange(sdpList.Distinct().ToList());
                            break;

                        case DMSDocumentLinkHelper.eDocumentField.TowerTagPhoto_DocStatus:
                            //Tower Tag Photo Doc Status
                            sdpList = from sdp in sdpRecord
                                      where !(from doc in sdpDocRecord
                                              select doc.record_id).Contains(sdp.id)
                                              && sdp.updated_at >= cutoffDate
                                              && sdp.tower_tag_photo_doc_status_id.HasValue && sdp.tower_tag_photo_doc_status_id.Value.Equals(1)
                                      select new DmsLinkDocumentStructure
                                      {
                                          siteID = sdp.site_uid,
                                          TypeCode = (int) obj.document.Type,
                                          RecordId = sdp.id,
                                          FieldCode = (int) obj.document.Field,
                                          Update_AT = sdp.updated_at
                                      };

                            returnObj.AddRange(sdpList.Distinct().ToList());
                            break;
                            
                        case DMSDocumentLinkHelper.eDocumentField.StructuralCalcs_DocStatus:
                            // Structural Calcs Doc Status
                            sdpList = from sdp in sdpRecord
                                      where !(from doc in sdpDocRecord
                                              select doc.record_id).Contains(sdp.id)
                                              && sdp.updated_at >= cutoffDate
                                              && sdp.structural_calcs_doc_status_id.HasValue && sdp.structural_calcs_doc_status_id.Value.Equals(1)
                                      select new DmsLinkDocumentStructure
                                      {
                                          siteID = sdp.site_uid,
                                          TypeCode = (int) obj.document.Type,
                                          RecordId = sdp.id,
                                          FieldCode = (int) obj.document.Field,
                                          Update_AT = sdp.updated_at
                                      };

                            returnObj.AddRange(sdpList.Distinct().ToList());
                            break;

                        case DMSDocumentLinkHelper.eDocumentField.StructuralAnalysis_DocStatus:
                            // Structural Analysis Current Doc Status
                            sdpList = from sdp in sdpRecord
                                      where !(from doc in sdpDocRecord
                                              select doc.record_id).Contains(sdp.id)
                                              && sdp.updated_at >= cutoffDate
                                              && sdp.structural_analysis_current_doc_status_id.HasValue && sdp.structural_analysis_current_doc_status_id.Value.Equals(1)
                                      select new DmsLinkDocumentStructure
                                      {
                                          siteID = sdp.site_uid,
                                          TypeCode = (int) obj.document.Type,
                                          RecordId = sdp.id,
                                          FieldCode = (int) obj.document.Field,
                                          Update_AT = sdp.updated_at
                                      };

                            returnObj.AddRange(sdpList.Distinct().ToList());
                            break;

                        case DMSDocumentLinkHelper.eDocumentField.TowerErectionDetail_DocStatus:
                            //Tower Erection Detail Doc Status
                            sdpList = from sdp in sdpRecord
                                      where !(from doc in sdpDocRecord
                                              select doc.record_id).Contains(sdp.id)
                                              && sdp.updated_at >= cutoffDate
                                              && sdp.tower_erection_detail_doc_status_id.HasValue && sdp.tower_erection_detail_doc_status_id.Value.Equals(1)
                                      select new DmsLinkDocumentStructure
                                      {
                                          siteID = sdp.site_uid,
                                          TypeCode = (int) obj.document.Type,
                                          RecordId = sdp.id,
                                          FieldCode = (int) obj.document.Field,
                                          Update_AT = sdp.updated_at
                                      };

                            returnObj.AddRange(sdpList.Distinct().ToList());
                            break;

                        case DMSDocumentLinkHelper.eDocumentField.FoundationDesign_DocStatus:
                            //Foundation Design Doc Status
                                sdpList = from sdp in sdpRecord
                                          where !(from doc in sdpDocRecord
                                                  select doc.record_id).Contains(sdp.id)
                                                  && sdp.updated_at >= cutoffDate
                                                  && sdp.foundation_design_doc_status_id.HasValue && sdp.foundation_design_doc_status_id.Value.Equals(1)
                                          select new DmsLinkDocumentStructure
                                          {
                                              siteID = sdp.site_uid,
                                              TypeCode = (int) obj.document.Type,
                                              RecordId = sdp.id,
                                              FieldCode = (int) obj.document.Field,
                                              Update_AT = sdp.updated_at
                                          };

                                returnObj.AddRange(sdpList.Distinct().ToList());
                            break;
                        case DMSDocumentLinkHelper.eDocumentField.ConstructionDrawings_DocStatus:
                            //Construction Drawings Doc Status
                                sdpList = from sdp in sdpRecord
                                          where !(from doc in sdpDocRecord
                                                  select doc.record_id).Contains(sdp.id)
                                                  && sdp.updated_at >= cutoffDate
                                                  && sdp.construction_drawings_doc_status_id.HasValue && sdp.construction_drawings_doc_status_id.Value.Equals(1)
                                          select new DmsLinkDocumentStructure
                                          {
                                              siteID = sdp.site_uid,
                                              TypeCode = (int) obj.document.Type,
                                              RecordId = sdp.id,
                                              FieldCode = (int) obj.document.Field,
                                              Update_AT = sdp.updated_at
                                          };

                                returnObj.AddRange(sdpList.Distinct().ToList());
                            break;
                        case DMSDocumentLinkHelper.eDocumentField.GeotechReport_DocStatus:
                            //Geotech Report Doc Status
                                sdpList = from sdp in sdpRecord
                                          where !(from doc in sdpDocRecord
                                                  select doc.record_id).Contains(sdp.id)
                                                  && sdp.updated_at >= cutoffDate
                                                  && sdp.geotech_report_doc_status_id.HasValue && sdp.geotech_report_doc_status_id.Value.Equals(1)
                                          select new DmsLinkDocumentStructure
                                          {
                                              siteID = sdp.site_uid,
                                              TypeCode = (int) obj.document.Type,
                                              RecordId = sdp.id,
                                              FieldCode = (int) obj.document.Field,
                                              Update_AT = sdp.updated_at
                                          };

                                returnObj.AddRange(sdpList.Distinct().ToList());
                            break;

                        case DMSDocumentLinkHelper.eDocumentField.BuildingPermit_DocStatus:
                            //Building Permit Doc Status
                                sdpList = from sdp in sdpRecord
                                          where !(from doc in sdpDocRecord
                                                  select doc.record_id).Contains(sdp.id)
                                                  && sdp.updated_at >= cutoffDate
                                                  && sdp.building_permit_doc_status_id.HasValue && sdp.building_permit_doc_status_id.Value.Equals(1)
                                          select new DmsLinkDocumentStructure
                                          {
                                              siteID = sdp.site_uid,
                                              TypeCode = (int) obj.document.Type,
                                              RecordId = sdp.id,
                                              FieldCode = (int) obj.document.Field,
                                              Update_AT = sdp.updated_at
                                          };

                                returnObj.AddRange(sdpList.Distinct().ToList());
                            break;

                        case DMSDocumentLinkHelper.eDocumentField.ZoningApproval_DocStatus:
                            //Zoning Approval Doc Status
                                sdpList = from sdp in sdpRecord
                                          where !(from doc in sdpDocRecord
                                                  select doc.record_id).Contains(sdp.id)
                                                  && sdp.updated_at >= cutoffDate
                                                  && sdp.zoning_approval_doc_status_id.HasValue && sdp.zoning_approval_doc_status_id.Value.Equals(1)
                                          select new DmsLinkDocumentStructure
                                          {
                                              siteID = sdp.site_uid,
                                              TypeCode = (int) obj.document.Type,
                                              RecordId = sdp.id,
                                              FieldCode = (int) obj.document.Field,
                                              Update_AT = sdp.updated_at
                                          };

                                returnObj.AddRange(sdpList.Distinct().ToList());
                            break;

                        case DMSDocumentLinkHelper.eDocumentField.PhaseI_DocStatus:
                            //Phase I Doc Status
                                sdpList = from sdp in sdpRecord
                                          where !(from doc in sdpDocRecord
                                                  select doc.record_id).Contains(sdp.id)
                                                  && sdp.updated_at >= cutoffDate
                                                  && sdp.phase_i_doc_status_id.HasValue && sdp.phase_i_doc_status_id.Value.Equals(1)
                                          select new DmsLinkDocumentStructure
                                          {
                                              siteID = sdp.site_uid,
                                              TypeCode = (int) obj.document.Type,
                                              RecordId = sdp.id,
                                              FieldCode = (int) obj.document.Field,
                                              Update_AT = sdp.updated_at
                                          };

                                returnObj.AddRange(sdpList.Distinct().ToList());
                            break;

                        case DMSDocumentLinkHelper.eDocumentField.PhaseII_DocStatus:
                            //Phase II Doc Status
                                sdpList = from sdp in sdpRecord
                                          where !(from doc in sdpDocRecord
                                                  select doc.record_id).Contains(sdp.id)
                                                  && sdp.updated_at >= cutoffDate
                                                  && sdp.phase_ii_doc_status_id.HasValue && sdp.phase_ii_doc_status_id.Value.Equals(1)
                                          select new DmsLinkDocumentStructure
                                          {
                                              siteID = sdp.site_uid,
                                              TypeCode = (int) obj.document.Type,
                                              RecordId = sdp.id,
                                              FieldCode = (int) obj.document.Field,
                                              Update_AT = sdp.updated_at
                                          };
                                returnObj.AddRange(sdpList.Distinct().ToList());
                            break;

                        case DMSDocumentLinkHelper.eDocumentField.NEPA_DocStatus:
                            //NEPA Doc Status
                                sdpList = from sdp in sdpRecord
                                          where !(from doc in sdpDocRecord
                                                  select doc.record_id).Contains(sdp.id)
                                                  && sdp.updated_at >= cutoffDate
                                                  && sdp.nepa_doc_status_id.HasValue && sdp.nepa_doc_status_id.Value.Equals(1)
                                          select new DmsLinkDocumentStructure
                                          {
                                              siteID = sdp.site_uid,
                                              TypeCode = (int) obj.document.Type,
                                              RecordId = sdp.id,
                                              FieldCode = (int) obj.document.Field,
                                              Update_AT = sdp.updated_at
                                          };

                                returnObj.AddRange(sdpList.Distinct().ToList());
                            break;

                        case DMSDocumentLinkHelper.eDocumentField.SHPO_DocStatus:
                            //SHPO Doc Status
                                sdpList = from sdp in sdpRecord
                                          where !(from doc in sdpDocRecord
                                                  select doc.record_id).Contains(sdp.id)
                                                  && sdp.updated_at >= cutoffDate
                                                  && sdp.shpo_doc_status_id.HasValue && sdp.shpo_doc_status_id.Value.Equals(1)
                                          select new DmsLinkDocumentStructure
                                          {
                                              siteID = sdp.site_uid,
                                              TypeCode = (int) obj.document.Type,
                                              RecordId = sdp.id,
                                              FieldCode = (int) obj.document.Field,
                                              Update_AT = sdp.updated_at
                                          };

                                returnObj.AddRange(sdpList.Distinct().ToList());
                            break;

                        case DMSDocumentLinkHelper.eDocumentField.GPS1a2c_DocStatus:
                            //GPS 1a2c Doc Status
                                sdpList = from sdp in sdpRecord
                                          where !(from doc in sdpDocRecord
                                                  select doc.record_id).Contains(sdp.id)
                                                  && sdp.updated_at >= cutoffDate
                                                  && sdp.gps_1a2c_doc_status_id.HasValue && sdp.gps_1a2c_doc_status_id.Value.Equals(1)
                                          select new DmsLinkDocumentStructure
                                          {
                                              siteID = sdp.site_uid,
                                              TypeCode = (int) obj.document.Type,
                                              RecordId = sdp.id,
                                              FieldCode = (int) obj.document.Field,
                                              Update_AT = sdp.updated_at
                                          };

                                returnObj.AddRange(sdpList.Distinct().ToList());
                            break;

                        case DMSDocumentLinkHelper.eDocumentField.Airspace_DocStatus:
                            //Airspace Doc Status
                                sdpList = from sdp in sdpRecord
                                          where !(from doc in sdpDocRecord
                                                  select doc.record_id).Contains(sdp.id)
                                                  && sdp.updated_at >= cutoffDate
                                                  && sdp.airspace_doc_status_id.HasValue && sdp.airspace_doc_status_id.Value.Equals(1)
                                          select new DmsLinkDocumentStructure
                                          {
                                              siteID = sdp.site_uid,
                                              TypeCode = (int) obj.document.Type,
                                              RecordId = sdp.id,
                                              FieldCode = (int) obj.document.Field,
                                              Update_AT = sdp.updated_at
                                          };

                                returnObj.AddRange(sdpList.Distinct().ToList());
                            break;

                        case DMSDocumentLinkHelper.eDocumentField.FAAStudyDet_DocStatus:
                            //FAA Study Determination Doc Status
                                sdpList = from sdp in sdpRecord
                                          where !(from doc in sdpDocRecord
                                                  select doc.record_id).Contains(sdp.id)
                                                  && sdp.updated_at >= cutoffDate
                                                  && sdp.faa_study_determination_doc_status_id.HasValue && sdp.faa_study_determination_doc_status_id.Value.Equals(1)
                                          select new DmsLinkDocumentStructure
                                          {
                                              siteID = sdp.site_uid,
                                              TypeCode = (int) obj.document.Type,
                                              RecordId = sdp.id,
                                              FieldCode = (int) obj.document.Field,
                                              Update_AT = sdp.updated_at
                                          };

                                returnObj.AddRange(sdpList.Distinct().ToList());
                            break;
                    
                        case DMSDocumentLinkHelper.eDocumentField.FCC_DocStatus:
                            //FCC Doc Status
                            sdpList = from sdp in sdpRecord
                                      where !(from doc in sdpDocRecord
                                              select doc.record_id).Contains(sdp.id)
                                              && sdp.updated_at >= cutoffDate
                                              && sdp.fcc_doc_status_id.HasValue && sdp.fcc_doc_status_id.Value.Equals(1)
                                      select new DmsLinkDocumentStructure
                                      {
                                          siteID = sdp.site_uid,
                                          TypeCode = (int) obj.document.Type,
                                          RecordId = sdp.id,
                                          FieldCode = (int) obj.document.Field,
                                          Update_AT = sdp.updated_at
                                      };

                            returnObj.AddRange(sdpList.Distinct().ToList());
                        break;

                        case DMSDocumentLinkHelper.eDocumentField.AMCertification_DocStatus:
                            //AMCertification Doc Status
                            sdpList = from sdp in sdpRecord
                                      where !(from doc in sdpDocRecord
                                              select doc.record_id).Contains(sdp.id)
                                              && sdp.updated_at >= cutoffDate
                                              && sdp.am_certification_doc_status_id.HasValue && sdp.am_certification_doc_status_id.Value.Equals(1)
                                      select new DmsLinkDocumentStructure
                                      {
                                          siteID = sdp.site_uid,
                                          TypeCode = (int) obj.document.Type,
                                          RecordId = sdp.id,
                                          FieldCode = (int) obj.document.Field,
                                          Update_AT = sdp.updated_at
                                      };

                            returnObj.AddRange(sdpList.Distinct().ToList());
                        break;

                        case DMSDocumentLinkHelper.eDocumentField.Towair_DocStatus:
                            //Towair Document Status
                            sdpList = from sdp in sdpRecord
                                      where !(from doc in sdpDocRecord
                                              select doc.record_id).Contains(sdp.id)
                                              && sdp.updated_at >= cutoffDate
                                              && sdp.towair_doc_status_id.HasValue && sdp.towair_doc_status_id.Value.Equals(1)
                                      select new DmsLinkDocumentStructure
                                      {
                                          siteID = sdp.site_uid,
                                          TypeCode = (int) obj.document.Type,
                                          RecordId = sdp.id,
                                          FieldCode = (int) obj.document.Field,
                                          Update_AT = sdp.updated_at
                                      };

                            returnObj.AddRange(sdpList.Distinct().ToList());
                        break;

                        case DMSDocumentLinkHelper.eDocumentField.PrimeLease_DocStatus:
                            //Prime Lease Doc Status
                            sdpList = from sdp in sdpRecord
                                      where !(from doc in sdpDocRecord
                                              select doc.record_id).Contains(sdp.id)
                                              && sdp.updated_at >= cutoffDate
                                              && sdp.prime_lease_doc_status_id.HasValue && sdp.prime_lease_doc_status_id.Value.Equals(1)
                                      select new DmsLinkDocumentStructure
                                      {
                                          siteID = sdp.site_uid,
                                          TypeCode = (int) obj.document.Type,
                                          RecordId = sdp.id,
                                          FieldCode = (int) obj.document.Field,
                                          Update_AT = sdp.updated_at
                                      };

                            returnObj.AddRange(sdpList.Distinct().ToList());
                        break;

                        case DMSDocumentLinkHelper.eDocumentField.Memorandum_DocStatus:
                            //Memorandum of Lease Doc Status
                            sdpList = from sdp in sdpRecord
                                      where !(from doc in sdpDocRecord
                                              select doc.record_id).Contains(sdp.id)
                                              && sdp.updated_at >= cutoffDate
                                              && sdp.memorandum_of_lease_doc_status_id.HasValue && sdp.memorandum_of_lease_doc_status_id.Value.Equals(1)
                                      select new DmsLinkDocumentStructure
                                      {
                                          siteID = sdp.site_uid,
                                          TypeCode = (int) obj.document.Type,
                                          RecordId = sdp.id,
                                          FieldCode = (int) obj.document.Field,
                                          Update_AT = sdp.updated_at
                                      };

                            returnObj.AddRange(sdpList.Distinct().ToList());
                        break;

                        case DMSDocumentLinkHelper.eDocumentField.Title_DocStatus:
                            //Title Doc Status
                            sdpList = from sdp in sdpRecord
                                      where !(from doc in sdpDocRecord
                                              select doc.record_id).Contains(sdp.id)
                                              && sdp.updated_at >= cutoffDate
                                              && sdp.title_doc_status_id.HasValue && sdp.title_doc_status_id.Value.Equals(1)
                                      select new DmsLinkDocumentStructure
                                      {
                                          siteID = sdp.site_uid,
                                          TypeCode = (int) obj.document.Type,
                                          RecordId = sdp.id,
                                          FieldCode = (int) obj.document.Field,
                                          Update_AT = sdp.updated_at
                                      };

                            returnObj.AddRange(sdpList.Distinct().ToList());
                        break;
                    }
                }
                return returnObj;
            }
        }
    }
}