﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using FlexigridASPNET;
using TrackIT2.BLL;
using TrackIT2.DAL;
using System.Collections.Specialized;
using System.Collections;

namespace TrackIT2.Handlers
{
    /// <summary>
    /// DMS FlexiGrid Handler
    /// </summary>
    public class DmsFlexiGridHandler : BaseFlexiHandler
    {
        // Var
        private const string HTML_JS_LINK = "<a href=\"javascript:void(0);\" onclick=\"{0}\" style=\"text-decoration:none;\">{1}</a>";
        private const string HTML_IMAGE = "<img src=\"{0}\" {1}/>";
        private const string HTML_IMAGE_ICON = "<img width=\"{2}\" height=\"{3}\" src=\"{0}\" {1}/>";
        private const string HTML_SELECT_ACTIONS = "<select class=\"sel-file-actions\" style=\"width:auto;\" disabled=\"disabled\"><option>- Select -</option><option value=\"{0}\">Download</option><option value=\"{0}\">Edit</option><option value=\"{0}\">Move</option><option value=\"{0}\">Delete</option></select>";
        private const string HTML_SELECT_ACTIONS_FOLDER = "<select class=\"sel-folder-actions\" style=\"width:auto;\"><option>- Select -</option><option value=\"{0}\">Edit</option><option value=\"{0}\">Move</option><option value=\"{0}\">Delete</option></select>";
        private const string HTML_SELECT_ACTIONS_NO_DELETE = "<select class=\"sel-file-actions\" style=\"width:auto;\" disabled=\"disabled\"><option>- Select -</option><option value=\"{0}\">Download</option><option value=\"{0}\">Edit</option><option value=\"{0}\">Move</option></select>";
        private const string HTML_SELECT_ACTIONS_REVISION = "<select class=\"sel-file-actions\" style=\"width:auto;\" ><option>- Select -</option><option value=\"{0}\">Download</option><option value=\"{0}\">Move</option><option value=\"{0}\">Delete</option></select></select>"; 
        private const string PATH_LOCK_IMG = "/images/icons/Lock.png";
        private const string PATH_UNLOCK_IMG = "/images/icons/Unlock.png";
        private const string PATH_FILE_TYPE_IMG_ROOT = "/images/icons/file-types/32px/";
        private const string ACTIONS_UNAVAILABLE = "<em>Actions Unavailable</em>";
        //add parameter for expand/collapse file revivision by Kantorn J. 2012-08-15
        private const string PATH_EXPAND_IMG = "/images/icons/details_open.png";
        private const string PATH_COLLAPSE_IMG = "/images/icons/details_close.png";

        private static bool _userHasAdministrationPrivileges;
        private DmsMetaDataHandler metaHand;

        private const string PSEUDO_FOLDER_NAME = "All Documents";

        //>
        public static List<KeyValuePair<string, object[]>> result;
        
        /// <summary>
        /// Gets the filtered data.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<KeyValuePair<string, object[]>> GetFilteredData()
        {

            // Flexigrid params
            result = new List<KeyValuePair<string, object[]>>();
            const int START_INDEX = 6;
            string basePath = "";
            _userHasAdministrationPrivileges = Security.UserIsInRole("TrackiT Docs Admin");
            bool bIsSortData = false;
            System.Collections.Hashtable hashFileRevision = new System.Collections.Hashtable();
            bool isGetAllDocument = false;

            if (HttpContext.Current.Request.QueryString["pseudoFolder"] != null && HttpContext.Current.Request.QueryString["pseudoFolder"] == "true")
            {
                isGetAllDocument = true;
            }

            // Check is item exist?
            if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["checkExist"]) && !string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["key"]))
            {
                string key = HttpContext.Current.Request.QueryString["key"];
                return CheckExist(key);
            }
            //>

            // Create new folder.
            if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["createNewFolder"]) && !string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["key"]))
            {
                string key = HttpContext.Current.Request.QueryString["key"];
                return CreateNewFolder(key);
            }
            //>

            // Item Load META Request?
            if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["requestType"]) && !string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["key"])
                && HttpContext.Current.Request.QueryString["requestType"] == "loadItemMeta")
            {
                // Key
                string key = HttpContext.Current.Request.QueryString["key"];

                // Load Actions?
                bool loadActions = !string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["loadActions"]) && HttpContext.Current.Request.QueryString["loadActions"] == "true";

                // Load revision?
                bool loadRevision = !string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["revison"]) && HttpContext.Current.Request.QueryString["revison"] == "true";
                // Load and return META

                return LoadItemMeta(key, result, loadActions, loadRevision, isGetAllDocument);

            }
            //>

            // Item Check-Out / In Request?
            if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["requestType"]) && !string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["key"])
                && (HttpContext.Current.Request.QueryString["basePath"] != null) && HttpContext.Current.Request.QueryString["requestType"] == "checkOutOrIn"
                    && (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["checkVal"])))
            {
                // Key
                basePath = HttpContext.Current.Request.QueryString["basePath"];
                string key = HttpContext.Current.Request.QueryString["key"];
                string sIsCheckOut = (Boolean.Parse(HttpContext.Current.Request.QueryString["checkVal"]) ? "CheckOut" : "CheckIn");

                // Check Item Out or In, Load and return META
                return CheckItemOutOrIn(basePath, key, sIsCheckOut, result, isGetAllDocument);
            }
            //>
            // Item revision Request? by Kntorn Jaruma
            if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["expandkey"]))
            {
                // Key
                string key = (HttpContext.Current.Request.QueryString["expandkey"]);
                string basepath = (HttpContext.Current.Request.QueryString["basepath"]);
                bool bIsLoadedRevision = false;

                //Check session has key or not? case using multitab
                if (HttpContext.Current.Session["hashAllRevisonItem"] != null)
                {
                    System.Collections.Hashtable hashAllRevisonItem = (System.Collections.Hashtable)HttpContext.Current.Session["hashAllRevisonItem"];
                    foreach (System.Collections.DictionaryEntry entry in hashAllRevisonItem)
                    {
                        if (HttpUtility.UrlDecode(((AWSObjectItem)entry.Key).Key) == HttpUtility.UrlDecode(key))
                        {
                            bIsLoadedRevision = true;
                            break;
                        }
                    }
                }

                if (!bIsLoadedRevision)
                {
                    var objectSetRev = AmazonDMS.GetObjects(basepath, null, isGetAll: true);
                    var queryItemsRev = objectSetRev.S3Objects.Where(a => a.Type != AWSObjectItemType.Folder).ToList();
                    var queryThisPageItemsRev = new List<AWSObjectItem>();

                    //get only currnet version by Kantorn J. 2012-08-15 
                    if (queryItemsRev.Count > 0)
                    {
                        queryItemsRev = GetCurrentVersionFile(queryItemsRev, ref hashFileRevision);
                    }
                }
 
                // Load revision data
                return LoadRevision(key, basepath);
            }
            //>

            // Amazon Result Set
            new AWSResultSet();

            // Get Form Data.
            NameValueCollection form = HttpContext.Current.Request.Form;
            bool bIsSiteDashBoard = false;
            if (HttpContext.Current.Request.UrlReferrer != null)
            {
                // Get Hash Form Value
                string hashFormVal = "";
                if ((form.Get("hash") != null) && (form.Get("hash") != "")) hashFormVal = form.Get("hash");
                bIsSortData = string.IsNullOrEmpty(form.Get("sortdata")) ? false : true;
                // Site Dashboard Detection / Configuration
                if (HttpContext.Current.Request.UrlReferrer.LocalPath.ToString(CultureInfo.InvariantCulture).Contains("SiteDashboard.aspx"))
                {
                    bIsSiteDashBoard = true;
                    // Folder Browsing Via Site ID
                    if (HttpContext.Current.Request.UrlReferrer.Query.ToString(CultureInfo.InvariantCulture).Contains("id") && (!hashFormVal.Contains("#")) && (form.Get("fd") == null || form.Get("fd") == ""))
                    {
                        string query = HttpContext.Current.Request.UrlReferrer.Query.Substring(1);
                        string[] sValues = query.Split('&');
                        foreach (string t in sValues)
                        {
                            string[] items = t.Split('=');
                            if (items[0] == "id")
                            {
                                basePath = items[1];
                            }
                        }
                    }
                    // Folder Browsing Via a URL, by using the hash
                    else if ((hashFormVal.Contains("#")) && (form.Get("fd") == null || form.Get("fd") == ""))
                    {
                        // Get Base Path From Hash
                        basePath = GetBasePathFromHash(hashFormVal);
                    }
                }
                else
                {
                    // Folder Browsing Via a URL, by using the hash
                    if ((hashFormVal.Contains("#")) && (form.Get("fd") == null || form.Get("fd") == ""))
                    {
                        // Get Base Path From Hash
                        basePath = GetBasePathFromHash(hashFormVal);
                    }
                }
            }
            //>
            


            GridSortOrder? customSorting = null;
            // Check Base Path
            if (form.Count > START_INDEX)
            {
                for (int i = START_INDEX; i < form.Count; i++)
                {
                    //get Custom sort
                    if (form.GetKey(i) == Globals.QS_SORT_DIRECTION)
                    {
                        //Using custom sort order from widget.
                        if (form.Get(i) == Globals.SORT_DIRECTION_VALUE_ASC)
                        {
                            customSorting = GridSortOrder.Asc;
                        }
                        else
                        {
                            customSorting = GridSortOrder.Desc;
                        }
                    }
                    // Folder Change.
                    if (form.GetKey(i) == "fd")
                    {
                        if (form.Get(i) != null && form.Get(i) != "")
                        {
                            basePath = HttpUtility.UrlDecode(form.Get(i));
                            basePath = basePath.Substring(0, basePath.Length - 1);
                        }
                    }

                    // Delete File
                    if (form.GetKey(i) == "del")
                    {
                        if (form.Get(i) != null && form.Get(i) != "")
                        {
                            bIsSortData = false;
                            string keyName = HttpUtility.UrlDecode(form.Get(i));
                            bool isFolder = !keyName.Contains(".");
                            //Delete
                            AmazonDMS.DeleteObjectsRecursively(keyName, isFolder);

                            //remove revision file
                            if (!isFolder)
                            {
                                System.Collections.Hashtable hashAllRevisonItem;
                                if (HttpContext.Current.Session["hashAllRevisonItem"] != null)
                                {
                                    hashAllRevisonItem = (System.Collections.Hashtable)HttpContext.Current.Session["hashAllRevisonItem"];
                                }
                                else
                                {
                                    hashAllRevisonItem = new System.Collections.Hashtable();
                                }
                                foreach (System.Collections.DictionaryEntry entry in hashAllRevisonItem)
                                {
                                    if (((AWSObjectItem)entry.Key).Key == keyName)
                                    {
                                        List<AWSObjectItem> lstRev = (List<AWSObjectItem>)entry.Value;
                                        foreach (AWSObjectItem s3Item in lstRev)
                                        {
                                            AmazonDMS.DeleteObjectsRecursively(s3Item.Key, false);
                                        }
                                    }
                                }

                                RemoveOtherSession(keyName);
                            }
                        }
                    }
                }
            }
            //>


            // Get all document on url
            if (basePath.Split('/').Length == 2 && basePath.Split('/')[1] == PSEUDO_FOLDER_NAME)
            {
                basePath = basePath.Split('/').FirstOrDefault();
                isGetAllDocument = true;
            }

            // Refresh data source
            AWSResultSet objectSet = new AWSResultSet();
			if(!isGetAllDocument)
			{
            	objectSet = AmazonDMS.GetObjects(basePath, null, isGetAll: true);
			}
			else
			{
				objectSet = GetAllDocument(basePath);
			}
            
            // Store current Bucket Object Names (for quick reference on file upload and file / folder name changes)
            AmazonDMS.CurrentBucketObjectNames = objectSet.S3Objects.Select(o => o.FileName).ToList();

            // Items Count
            //VitualItemsCount = objectSet.S3Objects.Count;

            //skip use thread retreieve metadata by Kantorn J. 2012-08-15
            Hashtable hashQueryStaticItems = new Hashtable();
            Hashtable tmpHashQueryStaticItems = new Hashtable();
            List<AWSObjectItem> queryStaticItems = new List<AWSObjectItem>();
            String tmpBasePath = isGetAllDocument ? basePath + "/AllDocument" : basePath;
            if (!bIsSortData)
            {
                int row = 0;

                if (HttpContext.Current.Session["queryStaticItems"] != null)
                {
                    tmpHashQueryStaticItems = (Hashtable)HttpContext.Current.Session["queryStaticItems"];
                }

                if ((!string.IsNullOrEmpty(basePath) && basePath.Split('/').Length == 1) && !isGetAllDocument)
                {
                    AWSObjectItem awsPsudofolder = new AWSObjectItem();
                    awsPsudofolder.Key = tmpBasePath + "/" + PSEUDO_FOLDER_NAME;
                    awsPsudofolder.Type = AWSObjectItemType.Folder;
                    awsPsudofolder.FileName = PSEUDO_FOLDER_NAME;
                    List<AWSObjectItem> bufferItems = objectSet.S3Objects;
                    objectSet.S3Objects = new List<AWSObjectItem>();
                    objectSet.S3Objects.Add(awsPsudofolder);
                    objectSet.S3Objects.AddRange(bufferItems);
                }

                foreach (AWSObjectItem obj in objectSet.S3Objects)
                {
                    if (obj.Type == AWSObjectItemType.File)
                    {
                        metaHand = new DmsMetaDataHandler(tmpBasePath, obj, true, row.ToString());
                        metaHand.OnComplete += new DmsMetaDataHandler.OnComplete_Handler(_OnRunComplete);
                    }
                    else
                    {
                        queryStaticItems.Add(obj);
                    }
                    row++;
                }

                //Change from static parameter to session by Kantorn J, 2012-08-24 
                if (tmpHashQueryStaticItems.ContainsKey(tmpBasePath))
                {
                    tmpHashQueryStaticItems.Remove(tmpBasePath);
                }
                if (!tmpHashQueryStaticItems.ContainsKey(tmpBasePath))
                {
                    tmpHashQueryStaticItems.Add(tmpBasePath, queryStaticItems);
                }

                HttpContext.Current.Session["queryStaticItems"] = tmpHashQueryStaticItems;
            }
            else
            {
                //Change from static parameter to session by Kantorn J, 2012-08-24 
                if (HttpContext.Current.Session["queryStaticItems"] != null)
                {
                    hashQueryStaticItems = (Hashtable)HttpContext.Current.Session["queryStaticItems"];
                    if (hashQueryStaticItems.ContainsKey(tmpBasePath))
                    {
                        queryStaticItems = (List<AWSObjectItem>)hashQueryStaticItems[tmpBasePath];
                        objectSet.S3Objects = queryStaticItems;
                    }
                }
            }

            // Get folders & Items
            var queryFolder = objectSet.S3Objects.Where(a => a.Type == AWSObjectItemType.Folder).ToList();
            var queryItems = objectSet.S3Objects.Where(a => a.Type != AWSObjectItemType.Folder).ToList();
            var queryThisPageFolders = new List<AWSObjectItem>();
            var queryThisPageItems = new List<AWSObjectItem>();
                
            //Add parameter keep total file count in S3 is equalt as thread retrieve by Kantorn J. 2012-08-24
            int bufferCount =  queryFolder.Count + queryItems.Count;
            //get only currnet version by Kantorn J. 2012-08-15 
            if (queryItems.Count > 0)
            {
                queryItems = GetCurrentVersionFile(queryItems, ref hashFileRevision);
            }
            //get total count from folder coun and current file revison by Kantorn J. 2012-08-16
            VitualItemsCount = queryFolder.Count + queryItems.Count;

            int skipItems = 0;

            // TODO: fix sorting!
            // not sort on root path
            // from now can sort by name and size but no metadata in item object
            // sort folder first and then file
            #region sorting

            //disable sort root path
            if (basePath != "")
            {
                // Sort Order
                string sortName = "";
                if (!String.IsNullOrWhiteSpace(this.SortName) && (this.SortName != "null"))
                {
                    sortName = this.SortName;
                }
                else
                {
                    sortName = "FileName";
                    customSorting = GridSortOrder.Asc;
                }
                //Sort Folder
                if (customSorting == null) customSorting = this.SortDirection;
                #region Sort and Skip Folder
                if (sortName == "FileName")
                {
                    // Calculate skip Folders
                    IQueryable<AWSObjectItem> folder = queryFolder.Select(x => x).AsQueryable<AWSObjectItem>();
                    if (customSorting == GridSortOrder.Asc)
                    {
                        var a = Utility.OrderBy(folder, sortName);
                        queryFolder = a.ToList();
                    }
                    else
                    {
                        var b = Utility.OrderByDescending(folder, sortName);
                        queryFolder = b.ToList();
                    }
                }

                int skipFolders = (Page - 1) * ItemsPerPage;
                queryThisPageFolders.AddRange(queryFolder.Skip(skipFolders).Take(ItemsPerPage));

                // Add Folders
                queryThisPageItems.AddRange(queryThisPageFolders);
               
                // Calculate skip Items
                if(queryThisPageFolders.Count > 0)
                {
                    skipItems = queryFolder.Count() - (((Page - 1) * (ItemsPerPage)) + queryThisPageFolders.Count);
                }

                #endregion


                #region Sort and Skip File
                //Add condition search when thread load complete by Kantorn J. 2012-08-24
                bool bIsCanSort = false;
                if (((queryStaticItems != null) && (queryStaticItems.Count == bufferCount)) &&
                    ((sortName == "FileName") || (sortName == "ItemObject.Size") || (sortName == "Metadata.Description") || (sortName == "Revision") 
                    || (sortName == "Metadata.UploadedBy") || (sortName == "Metadata.UploadDate") || (sortName == "Folder")))
                {
                    bIsCanSort = true;
                }
                else if ((sortName == "FileName") || (sortName == "ItemObject.Size") || (sortName == "Folder"))
                { 
                    bIsCanSort = true;
                }
                
                IQueryable<AWSObjectItem> item = queryItems.Select(x => x).AsQueryable<AWSObjectItem>();
                if (sortName == "Folder")
                {
                    foreach (var i in item)
                    {
                        string tmpFolder = string.Empty;
                        foreach (string s in i.Key.Split('/'))
                        {
                            if (s != i.Key.Split('/').FirstOrDefault() && s != i.Key.Split('/').LastOrDefault())
                            {
                                if (!string.IsNullOrEmpty(s))
                                {
                                    tmpFolder += "/";
                                }
                                tmpFolder += s;
                            }
                        }
                        i.Folder = tmpFolder;
                    }
                }

                if (bIsCanSort)
                {
                    //Sort File
                    if (customSorting == GridSortOrder.Asc)
                    {
                        var a = Utility.OrderBy(item, sortName);
                        queryItems = a.ToList();
                    }
                    else
                    {
                        var b = Utility.OrderByDescending(item, sortName);
                        queryItems = b.ToList();
                    }

                }
                else
                {
                    queryItems = item.ToList();
                }
                if (queryThisPageItems.Count() < ItemsPerPage)
                {
                    // Calculate skip Items
                    if (((Page - 1) * (ItemsPerPage)) <= queryFolder.Count())
                    {
                        skipItems = queryFolder.Count() - ((Page - 1) * (ItemsPerPage) + queryThisPageFolders.Count);
                    }
                    else
                    {
                        skipItems = ((Page - 1) * (ItemsPerPage) - queryFolder.Count());
                    }
                    // Add Files
                    queryThisPageItems.AddRange(queryItems.Skip(skipItems).Take(ItemsPerPage - queryThisPageFolders.Count));
                }

                #endregion
            }
            else
            {

                // Calculate skip Folders
                int skipFolders = (Page - 1) * ItemsPerPage;
                queryThisPageFolders.AddRange(queryFolder.Skip(skipFolders).Take(ItemsPerPage));

                // Add Folders
                queryThisPageItems.AddRange(queryThisPageFolders);

                if (queryThisPageItems.Count() < ItemsPerPage)
                {
                    // Calculate skip Items
                    // Calculate skip Items
                    if (((Page - 1) * (ItemsPerPage)) <= queryFolder.Count())
                    {
                        skipItems = queryFolder.Count() - ((Page - 1) * (ItemsPerPage) + queryThisPageFolders.Count);
                    }
                    else
                    {
                        skipItems = ((Page - 1) * (ItemsPerPage) - queryFolder.Count());
                    }
                    // Add Files
                    queryThisPageItems.AddRange(queryItems.Skip(skipItems).Take(ItemsPerPage - queryThisPageFolders.Count));
                }
            }

            #endregion sorting


            // Iterate & Build Results..
            foreach (AWSObjectItem s3Item in queryThisPageItems)
            {
                // var
                string classification = "";
                string keywords = "";
                string description = "";
                string uploadedBy = "";
                string uploadDate = "";
                string checkedInOutButton = "";

                // Type of AWSObjectItemType?
                if (s3Item.Type == AWSObjectItemType.Folder)
                {
                    // Action HTML
                    if (s3Item.FileName == PSEUDO_FOLDER_NAME)
                    {

                        string path = "<a href=\"javascript:void(0);\" onclick=\"ChangeDirectory('" + basePath + "/" + PSEUDO_FOLDER_NAME + "/',true);showActionsLoadingSpinner(this, true);\" style=\"text-decoration:none;\"><img src=\"/images/icons/file-types/folder-horizontal.png\" title=\"Construction\"/><br />" + PSEUDO_FOLDER_NAME + "</a>";
                        result.Add(new KeyValuePair<string, object[]>(basePath + "/" + PSEUDO_FOLDER_NAME + "/", new object[] { 
                         "",path,"","","","","","","","","",""}));
                    }
                    else
                    {
                        string actionFolderHtml = string.Format((_userHasAdministrationPrivileges && (s3Item.Key != "/")) ? HTML_SELECT_ACTIONS_FOLDER : "", s3Item.Key);

                        // Folder AWSObjectItemType
                        //
                        // Build Result
                        result.Add(new KeyValuePair<string, object[]>(s3Item.Key, new object[] { 
                    "",s3Item.FlexiPath,"","",""
                    ,"","","","","","",actionFolderHtml}));
                    }
                }
                else
                {
                    // File AWSObjectItemType
                    //
                    // File Name
                    string fileName = s3Item.FileName;

                    //add option to expand/collapse revision document by Kantorn J. 2012-08-15    
                    string optionImage = "";
                    string option = "";

                    System.Collections.Hashtable hashAllRevisonItem;
                    if (HttpContext.Current.Session["hashAllRevisonItem"] != null)
                    {
                        hashAllRevisonItem = (System.Collections.Hashtable)HttpContext.Current.Session["hashAllRevisonItem"];
                    }
                    else
                    {
                        hashAllRevisonItem = new System.Collections.Hashtable();
                    }

                    if (hashAllRevisonItem.ContainsKey(s3Item))
                    {
                        List<AWSObjectItem> lstRev = (List<AWSObjectItem>)hashAllRevisonItem[s3Item];
                        if (lstRev.Count > 0)
                        {
                            optionImage = string.Format(HTML_IMAGE_ICON, PATH_EXPAND_IMG, "title=\"Click to see revision\" class=\"expand-row\" id=\"img" + HttpUtility.UrlEncode(s3Item.Key) + "\"", "16", "16");
                            option = string.Format(HTML_JS_LINK, "ExpandRevision(this,'" + HttpUtility.UrlEncode(s3Item.Key.Replace(" ", "|")) + "','" + basePath + "');", optionImage + "<br />");
                        }
                        else
                        {
                            //remove expand button for non revision file by Kantorn J. 2012-08-24
                            //optionImage = string.Format(HTML_IMAGE_ICON, PATH_COLLAPSE_IMG, "title=\"File has 1 version\" class=\"collapse-row\" id=\"img" + s3Item.Key + "\"", "16", "16");
                            //option = string.Format(HTML_JS_LINK, " ", optionImage + "<br />"); ;
                        }
                    }

                    // Format File Name fpr FlexiGrid
                    FileUtility.FileType objFileType = FileUtility.GetFileType(fileName);
                    string title = string.Format("title=\"{0}\"", fileName);
                    string sFileImage = string.Format(HTML_IMAGE_ICON, PATH_FILE_TYPE_IMG_ROOT + objFileType.FileIcon, title, "24", "24");
                    fileName = string.Format(HTML_JS_LINK, "OnDownloadClick('" + HttpUtility.UrlEncode(s3Item.Key) + "');", sFileImage + "<br />" + fileName);
                    
                    // Action HTML
                    string actionHtml = string.Format(_userHasAdministrationPrivileges ? HTML_SELECT_ACTIONS : HTML_SELECT_ACTIONS_NO_DELETE, HttpUtility.UrlEncode(s3Item.Key));

                    // META Data
                    if (s3Item.Metadata != null)
                    {
                        // Classification
                        if (s3Item.Metadata.Classification != null && s3Item.Metadata.Classification.Any())
                        {
                            if (s3Item.Metadata.Classification != null && s3Item.Metadata.Classification.Any())
                            {
                                bool blfirstClassification = true;
                                foreach (var classificationItem in s3Item.Metadata.Classification)
                                {
                                    if (!blfirstClassification) classification += ", ";
                                    classification += classificationItem;
                                    blfirstClassification = false;
                                }
                            }
                        }
                        if (s3Item.Metadata.Keywords != null)
                        {
                            keywords = s3Item.Metadata.Keywords;
                        }
                        if (s3Item.Metadata.Description != null)
                        {
                            description = s3Item.Metadata.Description;
                        }
                        if (s3Item.Metadata.UploadedBy != null)
                        {
                            uploadedBy = s3Item.Metadata.UploadedBy;
                        }
                        if (s3Item.Metadata.UploadDate != null && s3Item.Metadata.UploadDate.Year != 1)
                        {
                            uploadDate = s3Item.Metadata.UploadDate.ToLocalTime().ToString(CultureInfo.InvariantCulture);
                        }
                        if (s3Item.Metadata.IsCheckedOut)
                        {
                            actionHtml = ACTIONS_UNAVAILABLE;

                            if (HttpContext.Current.User.Identity.Name == s3Item.Metadata.CheckedOutBy ||
                                HttpContext.Current.User.IsInRole("Administrator"))
                            {
                                checkedInOutButton = string.Format(HTML_JS_LINK, "OnCheckInOutClick('" + HttpUtility.UrlEncode(s3Item.Key) + "|false" + "', this)",
                                                        string.Format(HTML_IMAGE, PATH_LOCK_IMG,
                                                        string.Format("title=\"{0}\"", "Checked out by " + s3Item.Metadata.CheckedOutBy + " on " + s3Item.Metadata.CheckOutDate.ToLocalTime()))
                                                        );
                            }
                            else
                            {
                                checkedInOutButton = string.Format(HTML_JS_LINK, "void(0)",
                                                        string.Format(HTML_IMAGE, PATH_LOCK_IMG,
                                                        string.Format("title=\"{0}\"", "[Check in not allow.]Checked out by " + s3Item.Metadata.CheckedOutBy + " on " + s3Item.Metadata.CheckOutDate.ToLocalTime()))
                                                        );
                            }
                        }
                        else
                        {
                            checkedInOutButton = string.Format(HTML_JS_LINK, "OnCheckInOutClick('" + HttpUtility.UrlEncode(s3Item.Key) + "|true" + "', this)",
                                                        string.Format(HTML_IMAGE, PATH_UNLOCK_IMG, "title=\"Check Out File\"")
                                                        );
                        }
                    }
                    //>


                    //get max revision by Kantorn J. 2012-08-15
                    string revision = "1";
                    if(hashFileRevision.ContainsKey(s3Item))
                    {
                        revision = s3Item.Revision.ToString();
                    }

                    // Build Result
                    result.Add(new KeyValuePair<string, object[]>(s3Item.Key, new object[] 
                    { option,fileName,"", classification, 
                        keywords, description, s3Item.ItemObject.Size.ToFileSize(),
                        uploadedBy,revision, uploadDate, checkedInOutButton,actionHtml,title
                        }));
                }
            }

            return result;
        }
        //-->

        private static List<KeyValuePair<string, object[]>> LoadRevision(string key, string basepath)
        {
            using (AmazonDMS.S3Client = Amazon.AWSClientFactory.CreateAmazonS3Client())
            {
                System.Collections.Hashtable hashAllRevisonItem;
                //buffer or sort file by revision
                List<DmsMetaData> lstMeataData = new List<DmsMetaData>();
                if (HttpContext.Current.Session["hashAllRevisonItem"] != null)
                {
                    hashAllRevisonItem = (System.Collections.Hashtable)HttpContext.Current.Session["hashAllRevisonItem"];
                }
                else
                {
                    hashAllRevisonItem = new System.Collections.Hashtable();
                }
                foreach (System.Collections.DictionaryEntry entry in hashAllRevisonItem)
                {
                    if (HttpUtility.UrlDecode(((AWSObjectItem)entry.Key).Key) == HttpUtility.UrlDecode(key))
                    {
                        //Meta data key is null or not?
                        if (((AWSObjectItem)entry.Key).Metadata == null)
                        {
                            ((AWSObjectItem)entry.Key).Metadata = AmazonDMS.GetObjectMetadata(((AWSObjectItem)entry.Key).Key);
                        }

                        List<AWSObjectItem> lstRev = (List<AWSObjectItem>)entry.Value;
                        foreach (AWSObjectItem s3Item in lstRev)
                        {

                            string classification = "";
                            string keywords = "";
                            string description = "";
                            string uploadedBy = "";
                            string uploadDate = "";
                            bool checkedInOutButton = false;
                            string bufferFileName = s3Item.FileName;
                            string fileName = s3Item.FileName;
                            string buffer_filename = string.Empty;

                            //remove revsion from extension
                            if (fileName.Split('.').Length > 1)
                            {
                                for (int i = 0; i < (fileName.Split('.').Length - 1); i++)
                                {
                                    if (i > 0)
                                    {
                                        buffer_filename += ".";
                                    }
                                    buffer_filename += fileName.Split('.')[i];
                                }
                            }

                            //add option to expand/collapse revision document by Kantorn J. 2012-08-15    
                            string optionImage = "";
                            string option = "";

                            // Format File Name fpr FlexiGrid
                            FileUtility.FileType objFileType = FileUtility.GetFileType(buffer_filename);
                            string title = string.Format("title=\"{0}\"", buffer_filename);
                            string sFileImage = string.Format(HTML_IMAGE_ICON, PATH_FILE_TYPE_IMG_ROOT + objFileType.FileIcon, title, "24", "24");
                            fileName = string.Format(HTML_JS_LINK, "OnDownloadClick('" + HttpUtility.UrlEncode(s3Item.Key) + "');", sFileImage + "<br />" + buffer_filename);


                            // Action HTML
                            string actionHtml = string.Format( HTML_SELECT_ACTIONS_REVISION, HttpUtility.UrlEncode(s3Item.Key));

                            if (((AWSObjectItem)entry.Key).Metadata.IsCheckedOut)
                            {
                                actionHtml = ACTIONS_UNAVAILABLE;
                                checkedInOutButton = true;
                            }

                            // META Data
                            if (s3Item.Metadata != null)
                            {
                                // Classification
                                if (s3Item.Metadata.Classification != null && s3Item.Metadata.Classification.Any())
                                {
                                    if (s3Item.Metadata.Classification != null && s3Item.Metadata.Classification.Any())
                                    {
                                        bool blfirstClassification = true;
                                        foreach (var classificationItem in s3Item.Metadata.Classification)
                                        {
                                            if (!blfirstClassification) classification += ", ";
                                            classification += classificationItem;
                                            blfirstClassification = false;
                                        }
                                    }
                                }
                                if (s3Item.Metadata.Keywords != null)
                                {
                                    keywords = s3Item.Metadata.Keywords;
                                }
                                if (s3Item.Metadata.Description != null)
                                {
                                    description = s3Item.Metadata.Description;
                                }
                                if (s3Item.Metadata.UploadedBy != null)
                                {
                                    uploadedBy = s3Item.Metadata.UploadedBy;
                                }
                                if (s3Item.Metadata.UploadDate != null && s3Item.Metadata.UploadDate.Year != 1)
                                {
                                    uploadDate = s3Item.Metadata.UploadDate.ToString(CultureInfo.InvariantCulture);
                                }
                            }
                                   
                            int Revison = s3Item.Revision;
                            int isExist = lstMeataData.Where(x => x.key.Equals(s3Item.Key)).Count();
                            if (isExist == 0)
                            {
                                lstMeataData.Add(new DmsMetaData(s3Item.Key, fileName, classification, keywords,
                                                description, s3Item.ItemObject.Size.ToFileSize(),
                                            uploadedBy, Revison, uploadDate, checkedInOutButton, actionHtml, title));
                            }
                        }
                    }
                }
                IQueryable<DmsMetaData> iqueryItem = lstMeataData.Select(x => x).AsQueryable<DmsMetaData>();
                lstMeataData = Utility.OrderByDescending(iqueryItem, "Revison").ToList();

                foreach (DmsMetaData metadata in lstMeataData)
                {
                    string folder = "";
                    var arrayString = key.Split('/');
                    foreach (string str in arrayString)
                    {
                        if (str != arrayString.FirstOrDefault() && str != arrayString.LastOrDefault())
                        {
                            if (!String.IsNullOrEmpty(folder))
                            {
                                folder += " / ";
                            }

                            folder += str;
                        }
                    }
                        result.Add(new KeyValuePair<string, object[]>(metadata.key, new object[] 
                                            { metadata.fileName, metadata.classification, metadata.Keyword,
                                                metadata.description, metadata.Size,
                                                metadata.uploadedBy,metadata.Revison,
                                                metadata.uploadDate, metadata.checkedInOutButton,metadata.actionHtml, metadata.title,folder
                                                }));
                }
                return result;
            }
        }

        //remove non current version file by Kantorn J. 2012-08-25
        public static List<AWSObjectItem> GetCurrentVersionFile( List<AWSObjectItem> items,ref System.Collections.Hashtable hashFileRevision)
        {
            using (AmazonDMS.S3Client = Amazon.AWSClientFactory.CreateAmazonS3Client())
            {
                List<AWSObjectItem> result = new List<AWSObjectItem>();
                IQueryable<AWSObjectItem> iqueryItem = items.Select(x => x).AsQueryable<AWSObjectItem>();
                items = Utility.OrderBy(iqueryItem, "FileName").ToList();
                List<AWSObjectItem> listAWSitem = new List<AWSObjectItem>();
                System.Collections.Hashtable hashAllRevisonItem = new System.Collections.Hashtable();
                
                System.Collections.Hashtable bufferhashFileRevision = new System.Collections.Hashtable();
                items.ForEach(delegate(AWSObjectItem item)
                {
                    List<AWSObjectItem> bufferItems = Utility.OrderBy(iqueryItem, "FileName").ToList();
                    if (item.FileName.Split('.').Length > 1)
                    {
                        string fileRev = item.FileName.Split('.')[item.FileName.Split('.').Length - 1];
                        string itemFolder = "";
                        
                        var arrayString = item.Key.Split('/');
                        foreach (string str in arrayString)
                        {
                            if (str != arrayString.FirstOrDefault() && str != arrayString.LastOrDefault())
                            {
                                if (!String.IsNullOrEmpty(itemFolder))
                                {
                                    itemFolder += " / ";
                                }

                                itemFolder += str;
                            }
                        }

                        int intRev;
                        if (!int.TryParse(fileRev, out intRev))
                        {
                            listAWSitem = new List<AWSObjectItem>();
                            int maxRevision = 0;
                            bufferItems.ForEach(delegate(AWSObjectItem bufferItem)
                            {
                                if (bufferItem.FileName.Split('.').Length > 1)
                                {
                                    string bufffileRev = bufferItem.FileName.Split('.')[bufferItem.FileName.Split('.').Length - 1];
                                    int buffintRev;
                                    if (int.TryParse(bufffileRev, out buffintRev))
                                    {
                                        bool bIsSameFile = false;
                                        if ((bufferItem.FileName.Length - bufffileRev.Length - 1) > 0)
                                        {
                                            // add more condition for check folder
                                            string keyFolder = "";
                                            var keyArrayString = bufferItem.Key.Split('/');
                                            foreach (string str in keyArrayString)
                                            {
                                                if (str != keyArrayString.FirstOrDefault() && str != keyArrayString.LastOrDefault())
                                                {
                                                    if (!String.IsNullOrEmpty(keyFolder))
                                                    {
                                                        keyFolder += " / ";
                                                    }

                                                    keyFolder += str;
                                                }
                                            }
                                            if (item.FileName.ToLower().Equals(bufferItem.FileName.Substring(0, bufferItem.FileName.Length - bufffileRev.Length - 1).ToLower()) && itemFolder.ToLower().Equals(keyFolder.ToLower()))
                                            {
                                                bIsSameFile = true;
                                            }
                                        }
                                        if (bIsSameFile)
                                        {
                                            item.Revision = buffintRev == 0 ? 1 : (buffintRev + 1);
                                            if (buffintRev > maxRevision)
                                            {
                                                maxRevision = buffintRev;
                                            }
                                            bufferItem.Revision = buffintRev == 0 ? 1 : (buffintRev);
                                            listAWSitem.Add(bufferItem);
                                        }
                                    }
                                }
                            });
                            item.Revision =  maxRevision + 1;
                            result.Add(item);
                            if (listAWSitem.Count > 0)
                            {
                                listAWSitem = Utility.OrderByDescending(listAWSitem.AsQueryable(), "Revision").ToList();
                                bufferhashFileRevision.Add(item, listAWSitem);
                            }
                        }
                    }
                    else
                    {
                        listAWSitem = new List<AWSObjectItem>();
                        int maxRevision = 0;
                        bufferItems.ForEach(delegate(AWSObjectItem bufferItem)
                        {
                            if (bufferItem.FileName.Split('.').Length > 1)
                            {
                                string bufffileRev = bufferItem.FileName.Split('.')[bufferItem.FileName.Split('.').Length - 1];
                                int buffintRev;
                                if (int.TryParse(bufffileRev, out buffintRev))
                                {
                                    bool bIsSameFile = false;
                                    if ((bufferItem.FileName.Length - bufffileRev.Length - 1) > 0)
                                    {
                                        if (item.FileName.ToLower().Equals(bufferItem.FileName.Substring(0, bufferItem.FileName.Length - bufffileRev.Length - 1).ToLower()))
                                        {
                                            bIsSameFile = true;
                                        }
                                    }
                                    if (bIsSameFile)
                                    {
                                        item.Revision = buffintRev == 0 ? 1 : (buffintRev + 1);
                                        if (buffintRev > maxRevision)
                                        {
                                            maxRevision = buffintRev;
                                        }
                                        bufferItem.Revision = buffintRev == 0 ? 1 : (buffintRev);
                                        listAWSitem.Add(bufferItem);
                                    }
                                }
                            }
                        });
                        item.Revision = maxRevision + 1;
                        result.Add(item);
                        if (listAWSitem.Count > 0)
                        {
                            listAWSitem = Utility.OrderByDescending(listAWSitem.AsQueryable(), "Revision").ToList();
                            bufferhashFileRevision.Add(item, listAWSitem);
                        }
                    }
                });
              
                hashFileRevision = bufferhashFileRevision;
                foreach (System.Collections.DictionaryEntry entry in bufferhashFileRevision)
                {
                    if (hashAllRevisonItem.ContainsKey(entry.Key))
                    {
                        hashAllRevisonItem.Remove(entry.Key);
                    }
                    hashAllRevisonItem.Add(entry.Key, entry.Value);
                }
                HttpContext.Current.Session["hashAllRevisonItem"] = hashAllRevisonItem;
                return result;
            }
        }

        /// <summary>
        /// Load Item META
        /// </summary>
        /// <param name="key">string</param>
        /// <param name="result">List</param>
        /// <param name="loadActionsHtml">bool</param>
        /// <returns>IEnumerable</returns>
        private static IEnumerable<KeyValuePair<string, object[]>> LoadItemMeta(string key, List<KeyValuePair<string, object[]>> result, bool loadActionsHtml = false, bool loadRevision = false, bool ispseodu = false)
        {
            // Get Object META Data
            using (AmazonDMS.S3Client = Amazon.AWSClientFactory.CreateAmazonS3Client())
            {
                //if (!ispseodu)
                //{
                    // Get Object META
                    AWSObjectMetadata metaDelayLoad = AmazonDMS.GetObjectMetadata(key);
                    // Get META
                    if (metaDelayLoad != null)
                    {

                        // Action HTML
                        string actionHtml = "";


                        if (loadActionsHtml)
                        {
                            //check action of revision or not? Kantorn J. 2012-08-17
                            if (loadRevision)
                            {
                                actionHtml = string.Format(HTML_SELECT_ACTIONS_REVISION, HttpUtility.UrlEncode(key));
                            }
                            else
                            {
                                actionHtml = string.Format(_userHasAdministrationPrivileges ? HTML_SELECT_ACTIONS : HTML_SELECT_ACTIONS_NO_DELETE, HttpUtility.UrlEncode(key));
                            }
                        }

                        // Var
                        string classificationDelayLoad = "",
                               fileTypeDelayLoad = "",
                               descriptionDelayLoad = "",
                               keywordsDelayLoad = "",
                               uploadedByDelayLoad = "",
                               checkedInOutButtonDelayLoad = "";

                        string buffer_filename = string.Empty;

                        if (key.Split('/').Length > 1)
                        {
                            buffer_filename = key.Split('/')[key.Split('/').Length - 1];
                        }
                        else
                        {
                            buffer_filename = key;
                        }
                        string title = string.Format("\"{0}\"", buffer_filename);

                        DateTime uploadDateDelayLoad;
                        bool actionsUnavailable = false;

                        // Classification
                        if (metaDelayLoad.Classification != null && metaDelayLoad.Classification.Any())
                        {
                            bool blfirstClassification = true;
                            foreach (var classificationItem in metaDelayLoad.Classification)
                            {
                                if (!blfirstClassification) classificationDelayLoad += ", ";
                                classificationDelayLoad += classificationItem;
                                blfirstClassification = false;
                            }
                        }

                        // File Type
                        if (!string.IsNullOrEmpty(metaDelayLoad.FileType))
                            fileTypeDelayLoad = metaDelayLoad.FileType;

                        // Description
                        if (!string.IsNullOrEmpty(metaDelayLoad.Description))
                            descriptionDelayLoad = metaDelayLoad.Description;

                        // Keywords
                        if (!string.IsNullOrEmpty(metaDelayLoad.Keywords))
                            keywordsDelayLoad = metaDelayLoad.Keywords;

                        // Uploaded By
                        if (!string.IsNullOrEmpty(metaDelayLoad.UploadedBy))
                            uploadedByDelayLoad = metaDelayLoad.UploadedBy;

                        // Upload Date
                        if (!DateTime.TryParse(metaDelayLoad.UploadDate.ToString(CultureInfo.InvariantCulture), out uploadDateDelayLoad)) uploadDateDelayLoad = DateTime.UtcNow;
                        string uploadDateDelayLoadAsString = uploadDateDelayLoad == DateTime.MinValue ? "" : uploadDateDelayLoad.ToString(CultureInfo.InvariantCulture);

                        // Checked In / Out?
                        if (metaDelayLoad.IsCheckedOut)
                        {
                            actionsUnavailable = true;

                            if (HttpContext.Current.User.Identity.Name == metaDelayLoad.CheckedOutBy ||
                                HttpContext.Current.User.IsInRole("Administrator"))
                            {
                                checkedInOutButtonDelayLoad = string.Format(HTML_JS_LINK, "OnCheckInOutClick('" + HttpUtility.UrlEncode(key) + "|false" + "', this)",
                                                        string.Format(HTML_IMAGE, PATH_LOCK_IMG,
                                                        string.Format("title=\"{0}\"", "Checked out by " + metaDelayLoad.CheckedOutBy + " on " + metaDelayLoad.CheckOutDate.ToLocalTime()))
                                                        );
                            }
                            else
                            {
                                checkedInOutButtonDelayLoad = string.Format(HTML_JS_LINK, "void(0)",
                                                        string.Format(HTML_IMAGE, PATH_LOCK_IMG,
                                                        string.Format("title=\"{0}\"", "[Check in not allow.]Checked out by " + metaDelayLoad.CheckedOutBy + " on " + metaDelayLoad.CheckOutDate.ToLocalTime()))
                                                        );
                            }
                        }
                        else
                        {
                            checkedInOutButtonDelayLoad = string.Format(HTML_JS_LINK, "OnCheckInOutClick('" + HttpUtility.UrlEncode(key) + "|true" + "', this)",
                                                        string.Format(HTML_IMAGE, PATH_UNLOCK_IMG, "title=\"Check Out File\"")
                                                        );
                        }
                        //>

                        ///Set action for revision file
                        string actionRevHtml = HTML_SELECT_ACTIONS_REVISION;
                        string folder = "";
                        if (ispseodu)
                        {
                            var arrayString = key.Split('/');
                            foreach (string str in arrayString)
                            {
                                if (str != arrayString.FirstOrDefault() && str != arrayString.LastOrDefault())
                                {
                                    if (!String.IsNullOrEmpty(folder))
                                    {
                                        folder += " / ";
                                    }

                                    folder += str;
                                }
                            }
                        }

                        // Build & Return Result
                        result.Add(new KeyValuePair<string, object[]>(key, new object[]
                                                                                    {
                                                                                        classificationDelayLoad,
                                                                                        fileTypeDelayLoad,
                                                                                        keywordsDelayLoad,
                                                                                        descriptionDelayLoad,
                                                                                        uploadedByDelayLoad,
                                                                                        uploadDateDelayLoadAsString,
                                                                                        checkedInOutButtonDelayLoad,
                                                                                        actionsUnavailable,
                                                                                        actionHtml,
                                                                                        actionRevHtml,
                                                                                        title,
                                                                                        folder
                                                                                    }));
                    }
            }
            return result;
        }
        //-->

        /// <summary>
        /// Check Item Out or In
        /// </summary>
        /// <param name="basePath">string</param>
        /// <param name="key">key</param>
        /// <param name="sIsCheckOut">string</param>
        /// <param name="result">List</param>
        /// <returns>IEnumerable</returns>
        private static IEnumerable<KeyValuePair<string, object[]>> CheckItemOutOrIn(string basePath, string key, string sIsCheckOut, List<KeyValuePair<string, object[]>> result, bool isGetAllDocument)
        {
            // Get Current Object
            var objectSetCheckout = AmazonDMS.GetObjects(basePath, null, isGetAll: true);
            var queryItemsCheckout = objectSetCheckout.S3Objects.Where(a => a.Type != AWSObjectItemType.Folder).ToList();
            var editObjectCheckout = queryItemsCheckout.FirstOrDefault(o => o.Key == key);

            // Setup Var
            string fileType = "", description = "", keywords = "", uploadedBy = "";
            List<string> classification = new List<string>();
            DateTime uploadDate = new DateTime();

            // Get Object META Data
            AWSObjectMetadata metaCheckedInOut;
            using (AmazonDMS.S3Client = Amazon.AWSClientFactory.CreateAmazonS3Client())
            {
                // Get Object META
                metaCheckedInOut = AmazonDMS.GetObjectMetadata(key);
            }

            // Get META
            if (editObjectCheckout != null && metaCheckedInOut != null)
            {
                // Classification
                if (metaCheckedInOut.Classification != null && metaCheckedInOut.Classification.Any())
                    classification = metaCheckedInOut.Classification;

                // File Type
                if (!string.IsNullOrEmpty(metaCheckedInOut.FileType))
                    fileType = metaCheckedInOut.FileType;

                // Description
                if (!string.IsNullOrEmpty(metaCheckedInOut.Description))
                    description = metaCheckedInOut.Description;

                // Keywords
                if (!string.IsNullOrEmpty(metaCheckedInOut.Keywords))
                    keywords = metaCheckedInOut.Keywords;

                // Uploaded By
                if (!string.IsNullOrEmpty(metaCheckedInOut.UploadedBy))
                    uploadedBy = metaCheckedInOut.UploadedBy;

                // Upload Date
                if (!DateTime.TryParse(metaCheckedInOut.UploadDate.ToString(CultureInfo.InvariantCulture), out uploadDate))
                    uploadDate = DateTime.UtcNow;
            }

            // Check it!
            AmazonDMS.CheckInOutObject(key, sIsCheckOut, HttpContext.Current.User.Identity.Name,
                                classification, fileType, description, keywords, uploadedBy, uploadDate.ToString(CultureInfo.InvariantCulture));

            //Update session if IsLock status change
            if (HttpContext.Current.Session["hashAllRevisonItem"] != null)
            {
                System.Collections.Hashtable hashAllRevisonItem = new System.Collections.Hashtable();
                System.Collections.Hashtable bufferhashAllRevisonItem = new System.Collections.Hashtable();
                hashAllRevisonItem = (System.Collections.Hashtable)HttpContext.Current.Session["hashAllRevisonItem"];
                foreach (System.Collections.DictionaryEntry entry in hashAllRevisonItem)
                {
                    if (((AWSObjectItem)entry.Key).Key == key)
                    {
                        AWSObjectItem item;
                        //Update metadata 
                        item = (AWSObjectItem)entry.Key;
                        item.Key = key;
                        if (item.Metadata == null)
                        {
                            item.Metadata = new AWSObjectMetadata();
                        }
                        item.Metadata.FileType = fileType;
                        if (item.Metadata.Classification == null)
                        {
                            item.Metadata.Classification = new List<string>();
                        }
                        item.Metadata.Classification.AddRange(classification);
                        item.Metadata.Description = description;
                        item.Metadata.Keywords = keywords;
                        item.Metadata.UploadedBy = uploadedBy;
                        item.Metadata.UploadDate = uploadDate;
                        item.Metadata.IsCheckedOut = sIsCheckOut == "CheckOut" ? true : false;
                        bufferhashAllRevisonItem.Add(item, (List<AWSObjectItem>)entry.Value);

                        //update list for thread
                        Hashtable hashQueryStaticItems ;
                        if (HttpContext.Current.Session["queryStaticItems"] != null)
                        {
                            hashQueryStaticItems = (Hashtable)HttpContext.Current.Session["queryStaticItems"];
                        }
                        else
                        {
                            hashQueryStaticItems = new Hashtable();
                        }
                        List<AWSObjectItem> list = null; ;
                        if (hashQueryStaticItems.ContainsKey(basePath))
                        {
                            list = (List<AWSObjectItem>)hashQueryStaticItems[basePath];
                        }
                        if (list != null)
                        {
                            list.Remove((AWSObjectItem)entry.Key);
                            list.Add(item);
                            hashQueryStaticItems[basePath] = list;
                            HttpContext.Current.Session["queryStaticItems"] = hashQueryStaticItems;
                        }
                    }
                    else
                    {
                        bufferhashAllRevisonItem.Add((AWSObjectItem)entry.Key, (List<AWSObjectItem>)entry.Value);
                    }
                }
                HttpContext.Current.Session["hashAllRevisonItem"] = bufferhashAllRevisonItem;
            }

            // Load and return META
            return LoadItemMeta(key, result, true,false, isGetAllDocument);
        }
        //-->

        /// <summary>
        /// Get Base Path From Hash
        /// </summary>
        /// <returns></returns>
        private static string GetBasePathFromHash(string fragPath)
        {
            if (HttpContext.Current.Request.UrlReferrer != null)
            {
                string basePath = fragPath.Replace("#/", "");
                if (basePath.Length > 1)
                {
                    if (basePath.EndsWith("/")) basePath = basePath.Substring(0, basePath.Length - 1);
                }
                return basePath;
            }
            return null;
        }
        //-->

        protected void _OnRunComplete(string basePath,AWSObjectItem aws, string rowClass)
        {
            //Change from static member to session by Kantorn J, 2012-08-24
            Hashtable hashQueryStaticItems = (Hashtable)HttpContext.Current.Session["queryStaticItems"];
            if (hashQueryStaticItems == null)
            {
                hashQueryStaticItems = new Hashtable();
            }
            if (!hashQueryStaticItems.ContainsKey(basePath))
            {
                hashQueryStaticItems.Add(basePath, new List<AWSObjectItem>());
            }
            List<AWSObjectItem> list = (List<AWSObjectItem>)hashQueryStaticItems[basePath];
            list.Add(aws);
            hashQueryStaticItems[basePath] = list;

            // update other basePath
            HttpContext.Current.Session["queryStaticItems"] = hashQueryStaticItems;
        }

        //load meta data with thread for DMSMetadataHandler.cs by Kantorn J. 
        public static AWSObjectMetadata LoadThreadMetaData(string key)
        {
            AWSObjectMetadata metaDelayLoad = new AWSObjectMetadata();
            using (AmazonDMS.S3Client = Amazon.AWSClientFactory.CreateAmazonS3Client())
            {
                // Get Object META
                metaDelayLoad = AmazonDMS.GetObjectMetadata(key);
            }
            return metaDelayLoad;
        }

        public static void RemoveOtherSession(string deleteKey)
        {
            Hashtable hashQueryStaticItems = (Hashtable)HttpContext.Current.Session["queryStaticItems"];
            Hashtable tmpHash = new Hashtable();
            Hashtable tmpRevHash = new Hashtable();
            List<AWSObjectItem> queryStaticItems = new List<AWSObjectItem>();

            // remove rev
            System.Collections.Hashtable hashAllRevisonItem;

            if (HttpContext.Current.Session["hashAllRevisonItem"] != null)
            {
                hashAllRevisonItem = (System.Collections.Hashtable)HttpContext.Current.Session["hashAllRevisonItem"];
            }
            else
            {
                hashAllRevisonItem = new System.Collections.Hashtable();
            }

            foreach (System.Collections.DictionaryEntry entry in hashAllRevisonItem)
            {
                if (!((AWSObjectItem)entry.Key).Key.Contains(deleteKey))
                {
                    tmpRevHash.Add(entry.Key,entry.Value);
                }
            }

            HttpContext.Current.Session["hashAllRevisonItem"] = tmpRevHash;

            // remove queryStaticItems
            foreach (string s in hashQueryStaticItems.Keys)
            {
                tmpHash.Add(s, new List<AWSObjectItem>());
            }

            foreach (string s in hashQueryStaticItems.Keys)
            {
                List<AWSObjectItem> list = (List<AWSObjectItem>)hashQueryStaticItems[s];
                if (list != null)
                {
                    var item = from i in list
                               where !(i.Key.Contains(deleteKey))
                               select i;
                    if (item.Count() == 0)
                    {
                        tmpHash.Remove(s);
                    }
                    else
                    {
                        tmpHash[s] = item.ToList();
                    }
                }
            }

            HttpContext.Current.Session["queryStaticItems"] = tmpHash;
        }

        public static AWSResultSet GetAllDocument(string key)
        {
            AWSResultSet returnResult = new AWSResultSet();
            returnResult.S3Objects = new List<AWSObjectItem>();

            AWSResultSet tmpObj = AmazonDMS.GetObjects(key, null, isGetAll: true);
            returnResult.CurrentPage = tmpObj.CurrentPage;
            returnResult.IsPartialResult = tmpObj.IsPartialResult;
            returnResult.NextMarker = tmpObj.NextMarker;
            returnResult.PreviousMarker = tmpObj.PreviousMarker;
            
            foreach (AWSObjectItem obj in tmpObj.S3Objects)
            {
                if (obj.Type == AWSObjectItemType.Folder)
                {
                    returnResult.S3Objects.AddRange(GetAllDocument(obj.Key).S3Objects);
                }
                else
                {
                    returnResult.S3Objects.Add(obj);
                }
            }

            return returnResult;
        }

        /// <summary>
        /// Check Exist
        /// </summary>
        /// <param name="key">string</param>
        /// <returns>IEnumerable</returns>
        private static IEnumerable<KeyValuePair<string, object[]>> CheckExist(string key)
        {
            bool isExist = true;
            if (!AmazonDMS.CheckObjectExist(key))
            {
                isExist = false;
            }

            // Build & Return Result
            result.Add(new KeyValuePair<string, object[]>(key, new object[]
                                                        {
                                                            isExist.ToString().ToLower()
                                                        }));
            return result;
        }
        //-->

        /// <summary>
        /// Create new folder
        /// </summary>
        /// <param name="key">string</param>
        /// <returns>IEnumerable</returns>
        private static IEnumerable<KeyValuePair<string, object[]>> CreateNewFolder(string key)
        {
            bool isExist = false;
            if (!AmazonDMS.CheckObjectExist(key))
            {
                AmazonDMS.CreateNewFolder(key);
                isExist = true;
            }

            // Build & Return Result
            result.Add(new KeyValuePair<string, object[]>(key, new object[]
                                                        {
                                                            isExist.ToString().ToLower()
                                                        }));
            return result;
        }
        //-->
    }
}