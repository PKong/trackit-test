﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TrackIT2.DAL;
using System.Web.Script.Serialization;

namespace TrackIT2.Handlers
{
    public class StateAutoCompleteHandler : IHttpHandler
    {
        #region IHttpHandler Members

        public bool IsReusable
        {
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                using (CPTTEntities ce = new CPTTEntities())
                {
                    String strQuery = context.Request.QueryString["term"];
                    IList<string> result = new List<string>();
                    var objStates = (from c in ce.sites
                                     where c.state.StartsWith(strQuery)
                                     select c.state
                                  ).Distinct().OrderBy(l => l);
                    foreach (var state in objStates)
                    {
                        result.Add(state);
                    }
                    JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                    string names = javaScriptSerializer.Serialize(result);
                    context.Response.ContentType = "text/html";
                    context.Response.Write(names);
                }
                
            }
            catch
            {
                context.Response.Write("");
            }
        }

        #endregion
    }
}

