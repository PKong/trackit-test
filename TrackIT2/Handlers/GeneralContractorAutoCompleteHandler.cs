﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TrackIT2.BLL;
using TrackIT2.DAL;
using System.Web.Script.Serialization;

namespace TrackIT2.Handlers
{
    public class GeneralContractorAutoCompleteHandler :IHttpHandler 
    {
        public bool IsReusable
        {
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                using (CPTTEntities ce = new CPTTEntities())
                {
                    var contractors = ce.general_contractors.Where("it.general_contractor_name LIKE '" + context.Request.QueryString["term"] + "%'").OrderBy("it.general_contractor_name");
                    // Convert Result
                    IList<string> result = new List<string>();
                    foreach (var c in contractors)
                    {
                        result.Add(c.general_contractor_name+ " ");
                    }
                    JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                    string ids = javaScriptSerializer.Serialize(result);
                    context.Response.ContentType = "text/html";
                    context.Response.Write(ids);
                }
            }
            catch
            {
                context.Response.Write("");
            }
        }
    }
}