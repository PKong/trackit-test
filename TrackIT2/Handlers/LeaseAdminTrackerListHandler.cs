﻿using System;
using System.Data.Objects;
using System.Globalization;
using System.Web;
using System.Collections.Generic;
using FlexigridASPNET;
using TrackIT2.BLL;
using TrackIT2.DAL;
using System.Linq;
using System.Collections.Specialized;
using System.Text;

namespace TrackIT2.Handlers
{
    public class LeaseAdminTrackerListHandler : BaseFlexiHandler
    {
        // ****** Var
        public CPTTEntities Ce = new CPTTEntities();
        private static bool _subFilterHasNoResults = false;
        public static AdvanceSearch.LeaseAdminFilter objAdvSearch = new AdvanceSearch.LeaseAdminFilter();
        public const string LINK_SITE = "{0}";
        public const string REVENUE_SHARE = "<a ><img alt='Revenue Share' title='Revenue Share' src='/images/icons/{0}'></a>";
        public const string REVENUE_SHARE_YES = "Apply.png";
        public const string REVENUE_SHARE_NO = "Delete.png";

        /// <summary>
        /// Gets the filtered data.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<KeyValuePair<string, object[]>> GetFilteredData()
        {
           var result = new List<KeyValuePair<string, object[]>>();
           List<LeaseAdminList> records;
           const int startIndex = 6;
           var ce = new CPTTEntities();
           var skipItems = (Page - 1) * this.ItemsPerPage;
           var sortName = !String.IsNullOrWhiteSpace(this.SortName) ? this.SortName : "site_uid";
           var form = HttpContext.Current.Request.Form;
           var parameters = new NameValueCollection();
           DefaultFilterSession filterSession = new DefaultFilterSession();
           string filterString = string.Empty;

           // If we have form values, extract them to build the object query.
           if (form.Count > startIndex)
           {
              for (var i = startIndex; i < form.Count; i++)
              {
                 var itemKey = form.GetKey(i);
                 var itemValue = form.Get(i);

                 // If the search came from the global search bar, a generic 
                 // "gs" item key is specified and needs to be changed.
                 if (itemKey == "gs")
                 {
                     itemKey = "General";
                 }

                 parameters.Add(itemKey, itemValue);

                 if (itemKey == DefaultFilterSession.SessionName)
                 {
                     filterString = itemValue;
                 }
              }
           }

           // Check session filter
           parameters.Remove(DefaultFilterSession.ApplyClick);
           parameters.Remove(DefaultFilterSession.SessionName);
           DefaultFilterSession.GetOrSetFilterSession(HttpContext.Current, ref filterSession, filterString, DefaultFilterSession.FilterType.LeaseAdminFilter);

           // Build ObjectQuery         
           ObjectQuery<lease_admin_records> recordQuery = ce.lease_admin_records;
           var searchCriteria = LeaseAdminRecord.GetSearchCriteria();
           recordQuery = Utility.BuildObjectQuery(recordQuery, parameters, searchCriteria);

           // Since sites are linked through a many to many relationship that the
           // object query cannot load together, we do one final filter manually.
           var filteredList = recordQuery.ToList();

           if (parameters.AllKeys.Contains("SiteID"))
           {
              // Reinitialize and build based off of filter.
              filteredList = new List<lease_admin_records>();

              var recordList = recordQuery.ToList();
              var parameterValue = parameters["SiteID"];

              foreach (var record in recordList)
              {
                 if (!record.sites.Any()) continue;

                 if (record.sites.First().site_uid.ToUpper()
                           .Contains(parameterValue.ToUpper()))
                 {
                    filteredList.Add(record);
                 }
              }
           }           

           // Execute query and get count.
           this.VitualItemsCount = filteredList.Count();

           // Sort Order
           if (VitualItemsCount > 0)
           {
              // Get additional details about lease admin record (site, etc).
              var leaseAdminObj = GenNewLeaseAdminRecord(filteredList.AsQueryable(), false);              
              var iqLeaseAdmin = leaseAdminObj.AsQueryable();

              if (this.SortDirection == GridSortOrder.Asc)
              {
                 var a = Utility.OrderBy(iqLeaseAdmin, sortName);
                 records = a.Skip(skipItems).Take(this.ItemsPerPage).ToList();
              }
              else
              {
                 var b = Utility.OrderByDescending(iqLeaseAdmin, sortName);
                 records = b.Skip(skipItems).Take(this.ItemsPerPage).ToList();
              }
              
              // Convert Result         
              foreach (var leaseAdmin in records)
              {
                 var executionDate = "";
                 var commencementDate = "";
                 var sSiteUid = "";
                 var sSiteName = "";

                 if (leaseAdmin.SiteId != null)
                 {
                    if (leaseAdmin.SiteId.IndexOf("Sites") > 0)
                    {
                       sSiteUid = String.Format(LINK_SITE, leaseAdmin.SiteId);
                       sSiteName = String.Format(LINK_SITE, leaseAdmin.SiteName);
                    }
                    else
                    {
                       sSiteUid = leaseAdmin.SiteId;
                       sSiteName = leaseAdmin.SiteName;
                    }
                 }
                 else
                 {
                    sSiteUid = String.Empty;
                    sSiteName = String.Empty;
                 }

                 executionDate = Utility.DateToString(leaseAdmin.LeaseExecutionDate);
                 commencementDate = Utility.DateToString(leaseAdmin.CommencementDate);
                 // Build Result
                 result.Add(new KeyValuePair<string, object[]>(leaseAdmin.id.ToString(CultureInfo.InvariantCulture),
                                                                 new object[] { 
                                                                    leaseAdmin.CustomerName,
                                                                    sSiteUid,
                                                                    sSiteName,
                                                                    leaseAdmin.Phase,
                                                                    executionDate,
                                                                    commencementDate,
                                                                    leaseAdmin.RevShare,
                                                                    leaseAdmin.LeaseAdminExecutionDocumentType,
                                                                    leaseAdmin.id.ToString(CultureInfo.InvariantCulture) }));
              }
           }
            
           return result;
        }

        public static List<LeaseAdminList> GenNewLeaseAdminRecord(IQueryable<lease_admin_records> iqLeaseAdmin, bool isExport)
        {
            var returnObj = new List<LeaseAdminList>();

            foreach (lease_admin_records data in iqLeaseAdmin)
            {
                var inObj = new LeaseAdminList();
                var siteData = GenSiteData(data);
                DateTime? LeaseExecuteDateValue = data.lease_application_id.HasValue ? data.lease_applications.lease_execution_date : data.lease_execution_date;
                inObj.id = data.id;
                inObj.SiteId = siteData["site_uid"];
                inObj.SiteName = siteData["site_name"];
                inObj.CustomerName = data.lease_application_id.HasValue ? data.lease_applications.customer == null ? string.Empty : data.lease_applications.customer.customer_name : String.Empty;
                if (inObj.CustomerName == String.Empty)
                {
                    inObj.CustomerName = data.customer == null ? string.Empty : data.customer.customer_name;
                }
                inObj.Phase = GetPhase(data);
                inObj.LeaseExecutionDate = LeaseExecuteDateValue;
                inObj.CommencementDate = data.commencement_date;

                inObj.RevShare = String.Empty;
                if (!isExport)
                {
                    if (data.lease_application_id.HasValue)
                    {
                        if (data.lease_applications.revenue_share_yn.HasValue)
                        {
                            if (data.lease_applications.revenue_share_yn.Value)
                            {
                                inObj.RevShare = String.Format(REVENUE_SHARE, REVENUE_SHARE_YES);
                            }
                            else
                            {
                                inObj.RevShare = String.Format(REVENUE_SHARE, REVENUE_SHARE_NO);
                            }
                        }
                    }
                    else if (data.revenue_share_yn.HasValue)
                    {
                        if (data.revenue_share_yn.Value)
                        {
                            inObj.RevShare = String.Format(REVENUE_SHARE, REVENUE_SHARE_YES);
                        }
                        else
                        {
                            inObj.RevShare = String.Format(REVENUE_SHARE, REVENUE_SHARE_NO);
                        }
                    }
                }
                else
                {
                    if (data.lease_application_id.HasValue)
                    {
                        if (data.lease_applications.revenue_share_yn != null)
                        {
                            inObj.RevShare = data.lease_applications.revenue_share_yn.Value.ToString().ToUpper();
                        }
                    }
                    else if (data.revenue_share_yn != null)
                    {
                        if (data.revenue_share_yn.Value)
                        {
                            inObj.RevShare = data.revenue_share_yn.Value.ToString().ToUpper();
                        }
                    }
                }

                inObj.LeaseAdminExecutionDocumentType = String.Empty;
                if (data.lease_admin_execution_document_type != null)
                {
                    inObj.LeaseAdminExecutionDocumentType = data.lease_admin_execution_document_type.document_type;
                }

                returnObj.Add(inObj);
            }

            return returnObj;
        }

        public static NameValueCollection GenSiteData(lease_admin_records LeaseAdmin)
        {
            var siteData = new NameValueCollection();

            if (LeaseAdmin.lease_applications != null)
                {
                    if (LeaseAdmin.lease_applications.site != null)
                    {
                        if (LeaseAdmin.lease_applications.site.site_uid != "")
                        {
                            siteData.Add("site_uid", LeaseAdmin.lease_applications.site.site_uid);
                            siteData.Add("site_name", LeaseAdmin.lease_applications.site.site_name);
                        }
                    }
                }
                else
                {
                    var lstLeaseAdminSite = LeaseAdmin.sites.ToList();
                    lstLeaseAdminSite = LeaseAdmin.sites.ToList();

                    if (lstLeaseAdminSite.Count > 1)
                    {
                        siteData.Add("site_uid", lstLeaseAdminSite.Count + " Sites");
                        siteData.Add("site_name", lstLeaseAdminSite.Count + " Sites");
                    }
                    else if (lstLeaseAdminSite.Count > 0)
                    {
                        siteData.Add("site_uid", lstLeaseAdminSite.FirstOrDefault().site_uid);
                        siteData.Add("site_name", lstLeaseAdminSite.FirstOrDefault().site_name);
                    }
            }

            return siteData;
        }
       
        public static string GetPhase(lease_admin_records LeaseAdmin)
        {
            using (CPTTEntities ce = new CPTTEntities())
            {
                string returnResult = "Execution";
                if (LeaseAdmin.lease_application_id.HasValue)
                {
                    if (LeaseAdmin.lease_applications.lease_execution_date != null && LeaseAdmin.lease_applications.lease_execution_date_status_id.HasValue)
                    {
                        var datestat = ce.date_statuses.Where(x => x.id.Equals(LeaseAdmin.lease_applications.lease_execution_date_status_id.Value) && x.date_status == "Actual");
                        if (datestat.ToList().Count > 0)
                        {
                            returnResult = "Commencement";
                        }
                    }
                }
                else if (LeaseAdmin.lease_execution_date != null && LeaseAdmin.lease_execution_date_status_id.HasValue)
                {
                    var datestat = ce.date_statuses.Where(x => x.id.Equals(LeaseAdmin.lease_execution_date_status_id.Value) && x.date_status == "Actual" );
                    if (datestat.ToList().Count > 0)
                    {
                        returnResult = "Commencement";
                    }
                }
                return returnResult;
            }
        }
        public static IQueryable<lease_admin_records> FilterSiteId(IQueryable<lease_admin_records> iqLeaseAdmin, String searchId)
        {
            List<lease_admin_records> returnObj = new List<lease_admin_records>();

            foreach (var leaseAdmin in iqLeaseAdmin.ToList())
            {
                if (leaseAdmin.lease_applications != null)
                {
                    if (leaseAdmin.lease_applications.site != null)
                    {
                        if (searchId == leaseAdmin.lease_applications.site.site_uid)
                        {
                            returnObj.Add(leaseAdmin);
                        }
                    }
                }
                else
                {
                    List<site> lstLeaseAdminSite = leaseAdmin.sites.ToList<site>();
                    lstLeaseAdminSite = leaseAdmin.sites.ToList<site>();
                    foreach (var site in lstLeaseAdminSite)
                    {
                        if (searchId == site.site_uid)
                        {
                            returnObj.Add(leaseAdmin);
                            break;
                        }
                    }
                }
            }
            searchId = "";
            return returnObj.AsQueryable();
        }        
    }    
}