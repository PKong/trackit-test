﻿using FlexigridASPNET;
using System;
using System.Collections;
using System.Data.Objects;
using System.Web;
using System.Collections.Generic;
using TrackIT2.BLL;
using TrackIT2.DAL;
using System.Linq;
using System.Collections.Specialized;

namespace TrackIT2.Handlers
{
   /// <summary>
   /// SitesListHandler: Returns a list of Sites for binding to a Flexigrid
   /// </summary>
   public class SitesListHandler : BaseFlexiHandler
   {
         /// <summary>
         /// Gets the filtered data.
         /// </summary>
         /// <returns></returns>
         public override IEnumerable<KeyValuePair<string, object[]>> GetFilteredData()
         {
            var result = new List<KeyValuePair<string, object[]>>();
            List<site> sites;
            const int startIndex = 6;
            var ce = new CPTTEntities();
            var skipItems = (Page - 1) * this.ItemsPerPage;
            var sortName = !String.IsNullOrWhiteSpace(this.SortName) ? this.SortName : "site_uid";
            var form = HttpContext.Current.Request.Form;
            var parameters = new NameValueCollection();
            DefaultFilterSession filterSession = new DefaultFilterSession();
            string filterString = string.Empty;

            // If we have form values, extract them to build the object query.
            if (form.Count > startIndex)
            {
               for (var i = startIndex; i < form.Count; i++)
               {
                  var itemKey = form.GetKey(i);
                  var itemValue = form.Get(i);

                  // If the search came from the global search bar, a generic 
                  // "gs" item key is specified and needs to be changed.
                  if (itemKey == "gs")
                  {
                      itemKey = "General";
                  }

                  if (itemKey == "State")
                  {
                      itemValue = Site.GetStateShortName(itemValue);
                  }

                  parameters.Add(itemKey, itemValue);

                  if (itemKey == DefaultFilterSession.SessionName)
                  {
                      filterString = itemValue;
                  }
               }
            }

            // Check session filter
            parameters.Remove(DefaultFilterSession.ApplyClick);
            parameters.Remove(DefaultFilterSession.SessionName);
            DefaultFilterSession.GetOrSetFilterSession(HttpContext.Current, ref filterSession, filterString, DefaultFilterSession.FilterType.SitesFilter);

         // Build ObjectQuery         
         ObjectQuery<site> siteQuery = ce.sites;
         var searchCriteria = Site.GetSearchCriteria();         
         siteQuery = Utility.BuildObjectQuery(siteQuery, parameters, searchCriteria);

         // Execute query and get count.
         this.VitualItemsCount = siteQuery.Count();

         // Sort Order
         if (VitualItemsCount > 0)
         {
            if (this.SortDirection == GridSortOrder.Asc)
            {
               var a = Utility.OrderBy(siteQuery, sortName);
               sites = a.Skip(skipItems).Take(this.ItemsPerPage).ToList();
            }
            else
            {
               var b = Utility.OrderByDescending(siteQuery, sortName);
               sites = b.Skip(skipItems).Take(this.ItemsPerPage).ToList();
            }
            //>

            // Convert Result         
            foreach (var site in sites)
               result.Add(new KeyValuePair<string, object[]>
                              (site.site_uid.ToString(), 
                                 new object[] 
                                    { 
                                    site.site_uid,
                                    site.site_name,
                                    site.market_name,
                                    site.site_class_desc,
                                    site.site_status_desc,
                                    Site.HasTowerModifications(site.site_uid).ToString(),
                                    site.rogue_equipment_yn.ToString(),
                                    site.site_uid.ToString() 
                                 }
                              )
                           );
         }        

            return result;
      }       
   }
}