﻿using FlexigridASPNET;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.Objects;
using System.Linq;
using System.Web;
using TrackIT2.BLL;
using TrackIT2.DAL;
using TrackIT2.Objects;

namespace TrackIT2.Handlers
{    
   /// <summary>
   /// TowerModificationsListHandler: Returns a list of Tower Modifications for binding to a Flexigrid
   /// </summary>
   public class TowerModificationsListHandler : BaseFlexiHandler
   {
      /// <summary>
      /// Gets the filtered data.
      /// </summary>
      /// <returns></returns>
      public override IEnumerable<KeyValuePair<string, object[]>> GetFilteredData()
      {
         var result = new List<KeyValuePair<string, object[]>>();
         const int startIndex = 6;
         var lstExTobj = new List<ExtendTowerModificaionInfo>();
         var ce = new CPTTEntities();
         var skipItems = (Page - 1) * this.ItemsPerPage;
         var form = HttpContext.Current.Request.Form;
         var parameters = new NameValueCollection();
         DefaultFilterSession filterSession = new DefaultFilterSession();
         string filterString = string.Empty;

         // Set sort name if it exists.
         var sortName = !String.IsNullOrWhiteSpace(this.SortName) ?
                        this.SortName : "site_uid";

         // If there are form values, extract them to build the object query.
         if (form.Count > startIndex)
         {
            for (var i = startIndex; i < form.Count; i++)
            {
               var itemKey = form.GetKey(i);
               var itemValue = form.Get(i);

               // If the search came from the global search bar, a generic 
               // "gs" item key is specified and needs to be changed.
               if (itemKey == "gs")
               {
                   itemKey = "General";
               }

               parameters.Add(itemKey, itemValue);

               if (itemKey == DefaultFilterSession.SessionName)
               {
                   filterString = itemValue;
               }
            }
         }

         // Check session filter
         parameters.Remove(DefaultFilterSession.ApplyClick);
         parameters.Remove(DefaultFilterSession.SessionName);
         DefaultFilterSession.GetOrSetFilterSession(HttpContext.Current, ref filterSession, filterString, DefaultFilterSession.FilterType.TowerModFilter);

         // Build ObjectQuery.
         var searchCriteria = TowerMod.GetSearchCriteria();
         ObjectQuery<tower_modifications> towerModQuery = ce.tower_modifications;
         towerModQuery = Utility.BuildObjectQuery(towerModQuery, parameters, searchCriteria);

         // Execute query and get count.
         this.VitualItemsCount = towerModQuery.Count();

         // Add additional info to object.
         var extendTmObj = from m in towerModQuery
                           select new ExtendTowerModificaionInfo
                           {
                              id = m.id,
                              customer = m.customer,
                              site_uid = m.site_uid,
                              tower_mod_statuses = m.tower_mod_statuses,
                              tower_mod_types = m.tower_mod_types,
                              tower_mod_pm_user = m.tower_mod_pm_user,
                              project_complete_date = m.project_complete_date,
                              site = m.site,
                              tower_mod_pos = m.tower_mod_pos
                                               .OrderByDescending(d => d.created_at)
                                               .FirstOrDefault()
                           };

         // Items Count
         VitualItemsCount = extendTmObj.Count();

         if (VitualItemsCount > 0)
         {
            // Sort Order
            if (this.SortDirection == GridSortOrder.Asc)
            {
               var a = Utility.OrderBy(extendTmObj, sortName);
               lstExTobj = a.Skip(skipItems).Take(this.ItemsPerPage).ToList();
            }
            else
            {
               var b = Utility.OrderByDescending(extendTmObj, sortName);
               lstExTobj = b.Skip(skipItems).Take(this.ItemsPerPage).ToList();
            }
            //>

            // Convert Result         
            foreach (var mod in lstExTobj)
            {
               //Added by Nam 2012-01-24
               List<LeaseAppTowerModInfo> lstResult = LeaseAppTowerMod.GetRalatedLeaseApp(mod.id);
               string tower_modifications__customer__customer_name = "";
               if (lstResult.Count == 0)
               {
                  //Old method for didn't have related lease application.
                  if (mod.customer != null) tower_modifications__customer__customer_name = mod.customer.customer_name;
               }
               else
               {
                  String sCustomerNames = "";
                  foreach (LeaseAppTowerModInfo item in lstResult)
                  {
                     if (item.CustomerName != null)
                     {
                        if (sCustomerNames == "")
                        {
                           sCustomerNames += item.CustomerName;
                        }
                        else
                        {
                           sCustomerNames += ", " + item.CustomerName;
                  }
                     }
                  }

                  tower_modifications__customer__customer_name = sCustomerNames;
               }
               //End Added

               string towerModStatusesTowerModStatusName = "";
               if (mod.tower_mod_statuses != null) towerModStatusesTowerModStatusName = mod.tower_mod_statuses.tower_mod_status_name;

               string towerModTypesTowerModType = "";
               if (mod.tower_mod_types != null) towerModTypesTowerModType = mod.tower_mod_types.tower_mod_type;
               //Tower Mod POs data

               string towerModPhaseTypesPhaseName = "";
               DateTime? dateCurPackageRcvd = null;

               if (mod.tower_mod_pos != null)
               {
                  if (mod.tower_mod_pos.tower_mod_phase_types != null) towerModPhaseTypesPhaseName = mod.tower_mod_pos.tower_mod_phase_types.phase_name.ToString();
                  dateCurPackageRcvd = mod.tower_mod_pos.ebid_req_rcvd_date;
               }
               //End Tower Mod POs data
               string siteRogueEquipmentYn = "false";
               if (mod.site != null) siteRogueEquipmentYn = mod.site.rogue_equipment_yn.ToString();

               string towerModPmUserName = "";
               if (mod.tower_mod_pm_user != null)
               {
                  towerModPmUserName = mod.tower_mod_pm_user.last_name + ", " + mod.tower_mod_pm_user.first_name;
               }

               //Checking if data comes from SiteDashboard
               if (HttpContext.Current.Request.UrlReferrer != null && HttpContext.Current.Request.UrlReferrer.LocalPath.ToString().Contains("SiteDashboard.aspx"))
               {
                  //Workaround for diference data column binding in SiteDashboard page.
                  result.Add(new KeyValuePair<string, object[]>(mod.ToString(), new object[] { mod.site_uid, tower_modifications__customer__customer_name, towerModStatusesTowerModStatusName,
                  towerModTypesTowerModType, towerModPhaseTypesPhaseName, Utility.DateToString(dateCurPackageRcvd), Utility.DateToString(mod.project_complete_date), siteRogueEquipmentYn,
                     mod.id.ToString() }));
               }
               else
               {
                  if (HttpContext.Current.Request.UrlReferrer == null)
                  {
                     //Calling from widget.
                     result.Add(new KeyValuePair<string, object[]>(mod.id.ToString(), new object[] { mod.site_uid, tower_modifications__customer__customer_name, towerModStatusesTowerModStatusName,
                     towerModTypesTowerModType, towerModPhaseTypesPhaseName, towerModPmUserName, Utility.DateToString(dateCurPackageRcvd), Utility.DateToString(mod.project_complete_date), siteRogueEquipmentYn,
                     mod.id.ToString() }));
                  }
                  else
                  {
                     //Calling from other page.
                     result.Add(new KeyValuePair<string, object[]>(mod.ToString(), new object[] { mod.site_uid, tower_modifications__customer__customer_name, towerModStatusesTowerModStatusName,
                     towerModTypesTowerModType, towerModPhaseTypesPhaseName, towerModPmUserName, Utility.DateToString(dateCurPackageRcvd), Utility.DateToString(mod.project_complete_date), siteRogueEquipmentYn,
                     mod.id.ToString() }));
                  }

               }
            }
         }

         return result;             
      }
      //-->        
        
      /// <summary>
      /// Template class for store the important info for Tower modificaion with Tower mod POs
      /// </summary>
      public class ExtendTowerModificaionInfo 
      {
         public int id{ get; set; }
         public customer customer { get; set; }
         public string site_uid { get; set; }
         public tower_mod_statuses tower_mod_statuses { get; set; }
         public tower_mod_types tower_mod_types { get; set; }
         public user tower_mod_pm_user { get; set; }
         public DateTime? project_complete_date { get; set; }
         public site site { get; set; }
         public DateTime? ebid_req_rcvd_date { get; set; }
         public tower_mod_phase_types tower_mod_phase_types { get; set; }
         public tower_mod_pos tower_mod_pos { get; set; }
      }
   }
}
