﻿using System;
using System.Web;
using TrackIT2.BLL;
using TrackIT2.DAL;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace TrackIT2.Handlers
{
    /// <summary>
    /// CustomersAutoCompleteHandler - returns the data for Users Auto-complete Text Fields
    /// </summary>
    public class CustomersAutoCompleteHandler : IHttpHandler
    {

        #region IHttpHandler Members

            /// <summary>
            /// IsReusable
            /// </summary>
            public bool IsReusable
            {
                get { return true; }
            }
            //>

            /// <summary>
            /// ProcessRequest
            /// </summary>
            /// <param name="context"></param>
            public void ProcessRequest(HttpContext context)
            {
                try
                {
                    CPTTEntities ce = new CPTTEntities();
                    var customers = ce.customers.Where("it.customer_name LIKE '" + context.Request.QueryString["term"] + "%'").OrderBy("it.sort_order").OrderBy("it.customer_name");

                    // Convert Result
                    IList<string> result = new List<string>();
                    foreach (var c in customers)
                    {
                        result.Add(c.customer_name);
                    }

                    JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                    string tmo_customers = javaScriptSerializer.Serialize(result);
                    context.Response.ContentType = "text/html";
                    context.Response.Write(tmo_customers);
                }
                catch
                {
                    context.Response.Write("");
                }
            }
            //--

        #endregion
    }
}
