﻿using System;
using System.Web;
using TrackIT2.BLL;
using TrackIT2.DAL;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Linq;
using System.Web.SessionState;

namespace TrackIT2.Handlers
{
    public class LeaseAdminCreateAndEditHandler : IHttpHandler, IRequiresSessionState 
	{
        int iLeaseAppID = 0;
        bool isRadAssoc = false;
        bool isCreate = false;
        string Hidden_selected_leaseapp_value = string.Empty;
        string pnlAssocLeaseAppText = string.Empty;
        string Hidden_selected_site_ids = string.Empty;
        string Hidden_selected_customer = string.Empty;

        #region IHttpHandler Members

        /// <summary>
        /// IsReusable
        /// </summary>
        public bool IsReusable
        {
            get { return true; }
        }
        //>

        /// <summary>
        /// ProcessRequest
        /// </summary>
        /// <param name="context"></param>
        public void ProcessRequest(HttpContext context)
        {
            string[] result;
            try
            {
                ClearParam();
                GenParam(context);

                if (iLeaseAppID != 0)
                {
                    result = UpdateLeaseAdmin(isRadAssoc, iLeaseAppID);
                }
                else
                {
                    result = AddNewLeaseAdmin(isRadAssoc);
                }
            }
            catch(Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                result = new string[] { "false", "Error: a problem occurred while creating the Issue, this operation has been aborted.", "", isCreate.ToString().ToLower().ToLower() };
            }

            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            string returnResult = javaScriptSerializer.Serialize(result);
            context.Response.ContentType = "json";
            context.Response.Write(returnResult);

        }
        //--

        public void GenParam(HttpContext context)
        {
            int.TryParse(context.Request.Form["id"], out iLeaseAppID);
            Boolean.TryParse(context.Request.Form["isRadAssoc"], out isRadAssoc);
            Boolean.TryParse(context.Request.Form["isCreate"], out isCreate);
            if (!string.IsNullOrEmpty(context.Request.Form["Hidden_selected_leaseapp_value"]) && context.Request.Form["Hidden_selected_leaseapp_value"] != "null")
            {
                Hidden_selected_leaseapp_value = context.Request.Form["Hidden_selected_leaseapp_value"];
            }
            if (!string.IsNullOrEmpty(context.Request.Form["pnlAssocLeaseAppText"]))
            {
                pnlAssocLeaseAppText = context.Request.Form["pnlAssocLeaseAppText"];
            }
            if (!string.IsNullOrEmpty(context.Request.Form["Hidden_selected_site_ids"]))
            {
                Hidden_selected_site_ids = context.Request.Form["Hidden_selected_site_ids"];
            }
            if (!string.IsNullOrEmpty(context.Request.Form["Hidden_selected_customer"]))
            {
                Hidden_selected_customer = context.Request.Form["Hidden_selected_customer"];
            }
        }

        public void ClearParam()
        {
            iLeaseAppID = 0;
            isRadAssoc = false;
            isCreate = false;
            Hidden_selected_leaseapp_value = string.Empty;
            pnlAssocLeaseAppText = string.Empty;
            Hidden_selected_site_ids = string.Empty;
            Hidden_selected_customer = string.Empty;
        }
        /// <summary>
        /// Check Exists Associated LeaseApp
        /// </summary>
        /// <param name="leaseAppId"></param>
        /// <param name="ceRef"></param>
        /// <returns></returns>
        private bool CheckExistsAssociatedLeaseApp(int? leaseAppId, int? leaseAdminId, CPTTEntities ceRef)
        {
            bool result = false;
            if (leaseAppId.HasValue)
            {
                if (leaseAdminId.HasValue)
                {
                    //Check on edit
                    var checkExist = from leaseAdmin in ceRef.lease_admin_records
                                     where leaseAdmin.lease_application_id == leaseAppId
                                     select leaseAdmin;
                    if (checkExist.ToList().Count == 0)
                    {
                        result = false;
                    }
                    else
                    {
                        foreach (lease_admin_records item in checkExist.ToList())
                        {
                            if (item.id != leaseAdminId)
                            {
                                result = true;
                            }
                        }
                    }
                }
                else
                {
                    //check on Add
                    var checkExist = from leaseAdmin in ceRef.lease_admin_records
                                     where leaseAdmin.lease_application_id == leaseAppId
                                     select leaseAdmin;
                    if (checkExist.ToList().Count > 0)
                    {
                        result = true;
                    }
                }
            }
            return result;
        }
        //-->

        /// <summary>
        /// Get Sites From Control
        /// </summary>
        /// <param name="bIsAssoc"></param>
        /// <param name="la"></param>
        /// <returns></returns>
        private List<site> GetSitesFromControl(bool bIsAssoc, ref lease_admin_records la)
        {
            List<site> lstSiteId = new List<site>();
            if (bIsAssoc)
            {
                if (Hidden_selected_leaseapp_value != "")
                {
                    if (!string.IsNullOrEmpty(pnlAssocLeaseAppText))
                    {
                        lstSiteId.Add(BLL.Site.Search(pnlAssocLeaseAppText.ToUpper()));
                        if (!string.IsNullOrEmpty(Hidden_selected_leaseapp_value) || Hidden_selected_leaseapp_value != "null")
                        {
                            la.lease_application_id = int.Parse(Hidden_selected_leaseapp_value);
                        }
                    }
                }
            }
            else
            {
                List<string> lstStringSiteId = new List<string>();
                if (!string.IsNullOrEmpty(Hidden_selected_site_ids))
                {
                    lstStringSiteId = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(Hidden_selected_site_ids);
                }

                foreach (string siteid in lstStringSiteId)
                {
                    site siteData = BLL.Site.Search(siteid.ToUpper());
                    if (siteData != null)
                    {
                        lstSiteId.Add(siteData);
                        if (!string.IsNullOrEmpty(Hidden_selected_leaseapp_value) && !string.IsNullOrWhiteSpace(Hidden_selected_leaseapp_value))
                        {
                            la.lease_application_id = int.Parse(Hidden_selected_leaseapp_value);
                        }
                    }
                }
            }
            return lstSiteId;
        }
        //-->

        /// <summary>
        /// Add New Lease Admin
        /// </summary>
        /// <param name="bIsAssoc"></param>
        private string[] AddNewLeaseAdmin(bool bIsAssoc)
        {
            string[] returnResult = null;  //{finalResult,messageDetail,targetControl,isCreate}
            string sResult;
            lease_admin_records LA = new lease_admin_records();
            List<site> lstSiteId = GetSitesFromControl(bIsAssoc, ref LA);

            using (CPTTEntities ce = new CPTTEntities())
            {
                try
                {
                    bool notFoundLeaseAppWithAssoc = false;
                    if (bIsAssoc)
                    {
                        if (lstSiteId.FirstOrDefault() == null)
                        {
                            returnResult = new string[] { "false", "Please verify a Site ID.", "txtAssocSiteID", isCreate.ToString().ToLower() };
                        }
                        else
                        {
                            if (!CheckExistsAssociatedLeaseApp(LA.lease_application_id, null, ce) && (lstSiteId.Count > 0))
                            {
                                if (Hidden_selected_leaseapp_value == "")
                                {
                                    notFoundLeaseAppWithAssoc = true;
                                }

                                if (!notFoundLeaseAppWithAssoc)
                                {
                                    foreach (site item in lstSiteId)
                                    {
                                        ce.Attach(item);
                                        LA.sites.Add(item);
                                    }
                                    ce.Connection.Open();
                                    sResult = LeaseAdminRecord.Add(LA, ce);
                                    if (!String.IsNullOrEmpty(sResult))
                                    {
                                        returnResult = new string[] { "false", "Error: a problem occurred while creating the Issue, this operation has been aborted.", "", isCreate.ToString().ToLower() };
                                    }
                                    else
                                    {
                                        returnResult = new string[] { "true", LA.id.ToString(), bIsAssoc.ToString(), isCreate.ToString().ToLower() };
                                    }
                                }
                                else
                                {
                                    returnResult = new string[] { "false", "Please select Lease Application first.", "txtAssocSiteID", isCreate.ToString().ToLower() };
                                }
                            }
                            else
                            {
                                returnResult = new string[] { "false", "Selected Lease Application has selected before.", "MainContent_ddlAssociatedLeaseApps", isCreate.ToString().ToLower() };
                            }
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(Hidden_selected_customer))
                        {
                            int data = int.Parse(Hidden_selected_customer);
                            LA.customer_id = data;
                        }

                        foreach (site item in lstSiteId)
                        {
                            ce.Attach(item);
                            LA.sites.Add(item);
                        }
                        ce.Connection.Open();
                        sResult = LeaseAdminRecord.Add(LA, ce);
                        if (!String.IsNullOrEmpty(sResult))
                        {
                            returnResult = new string[] { "false", "Error: a problem occurred while creating the Issue, this operation has been aborted.", "", isCreate.ToString().ToLower() };
                        }
                        else
                        {
                            returnResult = new string[] { "true", LA.id.ToString(), bIsAssoc.ToString(), isCreate.ToString().ToLower() };
                        }
                    }
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    sResult = ex.ToString();
                    returnResult = new string[] { "false", "Error: a problem occurred while creating the Issue, this operation has been aborted.", "", isCreate.ToString().ToLower() };
                }
                finally
                {
                    ce.Connection.Dispose();
                }
            }
            return returnResult;
        }
        //-->

        /// <summary>
        /// Update Lease Admin
        /// </summary>
        /// <param name="bIsAssoc"></param>
        /// <param name="id"></param>
        private string[] UpdateLeaseAdmin(bool bIsAssoc, int id)
        {
            string sResult;
            string[] returnResult;
            using (CPTTEntities ce = new CPTTEntities())
            {
                try
                {
                    lease_admin_records la = LeaseAdminRecord.Search(id, ce);

                    List<site> lstSiteId = GetSitesFromControl(bIsAssoc, ref la);

                    if (!CheckExistsAssociatedLeaseApp(la.lease_application_id, la.id, ce) || !bIsAssoc)
                    {
                        if (!bIsAssoc)
                        {
                            if (!string.IsNullOrEmpty(Hidden_selected_customer))
                            {
                                int customer_id = int.Parse(Hidden_selected_customer);
                                la.customer_id = customer_id;
                            }
                        }
                        if (la.sites != null)
                        {
                            var tmpRemoveList = la.sites.ToList();

                            for (int x = 0; x < tmpRemoveList.Count; x++)
                            {
                                var queryLeaseAdmin = from le in lstSiteId
                                                      where le.site_uid == tmpRemoveList[x].site_uid
                                                      select le;
                                if (!queryLeaseAdmin.Any())
                                {
                                    la.sites.Remove(tmpRemoveList[x]);
                                }
                            }

                        }
                        foreach (site item in lstSiteId)
                        {
                            site item1 = item;
                            var queryLeaseAdmin = from le in la.sites
                                                  where le.site_uid == item1.site_uid
                                                  select le;
                            if (!queryLeaseAdmin.Any())
                            {
                                ce.Attach(item);
                                if (la.sites != null) la.sites.Add(item);
                            }
                        }
                        ce.Connection.Open();
                        sResult = LeaseAdminRecord.Update(la, ce);
                        if (!String.IsNullOrEmpty(sResult))
                        {
                            returnResult = new string[] { "false", "Error: a problem occurred while creating the Issue, this operation has been aborted.", "", isCreate.ToString().ToLower() };
                        }
                        else
                        {
                            returnResult = new string[] { "true", la.id.ToString(), bIsAssoc.ToString(), isCreate.ToString().ToLower() };
                        }
                    }
                    else
                    {
                        returnResult = new string[] { "false", "Selected Lease Application has selected before.", "MainContent_ddlAssociatedLeaseApps", isCreate.ToString().ToLower() };
                    }
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    returnResult = new string[] { "false", "Error: a problem occurred while creating the Issue, this operation has been aborted.", "", isCreate.ToString().ToLower() };
                }
                finally
                {
                    ce.Connection.Dispose();
                }
            }
            return returnResult;
        }
        //-->
        #endregion
	}
}