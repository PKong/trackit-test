﻿using System;
using System.Data.Objects;
using System.Web;
using System.Collections.Generic;
using FlexigridASPNET;
using TrackIT2.BLL;
using TrackIT2.DAL;
using System.Linq;
using System.Collections.Specialized;

namespace TrackIT2.Handlers
{
    /// <summary>
    /// SiteDataPackagesListHandler: Returns a list of Site Data Packages for binding to a Flexigrid
    /// </summary>
    public class SiteDataPackagesListHandler : BaseFlexiHandler
    {
        /// <summary>
        /// Gets the filtered data.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<KeyValuePair<string, object[]>> GetFilteredData()
        {            
            var result = new List<KeyValuePair<string, object[]>>();
            List<site_data_packages> siteDataPackages;
            const int startIndex = 6;
            var ce = new CPTTEntities();
            var skipItems = (Page - 1) * this.ItemsPerPage;
            var sortName = !String.IsNullOrWhiteSpace(this.SortName) ? this.SortName : "site_uid";
            var form = HttpContext.Current.Request.Form;
            var parameters = new NameValueCollection();
            DefaultFilterSession filterSession = new DefaultFilterSession();
            string filterString = string.Empty;

            // If we have form values, extract them to build the object query.
            if (form.Count > startIndex)
            {
               for (var i = startIndex; i < form.Count; i++)
               {
                  var itemKey = form.GetKey(i);
                  var itemValue = form.Get(i);

                  // If the search came from the global search bar, a generic 
                  // "gs" item key is specified and needs to be changed.
                  if (itemKey == "gs")
                  {
                      itemKey = "General";
                  }

                  parameters.Add(itemKey, itemValue);

                  if (itemKey == DefaultFilterSession.SessionName)
                  {
                      filterString = itemValue;
                  }
               }
            }

            // Check session filter
            parameters.Remove(DefaultFilterSession.ApplyClick);
            parameters.Remove(DefaultFilterSession.SessionName);
            DefaultFilterSession.GetOrSetFilterSession(HttpContext.Current, ref filterSession, filterString, DefaultFilterSession.FilterType.SdpFilter);

            // Build ObjectQuery         
            ObjectQuery<site_data_packages> sdpQuery = ce.site_data_packages;
            var searchCriteria = BLL.SiteDataPackage.GetSearchCriteria();
            sdpQuery = Utility.BuildObjectQuery(sdpQuery, parameters, searchCriteria);

            // Execute query and get count.
            VitualItemsCount = sdpQuery.Count();            

            // Sort Order
            if (SortDirection == GridSortOrder.Asc)
            {
                var a = Utility.OrderBy(sdpQuery, sortName);
                siteDataPackages = a.Skip(skipItems).Take(this.ItemsPerPage).ToList();
            }
            else
            {
                var b = Utility.OrderByDescending(sdpQuery, sortName);
                siteDataPackages = b.Skip(skipItems).Take(this.ItemsPerPage).ToList();
            }
            //>

            // Convert Result
            foreach (var sdp in siteDataPackages)
            {
                var site__region_name = "";
                var site__market_code = "";
                var sdp_priorities__sdp_priority = "";
                var sdp_special_projects__sdp_special_project = "";
                var user_tmo_pm_emp = "";
                var user_tmo_spec_cur_emp = "";
                DateTime? site__site_on_air_date = null;

                if (sdp.site != null)
                {
                    site__region_name = sdp.site.region_name;
                    site__market_code = sdp.site.market_code;
                    if(sdp.site.site_on_air_date != null) site__site_on_air_date = sdp.site.site_on_air_date;
                }
                
                if (sdp.sdp_priorities != null)
                {
                   sdp_priorities__sdp_priority = sdp.sdp_priorities.sdp_priority;
                }
                                
                if (sdp.sdp_special_project_id != null) 
                {
                    var spProject = from sp in ce.sdp_special_projects
                                    where sp.id == sdp.sdp_special_project_id
                                    select sp;

                    if (spProject.Any())
                    {
                        sdp_special_projects__sdp_special_project = spProject.ToList<sdp_special_projects>()[0].sdp_special_project;
                    }
                }
                
                if (sdp.lease_applications != null)
                {
                    if (sdp.lease_applications.user_lease_applications_tmo_specialist_pm != null) user_tmo_pm_emp = sdp.lease_applications.user_lease_applications_tmo_specialist_pm.last_name + ", " + sdp.lease_applications.user_lease_applications_tmo_specialist_pm.first_name;
                    if (sdp.lease_applications.user_lease_applications_tmo_specialist_current != null) user_tmo_spec_cur_emp = sdp.lease_applications.user_lease_applications_tmo_specialist_current.last_name + ", " + sdp.lease_applications.user_lease_applications_tmo_specialist_current.first_name;
                }
                else
                {
                    if (sdp.user6 != null) user_tmo_pm_emp = sdp.user6.last_name + ", " + sdp.user6.first_name;
                    if (sdp.user7 != null) user_tmo_spec_cur_emp = sdp.user7.last_name + ", " + sdp.user7.first_name;
                }

                if (HttpContext.Current.Request.UrlReferrer != null)
                {
                    result.Add(new KeyValuePair<string, object[]>
                                   (sdp.id.ToString(), 
                                    new object[] 
                                    { 
                                       sdp.site_uid, 
                                       site__region_name, 
                                       site__market_code,
                                       Utility.DateToString(site__site_on_air_date), 
                                       sdp_priorities__sdp_priority, 
                                       Utility.DateToString(sdp.sdp_request_date), 
                                       sdp_special_projects__sdp_special_project,
                                       user_tmo_pm_emp, 
                                       user_tmo_spec_cur_emp, 
                                       sdp.id.ToString() 
                                    }
                                   )
                              );
                }
                else
                {
                    var sSiteOnAir = "";

                    if (site__site_on_air_date.HasValue)
                    {
                        sSiteOnAir = Utility.DateToString(site__site_on_air_date.Value);
                    }

                    result.Add(new KeyValuePair<string, object[]>
                                   (sdp.id.ToString(), 
                                    new object[] 
                                    { 
                                       sdp.site_uid, 
                                       site__region_name, 
                                       site__market_code,
                                       sSiteOnAir, 
                                       sdp_priorities__sdp_priority, 
                                       Utility.DateToString(sdp.sdp_request_date), 
                                       sdp_special_projects__sdp_special_project,
                                       user_tmo_pm_emp, 
                                       user_tmo_spec_cur_emp, 
                                       sdp.id.ToString() 
                                    }
                                   )
                              );
                }
                

            }

            return result;
        }        
    }
}
