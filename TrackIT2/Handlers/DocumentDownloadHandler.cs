﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.DAL;
using TrackIT2.BLL;
using System.IO;

namespace TrackIT2.Handlers
{
    public class DocumentDownloadHandler : IHttpHandler
    {
        /// <summary>
        /// You will need to configure this handler in the web.config file of your 
        /// web and register it with IIS before being able to use it. For more information
        /// see the following link: http://go.microsoft.com/?linkid=8101007
        /// </summary>
        #region IHttpHandler Members

        public bool IsReusable
        {
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["key"]))
            {
                string keyName = HttpContext.Current.Request.QueryString["key"];
                if (!string.IsNullOrEmpty(keyName))
                {
                    AmazonDMS.DownloadObject(HttpContext.Current.Server.UrlDecode(keyName));
                }
            }
        }

        #endregion
    }
}
