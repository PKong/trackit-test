﻿using System;
using System.Collections;
using System.Data.Objects;
using System.Web;
using System.Collections.Generic;
using FlexigridASPNET;
using TrackIT2.BLL;
using TrackIT2.DAL;
using System.Linq;
using System.Collections.Specialized;
using System.Text;

namespace TrackIT2.Handlers
{
    public class TowerListHandler : BaseFlexiHandler
    {
        public static bool _subFilterHasNoResults = false;

        public override IEnumerable<KeyValuePair<string, object[]>> GetFilteredData()
        {

            // Variables
            const string GLOBAL_SEARCH_QUERY = "(it.site_uid LIKE '%{0}%') || (it.site.site_name LIKE '%{0}%')"; //Add customer site UID from Ticket 381 : Nam 2012/02/06
            //
            CPTTEntities ce = new CPTTEntities();
            IQueryable<tower_list> iqTowerList;
            List<tower_list> towerList;
            int skipItems = (Page - 1) * this.ItemsPerPage;
            string sortName = !String.IsNullOrWhiteSpace(this.SortName) ? this.SortName : "site_uid";
            string globalSearch = null;
            GridSortOrder? customSorting = null;
            // Search Filter Values
            NameValueCollection form = HttpContext.Current.Request.Form;
            int startIndex = 6;
            StringBuilder sb = new StringBuilder();
            var result = new List<KeyValuePair<string, object[]>>();

            // Update status / In Request?
            if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["siteid"]) && HttpContext.Current.Request.QueryString["newStatus"] != null
                && (HttpContext.Current.Request.QueryString["type"] != null))
            {
                // Param
                String siteId = HttpContext.Current.Request.QueryString["siteid"];
                String newStatus = HttpContext.Current.Request.QueryString["newStatus"];
                string type = HttpContext.Current.Request.QueryString["type"];
                string currentstatus = HttpContext.Current.Request.QueryString["currentstatus"];

                // Update status and reture result
                return HandleListStatusChange(siteId, newStatus,currentstatus, type, result);
            }
            //>

            // Get Filters
            if (form.Count > startIndex)
            {
                for (int i = startIndex; i < form.Count; i++)
                {
                    // Check NOT Global Search param
                    if (form.GetKey(i) == Globals.QS_PROJECT_CATEGORY)
                    {
                        //Use for backward compatibility.
                        //Do nothing. 
                    }
                    else if (form.GetKey(i) == Globals.QS_GLOBAL_SEARCH)
                    {
                        // Global Search - get val
                        //globalSearch = form.Get(i);
                        globalSearch = Utility.PrepareSqlSearchString(form.Get(i));
                    }
                    else if (form.GetKey(i) == Globals.QS_SORT_DIRECTION)
                    {
                        //Using custom sort order from widget.
                        if (form.Get(i) == Globals.SORT_DIRECTION_VALUE_ASC)
                        {
                            customSorting = GridSortOrder.Asc;
                        }
                        else
                        {
                            customSorting = GridSortOrder.Desc;
                        }
                    }
                    else
                    {
                        // Build Where clause
                        buildSearchFilterWhereClause(sb, form.GetKey(i), Utility.PrepareSqlSearchString(form.Get(i)));
                    }
                }
            }
            //>

            _subFilterHasNoResults = false;
            if (!_subFilterHasNoResults)
            {
                // Get Working Set
                if (sb.Length > 0)
                {
                    // Add WHERE clause
                    if (globalSearch == null)
                    {
                        iqTowerList = ce.tower_list.Where(sb.ToString());
                    }
                    else
                    {
                        // Global Search
                        iqTowerList = ce.tower_list.Where(String.Format(GLOBAL_SEARCH_QUERY, globalSearch)).Where(sb.ToString());
                    }
                }
                else
                {
                    // Non-filtered Select
                    if (globalSearch == null)
                    {
                        // Standard
                        iqTowerList = ce.tower_list;
                    }
                    else
                    {
                        // Global Search
                        iqTowerList = ce.tower_list.Where(String.Format(GLOBAL_SEARCH_QUERY, globalSearch));
                    }
                }
                //>

                // Items Count
                this.VitualItemsCount = iqTowerList.Count();

                // Sort Order
                if (customSorting == null) customSorting = this.SortDirection;
                if (customSorting == GridSortOrder.Asc)
                {
                    var asc = Utility.OrderBy(iqTowerList, sortName);
                    towerList = asc.Skip(skipItems).Take(this.ItemsPerPage).ToList();
                }
                else
                {
                    var desc = Utility.OrderByDescending(iqTowerList, sortName);
                    towerList = desc.Skip(skipItems).Take(this.ItemsPerPage).ToList();
                }

                foreach (var tower in towerList)
                {
                    string trackit_list_status = string.Empty;
                    string map_list_status = string.Empty;
                    string site__status_desc = string.Empty;
                    string TOWER_STATUS_BUTTON = string.Empty;

                   

                    if (tower.trackit_override_list_status != null)
                    {
                        if (tower.trackit_override_list_status_id.HasValue && (tower.trackit_override_list_status_id.Value == tower.trackit_override_list_status.id))
                        {
                            trackit_list_status = tower.trackit_override_list_status.list_status;
                        }
                    }

                    if (tower.map_override_list_status != null)
                    {
                        if (tower.map_override_list_status_id.HasValue && (tower.map_override_list_status_id.Value == tower.map_override_list_status.id))
                        {
                            map_list_status = tower.map_override_list_status.list_status;
                        }
                    }

                    trackit_list_status = TowerListStatus.GetTrackitTowerStatusList(tower.site_uid, tower.trackit_override_list_status_id, trackit_list_status, tower.trackit_default_list_status);
                    map_list_status = TowerListStatus.GetMapTowerStatusList(tower.site_uid, tower.map_override_list_status_id, map_list_status, tower.map_default_list_status);

                    result.Add(new KeyValuePair<string, object[]>(tower.site_uid.ToString(), new object[] { tower.site_uid, tower.site_name,
                    tower.site_class_desc, tower.market_name, tower.site_status_desc, trackit_list_status, map_list_status}));
                }
            }

            return result;
        }
        //-->

        /// <summary>
        /// Update new status
        /// </summary>
        /// <param name="siteId">string</param>
        /// <param name="newStatus">string</param>
        /// <param name="type">string</param>
        /// <param name="result">List</param>
        /// <returns>IEnumerable</returns>
        private static IEnumerable<KeyValuePair<string, object[]>> HandleListStatusChange(string siteId, string newStatus,string currentstatus, string type, List<KeyValuePair<string, object[]>> result)
        {
            //Update new status
            try
            {
                using (CPTTEntities ce = new CPTTEntities())
                {
                    if (HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated)
                    {
                        if (siteId != null)
                        {
                            ce.Connection.Open();

                            tower_list obj = TowerListStatus.GetTowerList(siteId);
                            
                            if (obj != null)
                            {
                                TrackIT2.BLL.TowerListStatus.TowerListUpdateAction siteAction = TowerListStatus.TowerListUpdateAction.NONE;

                                ce.Attach(obj);

                                tower_list_statuses newStatusObj = TowerListStatus.GetTowerListStatus(newStatus);

                                // Set new status
                                if (newStatusObj != null && !newStatusObj.list_status.Contains("Default"))
                                {
                                    if (type == TrackIT2.BLL.TowerListStatus.TowerListType.TrackiT.ToString())
                                    {
                                        obj.trackit_override_list_status_id = newStatusObj.id;
                                    }
                                    else if (type == TrackIT2.BLL.TowerListStatus.TowerListType.Map.ToString())
                                    {
                                        obj.map_override_list_status_id = newStatusObj.id;
                                        obj.updated_at = DateTime.Now;
                                        string currentId = HttpContext.Current.Session["userId"].ToString();
                                        int id = 0;
                                        if(int.TryParse(currentId, out id))
                                        {
                                            obj.updater_id = id;
                                        }
                                    }
                                }
                                else
                                {
                                    if (type == TrackIT2.BLL.TowerListStatus.TowerListType.TrackiT.ToString())
                                    {
                                        obj.trackit_override_list_status_id = null;
                                    }
                                    else if (type == TrackIT2.BLL.TowerListStatus.TowerListType.Map.ToString())
                                    {
                                        obj.map_override_list_status_id = null;
                                        string currentId = HttpContext.Current.Session["userId"].ToString();
                                        int id = 0;
                                        if (int.TryParse(currentId, out id))
                                        {
                                            obj.updater_id = id;
                                        }
                                    }
                                }

                                // Set site action
                                if ((currentstatus.Contains("Off") && newStatus.Contains("On")) || (string.IsNullOrEmpty(currentstatus) && newStatus.Contains("On")))
                                {
                                    siteAction = TrackIT2.BLL.TowerListStatus.TowerListUpdateAction.ADD;
                                }
                                else if ((currentstatus.Contains("On") && newStatus.Contains("Off") || (currentstatus.Contains("On") && string.IsNullOrEmpty(newStatus))))
                                {
                                    siteAction = TrackIT2.BLL.TowerListStatus.TowerListUpdateAction.DELETE;
                                }

                                string updateResult = TowerListStatus.Update(obj, siteAction, type, ce);
                                if (string.IsNullOrEmpty(updateResult))
                                {
                                    string trackit_list_status = string.Empty;
                                    string map_list_status = string.Empty;
                                    string site__status_desc = string.Empty;
                                    string TOWER_STATUS_BUTTON = string.Empty;

                                    if (obj.trackit_override_list_status != null)
                                    {
                                        if (obj.trackit_override_list_status_id.HasValue && (obj.trackit_override_list_status_id.Value == obj.trackit_override_list_status.id))
                                        {
                                            trackit_list_status = obj.trackit_override_list_status.list_status;
                                        }
                                    }

                                    if (obj.map_override_list_status != null)
                                    {
                                        if (obj.map_override_list_status_id.HasValue && (obj.map_override_list_status_id.Value == obj.map_override_list_status.id))
                                        {
                                            map_list_status = obj.map_override_list_status.list_status;
                                        }
                                    }

                                    trackit_list_status = TowerListStatus.GetTrackitTowerStatusList(obj.site_uid, obj.trackit_override_list_status_id, trackit_list_status, obj.trackit_default_list_status);
                                    map_list_status = TowerListStatus.GetMapTowerStatusList(obj.site_uid, obj.map_override_list_status_id, map_list_status, obj.map_default_list_status);

                                    result.Add(new KeyValuePair<string, object[]>(obj.site_uid.ToString(), new object[] { obj.site_uid, obj.site_name,
                                    obj.site_class_desc, obj.market_name, obj.site_status_desc, trackit_list_status, map_list_status}));

                                    ce.SaveChanges();
                                }
                                else
                                {
                                    result.Add(new KeyValuePair<string, object[]>(obj.site_uid.ToString(), new object[] { "error", updateResult}));
                                }
                            }
                        }
                    }

                    ce.Connection.Close();
                    ce.Dispose();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        
            return result;
        }
        //-->

        /// <summary>
        /// Build Search Filter Where Clause
        /// </summary>
        /// <returns></returns>
        public static void buildSearchFilterWhereClause(StringBuilder sb, String itemKey, String itemValue)
        {
            if (sb.Length > 0)
            {
                sb.Append(" && ");
            }

            StringBuilder sbType = new StringBuilder();

            switch (itemKey)
            {
                // TowerListSearch
                case SearchFilterControls.TowerListSearch.Name:
                    sb.AppendFormat(SearchFilterControls.TowerListSearch.Criterea, Utility.PrepareSqlSearchString(itemValue));
                    break;

                case SearchFilterControls.TrackiTOn.Name:
                    sb.AppendFormat(SearchFilterControls.TrackiTOn.Criterea, Utility.PrepareSqlSearchString(itemValue));
                    break;

                case SearchFilterControls.TrackiTOff.Name:
                    sb.AppendFormat(SearchFilterControls.TrackiTOff.Criterea, Utility.PrepareSqlSearchString(itemValue));
                    break;

                case SearchFilterControls.MapOn.Name:
                    sb.AppendFormat(SearchFilterControls.MapOn.Criterea, Utility.PrepareSqlSearchString(itemValue));
                    break;

                case SearchFilterControls.MapOff.Name:
                    sb.AppendFormat(SearchFilterControls.MapOff.Criterea, Utility.PrepareSqlSearchString(itemValue));
                    break;
            }
        }
        //-->

        /// <summary>
        /// Search Filter Controls
        /// </summary>
        public struct SearchFilterControls
        {
            /// <summary>
            /// Tower List Search
            /// </summary>
            public struct TowerListSearch
            {
                public const String Name = "TowerListSearch";
                public const String ParameterName = "@TowerListSearch";
                public const String Criterea = "(it.site_name LIKE '%{0}%' || it.site_uid LIKE '%{0}%')";
            }

            /// <summary>
            /// TrackiTOn Search
            /// </summary>
            public struct TrackiTOn
            {
                public const String Name = "TrackiTOn";
                public const String ParameterName = "@TrackiTOn";
                public const String Criterea = "(it.trackit_override_list_status.list_status = 'Default On' || it.trackit_override_list_status.list_status = 'Always On' || (it.trackit_default_list_status.list_status = 'Default On' && it.trackit_override_list_status.list_status is null))";
            }

            /// <summary>
            /// TrackiTOff Search
            /// </summary>
            public struct TrackiTOff
            {
                public const String Name = "TrackiTOff";
                public const String ParameterName = "@TrackiTOff";
                public const String Criterea = "(it.trackit_override_list_status.list_status = 'Always Off' || it.trackit_override_list_status.list_status = 'Default Off' || (it.trackit_default_list_status.list_status = 'Default Off' && it.trackit_override_list_status.list_status is null))";
            }

            /// <summary>
            /// MapOn Search
            /// </summary>
            public struct MapOn
            {
                public const String Name = "MapOn";
                public const String ParameterName = "@MapOn";
                public const String Criterea = "(it.map_override_list_status.list_status = 'Always On' || it.map_override_list_status.list_status = 'Default On' || (it.map_default_list_status.list_status = 'Default On' && it.map_override_list_status.list_status is null))";
            }

            /// <summary>
            /// MapOff Search
            /// </summary>
            public struct MapOff
            {
                public const String Name = "MapOff";
                public const String ParameterName = "@MapOff";
                public const String Criterea = "(it.map_override_list_status.list_status = 'Default Off' || it.map_override_list_status.list_status = 'Always Off' || (it.map_default_list_status.list_status = 'Default Off' && it.map_override_list_status.list_status is null))";
            }
        }
    }
}