﻿using System;
using System.Web;
using TrackIT2.BLL;
using TrackIT2.DAL;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace TrackIT2.Handlers
{
    /// <summary>
    /// SiteNameAutoCompleteHandler - returns the data for Site Name Auto-complete Text Fields
    /// </summary>
    public class SiteNameAutoCompleteHandler : IHttpHandler
    {

        #region IHttpHandler Members

            /// <summary>
            /// IsReusable
            /// </summary>
            public bool IsReusable
            {
                get { return true; }
            }
            //>

            /// <summary>
            /// ProcessRequest
            /// </summary>
            /// <param name="context"></param>
            public void ProcessRequest(HttpContext context)
            {
                try
                {
                    CPTTEntities ce = new CPTTEntities();
                    var sites = ce.sites.Where("it.site_name LIKE '" + context.Request.QueryString["term"] + "%'").OrderBy("it.site_name");

                    // Convert Result
                    IList<string> result = new List<string>();
                    foreach (var s in sites)
                    {
                        result.Add(s.site_name);
                    }

                    JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                    string names = javaScriptSerializer.Serialize(result);
                    context.Response.ContentType = "text/html";
                    context.Response.Write(names);
                }
                catch
                {
                    context.Response.Write("");
                }
            }
            //--

        #endregion
    }
}
