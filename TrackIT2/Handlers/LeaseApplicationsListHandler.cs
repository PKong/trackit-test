﻿using FlexigridASPNET;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.Objects;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using TrackIT2.BLL;
using TrackIT2.DAL;
using TrackIT2.Objects;

namespace TrackIT2.Handlers
{
   /// <summary>
   /// LeaseApplicationsListHandler: Returns a list of Lease Applications for binding to a Flexigrid
   /// </summary>
   public class LeaseApplicationsListHandler : BaseFlexiHandler
   {
      /// <summary>
      /// Gets the filtered data.
      /// </summary>
      /// <returns></returns>
      public override IEnumerable<KeyValuePair<string, object[]>> GetFilteredData()
      {
         // Variables
         var result = new List<KeyValuePair<string, object[]>>();

         List<LeaseAppWraper> applications;
         var ce = new CPTTEntities();
         var skipItems = (Page - 1) * ItemsPerPage;
         var form = HttpContext.Current.Request.Form;
         var parameters = new NameValueCollection();
         var startIndex = 7;
         var queryFromListPage = false;
         DefaultFilterSession filterSession = new DefaultFilterSession();
         string filterString = string.Empty;

         // Set sort name if it exists.
         var sortName = !String.IsNullOrWhiteSpace(this.SortName) ?
                        this.SortName : "id";

         // Dashboard and "module" queries use different indexes than
         // the start page for their search criteria. Update each
         // parameter accordingly.
         if (HttpContext.Current.Request.UrlReferrer != null)
         {
            startIndex = 6;

            if (HttpContext.Current.Request.UrlReferrer.LocalPath == "/LeaseApplications/")
            {
               queryFromListPage = true;
            }
         }

         // If there are form values, extract them to build the object query.

         if (form.Count > startIndex)
         {
             for (var i = startIndex; i < form.Count; i++)
             {
                 var itemKey = form.GetKey(i);
                 var itemValue = form.Get(i);

                 // If the search came from the global search bar, a generic 
                 // "gs" item key is specified and needs to be changed.
                 if (itemKey == "gs")
                 {
                     itemKey = "General";
                 }

                 // The "pc" parameter is a shortcode for project category and
                 // needs to be updated accordingly.
                 if (itemKey == "pc")
                 {
                     itemKey = "ProjectCategory";
                 }

                 parameters.Add(itemKey, itemValue);

                 if (itemKey == DefaultFilterSession.SessionName)
                 {
                     filterString = itemValue;
                 }
             }
         }

        // Check session filter
        parameters.Remove(DefaultFilterSession.ApplyClick);
        parameters.Remove(DefaultFilterSession.SessionName);
        DefaultFilterSession.GetOrSetFilterSession(HttpContext.Current, ref filterSession, filterString, DefaultFilterSession.FilterType.LeaseAppFilter);

         // Build ObjectQuery
         var searchCriteria = BLL.LeaseApplication.GetSearchCriteria();
         ObjectQuery<lease_applications> leaseApplicationsQuery = ce.lease_applications;
         leaseApplicationsQuery = Utility.BuildObjectQuery(leaseApplicationsQuery, parameters, searchCriteria);

         VitualItemsCount = leaseApplicationsQuery.Count();

         // Sort Order
         if (VitualItemsCount > 0)
         {
            if (this.SortDirection == GridSortOrder.Asc)
            {
               var asc = Utility.OrderBy(leaseApplicationsQuery, sortName);
               applications = (from leaseAppQuery in asc.Skip(skipItems).Take(this.ItemsPerPage)
                               select new LeaseAppWraper
                               {
                                   id = leaseAppQuery.id,
                                   site_uid = leaseAppQuery.site != null ? leaseAppQuery.site.site_uid : string.Empty,
                                   site_name = leaseAppQuery.site != null ? leaseAppQuery.site.site_name : string.Empty,
                                   original_carrier = leaseAppQuery.original_carrier,
                                   lease_app_activity = leaseAppQuery.leaseapp_activities.lease_app_activity != null ? leaseAppQuery.leaseapp_activities.lease_app_activity : string.Empty,
                                   customer_name = leaseAppQuery.customer != null ? leaseAppQuery.customer.customer_name : string.Empty,
                                   leaseapp_rcvd_date = leaseAppQuery.leaseapp_rcvd_date,
                                   rogue_equipment_yn = leaseAppQuery.site != null ? leaseAppQuery.site.rogue_equipment_yn : false,
                                   lease_application_type = leaseAppQuery.leaseapp_types != null ? leaseAppQuery.leaseapp_types.lease_application_type : string.Empty,
                                   lease_application_status = leaseAppQuery.leaseapp_statuses != null ? leaseAppQuery.leaseapp_statuses.lease_application_status : string.Empty,
                                   site_status_desc = leaseAppQuery.site != null ? leaseAppQuery.site.site_status_desc : string.Empty,
                                   site_on_air_date = leaseAppQuery.site != null ? leaseAppQuery.site.site_on_air_date : null
                               }).ToList();
            }
            else
            {
               var desc = Utility.OrderByDescending(leaseApplicationsQuery, sortName);
               applications = (from leaseAppQuery in desc.Skip(skipItems).Take(this.ItemsPerPage)
                               select new LeaseAppWraper
                               {
                                   id = leaseAppQuery.id,
                                   site_uid = leaseAppQuery.site != null ? leaseAppQuery.site.site_uid : string.Empty,
                                   site_name = leaseAppQuery.site != null ? leaseAppQuery.site.site_name : string.Empty,
                                   original_carrier = leaseAppQuery.original_carrier,
                                   lease_app_activity = leaseAppQuery.leaseapp_activities.lease_app_activity != null ? leaseAppQuery.leaseapp_activities.lease_app_activity : string.Empty,
                                   customer_name = leaseAppQuery.customer != null ? leaseAppQuery.customer.customer_name : string.Empty,
                                   leaseapp_rcvd_date = leaseAppQuery.leaseapp_rcvd_date,
                                   rogue_equipment_yn = leaseAppQuery.site != null ? leaseAppQuery.site.rogue_equipment_yn : false,
                                   lease_application_type = leaseAppQuery.leaseapp_types != null ? leaseAppQuery.leaseapp_types.lease_application_type : string.Empty,
                                   lease_application_status = leaseAppQuery.leaseapp_statuses != null ? leaseAppQuery.leaseapp_statuses.lease_application_status : string.Empty,
                                   site_status_desc = leaseAppQuery.site != null ? leaseAppQuery.site.site_status_desc : string.Empty,
                                   site_on_air_date = leaseAppQuery.site != null ? leaseAppQuery.site.site_on_air_date : null
                               }).ToList();
            }

            foreach (var application in applications)
            {
                if (!queryFromListPage)
                {
                     result.Add(new KeyValuePair<string, object[]>
                                 (application.id.ToString(),
                                  new object[] 
                                    { 
                                       application.site_uid,
                                       application.site_name,
                                       application.customer_name,
                                       application.lease_application_type,
                                       Utility.DateToString(application.leaseapp_rcvd_date),
                                       application.lease_application_status,
                                       Site.HasTowerModifications(application.site_uid),
                                       application.rogue_equipment_yn.ToString(),
                                       RelatedWorkFlow.GetSiteWorkflowsList(application.site_uid),
                                       application.id.ToString()
                                    }
                                 )
                            );
                }
                else
                {
                     result.Add(new KeyValuePair<string, object[]>
                                 (application.id.ToString(),
                                  new object[]
                                    { 
                                       application.site_uid,
                                       application.site_name,
                                       application.customer_name,
                                       application.original_carrier,
                                       application.lease_application_type,
                                       application.lease_app_activity,
                                       Utility.DateToString(application.leaseapp_rcvd_date),
                                       application.lease_application_status,
                                       application.site_on_air_date != null ? Site.IsNotOnAir(application.site_on_air_date, application.site_status_desc) : true,
                                       application.site_uid,
                                       application.rogue_equipment_yn.ToString(),
                                       application.site_uid,
                                       application.site_uid,
                                       application.id.ToString() 
                                    }
                                 )
                            );
                }
            }
         }
         //>           
         return result;
      }
   }
}