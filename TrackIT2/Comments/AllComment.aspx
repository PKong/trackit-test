﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/TrackIT2.Master" AutoEventWireup="true" CodeBehind="AllComment.aspx.cs" Inherits="TrackIT2.Comment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   <!-- Styles -->
   <%: Styles.Render("~/Styles/all_comment") %>
   
    <script src="../Scripts/documentLink.js" type="text/javascript"></script>
    <script src="../Scripts/dms.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        // Doc Ready
        $(document).ready(function () {

            // Setup Comments Local
            var prmComments = Sys.WebForms.PageRequestManager.getInstance();
            prmComments.add_pageLoaded(setupCommentsLocal);

            //Setup Document link Dialog
            setupDocumentLinkDialog("Document Links");
            CreateCommentLinkDocument('MainContent_hidden_document_all_document_comments', 'document-comment-');
        });
        //-->

        // Setup Comments Local
        function setupCommentsLocal() {

            // Bind Table Sorting
            $(".all-comments-table").tablesorter({ headers: { 3: { sorter: false}} });

            // Bind Delete Comment Button Actions
            bindDeleteCommentButtonActions();

        }
        //-->
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="all-comments">
        <div style="float:left;">
            <h1 id="h2-comments" style="display:block;margin-bottom:10px;">
                All Comments - <span id="spnCommentsType" class="h1" runat="Server">&nbsp;</span> 
                <asp:HyperLink ID="lblSiteID" runat="server" Text="" CssClass="h1-span-site-link site-link-no-underline"></asp:HyperLink>
                <asp:Label ID="SiteID" runat="server" Text=""></asp:Label>

            </h1>
        </div>
        <a class="fancy-button" style="float:right;"  id="btnGoBack"  runat="server" >Back to Lease App</a>
        <div class="cb"></div>
        <div id="divNoComment" class="related-apps TableGenInfo" style="margin: 20px 20px 20px 20px;
            background: #f0f0f0;" runat="server" visible="false">
            <div style="margin: 0 auto 0 auto; width:100%; padding: 35px 35px 35px 35px; text-align: center; color: #888;">
                <em>
                    <asp:Label ID="lblNoComment" runat="server">No comments found.</asp:Label>
                </em>
            </div>
        </div>
        <asp:UpdatePanel id="upComments" updatemode="Always" runat="server">
            <ContentTemplate>
                <asp:Repeater ID="rptComments" runat="server">
                    <HeaderTemplate>
                        <table width="100%" cellspacing="0" cellpadding="0" class="sort-me all-comments-table">
	                        <thead>
		                        <tr>
			                        <th scope="col" style="width:70px;height:30px;">Date</th>
			                        <th scope="col" style="width:100px;">Area</th>
			                        <th scope="col" style="width:140px;">Employee</th>
			                        <th scope="col" style="width:630px;">&nbsp;</th>
		                        </tr>
	                        </thead>
                            <tbody>
                    </HeaderTemplate> 
                    <ItemTemplate>
                	            <tr>
						            <td><asp:Label id="lblDateCreated" runat="server" /></td>
						            <td><asp:Label id="lblArea" runat="server" /></td>
						            <td>
							            <div class="author-info">
                                            <asp:Image ID="imgEmployee"  Width="50" Height="50" ImageUrl="/images/avatars/default.png"
                                                ToolTip="Employee Profile Image" runat="server" />
								            <asp:Label id="lblEmployee" runat="server" />
							            </div>
						            </td>
						            <td>
							            <div class="comment">				
								            <div class="balloon">
									            <asp:Label id="lblComment" runat="server" />
									            <div class="balloon-left-side"></div>
								            </div>
                                            <div id="pnlDocument" runat="server" style="width:100%;text-align:left;" ></div>
                                            <asp:ImageButton id="btnDeleteComment" Width="12" ImageUrl="/images/icons/Erase-Greyscale.png"
                                                    ToolTip="Delete Comment" OnClick="btnDeleteComment_Click" CssClass="btn-comment-delete" runat="server" />
							            </div>
						            </td>
					            </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                	        </tbody>	
			            </table>
                    </FooterTemplate>
                </asp:Repeater>
                <asp:HiddenField ID="MainContent_hidden_document_all_document_comments" runat="server" ClientIDMode="Static" Value="" />
                <asp:HiddenField ID="hidDocumentDownload" runat="server" ClientIDMode="Static" /> 
                <div id="modalDocument" class="modal-container" style="overflow:hidden;"></div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <br /><br />
    </div>
</asp:Content>
