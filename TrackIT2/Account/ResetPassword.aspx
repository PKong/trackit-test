﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResetPassword.aspx.cs" Inherits="TrackIT2.Account.ResetPassword" Title="T-Mobile Towers TrackiT" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">    
   <title></title>

   <!-- Styles -->
   <%: Styles.Render("~/Styles/reset_password") %>	
</head>
<body>
	<div style="text-align: center;">
		<a href="/" title="Home">
			<img src="/images/logo.gif" alt="T-Mobile Towers" border="0" />
		</a>
        <h1 style="color:#6f7073;">TrackiT2</h1>
	</div>
	<form id="form1" runat="server">
	<asp:Panel ID="Panel6" runat="server" HorizontalAlign="Center">
		<asp:Label ID="Label1" runat="server" Text="" CssClass="Label_Message"></asp:Label>
	</asp:Panel>
	<asp:Panel ID="Panel1" runat="server" HorizontalAlign="Center" CssClass="Panel_Login">
		<asp:Panel ID="Panel2" runat="server" CssClass="Border_Login" HorizontalAlign="Left">
			<asp:PasswordRecovery ID="PasswordRecovery1" runat="server" 
            TextBoxStyle-CssClass="TextBox_Login" LabelStyle-CssClass="Label_Normal" 
            SubmitButtonText="Send me reset password instructions" 
            UserNameInstructionText="" UserNameLabelText="Email" UserNameTitleText="" 
            Width="100%" onverifyinguser="PasswordRecovery1_VerifyingUser">
				<LabelStyle CssClass="Label_Normal" />
				<TextBoxStyle CssClass="TextBox_Login" />
				<UserNameTemplate>
					<table cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 100%;">
						<tr>
							<td>
								<asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
							</td>
						</tr>
						<tr>
							<td class="Label_Normal">
								<asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">TMO User ID/Email</asp:Label>
							</td>
						</tr>
						<tr>
							<td>
								<asp:TextBox ID="UserName" runat="server" CssClass="TextBox_Login"></asp:TextBox>
								<asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" ErrorMessage="* TMO User ID/Email is Required" ToolTip="* TMO User ID/Email is Required" ValidationGroup="PasswordRecovery1" CssClass="error-text">* TMO User ID/Email is Required</asp:RequiredFieldValidator>
							</td>
						</tr>
						<tr>
							<td align="right">
								<asp:Button ID="SubmitButton" runat="server" CommandName="Submit" Text="Reset Password" ValidationGroup="PasswordRecovery1" CssClass="fancy-button" />
							</td>
						</tr>
					</table>
				</UserNameTemplate>
			</asp:PasswordRecovery>
		</asp:Panel>
		<asp:Panel ID="Panel5" runat="server" HorizontalAlign="Left" CssClass="Panel_Nav">
			<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Account/Login.aspx">Sign In</asp:HyperLink>
		</asp:Panel>
	</asp:Panel>
	</form>
</body>
</html>
