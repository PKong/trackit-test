﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.Objects;
using System.IO;
using System.Text;
using TrackIT2.BLL;

namespace TrackIT2.Account
{
    /// <summary>
    /// 
    /// </summary>
    public partial class SavedFiltersManage : System.Web.UI.Page
    {

        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Check Form Collection
            if (Request.Form.Count > 0)
            {
                // Create Saved Filter?
                if ((!String.IsNullOrEmpty(Request.Form["title"])) && (!String.IsNullOrEmpty(Request.Form["url"])) &&
                        (!String.IsNullOrEmpty(Request.Form["filter"])) && (!String.IsNullOrEmpty(Request.Form["listView"])))
                {

                    // Filter
                    string filter = Request.Form["filter"];
                    //
                    // Lease Apps - Types
                    if (!String.IsNullOrEmpty(Request.Form["pcFilter"])) filter += "&" + Request.Form["pcFilter"];
                    //>

                    // Create Saved Filter
                    CreateSavedFilter(Request.Form["title"], Request.Form["url"], filter, Request.Form["listView"]);
                }
                else if ((!String.IsNullOrEmpty(Request.Form["title"])) && (Request.Form["action"] == "delete"))
                {
                    // Delete Saved Filter
                    DeleteSavedFilter(Request.Form["title"]);
                }
                //>
            }
            //>
        }
        //-->

        /// <summary>
        /// Create Saved Filter
        /// </summary>
        /// <param name="newTitle"></param>
        /// <param name="newUrl"></param>
        /// <param name="newFilter"></param>
        /// <param name="newListView"></param>
        protected void CreateSavedFilter(string newTitle, string newUrl, string newFilter, string newListView)
        {
            UserProfile profile = UserProfile.GetUserProfile();
            TrackIT2.Objects.SavedFilters filters = profile.SavedFilters;
            SavedFilter newSavedFilter = new SavedFilter();

            bool titleAlreadyExists = false;

            if (filters.SavedFilterItems == null || filters.SavedFilterItems.Count == 0)
            {
                filters.SavedFilterItems = new List<SavedFilter>();
            }

            newSavedFilter.title = newTitle;
            newSavedFilter.url = newUrl;
            //newSavedFilter.filter = newFilter;
            newSavedFilter.filter = Utility.PrepareSqlSearchString(newFilter);
            newSavedFilter.listView = newListView;

            foreach (SavedFilter currFilter in filters.SavedFilterItems)
            {
                if (currFilter.title == newSavedFilter.title)
                {
                    titleAlreadyExists = true;
                    break;
                }
            }

            if (!titleAlreadyExists)
            {
                filters.SavedFilterItems.Add(newSavedFilter);
                profile.SavedFilters = filters;
                profile.Save();

                pageResponseContent(Globals.SUCCESS);
            }
            else
            {
                pageResponseContent(Globals.EXISTS);
            }
        }
        //-->

        /// <summary>
        /// Delete Saved Filter
        /// </summary>
        /// <param name="p"></param>
        private void DeleteSavedFilter(string title)
        {
            UserProfile profile = UserProfile.GetUserProfile();
            TrackIT2.Objects.SavedFilters filters = profile.SavedFilters;

            foreach (SavedFilter currFilter in filters.SavedFilterItems)
            {
                if (currFilter.title == title)
                {
                    filters.SavedFilterItems.Remove(currFilter);
                    profile.SavedFilters = filters;
                    profile.Save();
                    break;
                }
            }
            pageResponseContent(Globals.SUCCESS);
        }
        ///-->

        /// <summary>
        /// Page Response Content
        /// </summary>
        /// <param name="content"></param>
        protected void pageResponseContent(string message)
        {
            Response.Write(message);
            Response.Flush();
            Response.End();
        }
        //-->

        protected String CheckGlobalSearchString(string sFilter)
        {
            String sResult = "";
            String[] Filters = sFilter.Split('|');
            return sResult;
        }
    }
}