﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Elmah;
using TrackIT2.BLL;
using TrackIT2.DAL;
using TrackIT2.Objects;
using System.Text;
using System.Data;
using System.Collections;
using System.Globalization;

namespace TrackIT2.TowerModifications
{
    public partial class Edit : System.Web.UI.Page
    {        
        private const string TMP_DEFAULT_ID = "0";
        private const string ALL_COMMENTS_URL = "/Comments/AllComment.aspx?ct=tm&id=";
        private Hashtable m_htVarCharInvalidField;
        private bool towerModFound = true;

        public enum eTabName
        {
            General,
            TowerModPO,
            Other
        }

        public eTabName CurrentTab
        {
            get
            {
                eTabName ret = (eTabName)this.Session["cur_tab"];
                if (ret == eTabName.Other) CurrentTab = eTabName.General;
                return ret;
            }
            set
            {
                this.Session["cur_tab"] = value;
                this.Session["cur_tab_index"] = (int)value;
            }
        }

        public tower_modifications  CurrentTowerModifications
        {
            get
            {
                return (tower_modifications)this.ViewState["CurrentTowerModifications"];
            }
            set
            {
                this.ViewState["CurrentTowerModifications"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {            
            if (!Security.UserIsInRole("Administrator"))
            {
                btnTowerModDelete.Visible = false;
                btnTMPODelete.Visible = false;
            }

            if (!Page.IsPostBack)
            {
                int iTMID;
                bool isValidId = Int32.TryParse(Request.QueryString["Id"],
                                                out iTMID);
                Utility.ClearSubmitValidator();
                if (isValidId)
                {
                    if (TowerMod.Search(iTMID) != null)
                    {
                        Page.Title = "Edit Tower Modification - " + iTMID + " | TrackiT2";
                        this.CurrentTab = eTabName.General;
                        BindApplication(iTMID);
                        BindLookupLists();
                        BindingGeneralData();
                        CommentBinding();
                        //get document link
                        GetLinkDocument();
                        pnlNoID.Visible = false;
                        pnlSectionContent.Visible = true;
                        towerModFound = true;
                    }
                    else
                    {
                        // Page render event will write the 404 response code. This part
                        // of the code will display the No Id panel and hide the edit
                        // panel.
                        pnlSectionContent.Visible = false;
                        pnlNoID.Visible = true;
                        towerModFound = false;
                    }
                }
                else
                {
                   // Page render event will write the 404 response code. This part
                   // of the code will display the No Id panel and hide the edit
                   // panel.
                   pnlSectionContent.Visible = false;
                   pnlNoID.Visible = true;
                   towerModFound = false;
                }

                GetSessionFilter();
            }
            else
            {
               // Initialise Download from Amazon
               if (Request.Params["__EVENTARGUMENT"] == "DownloadDocument")
               {
                  string keyName = hidDocumentDownload.Value;
                  if (!string.IsNullOrEmpty(keyName))
                  {
                     try
                     {
                        AmazonDMS.DownloadObject(Server.UrlDecode(keyName));
                     }
                     catch (Amazon.S3.AmazonS3Exception ex)
                     {
                        this.hidden_popup.Value = ex.Message;

                        if (this.hiddenCurrentTab.Value == "PO")
                        {
                           this.CurrentTab = eTabName.TowerModPO;
                        }
                        else
                        {
                           this.CurrentTab = eTabName.General;
                        }
                        
                        Master.StatusBox.showErrorMessage(ex.Message);

                        int TowerModId = Int32.Parse(Request.QueryString["Id"]);
                        Page.Title = "Edit Tower Modification - " + TowerModId + " | TrackiT2";
                        BindApplication(TowerModId);
                        BindLookupLists();
                        BindingGeneralData();
                        CommentBinding();
                        //get document link
                        GetLinkDocument();
                        pnlNoID.Visible = false;
                        pnlSectionContent.Visible = true;
                        towerModFound = true;
                     }
                  }
               }
               //>
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
           base.Render(writer);

           if (!towerModFound)
           {
              Response.StatusCode = 404;
           }
        }

        #region Binding

        protected void BindApplication(int iTMID)
        {
            using (CPTTEntities ce = new CPTTEntities())
            {
                tower_modifications twMod = TowerMod.Search(iTMID);

                this.CurrentTowerModifications = twMod;
                ce.Attach(twMod);
                lblSiteID.Text = twMod.site_uid;
                if (!string.IsNullOrEmpty(twMod.site_uid))
                {
                    lblSiteID.NavigateUrl = Globals.SITE_LINK_URL + twMod.site_uid;
                }
                
                // Tower Image
                BindTowerImages(twMod.site_uid.ToString());

                //Site Informations
                if (twMod.site != null)
                {
                    Utility.ControlValueSetter(lblSiteLatitude, twMod.site.site_latitude, (0.0).ToString());

                    Utility.ControlValueSetter(lblDegreeLatitude, Utility.ConvertLatLongCoorToDegree(twMod.site.site_latitude), (0.0).ToString());
                    Utility.ControlValueSetter(lblSiteLongitude, twMod.site.site_longitude, (0.0).ToString());
                    Utility.ControlValueSetter(lblDegreeLongtitude, Utility.ConvertLatLongCoorToDegree(twMod.site.site_longitude), (0.0).ToString());

                    // Add Google Maps link to Latitude and Logitude
                    lblSiteLatLong.NavigateUrl = Utility.GetGooleMapLatLong(lblSiteLatitude.Text, lblSiteLongitude.Text).ToString();

                    Utility.ControlValueSetter(lblType, twMod.site.site_class_desc);
                    Utility.ControlValueSetter(lblHeight, twMod.site.structure_ht, (0).ToString());
                    Utility.ControlValueSetter(lblRogueEquip, twMod.site.rogue_equipment_yn, "None");
                    Utility.ControlValueSetter(lblStatus, twMod.site.site_status_desc);
                    if (BLL.Site.IsNotOnAir(twMod.site.site_on_air_date, twMod.site.site_status_desc))
                    {
                        imgStructureDetails_Construction.ImageUrl = "/images/icons/Construction.png";
                    }
                    else
                    {
                        imgStructureDetails_Construction.Visible = false;
                    }

                    if (twMod.site.address != null && twMod.site.city != null && twMod.site.state != null && twMod.site.zip != null && twMod.site.county != null)
                    {
                        //Link 0 = address,1 = city,2 = state,3 = zip,4 = country
                        string sAddressFormat = "{0}<br />{1}, {2}, {3}<br />{4}";
                        mybtn.Text = string.Format(sAddressFormat, twMod.site.address.ToString(), twMod.site.city
                            , twMod.site.state.ToString(), twMod.site.zip, twMod.site.county.ToString());
                        lblSiteName.Text = twMod.site.site_name;

                        // Google Maps Link
                        StringBuilder sbGoogleSearch = new StringBuilder();
                        sbGoogleSearch.Append(twMod.site.address.ToString());
                        if (!String.IsNullOrWhiteSpace(twMod.site.city)) sbGoogleSearch.Append(", " + twMod.site.city);
                        if (!String.IsNullOrWhiteSpace(twMod.site.state.ToString())) sbGoogleSearch.Append(", " + twMod.site.state.ToString());
                        if (!String.IsNullOrWhiteSpace(twMod.site.zip)) sbGoogleSearch.Append(", " + twMod.site.zip);
                        if (!String.IsNullOrWhiteSpace(twMod.site.county.ToString())) sbGoogleSearch.Append(", " + twMod.site.county.ToString());
                        mybtn.NavigateUrl = Utility.GetGoogleMapLink(sbGoogleSearch.ToString());

                    }
                    else
                    {
                        lblAddressNA.Text = "Site Address is not Available.";
                        lblAddressNA.Visible = true;
                        mybtn.Visible = false;
                    }
                }
                else
                {
                    Utility.ControlValueSetter(lblSiteLatitude, null, (0.0).ToString());
                    Utility.ControlValueSetter(lblSiteLongitude, null, (0.0).ToString());
                    Utility.ControlValueSetter(lblType, null,"");
                    Utility.ControlValueSetter(lblHeight, null, (0).ToString());
                    Utility.ControlValueSetter(lblRogueEquip, null, "None");
                    Utility.ControlValueSetter(lblStatus, null,"");
                    mybtn.Text = "N/A";
                    lblSiteName.Text = "N/A";
                    mybtn.NavigateUrl = "";
                }


                
                
                
                BindingPowerModPO();
                
                divNoRelatedLeaseApps.Visible = false;
                //Prepare data to bind with grid.
                List<LeaseAppTowerModInfo> lstResult = LeaseAppTowerMod.GetRalatedLeaseApp(twMod.id);
                if (lstResult.Count == 0)
                {
                    divNoRelatedLeaseApps.Visible = true;
                    if (twMod.customer != null)
                    {
                        Utility.ControlValueSetter(lblCustomerName, twMod.customer.customer_name, "(no customer found)");
                    }
                    else
                    {
                        Utility.ControlValueSetter(lblCustomerName, null, "(no customer found)");
                    }
                }
                else
                {
                    ddlCustomer.Visible = false;
                    lblCustomer.Visible = false;
                    String sCustomerNames = "";
                    foreach (LeaseAppTowerModInfo item in lstResult)
                    {
                        if (item.CustomerName != null)
                        {
                            if (sCustomerNames == "")
                            {
                                sCustomerNames += item.CustomerName;
                            }
                            else
                            {
                                sCustomerNames += ", " + item.CustomerName;
                            }
                        }
                    }
                    if (sCustomerNames == "") sCustomerNames = null;
                    Utility.ControlValueSetter(lblCustomerName, sCustomerNames, "(no customer found)");
                }
               //Binding data to grid.
                grdMain.DataSource = lstResult;
                grdMain.DataBind();

                btnAllComment.HRef = ALL_COMMENTS_URL + twMod.id;
                //Detach object.
                ce.Detach(twMod);
            }
            
        }

        protected void BindLookupLists()
        {
            using(CPTTEntities ce = new CPTTEntities())
            {
                //General

                Utility.BindDDLDataSource(this.ddlCustomer, ce.customers.OrderBy(Utility.GetField<customer>("customer_name")).OrderBy(Utility.GetIntField<customer>("sort_order")),
                    "id", "customer_name", "- Select -");
                Utility.BindDDLDataSource(this.ddlPayor, ce.customers.OrderBy(Utility.GetField<customer>("customer_name")).OrderBy(Utility.GetIntField<customer>("sort_order")), 
                    "id", "customer_name", "- Select -");
                
                List<tower_mod_statuses> TowerModStatus = ce.tower_mod_statuses.ToList<tower_mod_statuses>();
                var SortTowerModStatus = new DynamicComparer<tower_mod_statuses>();
                SortTowerModStatus.SortOrder(x => x.tower_mod_status_name);
                TowerModStatus.Sort(SortTowerModStatus);
                Utility.BindDDLDataSource(this.ddlTowerModStat, TowerModStatus, "id", "tower_mod_status_name", "- Select -");

                List<tower_mod_types> TowerModType = ce.tower_mod_types.ToList<tower_mod_types>();
                var SortTowerModType = new DynamicComparer<tower_mod_types>();
                SortTowerModType.SortOrder(x => x.tower_mod_type);
                TowerModType.Sort(SortTowerModType);
                Utility.BindDDLDataSource(this.ddlTowerModType, TowerModType, "id", "tower_mod_type", "- Select -");
                
                List<date_statuses> DateStatuses = ce.date_statuses.ToList<date_statuses>();
                var SortDateStatuses = new DynamicComparer<date_statuses>();
                SortDateStatuses.SortOrder(x => x.date_status);
                DateStatuses.Sort(SortDateStatuses);
                Utility.BindDDLDataSource(this.ddlPermitReceived, DateStatuses, "id", "date_status", "- Select -");
                Utility.BindDDLDataSource(this.ddlMaterialsReceived, DateStatuses, "id", "date_status", "- Select -");
                Utility.BindDDLDataSource(this.ddlWorkStart, DateStatuses, "id", "date_status", "- Select -");
                Utility.BindDDLDataSource(this.ddlWorkCompletion, DateStatuses, "id", "date_status", "- Select -");
                Utility.BindDDLDataSource(this.ddlInspection, DateStatuses, "id", "date_status", "- Select -");

                List<UserInfo> lstUser = BLL.User.GetUserInfo();
                var SortUser = new DynamicComparer<UserInfo>();
                SortUser.SortOrder(x => x.FullName);
                lstUser.Sort(SortUser);
                Utility.BindDDLDataSource(this.ddlPMEmp, lstUser, "ID", "FullName", "- Select -");
                
                List<tower_mod_phase_types> towerModPhaseTypes = ce.tower_mod_phase_types.ToList();
                var SortTowerModPhaseTypes = new DynamicComparer<tower_mod_phase_types>();
                SortTowerModPhaseTypes.SortOrder(x => x.phase_name);
                towerModPhaseTypes.Sort(SortTowerModPhaseTypes);
                Utility.BindDDLDataSource(this.ddlTowerModPhase1, towerModPhaseTypes, "ID", "phase_name", "- Select -");

                //Utility.BindDDLDataSource(this.ddlGeneralContractor1,
                //    ce.general_contractors.OrderBy(Utility.GetField<general_contractors>("general_contractor_name")), 
                //    "id", "general_contractor_name", "- Select -");
            }

        }

        protected void BindingGeneralData()
        {
            tower_modifications twMod = this.CurrentTowerModifications;
            if (twMod != null)
            {
                Utility.ControlValueSetter(this.ddlCustomer, twMod.customer_id);
                Utility.ControlValueSetter(this.ddlPayor, twMod.payor_id);
                Utility.ControlValueSetter(this.ddlTowerModStat, twMod.tower_mod_status_id);
                Utility.ControlValueSetter(this.ddlTowerModType, twMod.tower_mod_type_id);
                Utility.ControlValueSetter(this.txtTowerHeight, twMod.tower_height_proposed_ft);
                Utility.ControlValueSetter(this.txtCapacity, twMod.postmod_structural_capacity_pct);
                Utility.ControlValueSetter(this.ddlRegNotification, twMod.regulatory_notification_yn);
                Utility.ControlValueSetter(this.ddlPermitCarrier, twMod.building_permit_from_carrier_yn);
                Utility.ControlValueSetter(this.txtPermitReceived, twMod.building_permit_rcvd_date);
                Utility.ControlValueSetter(this.ddlPermitReceived, twMod.building_permit_rcvd_date_status_id);
                Utility.ControlValueSetter(this.ddlTopOutNotificationReq, twMod.top_out_notification_required_yn);
                Utility.ControlValueSetter(this.txtMaterialsOrdered, twMod.materials_ordered_date);
                Utility.ControlValueSetter(this.txtMaterialsReceived, twMod.materials_rcvd_date);
                Utility.ControlValueSetter(this.ddlMaterialsReceived, twMod.materials_rcvd_date_status_id);
                Utility.ControlValueSetter(this.txtWorkStart, twMod.work_start_date);
                Utility.ControlValueSetter(this.ddlWorkStart, twMod.work_start_date_status_id);
                Utility.ControlValueSetter(this.txtWorkCompletion, twMod.work_completion_date);
                Utility.ControlValueSetter(this.ddlWorkCompletion, twMod.work_completion_date_status_id);
                Utility.ControlValueSetter(this.txtInspection, twMod.third_party_inspection_date);
                Utility.ControlValueSetter(this.ddlInspection, twMod.third_party_inspection_date_status_id);
                Utility.ControlValueSetter(this.ddlPMEmp, twMod.tower_mod_pm_emp_id);
                Utility.ControlValueSetter(this.txtPackageSent, twMod.tower_mod_packet_sent_date);
            }
        }

        protected void BindingPowerModPO()
        {
            tower_modifications tm = this.CurrentTowerModifications;
            if(tm != null)
            {
                string json = "";
                int count = 0;
                using (CPTTEntities context = new CPTTEntities())
                {
                    //context.Attach(tm);
                    //Select only required field to prevent disposed object crash.
                    count = TowerModPhase.GetJSON(false, out json, tm.id);
                    
                    hidden_TM.Value = json;
                    hidden_TM_count.Value = count.ToString();
                    String jsonEmpty = "";
                    TowerModPhase.GetJSON(true, out jsonEmpty);
                    hidden_TM_template.Value = jsonEmpty;
                    
                }
            }
        }

        protected void CommentBinding(Boolean isGetAllReleate = false)
        {
            tower_modifications tm = this.CurrentTowerModifications;
            if (tm != null)
            {
                divComment1.Visible = false;
                divComment2.Visible = false;
                divComment3.Visible = false;
                divComment4.Visible = false;
                btnAllComment.Visible = false;

                List<AllComments> listComments = AllCommentsHelper.GetAllReleateTowerModComment(tm.id, false, true);
                AllComments currentComment;

                //Temp use before finding the way to iterate web UI control.
                System.Web.UI.HtmlControls.HtmlGenericControl pCommenter;
                System.Web.UI.HtmlControls.HtmlGenericControl divComment;
                System.Web.UI.HtmlControls.HtmlGenericControl pComment;
                System.Web.UI.HtmlControls.HtmlGenericControl pDocument;
                Image imgComment;
                ImageButton btnDeleteComment;
                List<System.Web.UI.HtmlControls.HtmlGenericControl> lstPCommenter = new List<System.Web.UI.HtmlControls.HtmlGenericControl>();
                List<System.Web.UI.HtmlControls.HtmlGenericControl> lstPComments = new List<System.Web.UI.HtmlControls.HtmlGenericControl>();
                List<System.Web.UI.HtmlControls.HtmlGenericControl> lstDivComment = new List<System.Web.UI.HtmlControls.HtmlGenericControl>();
                List<System.Web.UI.HtmlControls.HtmlGenericControl> lstDivDocument = new List<System.Web.UI.HtmlControls.HtmlGenericControl>();
                List<ImageButton> lstBtnDelete = new List<ImageButton>();
                List<Image> lstImageAvatar = new List<Image>();
                lstPCommenter.Add(pCommenter1);
                lstPCommenter.Add(pCommenter2);
                lstPCommenter.Add(pCommenter3);
                lstPCommenter.Add(pCommenter4);
                lstPComments.Add(pComment1);
                lstPComments.Add(pComment2);
                lstPComments.Add(pComment3);
                lstPComments.Add(pComment4);
                lstDivComment.Add(divComment1);
                lstDivComment.Add(divComment2);
                lstDivComment.Add(divComment3);
                lstDivComment.Add(divComment4);
                lstBtnDelete.Add(btnDeleteComment1);
                lstBtnDelete.Add(btnDeleteComment2);
                lstBtnDelete.Add(btnDeleteComment3);
                lstBtnDelete.Add(btnDeleteComment4);
                lstImageAvatar.Add(imgUserComment1);
                lstImageAvatar.Add(imgUserComment2);
                lstImageAvatar.Add(imgUserComment3);
                lstImageAvatar.Add(imgUserComment4);
                lstDivDocument.Add(pnlDocument1);
                lstDivDocument.Add(pnlDocument2);
                lstDivDocument.Add(pnlDocument3);
                lstDivDocument.Add(pnlDocument4);
                user objUser = null;

                hidden_count_main_comment.Value = listComments.Count.ToString();
                if (listComments.Count > 0)
                {
                    btnAllComment.Visible = true;
                    btnAllComment.InnerText = String.Format("see all {0} comments", listComments.Count);
                    if (!btnAllComment.HRef.Contains("showAllReleate"))
                    {
                        btnAllRelateComment.Visible = true;
                    }
                }
                btnAllRelateComment.Visible = true;

                if (!isGetAllReleate)
                {
                    var focusComment = from listComment in listComments
                                       where (listComment.Source.Equals(AllComments.TOWER_MOD) && listComment.TowerModID == tm.id)
                                       && listComment.TowerModID == tm.id
                                       select listComment;
                    listComments = focusComment.AsQueryable().OrderByDescending(a => a.CreateAt).ToList();
                }
                else
                {
                    var focusComment = from listComment in listComments
                                       where (listComment.Source.Equals(AllComments.TOWER_MOD) && listComment.TowerModID == tm.id)
                                       || listComment.Source.Equals(AllComments.LEASE_ADMIN)
                                       || listComment.Source.Equals(AllComments.LEASE_APPLICATION)
                                       || listComment.Source.Equals(AllComments.STRUCTURAL_ANALYSIS)
                                       select listComment;
                    listComments = focusComment.AsQueryable().OrderByDescending(a => a.CreateAt).ToList();
                }

                string commenter;
                string sCommenterFormat = "{0}, {1}";//name , date
                string sImageUrl = "";

                GetAllCommentLinkDocument(listComments);

                for (int i = 0; i < listComments.Count && i < 4; i++)
                {
                    currentComment = listComments[i];
                    pCommenter = lstPCommenter[i];
                    pComment = lstPComments[i];
                    divComment = lstDivComment[i];
                    btnDeleteComment = lstBtnDelete[i];
                    imgComment = lstImageAvatar[i];
                    pDocument = lstDivDocument[i];
                    if (!string.IsNullOrEmpty(currentComment.CreatorName))
                    {
                        commenter = string.Format(CultureInfo.CurrentCulture, sCommenterFormat, currentComment.CreatorName,
                                                  Utility.DateToString(currentComment.CreateAt));
                    }
                    else
                    {
                        commenter = string.Format(CultureInfo.CurrentCulture, sCommenterFormat, "No employee recorded",
                                                  Utility.DateToString(currentComment.CreateAt));
                    }


                    sImageUrl = BLL.User.GetUserAvatarImage(currentComment.CreatorUsername);

                    if (!Security.UserCanRemoveComment(User.Identity.Name, (int)Session["userId"], currentComment.CommentID, currentComment.Source))
                    {
                        btnDeleteComment.Visible = false;
                    }

                    imgComment.ImageUrl = sImageUrl;
                    pCommenter.InnerText = commenter;
                    divComment.Visible = true;
                    if (currentComment.Mode == "eTM" && currentComment.TowerModID == tm.id)
                    {
                        pComment.InnerHtml = currentComment.Comments;
                    }
                    else
                    {
                        pComment.InnerHtml = currentComment.CheckTMAndSdp;
                    }
                    btnDeleteComment.AlternateText = currentComment.Source + "/" + currentComment.CommentID.ToString();

                    if (currentComment.Mode == "eLA" || currentComment.Mode == "eSA" || currentComment.Mode == "eTM")
                    {
                        pDocument.Attributes.Add("class", "document-comment-" + currentComment.CommentID);
                    }

                    int commentID = listComments[i].CommentID;

                    if (divComment.Attributes["saCommentId"] != null)
                    {
                        divComment.Attributes.Remove("saCommentId");
                    }

                    if (divComment.Attributes["leaseAdminCommentId"] != null)
                    {
                        divComment.Attributes.Remove("leaseAdminCommentId");
                    }

                    if (listComments[i].Mode == "eSA")
                    {
                        divComment.Attributes.Add("saCommentId", "SA_" + commentID);
                    }
                    else if (listComments[i].Mode == "eLE")
                    {
                        divComment.Attributes.Add("leaseAdminCommentId", "LeaseAdmin_" + commentID);
                    }
                }
            }
        }

        protected void BindTowerImages(string siteId)
        {
            DMSHelper imageHelper = new DMSHelper();
            List<PhotoInfo> siteImages = new List<PhotoInfo>();

            siteImages = imageHelper.GetSitePhotos(siteId, false, string.Empty);
            Image2.ImageUrl = siteImages[0].photoURL;
            Image2.AlternateText = siteImages[0].photoID;
        }

        #endregion

        #region SaveButtonClicked
        protected void SaveTM()
        {
            if (Page.IsValid)
            {
                tower_modifications twMod = this.CurrentTowerModifications;
                if (twMod != null)
                {
                    twMod.customer_id = Utility.PrepareInt(ddlCustomer.SelectedValue);
                    twMod.payor_id = Utility.PrepareInt(ddlPayor.SelectedValue);
                    twMod.tower_mod_status_id = Utility.PrepareInt(ddlTowerModStat.SelectedValue);
                    twMod.tower_mod_type_id = Utility.PrepareInt(ddlTowerModType.SelectedValue);
                    twMod.tower_height_proposed_ft = Utility.PrepareDouble(txtTowerHeight.Text);
                    twMod.postmod_structural_capacity_pct = Utility.PrepareDouble(txtCapacity.Text);
                    twMod.regulatory_notification_yn = Utility.ConvertYNToBoolean(Utility.PrepareString(ddlRegNotification.SelectedValue));
                    twMod.building_permit_from_carrier_yn = Utility.ConvertYNToBoolean(Utility.PrepareString(ddlPermitCarrier.SelectedValue));
                    twMod.building_permit_rcvd_date = Utility.PrepareDate(txtPermitReceived.Text);
                    twMod.building_permit_rcvd_date_status_id = Utility.PrepareInt(ddlPermitReceived.SelectedValue);
                    twMod.top_out_notification_required_yn = Utility.ConvertYNToBoolean(Utility.PrepareString(ddlTopOutNotificationReq.SelectedValue));
                    twMod.materials_ordered_date = Utility.PrepareDate(txtMaterialsOrdered.Text);
                    twMod.materials_rcvd_date = Utility.PrepareDate(txtMaterialsReceived.Text);
                    twMod.materials_rcvd_date_status_id = Utility.PrepareInt(ddlMaterialsReceived.SelectedValue);
                    twMod.work_start_date = Utility.PrepareDate(txtWorkStart.Text);
                    twMod.work_start_date_status_id = Utility.PrepareInt(ddlWorkStart.SelectedValue);
                    twMod.work_completion_date = Utility.PrepareDate(txtWorkCompletion.Text);
                    twMod.work_completion_date_status_id = Utility.PrepareInt(ddlWorkCompletion.SelectedValue);
                    twMod.third_party_inspection_date = Utility.PrepareDate(txtInspection.Text);
                    twMod.third_party_inspection_date_status_id = Utility.PrepareInt(ddlInspection.SelectedValue);
                    twMod.tower_mod_packet_sent_date = Utility.PrepareDate(txtPackageSent.Text);
                    twMod.tower_mod_pm_emp_id = Utility.PrepareInt(ddlPMEmp.SelectedValue);

                    SavePO();

                    try
                    {
                        TowerMod.Update(twMod);
                        BindApplication(twMod.id);
                        CurrentTab = eTabName.General;
                        SaveTMOLinkDocument(twMod.id);
                        GetLinkDocument();
                        Master.StatusBox.showStatusMessage("Tower Modifications Updated.");
                    }
                    catch (OptimisticConcurrencyException oex)
                    {
                       // Log error in ELMAH for any diagnostic needs.
                       ErrorSignal.FromCurrentContext().Raise(oex);

                       Master.StatusBox.showErrorMessage
                                          ("The current record has been modifed since you have " +
                                           "last retrieved it. Please reload the record and " +
                                           "attempt to save it again.");
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                        //Show msgbox if needed.
                    }

                }
            }
        }
        protected void SavePO()
        {
            tower_mod_pos objTMP = null;
            Boolean blnIsValid = true;
            m_htVarCharInvalidField = new Hashtable();

            List<TowerModPOTemplate> lstResult = new List<TowerModPOTemplate>();
            if(String.IsNullOrEmpty(hidden_TM.Value))
            {
                lstResult = TowerModPhase.ParseFromJSON(hidden_TM_template.Value);
            }
            else
            {
                lstResult = TowerModPhase.ParseFromJSON(hidden_TM.Value);
            }
            using (CPTTEntities ce = new CPTTEntities())
            {
                Boolean blnIsError = false;
                foreach (TowerModPOTemplate item in lstResult)
                {
                    if (blnIsError)
                  {
                      break;
                  }
                    if (item.CheckIsNull())
                    {
                        //Skip Null item
                        //continue;
                    }
                    //Check for update or add new
                    if (item.ID != TMP_DEFAULT_ID)
                    {
                        //Update
                        int id;
                        if (int.TryParse(item.ID, out id))
                        {

                            var query = from sa in ce.tower_mod_pos
                                        where sa.id == id
                                        select sa;
                            if (query.ToList().Count > 0)
                            {
                                objTMP = query.ToList()[0];
                                ce.Detach(objTMP);
                            }
                        }
                    }
                    else
                    {
                        //Add new
                        objTMP = new tower_mod_pos();
                        objTMP.tower_modification_id = CurrentTowerModifications.id;    //TODO: Checking for error later
                    }
                    if (objTMP != null)
                    {

                        objTMP.tower_mod_phase_type_id = Utility.PrepareInt(item.tower_mod_phase_type_id);
                        objTMP.ebid_req_rcvd_date = Utility.PrepareDate(item.ebid_req_rcvd_date);
                        objTMP.ebid_packet_ready_date = Utility.PrepareDate(item.ebid_packet_ready_date);
                        objTMP.ebid_scheduled_published_date = Utility.PrepareDate(item.ebid_scheduled_published_date);
                        objTMP.ebid_scheduled_close_date = Utility.PrepareDate(item.ebid_scheduled_close_date);
                        objTMP.ebid_maturity_date = Utility.PrepareDate(item.ebid_maturity_date);
                        objTMP.ebid_comment = Utility.PrepareString(item.ebid_comment);
                        objTMP.estimated_construction_cost_amount = Utility.PrepareDecimal(item.estimated_construction_cost_amount);
                        objTMP.bid_amount = Utility.PrepareDecimal(item.bid_amount);
                        objTMP.tax_amount = Utility.PrepareDecimal(item.tax_amount);
                        objTMP.markup_amount = Utility.PrepareDecimal(item.markup_amount);
                        objTMP.total_from_customer_amount = Utility.PrepareDecimal(item.total_from_customer_amount);
                        objTMP.general_contractor_id = GeneralContractor.GetIDByName(Utility.PrepareString(item.general_contractor_name));
                        objTMP.sent_to_cs_pm_date = Utility.PrepareDate(item.sent_to_cs_pm_date);
                        objTMP.po_to_gc_requested_date = Utility.PrepareDate(item.po_to_gc_requested_date);

                        blnIsValid = blnIsValid & Utility.ValidateVarchar(Utility.PrepareString(item.shopping_cart_nbr), objTMP, m_htVarCharInvalidField, "shopping_cart_nbr", "Shopping Cart Number");
                        
                       if (blnIsValid)
                        {
                           objTMP.shopping_cart_nbr = Utility.PrepareString(item.shopping_cart_nbr);
                        }

                        objTMP.po_to_gc_rcvd_date = Utility.PrepareDate(item.po_to_gc_rcvd_date);
                        blnIsValid = blnIsValid & Utility.ValidateVarchar(Utility.PrepareString(item.po_nbr), objTMP, m_htVarCharInvalidField, "po_nbr", "PO Number");

                        if (blnIsValid)
                        {
                           objTMP.po_nbr = Utility.PrepareString(item.po_nbr);
                        }

                        if (blnIsValid)
                        {
                            if (item.ID != TMP_DEFAULT_ID)
                            {
                                //Update.
                                try
                                {
                                    ce.AttachUpdated(objTMP);
                                    ce.SaveChanges();

                                    hidden_popup.Value = "Tower Modifications Updated.";
                                }
                                catch (ObjectNotFoundException)
                                {
                                    blnIsError = true;
                                    throw;
                                    //Done some error notification here. And show msgbox if needed.
                                }
                                catch (Exception)
                                {
                                    blnIsError = true;
                                    throw;
                                    //Show msgbox if needed.
                                }
                            }
                            else
                            {
                                //Add.
                                TowerModPhase.Add(objTMP);
                                BindApplication(CurrentTowerModifications.id);
                                hidden_popup.Value = "Tower Modifications Updated.";
                            }
                        }
                        else
                        {
                            //Invalid varchar input.
                            StringBuilder sErrorMsg = new StringBuilder(Globals.ERROR_HEADER);
                            foreach (string itemVC in m_htVarCharInvalidField.Keys)
                            {
                                if (m_htVarCharInvalidField[itemVC] != null)
                                {
                                    sErrorMsg.AppendLine(String.Format(Globals.ERROR_INVALID_LENGTH_FORMAT, item, m_htVarCharInvalidField[itemVC]));
                                }
                            }
                            hidden_popup.Value = sErrorMsg.ToString();
                        }
                    }
                    else
                    {
                        //Error occur.
                    }
                }

                
            }
        }

        protected void btnSummitTowerModPOs1_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                SaveTM();
                this.CurrentTab = eTabName.TowerModPO;
            }
        }

        protected void btnSubmitSummary_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                 SaveTM();
                 this.CurrentTab = eTabName.General;
            }
        }

        protected void btnPost_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                tower_modifications twMod = this.CurrentTowerModifications;
                if (twMod != null)
                {
                    tower_mod_comments objComment = new tower_mod_comments();
                    objComment.comment = Server.HtmlEncode(txtComments.Text);
                    objComment.tower_modification_id = twMod.id;    
                    TowerModComment.Add(objComment);
                    BindApplication(twMod.id);
                    CommentBinding();
                }
            }
        }

        protected void btnDeleteComment_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                ImageButton objRef = sender as ImageButton;
                int id;
                string[] sTmp;
                string source;

                if (objRef != null && objRef.AlternateText != "")
                {
                    sTmp = objRef.AlternateText.Split('/');
                    if (sTmp.Length == 2)
                    {
                        source = sTmp[0];
                        id = int.Parse(sTmp[1]);
                        if (source == AllComments.LEASE_APPLICATION)
                        {
                            LeaseAppComment.Delete(id);
                        }
                        else if (source == AllComments.LEASE_ADMIN)
                        {
                            LeaseAdminComments.Delete(id);
                            // remove another leaseAdmin comment dialog
                        }
                        else if (source == AllComments.SDP)
                        {
                            SdpComment.Delete(id);
                            // remove another sdp comment dialog
                        }
                        else if (source == AllComments.TOWER_MOD)
                        {
                            TowerModComment.Delete(id);
                            // remove another towerMod comment dialog
                        }
                        else
                        {
                            LeaseAppSAComment.Delete(id);
                            // remove another sa comment dialog
                        }
                    }

                    BindApplication(CurrentTowerModifications.id);
                    CommentBinding();
                    Master.StatusBox.showStatusMessage("Comment Deleted.");
                }
            }
        }

        protected void btnAllRelateComment_Click(object sender, EventArgs e)
        {
            if (btnAllRelateComment.Text == "Hide related comments")
            {
                btnAllRelateComment.Text = "Show related comments";
                CommentBinding();
            }
            else
            {
                btnAllRelateComment.Text = "Hide related comments";
                CommentBinding(true);
            }
        }

        #endregion

        protected void grdMain_Command(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {

            }
        }

        protected void btnTowerModDelete_Click(object sender, EventArgs e)
        {
            String sError;
            tower_modifications tm = CurrentTowerModifications;
            if (tm != null)
            {
                sError = BLL.TowerMod.Delete(tm.id);
                if (sError == null)
                {
                    //Successful delete.
                   Master.StatusBox.showStatusMessageWithRedirect("Record deleted.", "Default.aspx");
                }
                else
                {
                    //Error occur.
                    Master.StatusBox.showStatusMessage("Error: some related records cannot be deleted; operation aborted.");
                    //TODO : Added error detail later. 
                }
            }
        }

        protected void btnTMPODelete_Click(object sender, EventArgs e)
        {
            String sError;
            int iTMPOs;
            //Checking for deletable page
            if (hidden_TM_ID.Value != TMP_DEFAULT_ID)
            {
                if (int.TryParse(hidden_TM_ID.Value, out iTMPOs))
                {
                    sError = BLL.TowerModPhase.Delete(iTMPOs);
                    if (sError == null)
                    {
                        //Successful delete.
                        BindApplication(CurrentTowerModifications.id);
                        this.CurrentTab = eTabName.TowerModPO;
                        Master.StatusBox.showStatusMessage("Tower Modification Process Removed.");
                    }
                    else
                    {
                        //Error occur.
                        Master.StatusBox.showStatusMessage("Error: Some related records cannot be deleted; operation aborted.");
                        //TODO : Added error detail later. 
                    }
                }
            }
        }

        #region Documnent Link
       
        private void GetAllCommentLinkDocument(List<AllComments> lstComment)
        {
            this.hidden_document_all_document_comments.Value = String.Empty;
            List<List<Dms_Document>> allComments = new List<List<Dms_Document>>();
            List<AllComments> allcomments = lstComment;
            foreach (AllComments comment in allcomments)
            {
                DMSDocumentLinkHelper.eDocumentField efield;
                if (comment.Mode == "eSA")
                {
                    efield = DMSDocumentLinkHelper.eDocumentField.SA_Comments;
                }
                else if (comment.Mode == "eLE")
                {
                    //Lease admin set field null because not document link
                    efield = DMSDocumentLinkHelper.eDocumentField.LA_Comments;
                }
                else if (comment.Mode == "eTM")
                {
                    efield = DMSDocumentLinkHelper.eDocumentField.TMO_Comments;
                }
                else if (comment.Mode == "eSDP")
                {
                    efield = DMSDocumentLinkHelper.eDocumentField.SDP_Comments;
                }
                else
                {
                    efield = DMSDocumentLinkHelper.eDocumentField.LE_Comments;
                }
                if (efield != DMSDocumentLinkHelper.eDocumentField.LA_Comments && efield != DMSDocumentLinkHelper.eDocumentField.SDP_Comments)
                {
                    List<Dms_Document> docData = DMSDocumentLinkHelper.GetDocumentLink(comment.CommentID, DMSDocumentLinkHelper.DocumentField(efield.ToString()));
                    if (docData.Count == 0)
                    {
                        Dms_Document doc = new Dms_Document();
                        doc.ID = comment.CommentID;
                        doc.ref_field = efield.ToString();
                        docData.Add(doc);
                    }
                    allComments.Add(docData);
                }
            }
            this.hidden_document_all_document_comments.Value = Newtonsoft.Json.JsonConvert.SerializeObject(allComments);
        }
        private void GetLinkDocument()
        {
            tower_modifications twMod = this.CurrentTowerModifications;

            List<Dms_Document> TowerModPacketSentDate = DMSDocumentLinkHelper.GetDocumentLink(twMod.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.TowerModPacketSentDate.ToString()));
            SetLinkDocument(twMod.id, DMSDocumentLinkHelper.eDocumentField.TowerModPacketSentDate, TowerModPacketSentDate);

            List<Dms_Document> BuildingPermitRcvdDates = DMSDocumentLinkHelper.GetDocumentLink(twMod.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.BuildingPermitRcvdDate.ToString()));
            SetLinkDocument(twMod.id, DMSDocumentLinkHelper.eDocumentField.BuildingPermitRcvdDate, BuildingPermitRcvdDates);
            
            List<TowerModPOTemplate> lstResult = new List<TowerModPOTemplate>();
            lstResult = TowerModPhase.ParseFromJSON(hidden_TM.Value);
            this.hidden_document_EbidComment.Value = String.Empty;
            this.hidden_document_BidAmount.Value = String.Empty;
            if (lstResult.Count > 0)
            {
                foreach (TowerModPOTemplate tmo in lstResult)
                {
                    List<Dms_Document> EbidComment = DMSDocumentLinkHelper.GetDocumentLink(int.Parse(tmo.ID), DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.EbidComment.ToString()));
                    SetLinkDocument(int.Parse(tmo.ID), DMSDocumentLinkHelper.eDocumentField.EbidComment, EbidComment);

                    List<Dms_Document> BidAmount = DMSDocumentLinkHelper.GetDocumentLink(int.Parse(tmo.ID), DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.BidAmount.ToString()));
                    SetLinkDocument(int.Parse(tmo.ID), DMSDocumentLinkHelper.eDocumentField.BidAmount, BidAmount);
                }
            }
            else
            {
                SetLinkDocument(0, DMSDocumentLinkHelper.eDocumentField.EbidComment, new List<Dms_Document>());
                SetLinkDocument(0, DMSDocumentLinkHelper.eDocumentField.BidAmount, new List<Dms_Document>());
            }
        }

        private void SetLinkDocument(int ID, DMSDocumentLinkHelper.eDocumentField field, List<Dms_Document> docData)
        {
            //insert template data in case no documents link
            if (docData.Count == 0)
            {
                Dms_Document doc = new Dms_Document();
                doc.ID = ID;
                doc.ref_field = field.ToString();
                docData.Add(doc);
            }

            switch (field)
            { 
                case DMSDocumentLinkHelper.eDocumentField.TowerModPacketSentDate:
                    this.hidden_document_TowerModPacketSentDate.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
                case DMSDocumentLinkHelper.eDocumentField.BuildingPermitRcvdDate:
                    this.hidden_document_BuildingPermitRcvdDate.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
                case DMSDocumentLinkHelper.eDocumentField.EbidComment:
                    if (!string.IsNullOrEmpty(hidden_document_EbidComment.Value))
                    {
                        List<Dms_Document> EbidComment = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(hidden_document_EbidComment.Value);
                        EbidComment.AddRange(docData);
                        docData = EbidComment;
                    }
                    this.hidden_document_EbidComment.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
                case DMSDocumentLinkHelper.eDocumentField.BidAmount:
                    if (!string.IsNullOrEmpty(hidden_document_BidAmount.Value))
                    {
                        List<Dms_Document> BidAmount = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(hidden_document_BidAmount.Value);
                        BidAmount.AddRange(docData);
                        docData = BidAmount;
                    }
                    this.hidden_document_BidAmount.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
                default:
                    // do the defalut action
                    break;
            }
        }

        protected void DocumentDownload(object sender, EventArgs e)
        {
            LinkButton obj = (LinkButton)sender;
            string keyName = obj.Attributes["key"];
            if (!string.IsNullOrEmpty(keyName))
            {
                AmazonDMS.DownloadObject(keyName);
            }
        }

        private Dms_Document updateNewDocumentID(Dms_Document document, int newID)
        {
            document.ID = newID;
            return document;
        }
        protected void SaveTMOLinkDocument(int tmoid)
        {
            int TMOPOid = GetTMOPOID(tmoid);
            List<Dms_Document> jsonDocData = new List<Dms_Document>();

            if (!string.IsNullOrEmpty(this.hidden_document_EbidComment.Value))
            {
                String jsonEbidComment = hidden_document_EbidComment.Value;
                List<Dms_Document> tmpListObj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(jsonEbidComment);
                var filterData = from a in tmpListObj
                                 where a.type != string.Empty && a.ID == 0
                                 select a;
                foreach (Dms_Document doc in filterData.ToList())
                {
                    jsonDocData.Add(updateNewDocumentID(doc, TMOPOid));
                }
            }

            if (!string.IsNullOrEmpty(this.hidden_document_BidAmount.Value))
            {
                //Add data from filed SA_Received
                String jsonBidAmount = hidden_document_BidAmount.Value;

                List<Dms_Document> tmpListObj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(jsonBidAmount);
                var filterData = from a in tmpListObj
                                 where a.type != string.Empty && a.ID == 0
                                 select a;
                foreach (Dms_Document doc in filterData.ToList())
                {
                    jsonDocData.Add(updateNewDocumentID(doc, TMOPOid));
                }
            }
            //Insert link document to DB
            foreach (Dms_Document doc in jsonDocData)
            {
                //Check if not template data will insert to DB
                if ((doc.ref_field != "") && (doc.ref_key != ""))
                {
                    List<Dms_Document> tmpList = DMSDocumentLinkHelper.GetDocumentLink(doc.ID, DMSDocumentLinkHelper.DocumentField(doc.ref_field), doc.ref_key);
                    if (tmpList.Count == 0)
                    {
                        DMSDocumentLinkHelper.AddDocumentLink(doc.ID, TMOPOid, DMSDocumentLinkHelper.DocumentField(doc.ref_field), doc.ref_key, true);
                        DocumentsChangeLog.AddLogOnCreateNewItem(doc, lblSiteID.Text, TMOPOid.ToString(), DocumentsChangeLog.eDocumentLogAction.Upload, Request, true);
                    }
                }
            }
        }
        private int GetTMOPOID(int tmoid)
        {
            int? tmpoid = null;
            using (CPTTEntities ce = new CPTTEntities())
            {
                if (this.hidden_TM_ID.Value == TMP_DEFAULT_ID)
                {
                    var result  = (from sa in ce.tower_mod_pos
                              where sa.tower_modification_id == tmoid
                               select sa.id);

                    if (result.Any())
                    {
                        tmpoid = result.Max();
                    }
                }
                else
                {
                    tmpoid = Convert.ToInt32(hidden_TM_ID.Value);
                }
            }
            if (!tmpoid.HasValue)
            {
                tmpoid = Convert.ToInt32(hidden_TM_ID.Value);
            }
            return tmpoid.Value;
        }
        #endregion

        private void GetSessionFilter()
        {
            // Check session filter
            DefaultFilterSession filterSession = new DefaultFilterSession();

            if (HttpContext.Current.Session != null && HttpContext.Current.Session[DefaultFilterSession.SessionName] != null)
            {
                filterSession = (DefaultFilterSession)HttpContext.Current.Session[DefaultFilterSession.SessionName];

                string hiddenFilter = DefaultFilterSession.GetFilterSession(filterSession, DefaultFilterSession.FilterType.TowerModFilter);
                backToList.NavigateUrl += "?filter=" + hiddenFilter;
            }
        }
    }
}
