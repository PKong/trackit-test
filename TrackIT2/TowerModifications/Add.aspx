<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="Add Tower Modifications | TrackiT2" Language="C#" MasterPageFile="~/Templates/Modal.Master" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="TrackIT2.TowerModifications.Add" %>
<%@ MasterType TypeName="TrackIT2.Templates.Modal" %>
<%@ Register TagPrefix="TrackIT2" TagName="TextFieldAutoComplete" Src="~/Controls/TextFieldAutoComplete.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   <!-- Styles -->
   <%: Styles.Render("~/Styles/tower_modification_add") %>
    
   <!-- Scripts -->
   <%: Scripts.Render("~/Scripts/tower_modification_add") %>

    <script type="text/javascript">
        $(document).ready(function () {
            $("li.ui-menu-item").live("click", function () {
                document.forms["Form1"].submit();
            });
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="message_alert"></div>
    <asp:Panel ID="pnlTowerModContainer" CssClass="create-container-small form_element" runat="server">
        <h4>Select Site</h4>
        <!-- Autocomplete element -->
        <TrackIT2:TextFieldAutoComplete ID="ctrSiteID" LabelFieldClientID="lblSiteID" 
        LabelFieldValue="Site ID "
        LabelFieldCssClass="label" 
        TextFieldClientID="txtSiteID" 
        TextFieldCssClass="input-autocomplete"
        TextFieldWidth="188" 
        TextFieldValue="" 
        ScriptKeyName="ScriptAutoCompleteSiteID"
        DataSourceUrl="SiteIdAutoComplete.axd" 
        ClientIDMode="Static" runat="server" />
        <br />
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ctrSiteID" />
            </Triggers>
            <ContentTemplate>
                <br />
                <asp:Label ID="Label1" runat="server" Text="" Font-Size="Small"></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>
        <br />
        <div id="divLeaseApplication" runat="server" clientidmode="Static">
           <h4>Select Associated Lease Apps</h4>
           <asp:ListBox ID="lbxAssociatedLeaseApps" ToolTip="Click to Select an Application"
               SelectionMode="Multiple" runat="server">
           </asp:ListBox>
        </div>
        <div class="cb">
        </div>
        <asp:Panel ID="divTowerModInfo" runat="server" Visible="false">
            <div class="form_element" style="margin-top:20px;">
                <h4>Tower Modification Info</h4>
                <asp:Label ID="labTMPacketSentDate" runat="server">TM Packet Sent</asp:Label>
                <asp:TextBox ID="txtTMPacketSentDate" runat="server" 
                                           CssClass="datepicker split"></asp:TextBox>
            </div>
        </asp:Panel>
        <div class="cb">
        </div>
        <asp:Button ID="btnSubmitCreateTower" runat="server" 
                    Text="Create" 
                    CssClass="buttonSubmit ui-button ui-widget ui-state-default ui-corner-all"
                    OnClick="btnSubmitCreateTower_Click" 
                    OnClientClick="if (!validateTowerModification()) {return false;}" 
                    UseSubmitBehavior="false" />
        <div class="cb">
        </div>
    </asp:Panel>
</asp:Content>
