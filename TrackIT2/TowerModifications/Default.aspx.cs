﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.BLL;
using TrackIT2.DAL;
using System.Collections.Specialized;
using System.Web.Script.Serialization;
using System.IO;
using TrackIT2.Handlers;
using System.ComponentModel;

namespace TrackIT2.TowerModifications
{
    /// <summary>
    /// List
    /// </summary>
    public partial class _Default : System.Web.UI.Page
    {

        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindLookupLists();
                PageUtility.SetupSearchResultFilters(this, panelFilterBox);
                this.SetupFlexiGrid();

                // Clear Session
                DefaultFilterSession.ClearSession(HttpContext.Current, DefaultFilterSession.FilterType.TowerModFilter);
            }
        }
        //-->

        /// <summary>
        /// Bind Lookup Lists
        /// </summary>
        protected void BindLookupLists()
        {

           using (CPTTEntities ce = new CPTTEntities())
           {
               // Customers
               var customers = ce.customers.OrderBy(Utility.GetField<customer>("customer_name")).OrderBy(Utility.GetIntField<customer>("sort_order"));
               this.lsbCustomer.Items.Add(new ListItem("- Select -", ""));
               this.lsbCustomer.Items.Add(new ListItem("Customer 1", "item1"));
               this.lsbCustomer.Items.Add(new ListItem("Customer 2", "item2"));
               Utility.BindDDLDataSource(this.lsbCustomer, customers, "id", "customer_name", null);

               // Tower Mod Types
               var towerModTypes = ce.tower_mod_types.ToList<tower_mod_types>();
               var sortTowerModTypes = new DynamicComparer<tower_mod_types>();
               sortTowerModTypes.SortOrder(x => x.tower_mod_type);
               towerModTypes.Sort(sortTowerModTypes);
               Utility.BindDDLDataSource(this.lsbTowerModType, towerModTypes, "id", "tower_mod_type", null);

               // Tower Mod Status
               var towerModStatus = ce.tower_mod_statuses.ToList<tower_mod_statuses>();
               var sortTowerModStatus = new DynamicComparer<tower_mod_statuses>();
               sortTowerModStatus.SortOrder(x => x.tower_mod_status_name);
               towerModStatus.Sort(sortTowerModStatus);
               Utility.BindDDLDataSource(this.lsbTowerModStatus, towerModStatus, "id", "tower_mod_status_name", null);

               // Tower Mod PM
               List<UserInfo> employees = BLL.User.GetUserInfo();
               var sortEmployees = new DynamicComparer<UserInfo>();
               sortEmployees.SortOrder(x => x.FullName);
               employees.Sort(sortEmployees);
               Utility.BindDDLDataSource(this.lsbTowerModPM, employees, "ID", "FullName", null);

               // Current Phase
               var towerModPhaseTypes = ce.tower_mod_phase_types.ToList<tower_mod_phase_types>();
               var sortTowerModPhaseTypes = new DynamicComparer<tower_mod_phase_types>();
               sortTowerModPhaseTypes.SortOrder(x => x.phase_name);
               towerModPhaseTypes.Sort(sortTowerModPhaseTypes);
               Utility.BindDDLDataSource(this.lsbCurrentPhase, towerModPhaseTypes, "id", "phase_name", null);

               // Advance Search 
               BindAdvanceSearch();
           }            
        }
        //-->

        /// <summary>
        /// Setup Flexi Grid
        /// </summary>
        protected void SetupFlexiGrid()
        {
            fgModifications.SortColumn = fgModifications.Columns.Find(col => col.Code == "site_uid");
            fgModifications.SortDirection = FlexigridASPNET.GridSortOrder.Asc;
            fgModifications.RowPerPageOptions = new int[] { 10, 20, 30, 40, 50 };
            fgModifications.NoItemMessage = "No Data";
        }
        //-->

        protected void BindAdvanceSearch()
        {
            ddlCriteria.Items.Clear();
            AdvanceSearch.TowerModificationFilter advSearch = new AdvanceSearch.TowerModificationFilter();

            List<ListItem> criteria = new List<ListItem>();
            foreach (AdvanceSearch.CritereaInfo item in advSearch.LstCriterea)
            {
                criteria.Add(new ListItem(item.NameInDDL, item.GetDDLValue()));
            }
            var sort_criteria = criteria;
            sort_criteria.Sort((obj1, obj2) => new AlphanumComparatorFast().Compare(obj1.Text, obj2.Text));
            ddlCriteria.Items.AddRange(sort_criteria.Cast<ListItem>().ToArray());
        }

        public void ExportClick(object s, EventArgs e)
        {
            try
            {
                ExportFile ef = new ExportFile();
                var searchData = hiddenSearchValue.Value;
                NameValueCollection data = new NameValueCollection();
                if (!string.IsNullOrEmpty(searchData))
                {
                    var jss = new JavaScriptSerializer();
                    var table = jss.Deserialize<dynamic>(searchData);

                    for (int x = 0; x < table.Length; x++)
                    {
                        data.Add(table[x]["name"], table[x]["value"]);
                    }
                }

                const int startIndex = 4;
                var parameters = new NameValueCollection();

                // If we have form values, extract them to build the object query.
                if (data.Count > startIndex)
                {
                   for (var i = startIndex; i < data.Count; i++)
                   {
                      var itemKey = data.GetKey(i);
                      var itemValue = data.Get(i);

                      parameters.Add(itemKey, itemValue);
                   }
                }

                NameValueCollection columnValue = ExportFile.GetColumn(hiddenColumnValue.Value);
                ExportFileHandler extHand;
                extHand = new ExportFileHandler(ExportFile.eExportType.eTowerMod, data, columnValue, parameters);
                extHand.Load_Data_OnComplete += new ExportFileHandler.Load_Data_OnComplete_Handler(Load_Data_OnComplete);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                Master.StatusBox.showStatusMessage("Export report error.");
            }
        }

        protected void Load_Data_OnComplete(List<ExportClass> loadDataResult, NameValueCollection data, NameValueCollection column, RunWorkerCompletedEventArgs e)
        {
            try
            {
                if (e.Error == null)
                {
                    ExportFile ef = new ExportFile();
                    MemoryStream mem = ef.WriteReport(loadDataResult, column, ExportFile.eExportType.eTowerMod, Server.MapPath("/"));

                    if (mem != null)
                    {
                        string fileName = ExportFile.GenarateFileName(data["fileName"]);
                        DownloadFile(mem, fileName);
                    }
                    else
                    {
                        Master.StatusBox.showStatusMessage("Export report error.");
                    }
                }
                else
                {
                    throw e.Error;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                Master.StatusBox.showStatusMessage("Export report error.");
            }
        }

        private void DownloadFile(MemoryStream mstream, string reportName)
        {
            try
            {
                byte[] byteArray = mstream.ToArray();
                Response.Clear();

                if (Response.Cookies.Count > 0)
                {
                    bool flag = true;
                    foreach (string c in Response.Cookies)
                    {
                        if (c == "fileDownloadToken")
                        {
                            Response.Cookies["fileDownloadToken"].Value = hiddenDownloadFlag.Value;
                            flag = false;
                        }
                    }
                    if (flag)
                    {
                        Response.AppendCookie(new HttpCookie("fileDownloadToken", hiddenDownloadFlag.Value));
                    }
                }
                else
                {
                    Response.AppendCookie(new HttpCookie("fileDownloadToken", hiddenDownloadFlag.Value));
                }
                Response.AddHeader("Content-Disposition", "attachment; filename=" + reportName + ".xls");
                Response.AddHeader("Content-Length", byteArray.Length.ToString());
                Response.ContentType = "application/vnd.msexcel";
                Response.BinaryWrite(byteArray);
                Response.Flush();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                Master.StatusBox.showStatusMessage("Export report error.");
            }
        }
    }
}