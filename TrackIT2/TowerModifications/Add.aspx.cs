﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.BLL;
using TrackIT2.DAL;

namespace TrackIT2.TowerModifications
{
    public partial class Add : System.Web.UI.Page
    {
        public static TextBox txtSiteID = new TextBox();

        protected void Page_Init(object sender, EventArgs e)
        {
            // Set Date Time Culture to en-US, some laptop set to local culture
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

            Control ctrl = PageUtility.FindControlRecursive(this.pnlTowerModContainer, this.ctrSiteID.TextFieldClientID);
            if (ctrl != null)
            {
                TextBox t = ctrl as TextBox;
                t.TextChanged += new EventHandler(txtSiteID_TextChanged);

            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            int iTMID = 0;

            if (!Page.IsPostBack && Request.QueryString["id"] != null && int.TryParse(Request.QueryString["id"], out iTMID))
            {
                ViewState["id"] = iTMID;
                Utility.ClearSubmitValidator();
                Control ctrl = PageUtility.FindControlRecursive(this.pnlTowerModContainer, this.ctrSiteID.TextFieldClientID);
                if (ctrl != null)
                {
                    TextBox t = ctrl as TextBox;
                    tower_modifications objTM = TowerMod.Search(iTMID);
                    if (objTM != null)
                    {
                        t.Text = objTM.site_uid;
                        BindListBox(objTM.site_uid);
                        BindLeaseAppData(objTM.id);
                    }
                }
                btnSubmitCreateTower.Text = "Save";
            }
            else if (!Page.IsPostBack && Request.QueryString["laid"] != null && int.TryParse(Request.QueryString["laid"], out iTMID))
            {
               // If laid is specified, then we're adding a Tower Mod record
               // from the lease application page. We won't have access to the
               // TowerMod id until after the transaction completes, so adding
               // records to the lease application / tower modification join 
               // table is not possible. In this case, we hide the lease 
               // application list box. 
               Utility.ClearSubmitValidator();
               divTowerModInfo.Visible = true;
               ViewState["laid"] = iTMID;

               // In the event we are creating a new tower mod request, we 
               // won't have access to the TowerMod id until after the
               // transaction completes, so adding records to the lease
               // application / tower modification join table is not possible.
               // In this case, we hide the lease application list box.
               divLeaseApplication.Style.Add("display", "none");
            }
            else
            {
               // In the event we are creating a new tower mod request on the
               // Tower Mod page and the same rules apply as above.
               divLeaseApplication.Style.Add("display", "none");
            }
        }

        protected void BindListBox(string site_id)
        {
            this.lbxAssociatedLeaseApps.Items.Clear();

            using (CPTTEntities ce = new CPTTEntities())
            {
                var result = from tower in ce.lease_applications
                                where tower.site_uid.Equals(site_id)
                                orderby tower.site_uid
                                select new { tower.site_uid, tower.customer, tower.leaseapp_rcvd_date, tower.id, tower.leaseapp_types };

                if (result.Count() > 0)
                {
                    // Add the related option from database
                    foreach (var row in result)
                    {
                        string customerName = (row.customer != null ? row.customer.customer_name.ToString() : "");
                        string sType = "";
                        if (row.leaseapp_types != null) sType = " " + LeaseApplication.GetLeaseAppTypeAliases(row.leaseapp_types.lease_application_type);
                        string listitem_text = row.site_uid.ToString() + " - " + customerName + " -" + sType + " " + Utility.DateToString(row.leaseapp_rcvd_date);
                        this.lbxAssociatedLeaseApps.Items.Add(new ListItem(listitem_text, row.id.ToString()));
                    }
                    Label1.Text = "";
                }
                else
                {
                    Label1.Text = "There are no related Lease Applications for this Site ID.";
                }
            }
        }

        protected void BindLeaseAppData(int TMID)
        {            
            //List<lease_applications_tower_modifications> lstTM = LeaseAppTowerMod.GetLeaseAppTM(TMID);
            List<lease_applications> lstLA = LeaseAppTowerMod.GetLeaseAppTM(TMID);
            
            if (lstLA.Count > 0 && lbxAssociatedLeaseApps.Items.Count > 0)
            {
                using (CPTTEntities ce = new CPTTEntities())
                {
                    foreach (lease_applications item in lstLA)
                    {
                        if (lbxAssociatedLeaseApps.Items.FindByValue(item.id.ToString()) != null)
                        {
                            lbxAssociatedLeaseApps.Items.FindByValue(item.id.ToString()).Selected = true;
                        }
                    }
                }
            }
        }
        protected void txtSiteID_TextChanged(object sender, EventArgs e)
        {
            Control ctrl = PageUtility.FindControlRecursive(this.pnlTowerModContainer, this.ctrSiteID.TextFieldClientID);
            if (ctrl != null)
            {
                // Get text from 'txtSiteID'
                TextBox t = ctrl as TextBox;

                // Create string to save site_id
                string site_id = t.Text.ToUpper();

                // Check SitID to return LeaseApp, then assign to ListBox
                BindListBox(site_id);
            }
        }

        protected void btnSubmitCreateTower_Click(object sender, EventArgs e)
        {
           string status = null;
           bool isSuccess = false;

           // Can't add lease application / tower mod records during initial
           // creation. We must wait until we have a Tower Mod ID value for
           // the join table.

           //List<lease_applications_tower_modifications> lstTM = new List<lease_applications_tower_modifications>();
           //lease_applications_tower_modifications objLeaseTM;

           ////Adding all selected related lease application to list.
           //if (lbxAssociatedLeaseApps.Items.Count > 0)
           //{
           //   foreach (ListItem item in lbxAssociatedLeaseApps.Items)
           //   {
           //      if (item.Selected)
           //      {
           //         objLeaseTM = new lease_applications_tower_modifications();
           //         objLeaseTM.lease_application_id = int.Parse(item.Value);
           //         lstTM.Add(objLeaseTM);
           //      }
           //   }
           //}
           tower_modifications objTM = null;

           // Start transaction for update.
           using (CPTTEntities ce = new CPTTEntities())
           {
              try
              {
                 using (TransactionScope trans = TransactionUtils.CreateTransactionScope())
                 {
                    // Manually open connection to prevent EF from auto closing
                    // connection and causing issues with future queries.
                    ce.Connection.Open();

                    if (btnSubmitCreateTower.Text != "Save")
                    {
                       //Add new.
                       objTM = new tower_modifications();
                    }
                    else
                    {
                       //Edit.
                       objTM = TowerMod.SearchWithinTransaction(int.Parse(ViewState["id"].ToString()), ce);
                    }

                    Control ctrl = PageUtility.FindControlRecursive(this.pnlTowerModContainer, this.ctrSiteID.TextFieldClientID);
                    if (objTM != null && ctrl != null)
                    {
                       TextBox t = ctrl as TextBox;
                       if (BLL.Site.SearchWithinTransaction(t.Text, ce) != null)
                       {
                          Utility.ClearErrorColorForTextbox(t);
                          objTM.site_uid = t.Text.ToUpper();
                          if (ViewState["laid"] != null)
                          {
                             objTM.tower_mod_packet_sent_date = Utility.PrepareDate(txtTMPacketSentDate.Text);
                          }

                          if (btnSubmitCreateTower.Text != "Save")
                          {
                             // Add new.
                             status = TowerMod.AddWithinTransaction(objTM, ce);
                          }
                          else
                          {
                             // Delete all related lease application first.
                             status = LeaseAppTowerMod.DeleteAllByTowerModWithinTransaction(objTM.id, ce);

                             // Null or empty status indicates success
                             if (string.IsNullOrEmpty(status))
                             {
                                // Adding all selected related lease application to list.
                                if (lbxAssociatedLeaseApps.Items.Count > 0)
                                {
                                   foreach (ListItem item in lbxAssociatedLeaseApps.Items)
                                   {
                                      // Skip next iteration if previous add had an error.
                                      if (string.IsNullOrEmpty(status))
                                      {
                                         if (item.Selected)
                                         {
                                            LeaseAppTowerMod.AddWithinTransaction(int.Parse(item.Value), objTM.id, ce);
                                         }
                                      }
                                      else
                                      {
                                         isSuccess = false;
                                      }

                                   }
                                }
                                
                                // Finally, update the Tower Mod record.
                                if (string.IsNullOrEmpty(status))
                                {
                                   //Update tower modification.
                                   status = TowerMod.UpdateWithinTransaction(objTM, ce);

                                   if (!string.IsNullOrEmpty(status))
                                   {
                                      isSuccess = false;
                                   }
                                }
                             }
                             else
                             {
                                isSuccess = false;
                             }
                          }

                          // If we've made it this far, then all nested transactions
                          // have completed successfully, and we can mark this
                          // transaction as complete as well.
                          if (string.IsNullOrEmpty(status))
                          {
                             isSuccess = true;
                             ce.SaveChanges();
                             trans.Complete();     // Transaction complete
                          }
                       }
                       else
                       {
                          //Invalid site UID
                          isSuccess = false;
                          Master.StatusBox.showStatusMessage(Globals.ERROR_TXT_INVALID_UID);
                          Utility.SettingErrorColorForTextbox(t);
                       }
                    }
                 }
              }
              catch (Exception ex)
              {
                 // Log error in ELMAH for any diagnostic needs.
                 Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                 status = "An error occcurred processing the tower modification. Please try again.";
                 isSuccess = false;
              }
              finally
              {
                 ce.Connection.Dispose();
              }

              // Finally accept all changes from the transaction, rebind the
              // application details and show a success message.
              if (isSuccess)
              {
                 ce.AcceptAllChanges();

                 //Redirect to edit page with created id.
                 ClientScript.RegisterStartupScript(GetType(), "Load",
                 String.Format("<script type='text/javascript'>" +
                          "   $(\"#MainContent_btnSubmitCreateTower\").val(\"Processing...\");" +
                          "   $(\"#MainContent_btnSubmitCreateTower\").attr(\"disabled\", true);" +
                          "   window.parent.location.href = 'Edit.aspx?id={0}';" +
                          "</script>",
                 objTM.id.ToString()));
              }
              else
              {
                 Master.StatusBox.showStatusMessage(status);
              }
           }
        }
    }
}