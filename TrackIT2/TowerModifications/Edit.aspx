﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="Edit Tower Modification | TrackiT2" Language="C#" MasterPageFile="~/Templates/TrackIT2.Master" AutoEventWireup="true" ValidateRequest="false" CodeBehind="Edit.aspx.cs" Inherits="TrackIT2.TowerModifications.Edit" %>
<%@ MasterType TypeName="TrackIT2.Templates.TrackIT2" %>
<%@ Register TagPrefix="TrackIT2" TagName="TextFieldAutoComplete" src="~/Controls/TextFieldAutoComplete.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   <!-- Styles -->
   <%: Styles.Render("~/Styles/tower_modification_edit") %>
    
   <!-- Scripts -->
   <%: Scripts.Render("~/Scripts/tower_modification_edit") %>

    <script type="text/javascript">
        $(document).ready(function () {
            var tab_index = '<%= Session["cur_tab_index"] %>';
            // Select Tabs	
            $(function () {
                $("#tabs2").tabs({ selected: parseInt(tab_index) });
            });

            setPageName(pageTM);
            var index = 0;
            if (parseInt($("#MainContent_hidden_TM_count").val()) > 0) {
                index = parseInt($("#MainContent_hidden_TM_count").val()) - 1;
            }

            setBufferIndexTM(index);
            setBtnCountTM($("#MainContent_hidden_TM_count").val());
            setupDocumentLinkDialog("Document Links");

            initEventClickBack();
            initEventClickNext();
            initEventClickAddNew();

            $("#add_Purchase_Panel").button();

            // Date Picker
            $(function () {
                $('input').filter('.datepicker').datepicker({
                    numberOfMonths: 2,
                    showButtonPanel: false
                });
            });

            // SetCssBtn
            cssBtnControlPage();

            // Comments
            var prmComments = Sys.WebForms.PageRequestManager.getInstance();
            prmComments.add_pageLoaded(setupCommentsLocal);
            //Create Link document for all comments
            CreateCommentLinkDocument('MainContent_hidden_document_all_document_comments', 'document-comment-');
        });
        //-->

        // Setup Comments Local
        function setupCommentsLocal()
        {
            setupComments();
            setShowCommentBox();
        }

        function setShowCommentBox() {
            var mainCommentCount = $("#MainContent_hidden_count_main_comment").val();
            if (parseInt(mainCommentCount) == 0 || $("#MainContent_divComment1").length == 0) {
                showCommentBox();
            }
        }
        //-->

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">    
    <script type="text/javascript" language="javascript">
        // Doc Ready
        $(function () {
            // Set the modal size
            setModalSize(550, 450, 330);
            // Setup List Create Modal
            setupListCreateModal("Edit Modification", window.location.toString().substr(window.location.toString().indexOf('=') + 1));
            
            // Setup Upload Document Modal
            setupDmsModal("Upload Document");
        });
        //-->
    </script>
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server" />
    
    <asp:Panel ID="pnlNoID" runat="server" CssClass="align-center">
      <h1>Not Found (404)</h1>

      <p>The tower modification specified is invalid or no longer exists.</p>

      <p>&nbsp;</p>

      <p>
         <a href="Default.aspx">&laquo; Go back to list page.</a>
      </p>

      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
    </asp:Panel>

    <asp:Panel id="pnlSectionContent" runat="server">    
       <div class="section content">
           <div class="wrapper">
               <div class="action-tab">
                    <div class="left-side">
                        <asp:HyperLink ID="backToList" runat="server" Text="« Return to List" NavigateUrl="~/TowerModifications/" CssClass="site-link-no-underline"></asp:HyperLink>
                    </div>
                    <div class="right-side">
                        <asp:LinkButton ID="btnTowerModDelete" CssClass="link-button site-link-no-underline" runat="server" ClientIDMode="Static" OnClientClick="return openDeleteAppButtonPopup('Tower Modification',event);" onclick="btnTowerModDelete_Click">Delete</asp:LinkButton>
                    </div>
               </div>

               <div class="application-information">
                   <h1>
                       Tower Modification <span>
                           <asp:HyperLink ID="lblSiteID" runat="server" Text="" CssClass="h1-span-site-link site-link-no-underline"></asp:HyperLink>
                           /
                           <asp:Label ID="lblSiteName" runat="server" Text=""></asp:Label>
                           /
                           <asp:Label ID="lblCustomerName" runat="server"></asp:Label>
                       </span>
                   </h1>
                   <div id="tabs" class="toggle-me">
                       <ul class="tabs">
                           <li><a href="#general-information">Related Leases and Tower</a></li>
                       </ul>
                       <div id="general-information">
                           <div style="padding: 20px;">
                               <div class="i-1-3">
                                   <div class="general-information-column">
                                       <div style="float: left;">
                                           <asp:GridView ID="grdMain"  OnRowCommand="grdMain_Command" CssClass="related-apps TableGenInfo" runat="server" AutoGenerateColumns="False">
                                               <Columns>
                                                   <asp:BoundField HeaderText="Site ID" DataField="SiteID" />
                                                   <asp:BoundField HeaderText="Customer" DataField="CustomerName" />
                                                   <asp:BoundField HeaderText="App Received" DataField="LAReceived" DataFormatString="{0:MM/dd/yyyy}"
                                                       HtmlEncode="false" />
                                                   <asp:HyperLinkField DataNavigateUrlFields="Link" Text="Go" />
                                               </Columns>
                                           </asp:GridView>
                                           <div id="divNoRelatedLeaseApps" class="related-apps TableGenInfo" style="margin-right: 20px;
                                               background: #f0f0f0;" runat="server">
                                               <div style="margin: 0 auto 0 auto; width: 200px; padding: 35px 0 0 0; text-align: center;
                                                   color: #888;">
                                                   <em>
                                                       <asp:Label ID="lblNoLeaseAppStatus" runat="server">No Related Lease Applications</asp:Label>
                                                   </em>
                                               </div>
                                           </div>
                                       </div>
                                       <ul class="information-list" style="width: 180px; margin-bottom: 10px;">
                                           <li>Tower Address</li>
                                           <li>
                                               <asp:HyperLink ID="mybtn" NavigateUrl="javascript:void(0)" Target="_blank" runat="server"> </asp:HyperLink>
                                               <asp:Label ID="lblAddressNA" runat="server" Visible="false"></asp:Label>
                                           </li>   
                                           <li>&nbsp;</li>
                                           <li>Latitude, Longitude:
                                               <br />
                                               <asp:HyperLink ID="lblSiteLatLong" Target="_blank" runat="server">
                                                   <asp:Label ID="lblDegreeLatitude" runat="server" ></asp:Label>,&nbsp;<asp:Label ID="lblDegreeLongtitude" runat="server"></asp:Label><br/>
                                                   <asp:Label ID="lblSiteLatitude" runat="server" Text="Label"></asp:Label>,&nbsp;<asp:Label ID="lblSiteLongitude" runat="server" Text="Label"></asp:Label>
                                               </asp:HyperLink>
                                           </li>      
                                       </ul>
                                   </div>
                                   <div class="cb"></div>
                                   <div class="general-information-column">
                                       <ul class="information-list" style="width: 180px;">
                                           <li>Structure Details</li>
                                           <li>Type:
                                               <asp:Label ID="lblType" runat="server" Text=""></asp:Label></li>
                                           <li>Height:
                                               <asp:Label ID="lblHeight" runat="server" Text=""></asp:Label></li>
                                           <li>Rogue Equip:
                                               <asp:Label ID="lblRogueEquip" runat="server" Text=""></asp:Label></li>
                                           <li>Status:
                                               <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
                                               <asp:Image ID="imgStructureDetails_Construction" runat="server" AlternateText="Construction" CssClass="construction-img" /></li>
                                       </ul>
                                   </div>
                               </div>
                               <div class="i-1-3">
                                   <div class="map">
                                       <asp:Image ID="Image2" Width="310" Height="235" frameborder="0" scrolling="no" marginheight="0"
                                           runat="server" />
                                   </div>
                                   <!-- .map -->
                               </div>
                               <!-- .i-1-3 -->
                               <a id="modalBtnExternal" class="edit-button" href="javascript:void(0)">edit</a>
                               
                               <!-- Modal -->
                               <div id="modalExternal" class="modal-container" style="overflow: hidden;">
                               <div id="modalDocument" class="modal-container" style="overflow:hidden;"></div>
                               </div>
                               <!-- /Modal -->
                               <div class="cb">
                               </div>
                           </div>
                           <!-- padding -->
                       </div>
                       <!-- #general-information -->
                   </div>
                   <!-- #tabs -->
                   <div class="cb">
                   </div>
               </div>
               <!-- .application-information -->
               <div class="application-process" style="float: left;">
                   <h2>
                       Tower Mod Process</h2>
                   <div id="tabs2">
                       <ul class="tabs">
                           <li><a href="#general" onclick="DynamicUpdate('GE');">General</a></li>
                           <li><a href="#towermod-po" onclick="DynamicUpdate('PO');">Tower Mod POs</a></li>
                       </ul>
                       <div id="message_alert"></div>
                       <div id="general">
                           <div style="padding: 20px;">
                               <div class="full-col align-right">
                                   <asp:Button ID="btnSubmitSummary2" runat="server" 
                                               Text="Save" 
                                               CssClass="ui-button ui-widget ui-state-default ui-corner-all"
                                               OnClick="btnSubmitSummary_Click" 
                                               OnClientClick="if (!funcBeforeSaveGen()) {return false;}"
                                               UseSubmitBehavior="false" />
                               </div>
                               <div class="left-col">
                                   <h4>
                                       Summary</h4>
                                   <label id="lblCustomer" runat="server">
                                       Customer</label>
                                   <asp:DropDownList ID="ddlCustomer" CssClass="select-field" runat="server">
                                   </asp:DropDownList>
                                   <label>
                                       Payor</label>
                                   <asp:DropDownList ID="ddlPayor" CssClass="select-field" runat="server">
                                   </asp:DropDownList>
                                   
                                   <!--Document Link-->
                                   <div class="document-link" id="TowerModPacketSentDate" >
                                       <label>Package Sent</label>
                                       <asp:TextBox ID="txtPackageSent" CssClass="datepicker" runat="server"></asp:TextBox>
                                       <asp:HiddenField  ID="hidden_document_TowerModPacketSentDate" ClientIDMode="Static" runat="server" />
                                   </div>

                                   <label>
                                       PM Emp</label>
                                   <asp:DropDownList ID="ddlPMEmp" CssClass="select-field" runat="server">
                                   </asp:DropDownList>
                                   <label>
                                       Tower Mod Status</label>
                                   <asp:DropDownList ID="ddlTowerModStat" CssClass="select-field" runat="server">
                                   </asp:DropDownList>
                                   <label>
                                       Tower Mod Type</label>
                                   <asp:DropDownList ID="ddlTowerModType" CssClass="select-field" runat="server">
                                   </asp:DropDownList>
                                   <label>
                                       Tower Height Proposed Feet</label>
                                   <asp:TextBox ID="txtTowerHeight" runat="server"></asp:TextBox>
                                   <label>
                                       Post Mod Structural Capacity %</label>
                                   <asp:TextBox ID="txtCapacity" runat="server" CssClass="pencent-field"></asp:TextBox><br />
                                   <p style="float: left; width: 90%; padding: 5px 5px 0; text-align: left; text-decoration: italic; font-size: 12px; font-style:italic; margin-bottom:20px;color:#666;">
                   Note: Please enter percentages using whole-number notation; e.g. eighty seven percent as 87.0</p>
                               </div>
                               <div class="right-col">
                                   <h4>
                                       Permitting for Building</h4>
                                   <label>
                                       Regulatory Notification</label>
                                   <asp:DropDownList ID="ddlRegNotification" CssClass="select-field" runat="server">
                                       <asp:ListItem Value="" Text="- Select -"></asp:ListItem>
                                       <asp:ListItem Value="yes" Text="Yes"></asp:ListItem>
                                       <asp:ListItem Value="no" Text="No"></asp:ListItem>
                                   </asp:DropDownList>
                                   <label>
                                       Permit from Carrier</label>
                                   <asp:DropDownList ID="ddlPermitCarrier" CssClass="select-field" runat="server">
                                       <asp:ListItem Value="" Text="- Select -"></asp:ListItem>
                                       <asp:ListItem Value="yes" Text="Yes"></asp:ListItem>
                                       <asp:ListItem Value="no" Text="No"></asp:ListItem>
                                   </asp:DropDownList>
                                      
                                   <!--Document Link-->
                                   <div class="document-link" id="BuildingPermitRcvdDate" >
                                       <label>Permit Received</label>
                                       <asp:TextBox ID="txtPermitReceived" CssClass="datepicker" runat="server"></asp:TextBox>
                                       <asp:DropDownList ID="ddlPermitReceived" CssClass="select-field split" runat="server">
                                       </asp:DropDownList>
                                       <asp:HiddenField  ID="hidden_document_BuildingPermitRcvdDate" ClientIDMode="Static" runat="server" />
                                   </div>

                                   <label>
                                       Top Out Notification Required</label>
                                   <asp:DropDownList ID="ddlTopOutNotificationReq" CssClass="select-field split" runat="server">
                                       <asp:ListItem Value="" Text="- Select -"></asp:ListItem>
                                       <asp:ListItem Value="yes" Text="Yes"></asp:ListItem>
                                       <asp:ListItem Value="no" Text="No"></asp:ListItem>
                                   </asp:DropDownList>
                                   <br />
                                   <h4>
                                       Materials / Build</h4>
                                   <label>
                                       Materials Ordered</label>
                                   <asp:TextBox ID="txtMaterialsOrdered" CssClass="datepicker" runat="server"></asp:TextBox>
                                   <label>
                                       Materials Received</label>
                                   <asp:TextBox ID="txtMaterialsReceived" CssClass="datepicker" runat="server"></asp:TextBox>
                                   <asp:DropDownList ID="ddlMaterialsReceived" CssClass="select-field split" runat="server">
                                   </asp:DropDownList>
                                   <label>
                                       Work Start</label>
                                   <asp:TextBox ID="txtWorkStart" CssClass="datepicker" runat="server"></asp:TextBox>
                                   <asp:DropDownList ID="ddlWorkStart" CssClass="select-field split" runat="server">
                                   </asp:DropDownList>
                                   <label>
                                       Work Completion</label>
                                   <asp:TextBox ID="txtWorkCompletion" CssClass="datepicker" runat="server"></asp:TextBox>
                                   <asp:DropDownList ID="ddlWorkCompletion" CssClass="select-field split" runat="server">
                                   </asp:DropDownList>
                                   <label>
                                       Third Party Inspection</label>
                                   <asp:TextBox ID="txtInspection" CssClass="datepicker" runat="server"></asp:TextBox>
                                   <asp:DropDownList ID="ddlInspection" CssClass="select-field split" runat="server">
                                   </asp:DropDownList>
                               </div>
                               <div class="full-col align-right">
                                   <asp:Button ID="btnSubmitSummary" runat="server" 
                                               Text="Save" 
                                               CssClass="ui-button ui-widget ui-state-default ui-corner-all"
                                               OnClick="btnSubmitSummary_Click" 
                                               OnClientClick="if (!funcBeforeSaveGen()) {return false;}"
                                               UseSubmitBehavior="false" />
                               </div>
                               <div class="cb">
                               </div>
                           </div>
                           <!-- padding -->
                       </div>
                       <!-- #general -->
                       <span id="hidden_count" style="display: none">1</span> <span id="BufferIndex" style="display: none">
                       </span><span id="PageIndex" style="display: none"></span>

                       <asp:HiddenField ID="hidden_TM_Index" runat="server" />
                       <asp:HiddenField ID="hidden_TM_ID" runat="server" />
                       <asp:HiddenField ID="hidden_TM_template" runat="server" />
                       <asp:HiddenField ID="hidDocumentDownload" runat="server" />
                       
                       <input type="hidden" id="hidden_TM_IsNew" />
                       <input type="hidden" id="hidden_TM_IsSave" />
                       <asp:HiddenField ID="hiddenCurrentTab" runat="server" ClientIDMode="Static" />
                       <div id="towermod-po">
                           <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                               <ContentTemplate>
                                   <div style="padding: 20px;">
                                       <asp:HiddenField ID="hidden_TM" runat="server" />
                                       <asp:HiddenField ID="hidden_TM_count" runat="server" />
                                       <asp:HiddenField ID="hidden_popup" runat="server" ClientIDMode="Static" />
                                       <asp:Panel ID="nav2" CssClass="pnlnav2" runat="server">
                                       </asp:Panel>
                                       <div>
                                           <div class="PanelPageNumber">
                                               <span id="current-page" class="textPageNumber">5</span> <span class="textPageNumber">
                                                   of</span> <span id="total-page" class="textPageNumber">5</span>
                                           </div>
                                           <div class="PanelPageControl">
                                               <script type="text/javascript">
                                                   function Test(sName, e,check) {
                                                       if (check != 0) {
                                                           alert(check);
                                                           openDeleteAppButtonPopup(sName, e);
                                                           return true;
                                                       }
                                                       else {
                                                           alert("Nooo");
                                                           return false;
                                                       }

                                                   }
                                               </script>
                                               <asp:Button ID="btnTMPODelete" Text="Delete" runat="server" OnClientClick="return openDeleteAppButtonPopup('Tower Mod POs',event,getTMID());" ClientIDMode="Static" onclick="btnTMPODelete_Click"/>&nbsp;&nbsp;&nbsp;
                                               <input id="btn_Previous" type="button" value="Previous" />&nbsp;&nbsp;&nbsp;
                                               <input id="btn_Next" type="button" value="Next" />&nbsp;&nbsp;&nbsp;
                                               <input id="btn_AddNew" type="button" value="Add New" />
                                           </div>
                                       </div>
                                       <div class="cb">
                                       </div>
                                       <div id="error_twmodpo"></div>
                                       <div class="slideshow2" id="slideshow2">
                                           <div>
                                               <!-- START SLIDE 1-->
                                               <div class="full-col align-right" style="margin-bottom: 30px;margin-top:25px;">
                                                   <asp:Button ID="btnSummitTowerModPOs2" runat="server" 
                                                               Text="Save" 
                                                               CssClass="ui-button ui-widget ui-state-default ui-corner-all"
                                                               OnClick="btnSubmitSummary_Click" 
                                                               OnClientClick="if (!CheckSavingTM()) {return false;}"
                                                               UseSubmitBehavior="false" />
                                               </div>
                                               <div class="left-col">
                                                   <h4>
                                                       Tower Mod POs</h4>
                                                   <label>
                                                       Tower Mod PO Type</label>
                                                   <asp:DropDownList ID="ddlTowerModPhase1" CssClass="select-field" runat="server">
                                                   </asp:DropDownList>
                                                   <h4>
                                                       eBid</h4>
                                                   <label>
                                                       Request Received</label>
                                                   <asp:TextBox ID="txtRequestReceived1" CssClass="datepicker" runat="server"></asp:TextBox>
                                                   <label>
                                                       Packet Ready</label>
                                                   <asp:TextBox ID="txtPacketReady1" CssClass="datepicker" runat="server"></asp:TextBox>
                                                   <label>
                                                       Scheduled Published</label>
                                                   <asp:TextBox ID="txtScheduledPublished1" CssClass="datepicker" runat="server"></asp:TextBox>
                                                   <label>
                                                       Scheduled Close</label>
                                                   <asp:TextBox ID="txtScheduledClosed1" CssClass="datepicker" runat="server"></asp:TextBox>
                                                   <label>
                                                       Maturity Date</label>
                                                   <asp:TextBox ID="txtMaturityDate1" CssClass="datepicker" runat="server"></asp:TextBox>
         
                                                   <!--Document Link-->
                                                   <div class="document-link" id="EbidComment" >
                                                       <label class="comments-label" style="float:left;">Comments</label>
                                                       <asp:TextBox ID="txt_eBidComments1" TextMode="MultiLine" Text="" runat="server" style="width:180px;min-height:80px"></asp:TextBox>
                                                       <asp:HiddenField  ID="hidden_document_EbidComment" ClientIDMode="Static" runat="server" />
                                                   </div>

                                                   <div class="cb">
                                                   </div>
                                               </div>
                                               <div class="right-col">
                                                   <h4>
                                                       Pricing</h4>
                                                   <label>
                                                       Estimated Construction Cost</label>
                                                   <asp:TextBox ID="txtEstConstructionCostAmount1" CssClass="moneyfield" runat="server"></asp:TextBox>
                                                             
                                                   <!--Document Link-->
                                                   <div class="document-link" id="BidAmount" >
                                                       <label>Bid</label>
                                                       <asp:TextBox ID="txtBidAmount1" CssClass="moneyfield" runat="server" Width="170px" ></asp:TextBox>
                                                       <asp:HiddenField  ID="hidden_document_BidAmount" ClientIDMode="Static" runat="server" />
                                                   </div>

                                                   <label>
                                                       Tax</label>
                                                   <asp:TextBox ID="txtTaxAmount1" CssClass="moneyfield" runat="server"></asp:TextBox>
                                                   <label>
                                                       Markup</label>
                                                   <asp:TextBox ID="txtMarkupAmount1" CssClass="moneyfield" runat="server"></asp:TextBox>
                                                   <label>
                                                       Total from Customer</label>
                                                   <asp:TextBox ID="txtTotalfromCustomerAmount1" CssClass="moneyfield" runat="server"></asp:TextBox>
                                                   <h4>
                                                       PO Creation</h4>
                                                   <TrackIT2:TextFieldAutoComplete ID="ctrGeneralContractorID"
                                                                   LabelFieldClientID="lblGeneralContractorID"
                                                                   LabelFieldValue="General Contractor"
                                                                   LabelFieldCssClass="label"
                                                                   TextFieldClientID="txtGeneralContractorID"
                                                                   TextFieldCssClass="input-autocomplete"
                                                                   TextFieldWidth="190"
                                                                   ScriptKeyName="GeneralContractorAutoCompleteName"
                                                                   DataSourceUrl="GeneralContractorAutoComplete.axd"
                                                                   ClientIDMode="Static"
                                                                   IsWholeTextUsage="true"
                                                                   runat="server" /><br />
                                                   <label>
                                                       Sent to CS PM</label>
                                                   <asp:TextBox ID="txtSendToCSPM1" CssClass="datepicker" runat="server"></asp:TextBox>
                                                   <label>
                                                       PO to GC Requested</label>
                                                   <asp:TextBox ID="txtPOtoGC1" CssClass="datepicker" runat="server"></asp:TextBox>
                                                   <label>
                                                       Shopping Cart ID</label>
                                                   <asp:TextBox ID="txtShoppingCartID1" runat="server"></asp:TextBox>
                                                   <label>
                                                       PO to GC Received</label>
                                                   <asp:TextBox ID="txtPOtoGCReceived1" CssClass="datepicker" runat="server"></asp:TextBox>
                                                   <label>
                                                       PO Number</label>
                                                   <asp:TextBox ID="txtPONumber1" runat="server"></asp:TextBox>
                                               </div>
                                               <div class="full-col align-right" style="margin-bottom: 30px;">
                                                   <asp:Button ID="btnSummitTowerModPOs1" runat="server" 
                                                               Text="Save" 
                                                               CssClass="ui-button ui-widget ui-state-default ui-corner-all"
                                                               OnClick="btnSubmitSummary_Click" 
                                                               OnClientClick="if (!CheckSavingTM()) {return false;}"
                                                               UseSubmitBehavior="false" />
                                               </div>
                                               <div class="cb">
                                               </div>
                                           </div>
                                           <!-- END SLIDE 1 -->
                                       </div>
                                       <!-- .slideshow -->
                                   </div>
                                   <!-- padding -->
                               </ContentTemplate>
                           </asp:UpdatePanel>
                       </div>
                       <!-- #towermod-process -->
                   </div>
                   <!-- #tabs2 -->
                   <div class="cb">
                   </div>
               </div>
               <!-- .application-process -->
               <div class="application-comments">
                   <h2 id="h2-comments" style="width: 140px;">
                       Comments</h2>
                   <div class="comments-wrapper">
                       <asp:UpdatePanel ID="upComments" UpdateMode="Always" runat="server">
                           <ContentTemplate>
                               <div class="blah-wrapper" style="width: 100%; height: 0px; position: relative; overflow: hidden">
                                   <div style="position: absolute; top: -200px; width: 100%; height: 200px; background: none;"
                                       id="blah">
                                       <div class="new-comment" style="display: none;">
                                           <div class="balloon">
                                               <asp:TextBox ID="txtComments" Rows="2" Columns="5" CssClass="comment-textarea" runat="server"
                                                   TextMode="MultiLine"></asp:TextBox>
                                               <div class="balloon-bottom">
                                               </div>
                                           </div>
                                           <div class="author-info" style="margin-top: 15px; font-size: 10pt; color: #a1a1a1;">
                                               <asp:Image ID="imgAuthor" Width="25" ImageUrl="/images/avatars/default.png" AlternateText="profile-picture"
                                                   runat="server" />
                                               <asp:Button ID="btnPost" ClientIDMode="Static" runat="server" CssClass="ui-button ui-widget ui-state-default ui-corner-all" OnClientClick="this.disabled = true; this.value = 'Processing...';showLoadingSpinner('h2-comments');" OnClick="btnPost_Click" Text="Post" UseSubmitBehavior="false"/>
                                               <asp:Button ID="btnCommentCancel" CssClass="comment-cancel" ClientIDMode="Static" runat="server" OnClientClick="return false;" Text="Cancel" />                                                                                       
                                           </div>
                                       </div>
                                   </div>
                                   <div class="clear-all">
                                   </div>
                               </div>
                               <div runat="server" id="divComment1" class="comment">
                                   <div class="balloon">
                                       <p runat="server" id="pComment1">
                                       </p>
                                       <div class="balloon-bottom">
                                       </div>
                                   </div>
                                   <div class="author-info" style="margin-top: 15px; font-size: 10pt; color: #a1a1a1;">
                                       <asp:Image ID="imgUserComment1" Width="25" ImageUrl="/images/avatars/default.png"
                                           ToolTip="profile-picture" runat="server" />
                                       <p runat="server" id="pCommenter1">
                                       </p>
                                       <div id="pnlDocument1" runat="server" style="width:100%;text-align:left;" ></div>
                                       <asp:ImageButton ID="btnDeleteComment1" ImageUrl="/images/icons/Erase-Greyscale.png"
                                           ToolTip="Delete Comment" Width="12" runat="server" OnClick="btnDeleteComment_Click"
                                           CssClass="btn-comment-delete" />
                                   </div>
                               </div>
                               <div runat="server" id="divComment2" class="comment">
                                   <div class="balloon">
                                       <p runat="server" id="pComment2">
                                       </p>
                                       <div class="balloon-bottom">
                                       </div>
                                   </div>
                                   <div class="author-info" style="margin-top: 15px; font-size: 10pt; color: #a1a1a1;">
                                       <asp:Image ID="imgUserComment2" Width="25" ImageUrl="/images/avatars/default.png"
                                           ToolTip="profile-picture" runat="server" />
                                       <p runat="server" id="pCommenter2">
                                       </p>
                                       <div id="pnlDocument2" runat="server" style="width:100%;text-align:left;" ></div>
                                       <asp:ImageButton ID="btnDeleteComment2" ImageUrl="/images/icons/Erase-Greyscale.png"
                                           ToolTip="Delete Comment" Width="12" runat="server" OnClick="btnDeleteComment_Click"
                                           CssClass="btn-comment-delete" />
                                   </div>
                               </div>
                               <div runat="server" id="divComment3" class="comment">
                                   <div class="balloon">
                                       <p runat="server" id="pComment3">
                                       </p>
                                       <div class="balloon-bottom">
                                       </div>
                                   </div>
                                   <div class="author-info" style="margin-top: 15px; font-size: 10pt; color: #a1a1a1;">
                                       <asp:Image ID="imgUserComment3" Width="25" ImageUrl="/images/avatars/default.png"
                                           ToolTip="profile-picture" runat="server" />
                                       <p runat="server" id="pCommenter3">
                                       </p>
                                       <div id="pnlDocument3" runat="server" style="width:100%;text-align:left;" ></div>
                                       <asp:ImageButton ID="btnDeleteComment3" ImageUrl="/images/icons/Erase-Greyscale.png"
                                           ToolTip="Delete Comment" Width="12" runat="server" OnClick="btnDeleteComment_Click"
                                           CssClass="btn-comment-delete" />
                                   </div>
                               </div>
                               <div runat="server" id="divComment4" class="comment">
                                   <div class="balloon">
                                       <p runat="server" id="pComment4">
                                       </p>
                                       <div class="balloon-bottom">
                                       </div>
                                   </div>
                                   <div class="author-info" style="margin-top: 15px; font-size: 10pt; color: #a1a1a1;">
                                       <asp:Image ID="imgUserComment4" Width="25" ImageUrl="/images/avatars/default.png"
                                           ToolTip="profile-picture" runat="server" />
                                       <p runat="server" id="pCommenter4">
                                       </p>
                                       <div id="pnlDocument4" runat="server" style="width:100%;text-align:left;" ></div>
                                       <asp:ImageButton ID="btnDeleteComment4" ImageUrl="/images/icons/Erase-Greyscale.png"
                                           ToolTip="Delete Comment" Width="12" runat="server" OnClick="btnDeleteComment_Click"
                                           CssClass="btn-comment-delete" />
                                   </div>
                               </div>
                               <a class="add-new" href="javascript:void(0)">add new</a>
                               <%--<asp:LinkButton ID="btnPost" CssClass="post" NavigateUrl="javascript:void(0)" runat="server" OnClientClick="showLoadingSpinner('h2-comments');"
                                   OnClick="btnPost_Click">post</asp:LinkButton>--%>
                               <%--<a class="cancel" href="javascript:void(0)">cancel</a> --%>
                               <a class="fancy-button" style="display: inline-block;
                                   text-align: center; width: 88%; margin: 0 auto;" id="btnAllComment" runat="server">
                                   more comments</a>
                                   <asp:Button ID="btnAllRelateComment" OnClick="btnAllRelateComment_Click" OnClientClick="showLoadingSpinner('h2-comments');" ClientIDMode="Static" runat="server" CssClass="fancy-button AllReleateCommentButton" Text="Show related comments" />
                              <asp:HiddenField ID="hidden_count_main_comment" runat="server"/>
                              <asp:HiddenField ID="hidden_document_all_document_comments" runat="server"/>
                              <div id="modalExternalDms" class="modal-container" style="overflow:hidden;"></div>
                              <asp:HiddenField id="hidBasePath" ClientIDMode="Static" runat="server"/>
                           </ContentTemplate>
                       </asp:UpdatePanel>
                     
                   </div>
                   <!-- .comments-wrapper -->
               </div>
               <!-- .application-comments -->
               <div class="cb">
               </div>
           </div>
       </div>
    </asp:Panel>
</asp:Content>
