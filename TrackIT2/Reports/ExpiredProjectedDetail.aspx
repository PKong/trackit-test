﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/TrackIT2.Master" AutoEventWireup="true" CodeBehind="ExpiredProjectedDetail.aspx.cs" Inherits="TrackIT2.Reports.ExpiredProjectedDetail" %>
<%@ MasterType TypeName="TrackIT2.Templates.TrackIT2" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   <h2>Reports</h2>
   <h3>TrackiT Health - Expired Projected Details</h3>

   <asp:Panel ID="pnlNoCredentials" runat="server">
      <p>
         Please update your <asp:HyperLink ID="hypProfile" runat="server" NavigateUrl="~/Account/Profile.aspx">profile</asp:HyperLink> and 
         specify your reporting server credentials before attempting to view reports.
      </p>
   </asp:Panel>

   <asp:Panel ID="pnlReport" runat="server">
      <rsweb:ReportViewer ID="rptExpiredProjectedDetail" runat="server" Font-Names="Verdana" Font-Size="8pt" Height="1150px" Width="1000px">   
      </rsweb:ReportViewer>
   </asp:Panel>

</asp:Content>