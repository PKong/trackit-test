﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.Objects;

namespace TrackIT2.Reports
{
   public partial class FutureActualDetail : System.Web.UI.Page
   {
      protected void Page_Load(object sender, EventArgs e)
      {
         UserProfile profile = UserProfile.GetUserProfile();

         if (string.IsNullOrEmpty(profile.ReportLogin) ||
             string.IsNullOrEmpty(profile.ReportPassword))
         {
            pnlReport.Visible = false;
            pnlNoCredentials.Visible = true;
         }
         else
         {
            pnlNoCredentials.Visible = false;
            pnlReport.Visible = true;

            rptFutureActualDetail.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;
            rptFutureActualDetail.ServerReport.ReportServerUrl = new Uri("http://tmtref01.tmtcpt.com/ReportServer");
            rptFutureActualDetail.ServerReport.ReportPath = "/Trackit Data Health/Trackit Data Health - Future Actual Detail";
            rptFutureActualDetail.ServerReport.ReportServerCredentials = new BLL.ReportServerCredentials(profile.ReportLogin, profile.ReportPassword,
                                                                                                         ConfigurationManager.AppSettings["ReportServerDomain"]);
            rptFutureActualDetail.ServerReport.Refresh();
         }
      }
   }
}