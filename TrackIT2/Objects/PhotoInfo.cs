﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrackIT2.Objects
{
   public class PhotoInfo
   {
      public string photoID;
      public string photoURL;

      public PhotoInfo()
      {
         photoID = string.Empty;
         photoURL = string.Empty;
      }

      public PhotoInfo(string Id, string url)
      {
         photoID = Id;
         photoURL = url;
      }
   }
}