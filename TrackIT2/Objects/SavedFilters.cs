﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrackIT2.Objects
{
    /// <summary>
    /// Saved Filters
    /// </summary>
    [Serializable]
    public class SavedFilters
    {
        public List<SavedFilter> SavedFilterItems;
    }
    //>
   
    /// <summary>
    /// Saved Filter
    /// </summary>
    [Serializable]
    public class SavedFilter
    {
        // Var
        public string title;
        public string url;
        public string filter;
        public string listView;
        //>

        // Constructor #1
        public SavedFilter() { }
        //-->

        // Constructor #2
        public SavedFilter(string newTitle, string newUrl, string newFilter, string newlistView)
        {
            title = newTitle;
            url = newUrl;
            filter = newFilter;
            listView = newlistView;
        }
        //-->
    }
    //>
}