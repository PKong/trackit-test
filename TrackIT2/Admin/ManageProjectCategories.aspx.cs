﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.BLL;
using TrackIT2.DAL;

namespace TrackIT2.Admin
{
   public partial class ManageProjectCategories : System.Web.UI.Page
   {
      protected void Page_Load(object sender, EventArgs e)
      {
         if (!Page.IsPostBack)
         {
            BindCategories();
         }
      }

      protected void gvCategories_RowDataBound(object sender, GridViewRowEventArgs e)
      {
         /* GridView Row has following column layout
          * 0 - Actions (Edit)
          * 1 - Id
          * 2 - Name
          * 3 - Description
          */
         if (e.Row.RowType == DataControlRowType.DataRow)
         {
            metagroup group = (metagroup)e.Row.DataItem;

            if (e.Row.RowIndex == gvCategories.EditIndex)
            {
               LinkButton updateButton = (LinkButton)e.Row.Cells[0].FindControl("lnbUpdate");
               TextBox nameText = (TextBox)e.Row.Cells[2].FindControl("txtEditCategoryName");
               TextBox descriptionText = (TextBox)e.Row.Cells[3].FindControl("txtEditCategoryDescription");

               updateButton.CommandArgument = group.id.ToString();
               e.Row.Cells[1].Text = group.id.ToString();
               nameText.Text = group.name;

               if (!String.IsNullOrEmpty(group.description))
               {
                  descriptionText.Text = group.description;
               }

            }
            else
            {
               LinkButton editButton = (LinkButton)e.Row.Cells[0].FindControl("lnbEdit");
               LinkButton deleteButton = (LinkButton)e.Row.Cells[0].FindControl("lnbDelete");
               LinkButton fieldsButton = (LinkButton)e.Row.Cells[0].FindControl("lnbSelect");

               if (gvCategories.EditIndex == -1)
               {
                  editButton.CommandArgument = group.id.ToString();
                  deleteButton.CommandArgument = group.id.ToString();
               }
               else
               {
                  editButton.Visible = false;
                  deleteButton.Visible = false;
                  fieldsButton.Visible = false;
               }

               e.Row.Cells[1].Text = group.id.ToString();
               e.Row.Cells[2].Text = group.name;

               if (!String.IsNullOrEmpty(group.description))
               {
                  e.Row.Cells[3].Text = group.description;
               }
               else
               {
                  e.Row.Cells[3].Text = "None.";
               }
            }
         }
      }

      protected void gvCategories_SelectedIndexChanged(object sender, EventArgs e)
      {
         int id = (int)gvCategories.SelectedDataKey.Value;
         pnlFieldInfo.Visible = true;
         BindMetaFields(id);
      }

      protected void gvCategories_RowEditing(object sender, GridViewEditEventArgs e)
      {
         CPTTEntities ce = new CPTTEntities();
         metagroup group = new metagroup();
         int id = (int)gvCategories.DataKeys[e.NewEditIndex].Value;

         group = ce.metagroups
                   .Where(mg => mg.id.Equals(id))
                   .First();

         // Detach the metadata group from the entity context so it can be used
         // for subsequent updates.
         ce.Detach(group);
         Session["currentCategory"] = group;

         gvCategories.EditIndex = e.NewEditIndex;
         pnlFieldInfo.Visible = false;
         BindCategories();
      }

      protected void gvCategories_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
      {
         gvCategories.EditIndex = -1;
         Session["currentCategory"] = null;
         BindCategories();

         Master.StatusBox.showStatusMessage("<p>Edit Cancelled.</p>");
      }

      protected void gvCategories_RowUpdating(object sender, GridViewUpdateEventArgs e)
      {
         /* GridView Row has following column layout
          * 0 - Actions (Edit)
          * 1 - Id
          * 2 - Name
          * 3 - Description
          */
         if (Page.IsValid)
         {
            int id = (int)gvCategories.DataKeys[e.RowIndex].Value;
            metagroup group = (metagroup)Session["currentMetaGroup"];
            GridViewRow editRow = gvCategories.Rows[e.RowIndex];

            TextBox txtName = (TextBox)editRow.Cells[2].FindControl("txtEditName");
            TextBox txtDescription = (TextBox)editRow.Cells[3].FindControl("txtEditDescription");

            try
            {
               MetaGroup.Update(group, txtName.Text.Trim(), txtDescription.Text.Trim());
               gvCategories.EditIndex = -1;
               BindCategories();
               Session["currentMetaGroup"] = null;

               Master.StatusBox.showStatusMessage("<p>Group updated.");
            }
            catch (OptimisticConcurrencyException)
            {
               string message = "<p>This record has been modified since you last loaded it.</p>" +
                                "<p>There may be new changes to the record.</p>" +
                                "<p>Please reload</a> the record and try again.</p>";
               Master.StatusBox.showErrorMessage(message);
            }
         }
      }

      protected void gvCategories_RowDeleting
                     (object sender, GridViewDeleteEventArgs e)
      {
         int id = (int)gvCategories.DataKeys[e.RowIndex].Value;

         try
         {
            MetaGroup.Delete(id);
            BindCategories();

            Master.StatusBox.showStatusMessage("<p>Group Deleted.</p>");
         }
         catch (UpdateException)
         {
            Master.StatusBox.showErrorMessage
                             ("<p>There are metadata fields that reference " +
                              "this group.</p><p>Please remove these fields " +
                              "before removing the group.</p>");
         }
      }

      protected void gvFields_RowDataBound(object sender, GridViewRowEventArgs e)
      {
         /* GridView Row has following column layout
          * 0 - Actions (Edit)
          * 1 - Id
          * 2 - Table
          * 3 - Database Field (Column)
          * 4 - Friendly Name
          */
         if (e.Row.RowType == DataControlRowType.DataRow)
         {
            metadata field = (metadata)e.Row.DataItem;

            if (e.Row.RowIndex == gvFields.EditIndex)
            {
               LinkButton lnbUpdate = (LinkButton)e.Row.Cells[0].FindControl("lnbUpdate");
               TextBox txtTable = (TextBox)e.Row.Cells[2].FindControl("txtEditTable");
               TextBox txtColumn = (TextBox)e.Row.Cells[3].FindControl("txtEditColumn");
               TextBox txtFriendlyName = (TextBox)e.Row.Cells[4].FindControl("txtEditFriendlyName");

               lnbUpdate.CommandArgument = field.id.ToString();
               e.Row.Cells[1].Text = field.id.ToString();
               txtTable.Text = field.table;
               txtColumn.Text = field.column;
               txtFriendlyName.Text = field.friendly_name;
            }
            else
            {
               LinkButton lnbEdit = (LinkButton)e.Row.Cells[0].FindControl("lnbEdit");
               LinkButton lnbDelete = (LinkButton)e.Row.Cells[0].FindControl("lnbDelete");

               if (gvFields.EditIndex == -1)
               {
                  lnbEdit.CommandArgument = field.id.ToString();
                  lnbDelete.CommandArgument = field.id.ToString();
               }
               else
               {
                  lnbEdit.Visible = false;
                  lnbDelete.Visible = false;
               }

               e.Row.Cells[1].Text = field.id.ToString();
               e.Row.Cells[2].Text = field.table;
               e.Row.Cells[3].Text = field.column;
               e.Row.Cells[4].Text = field.friendly_name;
            }
         }
      }

      protected void gvFields_RowEditing(object sender, GridViewEditEventArgs e)
      {
         CPTTEntities ce = new CPTTEntities();
         metadata field = new metadata();
         int id = (int)gvFields.DataKeys[e.NewEditIndex].Value;
         int groupId = (int)gvCategories.SelectedDataKey.Value;

         field = ce.metadatas
                   .Where(md => md.id.Equals(id))
                   .First();

         // Detach the metadata field from the entity context so it can be used
         // for subsequent updates.
         ce.Detach(field);
         Session["currentMetaField"] = field;

         gvFields.EditIndex = e.NewEditIndex;
         BindMetaFields(groupId);
      }

      protected void gvFields_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
      {
         int groupId = (int)gvCategories.SelectedDataKey.Value;
         gvFields.EditIndex = -1;
         Session["currentMetaField"] = null;
         BindMetaFields(groupId);

         Master.StatusBox.showStatusMessage("<p>Edit Cancelled.</p>");
      }

      protected void gvFields_RowUpdating(object sender, GridViewUpdateEventArgs e)
      {
         /* GridView Row has following column layout
          * 0 - Actions (Edit)
          * 1 - Id
          * 2 - Table
          * 3 - Database Field (Column)
          * 4 - Friendly Name
          */
         if (Page.IsValid)
         {

            int id = (int)gvFields.DataKeys[e.RowIndex].Value;
            int groupId = (int)gvCategories.SelectedDataKey.Value;
            metadata field = (metadata)Session["currentMetaField"];
            GridViewRow editRow = gvFields.Rows[e.RowIndex];

            TextBox txtTable = (TextBox)editRow.Cells[2].FindControl("txtEditTable");
            TextBox txtColumn = (TextBox)editRow.Cells[3].FindControl("txtEditColumn");
            TextBox txtFriendlyName = (TextBox)editRow.Cells[4].FindControl("txtEditFriendlyName");

            try
            {
               MetaData.Update(field, txtTable.Text.Trim(), txtColumn.Text.Trim(),
                               txtFriendlyName.Text.Trim());
               gvFields.EditIndex = -1;
               BindMetaFields(groupId);
               Session["currentMetaField"] = null;

               Master.StatusBox.showStatusMessage("<p>Metadata field updated.");
            }
            catch (OptimisticConcurrencyException)
            {
               string message = "<p>This record has been modified since you last loaded it.</p>" +
                                "<p>There may be new changes to the record.</p>" +
                                "<p>Please reload</a> the record and try again.</p>";
               Master.StatusBox.showErrorMessage(message);
            }
         }
      }

      protected void gvFields_RowDeleting
                     (object sender, GridViewDeleteEventArgs e)
      {
         int id = (int)gvFields.DataKeys[e.RowIndex].Value;
         int groupId = (int)gvCategories.SelectedDataKey.Value;

         MetaData.Delete(id);
         BindMetaFields(groupId);

         Master.StatusBox.showStatusMessage("<p>Field Deleted.</p>");
      }

      protected void lnbAddCategory_Click(object sender, EventArgs e)
      {
         TextBox txtName = (TextBox)gvCategories.FooterRow.FindControl("txtAddCategoryName");
         TextBox txtDescription = (TextBox)gvCategories.FooterRow.FindControl("txtAddCategoryDescription");


         if (!String.IsNullOrWhiteSpace(txtName.Text))
         {
            try
            {
               MetaGroup.Add(txtName.Text.Trim(), txtDescription.Text.Trim());
               BindCategories();
               Master.StatusBox.showStatusMessage("Category added.");
            }
            catch (UpdateException)
            {
               Master.StatusBox.showErrorMessage
                                ("A record already exists with the table and " +
                                 "database field specified.");
            }
         }
         else
         {
            Master.StatusBox.showErrorMessage("Please specify a name for the group.");
         }
      }

      protected void lnbAddField_Click(object sender, EventArgs e)
      {
         TextBox txtTable = (TextBox)(((LinkButton)sender).NamingContainer.FindControl("txtAddFieldTable"));
         TextBox txtColumn = (TextBox)(((LinkButton)sender).NamingContainer.FindControl("txtAddFieldColumn"));
         TextBox txtFriendlyName = (TextBox)(((LinkButton)sender).NamingContainer.FindControl("txtAddFieldFriendlyName"));

         if (!String.IsNullOrWhiteSpace(txtTable.Text) && !String.IsNullOrWhiteSpace(txtColumn.Text))
         {
            try
            {
               int groupId = (int)gvCategories.SelectedDataKey.Value;
               MetaData.Add(txtTable.Text.Trim(), txtColumn.Text.Trim(),
                            txtFriendlyName.Text.Trim(), groupId);

               BindMetaFields(groupId);
               Master.StatusBox.showStatusMessage("Field added.");
            }
            catch (UpdateException)
            {
               Master.StatusBox.showErrorMessage
                                ("A record already exists with the table and " +
                                 "database field specified.");
            }
         }
         else
         {
            Master.StatusBox.showErrorMessage
                             ("Please make sure the table, and database field," +
                              "<br />are specified for the new category field.");
         }
      }

      protected void lnbAddFieldFooter_Click(object sender, EventArgs e)
      {
         TextBox txtTable = (TextBox)(((LinkButton)sender).NamingContainer.FindControl("txtAddFieldTable"));
         TextBox txtColumn = (TextBox)(((LinkButton)sender).NamingContainer.FindControl("txtAddFieldColumn"));
         TextBox txtFriendlyName = (TextBox)(((LinkButton)sender).NamingContainer.FindControl("txtAddFieldFriendlyName"));

         if (!String.IsNullOrWhiteSpace(txtTable.Text) && !String.IsNullOrWhiteSpace(txtColumn.Text))
         {
            try
            {
               int groupId = (int)gvCategories.SelectedDataKey.Value;
               MetaData.Add(txtTable.Text.Trim(), txtColumn.Text.Trim(),
                            txtFriendlyName.Text.Trim(), groupId);

               BindMetaFields(groupId);
               Master.StatusBox.showStatusMessage("Field added.");
            }
            catch (UpdateException)
            {
               Master.StatusBox.showErrorMessage
                                ("A record already exists with the table and " +
                                 "database field specified.");
            }
         }
         else
         {
            Master.StatusBox.showErrorMessage
                             ("Please make sure the table, and database field," +
                              "<br />are specified for the new category field.");
         }
      }

      protected void BindCategories()
      {
         CPTTEntities ce = new CPTTEntities();
         List<metagroup> groups = new List<metagroup>();

         groups = ce.metagroups
                    .Where(g => g.name.Contains("Category"))
                    .OrderBy(Utility.GetField<metagroup>("name"))
                    .ToList();

         gvCategories.DataSource = groups;
         gvCategories.DataBind();
      }

      protected void BindMetaFields(int groupId)
      {
         CPTTEntities ce = new CPTTEntities();
         List<metadata> fields = new List<metadata>();

         fields = ce.metadatas
                    .Where(md => md.metagroup.id.Equals(groupId))
                    .ToList();

         gvFields.DataSource = fields;
         gvFields.DataBind();
      }

      protected void gvCategories_PageIndexChanging(object sender, GridViewPageEventArgs e)
      {
         gvCategories.PageIndex = e.NewPageIndex;
         gvCategories.SelectedIndex = -1;
         pnlFieldInfo.Visible = false;
         BindCategories();
      }
   }
}