﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.BLL;
using TrackIT2.DAL;

namespace TrackIT2.Admin
{
   public partial class ManageMetadata : System.Web.UI.Page
   {
      protected void Page_Load(object sender, EventArgs e)
      {
         if (!Page.IsPostBack)
         {
            BindMetaGroups();
         }
      }

      protected void gvGroups_RowDataBound(object sender, GridViewRowEventArgs e)
      {
         /* GridView Row has following column layout
          * 0 - Actions (Edit)
          * 1 - Id
          * 2 - Name
          * 3 - Description
          */
         if (e.Row.RowType == DataControlRowType.DataRow)
         {
            metagroup group = (metagroup)e.Row.DataItem;

            if (e.Row.RowIndex == gvGroups.EditIndex)
            {            
               LinkButton updateButton = (LinkButton)e.Row.Cells[0].FindControl("lnbUpdate");
               TextBox nameText = (TextBox)e.Row.Cells[2].FindControl("txtEditName");
               TextBox descriptionText = (TextBox)e.Row.Cells[3].FindControl("txtEditDescription");

               updateButton.CommandArgument = group.id.ToString();
               e.Row.Cells[1].Text = group.id.ToString();
               nameText.Text = group.name;

               if (!String.IsNullOrEmpty(group.description))
               {
                  descriptionText.Text = group.description;
               }

            }
            else
            {
               LinkButton editButton = (LinkButton)e.Row.Cells[0].FindControl("lnbEdit");
               LinkButton deleteButton = (LinkButton)e.Row.Cells[0].FindControl("lnbDelete");
               LinkButton fieldsButton = (LinkButton)e.Row.Cells[0].FindControl("lnbSelect");

               if (gvGroups.EditIndex == -1)
               {
                  editButton.CommandArgument = group.id.ToString();
                  deleteButton.CommandArgument = group.id.ToString();
               }
               else
               {
                  editButton.Visible = false;
                  deleteButton.Visible = false;
                  fieldsButton.Visible = false;
               }
               
               e.Row.Cells[1].Text = group.id.ToString();
               e.Row.Cells[2].Text = group.name;

               if (!String.IsNullOrEmpty(group.description))
               {
                  e.Row.Cells[3].Text = group.description;
               }
               else
               {
                  e.Row.Cells[3].Text = "None.";
               }
            }            
         }
      }

      protected void gvGroups_SelectedIndexChanged(object sender, EventArgs e)
      {
         int id = (int)gvGroups.SelectedDataKey.Value;
         pnlFieldInfo.Visible = true;
         BindMetaFields(id);
      }

      protected void gvGroups_RowEditing(object sender, GridViewEditEventArgs e)
      {
         CPTTEntities ce = new CPTTEntities();
         metagroup group = new metagroup();
         int id = (int)gvGroups.DataKeys[e.NewEditIndex].Value;

         group = ce.metagroups
                   .Where(mg => mg.id.Equals(id))
                   .First();

         // Detach the metadata group from the entity context so it can be used
         // for subsequent updates.
         ce.Detach(group);
         Session["currentMetaGroup"] = group;
         
         gvGroups.EditIndex = e.NewEditIndex;
         pnlFieldInfo.Visible = false;
         BindMetaGroups();
      }

      protected void gvGroups_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
      {
         gvGroups.EditIndex = -1;
         Session["currentMetaGroup"] = null;
         BindMetaGroups();

         Master.StatusBox.showStatusMessage("<p>Edit Cancelled.</p>");
      }

      protected void gvGroups_RowUpdating(object sender, GridViewUpdateEventArgs e)
      {
         /* GridView Row has following column layout
          * 0 - Actions (Edit)
          * 1 - Id
          * 2 - Name
          * 3 - Description
          */
         if (Page.IsValid)
         {
            int id = (int)gvGroups.DataKeys[e.RowIndex].Value;
            metagroup group = (metagroup)Session["currentMetaGroup"];
            GridViewRow editRow = gvGroups.Rows[e.RowIndex];

            TextBox txtName = (TextBox)editRow.Cells[2].FindControl("txtEditName");
            TextBox txtDescription = (TextBox)editRow.Cells[3].FindControl("txtEditDescription");

            try
            {
               MetaGroup.Update(group, txtName.Text.Trim(), txtDescription.Text.Trim());
               gvGroups.EditIndex = -1;
               BindMetaGroups();
               Session["currentMetaGroup"] = null;

               Master.StatusBox.showStatusMessage("<p>Group updated.");
            }
            catch (OptimisticConcurrencyException)
            {
               string message = "<p>This record has been modified since you last loaded it.</p>" +
                                "<p>There may be new changes to the record.</p>" +
                                "<p>Please reload</a> the record and try again.</p>";
               Master.StatusBox.showErrorMessage(message);
            }
         }
      }

      protected void gvGroups_RowDeleting
                     (object sender, GridViewDeleteEventArgs e)
      {
         int id = (int)gvGroups.DataKeys[e.RowIndex].Value;

         try
         {
            MetaGroup.Delete(id);
            BindMetaGroups();

            Master.StatusBox.showStatusMessage("<p>Group Deleted.</p>");
         }
         catch (UpdateException)
         {
            Master.StatusBox.showErrorMessage
                             ("<p>There are metadata fields that reference " + 
                              "this group.</p><p>Please remove these fields " +
                              "before removing the group.</p>");
         }         
      }

      protected void gvFields_RowDataBound(object sender, GridViewRowEventArgs e)
      {
         /* GridView Row has following column layout
          * 0 - Actions (Edit)
          * 1 - Id
          * 2 - Table
          * 3 - Database Field (Column)
          * 4 - Friendly Name
          */
         if (e.Row.RowType == DataControlRowType.DataRow)
         {
            metadata field = (metadata)e.Row.DataItem;

            if (e.Row.RowIndex == gvFields.EditIndex)
            {
               LinkButton lnbUpdate = (LinkButton)e.Row.Cells[0].FindControl("lnbUpdate");
               TextBox txtTable = (TextBox)e.Row.Cells[2].FindControl("txtEditTable");
               TextBox txtColumn = (TextBox)e.Row.Cells[3].FindControl("txtEditColumn");
               TextBox txtFriendlyName = (TextBox)e.Row.Cells[4].FindControl("txtEditFriendlyName");

               lnbUpdate.CommandArgument = field.id.ToString();
               e.Row.Cells[1].Text = field.id.ToString();
               txtTable.Text = field.table;
               txtColumn.Text = field.column;
               txtFriendlyName.Text = field.friendly_name;            
            }
            else
            {
               LinkButton lnbEdit = (LinkButton)e.Row.Cells[0].FindControl("lnbEdit");
               LinkButton lnbDelete = (LinkButton)e.Row.Cells[0].FindControl("lnbDelete");

               if (gvFields.EditIndex == -1)
               {
                  lnbEdit.CommandArgument = field.id.ToString();
                  lnbDelete.CommandArgument = field.id.ToString();
               }
               else
               {
                  lnbEdit.Visible = false;
                  lnbDelete.Visible = false;
               }

               e.Row.Cells[1].Text = field.id.ToString();
               e.Row.Cells[2].Text = field.table;
               e.Row.Cells[3].Text = field.column;
               e.Row.Cells[4].Text = field.friendly_name;
            }
         }
      }

      protected void gvFields_RowEditing(object sender, GridViewEditEventArgs e)
      {
         CPTTEntities ce = new CPTTEntities();
         metadata field = new metadata();
         int id = (int)gvFields.DataKeys[e.NewEditIndex].Value;
         int groupId = (int)gvGroups.SelectedDataKey.Value;

         field = ce.metadatas
                   .Where(md => md.id.Equals(id))
                   .First();

         // Detach the metadata field from the entity context so it can be used
         // for subsequent updates.
         ce.Detach(field);
         Session["currentMetaField"] = field;
         
         gvFields.EditIndex = e.NewEditIndex;
         BindMetaFields(groupId);
      }

      protected void gvFields_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
      {
         int groupId = (int)gvGroups.SelectedDataKey.Value;
         gvFields.EditIndex = -1;
         Session["currentMetaField"] = null;
         BindMetaFields(groupId);

         Master.StatusBox.showStatusMessage("<p>Edit Cancelled.</p>");
      }

      protected void gvFields_RowUpdating(object sender, GridViewUpdateEventArgs e)
      {
         /* GridView Row has following column layout
          * 0 - Actions (Edit)
          * 1 - Id
          * 2 - Table
          * 3 - Database Field (Column)
          * 4 - Friendly Name
          */
         if (Page.IsValid)
         {

            int id = (int)gvFields.DataKeys[e.RowIndex].Value;
            int groupId = (int)gvGroups.SelectedDataKey.Value;
            metadata field = (metadata)Session["currentMetaField"];
            GridViewRow editRow = gvFields.Rows[e.RowIndex];

            TextBox txtTable = (TextBox)editRow.Cells[2].FindControl("txtEditTable");
            TextBox txtColumn = (TextBox)editRow.Cells[3].FindControl("txtEditColumn");
            TextBox txtFriendlyName = (TextBox)editRow.Cells[4].FindControl("txtEditFriendlyName");

            try
            {
               MetaData.Update(field, txtTable.Text.Trim(), txtColumn.Text.Trim(),
                               txtFriendlyName.Text.Trim());
               gvFields.EditIndex = -1;
               BindMetaFields(groupId);
               Session["currentMetaField"] = null;

               Master.StatusBox.showStatusMessage("<p>Metadata field updated.");
            }
            catch (OptimisticConcurrencyException)
            {
               string message = "<p>This record has been modified since you last loaded it.</p>" +
                                "<p>There may be new changes to the record.</p>" +
                                "<p>Please reload</a> the record and try again.</p>";
               Master.StatusBox.showErrorMessage(message);
            }
         }
      }

      protected void gvFields_RowDeleting
                     (object sender, GridViewDeleteEventArgs e)
      {
         int id = (int)gvFields.DataKeys[e.RowIndex].Value;
         int groupId = (int)gvGroups.SelectedDataKey.Value;

         MetaData.Delete(id);
         BindMetaFields(groupId);

         Master.StatusBox.showStatusMessage("<p>Field Deleted.</p>");
      }

      protected void lnbAddGroup_Click(object sender, EventArgs e)
      {
         TextBox txtName = (TextBox) gvGroups.FooterRow.FindControl("txtAddGroupName");
         TextBox txtDescription = (TextBox)gvGroups.FooterRow.FindControl("txtAddGroupDescription");


         if (!String.IsNullOrWhiteSpace(txtName.Text))
         {
            try
            {
               MetaGroup.Add(txtName.Text.Trim(), txtDescription.Text.Trim());
               BindMetaGroups();
               Master.StatusBox.showStatusMessage("Group added.");
            }
            catch (UpdateException)
            {
               Master.StatusBox.showErrorMessage
                                ("A record already exists with the table and " +
                                 "database field specified.");
            }                        
         }
         else
         {
            Master.StatusBox.showErrorMessage("Please specify a name for the group.");
         }
      }

      protected void lnbAddField_Click(object sender, EventArgs e)
      {
         TextBox txtTable = (TextBox) (((LinkButton)sender).NamingContainer.FindControl("txtAddFieldTable"));
         TextBox txtColumn = (TextBox)(((LinkButton)sender).NamingContainer.FindControl("txtAddFieldColumn"));
         TextBox txtFriendlyName = (TextBox)(((LinkButton)sender).NamingContainer.FindControl("txtAddFieldFriendlyName"));

         if (!String.IsNullOrWhiteSpace(txtTable.Text) && !String.IsNullOrWhiteSpace(txtColumn.Text))
         {
            try
            {
               int groupId = (int)gvGroups.SelectedDataKey.Value;
               MetaData.Add(txtTable.Text.Trim(), txtColumn.Text.Trim(),
                            txtFriendlyName.Text.Trim(), groupId);

               BindMetaFields(groupId);
               Master.StatusBox.showStatusMessage("Field added.");
            }
            catch (UpdateException)
            {
               Master.StatusBox.showErrorMessage
                                ("A record already exists with the table and " +
                                 "database field specified.");
            }
         }
         else
         {
            Master.StatusBox.showErrorMessage
                             ("Please make sure the table and database field," +
                              "<br/> are specified for the new metadata field.");
         }
      }

      protected void lnbAddFieldFooter_Click(object sender, EventArgs e)
      {
         TextBox txtTable = (TextBox) gvFields.FooterRow.FindControl("txtAddFieldTable");
         TextBox txtColumn = (TextBox)gvFields.FooterRow.FindControl("txtAddFieldColumn");
         TextBox txtFriendlyName = (TextBox)gvFields.FooterRow.FindControl("txtAddFieldFriendlyName");
         
         if (!String.IsNullOrWhiteSpace(txtTable.Text) && !String.IsNullOrWhiteSpace(txtColumn.Text))
         {
            try
            {
               int groupId = (int)gvGroups.SelectedDataKey.Value;
               MetaData.Add(txtTable.Text.Trim(), txtColumn.Text.Trim(),
                            txtFriendlyName.Text.Trim(), groupId);

               BindMetaFields(groupId);
               Master.StatusBox.showStatusMessage("Field added.");
            }
            catch (UpdateException)
            {
               Master.StatusBox.showErrorMessage
                                ("A record already exists with the table and " +
                                 "database field specified.");
            }            
         }
         else
         {
            Master.StatusBox.showErrorMessage
                             ("Please make sure the table and database field" + 
                              "<br/>are specified for the new metadata field.");
         }
      }

      protected void BindMetaGroups()
      {
         CPTTEntities ce = new CPTTEntities();
         List<metagroup> groups = new List<metagroup>();

         groups = ce.metagroups
                    .OrderBy(Utility.GetField<metagroup>("name"))
                    .ToList();

         gvGroups.DataSource = groups;
         gvGroups.DataBind();
      }

      protected void BindMetaFields(int groupId)
      {
         CPTTEntities ce = new CPTTEntities();
         List<metadata> fields = new List<metadata>();

         fields = ce.metadatas
                    .Where(md => md.metagroup.id.Equals(groupId))
                    .ToList();

         gvFields.DataSource = fields;
         gvFields.DataBind();     
      }

      protected void gvGroups_PageIndexChanging(object sender, GridViewPageEventArgs e)
      {
         gvGroups.PageIndex = e.NewPageIndex;
         gvGroups.SelectedIndex = -1;
         pnlFieldInfo.Visible = false;
         BindMetaGroups();
      }                  
   }
}