﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/TrackIT2.Master" AutoEventWireup="true" CodeBehind="ManageFieldAccess.aspx.cs" Inherits="TrackIT2.Admin.ManageFieldAccess" %>
<%@ MasterType TypeName="TrackIT2.Templates.TrackIT2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   <h2>Manage Application Field Security</h2>

   <h3>Add New Field Security</h3>

   <p>            
      <b>Access Type: </b>
      <asp:RadioButtonList ID="rblAccessType" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
         <asp:ListItem Text="Role" Value="role" Selected="True"></asp:ListItem>
         <asp:ListItem Text="User" Value="user"></asp:ListItem>
      </asp:RadioButtonList>
   </p>

   <p>
      <b>Access Name: </b>
      <asp:TextBox ID="txtAccessName" runat="server" MaxLength="256"></asp:TextBox>
   </p>

   <p>
      <b>Table Name: </b>
      <asp:TextBox ID="txtTableName" runat="server" MaxLength="256"></asp:TextBox>
   </p>

   <p>
      <b>Field Name: </b>
      <asp:TextBox ID="txtFieldName" runat="server" MaxLength="256"></asp:TextBox>
   </p>

   <p>
      <b>Security: </b>
      <asp:CheckBox id="chkCanView" runat="server" Text="View" Checked="true" />
      <asp:CheckBox id="chkCanEdit" runat="server" Text="Edit" />
   </p>

   <p>
      <asp:Button ID="btnAddAccess" runat="server" Text="Add" onclick="btnAddAccess_Click" />
   </p>

   <p>&nbsp;</p>

   <h3>Current Field Security</h3>

   <p>
      <b>Filter By Name: </b>
      <asp:DropDownList ID="ddlFilterByName" runat="server"></asp:DropDownList>
      &nbsp;&nbsp;
      <asp:Button ID="btnFilterByName" runat="server" Text="Filter" 
         onclick="btnFilterByName_Click" />
   </p>   

   <asp:GridView ID="gvAccess" runat="server"                  
                 AllowPaging="True" 
                 AllowSorting="True"
                 AutoGenerateColumns="False"
                 DataKeyNames="id"
                 PageSize="30" 
                 onpageindexchanging="gvAccess_PageIndexChanging"
                 onrowcancelingedit="gvAccess_RowCancelingEdit"                  
                 onrowdatabound="gvAccess_RowDataBound"                  
                 onrowdeleting="gvAccess_RowDeleting"                           
                 onrowediting="gvAccess_RowEditing"
                 onrowupdating="gvAccess_RowUpdating"
                 onsorting="gvAccess_Sorting">

        <AlternatingRowStyle CssClass="GridViewAlternateRow" />      
        <SelectedRowStyle CssClass="GridViewSelectedRow" />         
        <EditRowStyle CssClass="GridViewEditRow" />

        <Columns>
            <asp:TemplateField HeaderText="Options">
                <ItemTemplate>
                    <asp:LinkButton ID="lnbEdit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    &nbsp;&nbsp;
                    <asp:LinkButton ID="lnbDelete" runat="server" CommandName="Delete" Text="Delete" OnClientClick="return confirm('Are you sure you want to remove this access?');"></asp:LinkButton>
                </ItemTemplate>

                <EditItemTemplate>
                  <asp:LinkButton ID="lnbCancel" runat="server" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                  &nbsp;&nbsp;
                  <asp:LinkButton ID="lnbUpdate" runat="server" CommandName="Update" Text="Update"></asp:LinkButton>
                </EditItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Access Type" SortExpression="type">
               <EditItemTemplate>
                  <asp:RadioButtonList ID="rblEditAccessType" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                     <asp:ListItem Text="Role" Value="role" Selected="True"></asp:ListItem>
                     <asp:ListItem Text="User" Value="user"></asp:ListItem>
                  </asp:RadioButtonList>
               </EditItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Access Name" SortExpression="name">
               <EditItemTemplate>
                  <asp:TextBox ID="txtEditAccessName" runat="server" MaxLength="256"></asp:TextBox>
               </EditItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Table Name" SortExpression="table_name">
               <EditItemTemplate>
                  <asp:TextBox ID="txtEditTableName" runat="server" MaxLength="256"></asp:TextBox>
               </EditItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Field Name" SortExpression="field_name">
               <EditItemTemplate>
                  <asp:TextBox ID="txtEditFieldName" runat="server" MaxLength="256"></asp:TextBox>
               </EditItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Can View" SortExpression="can_view">
               <EditItemTemplate>
                  <asp:CheckBox id="chkEditCanView" runat="server" Text="View" />
               </EditItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Can Edit" SortExpression="can_edit">
               <EditItemTemplate>
                  <asp:CheckBox id="chkEditCanEdit" runat="server" Text="Edit" />
               </EditItemTemplate>
            </asp:TemplateField>                    
        </Columns>
    </asp:GridView>
</asp:Content>
