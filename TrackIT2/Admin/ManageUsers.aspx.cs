﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.DAL;
using Utility = TrackIT2.BLL.Utility;

namespace TrackIT2.Admin
{
   public partial class ManageUsers : System.Web.UI.Page
   {
      protected void Page_Load(object sender, EventArgs e)
      {
         if (!Page.IsPostBack)
         {
            lblUserDetails.Text = "Add User";
            btnUpdateUser.Visible = false;
            btnCancelUpdate.Visible = false;
            btnCreateUser.Visible = true;
            pnlPasswordDetails.Visible = true;

            Session["UserSortCol"] = "tmo_userid";
            Session["UserSortDir"] = "ASC";
            BindUsers();
            BindLookupLists();
         }
      }

      protected void BindUsers()
      {
         CPTTEntities ce = new CPTTEntities();
         List<user> users = new List<user>();
         string sortCol = (string)Session["UserSortCol"];
         string sortDir = (string)Session["UserSortDir"];

         if (sortDir == "ASC")
         {
            users = ce.users
                      .Where(u => u.aspnet_user_id.HasValue)
                      .OrderBy(Utility.GetField<user>(sortCol))
                      .ToList();
         }
         else
         {
            users = ce.users
                    .Where(u => u.aspnet_user_id.HasValue)
                    .OrderByDescending(Utility.GetField<user>(sortCol))
                    .ToList();
         }

         gvUsers.DataSource = users;
         gvUsers.DataBind();
      }

      protected void gvUsers_RowDataBound(object sender, GridViewRowEventArgs e)
      {
         /* GridView Row has following column layout
         * 0 - Actions (Edit)
         * 1 - TMO Id
         * 2 - Active 
         * 3 - E-mail
         * 4 - First Name
         * 5 - Last Name
         * 6 - Track iT Role
         * 7 - Job Role
         * 8 - Employee Type
         * 9 - Manager
         */
         if (e.Row.RowType == DataControlRowType.Header)
         {
            LinkButton sortLink;
            string sortCol = (string)Session["UserSortCol"];
            string sortDir = (string)Session["UserSortDir"];

            // Use the HTML safe codes for the up arrow ? and down arrow ?.
            string sortArrow = sortDir == "ASC" ? "&#9650;" : "&#9660;";

            // GridView rows with sortable columns will have a linkbutton
            // generated. Compare to the CommandArgument since this is what
            // we set our sortColumn based on.
            foreach (System.Web.UI.WebControls.TableCell currCell in e.Row.Cells)
            {
               if (currCell.HasControls())
               {
                  sortLink = ((LinkButton)currCell.Controls[0]);
                  if (sortLink.CommandArgument == sortCol)
                  {
                     sortLink.Text = sortLink.Text + " " + sortArrow;
                  }
               }
            }
         }

         if (e.Row.RowType == DataControlRowType.DataRow)
         {
            CPTTEntities ce = new CPTTEntities();
            user currUser = (user)e.Row.DataItem;
            user manager;
            List<aspnet_Roles> userRoles = new List<aspnet_Roles>();
            List<string> currRoles = new List<string>();

            LinkButton editButton = (LinkButton)e.Row.Cells[0].FindControl("lnbEdit");
            LinkButton deleteButton = (LinkButton)e.Row.Cells[0].FindControl("lnbDelete");               

            if (gvUsers.SelectedIndex == -1)
            {
               editButton.CommandArgument = currUser.id.ToString();
               deleteButton.CommandArgument = currUser.id.ToString();
            }
            else
            {
               editButton.Visible = false;
               deleteButton.Visible = false;
            }

            if (!string.IsNullOrEmpty(currUser.tmo_userid))
            {
               e.Row.Cells[1].Text = currUser.tmo_userid;
            }

            if (currUser.active_emp_yn.HasValue)
            {
               e.Row.Cells[2].Text = currUser.active_emp_yn.ToString();
            }
            else
            {
               e.Row.Cells[2].Text = "None.";
            }
            
            e.Row.Cells[3].Text = currUser.email;
            e.Row.Cells[4].Text = currUser.first_name;
            e.Row.Cells[5].Text = currUser.last_name;

            userRoles = currUser.aspnet_Users.aspnet_Roles.ToList();
            currRoles = new List<string>();

            foreach (aspnet_Roles role in userRoles)
            {
               currRoles.Add(role.RoleName);
            }

            e.Row.Cells[6].Text = String.Join(", ", currRoles);

            if (currUser.job_role_id.HasValue)
            {
               e.Row.Cells[7].Text = currUser.job_roles.job_role;
            }
            else
            {
               e.Row.Cells[7].Text = "None.";
            }
            
            e.Row.Cells[8].Text = currUser.emp_types.emp_type;

            if (currUser.dir_mgr_user_uid.HasValue)
            {
               manager = ce.users.Where(u => u.id.Equals(currUser.dir_mgr_user_uid.Value)).First();
               e.Row.Cells[9].Text = manager.first_name + " " + manager.last_name;
            }
            else
            {
               e.Row.Cells[9].Text = "None.";
            }
         }
      }

      protected void BindLookupLists()
      {
         CPTTEntities ce = new CPTTEntities();

         List<aspnet_Roles> appRoles = new List<aspnet_Roles>();
         List<job_roles> jobRoles = new List<job_roles>();
         List<emp_types> employeeTypes = new List<emp_types>();
         List<user> managers = new List<user>();
         ListItem liManager;

         appRoles = ce.aspnet_Roles.OrderBy(Utility.GetField<aspnet_Roles>("RoleName")).ToList();
         cblRole.DataSource = appRoles;
         cblRole.DataTextField = "RoleName";
         cblRole.DataValueField = "RoleId";
         cblRole.DataBind();

         jobRoles = ce.job_roles.OrderBy(Utility.GetField<job_roles>("job_role")).ToList();
         ddlJobRole.DataSource = jobRoles;
         ddlJobRole.DataTextField = "job_role";
         ddlJobRole.DataValueField = "id";
         ddlJobRole.DataBind();

         employeeTypes = ce.emp_types.OrderBy(Utility.GetField<emp_types>("emp_type")).ToList();
         ddlEmployeeType.DataSource = employeeTypes;
         ddlEmployeeType.DataTextField = "emp_type";
         ddlEmployeeType.DataValueField = "id";
         ddlEmployeeType.DataBind();

         managers = ce.users
                       .Where(u => u.cptt_roles.cptt_role.Contains("Manager"))
                       .ToList();

         foreach (user manager in managers)
         {
            liManager = new ListItem();
            liManager.Text = manager.last_name + ", " + manager.first_name;
            liManager.Value = manager.id.ToString();
            ddlManager.Items.Add(liManager);
         }
      }

      protected void btnCreateUser_Click(object sender, EventArgs e)
      {
         if (Page.IsValid)
         {
            CPTTEntities ce = new CPTTEntities();
            MembershipCreateStatus createStatus; 
            MembershipUser newAspUser; 
            List<string> userRoles = new List<string>();
            string tmoId;

            newAspUser = Membership.CreateUser(txtUserName.Text, txtPassword.Text, 
                                               txtEmail.Text, null, null, true, 
                                               out createStatus);
            
            switch (createStatus) 
            { 
               case MembershipCreateStatus.Success:

                  foreach (ListItem roleItem in cblRole.Items)
                  {
                    if (roleItem.Selected)
                    {
                       userRoles.Add(roleItem.Text);
                    }
                  }

                  Roles.AddUserToRoles(txtUserName.Text, userRoles.ToArray());

                  // Only TMO User Ids are stored in the tmo_userid field. E-mail 
                  // addresses are ignored.

                  if (!txtUserName.Text.Contains("@"))
                  {
                     tmoId = txtUserName.Text.Trim();
                  }
                  else
                  {
                     tmoId = "";
                  }

                  BLL.User.Add(tmoId, txtEmail.Text.Trim(), txtFirstName.Text.Trim(),
                               txtLastName.Text.Trim(), chkActive.Checked,
                               Convert.ToInt32(ddlJobRole.SelectedValue),
                               Convert.ToInt32(ddlEmployeeType.SelectedValue),
                               Convert.ToInt32(ddlManager.SelectedValue),
                               Guid.Parse(newAspUser.ProviderUserKey.ToString()));

                  BindUsers();
                  ResetForm();

                  Master.StatusBox
                        .showStatusMessage("The user account was successfully created!"); 
                  break; 
               
               case MembershipCreateStatus.DuplicateUserName:                
                  Master.StatusBox
                        .showErrorMessage("There already exists a user with this username."); 
                  break; 
               
               case MembershipCreateStatus.DuplicateEmail:
                  Master.StatusBox
                        .showErrorMessage("There already exists a user with this email address."); 
                  break; 
               
               case MembershipCreateStatus.InvalidEmail:
                  Master.StatusBox
                        .showErrorMessage("There email address you provided in invalid.");
                  break; 
                              
               case MembershipCreateStatus.InvalidPassword:
                  Master.StatusBox
                        .showErrorMessage("The password you provided is invalid. It must be " 
                                          + Membership.MinRequiredPasswordLength.ToString() + 
                                          " characters long and have at least one non-alphanumeric character.");
                  break; 
               
               default:
                  Master.StatusBox
                        .showErrorMessage("There was an unknown error; the user account was NOT created."); 
                  break; 
            }
         }
      }

      protected void gvUsers_SelectedIndexChanged(object sender, EventArgs e)
      {
         int id = Convert.ToInt32(gvUsers.SelectedDataKey["id"].ToString());
         lblUserDetails.Text = "Edit User Details";
         btnCreateUser.Visible = false;
         btnCancelUpdate.Visible = true;
         btnUpdateUser.Visible = true;
         pnlPasswordDetails.Visible = false;
         txtUserName.Enabled = false;

         BindUsers();
         BindUserDetails(id);
      }

      protected void btnUpdateUser_Click(object sender, EventArgs e)
      {
         if (Page.IsValid)
         {
            string userName;
            user currUser;
            MembershipUser currAspUser;
            List<string> userRoles = new List<string>();
            string[] currRoles;
            currUser = (user)Session["currentUser"];

            if (!string.IsNullOrEmpty(currUser.tmo_userid))
            {
               userName = currUser.tmo_userid;
            }
            else
            {
               userName = currUser.email;
            }

            currRoles = Roles.GetRolesForUser(userName);

            foreach (ListItem roleItem in cblRole.Items)
            {
               if (roleItem.Selected)
               {
                  userRoles.Add(roleItem.Text);
               }
            }            

            try
            {
               currAspUser = Membership.GetUser(userName);
               currAspUser.Email = txtEmail.Text.Trim();
               
               Membership.UpdateUser(currAspUser);

               if (currRoles.Length > 0)
               {
                  Roles.RemoveUserFromRoles(userName, currRoles);
               }
               
               Roles.AddUserToRoles(userName, userRoles.ToArray());

               BLL.User.Update(currUser, currUser.tmo_userid, txtEmail.Text.Trim(), 
                               txtFirstName.Text.Trim(), txtLastName.Text.Trim(), 
                               chkActive.Checked, Convert.ToInt32(ddlJobRole.SelectedValue),
                               Convert.ToInt32(ddlEmployeeType.SelectedValue), 
                               Convert.ToInt32(ddlManager.SelectedValue));
               
               btnUpdateUser.Visible = false;
               btnCancelUpdate.Visible = false;
               btnCreateUser.Visible = true;
               pnlPasswordDetails.Visible = true;

               Session["currentUser"] = null;
               gvUsers.SelectedIndex = -1;

               BindUsers();
               ResetForm();
               Master.StatusBox.showStatusMessage("User Updated.");
            }
            catch (OptimisticConcurrencyException)
            {
               string message = "<p>This record has been modified since you last loaded it.</p>" +
                                "<p>There may be new changes to the record.</p>" +
                                "<p>Please reload</a> the record and try again.</p>";
               Master.StatusBox.showErrorMessage(message);
            }                        
         }
      }

      protected void btnCancelUpdate_Click(object sender, EventArgs e)
      {
         btnUpdateUser.Visible = false;
         btnCancelUpdate.Visible = false;
         btnCreateUser.Visible = true;
         pnlPasswordDetails.Visible = true;
         
         gvUsers.SelectedIndex = -1;
         BindUsers();
         ResetForm();
         Master.StatusBox.showStatusMessage("Update cancelled.");
      }

      protected void BindUserDetails(int id)
      {
         CPTTEntities ce = new CPTTEntities();
         user currUser;
         string[] userRoles;
         string userName;

         currUser = ce.users.Where(u => u.id.Equals(id)).First();         
         ce.Detach(currUser);
         Session["currentUser"] = currUser;

         if (!String.IsNullOrEmpty(currUser.tmo_userid))
         {            
            userName = currUser.tmo_userid;

         }
         else
         {
            userName = currUser.email;
         }

         userRoles = Roles.GetRolesForUser(userName);

         txtUserName.Text = userName;
         txtEmail.Text = currUser.email;
         txtFirstName.Text = currUser.first_name;
         txtLastName.Text = currUser.last_name;

         if (currUser.active_emp_yn.Value == true)
         {
            chkActive.Checked = true;
         }
         else
         {
            chkActive.Checked = false;
         }

         ddlEmployeeType.ClearSelection();
         ddlEmployeeType.Items.FindByValue(currUser.emp_type_id.Value.ToString()).Selected = true;

         ddlJobRole.ClearSelection();
         if (currUser.job_role_id.HasValue)
         {
            ddlJobRole.Items.FindByValue(currUser.job_role_id.Value.ToString()).Selected = true;
         }
         
         ddlManager.ClearSelection();
         if (currUser.dir_mgr_user_uid.HasValue)
         {
            ddlManager.Items.FindByValue(currUser.dir_mgr_user_uid.Value.ToString()).Selected = true;
         }
         
         cblRole.ClearSelection();
         foreach(string currRole in userRoles)
         {
            cblRole.Items.FindByText(currRole).Selected = true;
         }
      }

      protected void ResetForm()
      {
         lblUserDetails.Text = "Add User";
         txtUserName.Text = "";
         txtUserName.Enabled = true;
         txtEmail.Text = "";
         txtFirstName.Text = "";
         txtLastName.Text = "";
         chkActive.Checked = true;
         ddlEmployeeType.ClearSelection();
         ddlEmployeeType.SelectedIndex = -1;
         ddlJobRole.ClearSelection();
         ddlJobRole.SelectedIndex = -1;
         ddlManager.ClearSelection();
         ddlManager.SelectedIndex = -1;
         cblRole.ClearSelection();
      }

      protected void gvUsers_RowDeleting(object sender, GridViewDeleteEventArgs e)
      {
         int id = Convert.ToInt32(gvUsers.DataKeys[e.RowIndex]["id"].ToString());        
         string userName;

         if(gvUsers.DataKeys[e.RowIndex]["tmo_userid"] != null)
         {
            userName = gvUsers.DataKeys[e.RowIndex]["tmo_userid"].ToString();
         }
         else
         {
            userName = gvUsers.DataKeys[e.RowIndex]["email"].ToString();
         }         

         try
         {
            BLL.User.Delete(id);
            Membership.DeleteUser(userName);            
            BindUsers();

            Master.StatusBox.showStatusMessage("<p>User Deleted.</p>");
         }
         catch (UpdateException)
         {
            Master.StatusBox.showErrorMessage
                             ("<p>There are user fields that reference " +
                              "this user.</p><p>Please remove these fields " +
                              "before removing the user.</p>");
         }
      }

      protected void gvUsers_Sorting(object sender, GridViewSortEventArgs e)
      {
         string sortCol = (string)Session["UserSortCol"];
         string sortDir = (string)Session["UserSortDir"];

         if (gvUsers.SelectedIndex > -1)
         {
            Master.StatusBox.showErrorMessage("Cannot change sorting while a record is being edited.");
            e.Cancel = true;
         }
         else
         {
            if (sortCol != e.SortExpression)
            {
               Session["UserSortDir"] = "ASC";
            }
            else
            {
               if (sortDir == "ASC")
               {
                  Session["UserSortDir"] = "DESC";
               }
               else
               {
                  Session["UserSortDir"] = "ASC";
               }
            }

            Session["UserSortCol"] = e.SortExpression;
            BindUsers();
         }
      }

      protected void gvUsers_PageIndexChanging(object sender, GridViewPageEventArgs e)
      {
         if (gvUsers.SelectedIndex > -1)
         {
            Master.StatusBox.showErrorMessage("Cannot change pages while a record is being edited.");
            e.Cancel = true;
         }
         else
         {
            gvUsers.PageIndex = e.NewPageIndex;
            BindUsers();
         }
      }
   }
}