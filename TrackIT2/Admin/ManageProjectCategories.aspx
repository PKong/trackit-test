﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/TrackIT2.Master" AutoEventWireup="true" CodeBehind="ManageProjectCategories.aspx.cs" Inherits="TrackIT2.Admin.ManageProjectCategories" %>
<%@ MasterType TypeName="TrackIT2.Templates.TrackIT2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   <h2 style="text-align: center;">Manage Project Categories</h2>

   <p>&nbsp;</p>
   
   <h3>Groups</h3>   

   <asp:GridView ID="gvCategories" runat="server"
                 AllowPaging="true"
                 AutoGenerateColumns="False" 
                 DataKeyNames="id"
                 ShowFooter="true"
                 onrowdatabound="gvCategories_RowDataBound" 
                 onrowdeleting="gvCategories_RowDeleting" 
                 onselectedindexchanged="gvCategories_SelectedIndexChanged" 
                 onrowediting="gvCategories_RowEditing" 
                 onrowcancelingedit="gvCategories_RowCancelingEdit" 
                 onrowupdating="gvCategories_RowUpdating" 
                 onpageindexchanging="gvCategories_PageIndexChanging">

      <PagerSettings Mode="NextPreviousFirstLast" NextPageText="Next" PreviousPageText="Prev" />
      <AlternatingRowStyle CssClass="GridViewAlternateRow" />      
      <SelectedRowStyle CssClass="GridViewSelectedRow" />         
      <EditRowStyle CssClass="GridViewEditRow" />
      <PagerStyle HorizontalAlign="Right" />

      <Columns>
         <asp:TemplateField HeaderText="Options">
            <FooterStyle HorizontalAlign="Center" />
            <ItemTemplate>
               <asp:LinkButton ID="lnbSelect" runat="server" CommandName="Select" Text="Fields"></asp:LinkButton>
               &nbsp;
               <asp:LinkButton ID="lnbEdit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
               &nbsp;
               <asp:LinkButton ID="lnbDelete" runat="server" CommandName="Delete" Text="Delete" OnClientClick="return confirm('Are you sure you want to delete this project category?');"></asp:LinkButton>               
            </ItemTemplate>
                  
            <EditItemTemplate>
               <asp:LinkButton ID="lnbCancel" runat="server" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
               &nbsp;&nbsp;
               <asp:LinkButton ID="lnbUpdate" runat="server" CommandName="Update" Text="Update"></asp:LinkButton>
            </EditItemTemplate>
            
            <FooterTemplate>               
               <asp:LinkButton ID="lnbAddCategory" runat="server" Text="Add" onclick="lnbAddCategory_Click"></asp:LinkButton>
            </FooterTemplate>            
         </asp:TemplateField>
         
         <asp:TemplateField HeaderText="Id"></asp:TemplateField>

         <asp:TemplateField HeaderText="Name">
            <EditItemTemplate>
               <asp:TextBox ID="txtEditCategoryName" runat="server" MaxLength="50"></asp:TextBox>
               <asp:RequiredFieldValidator ID="valEditCategoryNameRequired" runat="server"                              
                                           ControlToValidate="txtEditCategoryName"
                                           CssClass="errorMessage"
                                           Display="Dynamic"
                                           EnableClientScript="true"
                                           ErrorMessage="Please specify a name.">                                                 
               </asp:RequiredFieldValidator>
            </EditItemTemplate>

            <FooterTemplate>               
               <asp:TextBox ID="txtAddCategoryName" runat="server" MaxLength="50"></asp:TextBox>
            </FooterTemplate>
         </asp:TemplateField>

         <asp:TemplateField HeaderText="Description">
            <EditItemTemplate>
               <asp:TextBox ID="txtEditCategoryDescription" runat="server" MaxLength="100"></asp:TextBox>
            </EditItemTemplate>

            <FooterTemplate>
               <asp:TextBox ID="txtAddCategoryDescription" runat="server" MaxLength="100"></asp:TextBox>
            </FooterTemplate>
         </asp:TemplateField>
      </Columns>
   </asp:GridView>

   <h3>Fields</h3>

   <asp:Panel ID="pnlFieldInfo" runat="server" Visible="false">      
      <asp:GridView ID="gvFields" runat="server"
                    AutoGenerateColumns="False"
                    ShowFooter="True" 
                    DataKeyNames="id"
                    onrowcancelingedit="gvFields_RowCancelingEdit" 
                    onrowdatabound="gvFields_RowDataBound" 
                    onrowdeleting="gvFields_RowDeleting"                           
                    onrowediting="gvFields_RowEditing"
                    onrowupdating="gvFields_RowUpdating">

         <AlternatingRowStyle CssClass="GridViewAlternateRow" />      
         <SelectedRowStyle CssClass="GridViewSelectedRow" />         
         <EditRowStyle CssClass="GridViewEditRow" />

         <EmptyDataTemplate>
            <p class="statusMessage">
               No fields for the category specified.
            </p>

            <table id="MainContent_gvFields" cellspacing="0" border="1" style="border-collapse:collapse;" rules="all">
               <tbody>
                  <tr>
                     <th scope="col">&nbsp;</th>
                     <th scope="col">Table</th>
                     <th scope="col">Database Field</th>
                     <th scope="col">Friendly Name</th>
                  </tr>

                  <tr>
                     <td>
                        <asp:LinkButton ID="lnbAddField" runat="server" Text="Add" onclick="lnbAddField_Click" />
                     </td>

                     <td>
                        <asp:TextBox ID="txtAddFieldTable" runat="server" MaxLength="50"></asp:TextBox>
                     </td>

                     <td>
                        <asp:TextBox ID="txtAddFieldColumn" runat="server" MaxLength="50"></asp:TextBox>
                     </td>

                     <td>
                        <asp:TextBox ID="txtAddFieldFriendlyName" runat="server" MaxLength="100"></asp:TextBox>
                     </td>
                  </tr>
               </tbody>
            </table>
         </EmptyDataTemplate>

         <Columns>
            <asp:TemplateField HeaderText="Options">
               <FooterStyle HorizontalAlign="Center" />
               <ItemTemplate>
                  <asp:LinkButton ID="lnbEdit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
                  &nbsp;&nbsp;
                  <asp:LinkButton ID="lnbDelete" runat="server" CommandName="Delete" Text="Delete" OnClientClick="return confirm('Are you sure you want to delete this field?');"></asp:LinkButton>               
               </ItemTemplate>
                  
               <EditItemTemplate>
                  <asp:LinkButton ID="lnbCancel" runat="server" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                  &nbsp;&nbsp;
                  <asp:LinkButton ID="lnbUpdate" runat="server" CommandName="Update" Text="Update"></asp:LinkButton>
               </EditItemTemplate>
               
               <FooterTemplate>
                  <asp:LinkButton ID="lnbAddFieldFooter" runat="server" Text="Add" onclick="lnbAddFieldFooter_Click" />
               </FooterTemplate>          
            </asp:TemplateField>
         
            <asp:TemplateField HeaderText="Id"></asp:TemplateField>
               
            <asp:TemplateField HeaderText="Table">
               <EditItemTemplate>
                  <asp:TextBox ID="txtEditTable" runat="server" MaxLength="50"></asp:TextBox>
                  <asp:RequiredFieldValidator ID="valEditTableRequired" runat="server"
                                                ControlToValidate="txtEditTable"
                                                CssClass="errorMessage"
                                                Display="Dynamic"
                                                EnableClientScript="true"
                                                ErrorMessage="Please specify a table name.">                                                 
                  </asp:RequiredFieldValidator>
               </EditItemTemplate>

               <FooterTemplate>
                  <asp:TextBox ID="txtAddFieldTable" runat="server" MaxLength="50"></asp:TextBox>
               </FooterTemplate>
            </asp:TemplateField>
               
            <asp:TemplateField HeaderText="Database Field">
               <EditItemTemplate>
                  <asp:TextBox ID="txtEditColumn" runat="server" MaxLength="50"></asp:TextBox>
                  <asp:RequiredFieldValidator ID="valEditColumnRequired" runat="server"
                                                ControlToValidate="txtEditColumn"
                                                CssClass="errorMessage"
                                                Display="Dynamic"
                                                EnableClientScript="true"
                                                ErrorMessage="Please specify a database field.">                                                 
                  </asp:RequiredFieldValidator>
               </EditItemTemplate>

               <FooterTemplate>
                  <asp:TextBox ID="txtAddFieldColumn" runat="server" MaxLength="50"></asp:TextBox>
               </FooterTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Friendly Name">
               <EditItemTemplate>
                  <asp:TextBox ID="txtEditFriendlyName" runat="server" MaxLength="100"></asp:TextBox>                
               </EditItemTemplate>

               <FooterTemplate>
                  <asp:TextBox ID="txtAddFieldFriendlyName" runat="server" MaxLength="100"></asp:TextBox>
               </FooterTemplate>
            </asp:TemplateField>
         </Columns>
      </asp:GridView>
   </asp:Panel>
</asp:Content>