﻿using System;
using System.Web;

namespace TrackIT2
{
    /// <summary>
    /// Error
    /// </summary>
    public partial class Error : System.Web.UI.Page
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Handle Error Type
            if (Request.QueryString["type"] == "404")
            {
                div404.Visible = true;
                divgeneric.Visible = false;
            }
        }        
    }
}