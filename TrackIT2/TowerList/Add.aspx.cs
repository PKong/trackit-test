﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.BLL;
using TrackIT2.DAL;
using System.Text;
using System.Collections;

namespace TrackIT2.TowerList
{
    public partial class Add : System.Web.UI.Page
    {

        private Hashtable m_htVarCharInvalidField;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindLookupLists();
            }
        }

        protected void btnAdd_Click(object s, EventArgs e)
        {
            m_htVarCharInvalidField = new Hashtable();

            string status = null;
            Boolean blnIsValid = true;
            Boolean blnUIDIsValid = true;

            if (Page.IsValid)
            {
                tower_list newObj = new tower_list();
                string zip = string.Empty;
                string city = string.Empty;
                
                //Site UID
                newObj.site_uid = Utility.PrepareString(txtSiteID.Text.ToUpper());
                if (newObj.site_uid == null)
                {
                    blnIsValid = false;
                }
                else
                {
                    if (BLL.TowerListStatus.GetTowerList(newObj.site_uid) != null)
                    {
                        blnIsValid = false;
                        blnUIDIsValid = false;
                        Utility.SettingErrorColorForTextbox(txtSiteID);
                        status = "Site ID " + newObj.site_uid + " already exists!";
                    }
                    else
                    {
                        Utility.ClearErrorColorForTextbox(txtSiteID);

                        CPTTEntities ce = new CPTTEntities();

                        //Site Name
                        blnIsValid = blnIsValid & Utility.ValidateVarchar(Utility.PrepareString(this.txtSiteName.Text), newObj, m_htVarCharInvalidField, "site_name", "Site Name");
                        if (blnIsValid) newObj.site_name = Utility.PrepareString(this.txtSiteName.Text);

                        //Region
                        newObj.region_name = Utility.PrepareString(this.ddlRegionName.SelectedValue);

                        //Market Name
                        newObj.market_name = Utility.PrepareString(this.ddlMarketName.SelectedValue);

                        //Market Code
                        string tmp_market_code = (from x in ce.sites
                                                  where x.market_name == newObj.market_name
                                                 select x.market_code).FirstOrDefault();
                        if (string.IsNullOrEmpty(tmp_market_code))
                        {
                            tmp_market_code = (from x in ce.tower_list
                                               where x.market_name == newObj.market_name
                                               select x.market_code).FirstOrDefault();
                        }
                        newObj.market_code = Utility.PrepareString(tmp_market_code);

                        //Class Details
                        newObj.site_class_desc = Utility.PrepareString(ddlSiteClassDesc.SelectedValue);
                    
                        //Height
                        blnIsValid = blnIsValid & Utility.ValidateVarchar(Utility.PrepareString(this.txtHeight.Text), newObj, m_htVarCharInvalidField, "structure_ht", "Height");
                        if (blnIsValid) newObj.structure_ht = Utility.PrepareDouble(this.txtHeight.Text);

                        //Status Details
                        newObj.site_status_desc = Utility.PrepareString(ddlSiteStatusDesc.SelectedValue);

                        //county
                        newObj.county = Utility.PrepareString(this.txtCounty.Text);

                        //state
                        string tmp_state = this.ddlState.SelectedValue;
                        if (tmp_state.Contains('-'))
                        {
                            tmp_state = tmp_state.Split('-').FirstOrDefault();
                        }
                        newObj.state = Utility.PrepareString(tmp_state);

                        //site_latitude
                        newObj.site_latitude = Utility.PrepareDecimal(this.txtLatitude.Text);

                        //site_longitude
                        newObj.site_longitude = Utility.PrepareDecimal(this.txtLongitude.Text);

                        //address
                        blnIsValid = blnIsValid & Utility.ValidateVarchar(Utility.PrepareString(this.txtAddress.Text), newObj, m_htVarCharInvalidField, "address", "Address");
                        if (blnIsValid) newObj.address = Utility.PrepareString(this.txtAddress.Text);

                        //city
                        newObj.city = Utility.PrepareString(this.txtCity.Text);

                        ////zip
                        newObj.zip = Utility.PrepareString(this.txtZip.Text);

                    }
                }

                if (string.IsNullOrEmpty(status) && blnIsValid)
                {
                    //Add
                    if (blnIsValid)
                    {
                        // set Default Off for new create.
                        newObj.trackit_default_list_status_id = 2;
                        newObj.map_default_list_status_id = 2;
                        status = BLL.TowerListStatus.Add(newObj);

                        // A null status message indicates success.
                        if (string.IsNullOrEmpty(status))
                        {
                            // Redirect to edit page with created id. Keep save button disabled
                            // to prevent double clicks.
                            ClientScript.RegisterStartupScript(GetType(), "Load",
                            String.Format("<script type='text/javascript'>" +
                                          "   $(\"#MainContent_btnSubmitCreateTowerList\").val(\"Processing...\");" +
                                          "   $(\"#MainContent_btnSubmitCreateTowerList\").attr(\"disabled\", true);" +
                                          "   window.parent.location.href = 'Default.aspx?forceSearch=true';" + 
                                          " </script>"
                                          ));
                            Master.StatusBox.showStatusMessage("Add complete.");
                        }
                        else
                        {
                            Master.StatusBox.showErrorMessage(status);
                        }
                    }
                    else
                    {
                        StringBuilder sErrorMsg = new StringBuilder(Globals.ERROR_HEADER);
                        if (!blnUIDIsValid)
                        {
                            //Invalid UID
                            sErrorMsg.AppendLine(Globals.ERROR_TXT_INVALID_UID + "<br//>");
                        }
                        foreach (string item in m_htVarCharInvalidField.Keys)
                        {
                            if (m_htVarCharInvalidField[item] != null)
                            {
                                sErrorMsg.AppendLine(String.Format(Globals.ERROR_INVALID_LENGTH_FORMAT, item, m_htVarCharInvalidField[item]));
                            }
                        }
                        Master.StatusBox.showStatusMessage(sErrorMsg.ToString());
                    }
                }
                else
                {
                    Master.StatusBox.showStatusMessage(status);
                }

            }
        }

        protected void BindLookupLists()
        {
            CPTTEntities ce = new CPTTEntities();
            //Region
            var regions = ce.sites.Select(x => new { region_name = x.region_name }).Distinct().ToList();
            regions.Sort((x, y) => new AlphanumComparatorFast().Compare(x.region_name, y.region_name));
            Utility.BindDDLDataSource(this.ddlRegionName, regions, "region_name", "region_name", "- Select -");

            //Market
            var marketNames = ce.sites.Select(x => new { market_name = x.market_name }).Distinct().ToList();
            marketNames.Sort((x, y) => new AlphanumComparatorFast().Compare(x.market_name, y.market_name));
            Utility.BindDDLDataSource(this.ddlMarketName, marketNames, "market_name", "market_name", "- Select -");

            // Site Class Desc
            var siteClassDescs = ce.sites.Select(x => new { site_class_desc = x.site_class_desc }).Distinct().Where(x => x.site_class_desc != null && x.site_class_desc != "<undefined>").ToList();
            siteClassDescs.Sort((x, y) => new AlphanumComparatorFast().Compare(x.site_class_desc, y.site_class_desc));
            Utility.BindDDLDataSource(this.ddlSiteClassDesc, siteClassDescs, "site_class_desc", "site_class_desc", "- Select -");

            // Site Status Desc
            var siteStatusDescs = ce.sites.Select(x => new { site_status_desc = x.site_status_desc }).Distinct().Where(x => x.site_status_desc != null && x.site_status_desc != "<undefined>").ToList();
            siteStatusDescs.Sort((x, y) => new AlphanumComparatorFast().Compare(x.site_status_desc, y.site_status_desc));
            Utility.BindDDLDataSource(this.ddlSiteStatusDesc, siteStatusDescs, "site_status_desc", "site_status_desc", "- Select -");

            //State
            var sort_state = ce.state_provinces.Distinct().Where("it.country_id == 1").ToList();
            var tmp_state_provinces = new DynamicComparer<state_provinces>();
            tmp_state_provinces.SortOrder(x => x.state_province);
            sort_state.Sort(tmp_state_provinces);
            Utility.BindDDLDataSource(this.ddlState, sort_state, "state_province", "state_province", "- Select -");
        }
    }
}
