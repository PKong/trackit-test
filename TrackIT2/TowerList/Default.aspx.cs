﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.BLL;
using System.Collections.Specialized;
using System.Web.Script.Serialization;
using TrackIT2.Handlers;
using System.ComponentModel;
using System.IO;

namespace TrackIT2.TowerList
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindLookupLists();
                this.SetupFlexiGrid();
            }
        }

        /// <summary>
        /// Bind Lookup Lists
        /// </summary>
        protected void BindLookupLists()
        {
           
        }
        //-->

        /// <summary>
        /// Setup Flexi Grid
        /// </summary>
        protected void SetupFlexiGrid()
        {
            fgTowerList.SortColumn = fgTowerList.Columns.Find(col => col.Code == "site_uid");
            fgTowerList.SortDirection = FlexigridASPNET.GridSortOrder.Asc;
            fgTowerList.RowPerPageOptions = new int[] { 10, 20, 30, 40, 50 };
            fgTowerList.NoItemMessage = "No Data";
        }
        //-->

        public void ExportClick(object s, EventArgs e)
        {

            ExportFile ef = new ExportFile();
            var searchData = hiddenSearchValue.Value;
            var data = new NameValueCollection();
            if (!string.IsNullOrEmpty(searchData))
            {
                var jss = new JavaScriptSerializer();
                var table = jss.Deserialize<dynamic>(searchData);

                for (int x = 0; x < table.Length; x++)
                {
                    if (table[x]["value"].GetType() == typeof(string))
                    {
                        data.Add(table[x]["name"], table[x]["value"]);
                    }
                    else
                    {
                        data.Add(table[x]["name"], table[x]["value"].ToString());
                    }

                }
            }

            const int startIndex = 4;
            var parameters = new NameValueCollection();

            // If we have form values, extract them to build the object query.
            if (data.Count > startIndex)
            {
                for (var i = startIndex; i < data.Count; i++)
                {
                    var itemKey = data.GetKey(i);
                    var itemValue = data.Get(i);

                    parameters.Add(itemKey, itemValue);
                }
            }

            NameValueCollection columnValue = ExportFile.GetColumn(hiddenColumnValue.Value, ExportFile.eExportType.eTowerlist);
            ExportFileHandler extHand;
            extHand = new ExportFileHandler(ExportFile.eExportType.eTowerlist, data, columnValue, parameters);
            extHand.Load_Data_OnComplete += new ExportFileHandler.Load_Data_OnComplete_Handler(Load_Data_OnComplete);
        }

        protected void Load_Data_OnComplete(List<ExportClass> loadDataResult, NameValueCollection data, NameValueCollection column, RunWorkerCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                ExportFile ef = new ExportFile();
                MemoryStream mem = ef.WriteReport(loadDataResult, column, ExportFile.eExportType.eTowerlist, Server.MapPath("/"));

                if (mem != null)
                {
                    string fileName = ExportFile.GenarateFileName(data["fileName"]);
                    DownloadFile(mem, fileName);
                }
                else
                {
                    Master.StatusBox.showStatusMessage("Export report error.");
                }
            }
            else
            {
                throw e.Error;
            }
        }

        private void DownloadFile(MemoryStream mstream, string reportName)
        {
            try
            {
                byte[] byteArray = mstream.ToArray();
                Response.Clear();

                if (Response.Cookies.Count > 0)
                {
                    bool flag = true;
                    foreach (string c in Response.Cookies)
                    {
                        if (c == "fileDownloadToken")
                        {
                            Response.Cookies["fileDownloadToken"].Value = hiddenDownloadFlag.Value;
                            flag = false;
                        }
                    }
                    if (flag)
                    {
                        Response.AppendCookie(new HttpCookie("fileDownloadToken", hiddenDownloadFlag.Value));
                    }
                }
                else
                {
                    Response.AppendCookie(new HttpCookie("fileDownloadToken", hiddenDownloadFlag.Value));
                }
                Response.AddHeader("Content-Disposition", "attachment; filename=" + reportName + ".xls");
                Response.AddHeader("Content-Length", byteArray.Length.ToString());
                Response.ContentType = "application/vnd.msexcel";
                Response.BinaryWrite(byteArray);
                Response.Flush();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                Master.StatusBox.showStatusMessage("Export report error.");
            }
        }
    }
}