﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/TrackIT2.Master" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="TrackIT2.LeaseAdminTracker.Edit" %>
<%@ MasterType TypeName="TrackIT2.Templates.TrackIT2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   <!-- Styles -->
   <%: Styles.Render("~/Styles/lease_admin_edit") %>   
    
   <!-- Scripts -->
   <%: Scripts.Render("~/Scripts/lease_admin_edit") %>
   <style type="text/css"  >
        table th 
        {
            border: 0;
            padding: 0;
        }
   </style>

    <script type="text/javascript" language="javascript">
        // Doc Ready
        $(document).ready(function () {

            // Site ID
            var id = $.urlParam('id');
            if (!String(id).isNullOrEmpty()) {
                addFilterPostParam("ID", id);
            }

            setModalSize(700, 670, 450);
            setupListLeaseAdminCreateModalWithId("Edit Lease Admin", id);
            //Setup Document link Dialog
            setupDocumentLinkDialog("Document Links");

            // Setup currentcy textbox
            $("#MainContent_txtFinanace_ForecastAmount").formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 2 });
            $("#MainContent_txtFinanace_CostShare").formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 2 });
            $("#MainContent_txtFinanace_Capcost").formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 2 });

            // Setup Upload Document Modal
            setupDmsModal("Upload Document");
        });
        //-->

        function disableButton(button) {
            button.disabled = true;
        }

        function onPostComment(obj) {
            obj.value = 'Processing...';
            showLoadingSpinner('h2-comments');
            setTimeOut(disableButton(obj), 300);
            return true;
        }
        
    </script> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Panel ID="pnlNoID" runat="server" CssClass="align-center">
      <h1>Not Found (404)</h1>

      <p>The Lease Admin Tracker specified is invalid or no longer exists.</p>

      <p>&nbsp;</p>

      <p>
         <a href="Default.aspx">&laquo; Go back to list page.</a>
      </p>

      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
    </asp:Panel>
    
    <asp:HiddenField ID="hidden_lease_admin_id" runat="server" Value="" />
    <asp:HiddenField ID="hidden_leaseapp_id" runat="server" Value="" />
    <asp:HiddenField ID="hidden_lease_comments" runat="server" Value="" />
    <asp:HiddenField ID="hidDocumentDownload" runat="server" />
    <asp:Panel ID="pnlSectionContent" runat="server" >        
       <div class="section content" >
            <div class="wrapper">
               <div class="action-tab">
                    <div class="left-side">
                        <asp:HyperLink ID="backToList" runat="server" Text="« Return to List" NavigateUrl="~/LeaseAdminTracker/" CssClass="site-link-no-underline"></asp:HyperLink>
                    </div>
                    <div class="right-side">
                        <asp:LinkButton ID="btnLeaseAdminDelete"  CssClass="link-button site-link-no-underline" runat="server" ClientIDMode="Static" OnClientClick="return openDeleteAppButtonPopup('Lease Admin Tracker',event);" onclick="btnLeaseAdminDelete_Click">Delete</asp:LinkButton>
                    </div>
               </div>

               <div class="application-information">

                    <h1 class="h1" id="spn_total_site" runat="server">
                        Lease Admin Tracker <span>
                            <asp:HyperLink ID="lblSiteID" runat="server" Text="" CssClass="h1-span-site-link site-link-no-underline"></asp:HyperLink>
                            <asp:Label ID="lblSiteName" runat="server" Text=""></asp:Label>
                            <asp:Label ID="lblCustomerName" runat="server"></asp:Label>
                        </span>
                    </h1>                 
                   <div id="tabs" class="toggle-me">
                       <a id="modalBtnExternal" class="edit-button"  href="javascript:void(0)">edit</a>
                       
                   
						<div id="tab_site_data" runat="server" >
							<ul class="tabs" >
								<li><a href="#general-information">Related Leases and Tower</a></li>
							</ul>
						</div>
                        
                        <div id="div_site"  runat="server" >
                            <div id="general-information">
							   <div style="padding: 20px;HEIGHT:240PX;">
									<div class="i-1-3">
										<div class="general-information-column" style="min-height:121px;height:auto !important; height:121px;">
											<div style="float: left;">
												<asp:GridView ID="grdMain" CssClass="related-apps TableGenInfo" runat="server" AutoGenerateColumns="False">
													   <Columns>
														   <asp:BoundField HeaderText="Site ID" DataField="SiteID" />
														   <asp:BoundField HeaderText="Customer" DataField="CustomerName" />
														   <asp:BoundField HeaderText="App Received" DataField="LAReceived" DataFormatString="{0:MM/dd/yyyy}"
															   HtmlEncode="false" />
														   <asp:HyperLinkField DataNavigateUrlFields="Link" Text="Go" />
													   </Columns>
												</asp:GridView>
												<div id="divNoRelatedLeaseApps" class="related-apps TableGenInfo" style="margin-right: 20px;width: 420px;
													   background: #f0f0f0;" runat="server">
													   <div style="margin: 0 auto 0 auto; width: 200px; padding: 30px 0 0 0; text-align: center;
														   color: #888;">
														   <em>
															   <asp:Label ID="lblNoLeaseAppStatus" runat="server">No Associated Lease Application</asp:Label>
														   </em>
													   </div>
												</div>
										   </div>
										   <ul class="information-list" style="width: 180px; margin-bottom: 10px;">
											   <li>Tower Address</li>
											   <li>
												   <asp:HyperLink ID="mybtn" Target="_blank" runat="server"></asp:HyperLink>
												   <asp:Label ID="lblAddressNA" runat="server" Visible="false"></asp:Label>                                       
											   </li>
                                    <li>&nbsp;</li>
											   <li>Latitude, Longitude:  
                                      <br/>
												  <asp:HyperLink ID="lblSiteLatLong" Text="" Target="_blank" runat="server">
													  <asp:Label ID="lblDegreeLatitude" runat="server" ></asp:Label>,&nbsp;<asp:Label ID="lblDegreeLongtitude" runat="server"></asp:Label><br/>
													  <asp:Label ID="lblSiteLatitude" runat="server" Text=""></asp:Label>,&nbsp;<asp:Label ID="lblSiteLongitude" runat="server" Text=""></asp:Label>
												  </asp:HyperLink>
											   </li>
										   </ul>
									   </div>
									   <div class="cb"></div>
									   <div class="general-information-column">
										   
                                            <ul class="information-list" style="margin-bottom: 10px;" >
                                                <li>
                                                    <asp:Label ID="lblCustomerLabel" runat="server" Text="" Font-Bold="true"  ></asp:Label>
                                                    <asp:Label ID="lblCustomer2" runat="server" Text=""  Font-Bold="false" ></asp:Label>
                                                </li>
                                            </ul>
										   <ul class="information-list" style="width: 180px; margin-bottom: 10px;margin-left:230px;">
											   <li>Structure Details</li>
											   <li>Type:
												   <asp:Label ID="lblType" runat="server" Text=""></asp:Label></li>
											   <li>Height:
												   <asp:Label ID="lblHeight" runat="server" Text=""></asp:Label></li>
											   <li>Rogue Equip:
												   <asp:Label ID="lblRogueEquip" runat="server" Text=""></asp:Label></li>
											   <li>Status:
												   <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
                                                   <asp:Image ID="imgStructureDetails_Construction" runat="server" AlternateText="Construction" CssClass="construction-img" Visible="false" /></li>
										   </ul>
										</div>
									   <div class="cb"></div>
								   </div>
								   <div class="i-1-3">
									   <div class="map">
										   <asp:Image ID="Image2" Width="310" Height="235" frameborder="0" scrolling="no" marginheight="0"
											   runat="server" ImageUrl="" AlternateText="" />
									   </div>
								   </div>
							   </div>
							</div>
                        </div>
                        <div id="div_site_list" runat="server"  class="site-list" style="overflow:visible; margin-bottom: 30px;margin-top:30px;">
                            <div style="height:25px;" >
                             <asp:Label ID="lblCustomer" runat="server" Text=""  style="margin-bottom:10px;" ></asp:Label>
                            </div>
                            <fx:Flexigrid ID="fgSites"
                                Width="990"
                                ResultsPerPage="10"
			                    ShowPager="true"
                                Resizable="false"
                                ShowToggleButton="false"
                                ShowTableToggleButton="false"
                                SearchEnabled="false"
                                UseCustomTheme="true"
                                CssClass="tmobile"
                                WrapCellText="true"
                                DoNotIncludeJQuery="true"
                                OnClientBeforeSendData="fgBeforeSendData"
                                OnClientDataLoad="fgDataLoad"
                                OnClientNoDataLoad="fgNoData"
			                    HandlerUrl="~/LeaseAdminSiteList.axd"
                                runat="server">
		                        <Columns>
			                        <fx:FlexiColumn Code="site_uid" Text="Site ID" Width="101" OnRowRender="fgSiteRowRender"  />
			                        <fx:FlexiColumn Code="site_name" Text="Site Name" Width="140" OnRowRender="fgSiteRowRender" />
                                    <fx:FlexiColumn Code="market_name" Text="Market Name" Width="110" OnRowRender="fgSiteRowRender"  />
                                    <fx:FlexiColumn Code="address" Text="Address" Width="300" OnRowRender="fgSiteRowRender"  />
                                    <fx:FlexiColumn Code="site_latitude" Text="Latitude Longitude" Width="200"  OnRowRender="fgSiteRowRender" />
                                    <fx:FlexiColumn Code="tower_modification_yn" Text="TM" Width="32" OnRowRender="fgSiteRowRender" />
                                    <fx:FlexiColumn Code="rogue_equipment_yn" Text="RE" Width="32" OnRowRender="fgSiteRowRender" />
                                    <fx:FlexiColumn Code="id" Text="Actions" Width="65" AllowSort="false"
                                                    Format="<a class='fancy-button' href='/Sites/SiteDashboard.aspx?id={0}'>Edit</a>" />
		                        </Columns>
	                        </fx:Flexigrid>
                            <!-- /Sites Data  Grid -->
                            <div class="cb"></div>
                        </div>
                        <div class="cb">
                        </div>                   
                        <div id="modalExternal" class="modal-container" style="overflow: hidden;">
                        <div id="modalDocument" class="modal-container" style="overflow:hidden;"></div>
                        </div>                            
                        <div class="cb">
                        </div>
                    </div>
                </div>
               <div class="cb">
               </div>
               <div class="application-process" style="float: left;width:100%;">
                   <h1>Lease Admin Process</h1>
                   <div id="tabs2">
                       <ul class="tabs">
                           <li><a href="#general">Execution & Commencement </a></li>
                       </ul>
                       <div id="general">
                           <div style="padding: 20px;">
                           <div id="error_general"></div>
                               <div class="full-col align-right" style="with:100%;">
                                   <asp:Button ID="btnSubmitLeaseAdmin_Top" runat="server" 
                                               Text="Save"
                                               CssClass="ui-button ui-widget ui-state-default ui-corner-all"
                                               OnClientClick="if (!CheckBeforeSaveLeaseAdmin()) {return false;}"
                                               OnClick="btnSubmitLeaseAdmin_Click" 
                                               UseSubmitBehavior="false"  />
                               </div>
                                <div ID="left_col_wrapper" runat="server" class="left-col" style="width:48%;">
                                    <div id="divExecutionPhase" style="width: 100%;height:40px;text-align:center;background:#F0F0F0;margin-top:10px;margin-bottom:20px;">
                                        <span id="lblExecutionPhase" style="width:100%;text-align:left;margin-left:5px;font-weight:bold;margin-top:10px;" >Execution Phase</span>
                                    </div>
                                        <!--Document Link-->
                                        <div id="divExecutablesSenttoCustomer" runat="server" ClientIDMode="Static" >
                                            <div class="document-link" id="LA_ExecutablesSenttoCustomer" style="width:345px;" >
                                                <asp:Label ID="lblExecutablesSenttoCustomer" runat="server">Executables Sent to Customer</asp:Label>
                                                <asp:TextBox ID="txtExecDocs_ExecSendtoCust" CssClass="datepicker"  runat="server"></asp:TextBox>
                                                <asp:DropDownList ID="ddlExecDocs_SenttoCustStatus" runat="server" CssClass="split">
                                                    <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:HiddenField  ID="hidden_document_LA_ExecutablesSenttoCustomer" ClientIDMode="Static" runat="server" />
                                            </div>
                                        </div>
                                        <br />
                                       <label>
                                           Document Type</label>
                                       <asp:DropDownList ID="ddlExecDocs_DocType" runat="server">
                                            <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                                       </asp:DropDownList>
                                   <div id="divDOcumentDate" runat="server" >
                                       <h4>
                                           Document Dates</h4>
                                           <div ID="docs_back_from_customer_wrapper" runat="server" clientidmode="Static">
                                                <!--Document Link-->
                                                <div id="divLOEHoldupReasonTypes_DocsBackfromCustomer" runat="server"  ClientIDMode="Static" >
                                                    <div class="document-link" id="LA_LOEHoldupReasonTypes_DocsBackfromCustomer" runat="server" style="width:345px;" >
                                                        <label>Docs Back from Customer</label>
                                                        <asp:TextBox ID="txtExecDocs_BackFromCustDate" CssClass="datepicker" runat="server"></asp:TextBox>
                                                        <asp:DropDownList ID="ddlExecDocs_BackFromCustDate_ColLeft1" runat="server" CssClass="split">
                                                            <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:HiddenField  ID="hidden_document_LA_LOEHoldupReasonTypes_DocsBackfromCustomer" ClientIDMode="Static" runat="server" />
                                                    </div>
                                                </div>
                                               <br />
                                               <asp:Label ID="lblLOEHoldupReasonTypes_SignedbyCustomer" runat="server">Signed by Customer</asp:Label>
                                               <asp:TextBox ID="txtExecDocs_SignedByCustDate" CssClass="datepicker"  runat="server"></asp:TextBox>
                                               <br />
                                           </div>
                                       
                                       <div ID="lease_execution_wrapper" runat="server" clientidmode="Static">
                                           <asp:Label ID="lblLOEHoldupReasonTypes_DocstoFSC" runat="server">Docs to FSC</asp:Label>
                                           <asp:TextBox ID="txtExecDocs_ToFSCDate" CssClass="datepicker" runat="server"></asp:TextBox>
                                           <br />
                                           <label>
                                              Date Received at FSC</label>
                                           <asp:TextBox ID="txtExecDocs_RecvedatFSCDate" CssClass="datepicker" runat="server"></asp:TextBox>
                                           <br />
                                       </div>
                                       <!--Document Link-->
                                       <div id="divLOEHoldupReasonTypes_LeaseExecution" runat="server" ClientIDMode="Static" >
                                           <div class="document-link" id="LA_LeaseExecution"  style="width:345px;" >
                                              <label>Lease Execution</label>
                                              <asp:TextBox ID="txtExecDocs_LeaseExecDate" CssClass="datepicker" runat="server"></asp:TextBox>
                                              <asp:DropDownList ID="ddlExecDocs_LeaseExecDate_ColLeft1" runat="server" CssClass="split">
                                                   <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                                              </asp:DropDownList>
                                              <asp:HiddenField  ID="hidden_document_LA_LeaseExecution" ClientIDMode="Static" runat="server" />
                                           </div>
                                       </div>
                                       <div ID="LA_amendment_execution_wrapper" runat="server" clientidmode="Static">
                                           <div class="document-link" id="LA_AmendmentExecution"  > 
                                               <asp:Label ID="lblLOEHoldupReasonTypes_AmendmentExecution" runat="server" 
                                                   Text="Amendment Execution"></asp:Label>
                                               <asp:TextBox ID="txtLOEHoldupReasonTypes_AmendmentExecution" runat="server" 
                                                   CssClass="datepicker"></asp:TextBox>
                                               <asp:DropDownList ID="ddlLOEHoldupReasonTypes_AmendmentExecution" 
                                                   runat="server" CssClass="split double-line-label-field">
                                               </asp:DropDownList>
                                               <asp:HiddenField  ID="hidden_document_LA_AmendmentExecution" ClientIDMode="Static" runat="server" />
                                           </div>
                                           <br />
                                       </div>
                                       <br />
                                       <div ID="Financial_CommencementForecast_wrapper" runat="server" clientidmode="Static">
                                           <asp:Label ID="lblFinancial_CommencementForecast" runat="server">Commencement Forecast</asp:Label>
                                           <asp:TextBox ID="txtExecDocs_CommencementForecastDate" CssClass="datepicker"  runat="server"></asp:TextBox>
                                       </div>
                                       <br />
                                       <label>
                                            Carrier Signature Notarized</label>
                                        <asp:TextBox ID="txtExecDocs_CarrierSignature" CssClass="datepicker" runat="server"></asp:TextBox>
                                        <asp:DropDownList ID="ddlExecDocs_CarrierSignature" ClientIDMode="Static" CssClass="split-r double-line-label-field" runat="server">
                                                <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                                                <asp:ListItem Text="Yes" Value="yes"></asp:ListItem>
                                                <asp:ListItem Text="No" Value="no"></asp:ListItem>
                                       </asp:DropDownList>
                                       <br />
                                       <label>
                                           Sent to Signatory</label>
                                       <asp:TextBox ID="txtExecDocs_SendSignator" CssClass="datepicker" runat="server"></asp:TextBox>
                                       <br />
                                        <label>
                                           Back from Signatory</label>
                                       <asp:TextBox ID="txtExecDocs_BackSignator" CssClass="datepicker" runat="server"></asp:TextBox>
                                       <br />
                                        <label>
                                           Original Sent to UPS</label>
                                       <asp:TextBox ID="txtExecDocs_OrgSentUPS" CssClass="datepicker" runat="server"></asp:TextBox>
                                       <br />
                                        <label>
                                            Doc Loaded to CST</label>  
                                        <asp:DropDownList ID="ddlExecDocs_DocLoadCST" ClientIDMode="Static" CssClass="split-r double-line-label-field" runat="server">
                                                <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                                                <asp:ListItem Text="Yes" Value="yes"></asp:ListItem>
                                                <asp:ListItem Text="No" Value="no"></asp:ListItem>
                                       </asp:DropDownList>
                                       <br />
                                   </div>
                                   <div id="divFinancial" runat="server" >
                                       <h4>
                                           Financial</h4>
                                       <label>
                                           Amendment Rent Affecting</label>
                                       <asp:DropDownList ID="ddlFinance_AmendRentAffect" ClientIDMode="Static" CssClass="split-r double-line-label-field" runat="server">
                                                <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                                                <asp:ListItem Text="Yes" Value="yes"></asp:ListItem>
                                                <asp:ListItem Text="No" Value="no"></asp:ListItem>
                                       </asp:DropDownList>
                                       <br />
                                       <label>
                                           One Time Fee</label>
                                       <asp:DropDownList ID="ddlFinance_OnTimeFee" ClientIDMode="Static" CssClass="split-r double-line-label-field" runat="server">
                                                <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                                                <asp:ListItem Text="Yes" Value="yes"></asp:ListItem>
                                                <asp:ListItem Text="No" Value="no"></asp:ListItem>
                                       </asp:DropDownList>
                                       <br />
                                       <!--Document Link-->
                                       <div class="document-link" id="LA_RentForecastAmount"  style="width:345px;" >
                                           <asp:Label ID="lblFinancial_RentForecastAmount" runat="server">Rent Forecast Amount</asp:Label>
                                           <asp:TextBox ID="txtFinanace_ForecastAmount" CssClass="moneyfield" runat="server"></asp:TextBox>
                                           <asp:HiddenField  ID="hidden_document_LA_RentForecastAmount" ClientIDMode="Static" runat="server" />
                                       </div>
                                       <br />
                                       <asp:Label ID="lblFinancial_RevenueShare" runat="server">Revenue Share</asp:Label>
                                       <asp:DropDownList ID="ddlFinanace_RevShare" ClientIDMode="Static" CssClass="split-r double-line-label-field" runat="server">
                                                <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                                                <asp:ListItem Text="Yes" Value="yes"></asp:ListItem>
                                                <asp:ListItem Text="No" Value="no"></asp:ListItem>
                                       </asp:DropDownList>
                                       <br />
                                        <label>
                                           Revshare Loaded to CST Receivable</label>
                                       <asp:DropDownList ID="ddlFinanace_LoadCSTRecv" ClientIDMode="Static" CssClass="split-r double-line-label-field" runat="server">
                                                <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                                                <asp:ListItem Text="Yes" Value="yes"></asp:ListItem>
                                                <asp:ListItem Text="No" Value="no"></asp:ListItem>
                                       </asp:DropDownList>
                                       <br />
                                        <label>
                                           Revshare Loaded to CST Payable</label>
                                       <asp:DropDownList ID="ddlFinanace_LoadCSTPay" ClientIDMode="Static" CssClass="split-r double-line-label-field" runat="server">
                                                <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                                                <asp:ListItem Text="Yes" Value="yes"></asp:ListItem>
                                                <asp:ListItem Text="No" Value="no"></asp:ListItem>
                                       </asp:DropDownList>
                                       <br />
                                       <asp:Label ID="lblFinancial_REMLeaseSequence" runat="server">REM Lease Sequence</asp:Label>
                                       <asp:TextBox ID="txtFinancial_REMLeaseSequence" runat="server" 
                                           CssClass="double-line-label-field"></asp:TextBox>
                                       <br />
                                       <asp:Label ID="lblFinancial_CostShareAgreement" runat="server">Cost Share Agreement</asp:Label>
                                       <asp:DropDownList ID="ddlFinanace_CostShare" ClientIDMode="Static" CssClass="split-r double-line-label-field" runat="server">
                                                <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                                                <asp:ListItem Text="Yes" Value="yes"></asp:ListItem>
                                                <asp:ListItem Text="No" Value="no"></asp:ListItem>
                                       </asp:DropDownList>
                                       <asp:TextBox ID="txtFinanace_CostShare" CssClass="moneyfield split-r double-line-label-field" runat="server"></asp:TextBox>
                                       <br />
                                       <asp:Label ID="lblFinancial_CapCostRecovery" runat="server">Cap Cost Recovery</asp:Label>
                                       <asp:DropDownList ID="ddlFinanace_Capcost" ClientIDMode="Static" CssClass="split-r double-line-label-field" runat="server">
                                                <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                                                <asp:ListItem Text="Yes" Value="yes"></asp:ListItem>
                                                <asp:ListItem Text="No" Value="no"></asp:ListItem>
                                       </asp:DropDownList>
                                       <asp:TextBox ID="txtFinanace_Capcost" CssClass="moneyfield split-r double-line-label-field" runat="server"></asp:TextBox>
                                       <br />
                                       <asp:Label ID="lblLOEHoldupReasonTypes_TerminationDate" runat="server" 
                                           Text="Termination Date"></asp:Label>
                                       <asp:TextBox ID="txtFinance_TerminateDate" CssClass="datepicker"  runat="server"></asp:TextBox>
                                   </div>
                               </div>
                               <div class="right-col"  style="width:48%;">
                                    <div id="divCommencementPhase" style="width: 100%;height:40px;text-align:center;background:#F0F0F0;margin-top:10px;margin-bottom:20px;">
                                        <span id="lblCommencementPhase" style="width:100%;text-align:left;margin-left:5px;font-weight:bold;margin-top:10px;" >Commencement Phase</span>
                                    </div>
                                    
                                    <!--Document Link-->
                                    <div class="document-link" id="LA_NTPtoCust"  style="width:345px;" >
                                       <label>NTP to Customer</label>
                                       <asp:TextBox ID="txtComDocs_NTPtoCust" CssClass="datepicker" runat="server"></asp:TextBox>
                                       <asp:HiddenField  ID="hidden_document_LA_NTPtoCust" ClientIDMode="Static" runat="server" />
                                    </div>
                                    <br />
                                   <label>
                                       Document Type</label>
                                   <asp:DropDownList ID="ddlComDocs_DocType" runat="server" >
                                        <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                                   </asp:DropDownList>
                                       <br />
                                   <label>
                                       Commencement Date</label>
                                   <asp:TextBox ID="txtComDocs_CommencementDate" CssClass="datepicker" runat="server"></asp:TextBox>
                                   <div id="divComDocDate" runat="server" >
                                       <h4>
                                           Document Dates</h4>
                                       <label>
                                           Date Received at FSC</label>
                                       <asp:TextBox ID="txtComDocs_RecvFSCDate" CssClass="datepicker" runat="server"></asp:TextBox>
                                       <br />
                                       <label>
                                           Received By</label>
                                       <asp:DropDownList ID="ddlComDocs_RecvBy" runat="server">
                                            <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                                       </asp:DropDownList>
                                       <br />
                                       
                                        <!--Document Link-->
                                       <div class="document-link" id="LA_RegCommencementLetterDate"  style="width:345px;" >
                                          <label>Reg Commencement Letter Created</label>
                                          <asp:TextBox ID="txtComDocs_RegCommencementLetterDate" CssClass="datepicker" runat="server"></asp:TextBox>
                                          <asp:HiddenField  ID="hidden_document_LA_RegCommencementLetterDate" ClientIDMode="Static" runat="server" />
                                       </div>
                                       <br />
                                       <label>
                                           REM Alert&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</label>
                                       <asp:TextBox ID="txtComDocs_RemAlert" runat="server" 
                                           ClientIDMode="Static" 
                                           style="width: 180px; max-width:180px; margin-bottom:0px !important; " 
                                           TextMode="MultiLine"></asp:TextBox>
                                       <br />
                                   </div>
                               </div>
                               <div class="full-col align-right" style="margin-bottom: 30px;">
                                   <asp:Button ID="btnSubmitLeaseAdmin_Bottom" runat="server" 
                                               Text="Save"
                                               CssClass="ui-button ui-widget ui-state-default ui-corner-all"
                                               OnClick="btnSubmitLeaseAdmin_Click"
                                               OnClientClick="if (!CheckBeforeSaveLeaseAdmin()) {return false;}" 
                                               UseSubmitBehavior="false" />
                                </div>
                               <div class="cb">
                               </div>
                               <div class="full-col align-right" style="margin-bottom:30px;margin-top:20px;" id="divBtnAddLeaseComment" runat="server">
                                        <asp:LinkButton ID="link_addnew_lease" runat="server" CssClass="link-comment" 
                                            OnClientClick="return linkAddNewLeaseCommentClick();">Add New Comment</asp:LinkButton>
                                        <div id="lease-comment-link">
                                        </div>
                                </div>
                               <div class="application-comments-inner-lease" style="margin-bottom: 20px; margin-top: 30px;">
                                   <div class="new-comment" id="divAddnewLeaseComment" runat="server">
                                       <div class="balloon">
                                           <asp:TextBox ID="txt_comment_lease" runat="server" Columns="5" CssClass="comment-textarea"
                                               Rows="2" TextMode="MultiLine"></asp:TextBox>
                                           <div class="balloon-bottom">
                                           </div>
                                       </div>
                                       <div class="author-info" style="margin-top: 15px; font-size: 10pt; color: #a1a1a1;">
                                           <asp:Image ID="img_Author_LeaseAdmin" runat="server" AlternateText="profile-picture"
                                               ImageUrl="/images/avatars/default.png" Width="25" />
                                           <div id="commentLeaseButton" style="text-align: right;">
                                               <asp:Button ID="btnLeaseAdminCancel" runat="server" ClientIDMode="Static" CssClass="sa-comment-button"
                                                   OnClientClick="return linkCancelLeaseCommentClick();" Text="Cancel" />
                                               <asp:Button ID="btnLeaseAdminPost" runat="server" ClientIDMode="Static" CssClass="sa-comment-button"
                                                   NavigateUrl="javascript:void(0)" OnClientClick="linkLeasePostClick(); return false;" Text="Post" />
                                           </div>
                                       </div>
                                   </div>
                               </div>
                                <div class="cb">
                               </div>
                           </div>
                       </div>
                   </div>
                   <div class="cb">
                   </div>
               </div>
            </div>
            <div class="cb">
            </div>
        </div>
    </asp:Panel>
    <div id="modalExternalDms" class="modal-container" style="overflow:hidden;"></div>
    <asp:HiddenField id="hidBasePath" ClientIDMode="Static" runat="server"/>
    <!-- .content -->
</asp:Content>
