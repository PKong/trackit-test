﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="" Language="C#" EnableEventValidation="False"  MasterPageFile="~/Templates/Modal.Master" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="TrackIT2.LeaseAdminTracker.Add" %>
<%@ MasterType TypeName="TrackIT2.Templates.Modal" %>
<%@ Register TagPrefix="TrackIT2" TagName="TextFieldAutoComplete" Src="~/Controls/TextFieldAutoComplete.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <!-- Styles -->
    <%: Styles.Render("~/Styles/lease_admin_add") %>
       
    <!-- Scripts -->
    <%: Scripts.Render("~/Scripts/lease_admin_add") %>
   
    <script type="text/javascript" >
        $(function () {
            initAddLeaseAdmin();

            $('#MainContent_txtSiteIds').focusout(function () {
                validateSiteIDALL(false);
            })
            $('#MainContent_ddlAssociatedLeaseApps').change(function () {
                $('#Hidden_selected_leaseapp').val($('#MainContent_ddlAssociatedLeaseApps').val());
            });
            $('#MainContent_ddlCustomer').change(function () {
                $('#Hidden_selected_customer').val($('#MainContent_ddlCustomer').val());
            });
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel id="updatePanel"  ViewStateMode="Enabled" UpdateMode="Always" runat="server">
        <ContentTemplate>
            <div id="message_alert"></div>
            <div class="create-container" style="margin: 0 auto 0 auto;width:400px;">
                <div class="create-container-small form_element general-controls" style="padding-bottom: 0px;">
                    <h4>Associate with Lease Application</h4>
                    <input type="radio" id="radAssoc" name="groupAddType" value="1"  onclick="ManegeToggle(this);" runat="server" />&nbsp;<label>Yes</label>
                    <input type="radio" id="radNoAssoc" name="groupAddType" value="2" onclick="ManegeToggle(this);" runat="server" />&nbsp;<label>No</label> 
                </div>
        
                <div id="associate" runat="server"  enableviewstate="true" >
                    <asp:Panel ID="pnlAssocLeaseApp" CssClass="create-container-small form_element" runat="server">
                        <h4>Site ID</h4>
                        <label style="width:95px;">Select Site</label>
                        <TrackIT2:TextFieldAutoComplete ID="ctrAssocSiteID" 
                            LabelFieldClientID="lblAssocSiteID" 
                            LabelFieldValue=""
                            LabelFieldCssClass="label" 
                            TextFieldClientID="txtAssocSiteID" 
                            TextFieldCssClass="input-autocomplete"
                            TextFieldWidth="188" 
                            TextFieldValue="" 
                            ScriptKeyName="ScriptAssociateAutoCompleteSiteID"
                            DataSourceUrl="AssociateSiteIdAutoComplete.axd" 
                            ClientIDMode="Static" 
                            runat="server" />
                        <div>
                            <h4>Select Associated Lease Application</h4>
                            <br />
                            <asp:Label ID="lblNotfound" runat="server" Text="Associated applications not found." Font-Size="Small" Width="100%" style="text-align:center;font-style: italic;" ></asp:Label>
                            <br />
                            <asp:DropDownList ID="ddlAssociatedLeaseApps"  CssClass="select-field" 
                                runat="server" style="margin-left:80px;" Width="250px" >
                            </asp:DropDownList>
                        </div>
                        <br />
                        <asp:Button ID="btnSubmitCreateAssoc" runat="server" EnableViewState="true" style="float:right;margin-top:30px;" CssClass="ui-button ui-widget ui-state-default ui-corner-all" UseSubmitBehavior="false" Text="Create" OnClientClick="if (!validateCreateAssocLeaseAdmin()) { return false;}else{AjaxLeaseAdminCreateAndEditHandler(); return false;}" />
                    </asp:Panel> 
                </div>
                <div id="no_associate" runat="server" style="display:none;" enableviewstate="true"  >
                    <asp:Panel ID="pnlNonAsscoLeaseApp" CssClass="create-container-small form_element" runat="server">
                        <div id="divSite" runat="server">
                            <h4>Site ID</h4>
                            <label style="width:95px;">Select Site</label>
                            <TrackIT2:TextFieldAutoComplete ID="ctrSiteID" 
                                LabelFieldClientID="lblSiteID" 
                                LabelFieldValue=""
                                LabelFieldCssClass="label" 
                                TextFieldClientID="txtSiteID" 
                                TextFieldCssClass="input-autocomplete"
                                TextFieldWidth="188" 
                                TextFieldValue="" 
                                ScriptKeyName="ScriptAutoCompleteSiteID"
                                DataSourceUrl="SiteIdAutoComplete.axd" 
                                ClientIDMode="Static" 
                                runat="server" />
                            <div id="ListSiteId" runat="server" >
                                <ol class="asmList ui-sortable asmListSortable" id="siteIdList" style="width:198px;margin-left:103px;"></ol> 
                            </div>
                            <h4>Add Multiple Site ID</h4>
                            <label class="label-auto"><em>(separated by a comma or whitespace)</em></label>
                            <asp:TextBox ID="txtSiteIds" runat="server" TextMode="MultiLine" Style="margin-bottom: 10px;" Width="100%" Height="70px" ></asp:TextBox>
                            <br />
                            <h4>Customer</h4>
                             <asp:DropDownList ID="ddlCustomer" CssClass="select-field" runat="server" style="margin-left:80px;" Width="250px" >
                             </asp:DropDownList>
                            <br />
                        </div>
                        <asp:Button ID="btnSubmitCreateApps" runat="server" EnableViewState="true" style="float:right;margin-top:30px;" CssClass="ui-button ui-widget ui-state-default ui-corner-all"   UseSubmitBehavior="false"  Text="Create" OnClientClick="if (!validateCreateLeaseAdmin()) { return false;}else{AjaxLeaseAdminCreateAndEditHandler(); return false;}" />
                    </asp:Panel> 
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField ID="Hidden_Lease_Admin_ID"  runat="server" Value="0" ClientIDMode="Static"/>
    <asp:HiddenField ID="Hidden_selected_site_ids"  runat="server" Value="" ClientIDMode="Static"/>
    <asp:HiddenField ID="Hidden_selected_leaseapp"  runat="server" Value="" ClientIDMode="Static"/>
    <asp:HiddenField ID="Hidden_selected_customer"  runat="server" Value="" ClientIDMode="Static"/>
    <asp:HiddenField ID="HiddenDialogHeightFlag"  runat="server" Value="0" ClientIDMode="Static"/>
</asp:Content>
