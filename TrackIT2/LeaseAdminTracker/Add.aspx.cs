﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.BLL;
using TrackIT2.DAL;
using System.Text;
using System.Globalization;
using System.Web.Services;
using System.Collections.Specialized;
using System.Web.Script.Serialization;

namespace TrackIT2.LeaseAdminTracker
{
    /// <summary>
    /// Add - Class
    /// </summary>
    public partial class Add : System.Web.UI.Page
    {
        #region Event
        
        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                ddlAssociatedLeaseApps.Style.Add("display", "none");
                lblNotfound.Text = "Please select a Site ID first.";
                Control ctrl = PageUtility.FindControlRecursive(this.pnlAssocLeaseApp, this.ctrAssocSiteID.TextFieldClientID);
                TextBox t = ctrl as TextBox;
                t.Style.Add("background-color", "#FFFFFF");
                ddlAssociatedLeaseApps.Style.Add("background-color", "#FFFFFF");

                this.radAssoc.Checked = true;
                int iLeaseAppID = 0;
                Utility.ClearSubmitValidator();
                BindLookupLists();
                if (Request.QueryString["id"] != null && int.TryParse(Request.QueryString["id"], out iLeaseAppID))
                {
                    ViewState["id"] = iLeaseAppID;
                    Hidden_Lease_Admin_ID.Value = iLeaseAppID.ToString();
                    LoadData(iLeaseAppID);
                    btnSubmitCreateApps.Text = "Save";
                    btnSubmitCreateAssoc.Text = "Save";
                }
                else
                {
                    this.ResetForm();
                }
            }
        }
        //-->
        
        #endregion

        #region Function

        /// <summary>
        /// Btn Add Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BindLookupLists()
        {
            using (CPTTEntities ce = new CPTTEntities())
            {
                var customers = ce.customers.OrderBy(Utility.GetField<customer>("customer_name")).OrderBy(Utility.GetIntField<customer>("sort_order"));
                Utility.BindDDLDataSource(this.ddlCustomer, customers, "id", "customer_name", "- Select -");
            }
        }
        //-->
        /// <summary>
        /// Reset Form
        /// </summary>
        private void ResetForm()
        {
            //Clear Site and Associate Site box
            ((TextBox)ctrSiteID.Controls[1]).Text = string.Empty;
            ((TextBox)ctrAssocSiteID.Controls[1]).Text = string.Empty;
            this.txtSiteIds.Text = String.Empty;
        }
        //-->
        #endregion

        #region Binding Data
        
        /// <summary>
        /// Load Data - for Edit Screen
        /// </summary>
        /// <param name="iLeaseAppID"></param>
        private void LoadData(int iLeaseAppID)
        {
            using (CPTTEntities ce = new CPTTEntities())
            {
                lease_admin_records objLeaseAdmin = LeaseAdminRecord.Search(iLeaseAppID,ce);
                if (objLeaseAdmin != null)
                {
                    if (objLeaseAdmin.lease_application_id.HasValue)
                    {
                        this.radAssoc.Checked = true;
                        this.radNoAssoc.Checked = false;
                        this.radNoAssoc.Attributes.Add("disabled", "disabled");
                        this.associate.Style.Add("display", "block");
                        this.no_associate.Style.Add("display", "none");
                        this.Hidden_selected_leaseapp.Value = objLeaseAdmin.lease_application_id.ToString();
                        ((TextBox)ctrAssocSiteID.Controls[1]).Text = objLeaseAdmin.sites.ToList<site>()[0].site_uid;
                        BindListBox(objLeaseAdmin.sites.ToList<site>()[0].site_uid, objLeaseAdmin.lease_application_id);
                    }
                    else
                    {
                        this.radAssoc.Checked = false;
                        this.radNoAssoc.Checked = true;
                        this.radAssoc.Attributes.Add("disabled", "disabled");
                        this.associate.Style.Add("display", "none");
                        this.no_associate.Style.Add("display", "block");
                        Utility.ControlValueSetter(ddlCustomer, objLeaseAdmin.customer_id);
                        this.Hidden_selected_customer.Value = objLeaseAdmin.customer_id.ToString();
                        StringBuilder sSites = new StringBuilder();
                        bool bIsFirst = false;
                        foreach (site siteData in objLeaseAdmin.sites)
                        {
                            if (bIsFirst)
                            {
                                sSites.Append(",");
                            }
                            bIsFirst = true;
                            sSites.Append(siteData.site_uid);
                        }
                        this.txtSiteIds.Text = sSites.ToString();
                    }

                }
            }
        }
        //-->
        
        /// Bind List Box - binding associate Lease Aplication with Site ID
        /// </summary>
        /// <param name="siteID"></param>
        /// <param name="leaseAppId"></param>
        private void BindListBox(string siteID ,int? leaseAppId = null)
        {
            List<AssociatedLeaseAppInfo> lstAssociatedLa = AssociatedLeaseApp.GetAssociatedLeaseApp(siteID);
            if (lstAssociatedLa.Count > 0)
            {
                ddlAssociatedLeaseApps.Style.Add("display", "block");
                var sortLstAssociatedLa = lstAssociatedLa.ToList();
                var dynamicLstAssociatedLa = new DynamicComparer<AssociatedLeaseAppInfo>();
                dynamicLstAssociatedLa.SortOrder(x => x.Descriptions);
                sortLstAssociatedLa.Sort(dynamicLstAssociatedLa);
                Utility.BindDDLDataSource(ddlAssociatedLeaseApps, sortLstAssociatedLa, "id", "Descriptions", " Click to Select an Application ");                
                if (leaseAppId.HasValue)
                {
                    Utility.ControlValueSetter(this.ddlAssociatedLeaseApps, leaseAppId);
                    this.Hidden_selected_leaseapp.Value = leaseAppId.ToString();
                }
                lblNotfound.Style.Add("display", "none");
            }
            else
            {
                ddlAssociatedLeaseApps.Style.Add("display", "none");
                lblNotfound.Style.Add("display", "block");
            }
        }

        #endregion
    }

}