﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.DAL;
using TrackIT2.BLL;

namespace TrackIT2.LeaseAdminTracker.LEComment
{
    public partial class Delete : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int comment_id;
            if (!Page.IsPostBack && Request.QueryString["id"] != null && int.TryParse(Request.QueryString["id"], out comment_id))
            {
                LeaseAdminComments.Delete(comment_id);
            }
        }
    }
}