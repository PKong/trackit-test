﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="Edit Lease Application | TrackiT2" Language="C#" MasterPageFile="~/Templates/TrackIT2.Master" AutoEventWireup="true" ValidateRequest="false" CodeBehind="Edit.aspx.cs" Inherits="TrackIT2.LeaseApplications.Edit" %>
<%@ MasterType TypeName="TrackIT2.Templates.TrackIT2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

   <!-- Styles -->
   <%: Styles.Render("~/Styles/lease_application_edit") %>

   <style type="text/css">
    table
    {
        border-collapse:inherit;
    }
   </style>
   <!-- Scripts -->
   <%: Scripts.Render("~/Scripts/lease_application_edit") %>
     
    <script type="text/javascript">
        // Doc Ready
        $(document).ready(function () {
            var tab_index = '<%= Session["lower_tabs_index"] %>';
            var tabTest = '<%= Session["cur_tab_index"] %>';
            //   alert(tabTest);
            currentFlexiGridID = '<%= this.fgDMS.ClientID %>';
            var currentSaID = 0;
            if ($('#MainContent_hidden_SA_ID').val() != null)
            { currentSaID = $('#MainContent_hidden_SA_ID').val(); }

            // Select Tabs	
            $(function () {
                $("#tabs2").tabs({ selected: parseInt(tabTest) });
            });

            var index_sa = 0;
            if (parseInt($("#MainContent_hidden_SA_count").val()) > 0) {
                index_sa = parseInt($("#MainContent_hidden_SA_count").val()) - 1;
            }

            var index_rv = 0;
            if (parseInt($("#MainContent_hidden_revision_count").val()) > 0) {
                index_rv = parseInt($("#MainContent_hidden_revision_count").val()) - 1;
            }

            setPageName(pageSA);
            setBufferIndex(index_sa);
            setRVBufferIndex(index_rv);
            setBtnCount($("#MainContent_hidden_SA_count").val());
            setBtnRVCount($("#MainContent_hidden_revision_count").val());
            createLeaseAdminCommentUI();

            initEventClickBack();
            initEventClickNext();
            initEventClickAddNew();
            //Revisions
            initEventClickRVBack();
            initEventClickRVNext();
            initEventClickRVAddNew();

            // SetCssBtn
            cssBtnControlPage();

            // Date Picker
            $(function () {
                $('input').filter('.datepicker').datepicker({
                    numberOfMonths: 2,
                    showButtonPanel: false
                });
            });

            // Comments
            var prmComments = Sys.WebForms.PageRequestManager.getInstance();
            prmComments.add_pageLoaded(setupCommentsLocal);

            FlexigridParam.MAX_WIDTH = 944;
            FlexigridParam.MAX_WIDTH_DISPLAY = 944;
            initDocumentTabs();
            InitDMSFlexiRelatedControls("#h1", "#sBasePath", "#hidBasePath", "");
            loadGridDocuments('Application');
            $('#hidden_document_type').val('Application');

            //Antenne
            InitTab("Ant", "#labAntPageNum", "#labAntPageTotal", "", "#btnAntNext", "#btnAntPrev",
                        "#hidAntJSON", "#hidAntCount", "#sliAnt", true);

            //Amplifier
            InitTab("Amp", "#labAmpPageNum", "#labAmpPageTotal", "", "#btnAmpNext", "#btnAmpPrev",
                        "#hidAmpJSON", "#hidAmpCount", "#sliAmp", true);
            //GPS
            InitTab("GPS", "#labGPSPageNum", "#labGPSPageTotal", "", "#btnGPSNext", "#btnGPSPrev",
                        "#hidGPSJSON", "#hidGPSCount", "#sliGPS", true);
            //Microwave
            InitTab("Mic", "#labMicPageNum", "#labMicPageTotal", "", "#btnMicNext", "#btnMicPrev",
                        "#hidMicJSON", "#hidMicCount", "#sliMic", true);
            //Other equipment
            InitTab("Oth", "#labOthPageNum", "#labOthPageTotal", "", "#btnOthNext", "#btnOthPrev",
                        "#hidOthJSON", "#hidOthCount", "#sliOth", true);
            //Ground space
            InitTab("Gsp", "#labGspPageNum", "#labGspPageTotal", "", "#btnGspNext", "#btnGspPrev",
                        "#hidGspJSON", "#hidGspCount", "#sliGsp", true);

            // Set tab height
            SetTabHeight();

            // Add height
            dynamicSetUlHeight("MainContent_ulApplicationWorkflows", "liApplicationWorkflowsWrapper");

            //All dynamic tab data binding
            BindingData();

            // Select tab
            $('#aEquipmentInformation').click(function () {
                firstEquipmentSelect();
            });

            //Create Link document for all comments
            CreateCommentLinkDocument('MainContent_hidden_document_all_document_comments', 'document-comment-');
        });

        function toggleGridDocument(obj,eType){
            $('#liApplicationDocument').removeClass("DocumentButtonActive");
            $('#liApplicationDocument').addClass("DocumentButton");
            $('#liCustomerDocument').removeClass("DocumentButtonActive");
            $('#liCustomerDocument').addClass("DocumentButton");
            $('#liSiteDocument').removeClass("DocumentButtonActive");
            $('#liSiteDocument').addClass("DocumentButton");

            $('#hidden_document_type').val(eType)
            $(obj).parent().addClass("DocumentButtonActive");
            loadGridDocuments(eType);
        }

        function loadGridDocuments(eType) {
            removeFilterPostParam("listDocumentType");
            removeFilterPostParam("leaseAppId");

            addFilterPostParam("listDocumentType", eType);
            var id = $.urlParam('id');
            addFilterPostParam("leaseAppId", id);
            bIsLoading = true;
            reloadDataGrid();
        }

        function initDocumentTabs() {
            $('#liApplicationDocument a').bind('click', function (e) {
                if (!bIsLoading) {
                    toggleGridDocument(this, 'Application');
                }
                else {
                    e.preventDefault();
                    return false;
                }
            });

            $('#liCustomerDocument a').bind('click', function (e) {
                if (!bIsLoading) {
                    toggleGridDocument(this, 'Customer');
                }
                else {
                    e.preventDefault(); 
                    return false;
                }
            });

            $('#liSiteDocument a').attr('href', '/DMS/#/' + $('#MainContent_SiteID').text() + "/");
        }
          
            //-->
            function setShowCommentBox() {
                var mainCommentCount = $("#MainContent_hidden_count_main_comment").val();
                if (parseInt(mainCommentCount) == 0 || $("#MainContent_divComment1").length == 0) {
                    showCommentBox();
                }
            }
            // Setup Comments Local
            function setupCommentsLocal() {
                setupComments();
                setShowCommentBox();
            }
            
            function loadGridDocuments(eType) {
               removeFilterPostParam("listDocumentType");
               removeFilterPostParam("leaseAppId");

               addFilterPostParam("listDocumentType", eType);
               var id = $.urlParam('id');
               addFilterPostParam("leaseAppId", id);
               bIsLoading = true;
               reloadDataGrid();
            }

            function SetTabHeight() {
                var current = navigator.userAgent;
                if (current.indexOf('MSIE') > 0) {
                    $('#MainContent_liPreliminaryReview').height(48);
                    $('#MainContent_liStructuralAnalysis').height(48);
                    $('#MainContent_liDocumentReview').height(48);
                    $('#MainContent_liLease').height(48);
                    $('#MainContent_liConstruction').height(48);
                    $('#MainContent_liRevisions').height(48);
                }
                else if (current.indexOf('Firefox') > 0) {
                    $('#MainContent_liPreliminaryReview').height(48);
                    $('#MainContent_liStructuralAnalysis').height(48);
                    $('#MainContent_liDocumentReview').height(48);
                    $('#MainContent_liLease').height(48);
                    $('#MainContent_liConstruction').height(48);
                    $('#MainContent_liRevisions').height(48);
                }
                else if (current.indexOf('Chrome') > 0) {
                    $('#MainContent_liPreliminaryReview').height(46);
                    $('#MainContent_liStructuralAnalysis').height(46);
                    $('#MainContent_liDocumentReview').height(46);
                    $('#MainContent_liLease').height(46);
                    $('#MainContent_liConstruction').height(46);
                    $('#MainContent_liRevisions').height(46);
                }
            }

            function SetFirstSelectTab(name) {
                if (name != "null") {
                    $('#firstTab').val(name);
                }
            }

            function dynamicSetUlHeight(targetUl, targetNewHeight) {
                if ($('#' + targetUl).length > 0 && $('#' + targetNewHeight).length > 0) {
                    if ($('#' + targetUl).css('display') != "none") {
                        if ($('#' + targetUl).children().length > 0) {
                            var newHeight = 0;
                            $('#' + targetUl).children().each(function () {
                                newHeight += 15;
                            });
                            $('#' + targetNewHeight).height(newHeight);
                        }
                    }
                }
            }

            function firstEquipmentSelect() {
                if ($('#firstTab').val() != "null") {
                    switch ($('#firstTab').val()) {
                        case 'Antenna':
                            $('#aAntennas').click();
                            break;
                        case 'Amplifier':
                            $('#aAmplifiers').click();
                            break;
                        case 'GPS':
                            $('#aGps').click();
                            break;
                        case 'Microwave':
                            $('#aMicrowave').click();
                            break;
                        case 'Other':
                            $('#aOther').click();
                            break;
                        case 'GSP':
                            $('#aGsp').click();
                            break;
                        case 'Comment':
                            $('#aComments').click();
                            break;
                        default:
                            break;
                    }
                    $('#firstTab').val("null");
                }
            }

            // FG Column Change
            function fgColumnChange(cid, visible) {
                DynamicColumnChange(currentFlexiGridID, cid, visible);
            }
            //-->

            function autoLoadSaComment() {
                $(window).scroll(function () {
                    if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
                        if ($("#MainContent_liStructuralAnalysis").hasClass("ui-state-active")) {
                            $('#sa-comment-spinner-cont').activity({ segments: 8, width: 3, space: 0, length: 3, speed: 1.5, align: 'left' });
                            loadMoreSaComment();
                            $('#sa-comment-spinner-cont').fadeOut('fast');
                        }
                    }
                });
            }
        </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" language="javascript">
        // Doc Ready
        $(function () {
            var id = window.location.toString().substr(window.location.toString().indexOf('=') + 1);

            // Setup List Create Modal
            setupListCreateModal("Edit Application", id);
            setupListCreateModalSummary("Edit Summary", id);
            setupListCreateModalCustomerContacts("Edit Customer Contacts", id);
            setupModalHistory("History Application", id);
            setupModalCreateTowerMod("Create New Tower Mod", id);
            setupListCreateModalCreateSDP("Create New SDP", id);

            //Setup Document link Dialog
            setupDocumentLinkDialog("Document Links");

            jumpToSelectedSaOrLeaseAppTab();
            if ($("#HiddenSaID").val() != "null" && $("#HiddenSaID").val() !="") {
                SACommentClick($("#HiddenSaID").val());
            }
            else if ($("#HiddenLeaseAdmin").val() != "null" && $("#HiddenLeaseAdmin").val() !="") {
                LECommentClick($("#HiddenLeaseAdmin").val());
            }

            autoLoadSaComment();
        });
        //-->
    </script>
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server" />

    <asp:Panel ID="pnlNoId" runat="server" CssClass="align-center">
       <h1>Not Found (404)</h1>

       <p>The lease application specified is invalid or no longer exists.</p>

       <p>&nbsp;</p>

       <p>
          <a href="Default.aspx">&laquo; Go back to list page.</a>
       </p>

       <p>&nbsp;</p>
       <p>&nbsp;</p>
       <p>&nbsp;</p>
       <p>&nbsp;</p>
       <p>&nbsp;</p>
       <p>&nbsp;</p>
       <p>&nbsp;</p>
    </asp:Panel>
    <asp:HiddenField ID="HiddenSaID"  runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="HiddenLeaseAdmin"  runat="server" ClientIDMode="Static" />

    <asp:HiddenField ID="firstTab"  runat="server" ClientIDMode="Static" Value="null" />
    <asp:Panel ID="pnlEdit" runat="server" Visible="false">
        <h3>Edit Lease Application</h3>
        <p>
            <b>ID: </b>
            <asp:Label ID="lblID" runat="server"></asp:Label>
        </p>
        <p>
            <b>Site ID: </b>
            <%--<asp:Label ID="lblSiteId" runat="server" Text="[Site ID]"></asp:Label>--%>
            <asp:LinkButton ID="lblSiteId" runat="server" Text="[Site ID]"></asp:LinkButton>
        </p>
        <p>
            <b>Customer Site UID: </b>
            <asp:Label ID="lblCustomerSiteUid" runat="server" Text="[Location]"></asp:Label>
        </p>
        <p>
            <b>Customer: </b>
            <asp:Label ID="lblCustomer" runat="server" Text="[Customer Name]"></asp:Label>
        </p>
        <p>
            <b>Line Count: </b>
            <asp:TextBox ID="txtLineCount" runat="server" Width="100" MaxLength="5"></asp:TextBox>
            <asp:RangeValidator ID="valLineCountNumeric" runat="server" ControlToValidate="txtLineCount"
                CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please specify a numeric value from 0 to 99999"
                MinimumValue="0" MaximumValue="99999" Type="Integer">
            </asp:RangeValidator>
        </p>
        <p>
            &nbsp;
        </p>
        <p style="text-align: center;">
            <asp:Button ID="btnSubmit" runat="server" Text="Update" OnClick="btnSubmit_Click" />
            &nbsp;&nbsp;
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
        </p>
    </asp:Panel>
    <asp:Panel ID="pnlSectionContent" runat="server">    
       <div class="section content">
           <div class="wrapper">
               <div class="action-tab">
                <div class="left-side">
                    <asp:HyperLink ID="backToList" runat="server" Text="« Return to List" NavigateUrl="~/LeaseApplications/" CssClass="site-link-no-underline"></asp:HyperLink>
                </div>
                <div class="right-side">
                    <!-- Button Delete Lease Apps class="edit-button"-->
                    <a runat="server" id="modalCreateSDPExternal" class="link-button site-link-no-underline" href="javascript:void(0)" clientidmode="Static" visible="false">Create SDP</a> 

                    <!-- Button Delete Lease Apps class="edit-button"-->
                    <a runat="server" id="modalCreateTowermodExternal" class="link-button site-link-no-underline" href="javascript:void(0)" clientidmode="Static" visible="false">Create Tower Mod</a>
                    
                    <!-- Button Delete Lease Apps CssClass="del-button btn-app-delete"-->
                    <asp:LinkButton ID="btnAppDelete" class="link-button site-link-no-underline" OnClientClick="return openDeleteAppButtonPopup('Lease Application',event);" OnClick="btnLADelete_Click" runat="server">Delete</asp:LinkButton>
                </div>
                
               </div>
               <div class="application-information">                
                   <h1>                 
                     <asp:Label ID="ProjectCategory" runat="server" CssClass="h1"></asp:Label>&nbsp;Application Information 
                     <span>
                        <asp:HyperLink ID="SiteID" runat="server" Text="[Site ID]" CssClass="contactInfoFont site-link-no-underline"></asp:HyperLink>
                        - 
                        <asp:Label ID="SiteName" runat="server" Text="" class="contactInfoFont"></asp:Label>
                        <asp:Label ID="CustomerUIDHeader" runat="server" Text="/"></asp:Label>
                        <asp:Label ID="CustomerUID" runat="server"></asp:Label>
                        -
                        <asp:Label ID="CustomerName" runat="server" Text="" class="contactInfoFont"></asp:Label>
                        <asp:Label ID="OriginalCarrier" runat="server" Text="" class="contactInfoFont"></asp:Label>                                          
                     </span>
                   </h1>

                   <div id="tabs" class="toggle-me">
                       <ul class="tabs">
                           <li>
                              <a href="#general-information" >General Information</a>
                           </li>
                           <li id="LiEquipmentInformation">
                              <a id="aEquipmentInformation" href="#equipment-information" >Equipment Information</a>
                           </li>
                           <li id="liCustomerContacts" runat="server">
                              <a href="#customer_contacts" >Customer Contacts</a>
                           </li>
                           <li id="liDocuments" runat="server">
                              <a href="#aDocuments" >Documents</a>
                           </li>
                       </ul>
                       <div id="general-information">
                           <div style="padding: 20px;overflow:auto;">
                               <div class="i-2-3">
                                   <div class="general-information-column">
                                   <div style="float:left; width:210px;" >
                                       <ul id="ulAplicationDetails" runat="server" class="information-list"  >
                                           <li>Application Details</li>
                                           <li id="liAppType" runat="server" clientidmode="Static">Type: 
                                               <asp:Label ID="lblAppType" runat="server" Text="" class="contactInfoFont"></asp:Label></li>
                                           <li id="liActivity" runat="server" clientidmode="Static">Activity:
                                               <asp:Label ID="lblActivity" runat="server" Text="" class="contactInfoFont"></asp:Label></li>
                                           <li id="liTMOAppId" runat="server" clientidmode="Static">TMO App Id:
                                               <asp:Label ID="lblTMOAppId" runat="server" Text="" class="contactInfoFont"></asp:Label></li>
                                           <li id="liAppReceived" runat="server" clientidmode="Static">Received:
                                               <asp:Label ID="lblAppReceived" runat="server" Text="" class="contactInfoFont"></asp:Label></li>
                                           <li id="liStatus" runat="server" clientidmode="Static">Status:
                                               <asp:Label ID="lblStatus" runat="server" Text="" Style="color: green; font-weight: bold; text-shadow: 0 0 10px rgba(34,233,85,1)"></asp:Label></li>
                                           <li id="liAppStatusDate" runat="server" clientidmode="Static">App Status Date:
                                               <asp:Label ID="lblAppStatusDate" runat="server"></asp:Label></li>                                    
                                           <li id="liCustomerCarrierProject" runat="server" clientidmode="Static">Carrier Project:
                                               <asp:Label ID="lblCarrierProject" runat="server" Text="" class="contactInfoFont"></asp:Label></li>
                                            <li id="liPriority" runat="server" clientidmode="Static">Priority:
                                               <asp:Label ID="lblPriority" runat="server" Text="" class="contactInfoFont"></asp:Label></li>
                                           <li id="liNoOfRevision" runat="server" clientidmode="Static" >No. Revisions:
                                               <asp:Label ID="lblNoOfRevision" runat="server" Text="" class="contactInfoFont"></asp:Label></li>
                                           <li>&nbsp;</li>
                                           <li id="liRiskLevel" runat="server" clientidmode="Static">Risk Level: 
                                               <asp:Label ID="lblRiskLevel" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                           </li>
                                           <li id="liRiskNotes" runat="server" clientidmode="Static">Notes:
                                               <asp:Literal ID="litRiskNotes" runat="server" Text=""></asp:Literal>                                                                                        
                                           </li>
                                           <li>&nbsp;</li>
                                       </ul>

                                       <ul class="information-list">
                                          <li class="nested-information-list-wrapper" id="liApplicationWorkflowsWrapper" runat="server" clientidmode="Static">
                                             <ul id="ulApplicationWorkflows" runat="server" class="nested-information-list"></ul>                                    
                                          </li>

                                          <li>
                                             <ul id="ulRelatedWorkflows" runat="server" class="nested-information-list"></ul>
                                          </li>
                                       </ul>
                                       </div>
                                        <div style="float:left; width:210px;" >
                                            <ul id="ulPaymentDetails" runat="server" class="information-list"  >
                                               <li>Payment Details</li>
                                               <li>Fee Received:
                                                   <asp:Label ID="lblFeeProcessed" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                   <asp:Image ID="imgFeeProcessed" runat="server" ImageUrl="/Styles/images/icon-check.png" />
                                               </li>
                                               <li>Fee Amount:
                                                   <asp:Label ID="lblAmount" runat="server" Text="" class="contactInfoFont"></asp:Label></li>
                                               <li>Check #:
                                                   <asp:Label ID="lblCheckPONumber" runat="server" Text="" class="contactInfoFont"></asp:Label></li>
                                               <li>&nbsp;</li>
                                           </ul>
                                                                        
                                           <ul id="ulStructureDetails" runat="server" class="information-list" >
                                               <li>Structure Details</li>
                                               <li id="liCustomerSiteUID" runat="server" clientidmode="Static">Customer Site UID:
                                                   <asp:Label ID="lblGeneralCustomerSiteUID" runat="server"></asp:Label></li>
                                               <li id="liCustomerSiteName" runat="server" clientidmode="Static">Customer Site Name:
                                                   <asp:Label ID="lblCustomerSiteName" runat="server"></asp:Label></li>
                                               <li>Type:
                                                   <asp:Label ID="lblStructureDetails_Type" runat="server" Text="" class="contactInfoFont"></asp:Label></li>
                                               <li>Height:
                                                   <asp:Label ID="lblStructureDetails_Height" runat="server" Text="" class="contactInfoFont"></asp:Label></li>
                                               <li>Rogue Equip:
                                                   <asp:Label ID="lblStructureDetails_RogueEquip" runat="server" Text="" class="contactInfoFont"></asp:Label></li>
                                               <li>Status:
                                                   <asp:Label ID="lblStructureDetails_Status" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                   <asp:Image ID="imgStructureDetails_Construction" runat="server" AlternateText="Construction" CssClass="construction-img" />
                                                   </li>
                                               <li>Struct Owner:
                                                   <asp:Label ID="lblStructureDetails_StructOwner" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                               </li>
                                               <li>Management Status:
                                                   <asp:Label ID="lblStructureDetails_ManagementStatus" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                               </li>                                               
                                               <li>&nbsp;</li>
                                               <li><b>Engineering Market Details</b></li>                                           
                                               <li>Region:
                                                   <asp:Label ID="lblEngineeringDetailsRegion" runat="server" Text="" class="contactInfoFont"></asp:Label></li>
                                               <li>Market:
                                                   <asp:Label ID="lblEngineeringDetailsMarket" runat="server" Text="" class="contactInfoFont"></asp:Label></li>
                                               <li>Submarket:
                                                   <asp:Label ID="lblEngineeringDetailsSubmarket" runat="server" Text="" class="contactInfoFont"></asp:Label></li>
                                               <li>&nbsp;</li>
                                           </ul>

                                       <%----%>
                                       </div>
                                       <div style="float:left; width:210px;">
                                           <ul id="ulTowerAddress" runat="server" class="information-list"  >
                                               <li>Tower Address</li>
                                               <li>
                                                    <asp:HyperLink ID="mybtn" NavigateUrl="javascript:void(0)" Target="_blank" runat="server"> </asp:HyperLink>
                                                    <asp:Label ID="lblAddressNA" runat="server" Visible="false"></asp:Label>
                                                    <br />
                                                    <br />
                                                    <asp:Label ID="lbl_Latitude" runat="server" Text="Latitude, "></asp:Label>
                                                    <asp:Label ID="lbl_Longitude" runat="server" Text="Longitude: "/>                                                    
                                                    <br />
                                                    <asp:HyperLink ID="lblSiteLatLong" Text="" Target="_blank" runat="server">
                                                    </asp:HyperLink>
                                               </li>
                                               <li>&nbsp;</li>
                                            </ul>
                                            <ul id="ulTeamAssigned" runat="server" class="information-list"  >
                                               <li>Team Assigned</li>
                                               <li><asp:label ID="lblInitialSpecialistFront" runat="server">IS:</asp:label>
                                                   <asp:Label ID="lblInitialSpecialist" runat="server" Text="" class="contactInfoFont"></asp:Label></li>
                                               <li><asp:label ID="lblCurrentSpecialistFront" runat="server">CS:</asp:label>
                                                   <asp:Label ID="lblCurrentSpecialist" runat="server" Text="" class="contactInfoFont"></asp:Label></li>
                                               <li><asp:label ID="lblProjectManagerFront" runat="server">PM:</asp:label>
                                                   <asp:Label ID="lblProjectManager" runat="server" Text="" class="contactInfoFont"></asp:Label></li>
                                               <li><asp:label ID="lblColocationCoordinatorFront" runat="server">CC:</asp:label>
                                                   <asp:Label ID="lblColocationCoordinator" runat="server" Text="" class="contactInfoFont"></asp:Label></li>
                                                <li><asp:label ID="lblAccountManagerFront" runat="server">AM:</asp:label>
                                                   <asp:Label ID="lblAccountManager" runat="server" Text="" class="contactInfoFont"></asp:Label></li>
                                               <li>&nbsp;</li>
                                           </ul>
                                       </div>
                                   </div>
                                   <div class="cb"></div>
                               </div>
                               <div class="i-1-3">
                                   <div class="map">
                                       <asp:Image ID="imgTower" width="310" height="235" frameborder="0" scrolling="no" marginheight="0" runat="server" />
                                   </div>
                                   <!-- .map -->
                               </div>
                               <div class="edit-button">
                                  <%--<!-- Button Delete Lease Apps class="edit-button"-->
                                   <a runat="server" id="modalCreateSDPExternal" class="link-button" href="javascript:void(0)" clientidmode="Static" visible="false">Create SDP</a>--%>
                                   <%--<!-- Button Delete Lease Apps class="edit-button"-->
                                   <a runat="server" id="modalCreateTowermodExternal" class="link-button" href="javascript:void(0)" clientidmode="Static" visible="false">Create Tower Mod</a>--%>
                                   <%--<!-- Button Delete Lease Apps CssClass="del-button btn-app-delete"-->
                                   <asp:LinkButton ID="btnAppDelete" class="link-button" OnClientClick="return openDeleteAppButtonPopup('Lease Application',event);" OnClick="btnLADelete_Click" runat="server">delete</asp:LinkButton>--%>
                                   <!-- Button view History class="history-button" -->
                                   <a id="modalHisExternal" class="link-button" href="javascript:void(0)">history</a>
                                   <!-- Button Edit Lease Apps class="edit-button" -->
                                   <a id="modalBtnExternal" class="link-button" href="javascript:void(0)">edit</a>
                                 
                               </div>
                                <!-- Modal -->
                               <div id="modalExternalCreateSDP" class="modal-container" style="overflow:hidden;"></div>
                               <div id="modalExternalCreateTowermod" class="modal-container" style="overflow:hidden;"></div>
                               <div id="modalExternalHistory" class="modal-container" style="overflow:hidden;"></div>
                               <div id="modalExternal" class="modal-container" style="overflow:hidden;"></div>
                               <div id="modalDocument" class="modal-container" style="overflow:hidden;"></div>
                               <div class="cb">
                               </div>
                           </div>
                       </div>
                       <!-- #general-information -->
                       <div id="equipment-information">
                           <div style="overflow:auto;">
                               <div id="tabs3" class="sub-tabs">
                                   <ul>
                                       <li id="summaryLi"><a href="#summary">Summary</a></li>
                                       <li runat="server" id="liAntennas"><a id="aAntennas" href="#antennas">Antennas</a></li>
                                       <li runat="server" id="liAmplifiers"><a id="aAmplifiers" href="#amplifiers">Amplifiers</a></li>
                                       <li runat="server" id="liGps"><a id="aGps" href="#gps">GPS</a></li>
                                       <li runat="server" id="liMicrowave"><a id="aMicrowave" href="#microwave">Microwave Dishes</a></li>
                                       <li runat="server" id="liOther"><a id="aOther" href="#other">Other Equipment</a></li>
                                       <li runat="server" id="liGroundspace"><a id="aGsp" href="#groundspace">Ground Space</a></li>
                                       <li runat="server" id="liComments"><a id="aComments" href="#comments">Comments</a></li>
                                   </ul>
                                   
                                    <div id="divEditEqup" class="related-apps TableGenInfo" style="margin: 20px 20px 10px 20px;
                                        background: #f0f0f0; float:none; width:auto;"  runat="server" visible="false">
                                        <div style="margin: 0 auto 0 auto; width:100%; padding: 35px 0 0 0; text-align: center;
                                            color: #888;">
                                            <em>
                                                <asp:Label ID="lblEditEquipt" runat="server" Text="This application is currently being edited by the customer. The information shown here may not have been re-submitted yet by the customer."></asp:Label>
                                            </em>
                                        </div>
                                    </div>

                                   <div id="divNoEqip" class="related-apps TableGenInfo" style="margin: 20px 20px 20px 20px;
                                        background: #f0f0f0;" runat="server" visible="false">
                                        <div style="margin: 0 auto 0 auto; width:100%; padding: 35px 0 0 0; text-align: center;
                                            color: #888;">
                                            <em>
                                                <asp:Label ID="lblNoEqip" runat="server">No equipment found.</asp:Label>
                                            </em>
                                        </div>
                                    </div>
                                    <asp:HiddenField ID="ApplicationID" runat="server" ClientIDMode="Static" Value="null" />
                                   <div id="summary">
                                       <div style="margin-top:4px;padding:20px;overflow:auto;">
                                           <h4 style="width: 323px;">Equipment Information Summary</h4>
                                           <div class="cb"></div>
                                           <div class="left-col">
                                               <ul id="ul1" runat="server" class="information-list" style="width: 100%; margin: 0;">
                                                   <li></li>
                                                   <li id="liAntennaCount" runat="server" clientidmode="Static">
                                                       <span>Antenna Count:</span>
                                                       <asp:Label ID="lbl_antenna_count" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                   </li>
                                                   <li id="liAntennaDimensions" runat="server" clientidmode="Static">
                                                       <span>Antenna dimensions:</span>
                                                       <asp:Label ID="lbl_antenna_dimensions" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                   </li>
                                                   <li id="liAntennaWeight" runat="server" clientidmode="Static">
                                                       <span>Antenna Weight:</span>
                                                       <asp:Label ID="lbl_antenna_weight" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                   </li>

                                                   <li id="liAntennaRAD" runat="server" clientidmode="Static">
                                                       <span>Antenna RAD:</span>
                                                       <asp:Label ID="lbl_antenna_rad" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                   </li>

                                                   <li id="liAntennaMountType" runat="server" clientidmode="Static">
                                                       <span>Antenna Mount Type:</span>
                                                       <asp:Label ID="lbl_antenna_mount_type" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                   </li>

                                                   <li id="liAntennaMWDiamenter" runat="server" clientidmode="Static">
                                                       <span>MW Diameter:</span>
                                                       <asp:Label ID="lbl_mw_diameter" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                   </li>

                                                   <li id="liAntennaMWRAD" runat="server" clientidmode="Static">
                                                       <span>MW RAD:</span>
                                                       <asp:Label ID="lbl_mw_rad" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                   </li>                                                                                                
                                               </ul>
                                           </div>
                                           <div class="left-col">
                                               <ul id="ul2" runat="server" class="information-list" style="width: 100%; margin: 0;">
                                                   <li></li>
                                                   <li id="liLineCount" runat="server" clientidmode="Static">
                                                       <span>Line Count:</span>
                                                       <asp:Label ID="lbl_line_count" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                   </li>

                                                   <li id="liLineDiameter" runat="server" clientidmode="Static">
                                                       <span>Line Diameter:</span>
                                                       <asp:Label ID="lbl_line_diameter" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                   </li>

                                                   <li id="liLineLength" runat="server" clientidmode="Static">
                                                       <span>Line Length:</span>
                                                       <asp:Label ID="lbl_line_length" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                   </li>

                                                   <li id="liLNARDUCount" runat="server" clientidmode="Static">
                                                       <span>LNA RDU Count:</span>
                                                       <asp:Label ID="lbl_lna_rdu_count" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                   </li>

                                                   <li id="liLNARDUDimensions" runat="server" clientidmode="Static">
                                                       <span>LNA RDU Dimensions:</span>
                                                       <asp:Label ID="lbl_lna_rdu_dimensions" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                   </li>

                                                   <li id="liLNARDUWeight" runat="server" clientidmode="static">
                                                       <span>LNA RDU Weight:</span>
                                                       <asp:Label ID="lbl_lna_rdu_weight" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                   </li>

                                                   <li id="liLNARDURAD" runat="server" clientidmode="Static">
                                                       <span>LNA RDU RAD:</span>
                                                       <asp:Label ID="lbl_lna_rdu_rad" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                   </li>

                                                   <li id="liGroundExpansionRequired" runat="server" clientidmode="static">
                                                       <span>Ground Expansion Required:</span>
                                                       <asp:Label ID="lbl_ground_expansion_required_yn" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                   </li>                                                                                               
                                               </ul>
                                           </div>
                                           <div class="cb"></div>
                                           <div>
                                             <ul class="information-list">
                                                <li id="liOtherEquipment" runat="server" clientidmode="Static" style="width: 940px; font-weight: normal; margin-left: 25px">
                                                      <span>Other Equipment:</span>
                                                      <br />
                                                      <asp:Label ID="lbl_other_equipment" runat="server" Text="" CssClass="information-list"></asp:Label>
                                                </li> 
                                             </ul>
                                           </div>
                                           <div class="cb"></div>
                                       </div>
                                       <div class="cb">
                                       </div>
                                  </div>        
                                    <div id="antennas" runat="server" clientidmode="Static">
                                        <asp:UpdatePanel ID="uppAnt" runat="server" ClientIDMode="Static" UpdateMode="Conditional">
                                            <ContentTemplate>
                                            <asp:HiddenField ID="hidAntJSON" runat="server" ClientIDMode="Static"/>
                                            <asp:HiddenField ID="hidAntCount" runat="server" ClientIDMode="Static"/>
                                            <div id="AntPageCtrl" style="margin-top:4px;padding:20px;">
                                                    <div id="AntPageNumber" class="PanelPageNumber">
                                                        <span id="labAntPageNum" class="textPageNumber">0</span>
                                                        <span class="textPageNumber">of</span> <span id="labAntPageTotal" 
                                                            class="textPageNumber">0</span>
                                                    </div>
                                                    <div id="AntPageControl" class="PanelPageControl">
                                                        <input id="btnAntPrev" type="button" value="Previous" />
                                                        &nbsp;&nbsp;&nbsp;
                                                        <input id="btnAntNext" type="button" value="Next" />
                                                    </div>
                                            </div>
                                            <div class="cb">
                                            </div>
                                            <div class="slideshow2" id="sliAnt">
                                                <div style="margin-top:4px;padding:20px;overflow:auto;">
                                                    <div id="divAntInit" runat="server" clientidmode="Static" class="equip-init-div" style="width:453px;">
                                                        <h4 style="width: 453px;">
                                                            <asp:Label ID="divAntInitLabel" runat="server" Text="Initial Antenna Information" />
                                                        </h4>
                                                        <br />
                                                        <asp:Panel ID="aPanel" runat="server" Width="453px" ScrollBars="Auto">
                                                        <table class="tableData" id="AntennaAreaInit" >
                                                            <tbody>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label22" runat="server" CssClass="equip-prelabel">Quantity</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labAntQuantity_init"   ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label48" runat="server" CssClass="equip-prelabel">RAD Center</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labAntRADCenter_init"   ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label50" runat="server" CssClass="equip-prelabel">Azimuth</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labAntAzimuth_init"   ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label28" runat="server" CssClass="equip-prelabel">Manufacturer</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labAntManufacturer_init"   ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label36" runat="server" CssClass="equip-prelabel">Model</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labAntModel_init"   ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:200px;">
										                                <asp:Label ID="Label40" runat="server" CssClass="equip-prelabel">Weight (LBS.)</asp:Label> 
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labAntWeight_init"   ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label44" runat="server" CssClass="equip-prelabel">Dimensions</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                            <asp:Label runat="server" ID="labAntDimensions_L_init" ClientIDMode="Static"></asp:Label>
                                                                            <asp:Label runat="server" ID="labAntDimensions_L_init_after" ClientIDMode="Static" Text="&#34 x "></asp:Label>
											                                <asp:Label runat="server" ID="labAntDimensions_W_init" ClientIDMode="Static"></asp:Label>
                                                                            <asp:Label runat="server" ID="labAntDimensions_W_init_after" ClientIDMode="Static" Text="&#34 x "></asp:Label>
											                                <asp:Label runat="server" ID="labAntDimensions_H_init" ClientIDMode="Static"></asp:Label>
                                                                            <asp:Label runat="server" ID="labAntDimensions_H_init_after" ClientIDMode="Static" Text="&#34"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label52" runat="server" CssClass="equip-prelabel">Mounting Equipment Type</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labAntMountingType_init" readonly="readonly"  ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label54" runat="server" CssClass="equip-prelabel">Mounting Equipment Dimensions</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labAntMountingDimensions_L_init" ClientIDMode="Static"></asp:Label>
                                                                        <asp:Label runat="server" ID="labAntMountingDimensions_L_init_after" ClientIDMode="Static" Text="&#34 x "></asp:Label>
											                            <asp:Label runat="server" ID="labAntMountingDimensions_W_init" ClientIDMode="Static"></asp:Label>
                                                                        <asp:Label runat="server" ID="labAntMountingDimensions_W_init_after" ClientIDMode="Static" Text="&#34 x "></asp:Label>
											                            <asp:Label runat="server" ID="labAntMountingDimensions_H_init" ClientIDMode="Static"></asp:Label>
                                                                        <asp:Label runat="server" ID="labAntMountingDimensions_H_init_after" ClientIDMode="Static" Text="&#34"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr><td></td></tr>
                                                            </tbody>
                                                        </table>
                                                     
                                                        <h4 style="width: 453px;">
                                                            <asp:Label ID="divAntInitFrequencyLabel" runat="server" Text="Initial Frequency and Power Ranges" />
                                                        </h4>
                                                        <table class="tableData" id="AntennaTransmissionAreaInit" >
                                                            <tbody>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label58" runat="server" CssClass="equip-prelabel">Frequency Tx (MHz)</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labAntTxTransmit_init"   ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label61" runat="server" CssClass="equip-prelabel">Frequency Rx (MHz)</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labAntRxTransmit_init"   ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label63" runat="server" CssClass="equip-prelabel">Power Output (Watts)</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labAntPowerOutput_init"   ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    <br />
                                                        <asp:PlaceHolder ID="AntennaInitPlaceHolder" runat="server" ClientIDMode="Static">
                                                    
                                                        </asp:PlaceHolder>
                                                        </asp:Panel>
                                                    </div>
                                                    <div id="divAntMod" runat="server" clientidmode="Static" class="equip-mod-div">
                                                        <h4 style="width: 453px;"><asp:Label ID="divAntModLabel" runat="server" Text="Proposed Antenna Information" /></h4>
                                                        <br />
                                                        
                                                            <table class="tableData" id="AntennaAreaMod" >
                                                            <tbody>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label12" runat="server" CssClass="equip-prelabel">Quantity</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labAntQuantity_mod"   ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label24" runat="server" CssClass="equip-prelabel">RAD Center</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labAntRADCenter_mod"   ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label30" runat="server" CssClass="equip-prelabel">Azimuth</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labAntAzimuth_mod"   ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label34" runat="server" CssClass="equip-prelabel">Manufacturer</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labAntManufacturer_mod"   ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label38" runat="server" CssClass="equip-prelabel">Model</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labAntModel_mod"   ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:200px;">
										                                <asp:Label ID="Label45" runat="server" CssClass="equip-prelabel">Weight (LBS.)</asp:Label> 
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labAntWeight_mod"   ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label47" runat="server" CssClass="equip-prelabel">Dimensions</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labAntDimensions_L_mod" ClientIDMode="Static"></asp:Label>
                                                                        <asp:Label runat="server" ID="labAntDimensions_L_mod_after" ClientIDMode="Static" Text="&#34 x "></asp:Label>
											                            <asp:Label runat="server" ID="labAntDimensions_W_mod" ClientIDMode="Static"></asp:Label>
                                                                        <asp:Label runat="server" ID="labAntDimensions_W_mod_after" ClientIDMode="Static" Text="&#34 x "></asp:Label>
											                            <asp:Label runat="server" ID="labAntDimensions_H_mod" ClientIDMode="Static"></asp:Label>
                                                                        <asp:Label runat="server" ID="labAntDimensions_H_mod_after" ClientIDMode="Static" Text="&#34"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label51" runat="server" CssClass="equip-prelabel">Mounting Equipment Type</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labAntMountingType_mod" readonly="readonly"  ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label55" runat="server" CssClass="equip-prelabel">Mounting Equipment Dimensions</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labAntMountingDimensions_L_mod" ClientIDMode="Static"></asp:Label>
                                                                        <asp:Label runat="server" ID="labAntMountingDimensions_L_mod_after" ClientIDMode="Static" Text="&#34 x "></asp:Label>
											                            <asp:Label runat="server" ID="labAntMountingDimensions_W_mod" ClientIDMode="Static"></asp:Label>
                                                                        <asp:Label runat="server" ID="labAntMountingDimensions_W_mod_after" ClientIDMode="Static" Text="&#34 x "></asp:Label>
											                            <asp:Label runat="server" ID="labAntMountingDimensions_H_mod" ClientIDMode="Static"></asp:Label>
                                                                        <asp:Label runat="server" ID="labAntMountingDimensions_H_mod_after" ClientIDMode="Static" Text="&#34"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr><td></td></tr>
                                                            </tbody>
                                                        </table>
                                                     
                                                        <h4 style="width: 453px;">
                                                            <asp:Label ID="divModInitFrequencyLabel" runat="server" Text="Proposed Frequency and Power Ranges" />
                                                        </h4>
                                                        <table class="tableData" id="AntennaTransmissionAreaMod" >
                                                            <tbody>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label59" runat="server" CssClass="equip-prelabel">Frequency Tx (MHz)</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labAntTxTransmit_mod"   ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label64" runat="server" CssClass="equip-prelabel">Frequency Rx (MHz)</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labAntRxTransmit_mod"   ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label68" runat="server" CssClass="equip-prelabel">Power Output (Watts)</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labAntPowerOutput_mod"   ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    <br />
                                                        <asp:PlaceHolder ID="AntennaModPlaceHolder" runat="server" ClientIDMode="Static">
                                                        </asp:PlaceHolder>
                                                    </div>
                                                    <div class="cb"></div>
                                                </div>
                                                <div class="cb">
                                                </div>
                                            </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div> 
                                    <div id="amplifiers"  runat="server" clientidmode="Static">
                                        <asp:UpdatePanel ID="uppAmp" runat="server" ClientIDMode="Static" UpdateMode="Conditional">
                                            <ContentTemplate>
                                            <asp:HiddenField ID="hidAmpJSON" runat="server" ClientIDMode="Static"/>
                                            <asp:HiddenField ID="hidAmpCount" runat="server" ClientIDMode="Static"/>
                                            <div id="AmpPageCtrl" style="margin-top:4px;padding:20px;">
                                                    <div id="AmpPanelPageNumber" class="PanelPageNumber">
                                                        <span id="labAmpPageNum" class="textPageNumber">0</span>
                                                        <span class="textPageNumber">of</span> <span id="labAmpPageTotal" 
                                                            class="textPageNumber">0</span>
                                                    </div>
                                                    <div id="AmpPanelPageControl" class="PanelPageControl">
                                                        <input id="btnAmpPrev" type="button" value="Previous" />
                                                        &nbsp;&nbsp;&nbsp;
                                                        <input id="btnAmpNext" type="button" value="Next" />
                                                    </div>
                                            </div>
                                            <div class="cb">
                                            </div>
                                            <div class="slideshow2" id="sliAmp">
                                                <div style="margin-top:4px;padding:20px;overflow:auto;">
                                                    <div id="divAmpInit" runat="server" clientidmode="Static" class="equip-init-div">
                                                        <h4 style="width: 453px;">
                                                            <asp:Label ID="divAmpInitLabel" runat="server" Text="Initial Amplifier Information" />
                                                        </h4>
                                                        <table class="tableData" id="AmpInit">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label16" runat="server" CssClass="equip-prelabel">Quantity</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labAmpQuantity_init"   ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label32" runat="server" CssClass="equip-prelabel">Manufacturer</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labAmpManufacturer_init"   ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label42" runat="server" CssClass="equip-prelabel">Model</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labAmpModel_init"   ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label49" runat="server" CssClass="equip-prelabel">Weight</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labAmpWeight_init"   ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label56" runat="server" CssClass="equip-prelabel">Dimensions</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labAmpDimensions_L_init" ClientIDMode="Static"></asp:Label>
                                                                        <asp:Label runat="server" ID="labAmpDimensions_L_init_after" ClientIDMode="Static" Text="&#34 x "></asp:Label>
											                            <asp:Label runat="server" ID="labAmpDimensions_W_init" ClientIDMode="Static"></asp:Label>
                                                                        <asp:Label runat="server" ID="labAmpDimensions_W_init_after" ClientIDMode="Static" Text="&#34 x "></asp:Label>
											                            <asp:Label runat="server" ID="labAmpDimensions_H_init" ClientIDMode="Static"></asp:Label>
                                                                        <asp:Label runat="server" ID="labAmpDimensions_H_init_after" ClientIDMode="Static" Text="&#34"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div id="divAmpMod" runat="server" ClientIDMode="Static" class="equip-mod-div">
                                                        <h4 style="width: 453px;">
                                                            <asp:Label ID="divAmpModLabel" runat="server" Text="Proposed Amplifier Information" />
                                                        </h4>
                                                        <table class="tableData" id="AmpMod">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label4" runat="server" CssClass="equip-prelabel">Quantity</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labAmpQuantity_mod"   ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label14" runat="server" CssClass="equip-prelabel">Manufacturer</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labAmpManufacturer_mod"   ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label21" runat="server" CssClass="equip-prelabel">Model</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labAmpModel_mod"   ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label37" runat="server" CssClass="equip-prelabel">Weight</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labAmpWeight_mod"   ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label53" runat="server" CssClass="equip-prelabel">Dimensions</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labAmpDimensions_L_mod" ClientIDMode="Static"></asp:Label>
                                                                        <asp:Label runat="server" ID="labAmpDimensions_L_mod_after" ClientIDMode="Static" Text="&#34 x "></asp:Label>
											                            <asp:Label runat="server" ID="labAmpDimensions_W_mod" ClientIDMode="Static"></asp:Label>
                                                                        <asp:Label runat="server" ID="labAmpDimensions_W_mod_after" ClientIDMode="Static" Text="&#34 x "></asp:Label>
											                            <asp:Label runat="server" ID="labAmpDimensions_H_mod" ClientIDMode="Static"></asp:Label>
                                                                        <asp:Label runat="server" ID="labAmpDimensions_H_mod_after" ClientIDMode="Static" Text="&#34"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="cb"></div>
                                                </div>
                                                <div class="cb">
                                                </div>
                                            </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>       
                                    
                                    <div id="gps"  runat="server" clientidmode="Static">
                                        <asp:UpdatePanel ID="uppGPS" runat="server" ClientIDMode="Static" UpdateMode="Conditional">
                                            <ContentTemplate>
                                            <asp:HiddenField ID="hidGPSJSON" runat="server" ClientIDMode="Static"/>
                                            <asp:HiddenField ID="hidGPSCount" runat="server" ClientIDMode="Static"/>
                                            <div id="GPSPageCtrl" style="margin-top:4px;padding:20px;">
                                                   <div class="PanelPageNumber">
                                                       <span id="labGPSPageNum" class="textPageNumber">0</span>
                                                       <span class="textPageNumber">of</span> <span id="labGPSPageTotal" 
                                                           class="textPageNumber">0</span>
                                                   </div>
                                                   <div class="PanelPageControl">
                                                       <input id="btnGPSPrev" type="button" value="Previous" />
                                                       &nbsp;&nbsp;&nbsp;
                                                       <input id="btnGPSNext" type="button" value="Next" />
                                                   </div>
                                            </div>
                                            <div class="cb">
                                            </div>
                                            <div class="slideshow2" id="sliGPS">
                                                <div style="margin-top:4px;padding:20px;overflow:auto;">
                                                   <div id="divGPSInit" runat="server" clientidmode="Static" class="equip-init-div">
                                                        <h4 style="width: 453px;">
                                                            <asp:Label ID="divGPSInitlabel" runat="server" Text="Initial GPS Information" />
                                                        </h4>
                                                        <table class="tableData" id="GPSInit">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label10" runat="server" CssClass="equip-prelabel">Quantity</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labGPSQuantity_init"   ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label26" runat="server" CssClass="equip-prelabel">Manufacturer</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labGPSManufacturer_init"   ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label62" runat="server" CssClass="equip-prelabel">Model</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labGPSModel_init"   ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label70" runat="server" CssClass="equip-prelabel">Installed Location</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labGPSInstalledLocation_init"   ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr id="TowerRad_init" style="display:none">
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label39" runat="server" CssClass="equip-prelabel">RAD Center</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labGPSRADCenter_init" ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr id="TowerWeight_init" style="display:none">
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label29" runat="server" CssClass="equip-prelabel">Weight (LBS.)</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labGPSWeight_init"   ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr id="TowerDimensions_init" style="display:none">
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label20" runat="server" CssClass="equip-prelabel">Dimensions</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                            <asp:Label runat="server" ID="labGPSDimensions_L_init" ClientIDMode="Static"></asp:Label>
                                                                            <asp:Label runat="server" ID="labGPSDimensions_L_init_after" ClientIDMode="Static" Text="&#34 x "></asp:Label>
											                                <asp:Label runat="server" ID="labGPSDimensions_W_init" ClientIDMode="Static"></asp:Label>
                                                                            <asp:Label runat="server" ID="labGPSDimensions_W_init_after" ClientIDMode="Static" Text="&#34 x "></asp:Label>
											                                <asp:Label runat="server" ID="labGPSDimensions_H_init" ClientIDMode="Static"></asp:Label>
                                                                            <asp:Label runat="server" ID="labGPSDimensions_H_init_after" ClientIDMode="Static" Text="&#34"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                   </div>
                                                   <div id="divGPSMod" runat="server" clientidmode="Static" class="equip-mod-div">
                                                        <h4 style="width: 453px;">
                                                            <asp:Label ID="divGPSModLabel" runat="server" Text="Proposed GPS Information" />
                                                        </h4>
                                                        <table class="tableData" id="GPSMod">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label1" runat="server" CssClass="equip-prelabel">Quantity</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labGPSQuantity_mod"   ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label5" runat="server" CssClass="equip-prelabel">Manufacturer</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labGPSManufacturer_mod"   ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label18" runat="server" CssClass="equip-prelabel">Model</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labGPSModel_mod"   ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label66" runat="server" CssClass="equip-prelabel">Installed Location</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labGPSInstalledLocation_mod"   ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr id="TowerRad_mod" style="display:none">
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label65" runat="server" CssClass="equip-prelabel">RAD Center</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labGPSRADCenter_mod" ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr id="TowerWeight_mod" style="display:none">
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label60" runat="server" CssClass="equip-prelabel">Weight (LBS.)</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                        <asp:Label runat="server" ID="labGPSWeight_mod"   ClientIDMode="Static"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr id="TowerDimensions_mod" style="display:none">
                                                                    <td style="width:200px;">
                                                                        <asp:Label ID="Label57" runat="server" CssClass="equip-prelabel">Dimensions</asp:Label>
                                                                    </td>
                                                                    <td style="width:100%;">
                                                                            <asp:Label runat="server" ID="labGPSDimensions_L_mod" ClientIDMode="Static"></asp:Label>
                                                                            <asp:Label runat="server" ID="labGPSDimensions_L_mod_after" ClientIDMode="Static" Text="&#34 x "></asp:Label>
											                                <asp:Label runat="server" ID="labGPSDimensions_W_mod" ClientIDMode="Static"></asp:Label>
                                                                            <asp:Label runat="server" ID="labGPSDimensions_W_mod_after" ClientIDMode="Static" Text="&#34 x "></asp:Label>
											                                <asp:Label runat="server" ID="labGPSDimensions_H_mod" ClientIDMode="Static"></asp:Label>
                                                                            <asp:Label runat="server" ID="labGPSDimensions_H_mod_after" ClientIDMode="Static" Text="&#34"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                   </div>
                                                     <div class="cb"></div>
                                                </div>
                                            </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>    
                                    <div id="microwave"  runat="server" clientidmode="Static">
                                    <asp:UpdatePanel ID="uppMic" runat="server" ClientIDMode="Static" UpdateMode="Conditional">
                                            <ContentTemplate>
                                            <asp:HiddenField ID="hidMicJSON" runat="server" ClientIDMode="Static"/>
                                            <asp:HiddenField ID="hidMicCount" runat="server" ClientIDMode="Static"/>
                                            <div id="MicPageCtrl" style="margin-top:4px;padding:20px;">
                                                   <div class="PanelPageNumber">
                                                       <span id="labMicPageNum" class="textPageNumber">0</span>
                                                       <span class="textPageNumber">of</span> <span id="labMicPageTotal" 
                                                           class="textPageNumber">0</span>
                                                   </div>
                                                   <div class="PanelPageControl">
                                                       <input id="btnMicPrev" type="button" value="Previous" />
                                                       &nbsp;&nbsp;&nbsp;
                                                       <input id="btnMicNext" type="button" value="Next" />
                                                   </div>
                                            </div>
                                            <div class="cb">
                                            </div>
                                            <div class="slideshow2" id="sliMic">
                                        <div style="margin-top:4px;padding:20px;overflow:auto;">
                                            <div id="divMicInit" runat="server" clientidmode="Static" class="equip-init-div">
                                                <h4 style="width: 453px;">
                                                    <asp:Label ID="divMicInitLabel" runat="server" Text="Initial Microwave Information" />
                                                </h4>
                                                <table class="tableData" id="MicInit">
                                                    <tbody>
                                                        <tr>
                                                            <td style="width:200px;">
                                                                <asp:Label ID="Label3" runat="server" CssClass="equip-prelabel">Quantity</asp:Label>
                                                            </td>
                                                            <td style="width:100%;">
                                                                <asp:Label runat="server" ID="labMicQuantity_init"   ClientIDMode="Static"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:200px;">
                                                                <asp:Label ID="Label46" runat="server" CssClass="equip-prelabel">RAD Center</asp:Label>
                                                            </td>
                                                            <td style="width:100%;">
                                                                <asp:Label runat="server" ID="labMicRADCenter_init"   ClientIDMode="Static"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:200px;">
                                                                <asp:Label ID="Label77" runat="server" CssClass="equip-prelabel">Azimuth</asp:Label>
                                                            </td>
                                                            <td style="width:100%;">
                                                                <asp:Label runat="server" ID="labMicAzimuth_init"   ClientIDMode="Static"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:200px;">
                                                                <asp:Label ID="Label81" runat="server" CssClass="equip-prelabel">Manufacturer</asp:Label>
                                                            </td>
                                                            <td style="width:100%;">
                                                                <asp:Label runat="server" ID="labMicManufacturer_init"   ClientIDMode="Static"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:200px;">
                                                                <asp:Label ID="Label85" runat="server" CssClass="equip-prelabel">Model</asp:Label>
                                                            </td>
                                                            <td style="width:100%;">
                                                                <asp:Label runat="server" ID="labMicModel_init"   ClientIDMode="Static"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:200px;">
                                                                <asp:Label ID="Label89" runat="server" CssClass="equip-prelabel">Weight</asp:Label>
                                                            </td>
                                                            <td style="width:100%;">
                                                                <asp:Label runat="server" ID="labMicWeight_init"   ClientIDMode="Static"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:200px;">
                                                                <asp:Label ID="Label91" runat="server" CssClass="equip-prelabel">Diameter/Size</asp:Label>
                                                            </td>
                                                            <td style="width:100%;">
                                                                <asp:Label runat="server" ID="labMicDiameter_init"   ClientIDMode="Static"></asp:Label>
                                                            </td>
                                                        </tr>
                                                         <tr>
                                                            <td style="width:200px;">
                                                                <asp:Label ID="Label87" runat="server" CssClass="equip-prelabel">Mounting Equipment Type</asp:Label>
                                                            </td>
                                                            <td style="width:100%;">
                                                                <asp:Label runat="server" ID="labMicMounting_init"   ClientIDMode="Static"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                           </div>  
                                           <div id="divMicMod" runat="server" clientidmode="Static" class="equip-mod-div">
                                                <h4 style="width: 453px;">
                                                    <asp:Label ID="divMicModlabel" runat="server" Text="Proposed Microwave Information" />
                                                </h4>
                                                <table class="tableData" id="MicMod">
                                                    <tbody>
                                                        <tr>
                                                            <td style="width:200px;">
                                                                <asp:Label ID="Label2" runat="server" CssClass="equip-prelabel">Quantity</asp:Label>
                                                            </td>
                                                            <td style="width:100%;">
                                                                <asp:Label runat="server" ID="labMicQuantity_mod"   ClientIDMode="Static"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:200px;">
                                                                <asp:Label ID="Label7" runat="server" CssClass="equip-prelabel">RAD Center</asp:Label>
                                                            </td>
                                                            <td style="width:100%;">
                                                                <asp:Label runat="server" ID="labMicRADCenter_mod"   ClientIDMode="Static"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:200px;">
                                                                <asp:Label ID="Label11" runat="server" CssClass="equip-prelabel">Azimuth</asp:Label>
                                                            </td>
                                                            <td style="width:100%;">
                                                                <asp:Label runat="server" ID="labMicAzimuth_mod"   ClientIDMode="Static"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:200px;">
                                                                <asp:Label ID="Label15" runat="server" CssClass="equip-prelabel">Manufacturer</asp:Label>
                                                            </td>
                                                            <td style="width:100%;">
                                                                <asp:Label runat="server" ID="labMicManufacturer_mod"   ClientIDMode="Static"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:200px;">
                                                                <asp:Label ID="Label19" runat="server" CssClass="equip-prelabel">Model</asp:Label>
                                                            </td>
                                                            <td style="width:100%;">
                                                                <asp:Label runat="server" ID="labMicModel_mod"   ClientIDMode="Static"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:200px;">
                                                                <asp:Label ID="Label79" runat="server" CssClass="equip-prelabel">Weight</asp:Label>
                                                            </td>
                                                            <td style="width:100%;">
                                                                <asp:Label runat="server" ID="labMicWeight_mod"   ClientIDMode="Static"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:200px;">
                                                                <asp:Label ID="Label86" runat="server" CssClass="equip-prelabel">Diameter/Size</asp:Label>
                                                            </td>
                                                            <td style="width:100%;">
                                                                <asp:Label runat="server" ID="labMicDiameter_mod"   ClientIDMode="Static"></asp:Label>
                                                            </td>
                                                        </tr>
                                                         <tr>
                                                            <td style="width:200px;">
                                                                <asp:Label ID="Label90" runat="server" CssClass="equip-prelabel">Mounting Equipment Type</asp:Label>
                                                            </td>
                                                            <td style="width:100%;">
                                                                <asp:Label runat="server" ID="labMicMounting_mod"   ClientIDMode="Static"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                           </div>
                                           </div>
                                           </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        </div> 

                                    <div id="other"  runat="server" clientidmode="Static">
                                        <asp:UpdatePanel ID="uppOth" runat="server" ClientIDMode="Static" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:HiddenField ID="hidOthJSON" runat="server" ClientIDMode="Static"/>
                                                <asp:HiddenField ID="hidOthCount" runat="server" ClientIDMode="Static"/>
                                                <div id="OthPageCtrl" style="margin-top:4px;padding:20px;">
                                                       <div class="PanelPageNumber">
                                                           <span id="labOthPageNum" class="textPageNumber">0</span>
                                                           <span class="textPageNumber">of</span> <span id="labOthPageTotal" 
                                                               class="textPageNumber">0</span>
                                                       </div>
                                                       <div class="PanelPageControl">
                                                           <input id="btnOthPrev" type="button" value="Previous" />
                                                           &nbsp;&nbsp;&nbsp;
                                                           <input id="btnOthNext" type="button" value="Next" />
                                                       </div>
                                                </div>
                                                <div class="cb">
                                                </div>
                                                <div class="slideshow2" id="sliOth">
                                                    <div style="margin-top:4px;padding:20px;overflow:auto;">
                                                        <div id="divOthInit" runat="server" clientidmode="Static" class="equip-init-div">
                                                            <h4 style="width: 453px;">
                                                                <asp:Label ID="divOthInitLabel" runat="server" Text="Initial Other Equipment Information" />
                                                            </h4>
                                                            <table class="tableData" id="OthInit">
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="width:200px;">
                                                                            <asp:Label ID="Label6" runat="server" CssClass="equip-prelabel">Quantity</asp:Label>
                                                                        </td>
                                                                        <td style="width:100%;">
                                                                            <asp:Label runat="server" ID="labOthQuantity_init"   ClientIDMode="Static"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:200px;">
                                                                            <asp:Label ID="Label13" runat="server" CssClass="equip-prelabel">RAD Center</asp:Label>
                                                                        </td>
                                                                        <td style="width:100%;">
                                                                            <asp:Label runat="server" ID="labOthRADCenter_init"   ClientIDMode="Static"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:200px;">
                                                                            <asp:Label ID="Label74" runat="server" CssClass="equip-prelabel">Azimuth</asp:Label>
                                                                        </td>
                                                                        <td style="width:100%;">
                                                                            <asp:Label runat="server" ID="labOthAzimuth_init"   ClientIDMode="Static"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:200px;">
                                                                            <asp:Label ID="Label88" runat="server" CssClass="equip-prelabel">Manufacturer</asp:Label>
                                                                        </td>
                                                                        <td style="width:100%;">
                                                                            <asp:Label runat="server" ID="labOthManufacturer_init"   ClientIDMode="Static"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:200px;">
                                                                            <asp:Label ID="Label93" runat="server" CssClass="equip-prelabel">Model</asp:Label>
                                                                        </td>
                                                                        <td style="width:100%;">
                                                                            <asp:Label runat="server" ID="labOthModel_init"   ClientIDMode="Static"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:200px;">
                                                                            <asp:Label ID="Label95" runat="server" CssClass="equip-prelabel">Weight (LBS.)</asp:Label>
                                                                        </td>
                                                                        <td style="width:100%;">
                                                                            <asp:Label runat="server" ID="labOthWeight_init"   ClientIDMode="Static"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:200px;">
                                                                            <asp:Label ID="Label97" runat="server" CssClass="equip-prelabel">Dimensions</asp:Label>
                                                                        </td>
                                                                        <td style="width:100%;">
                                                                            <asp:Label runat="server" ID="labOthDimensions_L_init" ClientIDMode="Static"></asp:Label>
                                                                            <asp:Label runat="server" ID="labOthDimensions_L_init_after" ClientIDMode="Static" Text="&#34 x "></asp:Label>
											                                <asp:Label runat="server" ID="labOthDimensions_W_init" ClientIDMode="Static"></asp:Label>
                                                                            <asp:Label runat="server" ID="labOthDimensions_W_init_after" ClientIDMode="Static" Text="&#34 x "></asp:Label>
											                                <asp:Label runat="server" ID="labOthDimensions_H_init" ClientIDMode="Static"></asp:Label>
                                                                            <asp:Label runat="server" ID="labOthDimensions_H_init_after" ClientIDMode="Static" Text="&#34"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:200px;">
                                                                            <asp:Label ID="Label99" runat="server" CssClass="equip-prelabel">Mounting Equipment Type</asp:Label>
                                                                        </td>
                                                                        <td style="width:100%;">
                                                                            <asp:Label runat="server" ID="labOthMountingType_init"   ClientIDMode="Static"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:200px;">
                                                                            <asp:Label ID="Label101" runat="server" CssClass="equip-prelabel">Mounting Equipment Dimensions</asp:Label>
                                                                        </td>
                                                                        <td style="width:100%;">
                                                                            <asp:Label runat="server" ID="labOthMountEquipDimensionL_init" ClientIDMode="Static"></asp:Label>
                                                                            <asp:Label runat="server" ID="labOthMountEquipDimensionL_init_after" ClientIDMode="Static" Text="&#34 x "></asp:Label>
											                                <asp:Label runat="server" ID="labOthMountEquipDimensionW_init" ClientIDMode="Static"></asp:Label>
                                                                            <asp:Label runat="server" ID="labOthMountEquipDimensionW_init_after" ClientIDMode="Static" Text="&#34 x "></asp:Label>
											                                <asp:Label runat="server" ID="labOthMountEquipDimensionH_init" ClientIDMode="Static"></asp:Label>
                                                                            <asp:Label runat="server" ID="labOthMountEquipDimensionH_init_after" ClientIDMode="Static" Text="&#34"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:200px;">
                                                                            <asp:Label ID="Label103" runat="server" CssClass="equip-prelabel">Mounting Location</asp:Label>
                                                                        </td>
                                                                        <td style="width:100%;">
                                                                            <asp:Label runat="server" ID="labOthMountingLocation_init"   ClientIDMode="Static"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:200px;">
                                                                            <asp:Label ID="Label105" runat="server" CssClass="equip-prelabel">Equipment Purpose</asp:Label>
                                                                        </td>
                                                                        <td style="width:100%;">
                                                                            <asp:Label runat="server" ID="labOthEquipmentPurpose_init"   ClientIDMode="Static"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:200px;">
                                                                            <asp:Label ID="Label107" runat="server" CssClass="equip-prelabel">Equipment Notes</asp:Label>
                                                                        </td>
                                                                        <td style="width:100%;">
                                                                            <asp:Label runat="server" ID="labOthEquipmentNotes_init"   ClientIDMode="Static"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
									                    </div>
                                                        <div id="divOthMod" runat="server" clientidmode="Static" class="equip-mod-div">
                                                            <h4 style="width: 453px;">
                                                                <asp:Label ID="divOthModLabel" runat="server" Text="Proposed Other Equipment Information" />
                                                            </h4>
                                                            <table class="tableData" id="OthMod">
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="width:200px;">
                                                                            <asp:Label ID="Label8" runat="server" CssClass="equip-prelabel">Quantity</asp:Label>
                                                                        </td>
                                                                        <td style="width:100%;">
                                                                            <asp:Label runat="server" ID="labOthQuantity_mod"   ClientIDMode="Static"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:200px;">
                                                                            <asp:Label ID="Label17" runat="server" CssClass="equip-prelabel">RAD Center</asp:Label>
                                                                        </td>
                                                                        <td style="width:100%;">
                                                                            <asp:Label runat="server" ID="labOthRADCenter_mod"   ClientIDMode="Static"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:200px;">
                                                                            <asp:Label ID="Label23" runat="server" CssClass="equip-prelabel">Azimuth</asp:Label>
                                                                        </td>
                                                                        <td style="width:100%;">
                                                                            <asp:Label runat="server" ID="labOthAzimuth_mod"   ClientIDMode="Static"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:200px;">
                                                                            <asp:Label ID="Label27" runat="server" CssClass="equip-prelabel">Manufacturer</asp:Label>
                                                                        </td>
                                                                        <td style="width:100%;">
                                                                            <asp:Label runat="server" ID="labOthManufacturer_mod"   ClientIDMode="Static"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:200px;">
                                                                            <asp:Label ID="Label31" runat="server" CssClass="equip-prelabel">Model</asp:Label>
                                                                        </td>
                                                                        <td style="width:100%;">
                                                                            <asp:Label runat="server" ID="labOthModel_mod"   ClientIDMode="Static"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:200px;">
                                                                            <asp:Label ID="Label35" runat="server" CssClass="equip-prelabel">Weight (LBS.)</asp:Label>
                                                                        </td>
                                                                        <td style="width:100%;">
                                                                            <asp:Label runat="server" ID="labOthWeight_mod"   ClientIDMode="Static"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:200px;">
                                                                            <asp:Label ID="Label41" runat="server" CssClass="equip-prelabel">Dimensions</asp:Label>
                                                                        </td>
                                                                        <td style="width:100%;">
                                                                            <asp:Label runat="server" ID="labOthDimensions_L_mod" ClientIDMode="Static"></asp:Label>
                                                                            <asp:Label runat="server" ID="labOthDimensions_L_mod_after" ClientIDMode="Static" Text="&#34 x "></asp:Label>
											                                <asp:Label runat="server" ID="labOthDimensions_W_mod" ClientIDMode="Static"></asp:Label>
                                                                            <asp:Label runat="server" ID="labOthDimensions_W_mod_after" ClientIDMode="Static" Text="&#34 x "></asp:Label>
											                                <asp:Label runat="server" ID="labOthDimensions_H_mod" ClientIDMode="Static"></asp:Label>
                                                                            <asp:Label runat="server" ID="labOthDimensions_H_mod_after" ClientIDMode="Static" Text="&#34"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:200px;">
                                                                            <asp:Label ID="Label83" runat="server" CssClass="equip-prelabel">Mounting Equipment Type</asp:Label>
                                                                        </td>
                                                                        <td style="width:100%;">
                                                                            <asp:Label runat="server" ID="labOthMountingType_mod"   ClientIDMode="Static"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:200px;">
                                                                            <asp:Label ID="Label94" runat="server" CssClass="equip-prelabel">Mounting Equipment Dimensions</asp:Label>
                                                                        </td>
                                                                        <td style="width:100%;">
                                                                            <asp:Label runat="server" ID="labOthMountEquipDimensionL_mod" ClientIDMode="Static"></asp:Label>
                                                                            <asp:Label runat="server" ID="labOthMountEquipDimensionL_mod_after" ClientIDMode="Static" Text="&#34 x "></asp:Label>
											                                <asp:Label runat="server" ID="labOthMountEquipDimensionW_mod" ClientIDMode="Static"></asp:Label>
                                                                            <asp:Label runat="server" ID="labOthMountEquipDimensionW_mod_after" ClientIDMode="Static" Text="&#34 x "></asp:Label>
											                                <asp:Label runat="server" ID="labOthMountEquipDimensionH_mod" ClientIDMode="Static"></asp:Label>
                                                                            <asp:Label runat="server" ID="labOthMountEquipDimensionH_mod_after" ClientIDMode="Static" Text="&#34"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:200px;">
                                                                            <asp:Label ID="Label102" runat="server" CssClass="equip-prelabel">Mounting Location</asp:Label>
                                                                        </td>
                                                                        <td style="width:100%;">
                                                                            <asp:Label runat="server" ID="labOthMountingLocation_mod"   ClientIDMode="Static"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:200px;">
                                                                            <asp:Label ID="Label106" runat="server" CssClass="equip-prelabel">Equipment Purpose</asp:Label>
                                                                        </td>
                                                                        <td style="width:100%;">
                                                                            <asp:Label runat="server" ID="labOthEquipmentPurpose_mod"   ClientIDMode="Static"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:200px;">
                                                                            <asp:Label ID="Label109" runat="server" CssClass="equip-prelabel">Equipment Notes</asp:Label>
                                                                        </td>
                                                                        <td style="width:100%;">
                                                                            <asp:Label runat="server" ID="labOthEquipmentNotes_mod"   ClientIDMode="Static"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
									                    </div>
                                                    </div> 
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>  
                                    <div id="groundspace"  runat="server" clientidmode="Static">
                                        <asp:UpdatePanel ID="uppGsp" runat="server" ClientIDMode="Static" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:HiddenField ID="hidGspJSON" runat="server" ClientIDMode="Static"/>
                                                <asp:HiddenField ID="hidGspCount" runat="server" ClientIDMode="Static"/>
                                                <div class="cb">
                                                </div>
                                                <div class="slideshow2" id="sliGsp">
                                                    <div id="GspPageCtrl" style="margin-top:4px;padding:20px;overflow:auto;">
                                                        <div id="divGspInit" runat="server" clientidmode="Static" class="equip-gsi-div">
                                                            <h4 style="width: 800px;">
                                                                <asp:Label ID="divGspInitLabel" runat="server" Text="Ground Space Information" />
                                                            </h4>
                                                            <table class="tableData" id="GspInit">
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="width:200px;">
                                                                            <asp:Label ID="Label9" runat="server" CssClass="equip-prelabel">Width (feet)</asp:Label>
                                                                        </td>
                                                                        <td style="width:100%;">
                                                                            <asp:Label runat="server" ID="labGspDimensions_W_init"   ClientIDMode="Static"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:200px;">
                                                                            <asp:Label ID="Label25" runat="server" CssClass="equip-prelabel">Length (feet)</asp:Label>
                                                                        </td>
                                                                        <td style="width:100%;">
                                                                            <asp:Label runat="server" ID="labGspDimensions_L_init"   ClientIDMode="Static"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:200px;">
                                                                            <asp:Label ID="Label33" runat="server" CssClass="equip-prelabel">Manufacturer</asp:Label>
                                                                        </td>
                                                                        <td style="width:100%;">
                                                                            <asp:Label runat="server" ID="labGspManufacturer_init"   ClientIDMode="Static"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:200px;">
                                                                            <asp:Label ID="Label43" runat="server" CssClass="equip-prelabel">Size</asp:Label>
                                                                        </td>
                                                                        <td style="width:100%;">
                                                                            <asp:Label runat="server" ID="labGspSize_init"   ClientIDMode="Static"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:200px;">
                                                                            <asp:Label ID="Label96" runat="server" CssClass="equip-prelabel">Modulation Type</asp:Label>
                                                                        </td>
                                                                        <td style="width:100%;">
                                                                            <asp:Label runat="server" ID="labGspTypeOfModulation_init"   ClientIDMode="Static"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:200px;">
                                                                            <asp:Label ID="Label100" runat="server" CssClass="equip-prelabel">Duplexer Bandwidth -3DB (MHz)</asp:Label>
                                                                        </td>
                                                                        <td style="width:100%;">
                                                                            <asp:Label runat="server" ID="labGspThreedB_init"   ClientIDMode="Static"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:200px;">
                                                                            <asp:Label ID="Label108" runat="server" CssClass="equip-prelabel">Duplexer Bandwidth -20DB (MHz)</asp:Label>
                                                                        </td>
                                                                        <td style="width:100%;">
                                                                            <asp:Label runat="server" ID="labGspTwentydB_init"   ClientIDMode="Static"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
									                    </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div> 
                                    <div id="comments"  runat="server" clientidmode="Static">
                                       <div style="padding:20px;">
									<div class="full-col">
										<h4>Additional Comments</h4>
										<asp:Label runat="server" ID="labEquipmentComments" ></asp:Label>		
									</div><!-- .left-col -->
									
									<%--<div class="full-col align-right"><input type="submit" value="Save" class="ui-button ui-widget ui-state-default ui-corner-all" role="button" aria-disabled="false"></div>--%>

									<div class="cb"></div>
							
							</div>
                                    </div>              
                               </div>
                               <div class="edit-button">
                                   <a id="equipmentHistory" class="link-button" href="javascript:void(0)">history</a>
                                   <a id="modalBtnSummary" class="link-button" href="javascript:void(0)">edit</a>
                               </div>
                               <!-- Modal -->
                               <div id="modalExternalSummary" class="modal-container" style="overflow:hidden;"></div>
                               <!-- /Modal -->
                               <div class="cb"></div>
                           </div>
                       </div>
                       <!-- #equipment-information -->
                      <div id="customer_contacts" runat="server" clientidmode="Static" >
                           <div id="customer_contacts_with_no_TMO_Application" runat="server" style="padding:20px;overflow:auto;">
                               <div class="left-col">
                                   <h4>Site Acquisition Contact Info</h4>
                                   <ul id="ulSiteAcquisitionContactInfo" runat="server" class="information-list" style="width: 100%; margin: 0;">
                                       <li></li>
                                       <li>
                                           <span>Name:</span>
                                           <asp:Label ID="lblCustomer_Name" runat="server" Text="" class="contactInfoFont"></asp:Label></li>
                                       <li>
                                           <span>Phone:</span>
                                           <asp:Label ID="lblCustomer_Phone" runat="server" Text="" class="contactInfoFont"></asp:Label></li>
                                       <li>
                                           <span>Email:</span>
                                           <asp:Label ID="lblCustomer_Email" runat="server" Text="" class="contactInfoFont"></asp:Label></li>
                                       <li>
                                           <span>Company:</span>
                                           <asp:Label ID="lblCustomer_Company" runat="server" Text="" class="contactInfoFont"></asp:Label></li>
                                   </ul>
                               </div>

                               <div class="left-col">
                                   <h4>Site Acquisition Company Address</h4>
                                   <ul id="ulSiteAcquisitionCompanyAddress" runat="server" class="information-list" style="width: 100%; margin: 0;">
                                       <li></li>
                                       <li>
                                           <span>Street Address:</span>
                                           <asp:Label ID="lblCustomer_StreetAddress" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                       </li>
                                       <li>
                                           <span>City:</span>
                                           <asp:Label ID="lblCustomer_City" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                       </li>
                                       <li>
                                           <span>State:</span>
                                           <asp:Label ID="lblCustomer_State" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                       </li>
                                       <li>
                                           <span>Zip:</span>
                                           <asp:Label ID="lblCustomer_Zip" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                       </li>
                                   </ul>
                               </div>

                               <a id="modalBtnCustomerContacts" class="edit-button" href="javascript:void(0)">edit</a>
                               <!-- Modal -->
                               <div id="modalExternalCustomerContacts" class="modal-container" style="overflow:hidden;"></div>
                               <!-- /Modal -->
                               <div class="cb"></div>
                           </div>
                           <div id="customer_contacts_with_TMO_Application" runat="server" style="padding:20px;overflow:auto;">
                               <div class="left-col-equip">
                               <div id="divCustomerInformation" runat="server" style="height:300px;">
                                    <h4>Customer Information</h4>
                                    <br />
                                    <table class="tableData" runat="server">
                                        <tbody>
                                            <tr>
                                                <td style="width:150px;">
                                                    <span class="contactInfoFont" >Submitter Title </span>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblCustomerInformationSubmitterTitle" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:150px;">
                                                    <span class="contactInfoFont" >Legal Entity Name</span>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblCustomerInformationLegalEntityName" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:150px;">
                                                     <span class="contactInfoFont" >DBA</span>
                                                </td>
                                                <td>
                                                   <asp:Label ID="lblCustomerInformationDBA" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:150px;">
                                                     <span class="contactInfoFont" >Customer Site Number</span>
                                                </td>
                                                <td>
                                                   <asp:Label ID="lblCustomerInformationCustomerSiteNumber" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:150px;">
                                                    <span class="contactInfoFont" >FCC Licensed</span>
                                                </td>
                                                <td>
                                                   <asp:Label ID="lblCustomerInformationFCCLicensed" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr runat="server" id="FCC_Licensed_number" visible="false">
                                                <td style="width:150px;">
                                                    <span id="li_FCC_Licensed_number" runat="server" style="font-weight:bold" class="contactInfoFont">FCC Licensed Number</span>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblCustomerInformationFCCLicensedNumber" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:150px;">
                                                  <span class="contactInfoFont" >Applicant Address</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="CustomerInformationApplicantAddress" runat="server" colspan="2">
                                                    <asp:Label ID="lblCustomerInformationApplicantAddress" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="CustomerInformationCityAndStateAndZip" runat="server" colspan="2">
                                                    <asp:Label ID="lblCustomerInformationCityAndStateAndZip" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:Label ID="lblCustomerInformationCountry" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr><td></td></tr>
                                        </tbody>
                                   </table>
                                </div>
                                   
                                <div id="divCustomerDevelopmentManager" runat="server">
                                    <h4>Customer Development Manager</h4>
                                    <br />
                                    <table class="tableData" runat="server">
                                        <tbody>
                                            <tr>
                                                <td style="width:50px;">
                                                     <span class="contactInfoFont" >Name</span>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblCustomerDevelopmentManagerName" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:50px;">
                                                     <span class="contactInfoFont" >Phone</span>  
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblCustomerDevelopmentManagerPhone" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                    <asp:Label ID="lblCustomerDevelopmentManagerPhoneX" runat="server" Visible="false" Text=" x "></asp:Label>
                                                    <asp:Label ID="lblCustomerDevelopmentManagerPhoneExtension" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:50px;">
                                                     <span class="contactInfoFont" >Fax</span>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblCustomerDevelopmentManagerFax" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:50px;">
                                                     <span class="contactInfoFont" >Mobile</span>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblCustomerDevelopmentManagerMobile" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:50px;">
                                                     <span class="contactInfoFont" >Email</span>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblCustomerDevelopmentManagerEmail" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr><td></td></tr>
                                        </tbody>
                                    </table>
                                </div>
                                
                                <div id="divCustomerContact" runat="server">
                                   <h4>Customer Contact</h4>
                                   <br />
                                    <table id="Table2" class="tableData" runat="server">
                                        <tbody>
                                            <tr>
                                                <td style="width:50px;">
                                                      <span class="contactInfoFont" >Name</span>
                                                </td>
                                                <td>
                                                   <asp:Label ID="lblCustomerContactName" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:50px;">
                                                     <span class="contactInfoFont" >Phone</span>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblCustomerContactPhone" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                    <asp:Label ID="lblCustomerContactPhoneX" runat="server" Visible="false" Text=" x "></asp:Label>
                                                    <asp:Label ID="lblCustomerContactPhoneExtension" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:50px;">
                                                     <span class="contactInfoFont" >Fax</span>
                                                </td>
                                                <td>
                                                   <asp:Label ID="lblCustomerContactFax" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:50px;">
                                                     <span class="contactInfoFont" >Mobile</span>
                                                </td>
                                                <td>
                                                   <asp:Label ID="lblCustomerContactMobile" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:50px;">
                                                     <span class="contactInfoFont" >Email</span>
                                                </td>
                                                <td>
                                                   <asp:Label ID="lblCustomerContactEmail" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                               </div>

                               <div class="right-col">
                                    <div id="divBillingContact" runat="server" style="height:300px;">
                                       <h4>Billing Contact</h4>
                                       <br />
                                        <table id="Table1" class="tableData" runat="server">
                                            <tbody>
                                                <tr>
                                                    <td style="width:50px;">
                                                         <span class="contactInfoFont" >Name</span>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblBillingName" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:50px;">
                                                         <span class="contactInfoFont" >Phone</span>
                                                        
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblBillingPhone" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                        <asp:Label ID="lblBillingPhonex" runat="server" Visible="false" Text=" x "></asp:Label>
                                                        <asp:Label ID="lblBillingPhoneExtension" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:50px;">
                                                         <span class="contactInfoFont" >Fax</span>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblBillingFax" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:50px;">
                                                         <span class="contactInfoFont" >Mobile</span>    
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblBillingMobile" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:50px;">
                                                         <span class="contactInfoFont" >Email</span>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblBillingEmail" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                         <span class="contactInfoFont" >Billing Address</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td id="BillingAddress" runat="server" colspan="2">
                                                        <asp:Label ID="lblBillingAddress" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td id="BillingCityAndStateAndZip" runat="server" colspan="2">
                                                        <asp:Label ID="lblBillingCityAndStateAndZip" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <asp:Label ID="lblBillingCountry" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr><td></td></tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div id="divSiteAcquisitionContact" runat="server">
                                       <h4>Site Acquisition Contact</h4>
                                       <br />
                                       <table class="tableData" runat="server">
                                            <tbody>
                                                <tr>
                                                    <td style="width:50px;">
                                                          <span class="contactInfoFont" >Name</span>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblSiteAcquisitionContactName" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:50px;">
                                                         <span class="contactInfoFont" >Phone</span>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblSiteAcquisitionContactPhone" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                        <asp:Label ID="lblSiteAcquisitionContactPhoneX" runat="server" Visible="false" Text=" x "></asp:Label>
                                                        <asp:Label ID="lblSiteAcquisitionContactPhoneExtension" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:50px;">
                                                         <span class="contactInfoFont" >Fax</span>
                                                    </td>
                                                    <td>
                                                       <asp:Label ID="lblSiteAcquisitionContactFax" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:50px;">
                                                         <span class="contactInfoFont" >Mobile</span>
                                                    </td>
                                                    <td>
                                                       <asp:Label ID="lblSiteAcquisitionContactMobile" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:50px;">
                                                         <span class="contactInfoFont" >Email</span>
                                                    </td>
                                                    <td>
                                                       <asp:Label ID="lblSiteAcquisitionContactEmail" runat="server" Text="" class="contactInfoFont"></asp:Label>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                 
                                   </div>
                               </div>
                           </div>
                           <div class="cb">
                           </div>
                       </div>
                       <!-- #customer_contacts -->

                       <!-- #aDocument -->
                       <div id="aDocuments" runat="server" clientidmode="Static" >
                            <div style="overflow:hidden;">
                               <div class="sub-tabs ui-tabs ui-widget ui-widget-content ui-corner-all" >
                                  <ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all"  >
                                      <li id="liApplicationDocument" class="DocumentButtonActive" >
                                            <a >Application Documents</a>
                                      </li>
                                      <li id="liCustomerDocument" class="DocumentButton" >
                                            <a  >Customer Documents</a>
                                      </li>
                                      <li id="liSiteDocument"   class="DocumentButton" >
                                            <a target="_blank"   >Site Documents &raquo;</a>
                                      </li>
                                   </ul>
                               </div>
                                                                  
                                
	                            <h1 id="h1" style="height:25px;padding-right:30px;width:0px;"></h1>
                                <!-- DMS Grid -->
                                <div class="site-list" id="dms_grid_section" runat="server" clientidmode="Static" style="width:944px;overflow:visible; margin-bottom: 20px;margin-left:20px;margin-right:20px;position:relative;">
                                    <fx:Flexigrid ID="fgDMS"
                                        AutoLoadData="false"
                                        Width="944"
                                        ResultsPerPage="20"
		                                ShowPager="true"
                                        Resizable="false"
                                        ShowToggleButton="false"
                                        ShowTableToggleButton="false"
                                        SearchEnabled="false"
                                        UseCustomTheme="true"
                                        CssClass="tmobile"
                                        WrapCellText="true"
                                        DoNotIncludeJQuery="true"
                                        OnClientBeforeSendData="fgDMSBeforeSendData"
                                        OnClientDataLoad="fgDMSDataLoad"
                                        OnClientRowClick="fgDMSRowClick"
                                        OnClientNoDataLoad="fgDMSNoData"
                                        OnClientColumnToggle="fgColumnChange"
		                                HandlerUrl="~/ApplicationDocuments.axd"
                                        runat="server">
		                                <Columns>
			                                <fx:FlexiColumn Code="Option" Text=" " Width="25" />
			                                <fx:FlexiColumn Code="FileName" Text="Name" Width="177"   />
                                            <fx:FlexiColumn Code="Metadata.Classification" Text="Classification" Width="160"  />
                                            <fx:FlexiColumn Code="Metadata.Keywords" Text="Keywords" Width="120" IsVisible="False"  />
                                            <fx:FlexiColumn Code="Metadata.Description" Text="Description" Width="157" />                        
                                            <fx:FlexiColumn Code="ItemObject.Size" Text="Size" Width="70"  />
                                            <fx:FlexiColumn Code="Metadata.UploadedBy" Text="Uploaded By" Width="100"    />
                                            <fx:FlexiColumn Code="Revision" Text="Rev" Width="30"   />
                                            <fx:FlexiColumn Code="Metadata.UploadDate" Text="Upload Date" Width="90"  />
                                            <fx:FlexiColumn Code="Metadata.isCheckedOut" Text=" " Width="32"   />
                                            <fx:FlexiColumn Code="actions" Text="Actions" Width="90"  />
		                                </Columns>
	                                </fx:Flexigrid>

                                </div>
                                <!-- DMS Grid -->
                                <!-- DMS Unauthorized Access -->
                               <div class="site-list" id="dms_grid_unauthorized" runat="server" clientidmode="Static">
                                    <!-- Header -->
                                    <div class="o-1" style="margin:0;">
	                                    <h2 id="h3" style="float:left; padding-top:0px; padding-right:30px;">Site Documents</h2>
	                                        <div class="page-options-nav">
                                           &nbsp;
	                                        </div>
	                                    <div class="cb"></div>
                                    </div>
                                    <!-- /Header -->
                        
                                    <div class="dms-folder-bar-cont">
                                       <p>
                                          You are currently unauthorized to view this content.
                                       </p>

                                       <p>
                                          Please login with a different account if you wish to access this content.
                                       </p>
   
                                       <p>
                                          If you have any questions, please contact the site administrator.
                                       </p>
                                    </div>
                                </div>
                            </div>
                            <div id="modalExternalDms" class="modal-container" style="overflow:hidden;"></div>
                            <asp:HiddenField id="hidBasePath" ClientIDMode="Static" runat="server"/>
                            <asp:HiddenField id="hidden_document_type" ClientIDMode="Static" runat="server"/>
                            <asp:HiddenField ID="hiddenCurrentLocationCount" runat="server" Value="0" />
                            
                       </div>
                   </div>
                   <!-- #tabs -->
                   <div class="cb">
                   </div>
               </div>
               <!-- .application-information -->
               <div class="application-process" style="float: left;">
                   <h2>Application Process</h2>
                   <div id="progressbar" class="ui-progressbar ui-widget ui-widget-content ui-corner-all">
                       <asp:Panel ID="progressbar_pink" runat="server" CssClass="ui-progressbar-value ui-widget-header ui-corner-left" Width="75%"></asp:Panel>
                   </div>
                   <asp:Label CssClass="lblMilestone" runat="server" ID="lblMajorMileStone"></asp:Label>
                       <h2>
                       </h2>
                       <div ID="tabs2" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
                           <ul class="ui-tabs ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
                               <li ID="liPreliminaryReview" runat="server"><a href="#preliminary_review" 
                                       onclick="DynamicUpdate('PR');">Preliminary<br />Review</a> </li>
                               <li ID="liStructuralAnalysis" runat="server"><a href="#structural_analysis" 
                                       onclick="DynamicUpdate('SA');">Structural<br />Analysis</a> </li>
                               <li ID="liDocumentReview" runat="server"><a href="#document_review" 
                                       onclick="DynamicUpdate('DR');">Document<br />Review</a> </li>
                               <li ID="liLease" runat="server" style="float:left;"><a class="tabs-title-fix" 
                                       href="#lease" onclick="DynamicUpdate('LE');">Lease</a> </li>
                               <li ID="liConstruction" runat="server" style="float:left;">
                                   <a class="tabs-title-fix" href="#construction" onclick="DynamicUpdate('CO');">
                                   Construction</a> </li>
                               <li ID="liRevisions" runat="server" class="last-application-process">
                                   <a class="tabs-title-fix" href="#revisions" onclick="DynamicUpdate('RE');">
                                   Revisions</a> </li>
                           </ul>
                           <div ID="preliminary_review" runat="server" clientidmode="static">
                               <div style="padding: 20px;overflow:auto;">
                                   <div ID="error_prelim">
                                   </div>
                                   <div class="full-col align-right">
                                       <asp:Button ID="btnSubmit_PreliminaryReview_upper" runat="server" 
                                                   Text="Save"
                                                   CssClass="ui-button ui-widget ui-state-default ui-corner-all"
                                                   OnClick="btnSubmit_PreliminaryReview_Click"
                                                   OnClientClick="if (!prelimBeforeSave('PreliminaryReview')) {return false;}"
                                                   UseSubmitBehavior="false" />
                                   </div>
                                   <div class="left-col" style="width:345px;" >
                                       <!--Document Link-->
                                       <div id="divlblPreliminaryDecision_ColLeft1" runat="server" ClientIDMode="Static" >
                                           <div class="document-link" id="PreliminaryDecision"  >
                                               <asp:Label ID="lblPreliminaryDecision_ColLeft1" runat="server">Preliminary Decision</asp:Label>
                                               <asp:TextBox ID="txtPreliminaryDecision_ColLeft1" runat="server" 
                                                   CssClass="datepicker split"></asp:TextBox>
                                               <asp:DropDownList ID="ddlPreliminaryDecision_ColLeft1" runat="server" 
                                                   CssClass="split">
                                               </asp:DropDownList>
                                               <asp:HiddenField  ID="hidden_document_PreliminaryDecision" ClientIDMode="Static" runat="server" />
                                           </div>
                                       </div>
                                       <br />
                                       <div id="divSDPtoCustomer_ColLeft1" runat="server"  ClientIDMode="Static" >
                                            <!--Document Link-->
                                            <div class="document-link" id="SDPtoCustomer"  >
                                               <asp:Label ID="lblSDPtoCustomer_ColLeft1" runat="server">SDP to Customer</asp:Label>
                                               <asp:TextBox ID="txtSDPtoCustomer_ColLeft1" runat="server" 
                                                   CssClass="datepicker"></asp:TextBox>
                                                <asp:HiddenField  ID="hidden_document_SDPtoCustomer" ClientIDMode="Static" runat="server" />
                                            </div>
                                       </div>
                                       <br />
                                       <!--Document Link-->
                                       <div id="divlblPreliminarySitewalk_ColLeft1" runat="server" ClientIDMode="Static" >
                                           <div class="document-link" id="PreliminarySitewalk"  >
                                               <asp:Label ID="lblPreliminarySitewalk_ColLeft1" runat="server">Preliminary Sitewalk</asp:Label>
                                               <asp:TextBox ID="txtPreliminarySitewalk_ColLeft1" runat="server" 
                                                   CssClass="datepicker split"></asp:TextBox>
                                               <asp:DropDownList ID="ddlPreliminarySitewalk_ColLeft1" runat="server" 
                                                   CssClass="split">
                                               </asp:DropDownList>
                                               <asp:HiddenField  ID="hidden_document_PreliminarySitewalk" ClientIDMode="Static" runat="server" />
                                           </div>
                                       </div>
                                       <br />
                                       <!--Document Link-->
                                       <div id="divLandlordConsentType_ColLeft1" runat="server" ClientIDMode="Static" >
                                           <div class="document-link" id="LandlordConsentType"  >
                                               <asp:Label ID="lblLandlordConsentType_ColLeft1" runat="server">Landlord Consent Type</asp:Label>
                                               <asp:DropDownList ID="ddlLandlordConsentType_ColLeft1" runat="server" 
                                                   CssClass="split">
                                               </asp:DropDownList>
                                               <asp:HiddenField  ID="hidden_document_LandlordConsentType" ClientIDMode="Static" runat="server" />
                                           </div>
                                       </div>
                                       <br />
                                       <asp:Label ID="lblLandlordConsentTypeNote" runat="server" ClientIDMode="Static">Landlord Consent Type Note</asp:Label>
                                       <asp:TextBox ID="txtLandlordConsentTypeNote" runat="server" 
                                           ClientIDMode="Static" 
                                           style="width: 180px; max-width:180px; margin-bottom:0px !important; " 
                                           TextMode="MultiLine"></asp:TextBox>
                                       <br />
                                   </div>
                                   <div class="right-col">
                                   </div>
                                   <div class="full-col align-right">
                                       <asp:Button ID="btnSubmit_PreliminaryReview" runat="server" 
                                                   Text="Save"
                                                   CssClass="ui-button ui-widget ui-state-default Re,ui-corner-all"
                                                   OnClick="btnSubmit_PreliminaryReview_Click"
                                                   OnClientClick="if (!prelimBeforeSave('PreliminaryReview')) {return false;}"
                                                   UseSubmitBehavior="false" />
                                   </div>
                                   <div class="cb">
                                   </div>
                               </div>
                           </div>
                           <!-- .preliminary_review --><span id="hidden_count" style="display:none">1</span>
                           <span id="BufferIndex" style="display:none"></span><span ID="PageIndex" 
                               style="display:none"></span>
                           <asp:HiddenField ID="hidden_SA_ID" runat="server" />
                           <asp:HiddenField ID="hidden_lease_admin_id" runat="server" />
                           <asp:HiddenField ID="hidden_lease_comments" runat="server" Value="" />
                           <asp:HiddenField ID="hidden_SA_Vendor_ID" runat="server" />
                           <asp:HiddenField ID="hidden_SA_Template" runat="server" />
                           <input type="hidden" id="hidden_SA_IsNew" />
                           <input type="hidden" id="hidden_SA_IsSave" />
                           <input type="hidden" id="hidden_SA_IsShow_Add_Comment" />
                           <div ID="structural_analysis" runat="server" clientidmode="Static">
                               <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                   <ContentTemplate>
                                       <div style="padding: 20px;overflow:auto;">
                                           <asp:HiddenField ID="hidden_SA_popup" runat="server" ClientIDMode="Static" />
                                           <asp:HiddenField ID="hidden_SA" runat="server" />
                                           <asp:HiddenField ID="hidden_SA_Comment" runat="server" />
                                           <asp:HiddenField ID="hidden_SA_count" runat="server" /> 
                                           <asp:HiddenField ID="hidden_SA_updated_at" runat="server" />                                           
                                           <div>
                                               <div class="PanelPageNumber">
                                                   <span ID="current-page" class="textPageNumber">5</span>
                                                   <span class="textPageNumber">of</span> <span id="total-page" 
                                                       class="textPageNumber">5</span>
                                               </div>
                                               <div class="PanelPageControl">
                                                   <asp:Button ID="btnSADelete" runat="server" ClientIDMode="Static" 
                                                       onclick="btnSADelete_Click" 
                                                       OnClientClick="return openDeleteAppButtonPopup('Structural Analysis',event,getSAID());" 
                                                       Text="Delete" Visible="false" />
                                                   &nbsp;&nbsp;&nbsp;
                                                   <input id="btn_Previous" type="button" value="Previous" />
                                                   &nbsp;&nbsp;&nbsp;
                                                   <input id="btn_Next" type="button" value="Next" />
                                                   &nbsp;&nbsp;&nbsp;
                                                   <input id="btn_AddNew" type="button" value="Add New" />
                                               </div>
                                           </div>

                                           <div class="sa-control-detail">
                                           <div class="full-col" style="margin-top: 10px;">
                                               <asp:Label ID="lblHasTowermodTitle" runat="server">Has Tower Modification:</asp:Label>
                                               <asp:Label ID="lblHasTowerMod" runat="server"></asp:Label>
                                               <div style="float:right; margin-bottom: 10px;margin-top:10px">
                                                   <%-- style="margin-bottom: 30px;"--%>
                                                   <asp:Button ID="btnSubmit_StructuralAnalysis_Upper" runat="server" 
                                                               Text="Save"
                                                               CssClass="ui-button ui-widget ui-state-default ui-corner-all"                                                                           
                                                               OnClick="btnSubmit_StructuralAnalysis_Click"
                                                               OnClientClick="if (!funcSACheckBeforeSave('StructuralAnalysis')) {return false;}"
                                                               UseSubmitBehavior="false" />
                                               </div>
                                               <br />
                                               <div ID="structural_decision" runat="server" clientidmode="Static">
                                                   <%--<h4>Structural Decision</h4>--%>
                                                   <!--Document Link-->
                                                    <div class="document-link" id="Structural_Decision"  >
                                                       <asp:Label ID="lblSADecisionDate" runat="server">Decision:</asp:Label>
                                                       <asp:TextBox ID="txtSADecisionDate" runat="server" CssClass="datepicker split"></asp:TextBox>
                                                       <asp:DropDownList ID="ddlDecisionType" runat="server">
                                                       </asp:DropDownList>
                                                       <asp:HiddenField  ID="hidden_document_Structural_Decision" ClientIDMode="Static" runat="server" />
                                                   </div>
                                               </div>
                                               <%--                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                           <asp:Label ID="lbljcheck_2" runat="server" Text="Count start..."></asp:Label>--%>
                                           </div>
                                                <div ID="div_sa_summary_table" runat="server" class="sa-decision-table">
                                                   <asp:Repeater ID="saRepeater" runat="server">
                                                        <HeaderTemplate>
                                                            <table id="tableSaSummary" class="related-apps TableGenInfo" style="border-spacing: 0px;">
                                                                <tr>
                                                                    <th>
                                                                        <asp:Label ID="SummaryHeaderReceived" runat="server" CssClass="sa-decision-label">Received</asp:Label>
                                                                    </th>
                                                                    <th>
                                                                        <asp:Label ID="SummaryHeaderDecisionDate" runat="server" CssClass="sa-decision-label">Decision Date</asp:Label>
                                                                    </th>
                                                                    <th>
                                                                        <asp:Label ID="SummaryHeaderDecision" runat="server" CssClass="sa-decision-label">Decision</asp:Label>
                                                                    </th>
                                                                    <th>
                                                                        <asp:Label ID="SummaryHeaderProcessType" runat="server" CssClass="sa-decision-label">Process Type</asp:Label>
                                                                    </th>
                                                                </tr>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                           <tr>
                                                                <td>
                                                                    <asp:Label ID="SummaryReceived" runat="server" CssClass="sa-decision-label"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="SummaryDecisionDate" runat="server" CssClass="sa-decision-label"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="SummaryDecision" runat="server" CssClass="sa-decision-label"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="SummaryProcessType" runat="server" CssClass="sa-decision-label"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </div>
                                                </div>
                                           <%--<asp:Panel ID="nav2" runat="server" CssClass="pnlnav2">--%>
                                           <div class="sa-detail">
                                           <asp:Panel ID="Panel2" runat="server" CssClass="pnlnav2" Visible="false">
                                           </asp:Panel>
                                           <div class="cb">
                                           </div>
                                           <div ID="message_alert">
                                           </div>
                                           <asp:Panel ID="slideshow2" runat="server" CssClass="slideshow2">
                                               <asp:Panel ID="Panel1" runat="server">
                                                   <div class="left-col">
                                                       
                                                       <div ID="request_wrapper" runat="server" clientidmode="Static">
                                                           <h4>
                                                               Request</h4>
                                                           <asp:Label ID="lblRequest_SAWToCustomer_ColLeft1" runat="server">SAW to Customer</asp:Label>
                                                           <asp:TextBox ID="txtRequest_SAWToCustomer_ColLeft1" runat="server" 
                                                               CssClass="datepicker"></asp:TextBox>
                                                           <br/>
                                                           <asp:Label ID="lblRequest_SAWApproved_ColLeft1" runat="server">SAW Approved</asp:Label>
                                                           <asp:TextBox ID="txtRequest_SAWApproved_ColLeft1" runat="server" 
                                                               CssClass="datepicker"></asp:TextBox>
                                                           <br/>
                                                           <!--Document Link-->
                                                           <div class="document-link" id="SAWToSAC"  > 
                                                                <asp:Label ID="lblRequest_SAWToSAC_ColLeft1" runat="server">SAW to SAC</asp:Label>
                                                                <asp:TextBox ID="txtRequest_SAWToSAC_ColLeft1" runat="server" CssClass="datepicker"></asp:TextBox>
                                                                <asp:HiddenField  ID="hidden_document_SAWToSAC" ClientIDMode="Static" runat="server" />
                                                           </div>
                                                           <br/>
                                                           <!--Document Link-->
                                                           <div class="document-link" id="SAWRequest"  >
                                                               <asp:Label ID="lblRequest_SAWRequest_ColLeft1" runat="server">SAW Received</asp:Label>
                                                               <asp:TextBox ID="txtRequest_SAWRequest_ColLeft1" runat="server" 
                                                                   CssClass="datepicker"></asp:TextBox>
                                                                   <asp:HiddenField  ID="hidden_document_SAWRequest" ClientIDMode="Static" runat="server" />
                                                           </div>
                                                           <br/>
                                                           <asp:Label ID="lblRequest_SACName_ColLeft1" runat="server">SAC Name</asp:Label>
                                                           <asp:DropDownList ID="ddlRequest_SACName_ColLeft1" runat="server">
                                                           </asp:DropDownList>
                                                           <br/>
                                                           <asp:Label ID="lblRequest_SACPriority_ColLeft1" runat="server">SAC Priority</asp:Label>
                                                           <asp:DropDownList ID="ddlRequest_SACPriority_ColLeft1" runat="server">
                                                           </asp:DropDownList>
                                                           <br/>
                                                       </div>
                                                       <div ID="purchase_order_wrapper" runat="server" clientidmode="Static">
                                                           <h4>
                                                               Purchase Order</h4>
                                                           <asp:Label ID="lblPurchaseOrder_Vendor_ColLeft1" runat="server">Vendor</asp:Label>
                                                           <asp:DropDownList ID="ddlPurchaseOrder_Vendor_ColLeft1" runat="server">
                                                           </asp:DropDownList>
                                                           <br />
                                                           <asp:Label ID="lblPurchaseOrder_ShoppingCartNumber_ColLeft1" runat="server">Shopping Cart Number</asp:Label>
                                                           <asp:TextBox ID="txtPurchaseOrder_ShoppingCartNumber_ColLeft1" runat="server"></asp:TextBox>
                                                           <br/>
                                                           <asp:Label ID="lblPurchaseOrder_Ordered_ColLeft1" runat="server">Ordered</asp:Label>
                                                           <asp:TextBox ID="txtPurchaseOrder_Ordered_ColLeft1" runat="server" 
                                                               CssClass="datepicker"></asp:TextBox>
                                                           <br />
                                                           <!--Document Link-->
                                                           <div id="divPurchaseOrder_Received_ColLeft1" runat="server" ClientIDMode="Static" >
                                                           <div class="document-link" id="PurchaseOrder_Received"  >
                                                               <asp:Label ID="lblPurchaseOrder_Received_ColLeft1" runat="server">Received</asp:Label>
                                                               <asp:TextBox ID="txtPurchaseOrder_Received_ColLeft1" runat="server" 
                                                                   CssClass="datepicker"></asp:TextBox>
                                                                <asp:HiddenField  ID="hidden_document_PurchaseOrder_Received" ClientIDMode="Static" runat="server" />
                                                            </div>
                                                           </div>
                                                           <br />
                                                           <asp:Label ID="lblPurchaseOrder_Amount_ColLeft1" runat="server">Amount</asp:Label>
                                                           <%--<span class="DollarSign">$</span>--%>
                                                           <%--<asp:TextBox ID="txtPurchaseOrder_Amount_ColLeft1" runat="server" CssClass="DollarTextbox"></asp:TextBox>--%>
                                                           <asp:TextBox ID="txtPurchaseOrder_Amount_ColLeft1" runat="server" 
                                                               CssClass="moneyfield"></asp:TextBox>
                                                           <br />
                                                           <asp:Label ID="lblPurchaseOrder_Number_ColLeft1" runat="server">Number</asp:Label>
                                                           <asp:TextBox ID="txtPurchaseOrder_Number_ColLeft1" runat="server"></asp:TextBox>
                                                           <br />
                                                           <asp:Label ID="lblPurchaseOrder_BlanketPOMonthYear_ColLeft1" runat="server">Blanket PO</asp:Label>
                                                           <asp:DropDownList ID="ddlPurchaseOrder_BlanketPOMonth_ColLeft1" runat="server" 
                                                               CssClass="split">
                                                               <asp:ListItem Text=" - Month - " Value="" />
                                                               <asp:ListItem Text="January" Value="1" />
                                                               <asp:ListItem Text="February" Value="2" />
                                                               <asp:ListItem Text="March" Value="3" />
                                                               <asp:ListItem Text="April" Value="4" />
                                                               <asp:ListItem Text="May" Value="5" />
                                                               <asp:ListItem Text="June" Value="6" />
                                                               <asp:ListItem Text="July" Value="7" />
                                                               <asp:ListItem Text="August" Value="8" />
                                                               <asp:ListItem Text="September" Value="9" />
                                                               <asp:ListItem Text="October" Value="10" />
                                                               <asp:ListItem Text="November" Value="11" />
                                                               <asp:ListItem Text="December" Value="12" />
                                                           </asp:DropDownList>
                                                           <asp:DropDownList ID="ddlPurchaseOrder_BlanketPOYear_ColLeft1" runat="server" 
                                                               CssClass="split">
                                                               <asp:ListItem Text=" - Year - " Value="" />
                                                               <asp:ListItem Text="2010" Value="2010" />
                                                               <asp:ListItem Text="2011" Value="2011" />
                                                               <asp:ListItem Text="2012" Value="2012" />
                                                               <asp:ListItem Text="2013" Value="2013" />
                                                               <asp:ListItem Text="2014" Value="2014" />
                                                               <asp:ListItem Text="2015" Value="2015" />
                                                               <asp:ListItem Text="2016" Value="2016" />
                                                               <asp:ListItem Text="2017" Value="2017" />
                                                               <asp:ListItem Text="2018" Value="2018" />
                                                               <asp:ListItem Text="2019" Value="2019" />
                                                               <asp:ListItem Text="2020" Value="2020" />
                                                           </asp:DropDownList>
                                                           <br/>
                                                           <asp:Label ID="lblPurchaseOrder_POReleaseStatus_ColLeft1" runat="server">PO Release Status</asp:Label>
                                                           <asp:DropDownList ID="ddlPurchaseOrder_POReleaseStatus_ColLeft1" runat="server">
                                                           </asp:DropDownList>
                                                           <br/>
                                                           <asp:Label ID="lblPurchaseOrder_PORelease_ColLeft1" runat="server">PO Release</asp:Label>
                                                           <asp:TextBox ID="txtPurchaseOrder_PORelease_ColLeft1" runat="server" 
                                                               CssClass="datepicker"></asp:TextBox>
                                                           <br/>
                                                           <asp:Label ID="lblPurchaseOrder_PONotes_ColLeft1" runat="server" 
                                                               style="float: left;">PO Notes</asp:Label>
                                                           <asp:TextBox ID="txtPurchaseOrder_PONotes_ColLeft1" runat="server" 
                                                               style="width: 203px; max-width:203px; margin-bottom: 20px;" 
                                                               TextMode="MultiLine"></asp:TextBox>
                                                       </div>
                                                       <div ID="SA_process_dates_wrapper" runat="server" clientidmode="Static">
                                                           <h4>
                                                               Process Dates</h4>
                                                           <asp:Label ID="lblProcessDates_Type_ColLeft1" runat="server">Type</asp:Label>
                                                           <asp:DropDownList ID="ddlProcessDates_Type_ColLeft1" runat="server">
                                                           </asp:DropDownList>
                                                           <br />
                                                           <asp:Label ID="lblProcessDates_SACDescription_ColLeft1" runat="server">SAC Description</asp:Label>
                                                           <asp:DropDownList ID="ddlProcessDates_SACDescription_ColLeft1" runat="server">
                                                           </asp:DropDownList>
                                                           <br/>
                                                           <asp:Label ID="lblProcessDates_Ordered_ColLeft1" runat="server">Ordered</asp:Label>
                                                           <asp:TextBox ID="txtProcessDates_Ordered_ColLeft1" runat="server" 
                                                               CssClass="datepicker split"></asp:TextBox>
                                                           <asp:DropDownList ID="ddlProcessDates_ColLeft1" runat="server" CssClass="split">
                                                           </asp:DropDownList>
                                                           <br />

                                                           <!--Document Link-->
                                                           <div id="divProcessDates_Received_ColLeft1" runat="server" ClientIDMode="Static" >
                                                               <div class="document-link" id="SA_Received" > 
                                                                    <asp:Label ID="lblProcessDates_Received_ColLeft1" runat="server">Received</asp:Label>
                                                                    <asp:TextBox ID="txtProcessDates_Received_ColLeft1" runat="server" CssClass="datepicker"></asp:TextBox>
                                                                    <asp:DropDownList ID="ddlProcess_ReceivedStatus_ColLeft1" runat="server"  CssClass="split"></asp:DropDownList>
                                                                    <asp:HiddenField  ID="hidden_document_SA_Received" ClientIDMode="Static" runat="server" />
                                                               </div>
                                                           </div>
                                                           <br />
                                                           <asp:Label ID="lblProcessDates_TowerPercent_ColLeft1" runat="server">Tower Percent</asp:Label>
                                                           <asp:TextBox ID="txtProcessDates_TowerPercent_ColLeft1" runat="server" 
                                                               CssClass="pencent-field"></asp:TextBox>
                                                           <p style="float: left; width: 90%; padding: 5px 5px 0; text-align: left; text-decoration: italic; font-size: 12px; font-style:italic; margin-bottom:20px;color:#666;">
                                                               Note: Please enter percentages using whole-number notation; e.g. eighty seven 
                                                               percent as 87.0
                                                           </p>
                                                           <%--<asp:Label ID="lblProcessDates_PSReceived_ColLeft1" runat="server">PS Received</asp:Label>
                                                      <asp:TextBox ID="txtProcessDates_PSReceived_ColLeft1" CssClass="datepicker" runat="server"></asp:TextBox>--%>
                                                       </div>
                                                   </div>
                                                   <div class="right-col">
                                                       <div ID="div_sa_decision_wrapper" runat="server" clientidmode="Static">
                                                            <asp:Label ID="lbl_sa_decision" runat="server" CssClass="div-sa-decision">SA Decision</asp:Label>
                                                            <asp:TextBox ID="txt_sa_decision" runat="server" 
                                                                CssClass="datepicker split"></asp:TextBox>
                                                            <asp:DropDownList ID="ddl_sa_decision" runat="server" CssClass="split">
                                                            </asp:DropDownList>
                                                        </div>

                                                       <div ID="delayed_order_wrapper" runat="server" clientidmode="Static">
                                                           <h4>
                                                               Delayed Order</h4>
                                                            <!--Document Link-->
                                                           <div id="divDelayedOrder_OnHold_ColRight1" runat="server" ClientIDMode="Static" >
                                                               <div class="document-link" id="DelayedOrder_OnHold"  > 
                                                                   <asp:Label ID="lblDelayedOrder_OnHold_ColRight1" runat="server">Onhold</asp:Label>
                                                                   <asp:TextBox ID="txtDelayedOrder_Onhold_ColRight1" runat="server" 
                                                                       CssClass="datepicker"></asp:TextBox>
                                                                    <asp:HiddenField  ID="hidden_document_DelayedOrder_OnHold" ClientIDMode="Static" runat="server" />
                                                               </div>
                                                           </div>
                                                           <br />
                                                           <!--Document Link-->
                                                           <div id="divDelayedOrder_Cancelled_ColRight1" runat="server" ClientIDMode="Static" >
                                                               <div class="document-link" id="DelayedOrder_Cancelled" > 
                                                                   <asp:Label ID="lblDelayedOrder_Cancelled_ColRight1" runat="server">Cancelled</asp:Label>
                                                                   <asp:TextBox ID="txtDelayedOrder_Cancelled_ColRight1" runat="server" 
                                                                       CssClass="datepicker"></asp:TextBox>
                                                                    <asp:HiddenField  ID="hidden_document_DelayedOrder_Cancelled" ClientIDMode="Static" runat="server" />
                                                                </div>
                                                            </div>
                                                       </div>
                                                       <br />
                                                       <div ID="structural_analysis_payment" runat="server" clientidmode="static">
                                                           <h4>
                                                               Payment</h4>
                                                           <asp:Label ID="lblPayment_FeeProcessed_ColRight1" runat="server">Fee Processed</asp:Label>
                                                           <asp:TextBox ID="txtPayment_FeeProcessed_ColRight1" runat="server" 
                                                               CssClass="datepicker"></asp:TextBox>
                                                           <br />
                                                           <asp:Label ID="lblPayment_FeeAmount_ColRight1" runat="server">Fee Amount</asp:Label>
                                                           <asp:TextBox ID="txtPayment_FeeAmount_ColRight1" runat="server" 
                                                               CssClass="moneyfield"></asp:TextBox>
                                                           <br />
                                                           <asp:Label ID="lblPayment_CheckNumber_ColRight1" runat="server">Check Number</asp:Label>
                                                           <asp:TextBox ID="txtPayment_CheckNumber_ColRight1" runat="server"></asp:TextBox>
                                                           <br />
                                                           <asp:Label ID="lblPayment_PayorType_ColRight1" runat="server">Payor Type</asp:Label>
                                                           <asp:DropDownList ID="ddlPayment_PayorType_ColRight1" runat="server">
                                                           </asp:DropDownList>
                                                       </div>
                                                   </div>
                                                   <div class="full-col align-right" style="margin-bottom: 30px;">
                                                       <asp:Button ID="btnSubmit_StructuralAnalysis" runat="server" 
                                                                   Text="Save"
                                                                   CssClass="ui-button ui-widget ui-state-default ui-corner-all"                                                                           
                                                                   OnClick="btnSubmit_StructuralAnalysis_Click"
                                                                   OnClientClick="if (!funcSACheckBeforeSave('StructuralAnalysis')) {return false;}"
                                                                   UseSubmitBehavior="false" />
                                                   </div>
                                                   <div class="full-col align-right" style="margin-bottom: 30px;">
                                                       <asp:LinkButton ID="link_addnew" runat="server" CssClass="link-comment" 
                                                           OnClientClick="return linkAddNewClick();">Add New Comment</asp:LinkButton>
                                                       <div ID="comment-link">
                                                           <%--<asp:LinkButton ID="link_cancel" runat="server" CssClass="link-comment link-comment-cancel" OnClientClick="HideAddComment();return linkCancelClick();">cancel</asp:LinkButton>
                                                       <asp:LinkButton ID="link_post" runat="server" CssClass="link-comment" OnClientClick="linkPostClick();" OnClick="link_addnew_Click">post</asp:LinkButton>--%>
                                                       </div>
                                                   </div>

                                                   <div class="application-comments-inner-sa" style="margin-bottom: 20px;">
                                                       <div ID="div-add-new-comment" class="new-comment">
                                                           <div class="balloon">
                                                               <asp:TextBox ID="txt_comment_SA" runat="server" Columns="5" 
                                                                   CssClass="comment-textarea" Rows="2" TextMode="MultiLine"></asp:TextBox>
                                                               <div class="balloon-bottom">
                                                               </div>
                                                           </div>
                                                           <div class="author-info" 
                                                               style="margin-top: 15px; font-size: 10pt; color: #a1a1a1;">
                                                               <asp:Image ID="img_Author_SA" runat="server" AlternateText="profile-picture" 
                                                                   ImageUrl="/images/avatars/default.png" Width="25" />
                                                               <div ID="commentButton" style="text-align:right;">
                                                                   <asp:Button ID="btnSACancel" runat="server" ClientIDMode="Static" 
                                                                       CssClass="sa-comment-button" 
                                                                       OnClientClick="HideAddComment();return linkCancelClick();" Text="Cancel" />
                                                                   <asp:Button ID="btnSAPost" runat="server" ClientIDMode="Static" 
                                                                       CssClass="sa-comment-button" NavigateUrl="javascript:void(0)" 
                                                                       OnClick="link_addnew_Click" OnClientClick="linkPostClick();" Text="Post" />
                                                               </div>
                                                           </div>
                                                       </div>
                                                   </div>
                                                   <div id="div-more-sa-comment" class="div-load-more-comment">
                                                    <div id="sa-comment-spinner-cont" style="display: block;float: left;width: 30px;height: 50px;"></div>
                                                   </div>
                                                   <div class="cb">
                                                   </div>
                                               </asp:Panel>
                                               
                                           </asp:Panel>
                                           <!-- .slideshow -->
                                           <div class="cb">
                                           </div>
                                           </div>
                                       </div>
                                   </ContentTemplate>
                               </asp:UpdatePanel>
                           </div>
                           <!-- .structural-analysis -->
                           <div ID="document_review" runat="server" clientidmode="static">
                               <div style="padding: 20px;overflow:auto;">
                                   <div ID="error_docreview">
                                   </div>
                                   <div class="full-col align-right">
                                       <asp:Button ID="btnSubmit_DocumentReview_Upper" runat="server"
                                                   Text="Save"
                                                   CssClass="ui-button ui-widget ui-state-default ui-corner-all"
                                                   OnClick="btnSubmit_DocumentReview_Click"
                                                   OnClientClick="if (!docReviewBeforeSave()) {return false;}"
                                                   UseSubmitBehavior="false" /> 
                                   </div>
                                   <div class="left-col" style="width:345px;">
                                       <asp:Label ID="lblLeaseExhibit_CoHostTenant" runat="server">Co-host Tenant</asp:Label>
                                       <asp:DropDownList ID="ddlLeaseExhibit_CoHostTenant" runat="server">
                                       </asp:DropDownList>
                                       <br />
                                       <div ID="lease_exhibit_wrapper" runat="server" clientidmode="Static">
                                           <h4>
                                               Lease Exhibit</h4>
                                           <asp:Label ID="lblLeaseExhibit_Ordered" runat="server">Ordered</asp:Label>
                                           <asp:TextBox ID="txtLeaseExhibit_Ordered" runat="server" 
                                               CssClass="datepicker split"></asp:TextBox>
                                           <asp:DropDownList ID="ddlLeaseExhibit_Ordered" runat="server" CssClass="split">
                                           </asp:DropDownList>
                                           <br/>
                                           <!--Document Link-->
                                           <div id="divLeaseExhibit_Received" runat="server" ClientIDMode="Static" >
                                               <div class="document-link" id="LeaseExhibit_Received"   > 
                                                   <asp:Label ID="lblLeaseExhibit_Received" runat="server">Received</asp:Label>
                                                   <asp:TextBox ID="txtLeaseExhibit_Received" runat="server" CssClass="datepicker"></asp:TextBox>
                                                   <asp:HiddenField  ID="hidden_document_LeaseExhibit_Received" ClientIDMode="Static" runat="server" />
                                               </div>
                                           </div>
                                           <br/>
                                           <!--Document Link-->
                                           <div id="divLeaseExhibit_Decision" runat="server" ClientIDMode="Static" >
                                               <div class="document-link" id="LeaseExhibit_Decision" > 
                                                   <asp:Label ID="lblLeaseExhibit_Decision" runat="server">Decision</asp:Label>
                                                   <asp:TextBox ID="txtLeaseExhibit_Decision" runat="server" 
                                                       CssClass="datepicker split"></asp:TextBox>
                                                   <asp:DropDownList ID="ddlLeaseExhibit_Decision" runat="server" CssClass="split">
                                                   </asp:DropDownList>
                                                   <asp:HiddenField  ID="hidden_document_LeaseExhibit_Decision" ClientIDMode="Static" runat="server" />
                                               </div>
                                           </div>
                                       </div>
                                       <br/>
                                       <div ID="landlord_consent_wrapper" runat="server" clientidmode="Static">
                                           <h4>
                                               Landlord Consent</h4>
                                           <!--Document Link-->
                                           <div id="divLandlordConsent_LandlordNotice" runat="server" ClientIDMode="Static" >
                                               <div class="document-link" id="LandlordConsent_LandlordNotice"   > 
                                                    <asp:Label ID="lblLandlordConsent_LandlordNotice" runat="server">Landlord Notice</asp:Label>
                                                    <asp:TextBox ID="txtLandlordConsent_LandlordNotice" runat="server" 
                                                        CssClass="datepicker"></asp:TextBox>
                                                    <asp:HiddenField  ID="hidden_document_LandlordConsent_LandlordNotice" ClientIDMode="Static" runat="server" />
                                                </div>
                                            </div>
                                            <!--Document Link-->
                                           <div id="divLandlordConsentRequested" runat="server" ClientIDMode="Static" >
                                               <div class="document-link" id="LandlordConsentRequested"  > 
                                                   <asp:Label ID="lblLandlordConsentRequested" runat="server">Consent Requested</asp:Label>
                                                   <asp:TextBox ID="txtLandlordConsent_ConsentRequested" runat="server" 
                                                       CssClass="datepicker double-line-label-field"></asp:TextBox>
                                                    <asp:HiddenField  ID="hidden_document_LandlordConsentRequested" ClientIDMode="Static" runat="server" />
                                                </div>
                                            </div>
                                           <br/>
                                           <!--Document Link-->
                                           <div id="divLandlordConsentReceived" runat="server">
                                               <div class="document-link" id="LandlordConsentReceived"  > 
                                                   <asp:Label ID="lblLandlordConsentReceived" runat="server">Received</asp:Label>
                                                   <asp:TextBox ID="txtLandlordConsent_ReceivedDate" runat="server" 
                                                       CssClass="datepicker"></asp:TextBox>
                                                    <asp:HiddenField  ID="hidden_document_LandlordConsentReceived" ClientIDMode="Static" runat="server" />
                                                </div>
                                            </div>
                                       </div>
                                       <br/>
                                       <div ID="construction_drawings_wrapper" runat="server" clientidmode="Static">
                                           <h4>
                                               Construction Drawings</h4>
                                           <!--Document Link-->
                                           <div id="divConstructionDrawingsReceived" runat="server" ClientIDMode="Static" >
                                               <div class="document-link" id="ConstructionDrawingsReceived" >  
                                                    <asp:Label ID="lblConstructionDrawingsReceived" runat="server">Received</asp:Label>
                                                    <asp:TextBox ID="txtConstructionDrawings_Received" runat="server" 
                                                        CssClass="datepicker"></asp:TextBox>
                                                    <asp:HiddenField  ID="hidden_document_ConstructionDrawingsReceived" ClientIDMode="Static" runat="server" />
                                                </div>
                                            </div>
                                           <br/>
                                           <!--Document Link-->
                                           <div id="divConstructionDrawingsDecision" runat="server" >
                                               <div class="document-link" id="ConstructionDrawingsDecision" > 
                                                   <asp:Label ID="lblConstructionDrawingsDecision" runat="server">Decision</asp:Label>
                                                   <asp:TextBox ID="txtConstructionDrawings_Decision" runat="server" 
                                                       CssClass="datepicker split"></asp:TextBox>
                                                   <asp:DropDownList ID="ddlConstructionDrawings_Decision" runat="server" 
                                                       CssClass="split">
                                                   </asp:DropDownList>
                                                   <asp:HiddenField  ID="hidden_document_ConstructionDrawingsDecision" ClientIDMode="Static" runat="server" />
                                               </div>
                                           </div>
                                       </div>
                                       <br/>
                                   </div>
                                   <div class="full-col align-right">
                                       <asp:Button ID="btnSubmit_DocumentReview" runat="server" 
                                                   Text="Save"
                                                   CssClass="ui-button ui-widget ui-state-default ui-corner-all"
                                                   OnClick="btnSubmit_DocumentReview_Click"
                                                   OnClientClick="if (!docReviewBeforeSave()) {return false;}"
                                                   UseSubmitBehavior="false" />
                                   </div>
                                   <div class="cb">
                                   </div>
                               </div>
                           </div>
                           <!-- #document-review -->

                           <div ID="lease" runat="server" clientidmode="Static">
                               <div style="padding: 20px;overflow:auto;">
                                   <div class="full-col align-right">
                                       <asp:Button ID="btnSubmit_Lease_Upper" runat="server" 
                                                   Text="Save"
                                                   CssClass="ui-button ui-widget ui-state-default ui-corner-all"
                                                   OnClick="btnSubmit_Lease_Click"
                                                   OnClientClick="if (!beforeSaveLeaseTab()) {return false;}"
                                                   UseSubmitBehavior="false"  />
                                   </div>
                                   <div ID="message_alert_Lease">
                                   </div>
                                   <div ID="left_col_wrapper" runat="server" class="left-col">
                                       <div id="divExecutionPhase"  runat="server" style="width: 100%;height:40px;text-align:center;background:#F0F0F0;margin-top:10px;margin-bottom:20px;">
                                           <span id="lblExecutionPhase" style="width:100%;text-align:left;margin-left:5px;font-weight:bold;margin-top:10px;" >Execution Phase</span>
                                       </div>
                                       <div ID="executables_sent_to_customer_wrapper" runat="server" class="full-col" clientidmode="static">                                       
                                           <%--Document Link--%> 
                                           <div id="divExecutablesSenttoCustomer" runat="server" ClientIDMode="Static" >
                                               <div class="document-link" id="ExecutablesSenttoCustomer"  > 
                                                   <asp:Label ID="lblExecutablesSenttoCustomer" runat="server">Executables Sent to Customer</asp:Label>
                                                   <asp:TextBox ID="txtExecutablesSenttoCustomer" runat="server" CssClass="datepicker split"></asp:TextBox>
                                                   <asp:DropDownList ID="ddlExecutablesSenttoCustomer" runat="server" CssClass="split"></asp:DropDownList>
                                                   <asp:HiddenField  ID="hidden_document_ExecutablesSenttoCustomer" ClientIDMode="Static" runat="server" />
                                               </div>
                                           </div>
                                           <br />
                                           <%--Document Link--%> 
                                           <div id="divExecutablesToCrown" runat="server" ClientIDMode="Static" >
                                               <div class="document-link" id="ExecutablesToCrown"  > 
                                                   <asp:Label ID="lblExecutablesToCrown" runat="server">Executables to Crown</asp:Label>
                                                   <asp:TextBox ID="txtExecutablesToCrown" runat="server" CssClass="datepicker split"></asp:TextBox>
                                                   <asp:DropDownList ID="ddlExecutablesToCrown" runat="server" CssClass="split"></asp:DropDownList>
                                                   <asp:HiddenField  ID="hidden_document_ExecutablesToCrown" ClientIDMode="Static" runat="server" />
                                               </div>
                                           </div>
                                       </div>
                                       <asp:Label ID="Label67" runat="server">Document Type</asp:Label>
                                        <asp:DropDownList ID="ddlExecDocs_DocType" runat="server" >
                                            <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                       <h4>Document Dates</h4>
                                       <div ID="docs_back_from_customer_wrapper" runat="server" clientidmode="Static">
                                           <%--Document Link--%> 
                                           <div id="divLOEHoldupReasonTypes_DocsBackfromCustomer" runat="server"  ClientIDMode="Static" >
                                               <div class="document-link" id="LOEHoldupReasonTypes_DocsBackfromCustomer" >
                                                   <asp:Label ID="lblLOEHoldupReasonTypes_DocsBackfromCustomer" runat="server">Docs Back from Customer</asp:Label>
                                                   <asp:TextBox ID="txtLOEHoldupReasonTypes_DocsBackfromCustomer" runat="server" 
                                                       CssClass="datepicker split double-line-label-field"></asp:TextBox>
                                                   <asp:DropDownList ID="ddlLOEHoldupReasonTypes_DocsBackfromCustomer" 
                                                       runat="server" CssClass="split double-line-label-field">
                                                   </asp:DropDownList>
                                                   <asp:HiddenField  ID="hidden_document_LOEHoldupReasonTypes_DocsBackfromCustomer" ClientIDMode="Static" runat="server" />
                                               </div>
                                           </div>
                                           <br />
                                           <asp:Label ID="lblLOEHoldupReasonTypes_SignedbyCustomer" runat="server">Signed by Customer</asp:Label>
                                           <asp:TextBox ID="txtLOEHoldupReasonTypes_SignedbyCustomer" runat="server" 
                                               CssClass="datepicker double-line-label-field"></asp:TextBox>
                                            <br />
                                       </div>
                                       <div ID="lease_execution_wrapper" runat="server" clientidmode="Static">
                                           <asp:Label ID="lblLOEHoldupReasonTypes_DocstoFSC" runat="server">Docs to FSC</asp:Label>
                                           <asp:TextBox ID="txtLOEHoldupReasonTypes_DocstoFSC" runat="server" 
                                               CssClass="datepicker"></asp:TextBox>
                                           <br />
                                           <label>
                                              Date Received at FSC</label>
                                           <asp:TextBox ID="txtExecDocs_RecvedatFSCDate" CssClass="datepicker" runat="server"></asp:TextBox>
                                           <br />
                                           <%--Document Link--%> 
                                           <div id="divLOEHoldupReasonTypes_LeaseExecution" runat="server" ClientIDMode="Static" >
                                               <div class="document-link" id="LeaseExecution"  > 
                                                   <asp:Label ID="lblLOEHoldupReasonTypes_LeaseExecution" runat="server">Lease Execution</asp:Label>
                                                   <asp:TextBox ID="txtLOEHoldupReasonTypes_LeaseExecution" runat="server" CssClass="datepicker split double-line-label-field"></asp:TextBox>
                                                   <asp:DropDownList ID="ddlLOEHoldupReasonTypes_LeaseExecution" runat="server"  CssClass="split double-line-label-field"></asp:DropDownList>
                                                   <asp:HiddenField  ID="hidden_document_LeaseExecution" ClientIDMode="Static" runat="server" />
                                               </div>
                                           </div>
                                           <br />
                                       </div>
                                       <div ID="amendment_execution_wrapper" runat="server" clientidmode="Static">
                                           <div class="document-link" id="AmendmentExecution"  > 
                                               <asp:Label ID="lblLOEHoldupReasonTypes_AmendmentExecution" runat="server" 
                                                   Text="Amendment Execution"></asp:Label>
                                               <asp:TextBox ID="txtLOEHoldupReasonTypes_AmendmentExecution" runat="server" 
                                                   CssClass="datepicker split double-line-label-field"></asp:TextBox>
                                               <asp:DropDownList ID="ddlLOEHoldupReasonTypes_AmendmentExecution" 
                                                   runat="server" CssClass="split double-line-label-field">
                                               </asp:DropDownList>
                                               <asp:HiddenField  ID="hidden_document_AmendmentExecution" ClientIDMode="Static" runat="server" />
                                           </div>
                                           <br />
                                       </div>
                                       <div ID="Financial_CommencementForecast_wrapper" runat="server" clientidmode="Static">
                                           <asp:Label ID="lblFinancial_CommencementForecast" runat="server">Commencement Forecast</asp:Label>
                                           <asp:TextBox ID="txtFinancial_CommencementForecast" runat="server" 
                                               CssClass="datepicker double-line-label-field"></asp:TextBox>
                                        </div>
                                        <div ID="divLeaseAdminDocuments" runat="server" ClientIDMode="Static">
                                        <label>
                                            Carrier Signature Notarized</label>
                                        <asp:TextBox ID="txtExecDocs_CarrierSignature" CssClass="datepicker" runat="server"></asp:TextBox>
                                        <asp:DropDownList ID="ddlExecDocs_CarrierSignature" ClientIDMode="Static" CssClass="split-r double-line-label-field" runat="server">
                                                <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                                                <asp:ListItem Text="Yes" Value="yes"></asp:ListItem>
                                                <asp:ListItem Text="No" Value="no"></asp:ListItem>
                                       </asp:DropDownList>
                                       <br />
                                       <label>
                                           Sent to Signatory</label>
                                       <asp:TextBox ID="txtExecDocs_SendSignator" CssClass="datepicker" runat="server"></asp:TextBox>
                                       <br />
                                        <label>
                                           Back to Signatory</label>
                                       <asp:TextBox ID="txtExecDocs_BackSignator" CssClass="datepicker" runat="server"></asp:TextBox>
                                       <br />
                                        <label>
                                           Original Sent to UPS</label>
                                       <asp:TextBox ID="txtExecDocs_OrgSentUPS" CssClass="datepicker" runat="server"></asp:TextBox>
                                       <br />
                                        <label>
                                            Doc Loaded to CST</label>  
                                        <asp:DropDownList ID="ddlExecDocs_DocLoadCST" ClientIDMode="Static" CssClass="split-r double-line-label-field" runat="server">
                                                <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                                                <asp:ListItem Text="Yes" Value="yes"></asp:ListItem>
                                                <asp:ListItem Text="No" Value="no"></asp:ListItem>
                                       </asp:DropDownList>
                                      </div>
                                       
                                      <br />
                                      
                                      </div>
                                   <div class="right-col" >
                                        
                                   <div ID="financial_wrapper" runat="server" 
                                       clientidmode="Static">
                                       <h4>
                                           Financial</h4>
                                       <div id="divLeaseAdminFinancial" runat="server" ClientIDMode="Static">
                                           <label>
                                           Amendment Rent Affecting</label>
                                       <asp:DropDownList ID="ddlFinance_AmendRentAffect" ClientIDMode="Static" CssClass="split-r double-line-label-field" runat="server">
                                                <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                                                <asp:ListItem Text="Yes" Value="yes"></asp:ListItem>
                                                <asp:ListItem Text="No" Value="no"></asp:ListItem>
                                       </asp:DropDownList>
                                       <br />
                                       <label>
                                           One Time Fee</label>
                                       <asp:DropDownList ID="ddlFinance_OnTimeFee" ClientIDMode="Static" CssClass="split-r double-line-label-field" runat="server">
                                                <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                                                <asp:ListItem Text="Yes" Value="yes"></asp:ListItem>
                                                <asp:ListItem Text="No" Value="no"></asp:ListItem>
                                       </asp:DropDownList>
                                       <br />  
                                          </div> 
                                       <%--Document Link--%> 
                                        <div class="document-link" id="RentForecastAmount"  > 
                                           <asp:Label ID="lblFinancial_RentForecastAmount" runat="server">Rent Forecast Amount</asp:Label>
                                           <asp:TextBox ID="txtFinancial_RentForecastAmount" runat="server" 
                                               CssClass="moneyfield double-line-label-field" Width="170px"></asp:TextBox>
                                            <asp:HiddenField  ID="hidden_document_Financial_RentForecastAmount" ClientIDMode="Static" runat="server" />
                                        </div>
                                       <br />
                                       <asp:Label ID="lblFinancial_RevenueShare" runat="server">Revenue Share</asp:Label>
                                       <asp:DropDownList ID="ddlFinancial_RevenueShare" runat="server" 
                                           CssClass="split-r">
                                           <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                                           <asp:ListItem Text="Yes" Value="yes"></asp:ListItem>
                                           <asp:ListItem Text="No" Value="no"></asp:ListItem>
                                       </asp:DropDownList>
                                       <%--<asp:TextBox ID="txtFinancial_RevenueShare" CssClass="split-r" runat="server"></asp:TextBox>--%>
                                       <br />
                                      <div id="divLeaseAdminRevShare" runat="server" ClientIDMode="Static">
                                        <label>
                                           Revshare Loaded to CST Receivable</label>
                                       <asp:DropDownList ID="ddlFinanace_LoadCSTRecv" ClientIDMode="Static" CssClass="split-r double-line-label-field" runat="server">
                                                <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                                                <asp:ListItem Text="Yes" Value="yes"></asp:ListItem>
                                                <asp:ListItem Text="No" Value="no"></asp:ListItem>
                                       </asp:DropDownList>
                                       <br />
                                        <label>
                                           Revshare Loaded to CST Payable</label>
                                       <asp:DropDownList ID="ddlFinanace_LoadCSTPay" ClientIDMode="Static" CssClass="split-r double-line-label-field" runat="server">
                                                <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                                                <asp:ListItem Text="Yes" Value="yes"></asp:ListItem>
                                                <asp:ListItem Text="No" Value="no"></asp:ListItem>
                                       </asp:DropDownList>
                                       <br />
                                       </div>
                                       <asp:Label ID="lblFinancial_REMLeaseSequence" runat="server">REM Lease Sequence</asp:Label>
                                       <asp:TextBox ID="txtFinancial_REMLeaseSequence" runat="server" 
                                           CssClass="double-line-label-field"></asp:TextBox>
                                       <br />
                                       <asp:Label ID="lblFinancial_CostShareAgreement" runat="server">Cost Share Agreement</asp:Label>
                                       <asp:DropDownList ID="ddlFinancial_CostShareAgreement" runat="server" 
                                           CssClass="split-r double-line-label-field">
                                           <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                                           <asp:ListItem Text="Yes" Value="yes"></asp:ListItem>
                                           <asp:ListItem Text="No" Value="no"></asp:ListItem>
                                       </asp:DropDownList>
                                       <asp:TextBox ID="txtFinancial_CostShareAgreementAmount" runat="server" 
                                           CssClass="moneyfield split-r double-line-label-field"></asp:TextBox>
                                       <br />
                                       <div ID="cap_cost_recovery" runat="server" clientidmode="Static">
                                       </div>
                                       <asp:Label ID="lblFinancial_CapCostRecovery" runat="server">Cap Cost Recovery</asp:Label>
                                       <asp:DropDownList ID="ddlFinancial_CapCostRecovery" runat="server" 
                                           CssClass="split-r double-line-label-field">
                                           <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                                           <asp:ListItem Text="Yes" Value="yes"></asp:ListItem>
                                           <asp:ListItem Text="No" Value="no"></asp:ListItem>
                                       </asp:DropDownList>
                                       <asp:TextBox ID="txtFinancial_CapCostRecoveryAmount" runat="server" 
                                           CssClass="moneyfield split-r double-line-label-field"></asp:TextBox>
                                       <br />
                                       <asp:Label ID="lblFinancial_BulkPOAvailable" runat="server">Bulk PO Available</asp:Label>
                                       <asp:DropDownList ID="ddlFinancial_BulkPOAvailable" runat="server" 
                                           CssClass="split-r double-line-label-field">
                                           <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                                           <asp:ListItem Text="Yes" Value="yes"></asp:ListItem>
                                           <asp:ListItem Text="No" Value="no"></asp:ListItem>
                                       </asp:DropDownList>
                                       <br />
                                       <asp:Label ID="lblLOEHoldupReasonTypes_TerminationDate" runat="server" 
                                           Text="Termination Date"></asp:Label>
                                       <asp:TextBox ID="txtLOEHoldupReasonTypes_TerminationDate" runat="server" 
                                           CssClass="datepicker double-line-label-field"></asp:TextBox>
                                            </div>
                                       <div id="divLeaseAdminCommencement" runat="server" ClientIDMode="Static">                                   
                                          <div id="divCommencementPhase" style="width: 100%;height:40px;text-align:center;background:#F0F0F0;margin-top:10px;margin-bottom:20px;">
                                            <span id="lblCommencementPhase" style="width:100%;text-align:left;margin-left:5px;font-weight:bold;margin-top:10px;" >Commencement Phase</span>
                                       </div>
                                       <%--Document Link--%> 
                                       <div class="document-link" id="NTPtoCust"  >
                                            <label>NTP to Customer</label>
                                            <asp:TextBox ID="txtComDocs_NTPtoCust" CssClass="datepicker" runat="server"></asp:TextBox>
                                            <asp:HiddenField  ID="hidden_document_NTPtoCust" ClientIDMode="Static" runat="server" />
                                       </div>
                                       <label>
                                       Document Type</label>
                                       <asp:DropDownList ID="ddlComDocs_DocType" runat="server">
                                            <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                                       </asp:DropDownList>
                                        <br />
                                       <label>
                                           Commencement Dates</label>
                                       <asp:TextBox ID="txtComDocs_CommencementDate" CssClass="datepicker" runat="server"></asp:TextBox>
                                       <div id="divComDocDate" runat="server" >
                                           <h4>
                                               Document Date</h4>
                                           <label>
                                               Date Received at FSC</label>
                                           <asp:TextBox ID="txtComDocs_RecvFSCDate" CssClass="datepicker" runat="server"></asp:TextBox>
                                           <br />
                                           <label>
                                               Received By</label>
                                           <asp:DropDownList ID="ddlComDocs_RecvBy" runat="server">
                                                <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                                           </asp:DropDownList>
                                           <br />
                                           <%--Document Link--%> 
                                           <div class="document-link" id="RegCommencementLetterDate"  >
                                               <label>
                                                   Reg Commencement Letter Created</label>
                                               <asp:TextBox ID="txtComDocs_RegCommencementLetterDate" CssClass="datepicker" runat="server"></asp:TextBox>
                                               <asp:HiddenField  ID="hidden_document_RegCommencementLetterDate" ClientIDMode="Static" runat="server" />
                                           </div>
                                           <br />
                                           <label>
                                               REM Alert&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                           <asp:TextBox ID="txtComDocs_RemAlert" runat="server" 
                                               ClientIDMode="Static" 
                                               style="width: 180px; max-width:180px; margin-bottom:0px !important; " 
                                               TextMode="MultiLine"></asp:TextBox>
                                           <br />
                                       </div> 
                                       </div> 
                                       <div ID="loe_holdup_reason_wrapper" runat="server"  class="right-col" >
                                           <div ID="loe_holdup_reason_types_wrapper" runat="server" clientidmode="Static">
                                            <h4>LOE Holdup Reason Types</h4>
                                            <asp:ListBox ID="lstLOEHoldup" runat="server" SelectionMode="Multiple" 
                                                ToolTip="Click to Select a reason"></asp:ListBox>
                                            <label style="text-align: left;">
                                            Comments</label>
                                            <asp:TextBox ID="txtLOEHoldupReasonTypes_Comments" runat="server" Columns="20" 
                                                CssClass="txtareaComment" Rows="2" Text="A sample comment." 
                                                TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                       </div>
                                    </div>
                                   <div class="full-col align-right">
                                       <asp:Button ID="btnSubmit_Lease" runat="server"
                                                   Text="Save"
                                                   CssClass="ui-button ui-widget ui-state-default ui-corner-all"
                                                   OnClick="btnSubmit_Lease_Click"
                                                   OnClientClick="if (!beforeSaveLeaseTab()) {return false;}"
                                                   UseSubmitBehavior="false"  />
                                   </div>
                                   <br />
                                   <div class="cb">
                                   </div>
                                    <div class="full-col align-right" style="margin-bottom:30px;margin-top:20px;" id="divBtnAddLeaseComment" runat="server">
                                        <asp:LinkButton ID="link_addnew_lease" runat="server" CssClass="link-comment" 
                                            OnClientClick="return linkAddNewLeaseCommentClick();">Add New Comment</asp:LinkButton>
                                        <div ID="lease-comment-link">
                                        <%--<asp:LinkButton ID="link_cancel" runat="server" CssClass="link-comment link-comment-cancel" OnClientClick="HideAddComment();return linkCancelClick();">cancel</asp:LinkButton>
                                        <asp:LinkButton ID="link_post" runat="server" CssClass="link-comment" OnClientClick="linkPostClick();" OnClick="link_addnew_Click">post</asp:LinkButton>--%>
                                        </div>
                                    </div>
                                    <div class="application-comments-inner-lease" style="margin-bottom:20px;margin-top:30px;"  >
                                        <div  class="new-comment" id="divAddnewLeaseComment" runat="server">
                                            <div class="balloon">
                                                <asp:TextBox ID="txt_comment_lease" runat="server" Columns="5" 
                                                    CssClass="comment-textarea" Rows="2" TextMode="MultiLine"></asp:TextBox>
                                                <div class="balloon-bottom">
                                                </div>
                                            </div>
                                            <div class="author-info" 
                                                style="margin-top: 15px; font-size: 10pt; color: #a1a1a1;">
                                                <asp:Image ID="img_Author_LeaseAdmin" runat="server" AlternateText="profile-picture" 
                                                    ImageUrl="/images/avatars/default.png" Width="25" />
                                                <div id="commentLeaseButton" style="text-align:right;">
                                                    <asp:Button ID="btnLeaseAdminCancel" runat="server" ClientIDMode="Static" 
                                                        CssClass="sa-comment-button" 
                                                        OnClientClick="return linkCancelLeaseCommentClick();" Text="Cancel" />
                                               <asp:Button ID="btnLeaseAdminPost" runat="server" ClientIDMode="Static" CssClass="sa-comment-button"
                                                   NavigateUrl="javascript:void(0)" OnClientClick="linkLeasePostClick(); return false;" Text="Post" />
                                                </div>
                                            </div>
                                        </div>
                                   </div>
                                   <!-- #lease comment-->
                                </div>
                           </div>
                           <!-- #lease-->
                           <div ID="construction" runat="server" clientidmode="Static">
                               <div style="padding: 20px;overflow:auto;">
                                   <div ID="error_construction">
                                   </div>
                                   <div class="full-col align-right">
                                       <asp:Button ID="btnSubmit_Construction_Upper" runat="server"
                                                   Text="Save"
                                                   CssClass="buttonSubmit ui-button ui-widget ui-state-default ui-corner-all"
                                                   OnClick="btnSubmit_Construction_Click" 
                                                   OnClientClick="if (!constructionBeforeSave()) {return false;}"
                                                   UseSubmitBehavior="false"  />
                                   </div>
                                   <div class="left-col">
                                       <h4>
                                           Preconstruction</h4>
                                       <%--Document Link--%> 
                                       <div class="document-link" id="Preconstruction_PreconstructionSitewalk"  >
                                           <asp:Label ID="lblPreconstruction_PreconstructionSitewalk" runat="server">Preconstruction Sitewalk</asp:Label>
                                           <asp:TextBox ID="txtPreconstruction_PreconstructionSitewalk" runat="server" 
                                               CssClass="datepicker split double-line-label-field"></asp:TextBox>
                                           <asp:DropDownList ID="ddlPreconstruction_PreconstructionSitewalk" 
                                               runat="server" CssClass="split double-line-label-field">
                                           </asp:DropDownList>
                                           <asp:HiddenField  ID="hidden_document_Preconstruction_PreconstructionSitewalk" ClientIDMode="Static" runat="server" />
                                       </div>
                                       <br/>
                                       <%--Document Link--%>
                                       <div class="document-link" id="NTPIssuedDateStatus"  >
                                           <asp:Label ID="lblPreconstruction_NTPIssuedDateStatus" runat="server">NTP Issued Date </asp:Label>
                                           <asp:TextBox ID="txtPreconstruction_NTPIssuedDateStatus" runat="server" 
                                               CssClass="datepicker split double-line-label-field"></asp:TextBox>
                                           <asp:DropDownList ID="ddlPreconstruction_NTPIssuedDateStatus" runat="server" 
                                               CssClass="split-r double-line-label-field">
                                               <asp:ListItem Text="select" Value=""></asp:ListItem>
                                               <asp:ListItem Text="Yes" Value="yes"></asp:ListItem>
                                               <asp:ListItem Text="No" Value="no"></asp:ListItem>
                                           </asp:DropDownList>
                                           <asp:HiddenField  ID="hidden_document_NTPIssuedDateStatus" ClientIDMode="Static" runat="server" />
                                       </div>
                                       <br/>
                                       <asp:Label ID="lblPreconstruction_NTPVerified" runat="server">NTP Verified</asp:Label>
                                       <asp:TextBox ID="txtPreconstruction_NTPVerified" runat="server" 
                                           CssClass="datepicker double-line-label-field"></asp:TextBox>
                                       <br/>
                                       <%-- Early Installation --%>
                                       <div ID="early_installation_wrapper" runat="server" clientidmode="Static">
                                           <h4>
                                               Early Installation</h4>
                                           <asp:Label ID="lblEarlyInstallation_ViolationDiscovered" runat="server">Discovered</asp:Label>
                                           <asp:TextBox ID="txtEarlyInstallation_ViolaitonDiscovered" runat="server" 
                                               CssClass="datepicker double-line-label-field"></asp:TextBox>
                                           <br />
                                           <%--Document Link--%>
                                           <div class="document-link" id="EarlyInstalltion_ViolationLetterSent"  >
                                               <asp:Label ID="lblEarlyInstalltion_ViolationLetterSent" runat="server">Violation Letter Sent</asp:Label>
                                               <asp:TextBox ID="txtEarlyInstallation_ViolationLetterSent" runat="server" 
                                               CssClass="datepicker double-line-label-field"></asp:TextBox>
                                               <asp:HiddenField  ID="hidden_document_EarlyInstalltion_ViolationLetterSent" ClientIDMode="Static" runat="server" />
                                            </div>
                                           <br />
                                           <%--Document Link--%>
                                           <div class="document-link" id="EarlyInstallation_ViolationInvoiceCreatedByFinops"  >
                                               <asp:Label ID="lblEarlyInstallation_ViolationInvoiceCreatedByFinops" 
                                                   runat="server">Violation Invoice Created By Finops</asp:Label>
                                               <asp:TextBox ID="txtEarlyInstallation_ViolationInvoiceCreatedByFinops" 
                                                   runat="server" CssClass="datepicker double-line-label-field"></asp:TextBox>
                                                <asp:HiddenField  ID="hidden_document_EarlyInstallation_ViolationInvoiceCreatedByFinops" ClientIDMode="Static" runat="server" />
                                            </div>
                                           <br />
                                           <asp:Label ID="lblEarlyInstallation_ViolationInvoiceNumber" runat="server">Violation Invoice Number</asp:Label>
                                           <asp:TextBox ID="txtEarlyInstallation_ViolationInvoiceNumber" runat="server" 
                                               CssClass="double-line-label-field"></asp:TextBox>
                                           <br />
                                           <asp:Label ID="lblEarlyInstallation_ViolationFeeReceived" runat="server">Violation Fee Received</asp:Label>
                                           <asp:TextBox ID="txtEarlyInstallation_ViolationFeeReceived" runat="server" 
                                               CssClass="datepicker double-line-label-field"></asp:TextBox>
                                       </div>
                                       <%-- Extension Notice --%>
                                       <div ID="extension_notice_wrapper" runat="server" clientidmode="Static">
                                           <h4>
                                               Extension Notice</h4>
                                           <asp:Label ID="lblExtensionNotice_Reveeived" runat="server">Received</asp:Label>
                                           <asp:TextBox ID="txtExtensionNotice_Received" runat="server" 
                                               CssClass="datepicker double-line-label-field"></asp:TextBox>
                                           <br />
                                           <asp:Label ID="lblExtensionNotice_FeeReceived" runat="server">Fee Received</asp:Label>
                                           <asp:TextBox ID="txtExtensionNotice_FeeReceived" runat="server" 
                                               CssClass="datepicker double-line-label-field"></asp:TextBox>
                                           <br />
                                           <asp:Label ID="lblExtensionNotice_InvoiceCreatedByFinops" runat="server">Invoice Created By Finops</asp:Label>
                                           <asp:TextBox ID="txtExtensionNotice_InvoiceCreatedByFinops" runat="server" 
                                               CssClass="datepicker double-line-label-field"></asp:TextBox>
                                           <br />
                                           <asp:Label ID="lblExtensionNotice_FeeInvoiceNumber" runat="server">Fee Invoice Number</asp:Label>
                                           <asp:TextBox ID="txtExtensionNotice_FeeInvoiceNumber" runat="server" 
                                               CssClass="double-line-label-field"></asp:TextBox>
                                           <br />
                                           <label>
                                           150 days Post NTP</label>
                                           <asp:Label ID="lblExtensionNotice_150daysPostNTP" runat="server" 
                                               CssClass="double-line-label-field"></asp:Label>
                                           <label>
                                           180 days Post NTP</label>
                                           <asp:Label ID="lblExtensionNotice_180daysPostNTP" runat="server" 
                                               CssClass="double-line-label-field"></asp:Label>
                                           <label>
                                           210 days Post NTP</label>
                                           <asp:Label ID="lblExtensionNotice_210daysPostNTP" runat="server" 
                                               CssClass="double-line-label-field"></asp:Label>
                                       </div>
                                       <%-- Equipment --%>
                                       <div ID="equipment_wrapper" runat="server" clientidmode="Static">
                                           <h4>
                                               Equipment Removal</h4>
                                           <asp:Label ID="lblEquipment_Removed" runat="server">Equipment Removed</asp:Label>
                                           <asp:TextBox ID="txtEquipment_Removed" runat="server" 
                                               CssClass="datepicker double-line-label-field"></asp:TextBox>
                                           <br />
                                           <asp:Label ID="lblEquipment_FailureToRemoveViolation" runat="server">Failure To Remove Violation</asp:Label>
                                           <asp:DropDownList ID="ddlEquipment_FailureToRemoveViolation" runat="server" 
                                               CssClass="double-line-label-field">
                                               <asp:ListItem Text="select" Value=""></asp:ListItem>
                                               <asp:ListItem Text="Yes" Value="yes"></asp:ListItem>
                                               <asp:ListItem Text="No" Value="no"></asp:ListItem>
                                           </asp:DropDownList>
                                           <br />
                                           <asp:Label ID="lblEquipment_FailureToRemoveViolationLetterSent" runat="server">Failure To Remove Violation Letter Sent</asp:Label>
                                           <asp:TextBox ID="txtEquipment_FailureToRemoveViolationLetterSent" 
                                               runat="server" CssClass="datepicker double-line-label-field"></asp:TextBox>
                                           <br />
                                           <asp:Label ID="lblEquipment_FailureToRemoveViolationLetterSubmittedToCst" 
                                               runat="server">Failure to Remove Violation Letter Submitted To Cell Site Tracker</asp:Label>
                                           <asp:TextBox ID="txtEquipment_FailureToRemoveViolationLetterSubmittedToCst" 
                                               runat="server" CssClass="datepicker double-line-label-field"></asp:TextBox>
                                           <br />
                                           <asp:Label ID="lblEquipment_ViolationEquipmentRemovalConfirmedSubmittedToCst" 
                                               runat="server">Violation Equipment Removal Confirmed Submitted To Cell Site Tracker</asp:Label>
                                           <asp:TextBox ID="txtEquipment_ViolationEquipmentRemovalConfirmedSubmittedToCst" 
                                               runat="server" CssClass="datepicker double-line-label-field"></asp:TextBox>
                                       </div>
                                   </div>
                                   <div class="right-col">
                                       <div id="post_construction_wrapper" runat="server" clientidmode="Static">
                                          <h4>Post Construction</h4>
                                          <asp:Label ID="lblPostConstruction_Ordered" runat="server">Ordered</asp:Label>
                                          <asp:TextBox ID="txtPostConstruction_Ordered" runat="server" 
                                              CssClass="datepicker split"></asp:TextBox>
                                          <asp:DropDownList ID="ddlPostConstruction_Ordered" runat="server" 
                                              CssClass="split">
                                          </asp:DropDownList>
                                          <br/>
                                          <%--Document Link--%>
                                          <div class="document-link" id="PostConstruction_Sitewalk" >
                                              <asp:Label ID="lblPostConstruction_Sitewalk" runat="server">Sitewalk</asp:Label>
                                              <asp:TextBox ID="txtPostConstruction_SiteWalk" runat="server" 
                                                  CssClass="datepicker split"></asp:TextBox>
                                              <asp:DropDownList ID="ddlPostConstruction_SiteWalk" runat="server" 
                                                  CssClass="split">
                                              </asp:DropDownList>
                                              <asp:HiddenField  ID="hidden_document_PostConstruction_Sitewalk" ClientIDMode="Static" runat="server" />
                                          </div>
                                          <br/>
                                          <asp:Label ID="lblPostConstruction_ConstructionCompleted" runat="server">Construction Completed</asp:Label>
                                          <asp:TextBox ID="txtPostConstruction_ConstructionCompleted" runat="server" 
                                              CssClass="datepicker split double-line-label-field"></asp:TextBox>
                                          <asp:DropDownList ID="ddlPostConstruction_ConstructionCompleted" runat="server" 
                                              CssClass="split double-line-label-field">
                                          </asp:DropDownList>
                                       </div>
                                       
                                       <div ID="close_out_wrapper" runat="server" clientidmode="Static">
                                           <h4>
                                               Close Out</h4>
                                           <ul class="information-list">
                                               <li ID="liCloseOut_DueDate" runat="server" clientidmode="Static">
                                                   <asp:Label ID="lblCloseOut_DueDateHeader" runat="server">Docs Due Date</asp:Label>
                                                   <asp:Label ID="lblCloseOut_DueDate" runat="server"></asp:Label>
                                               </li>
                                               <li ID="liCloseOut_Received" runat="server" clientidmode="Static">
                                                   <asp:Label ID="lblCloseOut_Received" runat="server">Received</asp:Label>
                                                   <asp:TextBox ID="txtCloseOut_Received" runat="server" CssClass="datepicker"></asp:TextBox>
                                               </li>
                                               <li ID="liCloseOut_NoticeSent" runat="server" clientidmode="Static">
                                                   <asp:Label ID="lblCloseOut_NoticeSent" runat="server">Notice Sent</asp:Label>
                                                   <asp:TextBox ID="txtCloseOut_NoticeSent" runat="server" CssClass="datepicker"></asp:TextBox>
                                               </li>
                                               <li ID="liCloseOut_FeeRequired" runat="server" clientidmode="Static">
                                                   <asp:Label ID="lblCloseOut_FeeRequired" runat="server">Fee Required</asp:Label>
                                                   <asp:DropDownList ID="ddlCloseOut_FeeRequired" runat="server" 
                                                       CssClass="double-line-label-field">
                                                       <asp:ListItem Text="select" Value=""></asp:ListItem>
                                                       <asp:ListItem Text="Yes" Value="yes"></asp:ListItem>
                                                       <asp:ListItem Text="No" Value="no"></asp:ListItem>
                                                   </asp:DropDownList>
                                               </li>
                                               <li ID="liCloseOut_FeeInvoiceSent" runat="server" clientidmode="Static">
                                                   <asp:Label ID="lblCloseOut_FeeInvoiceSent" runat="server">Fee Invoice Sent</asp:Label>
                                                   <asp:TextBox ID="txtCloseOut_FeeInvoiceSent" runat="server" 
                                                       CssClass="datepicker"></asp:TextBox>
                                               </li>
                                               <li ID="liCloseOut_FeeInvoiceCreatedByFinOps" runat="server" 
                                                   clientidmode="Static">
                                                   <asp:Label ID="lblCloseOut_FeeInvoiceCreatedByFinops" runat="server">Fee Invoice Created By Finops</asp:Label>
                                                   <asp:TextBox ID="txtCloseOut_FeeInvoiceCreatedByFinops" runat="server" 
                                                       CssClass="datepicker double-line-label-field"></asp:TextBox>
                                               </li>
                                               <li ID="liCloseOut_FeeInvoiceNumber" runat="server" clientidmode="Static">
                                                   <asp:Label ID="lblCloseOut_FeeInvoiceNumber" runat="server">Fee Invoice Number</asp:Label>
                                                   <asp:TextBox ID="txtCloseOut_FeeInvoiceNumber" runat="server" 
                                                       CssClass="double-line-label-field"></asp:TextBox>
                                               </li>
                                               <li ID="liCloseOut_FeeReceived" runat="server" clientidmode="Static">
                                                   <asp:Label ID="lblCloseOut_FeeReceived" runat="server">Fee Received</asp:Label>
                                                   <asp:TextBox ID="txtCloseOut_FeeReceived" runat="server" 
                                                       CssClass="datepicker double-line-label-field"></asp:TextBox>
                                               </li>
                                               <li ID="liCloseOut_FinalSiteApproval" runat="server" clientidmode="Static">
                                               <%--Document Link--%>
                                               <div class="document-link" id="CloseOut_FinalSiteApproval" >
                                                   <asp:Label ID="lblCloseOut_FinalSiteApproval" runat="server">Final Site Approval</asp:Label>
                                                   <asp:TextBox ID="txtCloseOut_FinalSiteApproval" runat="server" 
                                                       CssClass="datepicker split double-line-label-field"></asp:TextBox>
                                                   <asp:DropDownList ID="ddlCloseOut_FinalSiteApproval" runat="server" 
                                                       CssClass="split double-line-label-field">
                                                   </asp:DropDownList>
                                                   <asp:HiddenField  ID="hidden_document_CloseOut_FinalSiteApproval" ClientIDMode="Static" runat="server" />
                                               </div>
                                               </li>
                                               <li ID="liCloseOut_AsBuilt" runat="server" clientidmode="Static">
                                               <%--Document Link--%>
                                               <div class="document-link" id="CloseOut_AsBuilt" >
                                                   <asp:Label ID="lblCloseOut_AsBuilt" runat="server">As Built</asp:Label>
                                                   <asp:TextBox ID="txtCloseOut_AsBuilt" runat="server" CssClass="datepicker"></asp:TextBox>
                                                   <asp:HiddenField  ID="hidden_document_CloseOut_AsBuilt" ClientIDMode="Static" runat="server" />
                                               </div>
                                               </li>
                                           </ul>
                                       </div>
                                       <div ID="market_handoff_wrapper" runat="server">
                                           <h4>
                                               Market Handoff</h4>
                                           <%--Document Link--%>
                                           <div class="document-link" id="MarketHandoff_Status"  >
                                               <asp:Label ID="lblMarketHandoff_Status" runat="server">Market Handoff</asp:Label>
                                               <asp:TextBox ID="txtMarketHandoff_Date" runat="server" 
                                                   CssClass="datepicker split"></asp:TextBox>
                                               <asp:DropDownList ID="ddlMarketHandoff_Status" runat="server" CssClass="split">
                                               </asp:DropDownList>
                                               <asp:HiddenField  ID="hidden_document_MarketHandoff_Status" ClientIDMode="Static" runat="server" />
                                           </div>
                                           <br/>
                                       </div>
                                   </div>
                                   <div class="full-col align-right">
                                       <asp:Button ID="btnSubmit_Construction" runat="server" 
                                                   Text="Save"
                                                   CssClass="buttonSubmit ui-button ui-widget ui-state-default ui-corner-all"
                                                   OnClick="btnSubmit_Construction_Click" 
                                                   OnClientClick="if (!constructionBeforeSave()) {return false;}"
                                                   UseSubmitBehavior="false"  />
                                   </div>
                                   <div class="cb">
                                   </div>
                               </div>
                           </div>
                           <!-- #construction --><span ID="hidden_rv_count" style="display:none">1</span>
                           <span ID="rv_bufferIndex" style="display:none"></span><span ID="rv_pageIndex" 
                               style="display:none"></span>
                           <asp:HiddenField ID="hidden_revision_id" runat="server" />
                           <asp:HiddenField ID="hidden_revision_template" runat="server" />
                           <input type="hidden" id="hidden_RV_IsNew" />
                           <input type="hidden" id="hidden_RV_IsSave" />
                           <div ID="revisions_wrapper" runat="server" clientidmode="Static">
                               <div ID="revisions">
                                   <div style="padding: 20px;overflow:auto;">
                                       <asp:UpdatePanel ID="upPnlRevisions" runat="server" UpdateMode="Conditional">
                                           <ContentTemplate>
                                               <asp:HiddenField ID="hidden_revisions_popup" runat="server" 
                                                   ClientIDMode="Static" />
                                               <asp:HiddenField ID="hidden_revisions" runat="server" />
                                               <asp:HiddenField ID="hidden_revision_count" runat="server" />
                                               <div>
                                                   <div class="PanelPageNumber">
                                                       <span ID="spanPageNumberCurrent" class="textPageNumber">0</span>
                                                       <span class="textPageNumber">of</span> <span ID="spanPageNumberMax" 
                                                           class="textPageNumber">0</span>
                                                   </div>
                                                   <div class="PanelPageControl">
                                                       <asp:Button ID="btnRVDelete" runat="server" ClientIDMode="Static" 
                                                           onclick="btnRVDelete_Click" 
                                                           OnClientClick="return openDeleteAppButtonPopup('Lease App Revision',event,getRVID());" 
                                                           Text="Delete" />
                                                       &nbsp;&nbsp;&nbsp;
                                                       <input id="btnRevisionPrevious" type="button" value="Previous" />
                                                       &nbsp;&nbsp;&nbsp;
                                                       <input id="btnRevisionNext" type="button" value="Next" />
                                                       &nbsp;&nbsp;&nbsp;
                                                       <input id="btnRevisionAdd" type="button" value="Add New" />
                                                   </div>
                                               </div>
                                               <div class="cb">
                                               </div>
                                               <div ID="rv_message_alert">
                                               </div>
                                               <asp:Panel ID="pnlRevisionSlideshow" runat="server">
                                                   <asp:Panel ID="Panel3" runat="server">
                                                       <div ID="Test-div" class="full-col align-right" 
                                                           style="margin-top: 20px; margin-bottom: 10px;">
                                                           <asp:Button ID="btnSubmitRevisionTop" runat="server"                                                                        
                                                                       CssClass="ui-button ui-widget ui-state-default ui-corner-all"
                                                                       Text="Save"
                                                                       OnClick="btnSubmitRevision_Click" 
                                                                       OnClientClick="if (!funcRVCheckBeforeSave()) {return false;}"
                                                                       UseSubmitBehavior="false"  />
                                                           <div class="left-col">
                                                               <div ID="divRevisions" runat="server" clientidmode="Static">
                                                                   <h4>Lease App Revisions</h4>
                                                                   <asp:HiddenField ID="hidden_document_RevisionUpdatedAt" runat="server" />
                                                                   <%--Document Link--%>
                                                                   <div class="document-link" id="RevisionReveivedDate"  >
                                                                       <asp:Label ID="lblRevisionReveivedDate" runat="server">Received</asp:Label>
                                                                       <asp:TextBox ID="txtRevisionReceivedDate" runat="server" CssClass="datepicker"></asp:TextBox>
                                                                       <asp:HiddenField  ID="hidden_document_RevisionReveivedDate" ClientIDMode="Static" runat="server" />
                                                                   </div>
                                                                   <br />
                                                                   <asp:Label ID="lblRevisionFeeReceived" runat="server">Fee Received</asp:Label>
                                                                   <asp:TextBox ID="txtRevisionFeeReceivedDate" runat="server" CssClass="datepicker"></asp:TextBox>
                                                                   <br />
                                                                   <asp:Label ID="lblRevisionCheckNo" runat="server">PO Check Number</asp:Label>
                                                                   <asp:TextBox ID="txtRevisionCheckNo" runat="server"></asp:TextBox>
                                                                   <br />
                                                                   <asp:Label ID="lblRevisionFeeAmount" runat="server">Fee Amount</asp:Label>
                                                                   <asp:TextBox ID="txtRevisionFeeAmount" runat="server" CssClass="moneyfield"></asp:TextBox>
                                                               </div>
                                                           </div>
                                                           <div class="full-col align-right" style="margin-bottom: 30px;">
                                                               <asp:Button ID="btnSubmitRevisionBottom" runat="server" 
                                                                           CssClass="ui-button ui-widget ui-state-default ui-corner-all"
                                                                           Text="Save"
                                                                           OnClick="btnSubmitRevision_Click" 
                                                                           OnClientClick="if (!funcRVCheckBeforeSave()) {return false;}"
                                                                           UseSubmitBehavior="false" />
                                                           </div>
                                                       </div>
                                                   </asp:Panel>
                                               </asp:Panel>
                                               <div class="cb">
                                               </div>
                                           </ContentTemplate>
                                       </asp:UpdatePanel>
                                   </div>
                               </div>
                               <!-- #Revisions -->
                           </div>
                       </div>
                       <!-- #tabs2 -->
                       <div class="cb">
                       </div>
                      
               </div>
               <!-- .application-process -->
               <div class="application-comments">
                   <h2 id="h2-comments" style="width:140px;">Comments</h2>
                   <div class="comments-wrapper">
                       <asp:UpdatePanel id="upComments" updatemode="Always" runat="server">
                           <ContentTemplate>
                               <div class="blah-wrapper" style="width: 100%; height: 0px; position: relative; overflow: hidden;" id ="divNewComment_test">
                                   <div style="position: absolute; top: -200px; width: 100%; height: 260px; background: none;" id="blah">
                                       <div class="new-comment" style="display: none;" >
                                           <div class="balloon">
                                               <asp:TextBox ID="txtComments" Rows="2" Columns="5" CssClass="comment-textarea" runat="server"
                                                   TextMode="MultiLine"></asp:TextBox>

                                               <div class="balloon-bottom">
                                               </div>
                                           </div>
                                        
                                           <div class="author-info" style="margin-top: 15px; font-size: 10pt; color: #a1a1a1;">
                                               <asp:Image ID="imgAuthor" Width="25" ImageUrl="/images/avatars/default.png" AlternateText="profile-picture" runat="server" />
                                               <asp:Button ID="btnPost"  ClientIDMode="Static" runat="server" OnClientClick="this.disabled = true; this.value = 'Processing...';showLoadingSpinner('h2-comments');" OnClick="btnPost_Click" Text="Post" UseSubmitBehavior="false"/>
                                               <asp:Button ID="btnCommentCancel" CssClass="comment-cancel" ClientIDMode="Static" runat="server" OnClientClick="return false;" Text="Cancel" />
                                           </div>
                                       </div>
                                   </div>
                                   <div class="clear-all">
                                   </div>
                               </div>

                               <div runat="server" id="divComment1" class="comment">
                                   <div class="balloon">
                                       <p runat="server" id="pComment1">
                                       </p>
                                       <div class="balloon-bottom">
                                       </div>
                                   </div>
                                   <div class="author-info" style="margin-top: 15px; font-size: 10pt; color: #a1a1a1;">
                                       <asp:Image ID="imgUserComment1"  Width="25" ImageUrl="/images/avatars/default.png"
                                           ToolTip="profile-picture" runat="server" />
                                           <p runat="server" id="pCommenter1">
                                           </p>
                                           <div id="pnlDocument1" runat="server" style="width:100%;text-align:left;" ></div>
                                           <asp:ImageButton id="btnDeleteComment1" ImageUrl="/images/icons/Erase-Greyscale.png" ToolTip="Delete Comment" Width="12" runat="server"
                                           OnClick="btnDeleteComment_Click" CssClass="btn-comment-delete" />
                                   </div>
                               </div>

                               <div runat="server" id="divComment2" class="comment">
                                   <div class="balloon">
                                       <p runat="server" id="pComment2">
                                       </p>
                                       <div class="balloon-bottom">
                                       </div>
                                   </div>
                                   <div class="author-info" style="margin-top: 15px; font-size: 10pt; color: #a1a1a1;">
                                       <asp:Image ID="imgUserComment2"  Width="25" ImageUrl="/images/avatars/default.png"
                                           ToolTip="profile-picture" runat="server" />
                                            <p runat="server" id="pCommenter2">
                                           </p>
                                            <div id="pnlDocument2" runat="server" style="width:100%;text-align:left;" ></div>
                                           <asp:ImageButton id="btnDeleteComment2" ImageUrl="/images/icons/Erase-Greyscale.png" ToolTip="Delete Comment" Width="12" runat="server"
                                           OnClick="btnDeleteComment_Click" CssClass="btn-comment-delete" />
                                   </div>
                               </div>

                               <div runat="server" id="divComment3" class="comment">
                                   <div class="balloon">
                                       <p runat="server" id="pComment3">
                                       </p>
                                       <div class="balloon-bottom">
                                       </div>
                                   </div>
                                   <div class="author-info" style="margin-top: 15px; font-size: 10pt; color: #a1a1a1;">
                                       <asp:Image ID="imgUserComment3"  Width="25" ImageUrl="/images/avatars/default.png"
                                           ToolTip="profile-picture" runat="server" />
                                           <p runat="server" id="pCommenter3">
                                           </p>
                                           <div id="pnlDocument3" runat="server" style="width:100%;text-align:left;" ></div>
                                           <asp:ImageButton id="btnDeleteComment3" ImageUrl="/images/icons/Erase-Greyscale.png" ToolTip="Delete Comment" Width="12" runat="server"
                                           OnClick="btnDeleteComment_Click" CssClass="btn-comment-delete" />
                                   </div>
                               </div>

                               <div runat="server" id="divComment4" class="comment">
                                   <div class="balloon">
                                       <p runat="server" id="pComment4">
                                       </p>
                                       <div class="balloon-bottom">
                                       </div>
                                   </div>
                                   <div class="author-info" style="margin-top: 15px; font-size: 10pt; color: #a1a1a1;">
                                       <asp:Image ID="imgUserComment4"  Width="25" ImageUrl="/images/avatars/default.png"
                                           ToolTip="profile-picture" runat="server" />
                                           <p runat="server" id="pCommenter4">
                                           </p>
                                           <div id="pnlDocument4" runat="server" style="width:100%;text-align:left;" ></div>
                                           <asp:ImageButton id="btnDeleteComment4" ImageUrl="/images/icons/Erase-Greyscale.png" ToolTip="Delete Comment" Width="12" runat="server"
                                           OnClick="btnDeleteComment_Click" CssClass="btn-comment-delete" />
                                   </div>
                               </div>    
                            
                                <a id="add_new" runat="server" clientidmode="Static" class="add-new" href="javascript:void(0)">add new</a> 

                                 <script type="text/javascript">
                                     function AllCommentCancel() {
   //                                      $("#divNewComment_test").hide();
   //                                      $(".add-new").show();
   //                                      $("#btnAddNew").show();
   //                                      setupCommentsLocal();
                                         return false;
                                     }
                                     function AddNewFunction() {
   //                                      var parentDiv = $(this).parent();
   //                                      $('.new-comment', parentDiv).toggle();
   //                                      $(this).hide();
   //                                      $('.post, .cancel', parentDiv).show();
   //                                      $('.comment-textarea', parentDiv).height('80px');
   //                                      $('.blah-wrapper', parentDiv).animate({
   //                                          height: "190px"
   //                                      }, 500);
   //                                      $('#blah', parentDiv).animate({
   //                                          top: "0px"
   //                                      }, 500);
                                         $("#divNewComment_test").show();
                                         $("#btnAddNew").hide();
                                         return false;
                                     }
                                               </script>
                                <a class="fancy-button" style="display: inline-block; text-align: center; width: 88%; margin: 0 auto;" id="btnAllComment" runat="server">more comments</a>
                                <asp:Button ID="btnAllRelateComment" OnClick="btnAllRelateComment_Click" OnClientClick="showLoadingSpinner('h2-comments');" ClientIDMode="Static" runat="server" CssClass="fancy-button AllReleateCommentButton" Text="Show related comments" />
                               <asp:Button ID="btnRefreshComment" OnClick="btnRefreshComment_Click" ClientIDMode="Static" runat="server" CssClass="not-displayed" />
                               <asp:HiddenField ID="hidden_count_main_comment" runat="server"/>
                               <asp:HiddenField ID="hidden_deleted_comment_id" Value="" runat="server" ClientIDMode="Static" />
                               <asp:HiddenField ID="hidden_document_all_document_comments" runat="server"/>
                               
                           </ContentTemplate>
                           <Triggers>
                               <asp:AsyncPostBackTrigger ControlID="btnRefreshComment" EventName="Click" />
                           </Triggers>
                       </asp:UpdatePanel>

                    
                   </div>
                   <!-- .comments-wrapper -->
               </div>
               <!-- .application-comments -->
               <div class="cb">
               </div>
               <!-- end wrapper -->
           </div>
       </div>
        </label>
    </asp:Panel>
    <!-- end content -->
    <asp:HiddenField ID="hiddenErr" runat="server" />
    <asp:HiddenField ID="hiddenCuerrentTab" runat="server" />
    <asp:HiddenField ID="hidDocumentDownload" runat="server" />
    <asp:HiddenField ID="hiddenCurrentSaCommentCount" runat="server" ClientIDMode="Static" Value="0" />
</asp:Content>