﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.BLL;
using TrackIT2.DAL;
using TrackIT2.Objects;
using System.Collections;
using System.Text;
using System.Text.RegularExpressions;
using System.Configuration;

namespace TrackIT2.LeaseApplications
{
    public partial class TMTtransfer : System.Web.UI.Page
    {
        private const String QUERYSTRING_TMOAPP = "tmoid";
        private const String QUERYSTRING_SITE = "site";
        private const String QUERYSTRING_APPTYPE = "apptype";
        private const String QUERYSTRING_CUSTOMER = "customer";
        private const String QUERYSTRING_DBA = "dba";
        private Hashtable m_htVarCharInvalidField;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.tmoURL.Value = ConfigurationManager.AppSettings["TMobileURL"];
                if ((Request.QueryString[QUERYSTRING_TMOAPP] != null) &&
                    (Request.QueryString[QUERYSTRING_SITE] != null) &&
                    (Request.QueryString[QUERYSTRING_APPTYPE] != null))
                {
                    BindLookupLists();
                    initialdata();
                }
                else
                {
                    Response.Redirect(Request.Url.GetLeftPart(UriPartial.Authority));
                }
            }
        }

        private void initialdata()
        {
            //this.ddlAppType.Items.FindByText("").Selected = true;
            String TMOAppId = (HttpUtility.UrlDecode(Request.QueryString[QUERYSTRING_TMOAPP]));
            String SiteId = (HttpUtility.UrlDecode(Request.QueryString[QUERYSTRING_SITE]));
            this.ddlActivityType.Items.FindByText("In Review").Selected = true;
            this.ddlAppStatus.Items.FindByText("Active").Selected = true;
            this.txt_TMOAppId.Text = TMOAppId;
            this.txt_Site.Text = SiteId;
            if (Request.QueryString[QUERYSTRING_CUSTOMER] != null)
            {
                this.txtLegalEntityName.Text = (HttpUtility.UrlDecode(Request.QueryString[QUERYSTRING_CUSTOMER]));
            }
            if (Request.QueryString[QUERYSTRING_DBA] != null)
            {
                this.txtDBA.Text = (HttpUtility.UrlDecode(Request.QueryString[QUERYSTRING_DBA]));
            }
        }

      
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            string status = null;

            if (Page.IsValid)
            {
                m_htVarCharInvalidField = new Hashtable();
                lease_applications row = new lease_applications();
                Boolean blnIsValid = true;  //Use for double-check data.
                Boolean blnUIDIsValid = true;

                //Application Details
                row.leaseapp_type_id = Utility.PrepareInt(this.ddlAppType.SelectedValue);
                row.leaseapp_rcvd_date = Utility.PrepareDate(DateTime.Now.ToString(Globals.SHORT_DATE_FORMAT));

                if (row.leaseapp_type_id == null)
                {
                    blnIsValid = false;
                }

                row.leaseapp_activity_id = Utility.PrepareInt(this.ddlActivityType.SelectedValue);

                if (row.leaseapp_activity_id == null)
                {
                    blnIsValid = false;
                }

                row.leaseapp_status_id = Utility.PrepareInt(this.ddlAppStatus.SelectedValue);

                //Customer Details
                row.customer_id = Utility.PrepareInt(this.ddlCustomer.SelectedValue);

                //Site
                row.site_uid = Utility.PrepareString(txt_Site.Text.ToUpper());
                if (row.site_uid == null)
                {
                    blnIsValid = false;
                }
                else
                {
                    if (BLL.Site.Search(row.site_uid) == null)
                    {
                        blnIsValid = false;
                        blnUIDIsValid = false;
                        Utility.SettingErrorColorForTextbox(txt_Site);
                    }
                    else
                    {
                        Utility.ClearErrorColorForTextbox(txt_Site);
                    }
                }

                // TMO Application
                row.TMO_AppID = Utility.PrepareString(txt_TMOAppId.Text.ToUpper());

                int userId = -1;
                HttpContext currContext = HttpContext.Current;
                if (currContext != null)
                {
                    if (currContext.User.Identity.IsAuthenticated)
                    {
                        if (currContext.Session["userId"] != null)
                        {
                            userId = (int)currContext.Session["userId"];
                        }
                        else
                        {
                            userId = Security.GetUserId(currContext.User.Identity.Name);
                        }
                    }
                }
                
                if (btnSubmitCreateApps.Text != "Save")
                {
                    //Add
                    if (blnIsValid)
                    {
                        status = LeaseApplication.Add(row);

                        // A null status message indicates success.
                        if (string.IsNullOrEmpty(status))
                        {
                           // Upsert import status in [t_TrackitImports]
                           var currentUser = BLL.User.Search(userId);                           
                           var import = new TrackitImportApplication.TrackiTImportServiceClient();
                           import.UpsertImportApplication(HMACHelper.GenerateHMAC(), null,
                                                          Utility.PrepareString(txt_TMOAppId.Text.ToUpper()),
                                                          TrackitImportApplication.eTrackiTImportStatus.imported,
                                                          currentUser.email);
                           
                            // Redirect to edit page with created id. Keep save button disabled
                            // to prevent double clicks.
                            string currentUrl = Request.Url.GetLeftPart(UriPartial.Authority) + String.Format("/LeaseApplications/Edit.aspx?id={0}",row.id.ToString());
                            
                            ClientScript.RegisterStartupScript(GetType(), "Load",
                            String.Format("<script type='text/javascript'>" +
                                          "   $(\"#MainContent_btnSubmitCreateApps\").val(\"Processing...\");" +
                                          "   $(\"#MainContent_btnSubmitCreateApps\").attr(\"disabled\", true);" +
                                          "   showMessageTransfer('" + currentUrl + "');" +
                                          "</script>", row.id));
                        }
                        else
                        {
                            Master.StatusBox.showErrorMessage(status);
                        }
                    }
                    else
                    {
                        StringBuilder sErrorMsg = new StringBuilder(Globals.ERROR_HEADER);
                        if (!blnUIDIsValid)
                        {
                            //Invalid UID
                            sErrorMsg.AppendLine(Globals.ERROR_TXT_INVALID_UID + "<br//>");
                        }
                        foreach (string item in m_htVarCharInvalidField.Keys)
                        {
                            if (m_htVarCharInvalidField[item] != null)
                            {
                                sErrorMsg.AppendLine(String.Format(Globals.ERROR_INVALID_LENGTH_FORMAT, item, m_htVarCharInvalidField[item]));
                            }
                        }
                        Master.StatusBox.showStatusMessage(sErrorMsg.ToString());
                        //Data not valid.
                    }
                }
                else
                {
                    //Edit
                    if (blnIsValid)
                    {
                        //Update current object to entity. 
                        status = LeaseApplication.Update(row);

                        // A null status indicates success.
                        if (string.IsNullOrEmpty(status))
                        {
                           // Upsert import status in [t_TrackitImports]
                           var currentUser = BLL.User.Search(userId);
                           var import = new TrackitImportApplication.TrackiTImportServiceClient();
                           import.UpsertImportApplication(HMACHelper.GenerateHMAC(), null,
                                                          Utility.PrepareString(txt_TMOAppId.Text.ToUpper()),
                                                          TrackitImportApplication.eTrackiTImportStatus.imported,
                                                          currentUser.email);
                           
                           // Redirect to edit page with created id. Keep save button disabled
                           // to prevent double clicks.
                           ClientScript.RegisterStartupScript(GetType(), "Load",
                           String.Format("<script type='text/javascript'>" +
                                       "   $(\"#MainContent_btnSubmitCreateApps\").val(\"Processing\");" +
                                       "   $(\"#MainContent_btnSubmitCreateApps\").attr(\"disabled\", true);" +
                                       "</script>", row.id));
                        }
                        else
                        {
                            Master.StatusBox.showErrorMessage(status);
                        }
                    }
                    else
                    {
                        StringBuilder sErrorMsg = new StringBuilder(Globals.ERROR_HEADER);
                        if (!blnUIDIsValid)
                        {
                            //Invalid UID
                            sErrorMsg.AppendLine(Globals.ERROR_TXT_INVALID_UID + "<br//>");
                        }
                        foreach (string item in m_htVarCharInvalidField.Keys)
                        {
                            if (m_htVarCharInvalidField[item] != null)
                            {
                                sErrorMsg.AppendLine(String.Format(Globals.ERROR_INVALID_LENGTH_FORMAT, item, m_htVarCharInvalidField[item]));
                            }
                        }
                        Master.StatusBox.showStatusMessage(sErrorMsg.ToString());
                    }
                }
            }
        }

        private void setSelectCustomer(List<customer> customers, string sCustomer)
        {
            if (!String.IsNullOrEmpty(sCustomer))
            {
                IQueryable<customer> iqCustomer = customers.AsQueryable().Where(x => x.customer_name.ToLower() == sCustomer.ToLower());
                if (iqCustomer.ToList<customer>().Count() > 0)
                {
                    this.ddlCustomer.Items.FindByValue(iqCustomer.ToList<customer>()[0].id.ToString()).Selected = true;
                }
                else
                {
                    iqCustomer = customers.AsQueryable().Where(x => sCustomer.ToLower().Contains(x.customer_name.ToLower()) && x.sort_order < 100);
                    if (iqCustomer.ToList<customer>().Count() > 0)
                    {
                        this.ddlCustomer.Items.FindByValue(iqCustomer.ToList<customer>()[0].id.ToString()).Selected = true;
                    }
                    else
                    {
                        iqCustomer = customers.AsQueryable().Where(x => x.customer_name.ToLower().Contains(sCustomer.ToLower()) );
                        if (iqCustomer.ToList<customer>().Count() > 0)
                        {
                            this.ddlCustomer.Items.FindByValue(iqCustomer.ToList<customer>()[0].id.ToString()).Selected = true;
                        }
                    }
                }
            }
        }

        protected void BindLookupLists()
        {
            CPTTEntities ce = new CPTTEntities();

            //Customer List
            var customers = ce.customers.OrderBy(Utility.GetField<customer>("customer_name")).OrderBy(Utility.GetIntField<customer>("sort_order"));
            Utility.BindDDLDataSource(this.ddlCustomer, customers, "id", "customer_name", "- Select -");

            if (Request.QueryString[QUERYSTRING_DBA] != null)
            {

                String DBA = (HttpUtility.UrlDecode(Request.QueryString[QUERYSTRING_DBA]));
                setSelectCustomer(customers.ToList<customer>(), DBA);
            }

            //Application Details
            String AppType = (HttpUtility.UrlDecode(Request.QueryString[QUERYSTRING_APPTYPE]));
            List<leaseapp_types> lstLeasApptyp = new List<leaseapp_types>();
            if (AppType != String.Empty)
            {
                lstLeasApptyp = ce.leaseapp_types.Where(x => x.lease_application_type_group == AppType).ToList();
            }
            else
            {
                lstLeasApptyp = ce.leaseapp_types.ToList();
            }
            ddlAppType.Enabled = true;            

            var sortAlgorithm = new DynamicComparer<leaseapp_types>();
            sortAlgorithm.SortOrder(x => x.lease_application_type);
            lstLeasApptyp.Sort(sortAlgorithm);
            Utility.BindDDLDataSource(this.ddlAppType, lstLeasApptyp, "id", "lease_application_type", null);
            if (AppType.ToUpper() == "MOD")
            {
                this.ddlAppType.Items.FindByText("Modification").Selected = true;
            }

            var sort_lease_app_activity = ce.leaseapp_activities.ToList();
            var dynamic_leaseapp_activities = new DynamicComparer<leaseapp_activities>();
            dynamic_leaseapp_activities.SortOrder(x => x.lease_app_activity);
            sort_lease_app_activity.Sort(dynamic_leaseapp_activities);
            Utility.BindDDLDataSource(this.ddlActivityType, sort_lease_app_activity, "id", "lease_app_activity", "- Select -");


            var sort_lease_application_status = ce.leaseapp_statuses.ToList();
            var dynamic_lease_application_status = new DynamicComparer<leaseapp_statuses>();
            dynamic_lease_application_status.SortOrder(x => x.lease_application_status);
            sort_lease_application_status.Sort(dynamic_lease_application_status);
            Utility.BindDDLDataSource(this.ddlAppStatus, sort_lease_application_status, "id", "lease_application_status", "- Select -");

        }
    }
}