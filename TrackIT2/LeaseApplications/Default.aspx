﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="Lease Applications | TrackiT2" Language="C#" Async="true" MasterPageFile="~/Templates/TrackIT2.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TrackIT2.LeaseApplications._Default" %>
<%@ MasterType TypeName="TrackIT2.Templates.TrackIT2" %>
<%@ Register TagPrefix="TrackIT2" TagName="TextFieldAutoComplete" src="~/Controls/TextFieldAutoComplete.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server" ClientIDMode="Static">        
   <!-- Styles -->
   <%: Styles.Render("~/Styles/lease_application") %>

   <!-- Scripts -->
   <%: Scripts.Render("~/Scripts/lease_application") %>

    <script type="text/javascript" language="javascript">
        // Global Var
        //change serach filed from dropdown "Lease App Type","Activity Type","Lease App Status" to multi select
        searchFilterFields = "txtSiteID,lsbCustomer,lsbLeaseAppType,lsbLeaseAppStatus,txtAppReceivedFrom,txtAppReceivedTo,txtAppFeeReceivedFrom,txtAppFeeReceivedTo,lsbMarketName,lsbInitialSpecialist,lsbCurrentSpecialist,lsbProjectManager,lsbCarrierProject,lsbActivityType,lsbSiteManagementStatus";

        currentFlexiGridID = '<%= this.fgApplications.ClientID %>';
        currentPage = "/LeaseApplications";

        // ADV Search Var
        var GETdatas = new Array();
        var blnHaveGET = false;
        var blnExec = false;

        // For test thread
        var threadCount = 0;

        // Doc Ready
        $(function () {
            
            var sTitlePrefix = "";
            
            // Get query string.(query string, out arrayGET data)
            blnHaveGET = GrabGETData(window.location.search.substring(1), GETdatas);

            // Setup List Create Modal
            setupListCreateModal("Create New Application");

            // Load Advanced Search
            if (typeof $("#ddlCriteria") != "undefined") {
                ADVSearchInit($("#ddlCriteria"), "/LeaseApplications");
            }
            if (typeof GETdatas['filter'] == "string") {
                
                //Checking prefix for header.
                var queries = GETdatas['filter'].split("|");
                var isModification = true;

                for (var i = 0; i < queries.length; i += 2) {
                    
                    //add for support multi select "lbxLeaseAppType","lbxLeaseAppStatus","lbxActivityType"
                    if (queries[i] == 'lsbLeaseAppType' || queries[i] == 'lsbLeaseAppStatus' || queries[i] == 'lsbActivityType' || queries[i] == 'ddlLeaseAppType') {
                        if (queries.length == 2 && queries[i + 1] == 7) {
                            sTitlePrefix = "Initial ";
                            isModification = false;
                        }
                        else if (queries.length == 22) {
                            var id = parseInt(queries[i + 1]);
                            if (!((id > 7 && id < 14) || (id > 16 && id < 22))) {
                                isModification = false;
                            }
                        }
                        else {
                            isModification = false;
                        }
                    }
                    else if (GETdatas['filter'].indexOf('lsbLeaseAppType') == -1 && queries[i] == 'ddlCustomer') {
                        if (queries.length == 2 && queries[i + 1] == 204) {
                            sTitlePrefix = "TMO Premod ";
                        }
                        isModification = false;
                    }
                    else {
                        isModification = false;
                    }
                }
                if (isModification) {
                    sTitlePrefix = "Modification ";
                }
                //End checking.
                blnExec = LoadAdvanceSearch(GETdatas['filter'], searchFilterFields);
                blnExec = blnExec | CheckGSFilter(GETdatas['filter']);
            }
            else {
                //Added 2012/02/28 for checking global search
                if (typeof GETdatas['gs'] == "string") {
                    var txtGs = GETdatas['gs'];
                    if (txtGs != undefined && txtGs != null) {
                        setCurrentGS(txtGs);
                        blnExec = true;
                    }
                }
            }
            // /Load Advanced Search >

            // Title
            $("#h1").prepend(sTitlePrefix);
            
            // Setup Search Filter
            setupSearchFilter();

            // Setup Saved Filter Modal 
            setupSavedFilterModal(SavedFilterConstants.buttonCreateNew, SavedFilterConstants.listViews.leaseApps.name, SavedFilterConstants.listViews.leaseApps.url);
        });
        //-->

        // Flexigrid
        //
        // FG Before Send Data
        function fgBeforeSendData(data)
        {

            // Show - Loading Spinner
            $('#h1').activity({ segments: 8, width: 2, space: 0, length: 3, speed: 1.5, align: 'right' });

            // disable export button
            $("#MainContent_exportButton").val("Processing...");
            $("#MainContent_exportButton").attr("disabled", true);

            // Global Search
            var gs = $.urlParam(GlobalSearch.searchQueryString);
            //
            addGlobal:
            if (!String(gs).isNullOrEmpty()) {
                for (i = 0; i < arrayOfFilters.length; i++) {
                    if (arrayOfFilters[i].name == GlobalSearch.searchQueryString) break addGlobal;
                }
                addFilterPostParam(GlobalSearch.searchQueryString, gs);
            }
            //>

            // Project Category
            var pc = $.urlParam(ConstGlobal.GENERAL.projectCategoryQueryString);

            //
            addCategory:
            if (!String(pc).isNullOrEmpty()) {
                for (i = 0; i < arrayOfFilters.length; i++) {
                    if (arrayOfFilters[i].name == ConstGlobal.GENERAL.projectCategoryQueryString) break addCategory;
                }
                addFilterPostParam(ConstGlobal.GENERAL.projectCategoryQueryString, pc);
            }
            //>

            // Set Default filter
            removeFilterPostParam(ConstGlobal.DEFAULT.returnPage);
            addFilterPostParam(ConstGlobal.DEFAULT.returnPage, savedFiltersQueryString);

            // Search Filter Data
            sendSearchFilterDataToHandler(data);
        }
        //

        // FG Data Load
        function fgDataLoad(sender, args)
        {

            // Hide - Loading Spinner 
            $('#h1').activity(false);

            // Enable export button
            $("#MainContent_exportButton").val("Export to Excel");
            $('#MainContent_exportButton').removeAttr("disabled");

            // Workflow Button
            GetAjaxCallSiteWorkFlow(sender._handlerUrl);
        }
        //
        // FG Row Click
        function fgRowClick(sender, args)
        {

            return false;
        }
        //
        // FG Row Render
        function fgRowRender(tdCell, data, dataIdx)
        {

            var sHtml = "";
            var cVal = new String(data.cell[dataIdx]);

            // Type?
            switch (Number(dataIdx))
            {

                // App Type
                case 4:
                    if (cVal.isNullOrEmpty()) {
                        tdCell.innerHTML = ConstGlobal.DATA.noDataHtml;
                    } else {
                        var appType = cVal.replace(/\s/g, '');
                        if (ConstAppTypeAliases.hasOwnProperty(appType)) {
                            tdCell.innerHTML = ConstAppTypeAliases[appType].aliases;
                        }
                        else
                        {
                            tdCell.innerHTML = cVal;
                        }
                    }
                    break;
                // Date 
                case 6:
                    if (cVal.isNullOrEmpty())
                    {
                        tdCell.innerHTML = ConstGlobal.DATA.noDataHtml;
                    } else
                    {
                        tdCell.innerHTML = cVal;
                    }
                    break;
                // OA
                case 8:
                    sHtml = getColumnImageIcon(ColumnTypes.UNDERCONSTRUCTION, cVal, data.cell[0]);
                    tdCell.innerHTML = sHtml;
                    break;
                // Tower Mod 
                case 9:
                    tdCell.innerHTML = "<img siteId='" + data.cell[0] + "' alt=\"Loading...\" title=\"Loading...\" src=\"/Styles/images/load.gif\"> " +
                        "<div class='loading-hide' siteId='" + data.cell[0] + "' style='width: 66px; white-space: normal;'></div><div class=\"towerMod\" siteId=\"" + data.cell[0] + "\" style=\"width: 30px; white-space: normal;\"></div>";
                    break;

                // Rogue Equipment  
                case 10:
                    sHtml = getColumnImageIcon(ColumnTypes.ROGUE_EQUIP, cVal, null);
                    tdCell.innerHTML = sHtml;
                    break;

                // Issue Tracker 
                case 11:
                    tdCell.innerHTML = "<img siteId='" + data.cell[0] + "' alt=\"Loading...\" title=\"Loading...\" src=\"/Styles/images/load.gif\"> " +
                                            "<div class='loading-hide' siteId='" + data.cell[0] + "' style='width: 66px; white-space: normal;'></div><div class=\"issue\" siteId=\"" + data.cell[0] + "\" style=\"width: 30px; white-space: normal;\"></div>";
                    break;

                // Relate Workflow 
                case 12:
                    // onRowRender add class
                    tdCell.innerHTML = "<img siteId='" + data.cell[0] + "' alt=\"Loading...\" title=\"Loading...\" src=\"/Styles/images/load.gif\"> <div style='width: 66px; white-space: normal;'><div class='workflow-container loading-hide' siteId='" + data.cell[0] + "' style='position:relative;'><a href='javascript:void(0)' onClick='WorkflowClick(this);' class='fancy-button workflow-button'>menu<div class='arrow-down-icon'></div><div class='cog-icon'></div></a><ul class='workflow-menu ajaxLoad' siteId='" + data.cell[0] + "'></ul></div></div>";
                    break;
                // Standard String 
                default:
                    if (cVal.isNullOrEmpty())
                    {
                        tdCell.innerHTML = ConstGlobal.DATA.noDataHtml;
                    } else
                    {
                        tdCell.innerHTML = cVal;
                    }
                    break;
            }
            //>
        }
        //

        // WorkflowClick
        function WorkflowClick(clickObj) {
            // Workflow Button
            var a = $(clickObj).parent().find(".workflow-menu");
            $('.workflow-menu').not(a).slideUp();
            $(a).slideToggle('fast');
        }

        // FG No Data
        function fgNoData()
        {

            // Hide - Loading Spinner 
            $('#h1').activity(false);

            // Disable export button
            $("#MainContent_exportButton").val("Export to Excel");
        }
        //-->

        // Window Load
        $(window).load(function () {

            // Apply Search Filter?
            if (blnExec) {

                // Wait For Last Field To Load
                var waitForLastFieldToLoadLimit = 0;
                var waitForLastFieldToLoad = setInterval(function () {
                    if (($('#hiddenLastFieldToCheckLoaded').val().length === 0) || (waitForLastFieldToLoadLimit === 50)) {
                        clearInterval(waitForLastFieldToLoad);

                        // Apply filters and reload the DataGrid
                        applySearchFilter(true);
                    }
                    waitForLastFieldToLoadLimit++;
                }, 100);
                //>

            } else {
                // Normal DataGrid Load
                reloadDataGrid();
            }
        });
        //-->
    </script>    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div id="message_alert" class="full-col"></div>

    <!-- Page Content Header -->
    <div class="o-1" style="margin:0;">
	    <h1 id="h1" style="float:left; padding-top:3px; padding-right:30px;">Lease Applications</h1>
	        <div class="page-options-nav">
                <a class="fancy-button clear-filter-button" href="javascript:void(0)">Clear Filter</a> 
                <a class="fancy-button filter-button arrow-down" href="javascript:void(0)">Filter Results<span class="arrow-down-icon"></span></a> 
                <a id="modalBtnExternalSaveFilter" class="fancy-button save-filter-button" href="javascript:void(0)">Save Filter</a>
                &nbsp;&nbsp;&nbsp;&nbsp;
		        <a id="modalBtnExternal" class="fancy-button" href="javascript:void(0)">Create New Application</a>
	        </div>
	    <div class="cb"></div>
    </div>
    <!-- /Page Content Header -->

    <!-- Search Filter Box -->
    <div class="filter-box">
	    <div style="padding:20px">
            <div class="filter-button-close"><a class="fancy-button close-filter" style="padding:2px 6px 2px 6px;" href="javascript:void(0)">x</a></div>
                <asp:Panel ID="panelFilterBox" runat="server">
                    <div class="left-col">
                        <TrackIT2:TextFieldAutoComplete ID="ctrSiteID"
                                LabelFieldClientID="lblSiteID"
                                LabelFieldValue="Site ID"
                                LabelFieldCssClass="label"
                                TextFieldClientID="txtSiteID"
                                TextFieldCssClass="input-autocomplete"
                                TextFieldWidth="188"
                                ScriptKeyName="ScriptAutoCompleteSiteID"
                                DataSourceUrl="SiteIdAutoComplete.axd"
                                ClientIDMode="Static"
                                runat="server" />
		                    <br />

		                <label style="display: inline-block; vertical-align:top">Lease App Type</label>
                        <div id="divLeaseAppType" style="width: 188px;margin-left: 188px; vertical-align:top;margin-top:-32px" >
                            <asp:ListBox ID="lsbLeaseAppType" SelectionMode="Multiple" ToolTip="- Select -" CssClass="multi-local-config" ClientIDMode="Static" runat="server" />
                        </div>
                        <br />

                        <label>Activity Type</label>
                        <div id="divActivityType" style="width: 188px;margin-left: 188px; vertical-align:top;margin-top:-32px" >
                            <asp:ListBox ID="lsbActivityType" SelectionMode="Multiple" ToolTip="- Select -" CssClass="multi-local-config" ClientIDMode="Static" runat="server" />
                        </div>
                        <br />

		                <label>App Received</label>
		                <ul class="field-list">
                          <li>
                             <asp:TextBox ID="txtAppReceivedFrom" ClientIDMode="Static" CssClass="datepicker" runat="server" />
                             <asp:CompareValidator ID="valAppReceivedFrom" runat="server"
                                                   ControlToValidate="txtAppReceivedFrom"
                                                   ClientIDMode="Static" 
                                                   CssClass="inline-error"
                                                   Display="Dynamic"
                                                   Type="Date"
                                                   Operator="DataTypeCheck"
                                                   ErrorMessage="*">
                              </asp:CompareValidator>
                          </li>
		                    <li>&nbsp;to&nbsp;</li>
		                    <li>
                             <asp:TextBox ID="txtAppReceivedTo" ClientIDMode="Static" CssClass="datepicker" runat="server" />
                             <asp:CompareValidator ID="valAppReceivedTo" runat="server"
                                                   ControlToValidate="txtAppReceivedTo" 
                                                   ClientIDMode="Static"
                                                   CssClass="inline-error"
                                                   Display="Dynamic"
                                                   Type="Date"
                                                   Operator="DataTypeCheck"
                                                   ErrorMessage="*">
                             </asp:CompareValidator>
                          </li>
                      </ul>                                            
                      <br />

                        <label>Lease App Status</label>
                        <div id="divLeaseAppStatus" style="width: 188px;margin-left: 188px; vertical-align:top;margin-top:-32px" >
                            <asp:ListBox ID="lsbLeaseAppStatus" SelectionMode="Multiple" ToolTip="- Select -" CssClass="multi-local-config" ClientIDMode="Static" runat="server" />
                        </div>
                        <br />

                        <label>Customer</label>
                        <div id="divCustomer" style="width: 188px;margin-left: 188px; vertical-align:top;margin-top:-32px" >
                            <asp:ListBox ID="lsbCustomer" SelectionMode="Multiple" ToolTip="- Select -" CssClass="multi-local-config" ClientIDMode="Static" runat="server" />
                        </div>
		                <br />
                    </div>
                    <div class="right-col">
                        <label>Carrier Project</label>
                        <div id="divCarrierProject" style="width: 188px;margin-left: 188px; vertical-align:top;margin-top:-32px" >
                            <asp:ListBox ID="lsbCarrierProject" SelectionMode="Multiple" ToolTip="- Select -" CssClass="multi-local-config" ClientIDMode="Static" runat="server" />
                        </div>
		                <br />

                    	<label>App Fee Received</label>
		                <ul class="field-list">
                            <li>
                               <asp:TextBox ID="txtAppFeeReceivedFrom" ClientIDMode="Static" CssClass="datepicker" runat="server" />
                               <asp:CompareValidator ID="valAppFeeReceivedFrom" runat="server"
                                                   ControlToValidate="txtAppFeeReceivedFrom"
                                                   ClientIDMode="Static" 
                                                   CssClass="inline-error"
                                                   Display="Dynamic"
                                                   Type="Date"
                                                   Operator="DataTypeCheck"
                                                   ErrorMessage="*">
                              </asp:CompareValidator>
                            </li>
		                      <li>&nbsp;to&nbsp;</li>
		                      <li>
                               <asp:TextBox ID="txtAppFeeReceivedTo" ClientIDMode="Static" CssClass="datepicker" runat="server" />
                               <asp:CompareValidator ID="valAppFeeReceivedTo" runat="server"
                                                   ControlToValidate="txtAppReceivedTo"
                                                   ClientIDMode="Static" 
                                                   CssClass="inline-error"
                                                   Display="Dynamic"
                                                   Type="Date"
                                                   Operator="DataTypeCheck"
                                                   ErrorMessage="*">
                              </asp:CompareValidator>
                            </li>
                        </ul>

                        <label>Initial Specialist</label>
                        <div id="divInitialSpecialist" style="width: 240px;margin-left: 188px; vertical-align:top;margin-top:-32px" >
                            <asp:ListBox ID="lsbInitialSpecialist" SelectionMode="Multiple" ToolTip="- Select -" CssClass="multi-local-config" ClientIDMode="Static" runat="server" />
                        </div>
		                <br />
                        
                        <label>Current Specialist</label>
                        <div id="divCurrentSpecialist" style="width: 240px;margin-left: 188px; vertical-align:top;margin-top:-32px" >
                            <asp:ListBox ID="lsbCurrentSpecialist" SelectionMode="Multiple" ToolTip="- Select -" CssClass="multi-local-config" ClientIDMode="Static" runat="server" />
                        </div>
		                <br />

                        <label>Project Manager</label>
                        <div id="divProjectManager" style="width: 240px;margin-left: 188px; vertical-align:top;margin-top:-32px" >
                            <asp:ListBox ID="lsbProjectManager" SelectionMode="Multiple" ToolTip="- Select -" CssClass="multi-local-config" ClientIDMode="Static" runat="server" />
                        </div>
		                <br />

                        <label>Market Name</label>
                        <div id="divMarketName" style="width: 188px;margin-left: 188px; vertical-align:top;margin-top:-32px" >
                            <asp:ListBox ID="lsbMarketName" SelectionMode="Multiple" ToolTip="- Select -" CssClass="multi-local-config" ClientIDMode="Static" runat="server" />
                        </div>
		                <br />

                        <label>Site Management Status</label>
                        <div id="divSiteManagementStatus" style="width: 188px;margin-left: 188px; vertical-align:top;margin-top:-32px" >
                            <asp:ListBox ID="lsbSiteManagementStatus" SelectionMode="Multiple" ToolTip="- Select -" CssClass="multi-local-config" ClientIDMode="Static" runat="server" />
                        </div>
		                <br />

                    </div>

                    <div class="cb"></div>
                     <!--Advance search -->
                    <div id="pnlAdvanceCriteria"></div>

                    <div class="cb"></div>
                    <asp:Label ID="lblAdvanceCriteria" CssClass="critesria-title" runat="server" >Add More Filter Criteria >></asp:Label>
                    <asp:DropDownList ID="ddlCriteria" CssClass="criteria-select" runat="server" ClientIDMode="Static"></asp:DropDownList>
                    <button id="blnAddAdvanceCriteria" class="fancy-button" type="button" onclick="OnAddingCriteria(null,null,null);" runat="server" >Add Criteria</button>
                    <div class="cb"></div>
		
                    <div style="padding:20px 0 15px 0; overflow:visible;">
                        <div style="width:auto;float:left;"><a class="fancy-button apply-search-filter" href="javascript:void(0)">Apply Filter</a></div>
                        <div style="width:auto;float:right;"><a class="fancy-button apply-search-filter" href="javascript:void(0)">Apply Filter</a></div>
                    </div>
                </asp:Panel>
	    </div>
    </div>
    <!-- /Search Filter Box -->

    <div class="cb"></div>

    <!-- Filter Tags -->
    <div id="filterTagsContainer" class="filter-tags-container not-displayed"  runat="server"></div>
    <!-- /Filter Tags -->

    <div class="cb"></div>

    <!-- Lease Applications Grid -->
    <div style="overflow:visible; margin-bottom: 20px; min-height:580px; position:relative;">

        <fx:Flexigrid ID="fgApplications"
            AutoLoadData="False"
            Width="990"
            ResultsPerPage="20"
			ShowPager="true"
            Resizable="false"
            ShowToggleButton="false"
            ShowTableToggleButton="false"
            SearchEnabled="false"
            UseCustomTheme="true"
            CssClass="tmobile"
            WrapCellText="true"
            DoNotIncludeJQuery="true"
            OnClientBeforeSendData="fgBeforeSendData"
            OnClientDataLoad="fgDataLoad"
            OnClientRowClick="fgRowClick"
            OnClientNoDataLoad="fgNoData"
			HandlerUrl="~/LeaseAppList.axd"
            runat="server">
		    <Columns>
			    <fx:FlexiColumn Code="site_uid" Text="Site ID" Width="65" />
                <fx:FlexiColumn Code="site.site_name" Text="Site Name" Width="150" OnRowRender="fgRowRender" />
                <fx:FlexiColumn Code="customer.customer_name" Text="Customer" Width="108" OnRowRender="fgRowRender" />
                <fx:FlexiColumn Code="original_carrier" Text="Original Customer" Width="108" OnRowRender ="fgRowRender" />
                <fx:FlexiColumn Code="leaseapp_types.lease_application_type" Text="App Type" Width="53" OnRowRender ="fgRowRender"/>                                
                <fx:FlexiColumn Code="leaseapp_activities.lease_app_activity" Text="Activity" Width="100" OnRowRender="fgRowRender" />
                <fx:FlexiColumn Code="leaseapp_rcvd_date" Text="Received" Width="60" OnRowRender="fgRowRender" />
                <fx:FlexiColumn Code="leaseapp_statuses.lease_application_status" Text="Status" Width="70" />
                <fx:FlexiColumn Code="site.site_on_air_date" Text="OA" Width="30" OnRowRender="fgRowRender" />
                <fx:FlexiColumn Code="site.tower_modification_yn" Text="TM" Width="30" AllowSort="false" OnRowRender="fgRowRender" />
                <fx:FlexiColumn Code="site.rogue_equipment_yn" Text="RE" Width="30" OnRowRender="fgRowRender" />
                <fx:FlexiColumn Code="site.issue_tracker.Count" Text="IT" Width="29" OnRowRender="fgRowRender" />
                <fx:FlexiColumn Code="workflows" Text="Workflows" Width="66" AllowSort="false" OnRowRender="fgRowRender"/>
                <fx:FlexiColumn Code="id" Text="Actions" Width="61" AllowSort="false"
                                Format="<a class='fancy-button' href='/LeaseApplications/Edit.aspx?id={0}'>Edit</a>" />
		    </Columns>
	    </fx:Flexigrid>
        
        <br />
        <div>
            <div id="exportLoadSpinner" style="display: block; width:25px; height:25px;margin-left:80%; float:left;"></div>
            <asp:Button ID="exportButton" CssClass="ui-button ui-widget ui-state-default ui-corner-all excelExportButton" runat="server" Text="Export to Excel" UseSubmitBehavior="false" OnClientClick="FrontExportClick('fgApplications');" OnClick="ExportClick" />
        </div>
        <asp:HiddenField ID="hiddenSearchValue" runat="server" ClientIDMode="Static" Value="null" />
        <asp:HiddenField ID="hiddenColumnValue" runat="server" ClientIDMode="Static" Value="null" />
        <asp:HiddenField ID="hiddenDownloadFlag" runat="server" ClientIDMode="Static" Value="" />
        <asp:HiddenField ID="hiddenProgress" runat="server" ClientIDMode="Static" Value="" />
        <asp:HiddenField ID="hiddenLastFieldToCheckLoaded" runat="server" ClientIDMode="Static" Value="" />
        <asp:HiddenField ID="hiddenFilterParam" runat="server" ClientIDMode="Static" Value="" />
        <div class="cb"></div>

    </div>
    <!-- /Lease Applications Grid -->

    <!-- Modal -->
    <div id="modalExternal" class="modal-container" style="overflow:hidden;"></div>
    <div id="modalExternalSavedFilterManage" class="modal-container" style="overflow:hidden;"></div>
    <!-- /Modal -->

</asp:Content>