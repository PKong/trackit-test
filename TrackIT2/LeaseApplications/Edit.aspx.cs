﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.BLL;
using TrackIT2.DAL;
using TrackIT2.Objects;
using System.Transactions;
using System.Text;
using System.Collections;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Globalization;
using Newtonsoft.Json.Linq;

namespace TrackIT2.LeaseApplications
{
   public partial class Edit : System.Web.UI.Page
   {
      private MajorMileStone m_MileStone;
      private const string SA_DEFAULT_ID = "0";
      private const string ALL_COMMENTS_URL = "/Comments/AllComment.aspx?ct=la&id=";
      private Hashtable m_htVarCharInvalidField;
      private bool leaseApplicationFound = true;

      ProjectCategory category;
      List<metadata> metaDataList = MetaData.GetAllMetadatasList();
      List<metadata> viewFields;
      List<application_field_access> application_field_access = Security.GetApplicationFieldAccess();
      List<string> userRole = new List<string>();

      public lease_applications CurrentLeaseApplication
      {
         get
         {
            return (lease_applications)this.ViewState["CurrentLeaseApplication"];
         }
         set
         {
            this.ViewState["CurrentLeaseApplication"] = value;
         }
      }

      public eTabName CurrentTab
      {
         get
         {
            eTabName ret = (eTabName)this.Session["cur_tab"];
            if (ret == eTabName.OT) CurrentTab = eTabName.PR;
            return ret;
         }
         set
         {
            this.Session["cur_tab"] = value;
            this.Session["cur_tab_index"] = (int)value;
         }
      }

      public enum eTabName
      {
         PR,    //PreliminaryReview
         SA,    //StructuralAnalysis
         DR,    //Document Review
         LE,    //Lease
         CO,    //Construction
         RV,    //Revisions
         OT     //Other
      }

      protected void Page_Load(object sender, EventArgs e)
      {
         this.hiddenErr.Value = String.Empty;
         TrackIT2.Controls.TopNav topNav = (TrackIT2.Controls.TopNav)Master.FindControl("topNav");
         topNav.ImpersonationChanged += new TrackIT2.Controls.TopNav.StatusChangedEventHandler(topNav_ImpersonationChanged);

         modalCreateTowermodExternal.Visible = Utility.ExternalLabelTowerMod();
         modalCreateSDPExternal.Visible = Utility.ExternalLabelSDP();
         btnSADelete.Visible = Utility.ExternalButtonSaDelete();

         if (!Security.UserIsInRole("Administrator") && !Security.UserIsInRole("Application Coordinator"))
         {
            btnAppDelete.Visible = false;
            btnRVDelete.Visible = false;            
         }

         if (
             !User.IsInRole("TrackiT Docs User") &&
             !User.IsInRole("TrackiT Docs Admin") &&
             !User.IsInRole("Administrator")
            )
         {
             dms_grid_section.Style.Add("display", "none");
             SetupFlexiGrid();
         }
         else
         {
             dms_grid_unauthorized.Style.Add("display", "none");
         }

         if (!Page.IsPostBack)
         {
            int leaseAppId;
            bool isValidId = Int32.TryParse(Request.QueryString["Id"], out leaseAppId);
            Utility.ClearSubmitValidator();
            if (isValidId)
            {
               if (LeaseApplication.Search(leaseAppId) != null)
               {
                  // Set default Process Tab to Preliminary Results. It will be
                  // updated to display a different tab if it is hidden during
                  // the binding process.
                  Page.Title = "Edit Lease Application - " + leaseAppId.ToString() + " | TrackiT2";

                  this.CurrentTab = eTabName.PR;
                  pnlNoId.Visible = false;
                  pnlEdit.Visible = false; 
                  pnlSectionContent.Visible = true;
                  BindLookupLists();
                  BindApplication(leaseAppId);
                  RefreshDataBinding();
                  MajorMileStoneBinding();
                  leaseApplicationFound = true;

                  if (Request.QueryString["saId"] != null)
                  {
                      HiddenSaID.Value = Request.QueryString["saId"].ToString();
                  }
                  else if (Request.QueryString["LeaseAdminId"] != null)
                  {
                      HiddenLeaseAdmin.Value = Request.QueryString["LeaseAdminId"].ToString();
                  }
                  else
                  {
                      HiddenSaID.Value = "null";
                      HiddenLeaseAdmin.Value = "null";
                  }

                  GetSessionFilter();
               }
               else
               {
                  // Page render event will write the 404 response code. This part
                  // of the code will display the No Id panel and hide the edit
                  // panel.
                  pnlEdit.Visible = false;
                  pnlSectionContent.Visible = false;
                  pnlNoId.Visible = true;
                  leaseApplicationFound = false;
               }
            }
            else
            {
               // Page render event will write the 404 response code. This part
               // of the code will display the No Id panel and hide the edit
               // panel.
               pnlEdit.Visible = false;
               pnlSectionContent.Visible = false;
               pnlNoId.Visible = true;
               leaseApplicationFound = false;
            }            
         }
         else
         {
             // Initialise Download from Amazon
             if (Request.Params["__EVENTARGUMENT"] == "DownloadDocument")
             {
                 string keyName = hidDocumentDownload.Value;
                 if (!string.IsNullOrEmpty(keyName))
                 {
                     try
                     {
                         AmazonDMS.DownloadObject(Server.UrlDecode(keyName));
                     }
                     catch (Amazon.S3.AmazonS3Exception ex)
                     {
                         this.hiddenErr.Value = ex.Message;
                         switch (this.hiddenCuerrentTab.Value)
                         {
                             case "PR":
                                 this.CurrentTab = eTabName.PR;
                                 hidden_SA_ID.Value = "";
                                 break;
                             case "SA":
                                 this.CurrentTab = eTabName.SA;
                                 break;
                             case "DR":
                                 this.CurrentTab = eTabName.DR;
                                 hidden_SA_ID.Value = "DR";
                                 break;
                             case "LE":
                                 this.CurrentTab = eTabName.LE;
                                 hidden_SA_ID.Value = "LE";
                                 break;
                             case "CO":
                                 this.CurrentTab = eTabName.CO;
                                 hidden_SA_ID.Value = "";
                                 break;
                             case "RV":
                                 this.CurrentTab = eTabName.RV;
                                 hidden_SA_ID.Value = "";
                                 break;
                             case "OT":
                                 this.CurrentTab = eTabName.OT;
                                 hidden_SA_ID.Value = "";
                                 break;
                         }
                         Master.StatusBox.showErrorMessage(ex.Message);
                         loadData(this.CurrentTab);
                     }
                 }
             }
             //>
         }
      }

      protected void SetupFlexiGrid()
      {
          fgDMS.SortColumn = fgDMS.Columns.Find(col => col.Code == "site_uid");
          fgDMS.SortDirection = FlexigridASPNET.GridSortOrder.Asc;
          fgDMS.RowPerPageOptions = new int[] { 10, 20, 30, 40, 50 };
          fgDMS.NoItemMessage = "No Data";


      }

      private void loadData( eTabName tab = eTabName.PR)
      {
          int leaseAppId;
          bool isValidId = Int32.TryParse(Request.QueryString["Id"], out leaseAppId);
          Utility.ClearSubmitValidator();

          if (isValidId)
          {
              if (LeaseApplication.Search(leaseAppId) != null)
              {
                  // Set default Process Tab to Preliminary Results. It will be
                  // updated to display a different tab if it is hidden during
                  // the binding process.
                  Page.Title = "Edit Lease Application - " + leaseAppId.ToString() + " | TrackiT2";
                  this.CurrentTab = tab;
                  pnlNoId.Visible = false;
                  pnlEdit.Visible = false; //true
                  pnlSectionContent.Visible = true;
                  BindLookupLists();
                  BindApplication(leaseAppId);
                  RefreshDataBinding();
                  MajorMileStoneBinding();
                  leaseApplicationFound = true;

                  //for jump to select SA
                  if (Request.QueryString["saId"] != null)
                  {
                      if (!string.IsNullOrEmpty(hiddenErr.Value))
                      {
                          int tmp = 0;
                          if (int.TryParse(hidden_SA_ID.Value, out tmp))
                          {
                              HiddenSaID.Value = hidden_SA_ID.Value;
                          }
                          else
                          {
                              HiddenSaID.Value = "null";
                          }
                      }
                      else
                      {
                          HiddenSaID.Value = Request.QueryString["saId"].ToString();
                      }
                  }
                  else if (Request.QueryString["LeaseAdminId"] != null)
                  {
                      HiddenLeaseAdmin.Value = Request.QueryString["LeaseAdminId"].ToString();
                  }
                  else
                  {
                      HiddenSaID.Value = "null";
                      HiddenLeaseAdmin.Value = "null";
                  }
              }
              else
              {
                  // Page render event will write the 404 response code. This part
                  // of the code will display the No Id panel and hide the edit
                  // panel.
                  pnlEdit.Visible = false;
                  pnlSectionContent.Visible = false;
                  pnlNoId.Visible = true;
                  leaseApplicationFound = false;
              }
          }
          else
          {
              // Page render event will write the 404 response code. This part
              // of the code will display the No Id panel and hide the edit
              // panel.
              pnlEdit.Visible = false;
              pnlSectionContent.Visible = false;
              pnlNoId.Visible = true;
              leaseApplicationFound = false;
          }    
      }

      protected override void Render(HtmlTextWriter writer)
      {
         base.Render(writer);

         if (!leaseApplicationFound)
         {
            Response.StatusCode = 404;
         }         
      }

      void topNav_ImpersonationChanged(string Role)
      {
         // Do a complete reload of the page so that all controls will have
         // the proper enabling/disabling of controls.
         Response.Redirect("~/LeaseApplications/Edit.aspx?id=" + Request.QueryString["Id"], true);
      }


      protected void btnCancel_Click(object sender, EventArgs e)
      {
         Response.Redirect("~/LeaseApplications/", false);
      }

      protected void btnSubmit_Click(object sender, EventArgs e)
      {
         string status = null;
         
         if (Page.IsValid)
         {                                          
            lease_applications application;
            application = (lease_applications)Session["currentApplication"];

            application.line_count = txtLineCount.Text.Trim();

            status = LeaseApplication.Update(application);

            // A null status indicates success
            if (string.IsNullOrEmpty(status))
            {
               BindApplication(application.id);
               Session["currentApplication"] = null;

               Master.StatusBox.showStatusMessage("Lease Application Updated.");
            }
            else
            {
               Master.StatusBox.showErrorMessage(status);
            }
         }
      }

      protected void BindApplication(int leaseAppId)
      {
         CPTTEntities ce = new CPTTEntities();
         lease_applications application;
         //ProjectCategory category;

         application = ce.lease_applications
                           .Where(la => la.id.Equals(leaseAppId))
                           .OrderBy("lock_version")
                           .First();

         // Detach the application from the entity context so it can be used
         // for subsequent updates.
         // detach for save object.
         ce.Detach(application);
         this.CurrentLeaseApplication = application;
         // attach again for using some reference data.
         ce.Attach(application);

         lblID.Text = application.id.ToString();
         category = LeaseApplication.GetProjectCategory(application);
         viewFields = MetaData.GetViewableFieldsByCategory(category);
         userRole = Security.GetUserRoles(User.Identity.Name, (int)Session["userId"]);
          ProjectCategory.Text = LeaseApplication.GetProjectCategoryFriendlyName(category);

         if (!String.IsNullOrEmpty(application.site_uid))
         {
            BindTowerImages(application.site_uid);
            lblSiteId.Text = application.site_uid;
         }
         else
         {
            lblSiteId.Text = "None.";
         }

         if (!String.IsNullOrEmpty(application.line_count))
         {
            txtLineCount.Text = application.line_count;
         }
         Utility.ControlValueSetter(this.lblCustomer, application.customer_id, "None.");

         //Application Information
         Utility.ControlValueSetter(SiteID, application.site_uid);
         if (application.site != null)
         {
            Utility.ControlValueSetter(this.SiteName, application.site.site_name);
            SiteID.NavigateUrl = Globals.SITE_LINK_URL + application.site_uid;
         }
         else
         {
            Utility.ControlValueSetter(this.SiteName, null, "N/A");
         }

         if (application.customer == null)
         {
            this.CustomerName.Text = "(no customer found)";
         }
         else
         {
            Utility.ControlValueSetter(this.CustomerName, application.customer.customer_name, "(no customer found)");
         }

         if (!String.IsNullOrEmpty(application.customer_site_uid))
         {
            // Some customer site UIDs contain a / to have a more descriptive UID
            if (application.customer_site_uid.Contains("/"))
            {
               CustomerUID.Text = application.customer_site_uid.Substring(0, application.customer_site_uid.IndexOf("/", StringComparison.CurrentCulture));
            }
            else if (application.customer_site_uid.Contains("\\"))
            {
               CustomerUID.Text = application.customer_site_uid.Substring(0, application.customer_site_uid.IndexOf("\\", StringComparison.CurrentCulture));
            }
            else
            {
               CustomerUID.Text = application.customer_site_uid;
            }
         }
         else
         {
            CustomerUIDHeader.Visible = false;
            CustomerUID.Visible = false;
         }


         if (application.original_carrier != null && application.original_carrier != "")
         {
            Utility.ControlValueSetter(this.OriginalCarrier, "(" + application.original_carrier + ")", "");
         }

         btnAllComment.HRef = ALL_COMMENTS_URL + application.id;
         // Detach the application from the entity context so it can be used
         // for subsequent updates.
         ce.Detach(application);
         SABinding();
         RevisionsBinding();
         // need to check if sa save not bind
         // Get and set link documnet data
         // check management status != T3/Crown
         GetLinkDocument();         
      }

      protected void MajorMileStoneBinding()
      {
         lease_applications application = this.CurrentLeaseApplication;
         if (application != null)
         {
            //Set text for Major Mile Stone
            m_MileStone = new MajorMileStone(application);
            lblMajorMileStone.Text = m_MileStone.MileStone;
            progressbar_pink.Width = new System.Web.UI.WebControls.Unit(m_MileStone.ProgCurrent, UnitType.Percentage);
         }

      }

      protected void BindTowerImages(string siteId)
      {
         DMSHelper imageHelper = new DMSHelper();
         List<PhotoInfo> siteImages = new List<PhotoInfo>();

         siteImages = imageHelper.GetSitePhotos(siteId, false, string.Empty);
         imgTower.ImageUrl = siteImages[0].photoURL;
         imgTower.AlternateText = siteImages[0].photoID;
      }

      private string SaveLeaseAdmin(CPTTEntities ceRef, lease_applications leaseApp)
      {
            string results = string.Empty;
            //Get associate data from Lease Admin
            List<lease_admin_records> list_assoc_app = ceRef.lease_admin_records.Where(x => x.lease_application_id == leaseApp.id).ToList<lease_admin_records>();
            lease_admin_records assoc_app = null;
            if (list_assoc_app.Count() > 0)
            {
                assoc_app = list_assoc_app[0];
            }

            //Execution Document Dates
            if((assoc_app == null) && (this.txtExecDocs_RecvedatFSCDate.Text.Trim() != String.Empty))
            {
                //Auto Create Lease Admin
                assoc_app = new lease_admin_records();
                assoc_app.lease_application_id = leaseApp.id;
                if (leaseApp.site != null)
                {
                    assoc_app.sites.Add(leaseApp.site);
                }
                else
                {
                    site objSite = ceRef.sites.Where(x => x.site_uid == leaseApp.site_uid).ToList<site>()[0];
                    ceRef.Attach(objSite);
                    assoc_app.sites.Add(objSite);
                }
                results = LeaseAdminRecord.Add(assoc_app, ceRef);

            }
            //binding assciate data
            if (assoc_app != null)
            {
                ceRef.Attach(assoc_app);
                //disable control
                //new Execution Document Date
                assoc_app.lease_admin_execution_document_type_id = Utility.PrepareInt(this.ddlExecDocs_DocType.SelectedValue);
                assoc_app.date_received_at_fsc = Utility.PrepareDate(this.txtExecDocs_RecvedatFSCDate.Text);
                assoc_app.carrier_signature_notarized_date = Utility.PrepareDate(this.txtExecDocs_CarrierSignature.Text);
                assoc_app.carrier_signature_notarized_yn = Utility.ConvertYNToBoolean(Utility.PrepareString(ddlExecDocs_CarrierSignature.SelectedValue));
                assoc_app.sent_to_signatory_date = Utility.PrepareDate(this.txtExecDocs_SendSignator.Text);
                assoc_app.back_from_signatory_date = Utility.PrepareDate(this.txtExecDocs_BackSignator.Text);
                assoc_app.original_sent_to_ups_date = Utility.PrepareDate(this.txtExecDocs_OrgSentUPS.Text);
                assoc_app.doc_loaded_to_cst_yn = Utility.ConvertYNToBoolean(Utility.PrepareString(ddlExecDocs_DocLoadCST.SelectedValue));

                //new Execution Financial
                assoc_app.revshare_loaded_to_cst_receivable_yn = Utility.ConvertYNToBoolean(Utility.PrepareString(ddlFinanace_LoadCSTRecv.SelectedValue));
                assoc_app.revshare_loaded_to_cst_payable_yn = Utility.ConvertYNToBoolean(Utility.PrepareString(ddlFinanace_LoadCSTPay.SelectedValue));
                assoc_app.amendment_rent_affecting_yn = Utility.ConvertYNToBoolean(Utility.PrepareString(ddlFinance_AmendRentAffect.SelectedValue));
                assoc_app.one_time_fee_yn = Utility.ConvertYNToBoolean(Utility.PrepareString(ddlFinance_OnTimeFee.SelectedValue));

                //new Commencement Document Date
                assoc_app.commencement_ntp_to_customer_date = Utility.PrepareDate(this.txtComDocs_NTPtoCust.Text);
                assoc_app.lease_admin_commencement_document_type_id = Utility.PrepareInt(this.ddlComDocs_DocType.SelectedValue);
                assoc_app.commencement_date = Utility.PrepareDate(this.txtComDocs_CommencementDate.Text);
                assoc_app.date_received_at_fsc_date = Utility.PrepareDate(this.txtComDocs_RecvFSCDate.Text);
                assoc_app.document_received_by_emp_id = Utility.PrepareInt(this.ddlComDocs_RecvBy.SelectedValue);
                assoc_app.date_received_at_fsc_date = Utility.PrepareDate(this.txtComDocs_RecvFSCDate.Text);
                assoc_app.reg_commencement_letter_created_date = Utility.PrepareDate(this.txtComDocs_RegCommencementLetterDate.Text);
                assoc_app.rem_alert_comments = Utility.PrepareString(this.txtComDocs_RemAlert.Text);

            }
            if (assoc_app != null)
            {
                results = LeaseAdminRecord.Update(assoc_app, ceRef);
            }
            return results;
        }

      #region Submit Button Click Events

      protected void SaveLeaseApplicationInformation()
      {
         string status = null;
         bool isSuccess = false;
         lease_applications app = this.CurrentLeaseApplication;

         if (app != null)
         {
            // Preliminary Review Details
            app.prelim_approval_date = Utility.PrepareDate(this.txtPreliminaryDecision_ColLeft1.Text);
            app.prelim_decision_id = Utility.PrepareInt(this.ddlPreliminaryDecision_ColLeft1.SelectedValue);
            app.sdp_to_customer_date = Utility.PrepareDate(this.txtSDPtoCustomer_ColLeft1.Text);
            app.prelim_sitewalk_date = Utility.PrepareDate(this.txtPreliminarySitewalk_ColLeft1.Text);
            app.prelim_sitewalk_date_status_id = Utility.PrepareInt(this.ddlPreliminarySitewalk_ColLeft1.SelectedValue);
            app.landlord_consent_type_id = Utility.PrepareInt(this.ddlLandlordConsentType_ColLeft1.SelectedValue);
            app.landlord_consent_type_notes = Utility.PrepareString(this.txtLandlordConsentTypeNote.Text);

            // Document Review Details
            app.cohost_tenant_id = Utility.PrepareInt(this.ddlLeaseExhibit_CoHostTenant.SelectedValue);
            app.leaseexhbt_decision_date = Utility.PrepareDate(this.txtLeaseExhibit_Decision.Text);
            app.leaseexhbt_rcvd_date = Utility.PrepareDate(this.txtLeaseExhibit_Received.Text);
            app.leaseexhbt_ordered_date = Utility.PrepareDate(this.txtLeaseExhibit_Ordered.Text);
            app.leaseexhbt_decision_id = Utility.PrepareInt(this.ddlLeaseExhibit_Decision.SelectedValue);
            app.leaseexhbt_ordered_date_status_id = Utility.PrepareInt(this.ddlLeaseExhibit_Ordered.SelectedValue);
            app.landlord_notice_sent_date = Utility.PrepareDate(this.txtLandlordConsent_LandlordNotice.Text);
            app.landlord_consent_sent_date = Utility.PrepareDate(this.txtLandlordConsent_ConsentRequested.Text);
            app.landlord_consent_rcvd_date = Utility.PrepareDate(this.txtLandlordConsent_ReceivedDate.Text);
            app.cd_rcvd_date = Utility.PrepareDate(this.txtConstructionDrawings_Received.Text);
            app.cd_decision_date = Utility.PrepareDate(this.txtConstructionDrawings_Decision.Text);
            app.cd_decision_id = Utility.PrepareInt(this.ddlConstructionDrawings_Decision.SelectedValue);

            // Construction Details
            app.preconstr_sitewalk_date = Utility.PrepareDate(this.txtPreconstruction_PreconstructionSitewalk.Text);
            app.preconstr_sitewalk_date_status_id = Utility.PrepareInt(this.ddlPreconstruction_PreconstructionSitewalk.SelectedValue);
            app.ntp_issued_date = Utility.PrepareDate(this.txtPreconstruction_NTPIssuedDateStatus.Text);
            app.ntp_issued_date_status_id = Utility.PrepareInt(this.ddlPreconstruction_NTPIssuedDateStatus.SelectedValue);
            app.ntp_verified_date = Utility.PrepareDate(this.txtPreconstruction_NTPVerified.Text);
            app.early_installation_discovered_date = Utility.PrepareDate(this.txtEarlyInstallation_ViolaitonDiscovered.Text);
            app.early_installation_violation_letter_sent_date = Utility.PrepareDate(this.txtEarlyInstallation_ViolationLetterSent.Text);
            app.early_installation_violation_invoice_created_by_finops_date = Utility.PrepareDate(this.txtEarlyInstallation_ViolationInvoiceCreatedByFinops.Text);
            app.early_installation_violation_invoice_number = Utility.PrepareInt(this.txtEarlyInstallation_ViolationInvoiceNumber.Text);
            app.early_installation_violation_fee_rcvd_date = Utility.PrepareDate(this.txtEarlyInstallation_ViolationFeeReceived.Text);
            app.extension_notice_rcvd_date = Utility.PrepareDate(this.txtExtensionNotice_Received.Text);
            app.extension_notice_fee_rcvd_date = Utility.PrepareDate(this.txtExtensionNotice_FeeReceived.Text);
            app.extension_notice_invoice_created_by_finops_date = Utility.PrepareDate(this.txtExtensionNotice_InvoiceCreatedByFinops.Text);
            app.extension_notice_fee_invoice_number = Utility.PrepareInt(this.txtExtensionNotice_FeeInvoiceNumber.Text);
            app.equipment_removed_date = Utility.PrepareDate(this.txtEquipment_Removed.Text);
            app.failure_to_remove_violation = Utility.ConvertYNToBoolean(this.ddlEquipment_FailureToRemoveViolation.SelectedValue);
            app.failure_to_remove_violation_letter_sent_date = Utility.PrepareDate(this.txtEquipment_FailureToRemoveViolationLetterSent.Text);
            app.failure_to_remove_violation_letter_submitted_to_cst_date = Utility.PrepareDate(this.txtEquipment_FailureToRemoveViolationLetterSubmittedToCst.Text);
            app.violation_equipment_removal_confirmed_submitted_to_cst_date = Utility.PrepareDate(this.txtEquipment_ViolationEquipmentRemovalConfirmedSubmittedToCst.Text);
            app.postconstr_sitewalk_date = Utility.PrepareDate(this.txtPostConstruction_SiteWalk.Text);
            app.postconstr_ordered_date = Utility.PrepareDate(this.txtPostConstruction_Ordered.Text);
            app.constr_cmplt_date = Utility.PrepareDate(this.txtPostConstruction_ConstructionCompleted.Text);
            app.postconstr_sitewalk_date_status_id = Utility.PrepareInt(this.ddlPostConstruction_SiteWalk.SelectedValue);
            app.postconstr_ordered_date_status_id = Utility.PrepareInt(this.ddlPostConstruction_Ordered.SelectedValue);
            app.constr_cmplt_date_status_id = Utility.PrepareInt(this.ddlPostConstruction_ConstructionCompleted.SelectedValue);
            app.all_closeout_docs_rcvd_date = Utility.PrepareDate(this.txtCloseOut_Received.Text);
            app.closeout_docs_notice_sent_date = Utility.PrepareDate(this.txtCloseOut_NoticeSent.Text);
            app.closeout_docs_fee_required = Utility.ConvertYNToBoolean(this.ddlCloseOut_FeeRequired.SelectedValue);
            app.closeout_docs_fee_invoice_sent_date = Utility.PrepareDate(this.txtCloseOut_FeeInvoiceSent.Text);
            app.closeout_doc_fee_invoice_created_by_finops_date = Utility.PrepareDate(this.txtCloseOut_FeeInvoiceCreatedByFinops.Text);
            app.closeout_docs_fee_invoice_number = Utility.PrepareInt(this.txtCloseOut_FeeInvoiceNumber.Text);
            app.closeout_docs_fee_rcvd_date = Utility.PrepareDate(this.txtCloseOut_FeeReceived.Text);
            app.final_site_apprvl_date = Utility.PrepareDate(this.txtCloseOut_FinalSiteApproval.Text);
            app.as_built_date = Utility.PrepareDate(this.txtCloseOut_AsBuilt.Text);
            app.final_site_apprvl_date_status_id = Utility.PrepareInt(this.ddlCloseOut_FinalSiteApproval.SelectedValue);
            app.market_handoff_date = Utility.PrepareDate(this.txtMarketHandoff_Date.Text);
            app.market_handoff_date_status_id = Utility.PrepareInt(this.ddlMarketHandoff_Status.SelectedValue);

         
            // Lease Details
            //Get associate data from Lease Admin
            app.leasedoc_sent_to_cust_date = Utility.PrepareDate(this.txtExecutablesSenttoCustomer.Text);
            app.leasedoc_sent_to_cust_date_status_id = Utility.PrepareInt(this.ddlExecutablesSenttoCustomer.SelectedValue);
            app.executables_to_crown_date = Utility.PrepareDate(this.txtExecutablesToCrown.Text);
            app.executables_to_crown_date_status_id = Utility.PrepareInt(this.ddlExecutablesToCrown.SelectedValue);
            app.loe_holdup_comments = Utility.PrepareString(this.txtLOEHoldupReasonTypes_Comments.Text);
            app.signed_by_cust_date = Utility.PrepareDate(this.txtLOEHoldupReasonTypes_SignedbyCustomer.Text);
            app.lease_back_from_cust_date = Utility.PrepareDate(this.txtLOEHoldupReasonTypes_DocsBackfromCustomer.Text);
            app.lease_back_from_cust_date_status_id = Utility.PrepareInt(this.ddlLOEHoldupReasonTypes_DocsBackfromCustomer.Text);
            app.leasedoc_to_fsc_date = Utility.PrepareDate(this.txtLOEHoldupReasonTypes_DocstoFSC.Text);
            app.lease_execution_date = Utility.PrepareDate(this.txtLOEHoldupReasonTypes_LeaseExecution.Text);
            app.lease_execution_date_status_id = Utility.PrepareInt(this.ddlLOEHoldupReasonTypes_LeaseExecution.SelectedValue);
            app.amendment_execution_date = Utility.PrepareDate(this.txtLOEHoldupReasonTypes_AmendmentExecution.Text);
            app.amendment_execution_date_status_id = Utility.PrepareInt(this.ddlLOEHoldupReasonTypes_AmendmentExecution.Text);
            app.termination_date = Utility.PrepareDate(this.txtLOEHoldupReasonTypes_TerminationDate.Text);
            app.rent_forecast_amount = Utility.PrepareDecimal(this.txtFinancial_RentForecastAmount.Text);
            app.revenue_share_yn = Utility.ConvertYNToBoolean(Utility.PrepareString(this.ddlFinancial_RevenueShare.SelectedValue));
            app.commencement_forcast_date = Utility.PrepareDate(this.txtFinancial_CommencementForecast.Text);
            app.rem_lease_seq = Utility.PrepareInt(this.txtFinancial_REMLeaseSequence.Text);
            app.cost_share_agreement_amount = Utility.PrepareDecimal(this.txtFinancial_CostShareAgreementAmount.Text);
            app.cost_share_agreement_yn = Utility.ConvertYNToBoolean(Utility.PrepareString(this.ddlFinancial_CostShareAgreement.SelectedValue));
            app.cap_cost_amount = Utility.PrepareDecimal(this.txtFinancial_CapCostRecoveryAmount.Text);
            app.cap_cost_recovery_yn = Utility.ConvertYNToBoolean(Utility.PrepareString(this.ddlFinancial_CapCostRecovery.SelectedValue));
            app.bulk_po_availible_yn = Utility.ConvertYNToBoolean(Utility.PrepareString(this.ddlFinancial_BulkPOAvailable.SelectedValue));            

            // Structural Decision Details
            app.struct_decision_id = Utility.PrepareInt(ddlDecisionType.SelectedValue);
            app.struct_decision_date = Utility.PrepareDate(txtSADecisionDate.Text);

            // Saving lease application also involves saving structural analysis
            // and revision data. Methods have their own exception handling to
            // properly rollback the transaction if an error occurs during the save.

            // Start transaction for update.
            using (CPTTEntities ce = new CPTTEntities())
            {
               try
               {
                  using (TransactionScope trans = TransactionUtils.CreateTransactionScope())
                  {
                     // Manually open connection to prevent EF from auto closing
                     // connection and causing issues with future queries.
                     ce.Connection.Open();

                     // Update current object entity.
                     status = SaveLeaseAdmin(ce, app);
                     status = SaveSA(ce);

                     // A null status indicates success.
                     if (String.IsNullOrEmpty(status))
                     {
                        status = SaveRV(ce);
 
                        if (String.IsNullOrEmpty(status))
                        {
                           //Update current object entity. 
                           status = LeaseApplication.UpdateWithinTransaction(app, ce);


                           // A null status indicates success.
                           if (String.IsNullOrEmpty(status))
                           {
                              if (lstLOEHoldup.Items.Count > 0)
                              {
                                 List<loe_holdup_reasons> lstLOE = new List<loe_holdup_reasons>();
                                 loe_holdup_reasons obj;

                                 foreach (ListItem item in lstLOEHoldup.Items)
                                 {
                                    if (item.Selected)
                                    {
                                       obj = new loe_holdup_reasons();
                                       obj.lease_application_id = app.id;
                                       obj.loe_holdup_reason_type_id = int.Parse(item.Value);
                                       lstLOE.Add(obj);
                                    }
                                 }

                                 status = LOEHoldupReasons.AddWithinTransaction(lstLOE, app.id, ce);

                                 // A null value indicates success
                                 if (string.IsNullOrEmpty(status))
                                 {
                                    isSuccess = true;
                                 }
                                 else
                                 {
                                    isSuccess = false;
                                 }
                              }

                              // If we've made it this far, then all nested transactions
                              // have completed successfully, and we can mark this
                              // transaction as complete as well.
                              isSuccess = true;
                              ce.SaveChanges();
                              trans.Complete();     // Transaction complete
                           }
                           else
                           {
                              isSuccess = false;
                           }
                        }
                        else
                        {
                           isSuccess = false;
                        }                        
                     }
                     else
                     {
                        isSuccess = false;
                     }                     
                 }
              }              
              catch (Exception ex)
              {
                 // Log error in ELMAH for any diagnostic needs.
                 Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                 status = "An error occcurred updating the lease application. Please try again.";
                 isSuccess = false;
              }
              finally
              {
                 ce.Connection.Dispose();
              }

              // Finally accept all changes from the transaction, rebind the
              // application details and show a success message.
            if (isSuccess)
            {
               ce.AcceptAllChanges();
               //BindApplication(app.id);
               MajorMileStoneBinding();
               //update doc with new sa
               //Add check null data not save document link
               List<StructuralAnalysisTemplate> lstResult = new List<StructuralAnalysisTemplate>();
               lstResult = StructuralAnalysis.ParseFromJSON(hidden_SA.Value);
               if (lstResult.Count > 0)
               {
                   if (!lstResult[lstResult.Count - 1].CheckIsNull())
                   {
                       hidden_SA_popup.Value = "Lease Application Updated.";
                       hidden_revisions_popup.Value = "Lease Application Updated";
                       Master.StatusBox.showStatusMessage("Lease Application Updated.");
                   }
               }
               else
               {
                   Master.StatusBox.showStatusMessage("Lease Application Updated.");
               }


               //save new SA Document
               SaveSALinkDocument(app.id);
               SaveRVLinkDocument(app.id);
               BindApplication(app.id);
               RefreshDataBinding();
               MajorMileStoneBinding();
               EquipmentBinding();
               this.CurrentTab = eTabName.PR;
               hidden_SA_popup.Value = "Lease Application Updated.";
               hidden_revisions_popup.Value = "Lease Application Updated";
               Master.StatusBox.showStatusMessage("Lease Application Updated.");
            }
            else
            {
               hidden_SA_popup.Value = status;
               hidden_revisions_popup.Value = status;
               Master.StatusBox.showStatusMessage(status);
            }
         }
        }
      }

      /// <summary>
      ///    Saves all Structural Analysis records. The entity context is
      ///    passed in since this method is called within the lease applicaiton
      ///    transaction block.
      /// </summary>
      /// <param name="ceRef">Entity Context Reference.</param>
      /// <returns>String containing status of update.</returns>
      protected string SaveSA(CPTTEntities ceRef)
      {
         var blnIsValid = true;
         m_htVarCharInvalidField = new Hashtable();  //Initialize Invalid checking.
         string results = null;

          //Test          
          var blnIsError = false;

            try
            {
               var lstAnalyses = StructuralAnalysis.ParseFromJSON(hidden_SA.Value);

               foreach (var item in lstAnalyses)
               {
                  // Check for update or add new
                  structural_analyses objSA = null;
                  if (item.ID != SA_DEFAULT_ID && item.ID != "")
                  {
                     // Update
                     var id = Int32.Parse(item.ID);
                     objSA = StructuralAnalysis.SearchWithinTransaction(id, ceRef);

                     // Since structural analysis record is retrieved
                     // before updating, test the updated_at field that was 
                     // saved on the client side against the retrieved record 
                     // for an optimistic concurrency exception. Since the
                     // timestamp on the client side was saved as a string, the
                     // comparison must occur via string since a basic date
                     // comparison will not work.
                     var updateTest = "";

                     if (objSA.updated_at.HasValue)
                     {
                        updateTest = objSA.updated_at.Value.ToString("MM/dd/yyyy HH:mm:ss");
                     }

                     if (item.sa_updated_at != updateTest)
                     {
                        throw new OptimisticConcurrencyException();
                     }                    
                  }
                  else
                  {
                     //Add new
                     objSA = new structural_analyses();
                     objSA.lease_application_id = CurrentLeaseApplication.id;
                  }

                  objSA.sa_vendor_id = Utility.PrepareInt(item.sa_vendor_id);
                  objSA.po_ordered_date = Utility.PrepareDate(item.po_ordered_date);
                  objSA.po_received_date = Utility.PrepareDate(item.po_received_date);
                  objSA.po_amount = Utility.PrepareDecimal(item.po_amount);
                  blnIsValid = blnIsValid & Utility.ValidateVarchar(Utility.PrepareString(item.po_number), objSA, m_htVarCharInvalidField, "po_number", "PO Number");
                  if (blnIsValid) objSA.po_number = Utility.PrepareString(item.po_number);

                  objSA.structural_analysis_type_id = Utility.PrepareInt(item.structural_analysis_type_id);
                  objSA.sa_ordered_date = Utility.PrepareDate(item.sa_ordered_date);
                  objSA.sa_received_date = Utility.PrepareDate(item.sa_received_date);
                  objSA.sa_ordered_date_status_id = Utility.PrepareInt(item.sa_ordered_date_status_id);
                  objSA.sa_received_date_status_id = Utility.PrepareInt(item.sa_received_date_status_id);
                  objSA.sa_tower_prcnt = Utility.PrepareDouble(item.sa_tower_prcnt);

                  objSA.sa_order_onhold_date = Utility.PrepareDate(item.sa_order_onhold_date);
                  objSA.sa_order_cancelled_date = Utility.PrepareDate(item.sa_order_cancelled_date);

                  objSA.sa_fee_received_date = Utility.PrepareDate(item.sa_fee_received_date);
                  objSA.sa_fee_amount = Utility.PrepareDecimal(item.sa_fee_amount);

                  blnIsValid = blnIsValid & Utility.ValidateVarchar(Utility.PrepareString(item.sa_check_nbr), objSA, m_htVarCharInvalidField, "sa_check_nbr", "Payment Check Number");
                  if (blnIsValid) objSA.sa_check_nbr = Utility.PrepareString(item.sa_check_nbr);

                  objSA.structural_analysis_fee_payor_type_id = Utility.PrepareInt(item.structural_analysis_fee_payor_type_id);
                  objSA.sac_name_emp_id = Utility.PrepareInt(item.sac_name_emp_id);
                  objSA.saw_to_customer_date = Utility.PrepareDate(item.saw_to_customer_date);
                  objSA.saw_approved_date = Utility.PrepareDate(item.saw_approved_date);
                  objSA.saw_to_sac_date = Utility.PrepareDate(item.saw_to_sac_date);
                  objSA.saw_request_date = Utility.PrepareDate(item.saw_request_date);
                  objSA.sac_priority_id = Utility.PrepareInt(item.sac_priority_id);
                  objSA.sac_description_id = Utility.PrepareInt(item.sac_description_id);
                  blnIsValid = blnIsValid & Utility.ValidateVarchar(Utility.PrepareString(item.shopping_cart_number), objSA, m_htVarCharInvalidField, "shopping_cart_number", "Shopping Cart Number");
                  if (blnIsValid) objSA.shopping_cart_number = Utility.PrepareString(item.shopping_cart_number);

                  objSA.blanket_po_month = Utility.PrepareInt(item.blanket_po_month);
                  objSA.blanket_po_year = Utility.PrepareInt(item.blanket_po_year);
                  objSA.po_release_status_id = Utility.PrepareInt(item.po_release_status_id);
                  objSA.po_release_date = Utility.PrepareDate(item.po_release_date);
                  objSA.po_notes = Utility.PrepareString(item.po_notes);

                  objSA.sa_decision_date = Utility.PrepareDate(item.sa_decision_date);
                  objSA.sa_decision_type_id = Utility.PrepareInt(item.sa_decision_type_id);

                  if (blnIsValid)
                  {
                     if (item.ID != SA_DEFAULT_ID && item.ID != "")
                     {
                        // Update
                        results = StructuralAnalysis.UpdateWithinTransaction(objSA, ceRef);
                        // A null or empty status indicates success.
                        if (string.IsNullOrEmpty(results))
                        {
                           this.CurrentTab = eTabName.SA;
                           hidden_SA_popup.Value = "Lease Application Updated.";
                        }
                        else
                        {
                           hidden_SA_popup.Value = "Error Occur. Please Try Again.";
                           blnIsError = true;
                        }
                     }
                     else
                     {
                        // Add
                        results = StructuralAnalysis.AddWithinTransaction(objSA, ceRef);

                        // A null or empty status indicates success.
                        if (string.IsNullOrEmpty(results))
                        {
                           this.CurrentTab = eTabName.SA;
                           hidden_SA_popup.Value = "Lease Application Updated.";
                        }
                        else
                        {
                           hidden_SA_popup.Value = "Error Occur. Please Try Again.";
                           blnIsError = true;                 
                        }
                     }

                  }
                  else
                  {
                     var sErrorMsg = new StringBuilder(Globals.ERROR_HEADER);
                        
                     foreach (string varc in m_htVarCharInvalidField.Keys)
                     {
                        if (m_htVarCharInvalidField[varc] != null)
                        {
                           sErrorMsg.AppendLine(String.Format(Globals.ERROR_INVALID_LENGTH_FORMAT, item, m_htVarCharInvalidField[varc]));
                        }
                     }

                     hidden_SA_popup.Value = sErrorMsg.ToString();
                  }
               }
            }
            catch (OptimisticConcurrencyException oex)
            {
               // Log error in ELMAH for any diagnostic needs.
               Elmah.ErrorSignal.FromCurrentContext().Raise(oex);
               results = "A structural analysis record has been modifed since you have " +
                         "last loaded the lease application. Please reload the " +
                         "record and attempt to save it again.";
               hidden_SA_popup.Value = "A structural analysis record has been modifed since you have " +
                                       "last loaded the lease application. Please reload the " +
                                       "record and attempt to save it again.";
            }
            catch (Exception ex)
            {
               // Log error in ELMAH for any diagnostic needs.
               Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
               results = "Error saving structural analysis record(s). Please try again.";
            }

         return results;
      }

      /// <summary>
      ///    Saves all Lease Application revision records. The entity context
      ///    is passed in since this method is called within the lease
      ///    application transaction block.
      /// </summary>
      /// <param name="ceRef">Entity Context Reference.</param>
      /// <returns>String containing status of update.</returns>
      protected string SaveRV(CPTTEntities ceRef)
      {
          // Lease application related field update
          var app = CurrentLeaseApplication as lease_applications;
          string status = null;

          if (app != null)
          {             
            try
            {
               var lstRevisions = LeaseAppRevisions.ParseFromJson(hidden_revisions.Value);

               foreach (var item in lstRevisions)
               {                  
                  //Check for update or add new
                  leaseapp_revisions objRV = null;

                  if (item.ID != SA_DEFAULT_ID && item.ID != "")
                  {
                     // Update                        
                     var id = Int32.Parse(item.ID);
                     objRV = LeaseAppRevisions.SearchWithinTransaction(id, ceRef);

                     // Since lease application revision record is retrieved
                     // before updating, test the updated_at field that was 
                     // saved on the client side against the retrieved record 
                     // for an optimistic concurrency exception. Since the
                     // timestamp on the client side was saved as a string, the
                     // comparison must occur via string since a basic date
                     // comparison will not work.
                     var updateTest = "";

                     if (objRV.updated_at.HasValue)
                     {
                        updateTest = objRV.updated_at.Value.ToString("MM/dd/yyyy HH:mm:ss");
                     }

                     if (item.revision_updated_at != updateTest)
                     {
                        throw new OptimisticConcurrencyException();
                     }
                  }
                  else
                  {
                     // Add new
                     objRV = new leaseapp_revisions { lease_application_id = CurrentLeaseApplication.id };
                  }

                  objRV.revision_fee_amount = Utility.PrepareDecimal(item.revision_fee_amount);
                  objRV.revision_fee_check_po_nbr = Utility.PrepareInt(item.revision_fee_check_po_nbr);
                  objRV.revision_fee_rcvd_date = Utility.PrepareDate(item.revision_fee_rcvd_date);
                  objRV.revision_rcvd_date = Utility.PrepareDate(item.revision_rcvd_date);

                  if (item.ID != SA_DEFAULT_ID && item.ID != "")
                  {
                     // Update
                     status = LeaseAppRevisions.UpdateWithinTransaction(objRV, ceRef);                                                                                            
                  }
                  else
                  {
                     //Add
                     status = LeaseAppRevisions.AddWithinTransaction(objRV, ceRef);
                  }
               }
            }
            catch (OptimisticConcurrencyException oex)
            {
               // Log error in ELMAH for any diagnostic needs.
               Elmah.ErrorSignal.FromCurrentContext().Raise(oex);
               status = "A revision record has been modifed since you have " +
                        "last loaded the lease application. Please reload the " +
                        "record and attempt to save it again.";
               hidden_revisions_popup.Value = "A revision record has been modifed since you have " +
                                              "last loaded the lease application. Please reload the " +
                                              "record and attempt to save it again.";
            }
            catch (Exception ex)
            {
               // Log error in ELMAH for any diagnostic needs.
               Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
               status = "Errors occurred processing Revisions. " +
                        "Please check the error log for details.";
               hidden_revisions_popup.Value = "Errors occurred processing Revisions. " + 
                                              "Please check the error log for details.";
            }
         }
          
         return status;
      }

      protected void btnSubmit_PreliminaryReview_Click(object sender, EventArgs e)
      {
         Page.Validate();
         if (Page.IsValid)
         {
            SaveLeaseApplicationInformation();
            this.CurrentTab = eTabName.PR;            
         }
      }

      protected void btnSubmit_DocumentReview_Click(object sender, EventArgs e)
      {
         if (Page.IsValid)
         {
            SaveLeaseApplicationInformation();
            this.CurrentTab = eTabName.DR;            
         }
      }

      protected void btnSubmit_Construction_Click(object sender, EventArgs e)
      {
         if (Page.IsValid)
         {
            SaveLeaseApplicationInformation();
            this.CurrentTab = eTabName.CO;
         }
      }
      protected void btnSubmit_Lease_Click(object sender, EventArgs e)
      {
         if (Page.IsValid)
         {
            SaveLeaseApplicationInformation();
            this.CurrentTab = eTabName.LE;            
         }
      }

      protected void btnSubmit_StructuralAnalysis_Click(object sender, EventArgs e)
        {
            Page.Validate();
            if (Page.IsValid)
            {
                SaveLeaseApplicationInformation();
                this.CurrentTab = eTabName.SA;

                //display document
                HiddenSaID.Value = "newSA";
            }
        }

      protected void btnSubmitRevision_Click(object sender, EventArgs e)
      {
         if (Page.IsValid)
         {
             SaveLeaseApplicationInformation();
             this.CurrentTab = eTabName.RV;
         }
      }

      protected void btnPost_Click(object sender, EventArgs e)
      {
         if (Page.IsValid)
         {
            lease_applications app = this.CurrentLeaseApplication;
            if (txtComments.Text != "" && app != null)
            {
               leaseapp_comments objComment = new leaseapp_comments();
               objComment.comments = Server.HtmlEncode(txtComments.Text);
               objComment.lease_application_id = app.id;
               LeaseAppComment.Add(objComment);
               BindApplication(app.id);
               this.CurrentTab = eTabName.OT;
               AllCommentBinding();
            }
         }
      }

      protected void link_addnew_Click(object sender, EventArgs e)
      {
         if (Page.IsValid)
         {
            //Not allow to update on new SA page.
            if (hidden_SA_ID.Value != SA_DEFAULT_ID)
            {
               //Update
               int id;
               if (int.TryParse(hidden_SA_ID.Value, out id))
               {
                  using (CPTTEntities ce = new CPTTEntities())
                  {
                     structural_analysis_comments comment = new structural_analysis_comments();
                     comment.structural_analysis_id = id;
                     comment.comments = Utility.PrepareString(Server.HtmlEncode(txt_comment_SA.Text));
                     ce.structural_analysis_comments.AddObject(comment);
                     ce.SaveChanges();

                     BindApplication(CurrentLeaseApplication.id);
                     this.CurrentTab = eTabName.SA;
                     Master.StatusBox.showStatusMessage("Structural Analysis Comment Added.");
                  }
               }
            }
            else
            {
               Master.StatusBox.showErrorMessage("Comment on unregist SA page is not allow.");
            }
         }
      }

      protected void btnDeleteComment_Click(object sender, EventArgs e)
      {
         if (Page.IsValid)
         {
            ImageButton objRef = sender as ImageButton;
            string[] sTmp;
            string source;
            int id;
            hidden_deleted_comment_id.Value = "";
            if (objRef != null && objRef.AlternateText != "")
            {
               sTmp = objRef.AlternateText.Split('/');
               if (sTmp.Length == 2)
               {
                  source = sTmp[0];
                  id = int.Parse(sTmp[1]);
                  if (source == AllComments.LEASE_APPLICATION)
                  {
                     LeaseAppComment.Delete(id);
                  }
                  else if (source == AllComments.LEASE_ADMIN)
                  {
                      LeaseAdminComments.Delete(id);
                      // remove another leaseAdmin comment dialog
                      RemoveAnotherCommentDialog(id, AllComments.LEASE_ADMIN);
                  }
                  else if (source == AllComments.SDP)
                  {
                      SdpComment.Delete(id);
                      // remove another sdp comment dialog
                      RemoveAnotherCommentDialog(id, AllComments.SDP);
                  }
                  else if (source == AllComments.TOWER_MOD)
                  {
                      TowerModComment.Delete(id);
                      // remove another towerMod comment dialog
                      RemoveAnotherCommentDialog(id, AllComments.TOWER_MOD);
                  }
                  else
                  {
                     LeaseAppSAComment.Delete(id);
                     // remove another sa comment dialog
                     RemoveAnotherCommentDialog(id, AllComments.STRUCTURAL_ANALYSIS);
                  }
               }
               BindApplication(CurrentLeaseApplication.id);
               if (hidden_lease_admin_id.Value != String.Empty)
               {
                   using (CPTTEntities ce = new CPTTEntities())
                   {
                       BindLeaseComment(Convert.ToInt32(hidden_lease_admin_id.Value), ce);
                   }
               }
               AllCommentBinding();
               this.CurrentTab = eTabName.OT;
               Master.StatusBox.showStatusMessage("Comment Deleted.");
            }
         }
      }

      protected void btnRefreshComment_Click(object sender, EventArgs e)
      {
         AllCommentBinding();
      }

      protected void btnAllRelateComment_Click(object sender, EventArgs e)
      {
          if (btnAllRelateComment.Text == "Hide related comments")
          {
              btnAllRelateComment.Click += btnAllRelateComment_Click;
              btnAllRelateComment.Text = "Show related comments";
              AllCommentBinding();
          }
          else
          {
              btnAllRelateComment.Click += btnRefreshComment_Click;
              btnAllRelateComment.Text = "Hide related comments";
              AllCommentBinding(true);
          }
      }

      #endregion

      #region Fields Binding
      protected void RefreshDataBinding()
      {
         GeneralDataBinding();
         CustomerContactBinding();
         EquipmentBinding();
         PreliminaryReviewBinding();
         DocumentReviewBinding();
         ConstructionBinding();
         LeaseBinding();
         AllCommentBinding();
      }

      protected void BindLookupLists()
      {
         CPTTEntities context = new CPTTEntities();
         var sort_context_decision_types = context.decision_types.ToList();
         var dynamicSort_context_decision_types = new DynamicComparer<decision_types>();
         dynamicSort_context_decision_types.SortOrder(x => x.decision_type);
         sort_context_decision_types.Sort(dynamicSort_context_decision_types);

         var sort_context_date_status = context.date_statuses.ToList();
         var dynamicSort_context_date_status = new DynamicComparer<date_statuses>();
         dynamicSort_context_date_status.SortOrder(x => x.date_status);
         sort_context_date_status.Sort(dynamicSort_context_date_status);

         var sort_landlord_consent_type = context.landlord_consent_types.ToList();
         var dynamicSort_landlord_consent_type = new DynamicComparer<landlord_consent_types>();
         dynamicSort_landlord_consent_type.SortOrder(x => x.landlord_consent_type);
         sort_landlord_consent_type.Sort(dynamicSort_landlord_consent_type);

         var sort_cohost_tenants = context.cohost_tenants.ToList();
         var tmp_cohost_tenants = new DynamicComparer<cohost_tenants>();
         tmp_cohost_tenants.SortOrder(x => x.cohost_tenant);
         sort_cohost_tenants.Sort(tmp_cohost_tenants);

         var loe_holdup_reason = context.loe_holdup_reason_types.ToList();
         var tmp_loe_holdup_reason = new DynamicComparer<loe_holdup_reason_types>();
         tmp_loe_holdup_reason.SortOrder(x => x.loe_holdup_reason);
         loe_holdup_reason.Sort(tmp_loe_holdup_reason);

         //Preliminary Review
         Utility.BindDDLDataSource(this.ddlPreliminaryDecision_ColLeft1,
             sort_context_decision_types, "id", "decision_type", "- Select -");
         Utility.BindDDLDataSource(this.ddlPreliminarySitewalk_ColLeft1,
             sort_context_date_status, "id", "date_status", "- Select -");
         Utility.BindDDLDataSource(this.ddlLandlordConsentType_ColLeft1, sort_landlord_consent_type,
             "id", "landlord_consent_type", "- Select -");

         //Document Review
         Utility.BindDDLDataSource(this.ddlLeaseExhibit_CoHostTenant, sort_cohost_tenants, "id", "cohost_tenant", "- Select -");

         Utility.BindDDLDataSource(this.ddlLeaseExhibit_Decision, sort_context_decision_types, "id", "decision_type", "- Select -");
         Utility.BindDDLDataSource(this.ddlLeaseExhibit_Ordered, sort_context_date_status, "id", "date_status", "- Select -");
         Utility.BindDDLDataSource(this.ddlConstructionDrawings_Decision, sort_context_decision_types, "id", "decision_type", "- Select -");
         
          //Construction
         Utility.BindDDLDataSource(this.ddlPreconstruction_PreconstructionSitewalk,
             sort_context_date_status, "id", "date_status", "- Select -");
         Utility.BindDDLDataSource(this.ddlPreconstruction_NTPIssuedDateStatus,
             sort_context_date_status, "id", "date_status", "- Select -");
         Utility.BindDDLDataSource(this.ddlPostConstruction_Ordered,
             sort_context_date_status, "id", "date_status", "- Select -");
         Utility.BindDDLDataSource(this.ddlPostConstruction_SiteWalk,
             sort_context_date_status, "id", "date_status", "- Select -");
         Utility.BindDDLDataSource(this.ddlPostConstruction_ConstructionCompleted,
             sort_context_date_status, "id", "date_status", "- Select -");
         Utility.BindDDLDataSource(this.ddlCloseOut_FinalSiteApproval,
             sort_context_date_status, "id", "date_status", "- Select -");
         Utility.BindDDLDataSource(this.ddlMarketHandoff_Status,
             sort_context_date_status, "id", "date_status", "- Select -");

         //Lease
         //lstLOEHoldup
         Utility.BindDDLDataSource(this.lstLOEHoldup, loe_holdup_reason, "id", "loe_holdup_reason", null);

         Utility.BindDDLDataSource(this.ddlExecutablesSenttoCustomer,
             sort_context_date_status, "id", "date_status", "- Select -");
         Utility.BindDDLDataSource(this.ddlExecutablesToCrown, 
            sort_context_date_status, "id", "date_status", "- Select -");
         Utility.BindDDLDataSource(this.ddlLOEHoldupReasonTypes_DocsBackfromCustomer,
             sort_context_date_status, "id", "date_status", "- Select -");
         Utility.BindDDLDataSource(this.ddlLOEHoldupReasonTypes_LeaseExecution,
             sort_context_date_status, "id", "date_status", "- Select -");

         Utility.BindDDLDataSource(this.ddlLOEHoldupReasonTypes_AmendmentExecution,
             sort_context_date_status, "id", "date_status", "- Select -");

         //SA
         Utility.BindDDLDataSource(this.ddlProcessDates_ColLeft1, sort_context_date_status, "id", "date_status", "- Select -");

         List<SAVendorsInfo> lstSAVendors = SAVendors.GetAllVendors().ToList();
         var sort_lstSAVendors = lstSAVendors;
         var dynamicSort_lstSAVendors = new DynamicComparer<SAVendorsInfo>();
         dynamicSort_lstSAVendors.SortOrder(x => x.SAVendorName);
         sort_lstSAVendors.Sort(dynamicSort_lstSAVendors);
         Utility.BindDDLDataSource(this.ddlPurchaseOrder_Vendor_ColLeft1, sort_lstSAVendors, "ID", "SAVendorName", "- Select -");
         
          //The vendor dropdownlist binding need to bind again when the data in satab is valid.
         Utility.BindDDLDataSource(this.ddlProcessDates_Type_ColLeft1,
             context.structural_analysis_types.OrderBy(Utility.GetField<structural_analysis_types>("structural_analysis_type")).OrderBy(Utility.GetIntField<structural_analysis_types>("sort_order")),
             "id", "structural_analysis_type", "- Select -");

         var sort_structural_analysis_fee_payor_types = context.structural_analysis_fee_payor_types.ToList();
         var dynamicSort_structural_analysis_fee_payor_types = new DynamicComparer<structural_analysis_fee_payor_types>();
         dynamicSort_structural_analysis_fee_payor_types.SortOrder(x => x.sa_fee_payor_desc);
         sort_structural_analysis_fee_payor_types.Sort(dynamicSort_structural_analysis_fee_payor_types);
         Utility.BindDDLDataSource(this.ddlPayment_PayorType_ColRight1,
             sort_structural_analysis_fee_payor_types, "id", "sa_fee_payor_desc", "- Select -");

         List<UserInfo> lstUser = BLL.User.GetUserInfo().ToList();
         var sort_UserInfo = lstUser;
         var dynamicSort_lstUser = new DynamicComparer<UserInfo>();
         dynamicSort_lstUser.SortOrder(x => x.FullName);
         sort_UserInfo.Sort(dynamicSort_lstUser);
         Utility.BindDDLDataSource(this.ddlRequest_SACName_ColLeft1, sort_UserInfo, "ID", "FullName", "- Select -");

         var sort_sac_priorities = context.sac_priorities.ToList();
         var dynamicSort_sac_priorities = new DynamicComparer<sac_priorities>();
         dynamicSort_sac_priorities.SortOrder(x => x.priority);
         sort_sac_priorities.Sort(dynamicSort_sac_priorities);
         Utility.BindDDLDataSource(this.ddlRequest_SACPriority_ColLeft1,
            sort_sac_priorities, "id", "priority", "- Select -");

         var DynamicSac_priorities = new DynamicComparer<sac_priorities>();
         DynamicSac_priorities.SortOrder(x => x.priority);
         sort_sac_priorities.Sort(DynamicSac_priorities);

         Utility.BindDDLDataSource(this.ddlRequest_SACPriority_ColLeft1, sort_sac_priorities, "id", "priority", "- Select -");

         List<SACDescriptionsInfo> lstDesc = SACDescriptions.GetAllDescriptions().ToList();
         var sort_lstDesc = lstDesc;
         var dynamic_lstDesc = new DynamicComparer<SACDescriptionsInfo>();
         dynamic_lstDesc.SortOrder(x => x.DescriptionName);
         sort_lstDesc.Sort(dynamic_lstDesc);
         Utility.BindDDLDataSource(this.ddlProcessDates_SACDescription_ColLeft1, sort_lstDesc, "ID", "DescriptionName", "- Select -");

         var sort_po_release_statuses = context.po_release_statuses.ToList();
         var dynamicSort_po_release_statuses = new DynamicComparer<po_release_statuses>();
         dynamicSort_po_release_statuses.SortOrder(x => x.po_release_status);
         sort_po_release_statuses.Sort(dynamicSort_po_release_statuses);
         Utility.BindDDLDataSource(this.ddlPurchaseOrder_POReleaseStatus_ColLeft1,
             sort_po_release_statuses, "id", "po_release_status", "- Select -");

         var sort_struct_decision_types = context.struct_decision_types.ToList();
         var dynamicSort_struct_decision_types = new DynamicComparer<struct_decision_types>();
         dynamicSort_struct_decision_types.SortOrder(x => x.struct_decision_type);
         sort_struct_decision_types.Sort(dynamicSort_struct_decision_types);
         Utility.BindDDLDataSource(this.ddlDecisionType, sort_struct_decision_types, "id", "struct_decision_type", "- Select -");
         Utility.BindDDLDataSource(this.ddlProcess_ReceivedStatus_ColLeft1, sort_context_date_status, "id", "date_status", "- Select -");

         Utility.BindDDLDataSource(this.ddl_sa_decision,
            sort_struct_decision_types, "id", "struct_decision_type", "- Select -");

          //New Lease Admin control
          var context_lease_admin_document_types = context.lease_admin_document_types.ToList();
          var tmp_context_lease_admin_document_types = new DynamicComparer<lease_admin_document_types>();
          tmp_context_lease_admin_document_types.SortOrder(x => x.document_type);
          context_lease_admin_document_types.Sort(tmp_context_lease_admin_document_types);
          //Document Type
          Utility.BindDDLDataSource(this.ddlExecDocs_DocType, context_lease_admin_document_types, "id", "document_type", "- Select -");

          Utility.BindDDLDataSource(this.ddlComDocs_DocType, context_lease_admin_document_types, "id", "document_type", "- Select -");
          //Receieve Employee
          Utility.BindDDLDataSource(this.ddlComDocs_RecvBy, sort_UserInfo, "ID", "FullName", "- Select -");
      }

      protected void GeneralDataBinding()
      {
         lease_applications app = this.CurrentLeaseApplication;

         if (app != null)
         {
            // Attach application to entity context so lookups can be performed.
            CPTTEntities context = new CPTTEntities();
            context.Attach(app);

            // Application Details
            if (Security.IsDisplayGroupVisible("Application Details", category, metaDataList,viewFields))
            {
               if (Security.IsFieldViewable(viewFields, "lease_applications", "leaseapp_type_id"))
               {
                  if (app.leaseapp_type_id.HasValue)
                  {
                     Utility.ControlValueSetter(lblAppType, app.leaseapp_types.lease_application_type);
                  }
               }
               else
               {
                  liAppType.Style.Add("display", "none");
               }

               // TMO Application Id
               if (Security.IsFieldViewable(viewFields, "lease_applications", "TMO_AppID"))
               {
                  if (!string.IsNullOrEmpty(app.TMO_AppID))
                  {
                     Utility.ControlValueSetter(this.lblTMOAppId, app.TMO_AppID);
                  }
               }
               else
               {
                  liTMOAppId.Style.Add("display", "none");
               }

               if (Security.IsFieldViewable(viewFields, "lease_applications", "leaseapp_activity_id"))
               {
                  if (app.leaseapp_activity_id.HasValue)
                  {
                     Utility.ControlValueSetter(this.lblActivity, app.leaseapp_activities.lease_app_activity);
                  }
               }
               else
               {
                  liActivity.Style.Add("display", "none");
               }

               // App Status and Date function together
               if (Security.IsFieldViewable(viewFields, "lease_applications", "leaseapp_status_id"))
               {
                  if (app.leaseapp_status_id.HasValue)
                  {
                     Utility.ControlValueSetter(this.lblStatus, app.leaseapp_statuses.lease_application_status);
                  }

                  Utility.ControlValueSetter(this.lblAppStatusDate, app.leaseapp_status_date);
               }
               else
               {
                  liStatus.Style.Add("display", "none");
                  liAppStatusDate.Style.Add("display", "none");
               }
               //Customer Site UID 
               if (Security.IsFieldViewable(viewFields, "lease_applications", "customer_site_uid"))
               {
                  Utility.ControlValueSetter(this.lblGeneralCustomerSiteUID, app.customer_site_uid);
               }
               else
               {
                  liCustomerSiteUID.Style.Add("display", "none");
               }


               // Customer Site Name
               if (Security.IsFieldViewable(viewFields, "lease_applications", "customer_site_name"))
               {
                  Utility.ControlValueSetter(this.lblCustomerSiteName, app.customer_site_name);
               }
               else
               {
                  liCustomerSiteName.Style.Add("display", "none");
               }

               //Carrier name
               if (Security.IsFieldViewable(viewFields, "lease_applications", "leaseapp_carrier_project_id"))
               {
                  if (app.leaseapp_carrier_project_id.HasValue)
                  {
                     Utility.ControlValueSetter(this.lblCarrierProject, app.leaseapp_carrier_projects.carrier_project_name);
                  }
                  else
                  {
                     Utility.ControlValueSetter(this.lblCarrierProject, "No");
                  }
               }
               else
               {
                  liCustomerCarrierProject.Style.Add("display", "none");
               }
                
               //Priority
               if (Security.IsFieldViewable(viewFields, "lease_applications", "leaseapp_priority"))
               {
                   if (app.leaseapp_priority.HasValue)
                   {
                       Utility.ControlValueSetter(this.lblPriority, app.leaseapp_priority.Value);
                   }
                   else
                   {
                       Utility.ControlValueSetter(this.lblPriority, "No");
                   }
               }
               else
               {
                   liPriority.Style.Add("display", "none");
               }

               // Received date
               if (Security.IsFieldViewable(viewFields, "lease_applications", "leaseapp_rcvd_date"))
               {
                  Utility.ControlValueSetter(lblAppReceived, app.leaseapp_rcvd_date, "Not Received");
               }
               else
               {
                  liCustomerSiteName.Style.Add("display", "none");
               }

               // Received date
               if (Security.IsFieldViewable(viewFields, "lease_applications", "risk_level_id"))
               {
                  if (app.risk_level_id.HasValue)
                  {
                     Utility.ControlValueSetter(lblRiskLevel, app.risk_levels.risk_level, "None.");

                     switch (app.risk_levels.risk_level)
                     {
                        case "Red":
                           lblRiskLevel.CssClass = "risklevel-red";
                           break;

                        case "Yellow":
                           lblRiskLevel.CssClass = "risklevel-yellow";
                           break;

                        case "Green":
                           lblRiskLevel.CssClass = "risklevel-green";
                           break;
                        default:
                           // do the defalut action
                           break;
                     }                     
                  }
                  else
                  {
                     lblRiskLevel.Text = "None.";
                  }

                  if (!string.IsNullOrEmpty(app.risk_level_notes))
                  {
                     if (app.risk_level_notes.Length < 50)
                     {
                        litRiskNotes.Text = app.risk_level_notes;
                     }
                     else
                     {
                        string tooltip = "[<a class='tooltip' href='#'>more<span>" + Server.HtmlEncode(app.risk_level_notes) + "</span></a>]";                        
                        litRiskNotes.Text = Server.HtmlEncode(app.risk_level_notes.Substring(0, 47)) +
                                            "... " + tooltip;
                     }
                  }
                  else
                  {
                     litRiskNotes.Text = "None.";
                  }
               }
               else
               {
                  liRiskLevel.Style.Add("display", "none");
               }
            }
            else
            {
               ulAplicationDetails.InnerHtml = "<li>Application Details</li><li>(No Application Details)</li>";
            }

            // Team Assigned
            if (Security.IsDisplayGroupVisible("Team Assigned", category, metaDataList,viewFields))
            {
               List<UserInfo> lstUser;
               if (Security.IsFieldViewable(viewFields, "lease_applications", "tmo_specialist_initial_emp_id"))
               {
                  if (!app.tmo_specialist_initial_emp_id.HasValue) lblInitialSpecialist.Text = "N/A";
                  else
                  {
                     lblInitialSpecialist.Text = app.user_lease_applications_tmo_specialist_initial.last_name + ", " +
                                              app.user_lease_applications_tmo_specialist_initial.first_name;
                  }

               }
               else
               {
                  lblInitialSpecialist.Visible = false;
                  lblInitialSpecialistFront.Visible = false;
               }

               if (Security.IsFieldViewable(viewFields, "lease_applications", "tmo_specialist_current_emp_id"))
               {
                  if (!app.tmo_specialist_current_emp_id.HasValue) lblCurrentSpecialist.Text = "N/A";
                  else
                  {
                     lblCurrentSpecialist.Text = app.user_lease_applications_tmo_specialist_current.last_name + ", " +
                                              app.user_lease_applications_tmo_specialist_current.first_name;
                  }

               }
               else
               {
                  lblCurrentSpecialist.Visible = false;
                  lblCurrentSpecialistFront.Visible = false;
               }

               if (Security.IsFieldViewable(viewFields, "lease_applications", "tmo_pm_emp_id"))
               {
                  if (!app.tmo_pm_emp_id.HasValue) lblProjectManager.Text = "N/A";
                  else
                  {
                     lblProjectManager.Text = app.user_lease_applications_tmo_specialist_pm.last_name + ", " +
                                           app.user_lease_applications_tmo_specialist_pm.first_name;
                  }

               }
               else
               {
                  lblProjectManager.Visible = false;
                  lblProjectManagerFront.Visible = false;
               }

               if (Security.IsFieldViewable(viewFields, "lease_applications", "colocation_coordinator_emp_id"))
               {
                  if (!app.colocation_coordinator_emp_id.HasValue) lblColocationCoordinator.Text = "N/A";
                  else
                  {
                     lstUser = BLL.User.GetUserInfo(app.colocation_coordinator_emp_id);
                     if (lstUser.Count > 0)
                     {
                        lblColocationCoordinator.Text = lstUser[0].FullName;
                     }
                  }
               }
               else
               {
                  lblColocationCoordinator.Visible = false;
                  lblColocationCoordinatorFront.Visible = false;
               }

               if (Security.IsFieldViewable(viewFields, "lease_applications", "am_emp_id"))
               {
                   if (!app.am_emp_id.HasValue) lblAccountManager.Text = "N/A";
                   else
                   {
                       lstUser = BLL.User.GetUserInfo(app.am_emp_id);
                       if (lstUser.Count > 0)
                       {
                           lblAccountManager.Text = lstUser[0].FullName;
                       }
                   }
               }
               else
               {
                   lblAccountManager.Visible = false;
                   lblAccountManagerFront.Visible = false;
               }

            }
            else
            {
               ulTeamAssigned.InnerHtml = "<li>Team Assigned</li><li>(No Team Assignments)</li>";
            }

            // Site information
            site appSite = app.site;
            String SiteLatitude = String.Empty;
            String DegreeLatitude = String.Empty;
            String SiteLongitude = String.Empty;
            String DegreeLongtitude = String.Empty;
            if (appSite != null)
            {
               if (Security.IsFieldViewable(viewFields, "sites", "site_latitude") && appSite.site_latitude.HasValue)
               {

                   SiteLatitude = appSite.site_latitude.ToString();
                   DegreeLatitude = Utility.ConvertLatLongCoorToDegree(appSite.site_latitude).ToString();
               }

               if (Security.IsFieldViewable(viewFields, "sites", "site_longitude") && appSite.site_longitude.HasValue)
               {
                   SiteLongitude = appSite.site_longitude.ToString();
                   DegreeLongtitude = Utility.ConvertLatLongCoorToDegree(appSite.site_longitude).ToString();
                  // Add Google Maps link to Latitude, Logitude 
                   lblSiteLatLong.Text = DegreeLatitude + ",&nbsp;" + DegreeLongtitude + "<br />" + SiteLatitude + ",&nbsp;" + SiteLongitude;
                   lblSiteLatLong.NavigateUrl = Utility.GetGooleMapLatLong(SiteLatitude, SiteLongitude).ToString();
               }
               else
               {
                  if (SiteLatitude != String.Empty)
                  {
                     lblSiteLatLong.Text = String.Empty;
                     lblSiteLatLong.Visible = false;
                     lbl_Longitude.Visible = false;
                     lbl_Latitude.Visible = false;
                  }
               }

               // Structure Details
               if (Security.IsFieldViewable(viewFields, "sites", "site_class_desc") && !string.IsNullOrEmpty(appSite.site_class_desc))
               {
                  Utility.ControlValueSetter(lblStructureDetails_Type, appSite.site_class_desc);
               }
               else
               {
                  lblStructureDetails_Type.Visible = false;
               }

               if (Security.IsFieldViewable(viewFields, "sites", "structure_ht") && appSite.structure_ht.HasValue)
               {
                  Utility.ControlValueSetter(lblStructureDetails_Height, appSite.structure_ht, (0).ToString());
               }
               else
               {
                  lblStructureDetails_Height.Visible = false;
               }

               if (Security.IsFieldViewable(viewFields, "sites", "rogue_equipment_yn") && appSite.rogue_equipment_yn.HasValue)
               {
                  Utility.ControlValueSetter(lblStructureDetails_RogueEquip, appSite.rogue_equipment_yn, "None");
               }
               else
               {
                  lblStructureDetails_RogueEquip.Visible = false;
               }

               if (Security.IsFieldViewable(viewFields, "sites", "site_status_desc") && !string.IsNullOrEmpty(appSite.site_status_desc))
               {
                  Utility.ControlValueSetter(lblStructureDetails_Status, appSite.site_status_desc);
                  if (BLL.Site.IsNotOnAir(appSite.site_on_air_date, appSite.site_status_desc))
                  {
                      imgStructureDetails_Construction.ImageUrl = "/images/icons/Construction.png";
                  }
                  else
                  {
                      imgStructureDetails_Construction.Visible = false;
                  }
               }
               else
               {
                  lblStructureDetails_Status.Visible = false;
                  imgStructureDetails_Construction.Visible = false;
               }

               if (Security.IsFieldViewable(viewFields, "sites", "struct_owner") && !string.IsNullOrEmpty(appSite.struct_owner))
               {
                   Utility.ControlValueSetter(lblStructureDetails_StructOwner, appSite.struct_owner);
               }
               else
               {
                   lblStructureDetails_StructOwner.Visible = false;
               }

               if (Security.IsFieldViewable(viewFields, "sites", "management_status_id") && appSite.management_status_id.HasValue)
               {
                  Utility.ControlValueSetter(lblStructureDetails_ManagementStatus, BLL.ManagementStatus.Search(appSite.management_status_id.Value).name);
               }
               else
               {
                  lblStructureDetails_ManagementStatus.Visible = false;
               }

               // Security for site address is assumed that viewing the address can view everything else
               if (Security.IsFieldViewable(viewFields, "sites", "address"))
               {
                  if (appSite.address != null && appSite.city != null && appSite.state != null && appSite.zip != null && appSite.county != null)
                  {
                     // Link 0 = address,1 = city,2 = state,3 = zip,4 = country                                         
                     string sAddressFormat = "{0}<br />{1}, {2}, {3}<br />{4}";
                     mybtn.Text = string.Format(CultureInfo.CurrentCulture, sAddressFormat, appSite.address.ToString(), appSite.city.ToString(),
                                                appSite.state.ToString(), appSite.zip.ToString(), appSite.county.ToString());

                     // Google Maps Link
                     StringBuilder sbGoogleSearch = new StringBuilder();
                     sbGoogleSearch.Append(appSite.address.ToString());
                     if (!String.IsNullOrWhiteSpace(appSite.city)) sbGoogleSearch.Append(", " + appSite.city);
                     if (!String.IsNullOrWhiteSpace(appSite.state.ToString())) sbGoogleSearch.Append(", " + appSite.state.ToString());
                     if (!String.IsNullOrWhiteSpace(appSite.zip)) sbGoogleSearch.Append(", " + appSite.zip);
                     mybtn.NavigateUrl = Utility.GetGoogleMapLink(sbGoogleSearch.ToString());
                  }
                  else
                  {
                     lblAddressNA.Text = "Site Address is not Available.";
                     lblAddressNA.Visible = true;
                     mybtn.Visible = false;
                  }
               }
               else
               {
                  mybtn.Visible = false;
               }

               // Engineering Market Details
               Utility.ControlValueSetter(lblEngineeringDetailsRegion, appSite.region_name);
               Utility.ControlValueSetter(lblEngineeringDetailsMarket, appSite.market_name);
               submarket subMarket = BLL.SubmarketMapping.GetSubmarketBySite(appSite.site_uid);
               string subMarketInfo = "";

               if (subMarket != null)
               {
                  subMarketInfo = subMarket.code + " - " + subMarket.name;
               }
               
               Utility.ControlValueSetter(lblEngineeringDetailsSubmarket, subMarketInfo);
            }
            else
            {
               //Need default text or error raise.
            }

            // Workflows. Visible regardless of project category.
            string appWorkflowContent = RelatedWorkFlow.GetApplicationWorkflowsForLA(app.id);

            if (!string.IsNullOrWhiteSpace(appWorkflowContent))
            {
               ulApplicationWorkflows.InnerHtml = appWorkflowContent;
            }
            else
            {
               // Hiding the application Workflows list will push up the 
               // related workflows list to avoid any gaps in the display.
               liApplicationWorkflowsWrapper.Style.Add("display", "none");
            }

            ulRelatedWorkflows.InnerHtml = RelatedWorkFlow.GetSiteWorkflowsList(app.site_uid);            

            //Payment Details
            if (Security.IsDisplayGroupVisible("Payment Details", category, metaDataList, viewFields))
            {
               if (Security.IsFieldViewable(viewFields, "lease_applications", "leaseapp_fee_rcvd_date"))
               {
                  Utility.ControlValueSetter(lblFeeProcessed, app.leaseapp_fee_rcvd_date, "Not Received");
                  if (!app.leaseapp_fee_rcvd_date.HasValue)
                  {
                     imgFeeProcessed.Visible = false;
                  }
               }
               else
               {
                  lblFeeProcessed.Visible = false;
                  imgFeeProcessed.Visible = false;
               }

               if (Security.IsFieldViewable(viewFields, "lease_applications", "leaseapp_fee_check_po_nbr"))
               {
                  Utility.ControlValueSetter(lblCheckPONumber, app.leaseapp_fee_check_po_nbr);
                  if (app.leaseapp_fee_amount.HasValue)
                  {
                     lblAmount.Text = "$" + app.leaseapp_fee_amount.Value.ToString(".##");
                  }
                  else
                  {
                     lblAmount.Text = "";
                  }
               }
               else
               {
                  lblCheckPONumber.Visible = false;
                  lblAmount.Visible = false;
               }
            }
            else
            {
                ulPaymentDetails.InnerHtml = "<li>Payment Details</li><li>(No Payment Details)</li><br />";
            }

            if (Security.IsDisplayGroupVisible("Structural Decisions", category, metaDataList, viewFields))
            {
               // Structural decision and date work together
               if (Security.IsFieldViewable(viewFields, "lease_applications", "struct_decision_id"))
               {
                  Utility.ControlValueSetter(ddlDecisionType, app.struct_decision_id);
                  Utility.ControlValueSetter(txtSADecisionDate, app.struct_decision_date);

                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "struct_decision_id", userRole, application_field_access))
                  {
                     ddlDecisionType.Enabled = false;
                     txtSADecisionDate.Enabled = false;
                  }
               }
               else
               {
                  lblSADecisionDate.Visible = false;
                  ddlDecisionType.Visible = false;
                  txtSADecisionDate.Visible = false;
               }
            }
            else
            {
               structural_decision.Style.Add("display", "none");
            }

            if (Security.IsFieldViewable(viewFields, "sites", "tower_modification_yn"))
            {
               // There is no editing of the has tower modification label, just visibility.
               if (app.site != null)
               {
                  Utility.ControlValueSetter(lblHasTowerMod, BLL.Site.HasTowerModifications(app.site.site_uid), "N/A");
               }
               else
               {
                  Utility.ControlValueSetter(lblHasTowerMod, null, "N/A");
               }
            }
            else
            {
               lblHasTowermodTitle.Visible = false;
               lblHasTowerMod.Visible = false;
            }
            
            // Detach application from entity context for future updates.
            context.Detach(app);
         }
      }

      protected void EquipmentBinding()
      {
         lease_applications app = this.CurrentLeaseApplication;
         string[] InLblTxt = new string[16];
         int countObj = 0;

         if (app != null)
         {
             #region SecurityCheck
             if (Security.IsFieldViewable(viewFields, "lease_applications", "antenna_count"))
            {
               Utility.ControlValueSetter(this.lbl_antenna_count, app.antenna_count);
               InLblTxt[countObj] = lbl_antenna_count.Text;
            }
            else
            {
               liAntennaCount.Visible = false;
            }
            countObj++;

            if (Security.IsFieldViewable(viewFields, "lease_applications", "antenna_dimensions"))
            {
               Utility.ControlValueSetter(this.lbl_antenna_dimensions, app.antenna_dimensions);
               InLblTxt[countObj] = lbl_antenna_dimensions.Text;
            }
            else
            {
               liAntennaDimensions.Visible = false;
            }
            countObj++;

            if (Security.IsFieldViewable(viewFields, "lease_applications", "antenna_mount_type"))
            {
               Utility.ControlValueSetter(this.lbl_antenna_mount_type, app.antenna_mount_type);
               InLblTxt[countObj] = lbl_antenna_mount_type.Text;
            }
            else
            {
               liAntennaMountType.Visible = false;
            }
            countObj++;

            if (Security.IsFieldViewable(viewFields, "lease_applications", "antenna_rad"))
            {
               Utility.ControlValueSetter(this.lbl_antenna_rad, app.antenna_rad);
               InLblTxt[countObj] = lbl_antenna_rad.Text;
            }
            else
            {
               liAntennaRAD.Visible = false;
            }
            countObj++;

            if (Security.IsFieldViewable(viewFields, "lease_applications", "antenna_weight"))
            {
               Utility.ControlValueSetter(this.lbl_antenna_weight, app.antenna_weight);
               InLblTxt[countObj] = lbl_antenna_weight.Text;
            }
            else
            {
               liAntennaWeight.Visible = false;
            }
            countObj++;

            if (Security.IsFieldViewable(viewFields, "lease_applications", "line_count"))
            {
               Utility.ControlValueSetter(this.lbl_line_count, app.line_count);
               InLblTxt[countObj] = lbl_line_count.Text;
            }
            else
            {
               liLineCount.Visible = false;
            }
            countObj++;

            if (Security.IsFieldViewable(viewFields, "lease_applications", "line_diameter"))
            {
               Utility.ControlValueSetter(this.lbl_line_diameter, app.line_diameter);
               InLblTxt[countObj] = lbl_line_diameter.Text;
            }
            else
            {
               liLineDiameter.Visible = false;
            }
            countObj++;

            if (Security.IsFieldViewable(viewFields, "lease_applications", "line_length"))
            {
               Utility.ControlValueSetter(this.lbl_line_length, app.line_length);
               InLblTxt[countObj] = lbl_line_length.Text;
            }
            else
            {
               liLineLength.Visible = false;
            }
            countObj++;

            if (Security.IsFieldViewable(viewFields, "lease_applications", "lna_rdu_count"))
            {
               Utility.ControlValueSetter(this.lbl_lna_rdu_count, app.lna_rdu_count);
               InLblTxt[countObj] = lbl_lna_rdu_count.Text;
            }
            else
            {
               liLNARDUCount.Visible = false;
            }
            countObj++;

            if (Security.IsFieldViewable(viewFields, "lease_applications", "lna_rdu_dimensions"))
            {
               Utility.ControlValueSetter(this.lbl_lna_rdu_dimensions, app.lna_rdu_dimensions);
               InLblTxt[countObj] = lbl_lna_rdu_dimensions.Text;
            }
            else
            {
               liLNARDUDimensions.Visible = false;
            }
            countObj++;

            if (Security.IsFieldViewable(viewFields, "lease_applications", "lna_rdu_rad"))
            {
               Utility.ControlValueSetter(this.lbl_lna_rdu_rad, app.lna_rdu_rad);
               InLblTxt[countObj] = lbl_lna_rdu_rad.Text;
            }
            else
            {
               liLNARDURAD.Visible = false;
            }
            countObj++;

            if (Security.IsFieldViewable(viewFields, "lease_applications", "lna_rdu_weight"))
            {
               Utility.ControlValueSetter(this.lbl_lna_rdu_weight, app.lna_rdu_weight);
               InLblTxt[countObj] = lbl_lna_rdu_weight.Text;
            }
            else
            {
               liLNARDUWeight.Visible = false;
            }
            countObj++;

            if (Security.IsFieldViewable(viewFields, "lease_applications", "mw_diameter"))
            {
               Utility.ControlValueSetter(this.lbl_mw_diameter, app.mw_diameter);
               InLblTxt[countObj] = lbl_mw_diameter.Text;
            }
            else
            {
               liAntennaMWDiamenter.Visible = false;
            }
            countObj++;

            if (Security.IsFieldViewable(viewFields, "lease_applications", "mw_rad"))
            {
               Utility.ControlValueSetter(this.lbl_mw_rad, app.mw_rad);
               InLblTxt[countObj] = lbl_mw_rad.Text;
            }
            else
            {
               liAntennaMWRAD.Visible = false;
            }
            countObj++;

            if (Security.IsFieldViewable(viewFields, "lease_applications", "other_equipment"))
            {
               Utility.ControlValueSetter(this.lbl_other_equipment, app.other_equipment);
               InLblTxt[countObj] = lbl_other_equipment.Text;
            }
            else
            {
               liOtherEquipment.Visible = false;
            }
            countObj++;

            if (Security.IsFieldViewable(viewFields, "lease_applications", "ground_expansion_required_yn"))
            {
               Utility.ControlValueSetter(this.lbl_ground_expansion_required_yn, app.ground_expansion_required_yn);
               InLblTxt[countObj] = lbl_ground_expansion_required_yn.Text;
            }
            else
            {
               liGroundExpansionRequired.Visible = false;
            }
            #endregion SecurityCheck

            //final check
            bool finalCheckTxt = false;
            foreach (string txt in InLblTxt)
            {
                if (txt != "")
                {
                    finalCheckTxt = true;
                    break;
                }

            }            

            if (!finalCheckTxt)
            {
                ClientScript.RegisterStartupScript(typeof(Page), "HideSummaryOnly", "HideSummaryOnly();", true);
                divNoEqip.Visible = true;
            }

            if (app.TMO_AppID == null || app.TMO_AppID == "")
            {
                //Hide all f5827 equipment tab
                this.liAntennas.Visible = false;
                this.antennas.Visible = false;
                this.liAmplifiers.Visible = false;
                this.amplifiers.Visible = false;
                this.liGps.Visible = false;
                this.gps.Visible = false;
                this.liMicrowave.Visible = false;
                this.microwave.Visible = false;
                this.liOther.Visible = false;
                this.other.Visible = false;
                this.liGroundspace.Visible = false;
                this.groundspace.Visible = false;
                this.liComments.Visible = false;
                this.comments.Visible = false;
                this.customer_contacts_with_TMO_Application.Visible = false;
            }
            else
            {
                //Binding data to f5827 equipment tabs
                LeaseAppEquipment leaseAppEquip = new LeaseAppEquipment();

                ClientScript.RegisterStartupScript(typeof(Page), "DisplayEquipHistory", "DisplayEquipHistory();", true);

                using (EquipmentEntities enEquip = new EquipmentEntities())
                { 
                    t_Application objEquipment = leaseAppEquip.Get_t_Application(app.TMO_AppID.Trim());

                    if( objEquipment != null )
                    {
                        int iDivider = 2;
                        bool firstTabFlag = true;

                        //Check on edit?
                        if (!string.IsNullOrEmpty(objEquipment.ApplicationStatus))
                        {
                            if (objEquipment.ApplicationStatus == "Editing")
                            {
                                divEditEqup.Visible = true;
                            }
                        }

                        divNoEqip.Visible = false;
                        ClientScript.RegisterStartupScript(typeof(Page), "HideSummary", "HideSummary();", true);
                        
                        //Hide customer_contacts_with_no_TMO_Application
                        customer_contacts_with_no_TMO_Application.Visible = false;

                        ApplicationID.Value = objEquipment.ID.ToString();

                        if (objEquipment.ApplicationType != "MOD")
                        {
                            //Hide second column for each tab.
                            divAntMod.Visible = false;
                            divGPSMod.Visible = false;
                            divAmpMod.Visible = false;
                            divMicMod.Visible = false;
                            divOthMod.Visible = false;
                            iDivider = 1;
                            if (!Regex.IsMatch(objEquipment.TMO_AppID, "-M-"))
                            {
                                //Change wording from "Initial" to propose when found that TMO_AppID not -M-
                                divAntInitLabel.Text = "Proposed Antenna Information";
                                divAmpInitLabel.Text = "Proposed Amplifier Information";
                                divGPSInitlabel.Text = "Proposed GPS Information";
                                divMicInitLabel.Text = "Proposed Microwave Information";
                                divOthInitLabel.Text = "Proposed Other Equipment Information";
                                divAntInitFrequencyLabel.Text = "Proposed Frequency and Power Ranges";
                            }
                        }
                        else
                        {
                            //Change wording from "Initial" to "Existing" when found that have exixting and propose
                            divAntInitLabel.Text = "Existing Antenna Information";
                            divAmpInitLabel.Text = "Existing Amplifier Information";
                            divGPSInitlabel.Text = "Existing GPS Information";
                            divMicInitLabel.Text = "Existing Microwave Information";
                            divOthInitLabel.Text = "Existing Other Equipment Information";
                            divAntInitFrequencyLabel.Text = "Existing Frequency and Power Ranges";
                        }

                        #region Antenna
                        //Antenna
                        List<LeaseAppEquipment.AntennaEquipment> queryAnt = leaseAppEquip.GetAntennaEquipment(objEquipment.ID).ToList();
                        if (queryAnt.Count > 0)
                        {
                            firstTabFlag = false;
                            firstTab.Value = "Antenna";
                            
                            var groupQuery = queryAnt.OrderBy(e => e.EquipmentGroup).ThenByDescending(a => a.IsExist);
                            var tmpJson = Newtonsoft.Json.JsonConvert.SerializeObject(groupQuery);
                            //load line type
                            foreach (var antennaEquipmentItem in queryAnt)
                            {
                                List<LeaseAppEquipment.AntennaEquipmentTxLineType> antLine = leaseAppEquip.GetAntennaEquipmentTxLineType(antennaEquipmentItem.ID).ToList();
                                
                                //loop for add Transmission Line data into Antenna data
                                if (antLine.Count > 0)
                                {
                                    for (int antLineTypeItem = 0; antLineTypeItem < antLine.Count; antLineTypeItem++)
                                    {
                                        string[] antLineTypeItemString = leaseAppEquip.GenDataAntennaEquipmentTxLineType(antLine, antLineTypeItem, true);

                                        CreateTableTransmissionLine(antLineTypeItem, antLine, objEquipment.TMO_AppID);
                                        var tmpJasonLineType = Newtonsoft.Json.JsonConvert.SerializeObject(antLineTypeItemString).Replace("[", "").Replace("]", "").Replace("!\\\"", "\"");
                                        string patten = string.Format("\"ID\":" + antLine[antLineTypeItem].ID);
                                        if (Regex.IsMatch(tmpJson, patten))
                                        {//first time
                                            tmpJson = Regex.Replace(tmpJson, patten, tmpJasonLineType);
                                        }
                                        else
                                        {
                                            patten = string.Format("\"ID\":\"{0}\"", antLine[antLineTypeItem].ID);
                                            tmpJson = Regex.Replace(tmpJson, patten, tmpJasonLineType);
                                        }
                                    }
                                }
                                else
                                {
                                    string[] tmpA = leaseAppEquip.GenDataAntennaEquipmentTxLineType(antLine, 0, false);

                                    string patten = string.Format("\"ID\":\"{0}", antLine[0].ID);
                                    CreateTableTransmissionLine(0, antLine, objEquipment.TMO_AppID);
                                    var tmpJasonLineType = Newtonsoft.Json.JsonConvert.SerializeObject(tmpA).Replace("[", "").Replace("]", "").Replace("!\\\"", "\"");
                                    tmpJson = Regex.Replace(tmpJson, patten, tmpJasonLineType);
                                }
                            }

                            hidAntJSON.Value = tmpJson;
                            hidAntCount.Value = (queryAnt.Count / iDivider).ToString();
                        }
                        else
                        {
                            this.liAntennas.Visible = false;
                            this.antennas.Visible = false;
                        }
                        //End Antenna
                        #endregion Antenna

                        #region Amplifier
                        //Amplifier
                        List<LeaseAppEquipment.AmplifierEquipment> queryAmp = leaseAppEquip.GetAmplifierEquipment(objEquipment.ID).ToList();
                        if (queryAmp.Count > 0)
                        {
                            if (firstTabFlag)
                            {
                                firstTabFlag = false;
                                firstTab.Value = "Amplifier";
                            }

                            var groupQuery = queryAmp.OrderBy(e => e.EquipmentGroup).ThenByDescending(a => a.IsExist);
                            hidAmpJSON.Value = Newtonsoft.Json.JsonConvert.SerializeObject(groupQuery);
                            hidAmpCount.Value = (queryAmp.Count / iDivider).ToString();
                        }
                        else
                        {
                            this.liAmplifiers.Visible = false;
                            this.amplifiers.Visible = false;
                        }
                        //End Amplifier
                        #endregion Amplifier

                        #region GPS
                        //GPS
                        List<LeaseAppEquipment.GPSEquipment> queryGPS = leaseAppEquip.GetGPSEquipment(objEquipment.ID, objEquipment.ApplicationType).ToList();
                        if (queryGPS.Count > 0)
                        {
                            if (firstTabFlag)
                            {
                                firstTabFlag = false;
                                firstTab.Value = "GPS";
                            }

                            var groupQuery = queryGPS.OrderBy(e => e.EquipmentGroup).ThenByDescending(a => a.IsExist);
                            hidGPSJSON.Value = Newtonsoft.Json.JsonConvert.SerializeObject(groupQuery);
                            hidGPSCount.Value = (queryGPS.Count / iDivider).ToString();
                        }
                        else
                        {
                            this.liGps.Visible = false;
                            this.gps.Visible = false;
                        }
                        //End GPS
                        #endregion GPS

                        #region Microwave
                        //Microwave
                        var queryMic = leaseAppEquip.GetMicrowaveEquipment(objEquipment.ID).ToList();             
                        if (queryMic.Count > 0)
                        {
                            if (firstTabFlag)
                            {
                                firstTabFlag = false;
                                firstTab.Value = "Microwave";
                            }

                            var groupQuery = queryMic.OrderBy(e => e.EquipmentGroup).ThenByDescending(a => a.IsExist);
                            hidMicJSON.Value = Newtonsoft.Json.JsonConvert.SerializeObject(groupQuery);
                            hidMicCount.Value = (queryMic.Count / iDivider).ToString();
                        }
                        else
                        {
                            this.liMicrowave.Visible = false;
                            this.microwave.Visible = false;
                        }
                        //End microwave
                        #endregion Microwave

                        #region Other
                        //Other Equipment
                        List<LeaseAppEquipment.OtherEquipment> queryOth = leaseAppEquip.GetOtherEquipment(objEquipment.ID).ToList();
                        if (queryOth.Count > 0)
                        {
                            if (firstTabFlag)
                            {
                                firstTabFlag = false;
                                firstTab.Value = "Other";
                            }

                            var groupQuery = queryOth.OrderBy(e => e.EquipmentGroup).ThenByDescending(a => a.IsExist);
                            hidOthJSON.Value = Newtonsoft.Json.JsonConvert.SerializeObject(groupQuery);
                            hidOthCount.Value = (queryOth.Count / iDivider).ToString();
                        }
                        else
                        {
                            this.liOther.Visible = false;
                            this.other.Visible = false;
                        }
                        //End Other Equipment
                        #endregion Other

                        #region Gsp
                        //Ground Space
                        List<LeaseAppEquipment.GspEquipment> queryGsp = leaseAppEquip.GetGspEquipment(objEquipment.ID).ToList();
                        if (queryGsp.ToList().Count > 0)
                        {
                            if (firstTabFlag)
                            {
                                firstTabFlag = false;
                                firstTab.Value = "GSP";
                            }

                            var groupQuery = queryGsp.OrderByDescending(a => a.IsExist);
                            hidGspJSON.Value = Newtonsoft.Json.JsonConvert.SerializeObject(groupQuery);
                            hidGspCount.Value = (queryGsp.Count / iDivider).ToString();
                        }
                        else
                        {
                            this.liGroundspace.Visible = false;
                            this.groundspace.Visible = false;
                        }
                        //End Ground Space
                        #endregion Gsp

                        //Comment
                        if (objEquipment.Comments != null && objEquipment.Comments != "")
                        {
                            if (firstTabFlag)
                            {
                                firstTabFlag = false;
                                firstTab.Value = "Comment";
                            }
                            labEquipmentComments.Text = objEquipment.Comments;
                        }
                        else
                        {
                            this.liComments.Visible = false;
                            this.comments.Visible = false;
                        }
                        //End Comment
                        string script = "SetFirstSelectTab('" + firstTab.Value + "');";
                        ClientScript.RegisterStartupScript(typeof(Page), "SetFirstSelectTab", script, true);
                        
                        #region CustomerContacts

                        List<t_Contact> relateContact = leaseAppEquip.Get_t_ContactEquipment(objEquipment.TMO_AppID.Trim());

                        #region CustomerInfomation

                        //CustomerInfomation
                        if (objEquipment.ContactID_App.HasValue)
                        {
                            TrackIT2.BLL.LeaseAppEquipment.ContactCustomerInfomation CustomerInfomation = leaseAppEquip.GetcustomerInfomationEquipment(objEquipment, relateContact);

                            if (CustomerInfomation != null)
                            {
                                lblCustomerInformationSubmitterTitle.Text = CustomerInfomation.Submitter_Title;
                                lblCustomerInformationLegalEntityName.Text = CustomerInfomation.Legal_Entity_Name;
                                lblCustomerInformationDBA.Text = CustomerInfomation.DBA;
                                lblCustomerInformationCustomerSiteNumber.Text = CustomerInfomation.Customer_Site_Number;
                                if (CustomerInfomation.FCC_Licensed)
                                {
                                    FCC_Licensed_number.Visible = true;
                                    lblCustomerInformationFCCLicensedNumber.Visible = true;
                                    lblCustomerInformationFCCLicensed.Text = "Yes";
                                    lblCustomerInformationFCCLicensedNumber.Text = CustomerInfomation.FCC_Licensed_Number;
                                }
                                else
                                {
                                    lblCustomerInformationFCCLicensed.Text = "No";
                                    FCC_Licensed_number.Visible = false;
                                    lblCustomerInformationFCCLicensedNumber.Visible = false;
                                }


                                if (string.IsNullOrEmpty(CustomerInfomation.Address) && string.IsNullOrEmpty(CustomerInfomation.Address2))
                                {
                                    CustomerInformationApplicantAddress.Visible = false;
                                }
                                else
                                {
                                    lblCustomerInformationApplicantAddress.Text = CustomerInfomation.Address + " " + CustomerInfomation.Address2;
                                }

                                string CityAndStateAndZip = "";
                                if (!string.IsNullOrEmpty(CustomerInfomation.City))
                                {
                                    CityAndStateAndZip = CustomerInfomation.City;
                                }
                                if (!string.IsNullOrEmpty(CustomerInfomation.State))
                                {
                                    if (CityAndStateAndZip != "")
                                    {
                                        CityAndStateAndZip += ", " + CustomerInfomation.State;
                                    }
                                    else
                                    {
                                        CityAndStateAndZip = CustomerInfomation.State;
                                    }
                                }
                                if (!string.IsNullOrEmpty(CustomerInfomation.Zip))
                                {
                                    if (CityAndStateAndZip != "")
                                    {
                                        CityAndStateAndZip += " " + CustomerInfomation.Zip;
                                    }
                                    else
                                    {
                                        CityAndStateAndZip = CustomerInfomation.Zip;
                                    }
                                }
                                if (!string.IsNullOrEmpty(CityAndStateAndZip))
                                {
                                    lblCustomerInformationCityAndStateAndZip.Text = CityAndStateAndZip;
                                }
                                else
                                {
                                    CustomerInformationCityAndStateAndZip.Visible = false;
                                }

                                if (!string.IsNullOrEmpty(CustomerInfomation.County))
                                {
                                    lblCustomerInformationCountry.Text = CustomerInfomation.County;
                                }
                            }
                        }
                        
                        #endregion CustomerInfomation

                        #region BillingContact
                        
                        //Billing Contact
                        if (objEquipment.ContactID_Billing.HasValue)
                        {
                            t_Contact Billing_Contact = leaseAppEquip.GetContactEquipment(objEquipment.ContactID_Billing, relateContact);

                            if (Billing_Contact != null)
                            {
                                lblBillingName.Text = Billing_Contact.FirstName + " " + Billing_Contact.LastName;
                                lblBillingPhone.Text = Billing_Contact.Phone;
                                if (Billing_Contact.PhoneExtension != null)
                                {
                                    lblBillingPhonex.Visible = true;
                                    lblBillingPhoneExtension.Visible = true;
                                    lblBillingPhoneExtension.Text = Billing_Contact.PhoneExtension;
                                }
                                else
                                {
                                    lblBillingPhonex.Visible = false;
                                    lblBillingPhoneExtension.Visible = false;
                                }

                                lblBillingFax.Text = Billing_Contact.Fax;
                                lblBillingMobile.Text = Billing_Contact.Mobile;
                                lblBillingEmail.Text = Billing_Contact.Email;

                                if (string.IsNullOrEmpty(Billing_Contact.Address) && string.IsNullOrEmpty(Billing_Contact.Address2))
                                {
                                    BillingAddress.Visible = false;
                                }
                                else
                                {
                                    lblBillingAddress.Text = Billing_Contact.Address + " " + Billing_Contact.Address2;
                                }

                                string CityAndStateAndZip = "";
                                if (!string.IsNullOrEmpty(Billing_Contact.City))
                                {
                                    CityAndStateAndZip = Billing_Contact.City;
                                }
                                if (!string.IsNullOrEmpty(Billing_Contact.State))
                                {
                                    if (CityAndStateAndZip != "")
                                    {
                                        CityAndStateAndZip += ", " + Billing_Contact.State;
                                    }
                                    else
                                    {
                                        CityAndStateAndZip = Billing_Contact.State;
                                    }
                                }
                                if (!string.IsNullOrEmpty(Billing_Contact.Zip))
                                {
                                    if (CityAndStateAndZip != "")
                                    {
                                        CityAndStateAndZip += " " + Billing_Contact.Zip;
                                    }
                                    else
                                    {
                                        CityAndStateAndZip = Billing_Contact.Zip;
                                    }
                                }
                                if (!string.IsNullOrEmpty(CityAndStateAndZip))
                                {
                                    lblBillingCityAndStateAndZip.Text = CityAndStateAndZip;
                                }
                                else
                                {
                                    BillingCityAndStateAndZip.Visible = false;
                                }

                                if (Billing_Contact.t_Country != null)
                                {
                                    if (!string.IsNullOrEmpty(Billing_Contact.t_Country.FullName))
                                    {
                                        lblBillingCountry.Text = Billing_Contact.t_Country.FullName;
                                    }
                                }
                            }
                        }

                        
                        
                        #endregion BillingContact

                        #region SetCustomerInformationAndBillingContactDiv

                        int divHeight = 300;

                        if (lblBillingAddress.Text == "" && lblCustomerInformationApplicantAddress.Text == "")
                        {
                            divHeight -= 20;
                        }

                        if (lblBillingCityAndStateAndZip.Text == "" && lblCustomerInformationCityAndStateAndZip.Text == "")
                        {
                            divHeight -= 20;
                        }

                        if (lblBillingCountry.Text == "" && lblCustomerInformationCountry.Text == "")
                        {
                            divHeight -= 20;
                        }

                        if (!FCC_Licensed_number.Visible)
                        {
                            divHeight -= 20;
                        }

                        if (divHeight != 300)
                        {
                            divCustomerInformation.Style["height"] = divHeight + "px";
                            divBillingContact.Style["height"] = divHeight + "px";
                        }

                        #endregion SetCustomerInformationAndBillingContactDiv

                        #region CustomerDevelopmentManager
                        //Customer Development Manager
                        if (objEquipment.ContactID_CarrierDevMgr.HasValue)
                        {
                            t_Contact cdm = leaseAppEquip.GetContactEquipment(objEquipment.ContactID_CarrierDevMgr, relateContact);
                            if (cdm != null)
                            {
                                lblCustomerDevelopmentManagerName.Text = cdm.FirstName + " " + cdm.LastName;
                                lblCustomerDevelopmentManagerPhone.Text = cdm.Phone;
                                if (cdm.PhoneExtension != null)
                                {
                                    lblCustomerDevelopmentManagerPhoneX.Visible = true;
                                    lblCustomerDevelopmentManagerPhoneExtension.Visible = true;
                                    lblCustomerDevelopmentManagerPhoneExtension.Text = cdm.PhoneExtension;
                                }
                                else
                                {
                                    lblCustomerDevelopmentManagerPhoneX.Visible = false;
                                    lblCustomerDevelopmentManagerPhoneExtension.Visible = false;
                                }
                                lblCustomerDevelopmentManagerFax.Text = cdm.Fax;
                                lblCustomerDevelopmentManagerMobile.Text = cdm.Mobile;
                                lblCustomerDevelopmentManagerEmail.Text = cdm.Email;
                            }
                        }
                        #endregion CustomerDevelopmentManager

                        #region CustomerContact
                        // Customer Contact
                        if (objEquipment.ContactID_RFContact.HasValue)
                        {
                            t_Contact cusContact = leaseAppEquip.GetContactEquipment(objEquipment.ContactID_RFContact, relateContact);
                            if (cusContact != null)
                            {
                                lblCustomerContactName.Text = cusContact.FirstName + " " + cusContact.LastName;
                                lblCustomerContactPhone.Text = cusContact.Phone;
                                if (cusContact.PhoneExtension != null)
                                {
                                    lblCustomerContactPhoneX.Visible = true;
                                    lblCustomerContactPhoneExtension.Visible = true;
                                    lblCustomerContactPhoneExtension.Text = cusContact.PhoneExtension;
                                }
                                else
                                {
                                    lblCustomerContactPhoneX.Visible = false;
                                    lblCustomerContactPhoneExtension.Visible = false;
                                }
                                lblCustomerContactFax.Text = cusContact.Fax;
                                lblCustomerContactMobile.Text = cusContact.Mobile;
                                lblCustomerContactEmail.Text = cusContact.Email;
                            }
                        }
                        #endregion CustomerContact

                        #region SiteAcquisitionContact
                        //Site Acquisition Contact
                        if (objEquipment.ContactID_SiteAcquisition.HasValue)
                        {
                            t_Contact SiteContact = leaseAppEquip.GetContactEquipment(objEquipment.ContactID_SiteAcquisition, relateContact);
                            if (SiteContact != null)
                            {
                                lblSiteAcquisitionContactName.Text = SiteContact.FirstName + " " + SiteContact.LastName;
                                lblSiteAcquisitionContactPhone.Text = SiteContact.Phone;
                                if (SiteContact.PhoneExtension != null)
                                {
                                    lblSiteAcquisitionContactPhoneX.Visible = true;
                                    lblSiteAcquisitionContactPhoneExtension.Visible = true;
                                    lblSiteAcquisitionContactPhoneExtension.Text = SiteContact.PhoneExtension;
                                }
                                else
                                {
                                    lblSiteAcquisitionContactPhoneX.Visible = false;
                                    lblSiteAcquisitionContactPhoneExtension.Visible = false;
                                }
                                lblSiteAcquisitionContactFax.Text = SiteContact.Fax;
                                lblSiteAcquisitionContactMobile.Text = SiteContact.Mobile;
                                lblSiteAcquisitionContactEmail.Text = SiteContact.Email;
                            }
                        }
                        #endregion SiteAcquisitionContact
                        //End Customer Contacts
                        #endregion CustomerContacts
                    }
                    else
                    {
                        //Hide all f5827 equipment tab
                        this.liAntennas.Visible = false;
                        this.antennas.Visible = false;
                        this.liAmplifiers.Visible = false;
                        this.amplifiers.Visible = false;
                        this.liGps.Visible = false;
                        this.gps.Visible = false;
                        this.liMicrowave.Visible = false;
                        this.microwave.Visible = false;
                        this.liOther.Visible = false;
                        this.other.Visible = false;
                        this.liGroundspace.Visible = false;
                        this.groundspace.Visible = false;
                        this.liComments.Visible = false;
                        this.comments.Visible = false;
                        this.customer_contacts_with_TMO_Application.Visible = false;
                        ApplicationID.Value = "null";
                    }
                }
            }
         }
      }

      // create table for new Antenna TransmissionLine
      public void CreateTableTransmissionLine(int line, List<LeaseAppEquipment.AntennaEquipmentTxLineType> tmpData, string TMO_AppID)
      {
            Panel pn = new Panel();
            pn.ID = "AntPnLine_" + tmpData[line].ID + "_" + line;
            pn.Style.Add("display", "none");

            Table tb = new Table();
            tb.CssClass = "tableData";
            tb.ID = "AntTableLine_" + tmpData[line].ID + "_" + line;

            string headLine = string.Format("<h4 style=\"width: 453px;\">Transmission Line {0}</h4><br />", (line + 1).ToString());
            for (int row = 0; row < 6; row++)
            {
                TableRow TopTable = new TableRow();
                TableRow tr = new TableRow();

                TableCell cell_0 = new TableCell();
                TableCell cell_1 = new TableCell();
                Label lblInCell_0 = new Label();
                lblInCell_0.CssClass = "equip-prelabel";

                Label lblInCell_1 = new Label();
                switch (row)
                {
                    case 0:
                        lblInCell_0.Text = "Line Type";
                        lblInCell_1.Text = tmpData[line].TxLineType;
                        break;
                    case 1:
                        lblInCell_0.Text = "Number of Lines";
                        lblInCell_1.Text = tmpData[line].TxNumberOfLine.ToString();
                        break;
                    case 2:
                        lblInCell_0.Text = "Manufacturer";
                        lblInCell_1.Text = tmpData[line].TxManufacturer;
                        break;
                    case 3:
                        lblInCell_0.Text = "Model";
                        lblInCell_1.Text = tmpData[line].TxModel;
                        break;
                    case 4:
                        lblInCell_0.Text = "Diameter (INCHES)";
                        lblInCell_1.Text = tmpData[line].TxDiameter;
                        break;
                    case 5:
                        lblInCell_0.Text = "Length (FEET +/-)";
                        lblInCell_1.Text = tmpData[line].TxLength;
                        break;
                }
                cell_0.Style.Add("width","200px");
                cell_0.Controls.Add(lblInCell_0);

                cell_1.Style.Add("width", "100%");
                cell_1.Controls.Add(lblInCell_1);
                
                tr.Cells.Add(cell_0);
                tr.Cells.Add(cell_1);
                
                tb.Rows.Add(tr);
            }

            if (tmpData[line].IsExist)
            {
                pn.Controls.Add(new LiteralControl(headLine));
                pn.Controls.Add(tb);
                AntennaInitPlaceHolder.Controls.Add(pn);
            }
            else
            {
                pn.Controls.Add(new LiteralControl(headLine));
                pn.Controls.Add(tb);
                if (Regex.IsMatch(TMO_AppID, "-M-"))
                {
                    AntennaModPlaceHolder.Controls.Add(pn);
                }
                else
                {
                    AntennaInitPlaceHolder.Controls.Add(pn);
                }
            }
      }

      protected void CustomerContactBinding()
      {
         lease_applications app = this.CurrentLeaseApplication;

         if (app != null)
         {
            // Customer information has two display groups.
             if (Security.IsDisplayGroupVisible("Site Acquisition Contact Info", category, metaDataList, viewFields) &&
                Security.IsDisplayGroupVisible("Site Acquisition Company Address", category, metaDataList, viewFields))
            {
               if (Security.IsFieldViewable(viewFields, "lease_applications", "site_acq_contact_name"))
               {
                  Utility.ControlValueSetter(this.lblCustomer_Name, app.site_acq_contact_name);
               }
               else
               {
                  this.lblCustomer_Name.Visible = false;
               }

               if (Security.IsFieldViewable(viewFields, "lease_applications", "site_acq_contact_phone"))
               {
                  Utility.ControlValueSetter(this.lblCustomer_Phone, app.site_acq_contact_phone);
               }
               else
               {
                  this.lblCustomer_Phone.Visible = false;
               }

               if (Security.IsFieldViewable(viewFields, "lease_applications", "site_acq_contact_email"))
               {
                  Utility.ControlValueSetter(this.lblCustomer_Email, app.site_acq_contact_email);
               }
               else
               {
                  this.lblCustomer_Email.Visible = false;
               }

               if (Security.IsFieldViewable(viewFields, "lease_applications", "site_acq_contact_company"))
               {
                  Utility.ControlValueSetter(this.lblCustomer_Company, app.site_acq_contact_company);
               }
               else
               {
                  this.lblCustomer_Name.Visible = false;
               }

               if (Security.IsFieldViewable(viewFields, "lease_applications", "site_acq_contact_address"))
               {
                  Utility.ControlValueSetter(this.lblCustomer_StreetAddress, app.site_acq_contact_address);
               }
               else
               {
                  this.lblCustomer_StreetAddress.Visible = false;
               }

               if (Security.IsFieldViewable(viewFields, "lease_applications", "site_acq_contact_city"))
               {
                  Utility.ControlValueSetter(this.lblCustomer_City, app.site_acq_contact_city);
               }
               else
               {
                  this.lblCustomer_StreetAddress.Visible = false;
               }

               if (Security.IsFieldViewable(viewFields, "lease_applications", "site_acq_contact_state"))
               {
                  Utility.ControlValueSetter(this.lblCustomer_State, app.site_acq_contact_state);
               }
               else
               {
                  this.lblCustomer_State.Visible = false;
               }

               if (Security.IsFieldViewable(viewFields, "lease_applications", "site_acq_contact_zip"))
               {
                  Utility.ControlValueSetter(this.lblCustomer_Zip, app.site_acq_contact_zip);
               }
               else
               {
                  this.lblCustomer_Zip.Visible = false;
               }
            }
            else
            {
               liCustomerContacts.Style.Add("display", "none");
               customer_contacts.Style.Add("display", "none");
               ulSiteAcquisitionContactInfo.Style.Add("display", "none");
               ulSiteAcquisitionCompanyAddress.Style.Add("display", "none");
            }
         }
      }

      protected void PreliminaryReviewBinding()
      {
         lease_applications app = this.CurrentLeaseApplication;

         if (app != null)
         {
             if (Security.IsDisplayGroupVisible("Preliminary Review", category, metaDataList, viewFields))
            {
               // Preliminary decision and approval function together
               if (Security.IsFieldViewable(viewFields, "lease_applications", "prelim_decision_id"))
               {
                  Utility.ControlValueSetter(this.ddlPreliminaryDecision_ColLeft1, app.prelim_decision_id);
                  Utility.ControlValueSetter(this.txtPreliminaryDecision_ColLeft1, app.prelim_approval_date);

                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "prelim_decision_id", userRole, application_field_access))
                  {
                     this.ddlPreliminaryDecision_ColLeft1.Enabled = false;
                     this.txtPreliminaryDecision_ColLeft1.Enabled = false;
                  }
               }
               else
               {
                  this.lblPreliminaryDecision_ColLeft1.Visible = false;
                  this.txtPreliminaryDecision_ColLeft1.Visible = false;
                  this.ddlPreliminaryDecision_ColLeft1.Visible = false;
               }

               // Preliminary Sitewalk status and date function together.
               if (Security.IsFieldViewable(viewFields, "lease_applications", "prelim_sitewalk_date_status_id"))
               {
                  Utility.ControlValueSetter(this.ddlPreliminarySitewalk_ColLeft1, app.prelim_sitewalk_date_status_id);
                  Utility.ControlValueSetter(this.txtPreliminarySitewalk_ColLeft1, app.prelim_sitewalk_date);

                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "prelim_sitewalk_date_status_id", userRole, application_field_access))
                  {
                     this.ddlPreliminarySitewalk_ColLeft1.Enabled = false;
                     this.txtPreliminarySitewalk_ColLeft1.Enabled = false;
                  }
               }
               else
               {
                  this.divlblPreliminarySitewalk_ColLeft1.Visible = false; 
               }

               if (Security.IsFieldViewable(viewFields, "lease_applications", "sdp_to_customer_date"))
               {
                  Utility.ControlValueSetter(this.txtSDPtoCustomer_ColLeft1, app.sdp_to_customer_date);

                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "sdp_to_customer_date", userRole, application_field_access))
                  {
                     this.txtSDPtoCustomer_ColLeft1.Enabled = false;
                  }
               }
               else
               {
                   this.divSDPtoCustomer_ColLeft1.Visible = false;
               }

               // Landlord 
               if (Security.IsFieldViewable(viewFields, "lease_applications", "landlord_consent_type_id"))
               {
                  Utility.ControlValueSetter(this.ddlLandlordConsentType_ColLeft1, app.landlord_consent_type_id);
                  Utility.ControlValueSetter(this.txtLandlordConsentTypeNote, app.landlord_consent_type_notes);

                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "landlord_consent_type_id", userRole, application_field_access))
                  {
                     this.ddlLandlordConsentType_ColLeft1.Enabled = false;
                     this.txtLandlordConsentTypeNote.Enabled = false;
                  }
               }
               else
               {  
                  this.lblLandlordConsentTypeNote.Visible = false;
                  this.txtLandlordConsentTypeNote.Visible = false;
                  this.divLandlordConsentType_ColLeft1.Visible = false;
               }
            }
            else
            {
               liPreliminaryReview.Style.Add("display", "none");
               preliminary_review.Style.Add("display", "none");

               // Set selected tab to Structural Analysis so that data is automatically visible.
               this.CurrentTab = eTabName.PR;
            }
         }
      }

      protected void SABinding()
      {
         lease_applications app = this.CurrentLeaseApplication;

         if (app != null)
         {
            string json = "";
            int count = 0;

            using (CPTTEntities context = new CPTTEntities())
            {
              
               count = StructuralAnalysis.GetJSON(false, out json, app.id);
               if(count <= 0 )
               {
                   List<SAVendorsInfo> lstSAVendors = SAVendors.GetVendors().ToList();
                   var sort_lstSAVendors = lstSAVendors;
                  var dynamic_lstSAVendors = new DynamicComparer<SAVendorsInfo>();
                  dynamic_lstSAVendors.SortOrder(x => x.SAVendorName);
                  sort_lstSAVendors.Sort(dynamic_lstSAVendors);
                  Utility.BindDDLDataSource(this.ddlPurchaseOrder_Vendor_ColLeft1, sort_lstSAVendors, "ID", "SAVendorName", "- Select -");

                  List<SACDescriptionsInfo> lstDesc = SACDescriptions.GetDescriptions().ToList();
                   var sort_lstDesc = lstDesc;
                   var dynamic_lstDesc = new DynamicComparer<SACDescriptionsInfo>();
                   dynamic_lstDesc.SortOrder(x => x.DescriptionName);
                   sort_lstDesc.Sort(dynamic_lstDesc);
                   Utility.BindDDLDataSource(this.ddlProcessDates_SACDescription_ColLeft1, sort_lstDesc, "ID", "DescriptionName", "- Select -");
               }

               hidden_SA.Value = json;
               hidden_SA_count.Value = count.ToString();
               String jsonEmpty = "";
               StructuralAnalysis.GetJSON(true, out jsonEmpty);
               hidden_SA_Template.Value = jsonEmpty;

               // If none of the sections are viewable, hide the entire tab.
               if (Security.IsFieldViewable(viewFields, "sites", "tower_modification_yn") ||
                   Security.IsDisplayGroupVisible("Payment", category, metaDataList, viewFields) ||
                   Security.IsDisplayGroupVisible("SA Process Dates", category, metaDataList, viewFields) ||
                   Security.IsDisplayGroupVisible("Request", category, metaDataList, viewFields) ||
                   Security.IsDisplayGroupVisible("Delayed Order", category, metaDataList, viewFields)
                  )
               {
                  // Payment Section Security
                   if (!Security.IsDisplayGroupVisible("Payment", category, metaDataList, viewFields))
                  {
                     structural_analysis_payment.Style.Add("display", "none");
                  }
                  else
                  {
                     if (Security.IsFieldViewable(viewFields, "structural_analyses", "sa_fee_received_date"))
                     {
                        lblPayment_FeeProcessed_ColRight1.Visible = true;
                        txtPayment_FeeProcessed_ColRight1.Visible = true;

                        if (!Security.UserCanViewField(User.Identity.Name, "structural_analyses", "sa_fee_received_date", userRole, application_field_access))
                        {
                           lblPayment_FeeProcessed_ColRight1.Visible = false;
                           txtPayment_FeeProcessed_ColRight1.Visible = false;
                        }
                        else
                        {
                            if (!Security.UserCanEditField(User.Identity.Name, "structural_analyses", "sa_fee_received_date", userRole, application_field_access))
                           {
                              txtPayment_FeeProcessed_ColRight1.Enabled = false;
                           }
                        }
                     }
                     else
                     {
                        lblPayment_FeeProcessed_ColRight1.Visible = false;
                        txtPayment_FeeProcessed_ColRight1.Visible = false;
                     }

                     if (Security.IsFieldViewable(viewFields, "structural_analyses", "sa_fee_amount"))
                     {
                        lblPayment_FeeAmount_ColRight1.Visible = true;
                        txtPayment_FeeAmount_ColRight1.Visible = true;

                        if (!Security.UserCanViewField(User.Identity.Name, "structural_analyses", "sa_fee_amount", userRole, application_field_access))
                        {
                           lblPayment_FeeAmount_ColRight1.Visible = false;
                           txtPayment_FeeAmount_ColRight1.Visible = false;
                        }
                        else
                        {
                            if (!Security.UserCanEditField(User.Identity.Name, "structural_analyses", "sa_fee_amount", userRole, application_field_access))
                           {
                              txtPayment_FeeAmount_ColRight1.Enabled = false;
                           }
                        }
                     }
                     else
                     {
                        lblPayment_FeeAmount_ColRight1.Visible = false;
                        txtPayment_FeeAmount_ColRight1.Visible = false;
                     }

                     if (Security.IsFieldViewable(viewFields, "structural_analyses", "sa_check_nbr"))
                     {
                        lblPayment_CheckNumber_ColRight1.Visible = true;
                        txtPayment_CheckNumber_ColRight1.Visible = true;

                        if (!Security.UserCanViewField(User.Identity.Name, "structural_analyses", "sa_check_nbr", userRole, application_field_access))
                        {
                           lblPayment_CheckNumber_ColRight1.Visible = false;
                           txtPayment_CheckNumber_ColRight1.Visible = false;
                        }
                        else
                        {
                            if (!Security.UserCanEditField(User.Identity.Name, "structural_analyses", "sa_check_nbr", userRole, application_field_access))
                           {
                              txtPayment_CheckNumber_ColRight1.Enabled = false;
                           }
                        }
                     }
                     else
                     {
                        lblPayment_CheckNumber_ColRight1.Visible = false;
                        txtPayment_CheckNumber_ColRight1.Visible = false;
                     }

                     if (Security.IsFieldViewable(viewFields, "structural_analyses", "structural_analysis_fee_payor_type_id"))
                     {
                        lblPayment_PayorType_ColRight1.Visible = true;
                        ddlPayment_PayorType_ColRight1.Visible = true;

                        if (!Security.UserCanViewField(User.Identity.Name, "structural_analyses", "structural_analysis_fee_payor_type_id", userRole, application_field_access))
                        {
                           lblPayment_PayorType_ColRight1.Visible = false;
                           ddlPayment_PayorType_ColRight1.Visible = false;
                        }
                        else
                        {
                            if (!Security.UserCanEditField(User.Identity.Name, "structural_analyses", "structural_analysis_fee_payor_type_id", userRole, application_field_access))
                           {
                              ddlPayment_PayorType_ColRight1.Enabled = false;
                           }
                        }
                     }
                     else
                     {
                        lblPayment_PayorType_ColRight1.Visible = false;
                        ddlPayment_PayorType_ColRight1.Visible = false;
                     }
                  }

                  // SA Process Dates Security
                   if (!Security.IsDisplayGroupVisible("SA Process Dates", category, metaDataList, viewFields))
                  {
                     SA_process_dates_wrapper.Style.Add("display", "none");
                  }
                  else
                  {
                     // Structural Analysis Type
                     if (Security.IsFieldViewable(viewFields, "structural_analyses", "structural_analysis_type_id"))
                     {
                        lblProcessDates_Type_ColLeft1.Visible = true;
                        ddlProcessDates_Type_ColLeft1.Visible = true;

                        if (!Security.UserCanViewField(User.Identity.Name, "structural_analyses", "structural_analysis_type_id", userRole, application_field_access))
                        {
                           lblProcessDates_Type_ColLeft1.Visible = false;
                           ddlProcessDates_Type_ColLeft1.Visible = false;
                        }
                        else
                        {
                            if (!Security.UserCanEditField(User.Identity.Name, "structural_analyses", "structural_analysis_type_id", userRole, application_field_access))
                           {
                              ddlProcessDates_Type_ColLeft1.Enabled = false;
                           }
                        }
                     }
                     else
                     {
                        lblProcessDates_Type_ColLeft1.Visible = false;
                        ddlProcessDates_Type_ColLeft1.Visible = false;
                     }

                     // SAC Description
                     if (Security.IsFieldViewable(viewFields, "structural_analyses", "sac_description_id"))
                     {
                        lblProcessDates_SACDescription_ColLeft1.Visible = true;
                        ddlProcessDates_SACDescription_ColLeft1.Visible = true;

                        if (!Security.UserCanViewField(User.Identity.Name, "structural_analyses", "sac_description_id", userRole, application_field_access))
                        {
                           lblProcessDates_SACDescription_ColLeft1.Visible = false;
                           ddlProcessDates_SACDescription_ColLeft1.Visible = false;
                        }
                        else
                        {
                            if (!Security.UserCanEditField(User.Identity.Name, "structural_analyses", "sac_description_id", userRole, application_field_access))
                           {
                              ddlProcessDates_SACDescription_ColLeft1.Enabled = false;
                           }
                        }
                     }
                     else
                     {
                        lblProcessDates_SACDescription_ColLeft1.Visible = false;
                        ddlProcessDates_SACDescription_ColLeft1.Visible = false;
                     }

                     // Ordered status and datefunction together
                     if (Security.IsFieldViewable(viewFields, "structural_analyses", "sa_ordered_date_status_id"))
                     {
                        lblProcessDates_Ordered_ColLeft1.Visible = true;
                        ddlProcessDates_ColLeft1.Visible = true;
                        txtProcessDates_Ordered_ColLeft1.Visible = true;

                        if (!Security.UserCanViewField(User.Identity.Name, "structural_analyses", "sa_ordered_date_status_id", userRole, application_field_access))
                        {
                           lblProcessDates_Ordered_ColLeft1.Visible = false;
                           ddlProcessDates_ColLeft1.Visible = false;
                           txtProcessDates_Ordered_ColLeft1.Visible = false;
                        }
                        else
                        {
                            if (!Security.UserCanEditField(User.Identity.Name, "structural_analyses", "sa_ordered_date_status_id", userRole, application_field_access))
                           {
                              ddlProcessDates_ColLeft1.Enabled = false;
                              txtProcessDates_Ordered_ColLeft1.Enabled = false;
                           }
                        }
                     }
                     else
                     {
                        lblProcessDates_Ordered_ColLeft1.Visible = false;
                        ddlProcessDates_ColLeft1.Visible = false;
                        txtProcessDates_Ordered_ColLeft1.Visible = false;
                     }

                     // Received status and date function together
                     if (Security.IsFieldViewable(viewFields, "structural_analyses", "sa_received_date_status_id"))
                     {
                         divProcessDates_Received_ColLeft1.Visible = true;

                         if (!Security.UserCanViewField(User.Identity.Name, "structural_analyses", "sa_received_date_status_id", userRole, application_field_access))
                        {
                            divProcessDates_Received_ColLeft1.Visible = true;
                        }
                        else
                        {
                            if (!Security.UserCanEditField(User.Identity.Name, "structural_analyses", "sa_received_date_status_id", userRole, application_field_access))
                           {
                              ddlProcess_ReceivedStatus_ColLeft1.Enabled = false;
                              txtProcessDates_Received_ColLeft1.Enabled = false;
                           }
                        }
                     }
                     else
                     {
                         divProcessDates_Received_ColLeft1.Visible = false;
                     }

                     if (Security.IsFieldViewable(viewFields, "structural_analyses", "sa_tower_prcnt"))
                     {
                        lblProcessDates_TowerPercent_ColLeft1.Visible = true;
                        txtProcessDates_TowerPercent_ColLeft1.Visible = true;

                        if (!Security.UserCanViewField(User.Identity.Name, "structural_analyses", "sa_tower_prcnt", userRole, application_field_access))
                        {
                           lblProcessDates_TowerPercent_ColLeft1.Visible = false;
                           txtProcessDates_TowerPercent_ColLeft1.Visible = false;
                        }
                        else
                        {
                            if (!Security.UserCanEditField(User.Identity.Name, "structural_analyses", "sa_tower_prcnt", userRole, application_field_access))
                           {
                              txtProcessDates_TowerPercent_ColLeft1.Enabled = false;
                           }
                        }
                     }
                     else
                     {
                        lblProcessDates_TowerPercent_ColLeft1.Visible = false;
                        txtProcessDates_TowerPercent_ColLeft1.Visible = false;
                     }
                  }

                  // Request Security
                   if (!Security.IsDisplayGroupVisible("Request", category, metaDataList, viewFields))
                  {
                     request_wrapper.Style.Add("display", "none");
                  }
                  else
                  {
                     if (Security.IsFieldViewable(viewFields, "structural_analyses", "saw_to_customer_date"))
                     {
                        lblRequest_SAWToCustomer_ColLeft1.Visible = true;
                        txtRequest_SAWToCustomer_ColLeft1.Visible = true;

                        if (!Security.UserCanViewField(User.Identity.Name, "structural_analyses", "saw_to_customer_date", userRole, application_field_access))
                        {
                           lblRequest_SAWToCustomer_ColLeft1.Visible = false;
                           txtRequest_SAWToCustomer_ColLeft1.Visible = false;
                        }
                        else
                        {
                            if (!Security.UserCanEditField(User.Identity.Name, "structural_analyses", "saw_to_customer_date", userRole, application_field_access))
                           {
                              txtRequest_SAWToCustomer_ColLeft1.Enabled = false;
                           }
                        }
                     }
                     else
                     {
                        lblRequest_SAWToCustomer_ColLeft1.Visible = false;
                        txtRequest_SAWToCustomer_ColLeft1.Visible = false;
                     }

                     if (Security.IsFieldViewable(viewFields, "structural_analyses", "saw_approved_date"))
                     {
                        lblRequest_SAWApproved_ColLeft1.Visible = true;
                        txtRequest_SAWApproved_ColLeft1.Visible = true;

                        if (!Security.UserCanViewField(User.Identity.Name, "structural_analyses", "saw_approved_date", userRole, application_field_access))
                        {
                           lblRequest_SAWApproved_ColLeft1.Visible = false;
                           txtRequest_SAWApproved_ColLeft1.Visible = false;
                        }
                        else
                        {
                            if (!Security.UserCanEditField(User.Identity.Name, "structural_analyses", "saw_approved_date", userRole, application_field_access))
                           {
                              txtRequest_SAWApproved_ColLeft1.Enabled = false;
                           }
                        }
                     }
                     else
                     {
                        lblRequest_SAWToSAC_ColLeft1.Visible = false;
                        txtRequest_SAWToSAC_ColLeft1.Visible = false;
                     }

                     if (Security.IsFieldViewable(viewFields, "structural_analyses", "saw_to_sac_date"))
                     {
                        lblRequest_SAWToSAC_ColLeft1.Visible = true;
                        txtRequest_SAWToSAC_ColLeft1.Visible = true;

                        if (!Security.UserCanViewField(User.Identity.Name, "structural_analyses", "saw_to_sac_date", userRole, application_field_access))
                        {
                           lblRequest_SAWToSAC_ColLeft1.Visible = false;
                           txtRequest_SAWToSAC_ColLeft1.Visible = false;
                        }
                        else
                        {
                            if (!Security.UserCanEditField(User.Identity.Name, "structural_analyses", "saw_to_sac_date", userRole, application_field_access))
                           {
                              txtRequest_SAWToSAC_ColLeft1.Enabled = false;
                           }
                        }
                     }
                     else
                     {
                        lblRequest_SAWToSAC_ColLeft1.Visible = false;
                        txtRequest_SAWToSAC_ColLeft1.Visible = false;
                     }

                     if (Security.IsFieldViewable(viewFields, "structural_analyses", "saw_request_date"))
                     {
                        lblRequest_SAWRequest_ColLeft1.Visible = true;
                        txtRequest_SAWRequest_ColLeft1.Visible = true;

                        if (!Security.UserCanViewField(User.Identity.Name, "structural_analyses", "saw_request_date", userRole, application_field_access))
                        {
                           lblRequest_SAWRequest_ColLeft1.Visible = false;
                           txtRequest_SAWRequest_ColLeft1.Visible = false;
                        }
                        else
                        {
                            if (!Security.UserCanEditField(User.Identity.Name, "structural_analyses", "saw_request_date", userRole, application_field_access))
                           {
                              txtRequest_SAWRequest_ColLeft1.Enabled = false;
                           }
                        }
                     }
                     else
                     {
                        lblRequest_SAWRequest_ColLeft1.Visible = false;
                        txtRequest_SAWRequest_ColLeft1.Visible = false;
                     }

                     if (Security.IsFieldViewable(viewFields, "structural_analyses", "sac_priority_id"))
                     {
                        lblRequest_SACPriority_ColLeft1.Visible = true;
                        ddlRequest_SACPriority_ColLeft1.Visible = true;

                        if (!Security.UserCanViewField(User.Identity.Name, "structural_analyses", "sac_priority_id", userRole, application_field_access))
                        {
                           lblRequest_SACPriority_ColLeft1.Visible = false;
                           ddlRequest_SACPriority_ColLeft1.Visible = false;
                        }
                        else
                        {
                            if (!Security.UserCanEditField(User.Identity.Name, "structural_analyses", "sac_priority_id", userRole, application_field_access))
                           {
                              ddlRequest_SACPriority_ColLeft1.Enabled = false;
                           }
                        }
                     }
                     else
                     {
                        lblRequest_SACPriority_ColLeft1.Visible = false;
                        ddlRequest_SACPriority_ColLeft1.Visible = false;
                     }

                     if (Security.IsFieldViewable(viewFields, "structural_analyses", "sac_name_emp_id"))
                     {
                        lblRequest_SACName_ColLeft1.Visible = true;
                        ddlRequest_SACName_ColLeft1.Visible = true;

                        if (!Security.UserCanViewField(User.Identity.Name, "structural_analyses", "sac_name_emp_id", userRole, application_field_access))
                        {
                           lblRequest_SACName_ColLeft1.Visible = false;
                           ddlRequest_SACName_ColLeft1.Visible = false;
                        }
                        else
                        {
                            if (!Security.UserCanEditField(User.Identity.Name, "structural_analyses", "sac_name_emp_id", userRole, application_field_access))
                           {
                              ddlRequest_SACName_ColLeft1.Enabled = false;
                           }
                        }
                     }
                     else
                     {
                        lblRequest_SACName_ColLeft1.Visible = false;
                        ddlRequest_SACName_ColLeft1.Visible = false;
                     }
                  }

                  // Purchase Order Security
                   if (!Security.IsDisplayGroupVisible("Purchase Order", category, metaDataList, viewFields))
                  {
                     purchase_order_wrapper.Style.Add("display", "none");
                  }
                  else
                  {
                     if (Security.IsFieldViewable(viewFields, "structural_analyses", "sa_vendor_id"))
                     {
                        lblPurchaseOrder_Vendor_ColLeft1.Visible = true;
                        ddlPurchaseOrder_Vendor_ColLeft1.Visible = true;

                        if (!Security.UserCanViewField(User.Identity.Name, "structural_analyses", "sa_vendor_id", userRole, application_field_access))
                        {
                           lblPurchaseOrder_Vendor_ColLeft1.Visible = false;
                           ddlPurchaseOrder_Vendor_ColLeft1.Visible = false;
                        }
                        else
                        {
                            if (!Security.UserCanEditField(User.Identity.Name, "structural_analyses", "sa_vendor_id", userRole, application_field_access))
                           {
                              ddlPurchaseOrder_Vendor_ColLeft1.Enabled = false;
                           }
                        }
                     }
                     else
                     {
                        lblPurchaseOrder_Vendor_ColLeft1.Visible = false;
                        ddlPurchaseOrder_Vendor_ColLeft1.Visible = false;
                     }

                     if (Security.IsFieldViewable(viewFields, "structural_analyses", "shopping_cart_number"))
                     {
                        lblPurchaseOrder_ShoppingCartNumber_ColLeft1.Visible = true;
                        txtPurchaseOrder_ShoppingCartNumber_ColLeft1.Visible = true;

                        if (!Security.UserCanViewField(User.Identity.Name, "structural_analyses", "shopping_cart_number", userRole, application_field_access))
                        {
                           lblPurchaseOrder_ShoppingCartNumber_ColLeft1.Visible = false;
                           txtPurchaseOrder_ShoppingCartNumber_ColLeft1.Visible = false;
                        }
                        else
                        {
                            if (!Security.UserCanEditField(User.Identity.Name, "structural_analyses", "shopping_cart_number", userRole, application_field_access))
                           {
                              txtPurchaseOrder_ShoppingCartNumber_ColLeft1.Enabled = false;
                           }
                        }
                     }
                     else
                     {
                        lblPurchaseOrder_ShoppingCartNumber_ColLeft1.Visible = false;
                        txtPurchaseOrder_ShoppingCartNumber_ColLeft1.Visible = false;
                     }

                     if (Security.IsFieldViewable(viewFields, "structural_analyses", "po_ordered_date"))
                     {
                        lblPurchaseOrder_Ordered_ColLeft1.Visible = true;
                        txtPurchaseOrder_Ordered_ColLeft1.Visible = true;

                        if (!Security.UserCanViewField(User.Identity.Name, "structural_analyses", "po_ordered_date", userRole, application_field_access))
                        {
                           lblPurchaseOrder_Ordered_ColLeft1.Visible = false;
                           txtPurchaseOrder_Ordered_ColLeft1.Visible = false;
                        }
                        else
                        {
                            if (!Security.UserCanEditField(User.Identity.Name, "structural_analyses", "po_ordered_date", userRole, application_field_access))
                           {
                              txtPurchaseOrder_Ordered_ColLeft1.Enabled = false;
                           }
                        }
                     }
                     else
                     {
                        lblPurchaseOrder_Ordered_ColLeft1.Visible = false;
                        txtPurchaseOrder_Ordered_ColLeft1.Visible = false;
                     }

                     if (Security.IsFieldViewable(viewFields, "structural_analyses", "po_received_date"))
                     {
                        this.divPurchaseOrder_Received_ColLeft1.Visible = true;

                        if (!Security.UserCanViewField(User.Identity.Name, "structural_analyses", "po_received_date", userRole, application_field_access))
                        {
                        	this.divPurchaseOrder_Received_ColLeft1.Visible = false;
                        }
                        else
                        {
                            if (!Security.UserCanEditField(User.Identity.Name, "structural_analyses", "po_received_date", userRole, application_field_access))
                           {
                              txtPurchaseOrder_Received_ColLeft1.Enabled = false;
                           }
                        }
                     }
                     else
                     {
                        this.divPurchaseOrder_Received_ColLeft1.Visible = false;
                     }

                     if (Security.IsFieldViewable(viewFields, "structural_analyses", "po_amount"))
                     {
                        lblPurchaseOrder_Amount_ColLeft1.Visible = true;
                        txtPurchaseOrder_Amount_ColLeft1.Visible = true;

                        if (!Security.UserCanViewField(User.Identity.Name, "structural_analyses", "po_amount", userRole, application_field_access))
                        {
                           lblPurchaseOrder_Amount_ColLeft1.Visible = false;
                           txtPurchaseOrder_Amount_ColLeft1.Visible = false;
                        }
                        else
                        {
                            if (!Security.UserCanEditField(User.Identity.Name, "structural_analyses", "po_amount", userRole, application_field_access))
                           {
                              txtPurchaseOrder_Amount_ColLeft1.Enabled = false;
                           }
                        }
                     }
                     else
                     {
                        lblPurchaseOrder_Amount_ColLeft1.Visible = false;
                        txtPurchaseOrder_Amount_ColLeft1.Visible = false;
                     }

                     if (Security.IsFieldViewable(viewFields, "structural_analyses", "po_number"))
                     {
                        lblPurchaseOrder_Number_ColLeft1.Visible = true;
                        txtPurchaseOrder_Number_ColLeft1.Visible = true;

                        if (!Security.UserCanViewField(User.Identity.Name, "structural_analyses", "po_number", userRole, application_field_access))
                        {
                           lblPurchaseOrder_Number_ColLeft1.Visible = false;
                           txtPurchaseOrder_Number_ColLeft1.Visible = false;
                        }
                        else
                        {
                            if (!Security.UserCanEditField(User.Identity.Name, "structural_analyses", "po_number", userRole, application_field_access))
                           {
                              txtPurchaseOrder_Number_ColLeft1.Enabled = false;
                           }
                        }
                     }
                     else
                     {
                        lblPurchaseOrder_Number_ColLeft1.Visible = false;
                        txtPurchaseOrder_Number_ColLeft1.Visible = false;
                     }

                     // PO Blanket Month/Year function together
                     if (Security.IsFieldViewable(viewFields, "structural_analyses", "blanket_po_month"))
                     {
                        lblPurchaseOrder_BlanketPOMonthYear_ColLeft1.Visible = true;
                        ddlPurchaseOrder_BlanketPOMonth_ColLeft1.Visible = true;
                        ddlPurchaseOrder_BlanketPOYear_ColLeft1.Visible = true;

                        if (!Security.UserCanViewField(User.Identity.Name, "structural_analyses", "blanket_po_month", userRole, application_field_access))
                        {
                           lblPurchaseOrder_BlanketPOMonthYear_ColLeft1.Visible = false;
                           ddlPurchaseOrder_BlanketPOMonth_ColLeft1.Visible = false;
                           ddlPurchaseOrder_BlanketPOYear_ColLeft1.Visible = false;
                        }
                        else
                        {
                            if (!Security.UserCanEditField(User.Identity.Name, "structural_analyses", "blanket_po_month", userRole, application_field_access))
                           {
                              ddlPurchaseOrder_BlanketPOMonth_ColLeft1.Enabled = false;
                              ddlPurchaseOrder_BlanketPOYear_ColLeft1.Enabled = false;
                           }
                        }
                     }
                     else
                     {
                        lblPurchaseOrder_BlanketPOMonthYear_ColLeft1.Visible = false;
                        ddlPurchaseOrder_BlanketPOMonth_ColLeft1.Visible = false;
                        ddlPurchaseOrder_BlanketPOYear_ColLeft1.Visible = false;
                     }

                     // PO Release Status and Data function together
                     if (Security.IsFieldViewable(viewFields, "structural_analyses", "po_release_status_id"))
                     {
                        lblPurchaseOrder_POReleaseStatus_ColLeft1.Visible = true;
                        lblPurchaseOrder_PORelease_ColLeft1.Visible = true;
                        ddlPurchaseOrder_POReleaseStatus_ColLeft1.Visible = true;
                        txtPurchaseOrder_PORelease_ColLeft1.Visible = true;

                        if (!Security.UserCanViewField(User.Identity.Name, "structural_analyses", "po_release_status_id", userRole, application_field_access))
                        {
                           lblPurchaseOrder_POReleaseStatus_ColLeft1.Visible = false;
                           lblPurchaseOrder_PORelease_ColLeft1.Visible = false;
                           ddlPurchaseOrder_POReleaseStatus_ColLeft1.Visible = false;
                           txtPurchaseOrder_PORelease_ColLeft1.Visible = false;
                        }
                        else
                        {
                            if (!Security.UserCanEditField(User.Identity.Name, "structural_analyses", "po_release_status_id", userRole, application_field_access))
                           {
                              ddlPurchaseOrder_POReleaseStatus_ColLeft1.Enabled = false;
                              txtPurchaseOrder_PORelease_ColLeft1.Enabled = false;
                           }
                        }
                     }
                     else
                     {
                        lblPurchaseOrder_POReleaseStatus_ColLeft1.Visible = false;
                        lblPurchaseOrder_PORelease_ColLeft1.Visible = false;
                        ddlPurchaseOrder_POReleaseStatus_ColLeft1.Visible = false;
                        txtPurchaseOrder_PORelease_ColLeft1.Visible = false;
                     }

                     if (Security.IsFieldViewable(viewFields, "structural_analyses", "po_notes"))
                     {
                        lblPurchaseOrder_PONotes_ColLeft1.Visible = true;
                        txtPurchaseOrder_PONotes_ColLeft1.Visible = true;

                        if (!Security.UserCanViewField(User.Identity.Name, "structural_analyses", "po_notes", userRole, application_field_access))
                        {
                           lblPurchaseOrder_PONotes_ColLeft1.Visible = false;
                           txtPurchaseOrder_PONotes_ColLeft1.Visible = false;
                        }
                        else
                        {
                            if (!Security.UserCanEditField(User.Identity.Name, "structural_analyses", "po_notes", userRole, application_field_access))
                           {
                              txtPurchaseOrder_PONotes_ColLeft1.Enabled = false;
                           }
                        }
                     }
                     else
                     {
                        lblPurchaseOrder_PONotes_ColLeft1.Visible = false;
                        txtPurchaseOrder_PONotes_ColLeft1.Visible = false;
                     }
                  }

                  // Delayed Order Security
                   if (!Security.IsDisplayGroupVisible("Delayed Order", category, metaDataList, viewFields))
                  {
                     delayed_order_wrapper.Style.Add("display", "none");
                  }
                  else
                  {
                     if (Security.IsFieldViewable(viewFields, "structural_analyses", "sa_order_onhold_date"))
                     {
                         divDelayedOrder_OnHold_ColRight1.Visible = true;

                         if (!Security.UserCanViewField(User.Identity.Name, "structural_analyses", "sa_order_onhold_date", userRole, application_field_access))
                        {
                            divDelayedOrder_OnHold_ColRight1.Visible = false;
                        }
                        else
                        {
                            if (!Security.UserCanEditField(User.Identity.Name, "structural_analyses", "sa_order_onhold_date", userRole, application_field_access))
                           {
                              txtDelayedOrder_Onhold_ColRight1.Enabled = false;
                           }
                        }
                     }
                     else
                     {
                         divDelayedOrder_OnHold_ColRight1.Visible = false;
                     }

                     if (Security.IsFieldViewable(viewFields, "structural_analyses", "sa_order_cancelled_date"))
                     {
                         this.divDelayedOrder_Cancelled_ColRight1.Visible = true;

                         if (!Security.UserCanViewField(User.Identity.Name, "structural_analyses", "sa_order_cancelled_date", userRole, application_field_access))
                        {
                            this.divDelayedOrder_Cancelled_ColRight1.Visible = false;
                        }
                        else
                        {
                            if (!Security.UserCanEditField(User.Identity.Name, "structural_analyses", "sa_order_cancelled_date", userRole, application_field_access))
                           {
                              txtDelayedOrder_Cancelled_ColRight1.Enabled = false;
                           }
                        }
                     }
                     else
                     {
                         this.divDelayedOrder_Cancelled_ColRight1.Visible = false;
                     }
                  }

                  // Sa Decision
                  // TODO: may implement security later
                  if (count > 0)
                  {
                      CreateSaSummaryTable();
                      div_sa_summary_table.Visible = true;
                  }
                  else
                  {
                      div_sa_summary_table.Visible = false;
                  }
                  this.div_sa_decision_wrapper.Visible = true;

               }
               else
               {
                  liStructuralAnalysis.Visible = false;
                  structural_analysis.Style.Add("display", "none");
               }
            }
         }
      }

      protected void SaSummaryRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
      {
          if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
          {
              if (e.Item.DataItem.GetType().Name == "JObject")
              {
                  JObject item = (JObject)e.Item.DataItem;
                  if (item != null)
                  {
                      Label lblSummaryReceived = (Label)e.Item.FindControl("SummaryReceived");
                      lblSummaryReceived.Text = item["sa_received_date"].ToString();
                      Label lblSummaryDecisionDate = (Label)e.Item.FindControl("SummaryDecisionDate");
                      lblSummaryDecisionDate.Text = item["sa_decision_date"].ToString();
                      Label lblSummaryDecision = (Label)e.Item.FindControl("SummaryDecision");
                      lblSummaryDecision.Text = item["sa_decision_type_text"].ToString();
                      Label lblSummaryProcessType = (Label)e.Item.FindControl("SummaryProcessType");
                      lblSummaryProcessType.Text = item["structural_analysis_type_text"].ToString();
                  }
              }
          }
      }

      public void CreateSaSummaryTable()
      {
          string jsonString = hidden_SA.Value;
          var jsonSa = Newtonsoft.Json.JsonConvert.DeserializeObject(jsonString);
          Newtonsoft.Json.Linq.JArray jsonCollection = (Newtonsoft.Json.Linq.JArray)jsonSa;
          if (jsonCollection != null)
          {
              var sortData = jsonCollection.OrderByDescending(a => a["ID"]);

              saRepeater.DataSource = sortData;
              saRepeater.ItemDataBound += new RepeaterItemEventHandler(SaSummaryRepeater_ItemDataBound);
              saRepeater.DataBind();
          }
      }

      protected void RevisionsBinding()
      {
         lease_applications app = this.CurrentLeaseApplication;

         if (app != null)
         {
            string json = "";
            int count = 0;
            using (CPTTEntities ce = new CPTTEntities())
            {
                count = LeaseAppRevisions.GetJSON(false, out json, app.id);

                hidden_revisions.Value = json;
                hidden_revision_count.Value = count.ToString();
                String jsonEmpty = "";
                LeaseAppRevisions.GetJSON(true, out jsonEmpty);
                hidden_revision_template.Value = jsonEmpty;
                lblNoOfRevision.Text = count.ToString();
                if (Security.IsDisplayGroupVisible("Revisions", category, metaDataList, viewFields))
               {
                  // Apply view/edit security.

                  // Revision Received Date
                  if (Security.IsFieldViewable(viewFields, "leaseapp_revisions", "revision_rcvd_date"))
                  {
                     lblRevisionReveivedDate.Visible = true;
                     txtRevisionReceivedDate.Visible = true;

                     if (!Security.UserCanEditField(User.Identity.Name, "leaseapp_revisions", "revision_rcvd_date", userRole, application_field_access))
                     {
                        txtRevisionReceivedDate.Enabled = false;
                     }
                  }
                  else
                  {
                     lblRevisionReveivedDate.Visible = false;
                     txtRevisionReceivedDate.Visible = false;
                  }

                  // Revision Fee Received Date
                  if (Security.IsFieldViewable(viewFields, "leaseapp_revisions", "revision_fee_rcvd_date"))
                  {
                     lblRevisionFeeReceived.Visible = true;
                     txtRevisionFeeReceivedDate.Visible = true;

                     if (!Security.UserCanEditField(User.Identity.Name, "leaseapp_revisions", "revision_fee_rcvd_date", userRole, application_field_access))
                     {
                        txtRevisionFeeReceivedDate.Enabled = false;
                     }
                  }
                  else
                  {
                     lblRevisionFeeReceived.Visible = true;
                     txtRevisionFeeReceivedDate.Visible = true;
                  }

                  // Revision Fee Check PO Number
                  if (Security.IsFieldViewable(viewFields, "leaseapp_revisions", "revision_fee_check_po_nbr"))
                  {
                     lblRevisionCheckNo.Visible = true;
                     txtRevisionCheckNo.Visible = true;

                     if (!Security.UserCanEditField(User.Identity.Name, "leaseapp_revisions", "revision_fee_check_po_nbr", userRole, application_field_access))
                     {
                        txtRevisionCheckNo.Enabled = false;
                     }
                  }
                  else
                  {
                     lblRevisionCheckNo.Visible = true;
                     txtRevisionCheckNo.Visible = true;
                  }

                  // Revision Fee Amount
                  if (Security.IsFieldViewable(viewFields, "leaseapp_revisions", "revision_fee_amount"))
                  {
                     //lblRevisionFeeAmount.Visible = false;
                     //txtRevisionFeeAmount.Visible = false;

                     //Change to visible by Nam 2012-03-02
                     lblRevisionFeeAmount.Visible = true;
                     txtRevisionFeeAmount.Visible = true;

                     if (!Security.UserCanEditField(User.Identity.Name, "leaseapp_revisions", "revision_fee_amount", userRole, application_field_access))
                     {
                        txtRevisionFeeAmount.Enabled = false;
                     }
                  }
                  else
                  {
                     lblRevisionFeeAmount.Visible = true;
                     txtRevisionFeeAmount.Visible = true;
                  }
               }
               else
               {
                  liNoOfRevision.Visible = false;
                  lblNoOfRevision.Visible = false;
                  liRevisions.Visible = false;
                  revisions_wrapper.Style.Add("display", "none");
               }
            }
         }
      }

      protected void DocumentReviewBinding()
      {
         lease_applications app = this.CurrentLeaseApplication;

         if (app != null)
         {
            // If none of the sections are viewable, hide the entire tab.
            if (Security.IsFieldViewable(viewFields, "lease_applications", "cohost_tenant_id") ||
                Security.IsDisplayGroupVisible("Lease Exhibit", category, metaDataList, viewFields) ||
                Security.IsDisplayGroupVisible("Landlord Consent", category, metaDataList, viewFields) ||
                Security.IsDisplayGroupVisible("Construction Drawings", category, metaDataList, viewFields)
               )
            {
               // Cohost Tenant
               if (Security.IsFieldViewable(viewFields, "lease_applications", "cohost_tenant_id"))
               {
                  Utility.ControlValueSetter(this.ddlLeaseExhibit_CoHostTenant, app.cohost_tenant_id);

                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "cohost_tenant_id", userRole, application_field_access))
                  {
                     ddlLeaseExhibit_CoHostTenant.Enabled = false;
                  }
               }
               else
               {
                  this.lblLeaseExhibit_CoHostTenant.Visible = false;
                  this.ddlLeaseExhibit_CoHostTenant.Visible = false;
               }

               //Lease Exhibit
               if (Security.IsDisplayGroupVisible("Lease Exhibit", category, metaDataList, viewFields))
               {
                  // Lease exhibit ordered status and date function together
                  if (Security.IsFieldViewable(viewFields, "lease_applications", "leaseexhbt_ordered_date_status_id"))
                  {
                     Utility.ControlValueSetter(this.ddlLeaseExhibit_Ordered, app.leaseexhbt_ordered_date_status_id);
                     Utility.ControlValueSetter(this.txtLeaseExhibit_Ordered, app.leaseexhbt_ordered_date);

                     if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "leaseexhbt_ordered_date_status_id", userRole, application_field_access))
                     {
                        this.ddlLeaseExhibit_Ordered.Enabled = false;
                        this.txtLeaseExhibit_Ordered.Enabled = false;
                     }
                  }
                  else
                  {
                     this.lblLeaseExhibit_Ordered.Visible = false;
                     this.ddlLeaseExhibit_Ordered.Visible = false;
                     this.txtLeaseExhibit_Ordered.Visible = false;
                  }

                  if (Security.IsFieldViewable(viewFields, "lease_applications", "leaseexhbt_rcvd_date"))
                  {
                     Utility.ControlValueSetter(this.txtLeaseExhibit_Received, app.leaseexhbt_rcvd_date);

                     if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "leaseexhbt_rcvd_date", userRole, application_field_access))
                     {
                        this.txtLeaseExhibit_Received.Enabled = false;
                     }
                  }
                  else
                  {
                      this.divLeaseExhibit_Received.Visible = false;
                  }

                  // Lease exhibit decision status and date function together.
                  if (Security.IsFieldViewable(viewFields, "lease_applications", "leaseexhbt_decision_id"))
                  {
                     Utility.ControlValueSetter(this.ddlLeaseExhibit_Decision, app.leaseexhbt_decision_id);
                     Utility.ControlValueSetter(this.txtLeaseExhibit_Decision, app.leaseexhbt_decision_date);

                     if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "leaseexhbt_decision_id", userRole, application_field_access))
                     {
                        this.ddlLeaseExhibit_Decision.Enabled = false;
                        this.txtLeaseExhibit_Decision.Enabled = false;
                     }
                  }
                  else
                  {
                      this.divLeaseExhibit_Decision.Visible = false;
                  }
               }
               else
               {
                  lease_exhibit_wrapper.Style.Add("display", "none");
               }

               //Landlord Consent                  
               if (Security.IsDisplayGroupVisible("Landlord Consent", category, metaDataList, viewFields))
               {

                  if (Security.IsFieldViewable(viewFields, "lease_applications", "landlord_notice_sent_date"))
                  {
                     Utility.ControlValueSetter(this.txtLandlordConsent_LandlordNotice, app.landlord_notice_sent_date);

                     if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "landlord_notice_sent_date", userRole, application_field_access))
                     {
                        this.txtLandlordConsent_LandlordNotice.Enabled = false;
                     }
                  }
                  else
                  {
                      this.divLandlordConsent_LandlordNotice.Visible = false;
                  }

                  // Landlord consent sent status and date function together.
                  if (Security.IsFieldViewable(viewFields, "lease_applications", "landlord_consent_sent_date"))
                  {
                     Utility.ControlValueSetter(this.txtLandlordConsent_ConsentRequested, app.landlord_consent_sent_date);

                     if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "landlord_consent_sent_date", userRole, application_field_access))
                     {
                        this.txtLandlordConsent_ConsentRequested.Enabled = false;
                     }
                  }
                  else
                  {
                      this.divLandlordConsentRequested.Visible = false;
                  }

                  if (Security.IsFieldViewable(viewFields, "lease_applications", "landlord_consent_rcvd_date"))
                  {
                     Utility.ControlValueSetter(this.txtLandlordConsent_ReceivedDate, app.landlord_consent_rcvd_date);

                     if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "landlord_consent_rcvd_date", userRole, application_field_access))
                     {
                        this.txtLandlordConsent_ReceivedDate.Enabled = false;
                     }
                  }
                  else
                  {
                      this.divLandlordConsentReceived.Visible = false;
                  }
               }
               else
               {
                  landlord_consent_wrapper.Style.Add("display", "none");
               }

               // Construction Drawing
               if (Security.IsDisplayGroupVisible("Construction Drawings", category, metaDataList, viewFields))
               {
                  // Construction drawing decision status and date function together
                  if (Security.IsFieldViewable(viewFields, "lease_applications", "cd_decision_id"))
                  {
                     Utility.ControlValueSetter(this.ddlConstructionDrawings_Decision, app.cd_decision_id);
                     Utility.ControlValueSetter(this.txtConstructionDrawings_Decision, app.cd_decision_date);

                     if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "cd_decision_id", userRole, application_field_access))
                     {
                        this.ddlConstructionDrawings_Decision.Enabled = false;
                        this.txtConstructionDrawings_Decision.Enabled = false;
                     }
                  }
                  else
                  {
                     this.lblConstructionDrawingsDecision.Visible = false;
                     this.ddlConstructionDrawings_Decision.Visible = false;
                     this.txtConstructionDrawings_Decision.Visible = false;
                  }

                  if (Security.IsFieldViewable(viewFields, "lease_applications", "cd_rcvd_date"))
                  {
                     Utility.ControlValueSetter(this.txtConstructionDrawings_Received, app.cd_rcvd_date);

                     if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "cd_rcvd_date", userRole, application_field_access))
                     {
                        this.txtConstructionDrawings_Received.Enabled = false;
                     }
                  }
                  else
                  {
                      this.divConstructionDrawingsReceived.Visible = false;
                  }
               }
               else
               {
                  construction_drawings_wrapper.Style.Add("display", "none");
               }
            }
            else
            {
               liDocumentReview.Style.Add("display", "none");
               document_review.Style.Add("display", "none");
            }
         }
      }

      protected void ConstructionBinding()
      {
         lease_applications app = this.CurrentLeaseApplication;

         if (app != null)
         {
            //For NTP date Calculation fields
            NTPDateCalculation objNTP = new NTPDateCalculation(app.ntp_issued_date);

            //Preconstruction
            if (Security.IsDisplayGroupVisible("Preconstruction", category, metaDataList, viewFields))
            {
               // Preconstruction sitewalk status and date function together
               if (Security.IsFieldViewable(viewFields, "lease_applications", "preconstr_sitewalk_date_status_id"))
               {
                  Utility.ControlValueSetter(this.ddlPreconstruction_PreconstructionSitewalk, app.preconstr_sitewalk_date_status_id);
                  Utility.ControlValueSetter(this.txtPreconstruction_PreconstructionSitewalk, app.preconstr_sitewalk_date);

                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "preconstr_sitewalk_date_status_id", userRole, application_field_access))
                  {
                     this.ddlPreconstruction_PreconstructionSitewalk.Enabled = false;
                     this.txtPreconstruction_PreconstructionSitewalk.Enabled = false;
                  }
               }
               else
               {
                  this.lblPreconstruction_PreconstructionSitewalk.Visible = false;
                  this.ddlPreconstruction_PreconstructionSitewalk.Visible = false;
                  this.txtPreconstruction_PreconstructionSitewalk.Visible = false;
               }

               // NTP Issue status and date function together.
               if (Security.IsFieldViewable(viewFields, "lease_applications", "ntp_issued_date_status_id"))
               {
                  Utility.ControlValueSetter(this.ddlPreconstruction_NTPIssuedDateStatus, app.ntp_issued_date_status_id);
                  Utility.ControlValueSetter(this.txtPreconstruction_NTPIssuedDateStatus, app.ntp_issued_date);

                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "ntp_issued_date_status_id", userRole, application_field_access))
                  {
                     this.ddlPreconstruction_NTPIssuedDateStatus.Enabled = false;
                     this.txtPreconstruction_NTPIssuedDateStatus.Enabled = false;
                  }
               }
               else
               {
                  this.lblPreconstruction_NTPIssuedDateStatus.Visible = false;
                  this.ddlPreconstruction_NTPIssuedDateStatus.Visible = false;
                  this.txtPreconstruction_NTPIssuedDateStatus.Visible = false;
               }

               if (Security.IsFieldViewable(viewFields, "lease_applications", "ntp_verified_date"))
               {
                  Utility.ControlValueSetter(this.txtPreconstruction_NTPVerified, app.ntp_verified_date);

                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "ntp_verified_date", userRole, application_field_access))
                  {
                     this.txtPreconstruction_NTPVerified.Enabled = false;
                  }
               }
               else
               {
                  this.lblPreconstruction_NTPVerified.Visible = false;
                  this.txtPreconstruction_NTPVerified.Visible = false;
               }
            }
            else
            {
               liConstruction.Style.Add("display", "none");
               construction.Style.Add("display", "none");
            }

            //Early Installation
            if (Security.IsDisplayGroupVisible("Early Installation", category, metaDataList, viewFields))
            {
               if (Security.IsFieldViewable(viewFields, "lease_applications", "early_installation_discovered_date"))
               {
                  Utility.ControlValueSetter(this.txtEarlyInstallation_ViolaitonDiscovered, app.early_installation_discovered_date);

                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "early_installation_discovered_date", userRole, application_field_access))
                  {
                     this.txtEarlyInstallation_ViolaitonDiscovered.Enabled = false;
                  }
               }
               else
               {
                  this.lblEarlyInstallation_ViolationDiscovered.Visible = false;
                  this.txtEarlyInstallation_ViolaitonDiscovered.Visible = false;
               }

               if (Security.IsFieldViewable(viewFields, "lease_applications", "early_installation_violation_letter_sent_date"))
               {
                  Utility.ControlValueSetter(this.txtEarlyInstallation_ViolationLetterSent, app.early_installation_violation_letter_sent_date);

                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "early_installation_violation_letter_sent_date", userRole, application_field_access))
                  {
                     this.txtEarlyInstallation_ViolationLetterSent.Enabled = false;
                  }
               }
               else
               {
                  this.lblEarlyInstalltion_ViolationLetterSent.Visible = false;
                  this.txtEarlyInstallation_ViolationLetterSent.Visible = false;
               }

               if (Security.IsFieldViewable(viewFields, "lease_applications", "early_installation_violation_invoice_created_by_finops_date"))
               {
                  Utility.ControlValueSetter(this.txtEarlyInstallation_ViolationInvoiceCreatedByFinops, app.early_installation_violation_invoice_created_by_finops_date);

                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "early_installation_violation_invoice_created_by_finops_date", userRole, application_field_access))
                  {
                     this.txtEarlyInstallation_ViolationInvoiceCreatedByFinops.Enabled = false;
                  }
               }
               else
               {
                  this.lblEarlyInstallation_ViolationInvoiceCreatedByFinops.Visible = false;
                  this.txtEarlyInstallation_ViolationInvoiceCreatedByFinops.Visible = false;
               }

               if (Security.IsFieldViewable(viewFields, "lease_applications", "early_installation_violation_invoice_number"))
               {
                  Utility.ControlValueSetter(this.txtEarlyInstallation_ViolationInvoiceNumber, app.early_installation_violation_invoice_number);

                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "early_installation_violation_invoice_number", userRole, application_field_access))
                  {
                     this.txtEarlyInstallation_ViolationInvoiceNumber.Enabled = false;
                  }
               }
               else
               {
                  this.lblEarlyInstallation_ViolationInvoiceNumber.Visible = false;
                  this.txtEarlyInstallation_ViolationInvoiceNumber.Visible = false;
               }

               if (Security.IsFieldViewable(viewFields, "lease_applications", "early_installation_violation_fee_rcvd_date"))
               {
                  Utility.ControlValueSetter(this.txtEarlyInstallation_ViolationFeeReceived, app.early_installation_violation_fee_rcvd_date);

                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "early_installation_violation_fee_rcvd_date", userRole, application_field_access))
                  {
                     this.txtEarlyInstallation_ViolationFeeReceived.Enabled = false;
                  }
               }
               else
               {
                  this.lblEarlyInstallation_ViolationFeeReceived.Visible = false;
                  this.txtEarlyInstallation_ViolationFeeReceived.Visible = false;
               }
            }
            else
            {
               early_installation_wrapper.Style.Add("display", "none");
            }

            //Extension Notice
            if (Security.IsDisplayGroupVisible("Extension Notice", category, metaDataList, viewFields))
            {
               if (Security.IsFieldViewable(viewFields, "lease_applications", "extension_notice_rcvd_date"))
               {
                  Utility.ControlValueSetter(this.txtExtensionNotice_Received, app.extension_notice_rcvd_date);

                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "extension_notice_rcvd_date", userRole, application_field_access))
                  {
                     this.txtExtensionNotice_Received.Enabled = false;
                  }
               }
               else
               {
                  this.lblExtensionNotice_Reveeived.Visible = false;
                  this.txtExtensionNotice_Received.Visible = false;
               }

               if (Security.IsFieldViewable(viewFields, "lease_applications", "extension_notice_fee_rcvd_date"))
               {
                  Utility.ControlValueSetter(this.txtExtensionNotice_FeeReceived, app.extension_notice_fee_rcvd_date);

                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "extension_notice_fee_rcvd_date", userRole, application_field_access))
                  {
                     this.txtExtensionNotice_FeeReceived.Enabled = false;
                  }
               }
               else
               {
                  this.lblExtensionNotice_FeeReceived.Visible = false;
                  this.txtExtensionNotice_FeeReceived.Visible = false;
               }

               if (Security.IsFieldViewable(viewFields, "lease_applications", "extension_notice_invoice_created_by_finops_date"))
               {
                  Utility.ControlValueSetter(this.txtExtensionNotice_InvoiceCreatedByFinops, app.extension_notice_invoice_created_by_finops_date);

                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "extension_notice_invoice_created_by_finops_date", userRole, application_field_access))
                  {
                     this.txtExtensionNotice_InvoiceCreatedByFinops.Enabled = false;
                  }
               }
               else
               {
                  this.lblExtensionNotice_InvoiceCreatedByFinops.Visible = false;
                  this.txtExtensionNotice_InvoiceCreatedByFinops.Visible = false;
               }

               if (Security.IsFieldViewable(viewFields, "lease_applications", "extension_notice_fee_invoice_number"))
               {
                  Utility.ControlValueSetter(this.txtExtensionNotice_FeeInvoiceNumber, app.extension_notice_fee_invoice_number);

                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "extension_notice_fee_invoice_number", userRole, application_field_access))
                  {
                     this.txtExtensionNotice_FeeInvoiceNumber.Enabled = false;
                  }
               }
               else
               {
                  this.lblExtensionNotice_FeeInvoiceNumber.Visible = false;
                  this.txtExtensionNotice_FeeInvoiceNumber.Visible = false;
               }

               // These fields are calculated and auto assumed visible.
               Utility.ControlValueSetter(this.lblExtensionNotice_150daysPostNTP, objNTP.PostNTPnDay(150));
               Utility.ControlValueSetter(this.lblExtensionNotice_180daysPostNTP, objNTP.PostNTPnDay(180));
               Utility.ControlValueSetter(this.lblExtensionNotice_210daysPostNTP, objNTP.PostNTPnDay(210));
            }
            else
            {
               extension_notice_wrapper.Style.Add("display", "none");
            }

            //Equipment
            if (Security.IsDisplayGroupVisible("Equipment", category, metaDataList, viewFields))
            {
               if (Security.IsFieldViewable(viewFields, "lease_applications", "equipment_removed_date"))
               {
                  Utility.ControlValueSetter(this.txtEquipment_Removed, app.equipment_removed_date);

                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "equipment_removed_date", userRole, application_field_access))
                  {
                     this.txtEquipment_Removed.Enabled = false;
                  }
               }
               else
               {
                  this.lblEquipment_Removed.Visible = false;
                  this.txtEquipment_Removed.Visible = false;
               }

               if (Security.IsFieldViewable(viewFields, "lease_applications", "failure_to_remove_violation"))
               {
                  Utility.ControlValueSetter(this.ddlEquipment_FailureToRemoveViolation, app.failure_to_remove_violation);

                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "failure_to_remove_violation", userRole, application_field_access))
                  {
                     this.ddlEquipment_FailureToRemoveViolation.Enabled = false;
                  }
               }
               else
               {
                  this.lblEquipment_FailureToRemoveViolation.Visible = false;
                  this.ddlEquipment_FailureToRemoveViolation.Visible = false;
               }

               if (Security.IsFieldViewable(viewFields, "lease_applications", "failure_to_remove_violation_letter_sent_date"))
               {
                  Utility.ControlValueSetter(this.txtEquipment_FailureToRemoveViolationLetterSent, app.failure_to_remove_violation_letter_sent_date);

                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "failure_to_remove_violation_letter_sent_date", userRole, application_field_access))
                  {
                     this.txtEquipment_FailureToRemoveViolationLetterSent.Enabled = false;
                  }
               }
               else
               {
                  this.lblEquipment_FailureToRemoveViolationLetterSent.Visible = false;
                  this.txtEquipment_FailureToRemoveViolationLetterSent.Visible = false;
               }

               if (Security.IsFieldViewable(viewFields, "lease_applications", "failure_to_remove_violation_letter_submitted_to_cst_date"))
               {
                  Utility.ControlValueSetter(this.txtEquipment_FailureToRemoveViolationLetterSubmittedToCst, app.failure_to_remove_violation_letter_submitted_to_cst_date);

                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "failure_to_remove_violation_letter_submitted_to_cst_date", userRole, application_field_access))
                  {
                     this.txtEquipment_FailureToRemoveViolationLetterSubmittedToCst.Enabled = false;
                  }
               }
               else
               {
                  this.lblEquipment_FailureToRemoveViolationLetterSubmittedToCst.Visible = false;
                  this.txtEquipment_FailureToRemoveViolationLetterSubmittedToCst.Visible = false;
               }

               if (Security.IsFieldViewable(viewFields, "lease_applications", "extension_notice_fee_invoice_number"))
               {
                  Utility.ControlValueSetter(this.txtEquipment_ViolationEquipmentRemovalConfirmedSubmittedToCst, app.violation_equipment_removal_confirmed_submitted_to_cst_date);

                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "violation_equipment_removal_confirmed_submitted_to_cst_date", userRole, application_field_access))
                  {
                     this.txtEquipment_ViolationEquipmentRemovalConfirmedSubmittedToCst.Enabled = false;
                  }
               }
               else
               {
                  this.lblEquipment_ViolationEquipmentRemovalConfirmedSubmittedToCst.Visible = false;
                  this.txtEquipment_ViolationEquipmentRemovalConfirmedSubmittedToCst.Visible = false;
               }
            }
            else
            {
               equipment_wrapper.Style.Add("display", "none");
            }

            //Post Construction
            if (Security.IsDisplayGroupVisible("Post Construction", category, metaDataList, viewFields))
            {
               // Post construction sitewalk status and date function together
               if (Security.IsFieldViewable(viewFields, "lease_applications", "postconstr_sitewalk_date_status_id"))
               {
                  Utility.ControlValueSetter(this.ddlPostConstruction_SiteWalk, app.postconstr_sitewalk_date_status_id);
                  Utility.ControlValueSetter(this.txtPostConstruction_SiteWalk, app.postconstr_sitewalk_date);

                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "postconstr_sitewalk_date_status_id", userRole, application_field_access))
                  {
                     this.ddlPostConstruction_SiteWalk.Enabled = false;
                     this.txtPostConstruction_SiteWalk.Enabled = false;
                  }
               }
               else
               {
                  this.lblPostConstruction_Sitewalk.Visible = false;
                  this.ddlPostConstruction_SiteWalk.Visible = false;
                  this.txtPostConstruction_SiteWalk.Visible = false;
               }

               // Post construction ordered status and date function together.
               if (Security.IsFieldViewable(viewFields, "lease_applications", "postconstr_ordered_date_status_id"))
               {
                  Utility.ControlValueSetter(this.txtPostConstruction_Ordered, app.postconstr_ordered_date);
                  Utility.ControlValueSetter(this.ddlPostConstruction_Ordered, app.postconstr_ordered_date_status_id);
                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "postconstr_ordered_date_status_id", userRole, application_field_access))
                  {
                     this.ddlPostConstruction_Ordered.Enabled = false;
                     this.txtPostConstruction_Ordered.Enabled = false;
                  }
               }
               else
               {
                  this.lblPostConstruction_Ordered.Visible = false;
                  this.ddlPostConstruction_Ordered.Visible = false;
                  this.txtPostConstruction_Ordered.Visible = false;
               }

               // Construction complete status and date function together.
               if (Security.IsFieldViewable(viewFields, "lease_applications", "constr_cmplt_date_status_id"))
               {
                  Utility.ControlValueSetter(this.ddlPostConstruction_ConstructionCompleted, app.constr_cmplt_date_status_id);
                  Utility.ControlValueSetter(this.txtPostConstruction_ConstructionCompleted, app.constr_cmplt_date);

                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "constr_cmplt_date_status_id", userRole, application_field_access))
                  {
                     this.ddlPostConstruction_ConstructionCompleted.Enabled = false;
                     this.txtPostConstruction_ConstructionCompleted.Enabled = false;
                  }
               }
               else
               {
                  this.lblPostConstruction_ConstructionCompleted.Visible = false;
                  this.ddlPostConstruction_ConstructionCompleted.Visible = false;
                  this.txtPostConstruction_ConstructionCompleted.Visible = false;
               }
            }
            else
            {
               post_construction_wrapper.Style.Add("display", "none");
            }

            //TODO : add Lease Defined Construction Complete data after database field is added.

            //Close Out
            if (Security.IsDisplayGroupVisible("Close Out", category, metaDataList, viewFields))
            {
               // The close out date is calculated based off the extension notice fee rcvd date and 
               // will be visible/hidden accordingly.
               if (Security.IsFieldViewable(viewFields, "lease_applications", "extension_notice_fee_rcvd_date"))
               {
                  Utility.ControlValueSetter(this.lblCloseOut_DueDate, objNTP.AllCloseOutDocsDueDate(app.extension_notice_fee_rcvd_date));
               }
               else
               {
                  liCloseOut_DueDate.Style.Add("display", "none");
               }

               if (Security.IsFieldViewable(viewFields, "lease_applications", "all_closeout_docs_rcvd_date"))
               {
                  Utility.ControlValueSetter(this.txtCloseOut_Received, app.all_closeout_docs_rcvd_date);

                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "all_closeout_docs_rcvd_date", userRole, application_field_access))
                  {
                     this.txtCloseOut_Received.Enabled = false;
                  }
               }
               else
               {
                  liCloseOut_Received.Style.Add("display", "none");
               }

               if (Security.IsFieldViewable(viewFields, "lease_applications", "closeout_docs_fee_required"))
               {
                  Utility.ControlValueSetter(this.ddlCloseOut_FeeRequired, app.closeout_docs_fee_required);

                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "closeout_docs_fee_required", userRole, application_field_access))
                  {
                     this.ddlCloseOut_FeeRequired.Enabled = false;
                  }
               }
               else
               {
                  liCloseOut_FeeRequired.Style.Add("display", "none");
               }

               if (Security.IsFieldViewable(viewFields, "lease_applications", "closeout_docs_notice_sent_date"))
               {
                  Utility.ControlValueSetter(this.txtCloseOut_NoticeSent, app.closeout_docs_notice_sent_date);

                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "constr_cmplt_date_status_id", userRole, application_field_access))
                  {
                     this.txtCloseOut_NoticeSent.Enabled = false;
                  }
               }
               else
               {
                  liCloseOut_NoticeSent.Style.Add("display", "none");
               }

               if (Security.IsFieldViewable(viewFields, "lease_applications", "closeout_docs_fee_invoice_sent_date"))
               {
                  Utility.ControlValueSetter(this.txtCloseOut_FeeInvoiceSent, app.closeout_docs_fee_invoice_sent_date);

                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "closeout_docs_fee_invoice_sent_date", userRole, application_field_access))
                  {
                     this.txtCloseOut_FeeInvoiceSent.Enabled = false;
                  }
               }
               else
               {
                  liCloseOut_FeeInvoiceSent.Style.Add("display", "none");
               }

               if (Security.IsFieldViewable(viewFields, "lease_applications", "closeout_doc_fee_invoice_created_by_finops_date"))
               {
                  Utility.ControlValueSetter(this.txtCloseOut_FeeInvoiceCreatedByFinops, app.closeout_doc_fee_invoice_created_by_finops_date);

                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "closeout_doc_fee_invoice_created_by_finops_date", userRole, application_field_access))
                  {
                     this.txtCloseOut_FeeInvoiceCreatedByFinops.Enabled = false;
                  }
               }
               else
               {
                  liCloseOut_FeeInvoiceCreatedByFinOps.Style.Add("display", "none");
               }

               if (Security.IsFieldViewable(viewFields, "lease_applications", "closeout_docs_fee_invoice_number"))
               {
                  Utility.ControlValueSetter(this.txtCloseOut_FeeInvoiceNumber, app.closeout_docs_fee_invoice_number);

                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "closeout_docs_fee_invoice_number", userRole, application_field_access))
                  {
                     this.txtCloseOut_FeeInvoiceNumber.Enabled = false;
                  }
               }
               else
               {
                  liCloseOut_FeeInvoiceNumber.Style.Add("display", "none");
               }

               if (Security.IsFieldViewable(viewFields, "lease_applications", "closeout_docs_fee_rcvd_date"))
               {
                  Utility.ControlValueSetter(this.txtCloseOut_FeeReceived, app.closeout_docs_fee_rcvd_date);

                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "closeout_docs_fee_rcvd_date", userRole, application_field_access))
                  {
                     this.txtCloseOut_FeeReceived.Enabled = false;
                  }
               }
               else
               {
                  liCloseOut_FeeReceived.Style.Add("display", "none");
               }

               // Final site approval status and date function together
               if (Security.IsFieldViewable(viewFields, "lease_applications", "final_site_apprvl_date_status_id"))
               {
                  Utility.ControlValueSetter(this.ddlCloseOut_FinalSiteApproval, app.final_site_apprvl_date_status_id);
                  Utility.ControlValueSetter(this.txtCloseOut_FinalSiteApproval, app.final_site_apprvl_date);

                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "final_site_apprvl_date_status_id", userRole, application_field_access))
                  {
                     this.ddlCloseOut_FinalSiteApproval.Enabled = false;
                     this.txtCloseOut_FinalSiteApproval.Enabled = false;
                  }
               }
               else
               {
                  liCloseOut_FinalSiteApproval.Style.Add("display", "none");
               }

               if (Security.IsFieldViewable(viewFields, "lease_applications", "as_built_date"))
               {
                  Utility.ControlValueSetter(this.txtCloseOut_AsBuilt, app.as_built_date);

                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "as_built_date", userRole, application_field_access))
                  {
                     this.txtCloseOut_AsBuilt.Enabled = false;
                  }
               }
               else
               {
                  liCloseOut_AsBuilt.Style.Add("display", "none");
               }
            }
            else
            {
               close_out_wrapper.Style.Add("display", "none");
            }

            //Market Handoff
            if (Security.IsDisplayGroupVisible("Market Handoff", category, metaDataList, viewFields))
            {
               if (Security.IsFieldViewable(viewFields, "lease_applications", "market_handoff_date"))
               {
                  Utility.ControlValueSetter(this.txtMarketHandoff_Date, app.market_handoff_date);

                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "market_handoff_date", userRole, application_field_access))
                  {
                     this.txtMarketHandoff_Date.Enabled = false;
                  }
               }
               else
               {
                  this.lblMarketHandoff_Status.Visible = false;
                  this.txtMarketHandoff_Date.Visible = false;
               }

               if (Security.IsFieldViewable(viewFields, "lease_applications", "market_handoff_date_status_id"))
               {
                  Utility.ControlValueSetter(this.ddlMarketHandoff_Status, app.market_handoff_date_status_id);

                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "market_handoff_date_status_id", userRole, application_field_access))
                  {
                     this.ddlMarketHandoff_Status.Enabled = false;
                  }
               }
               else
               {
                  this.lblMarketHandoff_Status.Visible = false;
                  this.ddlMarketHandoff_Status.Visible = false;
               }
            }
            else
            {
               market_handoff_wrapper.Style.Add("display", "none");
            }
         }
      }

      protected void LeaseBinding()
      {
         lease_applications app = this.CurrentLeaseApplication;

         if (app != null)
         {
            CPTTEntities ce = new CPTTEntities();

            if (Security.IsDisplayGroupVisible("Executables Sent To Customer", category, metaDataList, viewFields) ||
                Security.IsDisplayGroupVisible("LOE Holdup Reason Types", category, metaDataList, viewFields) ||
                Security.IsDisplayGroupVisible("Lease Back From Customer", category, metaDataList, viewFields) ||
                Security.IsDisplayGroupVisible("Lease Execution", category, metaDataList, viewFields) ||
                Security.IsDisplayGroupVisible("Amendment Execution", category, metaDataList, viewFields) ||
                Security.IsDisplayGroupVisible("Financial", category, metaDataList, viewFields))
            {

                //Get associate data from Lease Admin
                List<lease_admin_records> list_assoc_app = ce.lease_admin_records.Where(x => x.lease_application_id == app.id).ToList<lease_admin_records>();
                lease_admin_records assoc_app = null;
                if (list_assoc_app.Count() > 0)
                {
                    assoc_app = list_assoc_app[0];
                    this.hidden_lease_admin_id.Value = assoc_app.id.ToString();
                }

                // Attach application to entity context for foreign key lookups.               
                ce.Attach(app);

                //binding assciate data
                if (assoc_app != null)
                {
                    ce.Attach(assoc_app);
                    //disable control
                    //new Execution Document Date
                    Utility.ControlValueSetter(ddlExecDocs_DocType, assoc_app.lease_admin_execution_document_type_id);
                    this.txtExecDocs_RecvedatFSCDate.Text = Utility.DateToString(assoc_app.date_received_at_fsc);
                    this.txtExecDocs_CarrierSignature.Text = Utility.DateToString(assoc_app.carrier_signature_notarized_date);
                    Utility.ControlValueSetter(this.ddlExecDocs_CarrierSignature, assoc_app.carrier_signature_notarized_yn);
                    this.txtExecDocs_SendSignator.Text = Utility.DateToString(assoc_app.sent_to_signatory_date);
                    this.txtExecDocs_BackSignator.Text = Utility.DateToString(assoc_app.back_from_signatory_date);
                    this.txtExecDocs_OrgSentUPS.Text = Utility.DateToString(assoc_app.original_sent_to_ups_date);
                    Utility.ControlValueSetter(ddlExecDocs_DocLoadCST, assoc_app.doc_loaded_to_cst_yn);

                    //new Execution Financial
                    Utility.ControlValueSetter(this.ddlFinanace_LoadCSTRecv, assoc_app.revshare_loaded_to_cst_receivable_yn);
                    Utility.ControlValueSetter(this.ddlFinanace_LoadCSTPay, assoc_app.revshare_loaded_to_cst_payable_yn);
                    Utility.ControlValueSetter(this.ddlFinance_AmendRentAffect, assoc_app.amendment_rent_affecting_yn);
                    Utility.ControlValueSetter(this.ddlFinance_OnTimeFee, assoc_app.one_time_fee_yn);

                    //new Commencement Document Date
                    this.txtComDocs_NTPtoCust.Text = Utility.DateToString(assoc_app.commencement_ntp_to_customer_date);
                    Utility.ControlValueSetter(this.ddlComDocs_DocType, assoc_app.lease_admin_commencement_document_type_id);
                    this.txtComDocs_CommencementDate.Text = Utility.DateToString(assoc_app.commencement_date);
                    this.txtComDocs_RecvFSCDate.Text = Utility.DateToString(assoc_app.date_received_at_fsc_date);
                    Utility.ControlValueSetter(this.ddlComDocs_RecvBy, assoc_app.document_received_by_emp_id);
                    this.txtComDocs_RegCommencementLetterDate.Text = Utility.DateToString(assoc_app.reg_commencement_letter_created_date);
                    this.txtComDocs_RemAlert.Text = Utility.PrepareString(assoc_app.rem_alert_comments);
                    BindLeaseComment(assoc_app.id, ce);

                    if (!ddlExecDocs_DocType.Enabled)
                    {
                        //new Execution Document Date
                        ddlExecDocs_DocType.Enabled = true;
                        //this.txtExecDocs_RecvedatFSCDate.Enabled = false;
                        txtExecDocs_CarrierSignature.ReadOnly = true;
                        txtExecDocs_SendSignator.Enabled = true;
                        txtExecDocs_BackSignator.Enabled = true;
                        txtExecDocs_OrgSentUPS.Enabled = true;
                        ddlExecDocs_DocLoadCST.Enabled = true;

                        //new Execution Financial
                        this.ddlFinanace_LoadCSTRecv.Enabled = true;
                        this.ddlFinanace_LoadCSTPay.Enabled = true;
                        ddlFinance_AmendRentAffect.Enabled = true;
                        ddlFinance_OnTimeFee.Enabled = true;

                        //new Commencement Document Date
                        txtComDocs_NTPtoCust.Enabled = true;
                        ddlComDocs_DocType.Enabled = true;
                        txtComDocs_CommencementDate.Enabled = true;
                        txtComDocs_RecvFSCDate.Enabled = true;
                        ddlComDocs_RecvBy.Enabled = true;
                        txtComDocs_RegCommencementLetterDate.Enabled = true;
                        txtComDocs_RemAlert.Enabled = true;
                    }
                }
                else
                {
                    //disable control
                    //new Execution Document Date
                    ddlExecDocs_DocType.Enabled = false;
                    //this.txtExecDocs_RecvedatFSCDate.Enabled = false;
                    txtExecDocs_CarrierSignature.ReadOnly = false;
                    txtExecDocs_SendSignator.Enabled = false;
                    txtExecDocs_BackSignator.Enabled = false;
                    txtExecDocs_OrgSentUPS.Enabled = false;
                    ddlExecDocs_DocLoadCST.Enabled = false;

                    //new Execution Financial
                    this.ddlFinanace_LoadCSTRecv.Enabled = false;
                    this.ddlFinanace_LoadCSTPay.Enabled = false;
                    ddlFinance_AmendRentAffect.Enabled = false;
                    ddlFinance_OnTimeFee.Enabled = false;

                    //new Commencement Document Date
                    txtComDocs_NTPtoCust.Enabled = false;
                    ddlComDocs_DocType.Enabled = false;
                    txtComDocs_CommencementDate.Enabled = false;
                    txtComDocs_RecvFSCDate.Enabled = false;
                    ddlComDocs_RecvBy.Enabled = false;
                    txtComDocs_RegCommencementLetterDate.Enabled = false;
                    txtComDocs_RemAlert.Enabled = false;
                }
               //Executables Sent to Customer
                if (Security.IsDisplayGroupVisible("Executables Sent to Customer", category, metaDataList, viewFields))
               {
                  // Lease docs sent to customer status and date function together.
                  if (Security.IsFieldViewable(viewFields, "lease_applications", "leasedoc_sent_to_cust_date_status_id"))
                  {
                      //Check if associate with Lease Admin get from field exec_leasedoc_sent_to_cust_date else get field leasedoc_sent_to_cust_date                     
                      Utility.ControlValueSetter(this.ddlExecutablesSenttoCustomer, app.leasedoc_sent_to_cust_date_status_id);
                      Utility.ControlValueSetter(this.txtExecutablesSenttoCustomer, app.leasedoc_sent_to_cust_date);
                      if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "leasedoc_sent_to_cust_date_status_id", userRole, application_field_access))
                      {
                          this.ddlExecutablesSenttoCustomer.Enabled = false;
                          this.txtExecutablesSenttoCustomer.Enabled = false;
                      }
                  }
                  else
                  {
                     this.divExecutablesSenttoCustomer.Visible = false;
                  }

                  // Executables sent to crown status and date function together.
                  if (Security.IsFieldViewable(viewFields, "lease_applications", "executables_to_crown_date_status_id"))
                  {
                     Utility.ControlValueSetter(this.ddlExecutablesToCrown, app.executables_to_crown_date_status_id);
                     Utility.ControlValueSetter(this.txtExecutablesToCrown, app.executables_to_crown_date);

                     if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "executables_to_crown_date", userRole, application_field_access))
                     {
                        this.ddlExecutablesToCrown.Enabled = false;
                        this.txtExecutablesToCrown.Enabled = false;
                     }
                  }
                  else
                  {
                      this.divExecutablesToCrown.Visible = false;
                  }
               }
               else
               {
                  executables_sent_to_customer_wrapper.Style.Add("display", "none");
               }

               // LOE Holdup Reason Types                          
                if (Security.IsDisplayGroupVisible("LOE Holdup Reason Types", category, metaDataList, viewFields))
               {
                  List<loe_holdup_reasons> holdupReasons = app.loe_holdup_reasons.ToList();

                  foreach (loe_holdup_reasons currReason in holdupReasons)
                  {
                     lstLOEHoldup.Items.FindByValue(currReason.loe_holdup_reason_type_id.ToString()).Selected = true;
                  }

                  if (Security.IsFieldViewable(viewFields, "loe_holdup_reasons", "loe_holdup_reason_type_id"))
                  {
                      if (!Security.UserCanEditField(User.Identity.Name, "loe_holdup_reasons", "loe_holdup_reason_type_id", userRole, application_field_access))
                     {
                        this.lstLOEHoldup.Enabled = false;
                     }
                  }
                  else
                  {

                     this.lstLOEHoldup.Visible = false;
                  }

                  if (Security.IsFieldViewable(viewFields, "lease_applications", "loe_holdup_comments"))
                  {
                     Utility.ControlValueSetter(this.txtLOEHoldupReasonTypes_Comments, app.loe_holdup_comments);

                     if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "loe_holdup_comments", userRole, application_field_access))
                     {
                        this.txtLOEHoldupReasonTypes_Comments.Enabled = false;
                     }
                  }
                  else
                  {
                     this.txtLOEHoldupReasonTypes_Comments.Visible = false;
                  }
               }
               else
               {
                  loe_holdup_reason_types_wrapper.Style.Add("display", "none");
               }

                if (Security.IsDisplayGroupVisible("Lease Back From Customer", category, metaDataList, viewFields))
               {
                  if (Security.IsFieldViewable(viewFields, "lease_applications", "signed_by_cust_date"))
                  {
                     Utility.ControlValueSetter(this.txtLOEHoldupReasonTypes_SignedbyCustomer, app.signed_by_cust_date);

                     if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "signed_by_cust_date", userRole, application_field_access))
                     {
                        this.txtLOEHoldupReasonTypes_SignedbyCustomer.Enabled = false;
                     }
                  }
                  else
                  {
                     this.lblLOEHoldupReasonTypes_SignedbyCustomer.Visible = false;
                     this.txtLOEHoldupReasonTypes_SignedbyCustomer.Visible = false;
                  }

                  // Lease back from customer status and date function together.
                  if (Security.IsFieldViewable(viewFields, "lease_applications", "lease_back_from_cust_date_status_id"))
                  {
                     Utility.ControlValueSetter(this.ddlLOEHoldupReasonTypes_DocsBackfromCustomer, app.lease_back_from_cust_date_status_id);
                     Utility.ControlValueSetter(this.txtLOEHoldupReasonTypes_DocsBackfromCustomer, app.lease_back_from_cust_date);

                     if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "lease_back_from_cust_date_status_id", userRole, application_field_access))
                     {
                        this.ddlLOEHoldupReasonTypes_DocsBackfromCustomer.Enabled = false;
                        this.txtLOEHoldupReasonTypes_DocsBackfromCustomer.Enabled = false;
                     }
                  }
                  else
                  {
                      this.divLOEHoldupReasonTypes_DocsBackfromCustomer.Visible = false;
                  }
               }
               else
               {
                  docs_back_from_customer_wrapper.Style.Add("display", "none");
               }

                if (Security.IsDisplayGroupVisible("Lease Execution", category, metaDataList, viewFields))
               {
                  if (Security.IsFieldViewable(viewFields, "lease_applications", "leasedoc_to_fsc_date"))
                  {
                     Utility.ControlValueSetter(this.txtLOEHoldupReasonTypes_DocstoFSC, app.leasedoc_to_fsc_date);

                     if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "leasedoc_to_fsc_date", userRole, application_field_access))
                     {
                        this.txtLOEHoldupReasonTypes_DocstoFSC.Enabled = false;
                     }
                  }
                  else
                  {
                     this.lblLOEHoldupReasonTypes_DocstoFSC.Visible = false;
                     this.txtLOEHoldupReasonTypes_DocstoFSC.Visible = false;
                  }

                  // Lease Execution status and date function together.
                  if (Security.IsFieldViewable(viewFields, "lease_applications", "lease_execution_date_status_id"))
                  {
                     Utility.ControlValueSetter(this.ddlLOEHoldupReasonTypes_LeaseExecution, app.lease_execution_date_status_id);
                     Utility.ControlValueSetter(this.txtLOEHoldupReasonTypes_LeaseExecution, app.lease_execution_date);

                     if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "lease_execution_date_status_id", userRole, application_field_access))
                     {
                        this.ddlLOEHoldupReasonTypes_LeaseExecution.Enabled = false;
                        this.txtLOEHoldupReasonTypes_LeaseExecution.Enabled = false;
                     }
                  }
                  else
                  {
                      this.divLOEHoldupReasonTypes_LeaseExecution.Visible = false;
                  }
               }
               else
               {
                  lease_execution_wrapper.Style.Add("display", "none");
               }


                if (Security.IsDisplayGroupVisible("Amendment Execution", category, metaDataList, viewFields))
               {
                  // Amendment execution status and date function together.   
                  if (Security.IsFieldViewable(viewFields, "lease_applications", "amendment_execution_date_status_id"))
                  {
                     Utility.ControlValueSetter(this.ddlLOEHoldupReasonTypes_AmendmentExecution, app.amendment_execution_date_status_id);
                     Utility.ControlValueSetter(this.txtLOEHoldupReasonTypes_AmendmentExecution, app.amendment_execution_date);

                     if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "amendment_execution_date_status_id", userRole, application_field_access))
                     {
                        this.ddlLOEHoldupReasonTypes_AmendmentExecution.Enabled = false;
                        this.txtLOEHoldupReasonTypes_AmendmentExecution.Enabled = false;
                     }
                  }
                  else
                  {
                     this.lblLOEHoldupReasonTypes_AmendmentExecution.Visible = false;
                     this.ddlLOEHoldupReasonTypes_AmendmentExecution.Visible = false;
                     this.txtLOEHoldupReasonTypes_AmendmentExecution.Visible = false;
                  }
               }
               else
               {
                  amendment_execution_wrapper.Style.Add("display", "none");
               }

                if (Security.IsDisplayGroupVisible("Financial", category, metaDataList, viewFields))
               {
                  //Financial
                  if (Security.IsFieldViewable(viewFields, "lease_applications", "rent_forecast_amount"))
                  {
                     Utility.ControlValueSetter(this.txtFinancial_RentForecastAmount, app.rent_forecast_amount);

                     if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "rent_forecast_amount", userRole, application_field_access))
                     {
                        this.txtFinancial_RentForecastAmount.Enabled = false;
                     }
                  }
                  else
                  {
                     this.lblFinancial_RentForecastAmount.Visible = false;
                     this.txtFinancial_RentForecastAmount.Visible = false;
                  }

                  if (Security.IsFieldViewable(viewFields, "lease_applications", "revenue_share_yn"))
                  {
                     Utility.ControlValueSetter(this.ddlFinancial_RevenueShare, app.revenue_share_yn);

                     if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "revenue_share_yn", userRole, application_field_access))
                     {
                        this.ddlFinancial_RevenueShare.Enabled = false;
                     }
                  }
                  else
                  {
                     this.lblFinancial_RevenueShare.Visible = false;
                     this.ddlFinancial_RevenueShare.Visible = false;
                  }

                  if (Security.IsFieldViewable(viewFields, "lease_applications", "commencement_forcast_date"))
                  {
                     Utility.ControlValueSetter(this.txtFinancial_CommencementForecast, app.commencement_forcast_date);

                     if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "commencement_forcast_date", userRole, application_field_access))
                     {
                        this.txtFinancial_CommencementForecast.Enabled = false;
                     }
                  }
                  else
                  {
                     this.lblFinancial_RevenueShare.Visible = false;
                     this.ddlFinancial_RevenueShare.Visible = false;
                  }

                  if (Security.IsFieldViewable(viewFields, "lease_applications", "rem_lease_seq"))
                  {
                     Utility.ControlValueSetter(this.txtFinancial_REMLeaseSequence, app.rem_lease_seq);

                     if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "rem_lease_seq", userRole, application_field_access))
                     {
                        this.txtFinancial_REMLeaseSequence.Enabled = false;
                     }
                  }
                  else
                  {
                     this.lblFinancial_REMLeaseSequence.Visible = false;
                     this.txtFinancial_REMLeaseSequence.Visible = false;
                  }

                  if (Security.IsFieldViewable(viewFields, "lease_applications", "cost_share_agreement_amount"))
                  {
                     Utility.ControlValueSetter(this.txtFinancial_CostShareAgreementAmount, app.cost_share_agreement_amount);

                     if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "cost_share_agreement_amount", userRole, application_field_access))
                     {
                        this.txtFinancial_CostShareAgreementAmount.Enabled = false;
                     }
                  }
                  else
                  {
                     this.lblFinancial_CostShareAgreement.Visible = false;
                     this.txtFinancial_CostShareAgreementAmount.Visible = false;
                  }

                  if (Security.IsFieldViewable(viewFields, "lease_applications", "cost_share_agreement_yn"))
                  {
                     Utility.ControlValueSetter(this.ddlFinancial_CostShareAgreement, app.cost_share_agreement_yn);

                     if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "cost_share_agreement_yn", userRole, application_field_access))
                     {
                        this.ddlFinancial_CostShareAgreement.Enabled = false;
                     }
                  }
                  else
                  {
                     this.lblFinancial_CostShareAgreement.Visible = false;
                     this.ddlFinancial_CostShareAgreement.Visible = false;
                  }

                  if (Security.IsFieldViewable(viewFields, "lease_applications", "cap_cost_amount"))
                  {
                     Utility.ControlValueSetter(this.txtFinancial_CapCostRecoveryAmount, app.cap_cost_amount);

                     if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "cap_cost_amount", userRole, application_field_access))
                     {
                        this.txtFinancial_CapCostRecoveryAmount.Enabled = false;
                     }
                  }
                  else
                  {
                     this.lblFinancial_CapCostRecovery.Visible = false;
                     this.txtFinancial_CapCostRecoveryAmount.Visible = false;
                  }

                  if (Security.IsFieldViewable(viewFields, "lease_applications", "cap_cost_recovery_yn"))
                  {
                     Utility.ControlValueSetter(this.ddlFinancial_CapCostRecovery, app.cap_cost_recovery_yn);

                     if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "cap_cost_recovery_yn", userRole, application_field_access))
                     {
                        this.ddlFinancial_CapCostRecovery.Enabled = false;
                     }
                  }
                  else
                  {
                     this.ddlFinancial_CapCostRecovery.Visible = false;
                     this.ddlFinancial_CapCostRecovery.Visible = false;
                  }

                  if (Security.IsFieldViewable(viewFields, "lease_applications", "bulk_po_availible_yn"))
                  {
                     Utility.ControlValueSetter(this.ddlFinancial_BulkPOAvailable, app.bulk_po_availible_yn);

                     if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "bulk_po_availible_yn", userRole, application_field_access))
                     {
                        this.ddlFinancial_BulkPOAvailable.Enabled = false;
                     }
                  }
                  else
                  {
                     this.lblFinancial_BulkPOAvailable.Visible = false;
                     this.ddlFinancial_BulkPOAvailable.Visible = false;
                  }
               }
               else
               {
                  financial_wrapper.Style.Add("display", "none");
                   // Update For issue 430 
                  Financial_CommencementForecast_wrapper.Style.Add("display", "none");
                  left_col_wrapper.Style.Add("display", "none");
                   // ==>
               }

               if (Security.IsFieldViewable(viewFields, "lease_applications", "termination_date"))
               {
                  Utility.ControlValueSetter(this.txtLOEHoldupReasonTypes_TerminationDate, app.termination_date);

                  if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "termination_date", userRole, application_field_access))
                  {
                     this.txtLOEHoldupReasonTypes_TerminationDate.Enabled = false;
                  }
               }
               else
               {
                  this.lblLOEHoldupReasonTypes_TerminationDate.Visible = false;
                  this.txtLOEHoldupReasonTypes_TerminationDate.Visible = false;
               }
            }
            else
            {
               liLease.Visible = false;
               lease.Style.Add("display", "none");
            }

            // Detach application from entity context for future updates.               
            ce.Detach(app);
         }
      }

      private void BindLeaseComment(int iLeaseAdminID,CPTTEntities ceRef)
      {
        List<lease_admin_comments> lstComment = LeaseAdminComments.GetComments(iLeaseAdminID, ceRef).OrderBy(x => x.created_at).ToList();
        List<LeaseAdminCommentData> result = new List<LeaseAdminCommentData>();
        foreach (lease_admin_comments comm in lstComment)
        {
            LeaseAdminCommentData data = new LeaseAdminCommentData();
            data.comment_id = comm.id;
            data.comment = comm.comments;
            data.created_at = Utility.DateToString(comm.created_at);
            data.update_at = Utility.DateToString(comm.updated_at);
            data.creator_id = comm.creator_id;
            data.updater = comm.creator_user.last_name + " " + comm.creator_user.first_name;
            result.Add(data);
        }
        this.hidden_lease_comments.Value = Newtonsoft.Json.JsonConvert.SerializeObject(result);
      }

      protected void AllCommentBinding(Boolean isGetAllReleate = false)
      {
         lease_applications app = this.CurrentLeaseApplication;
         if (app != null)
         {
            divComment1.Visible = false;
            divComment2.Visible = false;
            divComment3.Visible = false;
            divComment4.Visible = false;
            btnAllComment.Visible = false;
            List<AllComments> lstComment = AllCommentsHelper.GetAllCommentLeaseApp(app.id,null, false, true);
            AllComments currentComment;

            //Control paramter use before finding the way to iterate web UI control.
            System.Web.UI.HtmlControls.HtmlGenericControl pCommenter;
            System.Web.UI.HtmlControls.HtmlGenericControl divComment;
            System.Web.UI.HtmlControls.HtmlGenericControl pComment;
            System.Web.UI.HtmlControls.HtmlGenericControl pDocument;
            Image imgComment;
            ImageButton btnDeleteComment;
            List<System.Web.UI.HtmlControls.HtmlGenericControl> lstPCommenter = new List<System.Web.UI.HtmlControls.HtmlGenericControl>();
            List<System.Web.UI.HtmlControls.HtmlGenericControl> lstPComments = new List<System.Web.UI.HtmlControls.HtmlGenericControl>();
            List<System.Web.UI.HtmlControls.HtmlGenericControl> lstDivComment = new List<System.Web.UI.HtmlControls.HtmlGenericControl>();
            List<System.Web.UI.HtmlControls.HtmlGenericControl> lstDivDocument = new List<System.Web.UI.HtmlControls.HtmlGenericControl>();
            List<ImageButton> lstBtnDelete = new List<ImageButton>();
            List<Image> lstImageAvatar = new List<Image>();
            lstPCommenter.Add(pCommenter1);
            lstPCommenter.Add(pCommenter2);
            lstPCommenter.Add(pCommenter3);
            lstPCommenter.Add(pCommenter4);
            lstPComments.Add(pComment1);
            lstPComments.Add(pComment2);
            lstPComments.Add(pComment3);
            lstPComments.Add(pComment4);
            lstDivComment.Add(divComment1);
            lstDivComment.Add(divComment2);
            lstDivComment.Add(divComment3);
            lstDivComment.Add(divComment4);
            lstBtnDelete.Add(btnDeleteComment1);
            lstBtnDelete.Add(btnDeleteComment2);
            lstBtnDelete.Add(btnDeleteComment3);
            lstBtnDelete.Add(btnDeleteComment4);
            lstImageAvatar.Add(imgUserComment1);
            lstImageAvatar.Add(imgUserComment2);
            lstImageAvatar.Add(imgUserComment3);
            lstImageAvatar.Add(imgUserComment4);
            lstDivDocument.Add(pnlDocument1);
            lstDivDocument.Add(pnlDocument2);
            lstDivDocument.Add(pnlDocument3);
            lstDivDocument.Add(pnlDocument4);

            hidden_count_main_comment.Value = lstComment.Count.ToString();

            if (lstComment.Count > 0)
            {
                btnAllComment.Visible = true;
                btnAllComment.InnerText = String.Format(CultureInfo.CurrentCulture, "see all {0} comments", lstComment.Count);
            }
            btnAllRelateComment.Visible = true;

            if (!isGetAllReleate)
            {
                var focusComment = from listComment in lstComment
                                   where listComment.Source.Equals(AllComments.LEASE_APPLICATION)
                                   || listComment.Source.Equals(AllComments.LEASE_ADMIN)
                                   || listComment.Source.Equals(AllComments.STRUCTURAL_ANALYSIS)
                                   select listComment;
                lstComment = focusComment.AsQueryable().OrderByDescending(a => a.CreateAt).ToList();
            }

            string commenter;
            string sCommenterFormat = "{0}, {1}";//name , date
            string sImageUrl = "";

            //Add link document for all comments
            GetAllCommentLinkDocument(lstComment);

            // get user role
            lstComment = lstComment.Take(4).ToList();

            for (int i = 0; i < lstComment.Count && i < 4; i++)
            {
               currentComment = lstComment[i];
               pCommenter = lstPCommenter[i];
               pComment = lstPComments[i];
               divComment = lstDivComment[i];
               btnDeleteComment = lstBtnDelete[i];
               imgComment = lstImageAvatar[i];
               pDocument = lstDivDocument[i];
               if (!string.IsNullOrEmpty(currentComment.CreatorName))
               {
                  commenter = string.Format(CultureInfo.CurrentCulture, sCommenterFormat, currentComment.CreatorName,
                                            Utility.DateToString(currentComment.CreateAt));
               }
               else
               {
                  commenter = string.Format(CultureInfo.CurrentCulture, sCommenterFormat, "No employee recorded",
                                            Utility.DateToString(currentComment.CreateAt));
               }


               sImageUrl = BLL.User.GetUserAvatarImage(currentComment.CreatorUsername);

               if (!Security.UserCanRemoveComment(userRole, (int)Session["userId"], currentComment.CommentID, currentComment.Source, lstComment))
               {
                   btnDeleteComment.Visible = false;
               }

               imgComment.ImageUrl = sImageUrl;
               pCommenter.InnerText = commenter;
               divComment.Visible = true;
               pComment.InnerHtml = currentComment.CheckSA;

               if (currentComment.Mode == "eLA" && currentComment.LeaseApplicationID == app.id)
               {
                   pComment.InnerHtml = currentComment.Comments;
               }
               else
               {
                   pComment.InnerHtml = currentComment.CheckSA;
               }


               btnDeleteComment.AlternateText = currentComment.Source + "/" + currentComment.CommentID.ToString();

               if (currentComment.Mode != "eLE")
               {
                   pDocument.Attributes.Add("class", "document-comment-" + lstComment[i].CommentID);
               }
               else
               {
                   pDocument.Attributes.Remove("class");
               }

               int commentID = lstComment[i].CommentID;

               if (divComment.Attributes["saCommentId"] != null)
               {
                   divComment.Attributes.Remove("saCommentId");
               }

               if (divComment.Attributes["leaseAdminCommentId"] != null)
               {
                   divComment.Attributes.Remove("leaseAdminCommentId");
               }

               if (lstComment[i].Mode == "eSA")
               {
                   divComment.Attributes.Add("saCommentId", "SA_" + commentID);
               }
               else if (lstComment[i].Mode == "eLE")
               {
                   divComment.Attributes.Add("leaseAdminCommentId", "LeaseAdmin_" + commentID);
               }
               else if (lstComment[i].Mode == "eTM")
               {
                   divComment.Attributes.Add("towerModCommentId", "TowerMod_" + commentID);
               }
               else if (lstComment[i].Mode == "eSDP")
               {
                   divComment.Attributes.Add("sdpCommentId", "SDP_" + commentID);
               }
            }
         }
      }

      protected void RemoveAnotherCommentDialog(int id, string type)
      {
          switch (type)
          {
              case AllComments.STRUCTURAL_ANALYSIS:
                  hidden_deleted_comment_id.Value = "sa|"+id.ToString();
                  break;
              case AllComments.LEASE_ADMIN:
                  hidden_deleted_comment_id.Value = "admin|"+id.ToString();
                  break;
              case AllComments.SDP:
                  hidden_deleted_comment_id.Value = "sdp|" + id.ToString();
                  break;
              case AllComments.TOWER_MOD:
                  hidden_deleted_comment_id.Value = "tm|" + id.ToString();
                  break;
              default:
                  // do the defalut action
                  break;
          }

      }
       
      #endregion

      protected class NTPDateCalculation
      {
         DateTime? m_ntpIssueDate;
         public NTPDateCalculation(DateTime? ntpIssueDate)
         {
            m_ntpIssueDate = ntpIssueDate;
         }

         public DateTime? AllCloseOutDocsDueDate(DateTime? extRcvdDate)
         {
            DateTime? ret = null;
            if (extRcvdDate.HasValue)
            {
               ret = PostNTPnDay(255);
            }
            else
            {
               ret = PostNTPnDay(225);
            }
            return ret;
         }

         public DateTime? PostNTPnDay(int days)
         {
            DateTime? ret = null;
            if (m_ntpIssueDate.HasValue)
            {
               ret = m_ntpIssueDate.Value.AddDays(days);
            }
            return ret;
         }
      }

      protected class MajorMileStone
      {
         #region Declarations
         private double m_dblPercent;

         private double m_dblMax;
         public double ProgMaximum
         { get; set; }

         public double ProgCurrent
         {
            get
            {
               return m_dblPercent * m_dblMax;
            }
         }
         private MileStoneStatus m_eStatus;
         public String MileStone
         {
            get
            {
               String sResult = "";
               switch (m_eStatus)
               {
                  case MileStoneStatus.None:
                     break;
                  case MileStoneStatus.PrelimApproval:
                     sResult = "Preliminary Approval";
                     break;
                  case MileStoneStatus.PrelimeSitewalk:
                     sResult = "Preliminary Sitewalk";
                     break;
                  case MileStoneStatus.SAOrderd:
                     sResult = "SA Ordered";
                     break;
                  case MileStoneStatus.SAApproved:
                     sResult = "SA Approved";
                     break;
                  case MileStoneStatus.LEApproved:
                     sResult = "LE Approved";
                     break;
                  case MileStoneStatus.SAandLEApproved:
                     sResult = "SA and LE Approved";
                     break;
                  case MileStoneStatus.SentForExecution:
                     sResult = "Sent for Execution";
                     break;
                  case MileStoneStatus.LeaseDocs:
                     sResult = "Lease Docs";
                     break;
                  case MileStoneStatus.NTPIssued:
                     sResult = "NTP Issued";
                     break;
                  case MileStoneStatus.FinalSiteApproval:
                     sResult = "Final Site Approval";
                     break;
                  default:
                     break;
               }
               return sResult;
            }
         }
         #endregion

         public enum MileStoneStatus
         {
            None,
            PrelimApproval,
            PrelimeSitewalk,
            SAOrderd,
            SAApproved,
            LEApproved,
            SAandLEApproved,
            SentForExecution,
            LeaseDocs,
            NTPIssued,
            FinalSiteApproval
         }

         public MajorMileStone(lease_applications objLA)
         {
            m_eStatus = MileStoneStatus.None;
            m_dblPercent = 0;
            m_dblMax = 100; //default value for maximum.
            CalculateMileStone(objLA);
         }

         private void CalculateMileStone(lease_applications objLA)
         {
            using (CPTTEntities ce = new CPTTEntities())
            {
               // Test Preliminary Approval Milestone
               if (objLA.prelim_approval_date.HasValue)
               {
                  m_eStatus = MileStoneStatus.PrelimApproval;
                  m_dblPercent = 0.11;

                  // Test Preliminary Sitewalk Milestone
                  if (
                      objLA.prelim_sitewalk_date.HasValue &&
                      objLA.prelim_sitewalk_date_status_id.HasValue &&
                      objLA.prelim_sitewalk_date_status_id.Value == 1
                     )
                  {
                     m_eStatus = MileStoneStatus.PrelimeSitewalk;
                     m_dblPercent = 0.22;

                     CPTTEntities context = new CPTTEntities();
                     var querySA = from sa in context.structural_analyses
                                   where sa.lease_application_id == objLA.id
                                   select sa;

                     // Retrieve Structural Analysis Record(s) for Lease Application.
                     if (querySA.ToList().Count > 0 && querySA.ToList()[0].sa_ordered_date.HasValue &&
                         querySA.ToList()[0].sa_ordered_date_status_id.HasValue &&
                         (int)querySA.ToList()[0].sa_ordered_date_status_id.Value == 1)
                     {
                        m_eStatus = MileStoneStatus.SAOrderd;
                        m_dblPercent = 0.33;

                        //  SA or LE Approval Milestone
                        if (objLA.struct_decision_date.HasValue || objLA.leaseexhbt_decision_date.HasValue) //TODO: Change this into real checking after got the source for sa_approval_date
                        {
                           bool blnSAorLE = false;
                           if (objLA.struct_decision_date.HasValue && (!objLA.leaseexhbt_decision_date.HasValue))
                           {
                              if (objLA.struct_decision_id != 2) // Approved, Approved with mods, or N/A
                              {
                                 m_eStatus = MileStoneStatus.SAApproved;
                                 m_dblPercent = 0.44;
                                 blnSAorLE = true;
                              }
                           }
                           else if (objLA.leaseexhbt_decision_date.HasValue)
                           {
                              if (objLA.leaseexhbt_decision_id != 3) // Approved, Denied
                              {
                                 m_eStatus = MileStoneStatus.LEApproved;
                                 m_dblPercent = 0.44;
                                 blnSAorLE = true;
                              }
                           }

                           // Test SA and LE Approval Milestone
                           if (
                               blnSAorLE &&
                               objLA.struct_decision_date.HasValue &&
                               objLA.leaseexhbt_decision_date.HasValue
                               && objLA.struct_decision_id != 2
                               && objLA.leaseexhbt_decision_id != 3
                              )
                           {
                              m_eStatus = MileStoneStatus.SAandLEApproved;
                              m_dblPercent = 0.55;

                              // Sent for Execution Milestone
                              if (objLA.leasedoc_sent_to_cust_date.HasValue && objLA.leasedoc_sent_to_cust_date_status_id.HasValue
                              && (int)objLA.leasedoc_sent_to_cust_date_status_id.Value == 1)
                              {
                                 m_eStatus = MileStoneStatus.SentForExecution;
                                 m_dblPercent = 0.66;

                                 //  Lease Docs Milestone
                                 if ((objLA.lease_execution_date.HasValue && objLA.lease_execution_date_status_id.HasValue &&
                                     (int)objLA.lease_execution_date_status_id.Value == 1) ||
                                     (objLA.amendment_execution_date.HasValue && objLA.amendment_execution_date_status_id.HasValue &&
                                     (int)objLA.amendment_execution_date_status_id.Value == 1))
                                 {
                                    m_eStatus = MileStoneStatus.LeaseDocs;
                                    m_dblPercent = 0.77;

                                    //  NTP Milestone
                                    if (objLA.ntp_issued_date.HasValue && objLA.ntp_issued_date_status_id.HasValue &&
                                        (int)objLA.ntp_issued_date_status_id.Value == 1)
                                    {
                                       m_eStatus = MileStoneStatus.NTPIssued;
                                       m_dblPercent = 0.88;

                                       //  Final Site Approval Milestone
                                       if (objLA.final_site_apprvl_date.HasValue && objLA.final_site_apprvl_date_status_id.HasValue &&
                                           (int)objLA.final_site_apprvl_date_status_id.Value == 1)
                                       {
                                          m_eStatus = MileStoneStatus.FinalSiteApproval;
                                          m_dblPercent = 1.0;
                                       }
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }

      protected void btnLADelete_Click(object sender, EventArgs e)
      {
          String sError;
          lease_applications app = CurrentLeaseApplication;
          if (app != null)
          {
              sError = LeaseApplication.Delete(app.id);
              if (sError == null)
              {
                 Master.StatusBox.showStatusMessageWithRedirect("Record deleted.", "Default.aspx"); 
              }
              else
              {
                  //Error occur.
                  Master.StatusBox.showStatusMessage("Error: some related records cannot be deleted; operation aborted.");
              }
          }
      }

      protected void btnSADelete_Click(object sender, EventArgs e)
      {
          String sError;
          int iSAId;
          //Checking for deletable page
          if (hidden_SA_ID.Value != SA_DEFAULT_ID)
          {
              if (int.TryParse(hidden_SA_ID.Value, out iSAId))
              {
                  sError = StructuralAnalysis.Delete(iSAId);
                  if (sError == null)
                  {
                      //Successful delete.
                      BindApplication(CurrentLeaseApplication.id);
                      MajorMileStoneBinding();
                      this.CurrentTab = eTabName.SA;
                      Master.StatusBox.showStatusMessage("Structural Analysis record removed.");
                  }
                  else
                  {
                      //Error occur.
                      Master.StatusBox.showStatusMessage("Error: some related records cannot be deleted; operation aborted.");
                      this.CurrentTab = eTabName.SA;
                  }
              }
          }
      }

      protected void btnRVDelete_Click(object sender, EventArgs e)
      {
        String sError;
          int iRVId;
          //Checking for deletable page
          if (hidden_revision_id.Value != SA_DEFAULT_ID)
          {
              if (int.TryParse(hidden_revision_id.Value, out iRVId))
              {
                  sError = LeaseAppRevisions.Delete(iRVId);
                  if (sError == null)
                  {
                      //Successful delete.
                      BindApplication(CurrentLeaseApplication.id);
                      MajorMileStoneBinding();
                      this.CurrentTab = eTabName.RV;
                      Master.StatusBox.showStatusMessage("Revision Removed.");
                  }
                  else
                  {
                      //Error occur.
                      Master.StatusBox.showStatusMessage("Error: some related records cannot be deleted; operation aborted.");
                  }
              }
          }
      }

      #region Link Document
      private void GetAllCommentLinkDocument(List<AllComments> lstComment)
      {
          this.hidden_document_all_document_comments.Value = String.Empty;
          List<List<Dms_Document>> allComments = new List<List<Dms_Document>>();
          List<AllComments> allcomments = lstComment;
          List<Dms_Document> all_comment_document_list = new List<Dms_Document>();

          // Get all current leaseApp document
          all_comment_document_list = DMSDocumentLinkHelper.GetDocumentLinkList(lstComment);

          foreach (AllComments comment in allcomments)
          {
              DMSDocumentLinkHelper.eDocumentField efield;
              if (comment.Mode == "eSA")
              {
                  efield = DMSDocumentLinkHelper.eDocumentField.SA_Comments;
              }
              else if (comment.Mode == "eLE")
              {
                  //Lease admin set field null because not document link
                  efield = DMSDocumentLinkHelper.eDocumentField.LA_Comments ;
              }
              else if (comment.Mode == "eTM")
              {
                  efield = DMSDocumentLinkHelper.eDocumentField.TMO_Comments;
              }
              else if (comment.Mode == "eSDP")
              {
                  efield = DMSDocumentLinkHelper.eDocumentField.SDP_Comments;
              }
              else
              {
                  efield = DMSDocumentLinkHelper.eDocumentField.LE_Comments;
              }

              if (efield != DMSDocumentLinkHelper.eDocumentField.LA_Comments && efield != DMSDocumentLinkHelper.eDocumentField.SDP_Comments)
              {

                  List<Dms_Document> docData = DMSDocumentLinkHelper.GetDocumentLinkFromList(comment, DMSDocumentLinkHelper.DocumentField(efield.ToString()), all_comment_document_list);

                  if (docData.Count == 0)
                  {
                      Dms_Document doc = new Dms_Document();
                      doc.ID = comment.CommentID;
                      doc.ref_field = efield.ToString(); 
                      docData.Add(doc);
                  }
                  allComments.Add(docData);
              }
          }
          this.hidden_document_all_document_comments.Value = Newtonsoft.Json.JsonConvert.SerializeObject(allComments);
      }

      private void GetLinkDocument()
      {
          lease_applications app = this.CurrentLeaseApplication;

          List<documents_links> releateDoc = DMSDocumentLinkHelper.GetDocumentLinkListLeaseAppBinding();

          #region SA
          if (app != null)
          {
              string json = "";
              int count = 0;

              using (CPTTEntities context = new CPTTEntities())
              {
                  count = StructuralAnalysis.GetJSON(false, out json, app.id);
                  
                  // Clear all hidden sa 
                    hidden_document_SAWToSAC.Value = string.Empty;
                    hidden_document_SA_Received.Value = string.Empty;
                    hidden_document_Structural_Decision.Value = string.Empty;
                    hidden_document_DelayedOrder_OnHold.Value = string.Empty;
                    hidden_document_DelayedOrder_Cancelled.Value = string.Empty;
                    hidden_document_PurchaseOrder_Received.Value = string.Empty;

                  if (json != null || json != "")
                  {
                      //create document label
                      List<StructuralAnalysisTemplate> saTemplate = StructuralAnalysis.ParseFromJSON(json);
                      if (saTemplate.Count > 0)
                      {
                          foreach (StructuralAnalysisTemplate saTemp in saTemplate)
                          {
                              int saID = int.Parse(saTemp.ID);

                              //create document
                              List<Dms_Document> SAWToSACDoc = DMSDocumentLinkHelper.GetDocumentLink(saID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.SAWToSAC.ToString()), string.Empty, null, null, releateDoc);
                              SetLinkDocument(saID, DMSDocumentLinkHelper.eDocumentField.SAWToSAC, SAWToSACDoc);

                              List<Dms_Document> SAWRequest = DMSDocumentLinkHelper.GetDocumentLink(saID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.SAWRequest.ToString()), string.Empty, null, null, releateDoc);
                              SetLinkDocument(saID, DMSDocumentLinkHelper.eDocumentField.SAWRequest, SAWRequest);

                              List<Dms_Document> SA_Received = DMSDocumentLinkHelper.GetDocumentLink(saID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.SA_Received.ToString()), string.Empty, null, null, releateDoc);
                              SetLinkDocument(saID, DMSDocumentLinkHelper.eDocumentField.SA_Received, SA_Received);

                              List<Dms_Document> Structural_Decision = DMSDocumentLinkHelper.GetDocumentLink(saID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.Structural_Decision.ToString()), string.Empty, null, null, releateDoc);
                              SetLinkDocument(saID, DMSDocumentLinkHelper.eDocumentField.Structural_Decision, Structural_Decision);

                              //DelayedOrder
                              List<Dms_Document> DelayedOrder_OnHold = DMSDocumentLinkHelper.GetDocumentLink(saID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.DelayedOrder_OnHold.ToString()), string.Empty, null, null, releateDoc);
                              SetLinkDocument(saID, DMSDocumentLinkHelper.eDocumentField.DelayedOrder_OnHold, DelayedOrder_OnHold);

                              List<Dms_Document> DelayedOrder_Cancelled = DMSDocumentLinkHelper.GetDocumentLink(saID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.DelayedOrder_Cancelled.ToString()), string.Empty, null, null, releateDoc);
                              SetLinkDocument(saID, DMSDocumentLinkHelper.eDocumentField.DelayedOrder_Cancelled, DelayedOrder_Cancelled);

                              //Purchase Order
                              List<Dms_Document> PurchaseOrder_Received = DMSDocumentLinkHelper.GetDocumentLink(saID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.PurchaseOrder_Received.ToString()), string.Empty, null, null, releateDoc);
                              SetLinkDocument(saID, DMSDocumentLinkHelper.eDocumentField.PurchaseOrder_Received, PurchaseOrder_Received);
                          }
                      }
                      //Add condition case no revision file will create json string template
                      else
                      {
                          SetLinkDocument(0, DMSDocumentLinkHelper.eDocumentField.SAWToSAC, new List<Dms_Document>() );
                          SetLinkDocument(0, DMSDocumentLinkHelper.eDocumentField.SAWRequest, new List<Dms_Document>());

                          SetLinkDocument(0, DMSDocumentLinkHelper.eDocumentField.SA_Received, new List<Dms_Document>());

                          SetLinkDocument(0, DMSDocumentLinkHelper.eDocumentField.Structural_Decision, new List<Dms_Document>());
                          //DelayedOrder
                          SetLinkDocument(0, DMSDocumentLinkHelper.eDocumentField.DelayedOrder_OnHold, new List<Dms_Document>());
                          SetLinkDocument(0, DMSDocumentLinkHelper.eDocumentField.DelayedOrder_Cancelled, new List<Dms_Document>());
                          //Purchase Order
                          SetLinkDocument(0, DMSDocumentLinkHelper.eDocumentField.PurchaseOrder_Received, new List<Dms_Document>());
                      }
                  }

              }
          }

          #endregion SA
         
          // PR
          List<Dms_Document> PreliminaryDecision = DMSDocumentLinkHelper.GetDocumentLink(app.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.PreliminaryDecision.ToString()), string.Empty, null, null, releateDoc);
          SetLinkDocument(app.id, DMSDocumentLinkHelper.eDocumentField.PreliminaryDecision, PreliminaryDecision);

          List<Dms_Document> SDPtoCustomer = DMSDocumentLinkHelper.GetDocumentLink(app.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.SDPtoCustomer.ToString()), string.Empty, null, null, releateDoc);
          SetLinkDocument(app.id, DMSDocumentLinkHelper.eDocumentField.SDPtoCustomer, SDPtoCustomer);

          List<Dms_Document> PreliminarySitewalk = DMSDocumentLinkHelper.GetDocumentLink(app.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.PreliminarySitewalk.ToString()), string.Empty, null, null, releateDoc);
          SetLinkDocument(app.id, DMSDocumentLinkHelper.eDocumentField.PreliminarySitewalk, PreliminarySitewalk);

          List<Dms_Document> LandlordConsentType = DMSDocumentLinkHelper.GetDocumentLink(app.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.LandlordConsentType.ToString()), string.Empty, null, null, releateDoc);
          SetLinkDocument(app.id, DMSDocumentLinkHelper.eDocumentField.LandlordConsentType, LandlordConsentType);

          // DR
          List<Dms_Document> LeaseExhibit_Received = DMSDocumentLinkHelper.GetDocumentLink(app.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.LeaseExhibit_Received.ToString()), string.Empty, null, null, releateDoc);
          SetLinkDocument(app.id, DMSDocumentLinkHelper.eDocumentField.LeaseExhibit_Received, LeaseExhibit_Received);

          List<Dms_Document> LeaseExhibit_Decision = DMSDocumentLinkHelper.GetDocumentLink(app.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.LeaseExhibit_Decision.ToString()), string.Empty, null, null, releateDoc);
          SetLinkDocument(app.id, DMSDocumentLinkHelper.eDocumentField.LeaseExhibit_Decision, LeaseExhibit_Decision);

          List<Dms_Document> LandlordConsent_LandlordNotice = DMSDocumentLinkHelper.GetDocumentLink(app.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.LandlordConsent_LandlordNotice.ToString()), string.Empty, null, null, releateDoc);
          SetLinkDocument(app.id, DMSDocumentLinkHelper.eDocumentField.LandlordConsent_LandlordNotice, LandlordConsent_LandlordNotice);

          List<Dms_Document> LandlordConsentRequested = DMSDocumentLinkHelper.GetDocumentLink(app.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.LandlordConsentRequested.ToString()), string.Empty, null, null, releateDoc);
          SetLinkDocument(app.id, DMSDocumentLinkHelper.eDocumentField.LandlordConsentRequested, LandlordConsentRequested);

          List<Dms_Document> LandlordConsentReceived = DMSDocumentLinkHelper.GetDocumentLink(app.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.LandlordConsentReceived.ToString()), string.Empty, null, null, releateDoc);
          SetLinkDocument(app.id, DMSDocumentLinkHelper.eDocumentField.LandlordConsentReceived, LandlordConsentReceived);

          List<Dms_Document> ConstructionDrawingsReceived = DMSDocumentLinkHelper.GetDocumentLink(app.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.ConstructionDrawingsReceived.ToString()), string.Empty, null, null, releateDoc);
          SetLinkDocument(app.id, DMSDocumentLinkHelper.eDocumentField.ConstructionDrawingsReceived, ConstructionDrawingsReceived);

          List<Dms_Document> ConstructionDrawingsDecision = DMSDocumentLinkHelper.GetDocumentLink(app.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.ConstructionDrawingsDecision.ToString()), string.Empty, null, null, releateDoc);
          SetLinkDocument(app.id, DMSDocumentLinkHelper.eDocumentField.ConstructionDrawingsDecision, ConstructionDrawingsDecision);

          // LE
          List<Dms_Document> ExecutablesSenttoCustomer = DMSDocumentLinkHelper.GetDocumentLink(app.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.ExecutablesSenttoCustomer.ToString()), string.Empty, null, null, releateDoc);
          SetLinkDocument(app.id, DMSDocumentLinkHelper.eDocumentField.ExecutablesSenttoCustomer, ExecutablesSenttoCustomer);

          List<Dms_Document> ExecutablesToCrown = DMSDocumentLinkHelper.GetDocumentLink(app.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.ExecutablesToCrown.ToString()), string.Empty, null, null, releateDoc);
          SetLinkDocument(app.id, DMSDocumentLinkHelper.eDocumentField.ExecutablesToCrown, ExecutablesToCrown);

          List<Dms_Document> LOEHoldupReasonTypes_DocsBackfromCustomer = DMSDocumentLinkHelper.GetDocumentLink(app.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.LOEHoldupReasonTypes_DocsBackfromCustomer.ToString()), string.Empty, null, null, releateDoc);
          SetLinkDocument(app.id, DMSDocumentLinkHelper.eDocumentField.LOEHoldupReasonTypes_DocsBackfromCustomer, LOEHoldupReasonTypes_DocsBackfromCustomer);

          List<Dms_Document> LeaseExecution = DMSDocumentLinkHelper.GetDocumentLink(app.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.LeaseExecution.ToString()), string.Empty, null, null, releateDoc);
          SetLinkDocument(app.id, DMSDocumentLinkHelper.eDocumentField.LeaseExecution, LeaseExecution);

          List<Dms_Document> AmendmentExecution = DMSDocumentLinkHelper.GetDocumentLink(app.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.AmendmentExecution.ToString()), string.Empty, null, null, releateDoc);
          SetLinkDocument(app.id, DMSDocumentLinkHelper.eDocumentField.AmendmentExecution, AmendmentExecution);

          List<Dms_Document> Financial_RentForecastAmount = DMSDocumentLinkHelper.GetDocumentLink(app.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.Financial_RentForecastAmount.ToString()), string.Empty, null, null, releateDoc);
          SetLinkDocument(app.id, DMSDocumentLinkHelper.eDocumentField.Financial_RentForecastAmount, Financial_RentForecastAmount);

          List<Dms_Document> NTPtoCust = DMSDocumentLinkHelper.GetDocumentLink(app.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.NTPtoCust.ToString()), string.Empty, null, null, releateDoc);
          SetLinkDocument(app.id, DMSDocumentLinkHelper.eDocumentField.NTPtoCust, NTPtoCust);

          List<Dms_Document> RegCommencementLetterDate = DMSDocumentLinkHelper.GetDocumentLink(app.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.RegCommencementLetterDate.ToString()), string.Empty, null, null, releateDoc);
          SetLinkDocument(app.id, DMSDocumentLinkHelper.eDocumentField.RegCommencementLetterDate, RegCommencementLetterDate);

          //CO
          List<Dms_Document> Preconstruction_PreconstructionSitewalk = DMSDocumentLinkHelper.GetDocumentLink(app.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.Preconstruction_PreconstructionSitewalk.ToString()), string.Empty, null, null, releateDoc);
          SetLinkDocument(app.id, DMSDocumentLinkHelper.eDocumentField.Preconstruction_PreconstructionSitewalk, Preconstruction_PreconstructionSitewalk);

          List<Dms_Document> NTPIssuedDateStatus = DMSDocumentLinkHelper.GetDocumentLink(app.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.NTPIssuedDateStatus.ToString()), string.Empty, null, null, releateDoc);
          SetLinkDocument(app.id, DMSDocumentLinkHelper.eDocumentField.NTPIssuedDateStatus, NTPIssuedDateStatus);

          List<Dms_Document> EarlyInstalltion_ViolationLetterSent = DMSDocumentLinkHelper.GetDocumentLink(app.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.EarlyInstalltion_ViolationLetterSent.ToString()), string.Empty, null, null, releateDoc);
          SetLinkDocument(app.id, DMSDocumentLinkHelper.eDocumentField.EarlyInstalltion_ViolationLetterSent, EarlyInstalltion_ViolationLetterSent);

          List<Dms_Document> EarlyInstallation_ViolationInvoiceCreatedByFinops = DMSDocumentLinkHelper.GetDocumentLink(app.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.EarlyInstallation_ViolationInvoiceCreatedByFinops.ToString()), string.Empty, null, null, releateDoc);
          SetLinkDocument(app.id, DMSDocumentLinkHelper.eDocumentField.EarlyInstallation_ViolationInvoiceCreatedByFinops, EarlyInstallation_ViolationInvoiceCreatedByFinops);

          List<Dms_Document> PostConstruction_Sitewalk = DMSDocumentLinkHelper.GetDocumentLink(app.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.PostConstruction_Sitewalk.ToString()), string.Empty, null, null, releateDoc);
          SetLinkDocument(app.id, DMSDocumentLinkHelper.eDocumentField.PostConstruction_Sitewalk, PostConstruction_Sitewalk);

          List<Dms_Document> CloseOut_FinalSiteApproval = DMSDocumentLinkHelper.GetDocumentLink(app.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.CloseOut_FinalSiteApproval.ToString()), string.Empty, null, null, releateDoc);
          SetLinkDocument(app.id, DMSDocumentLinkHelper.eDocumentField.CloseOut_FinalSiteApproval, CloseOut_FinalSiteApproval);

          List<Dms_Document> CloseOut_AsBuilt = DMSDocumentLinkHelper.GetDocumentLink(app.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.CloseOut_AsBuilt.ToString()), string.Empty, null, null, releateDoc);
          SetLinkDocument(app.id, DMSDocumentLinkHelper.eDocumentField.CloseOut_AsBuilt, CloseOut_AsBuilt);

          List<Dms_Document> MarketHandoff_Status = DMSDocumentLinkHelper.GetDocumentLink(app.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.MarketHandoff_Status.ToString()), string.Empty, null, null, releateDoc);
          SetLinkDocument(app.id, DMSDocumentLinkHelper.eDocumentField.MarketHandoff_Status, MarketHandoff_Status);


          #region Revision
          this.hidden_document_RevisionReveivedDate.Value = String.Empty;
          if (app != null)
          {
              string json = "";
              int count = 0;
              using (CPTTEntities ce = new CPTTEntities())
              {
                  count = LeaseAppRevisions.GetJSON(false, out json, app.id);
                  if (json != null || json != "")
                  {
                      //create document label
                      List<LeaseAppRevisionsTemplate> rvTemplate = LeaseAppRevisions.ParseFromJson(json);
                      if (rvTemplate.Count > 0)
                      {
                          foreach (LeaseAppRevisionsTemplate rvTemp in rvTemplate)
                          {
                              int rvID = int.Parse(rvTemp.ID, CultureInfo.CurrentCulture);
                              List<Dms_Document> RevisionReveivedDate = DMSDocumentLinkHelper.GetDocumentLink(rvID, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.RevisionReveivedDate.ToString()), string.Empty, null, null, releateDoc);
                              SetLinkDocument(rvID, DMSDocumentLinkHelper.eDocumentField.RevisionReveivedDate, RevisionReveivedDate);
                          }
                      }
                      else
                      {
                          if (this.hidden_revision_id.Value == String.Empty)
                          {
                              this.hidden_revision_id.Value = SA_DEFAULT_ID;
                          }
                          SetLinkDocument(Convert.ToInt32(this.hidden_revision_id.Value, CultureInfo.CurrentCulture), DMSDocumentLinkHelper.eDocumentField.RevisionReveivedDate, new List<Dms_Document>());
                      }
                  }
              }
          }
          #endregion
      }

      private void SetLinkDocument(int ID, DMSDocumentLinkHelper.eDocumentField field, List<Dms_Document> docData)
      {
          //insert template data in case no documents link
          if (docData.Count == 0)
          {
              Dms_Document doc = new Dms_Document();
              doc.ID = ID;
              doc.ref_field = field.ToString();
              docData.Add(doc);
          }

          switch (field)
          {
              //SA
              case DMSDocumentLinkHelper.eDocumentField.SAWToSAC:

                  if (!string.IsNullOrEmpty(hidden_document_SAWToSAC.Value))
                  {
                      List<Dms_Document> SAWToSAC = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(hidden_document_SAWToSAC.Value);
                      SAWToSAC.AddRange(docData);
                      docData = SAWToSAC;
                  }

                  this.hidden_document_SAWToSAC.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                  break;
              case DMSDocumentLinkHelper.eDocumentField.SAWRequest:

                  if (!string.IsNullOrEmpty(hidden_document_SAWRequest.Value))
                  {
                      List<Dms_Document> SAWRequest = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(hidden_document_SAWRequest.Value);
                      SAWRequest.AddRange(docData);
                      docData = SAWRequest;
                  }

                  this.hidden_document_SAWRequest.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                  break;
              case DMSDocumentLinkHelper.eDocumentField.Structural_Decision:

                  if (!string.IsNullOrEmpty(hidden_document_Structural_Decision.Value))
                  {
                      List<Dms_Document> Structural_Decision = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(hidden_document_Structural_Decision.Value);
                      Structural_Decision.AddRange(docData);
                      docData = Structural_Decision;
                  }

                  this.hidden_document_Structural_Decision.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                  break;
              case DMSDocumentLinkHelper.eDocumentField.SA_Received:

                  if (!string.IsNullOrEmpty(hidden_document_SA_Received.Value))
                  {
                      List<Dms_Document> dmsList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(hidden_document_SA_Received.Value);
                      dmsList.AddRange(docData);
                      docData = dmsList;
                  }

                  this.hidden_document_SA_Received.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                  break;
              case DMSDocumentLinkHelper.eDocumentField.DelayedOrder_OnHold:

                  if (!string.IsNullOrEmpty(hidden_document_DelayedOrder_OnHold.Value))
                  {
                      List<Dms_Document> dmsList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(hidden_document_DelayedOrder_OnHold.Value);
                      dmsList.AddRange(docData);
                      docData = dmsList;
                  }

                  this.hidden_document_DelayedOrder_OnHold.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                  break;
              case DMSDocumentLinkHelper.eDocumentField.DelayedOrder_Cancelled:

                  if (!string.IsNullOrEmpty(hidden_document_DelayedOrder_Cancelled.Value))
                  {
                      List<Dms_Document> dmsList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(hidden_document_DelayedOrder_Cancelled.Value);
                      dmsList.AddRange(docData);
                      docData = dmsList;
                  }

                  this.hidden_document_DelayedOrder_Cancelled.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                  break;
              case DMSDocumentLinkHelper.eDocumentField.PurchaseOrder_Received:

                  if (!string.IsNullOrEmpty(hidden_document_PurchaseOrder_Received.Value))
                  {
                      List<Dms_Document> dmsList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(hidden_document_PurchaseOrder_Received.Value);
                      dmsList.AddRange(docData);
                      docData = dmsList;
                  }

                  this.hidden_document_PurchaseOrder_Received.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                  break;

              //PR
              case DMSDocumentLinkHelper.eDocumentField.PreliminaryDecision:
                  this.hidden_document_PreliminaryDecision.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                  break;
              case DMSDocumentLinkHelper.eDocumentField.SDPtoCustomer:
                  this.hidden_document_SDPtoCustomer.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                  break;
              case DMSDocumentLinkHelper.eDocumentField.PreliminarySitewalk:
                  this.hidden_document_PreliminarySitewalk.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                  break;
              case DMSDocumentLinkHelper.eDocumentField.LandlordConsentType:
                  this.hidden_document_LandlordConsentType.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                  break;
              //DR
              case DMSDocumentLinkHelper.eDocumentField.LeaseExhibit_Received:
                  this.hidden_document_LeaseExhibit_Received.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                  break;
              case DMSDocumentLinkHelper.eDocumentField.LeaseExhibit_Decision:
                  this.hidden_document_LeaseExhibit_Decision.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                  break;
              case DMSDocumentLinkHelper.eDocumentField.LandlordConsent_LandlordNotice:
                  this.hidden_document_LandlordConsent_LandlordNotice.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                  break;
              case DMSDocumentLinkHelper.eDocumentField.LandlordConsentRequested:
                  this.hidden_document_LandlordConsentRequested.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                  break;
              case DMSDocumentLinkHelper.eDocumentField.LandlordConsentReceived:
                  this.hidden_document_LandlordConsentReceived.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                  break;
              case DMSDocumentLinkHelper.eDocumentField.ConstructionDrawingsDecision:
                  this.hidden_document_ConstructionDrawingsDecision.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                  break;
              case DMSDocumentLinkHelper.eDocumentField.ConstructionDrawingsReceived:
                  this.hidden_document_ConstructionDrawingsReceived.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                  break;
              //LE
              case DMSDocumentLinkHelper.eDocumentField.LeaseExecution:
                  this.hidden_document_LeaseExecution.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                  break;
              case DMSDocumentLinkHelper.eDocumentField.AmendmentExecution:
                  this.hidden_document_AmendmentExecution.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                  break;
              case DMSDocumentLinkHelper.eDocumentField.Financial_RentForecastAmount:
                  this.hidden_document_Financial_RentForecastAmount.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                  break;
              case DMSDocumentLinkHelper.eDocumentField.ExecutablesSenttoCustomer:
                  this.hidden_document_ExecutablesSenttoCustomer.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                  break;
              case DMSDocumentLinkHelper.eDocumentField.ExecutablesToCrown:
                  this.hidden_document_ExecutablesToCrown.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                  break;
              case DMSDocumentLinkHelper.eDocumentField.LOEHoldupReasonTypes_DocsBackfromCustomer:
                  this.hidden_document_LOEHoldupReasonTypes_DocsBackfromCustomer.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                  break;
              case DMSDocumentLinkHelper.eDocumentField.NTPtoCust:
                  this.hidden_document_NTPtoCust.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                  break;
              case DMSDocumentLinkHelper.eDocumentField.RegCommencementLetterDate:
                  this.hidden_document_RegCommencementLetterDate.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                  break;
               //CO
              case DMSDocumentLinkHelper.eDocumentField.Preconstruction_PreconstructionSitewalk:
                  this.hidden_document_Preconstruction_PreconstructionSitewalk.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                  break;
              case DMSDocumentLinkHelper.eDocumentField.NTPIssuedDateStatus:
                  this.hidden_document_NTPIssuedDateStatus.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                  break;
              case DMSDocumentLinkHelper.eDocumentField.EarlyInstalltion_ViolationLetterSent:
                  this.hidden_document_EarlyInstalltion_ViolationLetterSent.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                  break;
              case DMSDocumentLinkHelper.eDocumentField.EarlyInstallation_ViolationInvoiceCreatedByFinops:
                  this.hidden_document_EarlyInstallation_ViolationInvoiceCreatedByFinops.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                  break;
              case DMSDocumentLinkHelper.eDocumentField.PostConstruction_Sitewalk:
                  this.hidden_document_PostConstruction_Sitewalk.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                  break;
              case DMSDocumentLinkHelper.eDocumentField.CloseOut_FinalSiteApproval:
                  this.hidden_document_CloseOut_FinalSiteApproval.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                  break;
              case DMSDocumentLinkHelper.eDocumentField.CloseOut_AsBuilt:
                  this.hidden_document_CloseOut_AsBuilt.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                  break;
              case DMSDocumentLinkHelper.eDocumentField.MarketHandoff_Status:
                  this.hidden_document_MarketHandoff_Status.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                  break;
              case DMSDocumentLinkHelper.eDocumentField.RevisionReveivedDate:

                  if (!string.IsNullOrEmpty(hidden_document_RevisionReveivedDate.Value))
                  {
                      List<Dms_Document> dmsList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(hidden_document_RevisionReveivedDate.Value);
                      dmsList.AddRange(docData);
                      docData = dmsList;
                  }
                  this.hidden_document_RevisionReveivedDate.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                  break;
              default:
                  // do the defalut action
                  break;
          }
      }

      protected void DocumentDownload(object sender, EventArgs e)
      {
          LinkButton obj = (LinkButton)sender;
          string keyName = obj.Attributes["key"];
          if (!string.IsNullOrEmpty(keyName))
          {
              AmazonDMS.DownloadObject(keyName);
          }
      }

      private Dms_Document updateNewDocumentID(Dms_Document document, int newID)
     {
         document.ID = newID;
         return document;
     }
      protected void SaveRVLinkDocument(int leaseAppID)
      {
          int intRVID = GetLeaseAppRevisionID(leaseAppID);
          List<Dms_Document> jsonDocData = new List<Dms_Document>();
          // insert RevisionReveivedDate
          if (!string.IsNullOrEmpty(this.hidden_document_RevisionReveivedDate.Value))
          {
              String jsonRevisionReveivedDate = hidden_document_RevisionReveivedDate.Value;
              List<Dms_Document> tmpListObj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(jsonRevisionReveivedDate);
              var filterData = from a in tmpListObj
                               where a.type != string.Empty && a.ID == 0
                               select a;
              foreach (Dms_Document doc in filterData.ToList())
              {
                  jsonDocData.Add(updateNewDocumentID(doc, intRVID));
              }
          }

          //Insert link document to DB
          foreach (Dms_Document doc in jsonDocData)
          {
              //Check if not template data will insert to DB
              if ((doc.ref_field != "") && (doc.ref_key != ""))
              {
                  List<Dms_Document> tmpList = DMSDocumentLinkHelper.GetDocumentLink(doc.ID, DMSDocumentLinkHelper.DocumentField(doc.ref_field), doc.ref_key);
                  if (tmpList.Count == 0)
                  {
                      DMSDocumentLinkHelper.AddDocumentLink(doc.ID, intRVID, DMSDocumentLinkHelper.DocumentField(doc.ref_field), doc.ref_key, true);
                      DocumentsChangeLog.AddLogOnCreateNewItem(doc, SiteID.Text, intRVID.ToString(), DocumentsChangeLog.eDocumentLogAction.Upload, Request, true);
                  }
              }
          }         
      }
      protected void SaveSALinkDocument(int leaseAppID)
      {   
            //get current SA ID
            int intSAID = GetStructureAnalysisID(leaseAppID);
            //save document
            List<Dms_Document> jsonDocData = new List<Dms_Document>();
         
            if (!string.IsNullOrEmpty(this.hidden_document_SAWToSAC.Value))
            {
                //Add data from filed SAWToSAC
                String jsonSAWToSAC = hidden_document_SAWToSAC.Value;
                List<Dms_Document> tmpListObj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(jsonSAWToSAC);
                var filterData = from a in tmpListObj 
                           where a.type != string.Empty && a.ID == 0
                           select a;
                foreach (Dms_Document doc in filterData.ToList())
                {
                    jsonDocData.Add(updateNewDocumentID(doc,leaseAppID) );
                }
            }

            if (!string.IsNullOrEmpty(this.hidden_document_SAWRequest.Value))
            {
                //Add data from filed SAWRequest
                String jsonSAWRequest = hidden_document_SAWRequest.Value;
                List<Dms_Document> tmpListObj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(jsonSAWRequest);
                var filterData = from a in tmpListObj
                                 where a.type != string.Empty && a.ID == 0
                                 select a;
                foreach (Dms_Document doc in filterData.ToList())
                {
                    jsonDocData.Add(updateNewDocumentID(doc, leaseAppID));
                }
            }
         
            if (!string.IsNullOrEmpty(this.hidden_document_SA_Received.Value))
            {
                //Add data from filed SA_Received
                String jsonSA_Received = hidden_document_SA_Received.Value;
                List<Dms_Document> tmpListObj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(jsonSA_Received);
                var filterData = from a in tmpListObj
                                 where a.type != string.Empty && a.ID == 0
                                 select a;
                foreach (Dms_Document doc in filterData.ToList())
                {
                    jsonDocData.Add(updateNewDocumentID(doc, leaseAppID));
                }
            }

            if (!string.IsNullOrEmpty(this.hidden_document_Structural_Decision.Value))
            {
                //Add data from filed SA_Received
                String jsonStructural_Decision = hidden_document_Structural_Decision.Value;
                List<Dms_Document> tmpListObj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(jsonStructural_Decision);
                var filterData = from a in tmpListObj
                                 where a.type != string.Empty && a.ID == 0
                                 select a;
                foreach (Dms_Document doc in filterData.ToList())
                {
                    jsonDocData.Add(updateNewDocumentID(doc, leaseAppID));
                }
            }

            //DelayedOrder
            if (!string.IsNullOrEmpty(this.hidden_document_DelayedOrder_OnHold.Value))
            {
                //Add data from filed SA_Received
                String jsonDelayedOrder_OnHold = hidden_document_DelayedOrder_OnHold.Value;

                List<Dms_Document> tmpListObj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(jsonDelayedOrder_OnHold);
                var filterData = from a in tmpListObj
                                 where a.type != string.Empty && a.ID == 0
                                 select a;
                foreach (Dms_Document doc in filterData.ToList())
                {
                    jsonDocData.Add(updateNewDocumentID(doc, leaseAppID));
                }
            }
            if (!string.IsNullOrEmpty(this.hidden_document_DelayedOrder_Cancelled.Value))
            {
                //Add data from filed SA_Received
                String jsonDelayedOrder_Cancelled = hidden_document_DelayedOrder_Cancelled.Value;

                List<Dms_Document> tmpListObj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(jsonDelayedOrder_Cancelled);
                var filterData = from a in tmpListObj
                                 where a.type != string.Empty && a.ID == 0
                                 select a;
                foreach (Dms_Document doc in filterData.ToList())
                {
                    jsonDocData.Add(updateNewDocumentID(doc, leaseAppID));
                }
            }

            //Purchase Order
            if (!string.IsNullOrEmpty(this.hidden_document_PurchaseOrder_Received.Value))
            {
                //Add data from filed SA_Received
                String jsonPurchaseOrder_Received = hidden_document_PurchaseOrder_Received.Value;

                List<Dms_Document> tmpListObj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dms_Document>>(jsonPurchaseOrder_Received);
                var filterData = from a in tmpListObj
                                 where a.type != string.Empty && a.ID == 0
                                 select a;
                foreach (Dms_Document doc in filterData.ToList())
                {
                    jsonDocData.Add(updateNewDocumentID(doc, leaseAppID));
                }
            }
            //Insert link document to DB
            foreach (Dms_Document doc in jsonDocData)
            {
                //Check if not template data will insert to DB
                if ((doc.ref_field != "") && (doc.ref_key != ""))
                {
                    List<Dms_Document> tmpList = DMSDocumentLinkHelper.GetDocumentLink(doc.ID, DMSDocumentLinkHelper.DocumentField(doc.ref_field), doc.ref_key);
                    if (tmpList.Count == 0)
                    {
                        DMSDocumentLinkHelper.AddDocumentLink(doc.ID, intSAID, DMSDocumentLinkHelper.DocumentField(doc.ref_field), doc.ref_key, true);
                        DocumentsChangeLog.AddLogOnCreateNewItem(doc, SiteID.Text, intSAID.ToString(), DocumentsChangeLog.eDocumentLogAction.Upload, Request, true);
                    }
                }
            }            
      }


      private int GetStructureAnalysisID(int leaseAppID)
      {
          int? intSAID = null;
          using (CPTTEntities ce = new CPTTEntities())
          {
              if (this.hidden_SA_ID.Value == SA_DEFAULT_ID)
              {
                  if ((from sa in ce.structural_analyses
                       where sa.lease_application_id == leaseAppID
                       select sa.id).Any())
                  {
                      intSAID = (from sa in ce.structural_analyses
                                 where sa.lease_application_id == leaseAppID
                                 select sa.id).Max();
                  }
              }
              else
              {
                  intSAID = Convert.ToInt32(hidden_SA_ID.Value, CultureInfo.CurrentCulture);
              }
          }
          if(!intSAID.HasValue )
          {
              intSAID =  Convert.ToInt32(SA_DEFAULT_ID, CultureInfo.CurrentCulture);
          }
          return intSAID.Value ;
      }

      private int GetLeaseAppRevisionID(int leaseAppID)
      {
          int? intRVID = null;
          using (CPTTEntities ce = new CPTTEntities())
          {
              if (this.hidden_revision_id.Value == SA_DEFAULT_ID)
              {
                  if ((from sa in ce.leaseapp_revisions
                       where sa.lease_application_id == leaseAppID
                       select sa.id).Any())
                  {
                      intRVID = (from sa in ce.leaseapp_revisions
                                 where sa.lease_application_id == leaseAppID
                                 select sa.id).Max();
                  }
              }
              else
              {
                  intRVID = Convert.ToInt32(hidden_revision_id.Value, CultureInfo.CurrentCulture);
              }
          }
          if (!intRVID.HasValue)
          {
              intRVID = Convert.ToInt32(SA_DEFAULT_ID, CultureInfo.CurrentCulture);
          }
          return intRVID.Value;
      }

      #endregion

      private void GetSessionFilter()
      {
          // Check session filter
          DefaultFilterSession filterSession = new DefaultFilterSession();

          if (HttpContext.Current.Session != null && HttpContext.Current.Session[DefaultFilterSession.SessionName] != null)
          {
              filterSession = (DefaultFilterSession)HttpContext.Current.Session[DefaultFilterSession.SessionName];

              string hiddenFilter = DefaultFilterSession.GetFilterSession(filterSession, DefaultFilterSession.FilterType.LeaseAppFilter);
              backToList.NavigateUrl += "?filter=" + hiddenFilter;
              
          }
      }
   }
}
