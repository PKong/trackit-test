﻿<%@ Page Title="Lease Application Version History | TrackIT2" Language="C#" MasterPageFile="~/Templates/Site.Master" AutoEventWireup="true" CodeBehind="Versions.aspx.cs" Inherits="TrackIT2.LeaseApplications.Versions" %>
<%@ MasterType TypeName="TrackIT2.Templates.SiteMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   <h3>Lease Application Version History</h3>

   <asp:Panel ID="pnlNoId" runat="server">
      <p class="errorMessage">
         No lease application Id specified. Please go back to the list/search
         page and select a record to view versions for.
      </p>      
   </asp:Panel>

   <asp:Panel ID="pnlVersions" runat="server">
      <asp:GridView ID="gvVersions" runat="server"
                    AllowSorting = "true"
                    AutoGenerateColumns="false" 
                    onrowdatabound="gvVersions_RowDataBound" 
         onsorting="gvVersions_Sorting">
         <AlternatingRowStyle CssClass="listAlternatingRow" />         

         <Columns>
            <asp:TemplateField HeaderText="Options">
               <ItemTemplate>
                  Link
               </ItemTemplate>            
            </asp:TemplateField>
         
            <asp:TemplateField HeaderText="Version" SortExpression="Version"></asp:TemplateField>
            <asp:TemplateField HeaderText="User" SortExpression="UpdatedBy"></asp:TemplateField>
            <asp:TemplateField HeaderText="Date" SortExpression="UpdatedAt"></asp:TemplateField>
            <asp:TemplateField HeaderText="Field Changed" SortExpression="FieldChanged"></asp:TemplateField>
            <asp:TemplateField HeaderText="Old Value"></asp:TemplateField>
            <asp:TemplateField HeaderText="New Value"></asp:TemplateField>
         </Columns>
      </asp:GridView>

      <asp:Label ID="lblNoVerions" runat="server" CssClass="statusMessage" Text="No versions exist for this application."></asp:Label>
   </asp:Panel>
</asp:Content>