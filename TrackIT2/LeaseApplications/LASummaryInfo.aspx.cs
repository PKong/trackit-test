﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.BLL;
using TrackIT2.DAL;
using TrackIT2.Objects;
using System.Collections;
using System.Text;

namespace TrackIT2.LeaseApplications.Summary
{
    public partial class Edit : System.Web.UI.Page
    {
        public lease_applications CurrentLeaseApplication
        {
            get
            {
                return (lease_applications)this.ViewState["CurrentLeaseApplication"];
            }
            set
            {
                this.ViewState["CurrentLeaseApplication"] = value;
            }
        }
        private Hashtable m_htVarCharInvalidField;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //this.InitialControl();
                int iLeaseAppID = 0;
                Utility.ClearSubmitValidator();
                //this.BindLookupLists();
                if (Request.QueryString["id"] != null && int.TryParse(Request.QueryString["id"], out iLeaseAppID))
                {
                    LoadData(iLeaseAppID);
                    //btnSubmitCreateApps.Text = "Save";
                }
            }
        }

        protected void LoadData(int leaseAppID)
        {
           lease_applications app = LeaseApplication.Search(leaseAppID);
           CurrentLeaseApplication = app;
           ProjectCategory category = LeaseApplication.GetProjectCategory(app);
           List<metadata> viewFields = MetaData.GetViewableFieldsByCategory(category);
          
           if (Security.IsFieldViewable(viewFields, "lease_applications", "antenna_count"))
           {
              Utility.ControlValueSetter(this.txt_antenna_count, app.antenna_count);

              if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "antenna_count"))
              {
                 txt_antenna_count.Enabled = false;
              }
           }
           else
           {
              this.lbl_antenna_count.Visible = false;
              this.txt_antenna_count.Visible = false;
           }

           if (Security.IsFieldViewable(viewFields, "lease_applications", "antenna_dimensions"))
           {
              Utility.ControlValueSetter(this.txt_antenna_dimensions, app.antenna_dimensions);

              if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "antenna_dimensions"))
              {
                 txt_antenna_dimensions.Enabled = false;
              }
           }
           else
           {
              this.lbl_antenna_dimensions.Visible = false;
              this.txt_antenna_dimensions.Visible = false;
           }


           if (Security.IsFieldViewable(viewFields, "lease_applications", "antenna_mount_type"))
           {
              Utility.ControlValueSetter(this.txt_antenna_mount_type, app.antenna_mount_type);

              if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "antenna_mount_type"))
              {
                 txt_antenna_mount_type.Enabled = false;
              }
           }
           else
           {
              this.lbl_antenna_mount_type.Visible = false;
              this.txt_antenna_mount_type.Visible = false;
           }

           if (Security.IsFieldViewable(viewFields, "lease_applications", "antenna_rad"))
           {
              Utility.ControlValueSetter(this.txt_antenna_rad, app.antenna_rad);

              if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "antenna_rad"))
              {
                 txt_antenna_rad.Enabled = false;
              }
           }
           else
           {
              this.lbl_antenna_rad.Visible = false;
              this.txt_antenna_rad.Visible = false;
           }

           if (Security.IsFieldViewable(viewFields, "lease_applications", "antenna_weight"))
           {
              Utility.ControlValueSetter(this.txt_antenna_weight, app.antenna_weight);

              if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "antenna_weight"))
              {
                 txt_antenna_weight.Enabled = false;
              }
           }
           else
           {
              this.lbl_antenna_weight.Visible = false;
              this.txt_antenna_weight.Visible = false;
           }

           if (Security.IsFieldViewable(viewFields, "lease_applications", "line_count"))
           {
              Utility.ControlValueSetter(this.txt_line_count, app.line_count);

              if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "line_count"))
              {
                 txt_line_count.Enabled = false;
              }
           }
           else
           {
              this.lbl_line_count.Visible = false;
              this.txt_line_count.Visible = false;
           }

           if (Security.IsFieldViewable(viewFields, "lease_applications", "line_diameter"))
           {
              Utility.ControlValueSetter(this.txt_line_diameter, app.line_diameter);

              if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "line_diameter"))
              {
                 txt_line_diameter.Enabled = false;
              }
           }
           else
           {
              this.lbl_line_diameter.Visible = false;
              this.txt_line_diameter.Visible = false;
           }

           if (Security.IsFieldViewable(viewFields, "lease_applications", "line_length"))
           {
              Utility.ControlValueSetter(this.txt_line_length, app.line_length);

              if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "line_length"))
              {
                 txt_line_length.Enabled = false;
              }
           }
           else
           {
              this.lbl_line_length.Visible = false;
              this.txt_line_length.Visible = false;
           }

           if (Security.IsFieldViewable(viewFields, "lease_applications", "lna_rdu_count"))
           {
              Utility.ControlValueSetter(this.txt_lna_rdu_count, app.lna_rdu_count);

              if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "lna_rdu_count"))
              {
                 txt_lna_rdu_count.Enabled = false;
              }
           }
           else
           {
              this.lbl_lna_rdu_count.Visible = false;
              this.txt_lna_rdu_count.Visible = false;
           }

           if (Security.IsFieldViewable(viewFields, "lease_applications", "lna_rdu_dimensions"))
           {
              Utility.ControlValueSetter(this.txt_lna_rdu_dimensions, app.lna_rdu_dimensions);

              if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "lna_rdu_dimensions"))
              {
                 txt_lna_rdu_dimensions.Enabled = false;
              }
           }
           else
           {
              this.lbl_lna_rdu_dimensions.Visible = false;
              this.txt_lna_rdu_dimensions.Visible = false;
           }

           if (Security.IsFieldViewable(viewFields, "lease_applications", "lna_rdu_rad"))
           {
              Utility.ControlValueSetter(this.txt_lna_rdu_rad, app.lna_rdu_rad);

              if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "lna_rdu_rad"))
              {
                 txt_lna_rdu_rad.Enabled = false;
              }
           }
           else
           {
              this.lbl_lna_rdu_rad.Visible = false;
              this.txt_lna_rdu_rad.Visible = false;
           }

           if (Security.IsFieldViewable(viewFields, "lease_applications", "lna_rdu_weight"))
           {
              Utility.ControlValueSetter(this.txt_lna_rdu_weight, app.lna_rdu_weight);

              if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "lna_rdu_weight"))
              {
                 txt_lna_rdu_weight.Enabled = false;
              }
           }
           else
           {
              this.lbl_lna_rdu_weight.Visible = false;
              this.txt_lna_rdu_weight.Visible = false;
           }

           if (Security.IsFieldViewable(viewFields, "lease_applications", "mw_diameter"))
           {
              Utility.ControlValueSetter(this.txt_mw_diameter, app.mw_diameter);

              if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "mw_diameter"))
              {
                 txt_mw_diameter.Enabled = false;
              }
           }
           else
           {
              this.lbl_mw_diameter.Visible = false;
              this.txt_mw_diameter.Visible = false;
           }

           if (Security.IsFieldViewable(viewFields, "lease_applications", "mw_rad"))
           {
              Utility.ControlValueSetter(this.txt_mw_rad, app.mw_rad);

              if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "mw_rad"))
              {
                 txt_mw_rad.Enabled = false;
              }
           }
           else
           {
              this.lbl_mw_rad.Visible = false;
              this.txt_mw_rad.Visible = false;
           }

           if (Security.IsFieldViewable(viewFields, "lease_applications", "other_equipment"))
           {
              Utility.ControlValueSetter(this.txt_other_equipment, app.other_equipment);

              if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "other_equipment"))
              {
                 txt_other_equipment.Enabled = false;
              }
           }
           else
           {
              this.lbl_other_equipment.Visible = false;
              this.txt_other_equipment.Visible = false;
           }

           if (Security.IsFieldViewable(viewFields, "lease_applications", "ground_expansion_required_yn"))
           {
              Utility.ControlValueSetter(this.ddl_ground_expansion_required_yn, app.ground_expansion_required_yn);

              if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "ground_expansion_required_yn"))
              {
                 ddl_ground_expansion_required_yn.Enabled = false;
              }
           }
           else
           {
              this.lbl_ground_expansion_required_yn.Visible = false;
              this.ddl_ground_expansion_required_yn.Visible = false;
           }                      
        }

        protected void btn_submit_edit_summary_Click(Object sender, EventArgs e)
        {
            if (CurrentLeaseApplication != null)
            {
                Boolean blnIsValid = true;
                m_htVarCharInvalidField = new Hashtable();

                lease_applications app = CurrentLeaseApplication;

                app.antenna_count = Utility.PrepareInt(this.txt_antenna_count.Text);
                blnIsValid = blnIsValid & Utility.ValidateVarchar(Utility.PrepareString(this.txt_antenna_dimensions.Text), app, m_htVarCharInvalidField, "antenna_dimensions", "Antenna Dimensions");
                if (blnIsValid) app.antenna_dimensions = Utility.PrepareString(this.txt_antenna_dimensions.Text);

                blnIsValid = blnIsValid & Utility.ValidateVarchar(Utility.PrepareString(this.txt_antenna_mount_type.Text), app, m_htVarCharInvalidField, "antenna_mount_type", "Antenna Mount Type");
                if (blnIsValid) app.antenna_mount_type = Utility.PrepareString(this.txt_antenna_mount_type.Text);

                blnIsValid = blnIsValid & Utility.ValidateVarchar(Utility.PrepareString(this.txt_antenna_rad.Text), app, m_htVarCharInvalidField, "antenna_rad", "Antenna Rad");
                if (blnIsValid) app.antenna_rad = Utility.PrepareString(this.txt_antenna_rad.Text);

                blnIsValid = blnIsValid & Utility.ValidateVarchar(Utility.PrepareString(this.txt_antenna_weight.Text), app, m_htVarCharInvalidField, "antenna_weight", "Antenna Weight");
                if (blnIsValid) app.antenna_weight = Utility.PrepareString(this.txt_antenna_weight.Text);

                blnIsValid = blnIsValid & Utility.ValidateVarchar(Utility.PrepareString(this.txt_line_count.Text), app, m_htVarCharInvalidField, "line_count", "Line Count");
                if (blnIsValid) app.line_count = Utility.PrepareString(this.txt_line_count.Text);

                blnIsValid = blnIsValid & Utility.ValidateVarchar(Utility.PrepareString(this.txt_line_diameter.Text), app, m_htVarCharInvalidField, "line_diameter", "Line Diameter");
                if (blnIsValid) app.line_diameter = Utility.PrepareString(this.txt_line_diameter.Text);

                blnIsValid = blnIsValid & Utility.ValidateVarchar(Utility.PrepareString(this.txt_line_length.Text), app, m_htVarCharInvalidField, "line_length", "Line Length");
                if (blnIsValid) app.line_length = Utility.PrepareString(this.txt_line_length.Text);

                blnIsValid = blnIsValid & Utility.ValidateVarchar(Utility.PrepareString(this.txt_lna_rdu_count.Text), app, m_htVarCharInvalidField, "lna_rdu_count", "LNA RDU Count");
                if (blnIsValid) app.lna_rdu_count = Utility.PrepareString(this.txt_lna_rdu_count.Text);

                blnIsValid = blnIsValid & Utility.ValidateVarchar(Utility.PrepareString(this.txt_lna_rdu_dimensions.Text), app, m_htVarCharInvalidField, "lna_rdu_dimensions", "LNA RDU Dimensions");
                if (blnIsValid) app.lna_rdu_dimensions = Utility.PrepareString(this.txt_lna_rdu_dimensions.Text);

                blnIsValid = blnIsValid & Utility.ValidateVarchar(Utility.PrepareString(this.txt_lna_rdu_rad.Text), app, m_htVarCharInvalidField, "lna_rdu_rad", "LNA RDU Rad");
                if (blnIsValid) app.lna_rdu_rad = Utility.PrepareString(this.txt_lna_rdu_rad.Text);

                blnIsValid = blnIsValid & Utility.ValidateVarchar(Utility.PrepareString(this.txt_lna_rdu_weight.Text), app, m_htVarCharInvalidField, "lna_rdu_weight", "LNA RDU Weight");
                if (blnIsValid) app.lna_rdu_weight = Utility.PrepareString(this.txt_lna_rdu_weight.Text);

                blnIsValid = blnIsValid & Utility.ValidateVarchar(Utility.PrepareString(this.txt_mw_diameter.Text), app, m_htVarCharInvalidField, "mw_diameter", "MW Diameter");
                if (blnIsValid) app.mw_diameter = Utility.PrepareString(this.txt_mw_diameter.Text);

                blnIsValid = blnIsValid & Utility.ValidateVarchar(Utility.PrepareString(this.txt_mw_rad.Text), app, m_htVarCharInvalidField, "mw_rad", "MW Rad");
                if (blnIsValid) app.mw_rad = Utility.PrepareString(this.txt_mw_rad.Text);

                app.other_equipment = Utility.PrepareString(this.txt_other_equipment.Text);
                app.ground_expansion_required_yn = Utility.ConvertYNToBoolean(this.ddl_ground_expansion_required_yn.SelectedValue);

                if (blnIsValid)
                {
                    if (String.IsNullOrEmpty(LeaseApplication.Update(app)))
                    {
                        Master.StatusBox.showStatusMessage("Update Complete.");
                        //Redirect to edit page with created id.
                        ClientScript.RegisterStartupScript(GetType(), "Load",
                        String.Format("<script type='text/javascript'>window.parent.location.href = 'Edit.aspx?id={0}'; </script>",
                        app.id.ToString()));
                    }
                    else
                    {
                        Master.StatusBox.showStatusMessage("Error Occur. Please Try again.");
                    }
                }
                else
                {
                    StringBuilder sErrorMsg = new StringBuilder(Globals.ERROR_HEADER);
                    foreach (string item in m_htVarCharInvalidField.Keys)
                    {
                        if (m_htVarCharInvalidField[item] != null)
                        {
                            sErrorMsg.AppendLine(String.Format(Globals.ERROR_INVALID_LENGTH_FORMAT, item, m_htVarCharInvalidField[item]));
                        }
                    }
                    Master.StatusBox.showStatusMessage(sErrorMsg.ToString());
                }
                

                
            }
        }
    }
}