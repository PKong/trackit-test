﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.DAL;
using TrackIT2.BLL;
using System.Collections;

namespace TrackIT2.LeaseApplications
{
    public partial class Search : System.Web.UI.Page
    {
        private AdvanceSearch.LeaseApplicationFilter objADVSearch = new AdvanceSearch.LeaseApplicationFilter();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if(Request["result"] != null && Request["result"] != "")
                {
                    Response.Clear();
                    string ret = AdvanceSearch.GetDDLDBJSON(objADVSearch, Request["result"]);
                    if (ret != "")
                    {
                        Response.ContentType = "text/plain";
                        Response.Write(ret);
                        Response.End();
                    }
                }
            }
            else
            {
                //clears the response written into the buffer and end the response.
                Response.Clear();
                Response.End();
            }
        }
    }

    
}