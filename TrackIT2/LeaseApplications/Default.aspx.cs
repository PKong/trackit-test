﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.BLL;
using TrackIT2.DAL;
using System.Collections.Specialized;
using System.Web.Script.Serialization;
using System.IO;
using TrackIT2.Handlers;
using System.ComponentModel;
using System.Web.Services;


namespace TrackIT2.LeaseApplications
{
    public partial class _Default : System.Web.UI.Page
    {

        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindLookupLists();
                PageUtility.SetupSearchResultFilters(this, panelFilterBox);
                this.SetupFlexiGrid();
                // Clear Session
                DefaultFilterSession.ClearSession(HttpContext.Current, DefaultFilterSession.FilterType.LeaseAppFilter);
            }
        }
        //-->

        /// <summary>
        /// Bind Lookup Lists
        /// </summary>
        protected void BindLookupLists()
        {

            CPTTEntities ce = new CPTTEntities();

            // Customers
            var customers = ce.customers.OrderBy(Utility.GetField<customer>("customer_name")).OrderBy(Utility.GetIntField<customer>("sort_order")).ToList();
            Utility.BindDDLDataSource(this.lsbCustomer, customers, "id", "customer_name", null);

            // Lease App Type
            var leaseAppTypeslist = ce.leaseapp_types.ToList();
            List<leaseapp_types> lstLeasApptyp = leaseAppTypeslist.OrderBy(Utility.GetField<leaseapp_types>("lease_application_type")).ToList();
            var sortAlgorithm = new DynamicComparer<leaseapp_types>();
            sortAlgorithm.SortOrder(x => x.lease_application_type);
            lstLeasApptyp.Sort(sortAlgorithm);
            Utility.BindDDLDataSource(this.lsbLeaseAppType, lstLeasApptyp, "id", "lease_application_type", null);

            
            var sort_leaseAppTypes = leaseAppTypeslist;
            var dynamicSort_leaseAppTypes = new DynamicComparer<leaseapp_types>();
            dynamicSort_leaseAppTypes.SortOrder(x => x.lease_application_type);
            sort_leaseAppTypes.Sort(dynamicSort_leaseAppTypes);
            Utility.BindDDLDataSource(this.lsbLeaseAppType, sort_leaseAppTypes, "id", "lease_application_type", null);
            

            // Lease Activity Type
            var leaseapp_activitiesList = ce.leaseapp_activities.ToList<leaseapp_activities>();
            List<leaseapp_activities> sort_lease_app_activity = leaseapp_activitiesList;
            var tmp_lease_app_activity = new DynamicComparer<leaseapp_activities>();
            tmp_lease_app_activity.SortOrder(x => x.lease_app_activity);
            sort_lease_app_activity.Sort(tmp_lease_app_activity);
            Utility.BindDDLDataSource(this.lsbActivityType, sort_lease_app_activity, "id", "lease_app_activity", null);


            var sort_leaseActivityTypes = leaseapp_activitiesList;
            var dynamicSort_leaseActivityTypes = new DynamicComparer<leaseapp_activities>();
            dynamicSort_leaseActivityTypes.SortOrder(x => x.lease_app_activity);
            sort_leaseActivityTypes.Sort(dynamicSort_leaseActivityTypes);
            Utility.BindDDLDataSource(this.lsbActivityType, sort_leaseActivityTypes, "id", "lease_app_activity", null);            

            // Lease App Status
            var sort_leaseapp_statuses = ce.leaseapp_statuses.ToList();
            var tmp_leaseapp_statuses = new DynamicComparer<leaseapp_statuses>();
            tmp_leaseapp_statuses.SortOrder(x => x.lease_application_status);
            sort_leaseapp_statuses.Sort(tmp_leaseapp_statuses);
            Utility.BindDDLDataSource(this.lsbLeaseAppStatus, sort_leaseapp_statuses, "id", "lease_application_status", null);

            // Carrier Project
            var sort_carrierProjects = ce.leaseapp_carrier_projects.ToList();
            var tmp_carrierProjects = new DynamicComparer<leaseapp_carrier_projects>();
            tmp_carrierProjects.SortOrder(x => x.carrier_project_name);
            sort_carrierProjects.Sort(tmp_carrierProjects);
            Utility.BindDDLDataSource(this.lsbCarrierProject, sort_carrierProjects, "id", "carrier_project_name", null);

            // Market Name
            var sort_marketNames = ce.sites.Select(x => new { market_name = x.market_name }).Distinct().ToList();
            sort_marketNames.Sort((obj1, obj2) => new AlphanumComparatorFast().Compare(obj1.market_name, obj2.market_name));
            Utility.BindDDLDataSource(this.lsbMarketName, sort_marketNames, "market_name", "market_name", "- Select -");

            // Site Management Status
            var sort_SiteManagementStatus = ce.management_statuses.ToList();
            var tmp_SiteManagementStatus = new DynamicComparer<management_statuses>();
            tmp_SiteManagementStatus.SortOrder(x => x.name);
            sort_SiteManagementStatus.Sort(tmp_SiteManagementStatus);
            Utility.BindDDLDataSource(this.lsbSiteManagementStatus, sort_SiteManagementStatus, "id", "name", null);

            // Initial Specialist
            var sort_employees = BLL.User.GetUserInfo().ToList();
            var DynamicComparer_employees = new DynamicComparer<UserInfo>();
            DynamicComparer_employees.SortOrder(x => x.FullName);
            sort_employees.Sort(DynamicComparer_employees);
            Utility.BindDDLDataSource(this.lsbInitialSpecialist, sort_employees, "ID", "FullName", null);

            // Current Specialist
            Utility.BindDDLDataSource(this.lsbCurrentSpecialist, sort_employees, "ID", "FullName", null);
            
            // Project Manager
            Utility.BindDDLDataSource(this.lsbProjectManager, sort_employees, "ID", "FullName", null);

            // Advance Search 
            BindAdvanceSearch();
        }
        //-->

        /// <summary>
        /// Setup Flexi Grid
        /// </summary>
        protected void SetupFlexiGrid()
        {
            fgApplications.SortColumn = fgApplications.Columns.Find(col => col.Code == "site_uid");
            fgApplications.SortDirection = FlexigridASPNET.GridSortOrder.Asc;
            fgApplications.RowPerPageOptions = new int[] { 10, 20, 30, 40, 50 };
            fgApplications.NoItemMessage = "No Data";
        }
        //-->

        public void ExportClick(object s, EventArgs e)
        {
            try
            {

                ExportFileHandler extHand;
                ExportFile ef = new ExportFile();
                var searchData = hiddenSearchValue.Value;
                NameValueCollection data = new NameValueCollection();
                if (!string.IsNullOrEmpty(searchData))
                {
                    var jss = new JavaScriptSerializer();
                    var table = jss.Deserialize<dynamic>(searchData);

                    for (int x = 0; x < table.Length; x++)
                    {
                        data.Add(table[x]["name"], table[x]["value"]);
                    }
                }

                const int startIndex = 4;
                var parameters = new NameValueCollection();

                // If we have form values, extract them to build the object query.
                if (data.Count > startIndex)
                {
                   for (var i = startIndex; i < data.Count; i++)
                   {
                      var itemKey = data.GetKey(i);
                      var itemValue = data.Get(i);

                      parameters.Add(itemKey, itemValue);
                   }
                }

                NameValueCollection columnValue = ExportFile.GetColumn(hiddenColumnValue.Value);

                extHand = new ExportFileHandler(TrackIT2.BLL.ExportFile.eExportType.eLeaseApp, data, columnValue, parameters);
                extHand.Load_Data_OnComplete += new ExportFileHandler.Load_Data_OnComplete_Handler(Load_Data_OnComplete);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                Master.StatusBox.showStatusMessage("Export report error.");
            }
        }

        protected void Load_Data_OnComplete(List<ExportClass> loadDataResult, NameValueCollection data, NameValueCollection column, RunWorkerCompletedEventArgs e)
        {
            try
            {
                if (e.Error == null)
                {
                    ExportFile ef = new ExportFile();
                    MemoryStream mem = ef.WriteReport(loadDataResult, column, ExportFile.eExportType.eLeaseApp, Server.MapPath("/"));

                    if (mem != null)
                    {
                        string fileName = ExportFile.GenarateFileName(data["fileName"]);
                        DownloadFile(mem, fileName);
                    }
                    else
                    {
                        Master.StatusBox.showStatusMessage("Export report error.");
                    }
                }
                else
                {
                    throw e.Error;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                Master.StatusBox.showStatusMessage("Export report error.");
            }
        }

        private void DownloadFile(MemoryStream mstream, string reportName)
        {
            try
            {
                byte[] byteArray = mstream.ToArray();
                Response.Clear();

                if (Response.Cookies.Count > 0)
                {
                    bool flag = true;
                    foreach (string c in Response.Cookies)
                    {   
                        if (c == "fileDownloadToken")
                        {
                            Response.Cookies["fileDownloadToken"].Value = hiddenDownloadFlag.Value;
                            flag = false;
                        }
                    }
                    if (flag)
                    {
                        Response.AppendCookie(new HttpCookie("fileDownloadToken", hiddenDownloadFlag.Value));
                    }
                }
                else
                {
                    Response.AppendCookie(new HttpCookie("fileDownloadToken", hiddenDownloadFlag.Value));
                }
                Response.AddHeader("Content-Disposition", "attachment; filename=" + reportName + ".xls");
                Response.AddHeader("Content-Length", byteArray.Length.ToString()); 
                Response.ContentType = "application/vnd.msexcel";
                Response.BinaryWrite(byteArray);
                Response.Flush();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                Master.StatusBox.showStatusMessage("Export report error.");
            }
        }

        protected void BindAdvanceSearch()
        {
            ddlCriteria.Items.Clear();
            AdvanceSearch.LeaseApplicationFilter advSearch = new AdvanceSearch.LeaseApplicationFilter();
            List<ListItem> criteria = new List<ListItem>();
            foreach (AdvanceSearch.CritereaInfo item in advSearch.LstCriterea)
            {
                criteria.Add(new ListItem(item.NameInDDL, item.GetDDLValue()));
            }
            var sort_criteria = criteria;
            sort_criteria.Sort((obj1, obj2) => new AlphanumComparatorFast().Compare(obj1.Text, obj2.Text));
            ddlCriteria.Items.AddRange(sort_criteria.Cast<ListItem>().ToArray());
        }
    }
}
