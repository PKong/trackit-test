﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/Modal.Master" AutoEventWireup="true" CodeBehind="TMTtransfer.aspx.cs" Inherits="TrackIT2.LeaseApplications.TMTtransfer" %>
<%@ MasterType TypeName="TrackIT2.Templates.Modal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   <!-- Styles -->
   <%: Styles.Render("~/Styles/tmt_transfer") %>
    
   <!-- Scripts -->
   <%: Scripts.Render("~/Scripts/tmt_transfer") %>

    <script type="text/javascript">
        function showMessageTransfer(url) {
            var header = "Lease Application Created";
            var message = "A new Lease Application has been created, you can view it by following this <a href='" + url + "' target='_blank'>link</a>.";
            var DialogHtml = '<div id="dialog-popup-confirm" title="' + header + '">' +
                        '<p>' + "<span style='font-size:1.1em;'>" + message + "</span>" + '</p></div>';
            $('#dialog-popup-confirm').remove();
            $(DialogHtml).dialog({
                modal: true,
                buttons: {
                    'OK': function () {
                        $("#MainContent_btnSubmitCreateApps").val("Save");
                        $("#MainContent_btnSubmitCreateApps").attr("disabled", false);
                       $(this).dialog('close');
                    }
               },
               close: function() {
                    closeDailog();
               }
           });
           $(DialogHtml).bind('dialogclose', function (event) {
               alert('closed');
           });
            $('#dialog-popup-confirm').dialog("option", "width", 400);
            $("#dialog-popup-confirm").dialog("option", "position", 'center');
            $('#dialog-popup-confirm').dialog('open');
        }

        function closeDailog() {
            if (window.location.host != window.parent.location.host) {
                $.postMessage({ is_close: true }, $('#MainContent_tmoURL').val(), parent);
            }
            else {
                parent.$('#modalImportExternal').dialog('close');
            }
        };

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div id="message_alert"></div>
    <div class="create-container" style="width:650px;margin: 0 auto 0 auto;">
        <div class="form_element">
            <div id="divCustomerDetails" runat="server">
                <h4>Please confirm the customer of this application</h4>
                <label ID="Label1" runat="server">
                    Legal Entity Name</label>
                <asp:TextBox ID="txtLegalEntityName" Text="" runat="server" Enabled="false" ></asp:TextBox><br />
                <label ID="Label2" runat="server">
                    DBA</label>
                <asp:TextBox ID="txtDBA" Text="" runat="server" Enabled="false" ></asp:TextBox><br />
                <label ID="lblCustomer" runat="server">
                    Customer</label>
                <asp:DropDownList ID="ddlCustomer" CssClass="select-field" runat="server"></asp:DropDownList><br />
            </div>
            <div id="divApplicationDetails" runat="server">
                <h4>Lease Application Details</h4>                
                <label ID="lblAppType" runat="server">App Type</label>
                <asp:DropDownList ID="ddlAppType" runat="server" CssClass="select-field" >
                </asp:DropDownList>
                <br />
                <label ID="lblActivityType" runat="server">Activity Type</label>
                <asp:DropDownList ID="ddlActivityType" runat="server" CssClass="select-field" ></asp:DropDownList>
                <br />
                <label ID="lblAppStatus" runat="server">App Status</label>
                <asp:DropDownList ID="ddlAppStatus" runat="server" CssClass="select-field" ></asp:DropDownList>
                <br />
            </div>
            <div id="divTMOApp" runat="server">
               <h4>TMO Application</h4>
                <label>Selected TMO</label>
               <asp:TextBox ID="txt_TMOAppId" Text="" runat="server" Enabled="false" ></asp:TextBox>
                <br />
            </div>
            <div id="divSite" runat="server">
                <h4>Site</h4>
                <label>Selected Site</label>
                <asp:TextBox ID="txt_Site" Text="" runat="server" Enabled="false" ></asp:TextBox>
            </div>
        </div>
        <asp:Button ID="btnSubmitCreateApps" runat="server" CssClass="buttonLASubmit ui-button ui-widget ui-state-default ui-corner-all" UseSubmitBehavior="false" Text="Create" OnClick="btnAdd_Click" OnClientClick="if (!validateTransferApp()) { return false;}" />                
    </div>
    <div id="modalExternal" class="modal-container" style="overflow: hidden;height:110px;"></div>
    <asp:HiddenField ID="tmoURL" runat="server"  Value="" />
</asp:Content>
