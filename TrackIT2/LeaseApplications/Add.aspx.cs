﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.BLL;
using TrackIT2.DAL;
using TrackIT2.Objects;
using System.Collections;
using System.Text;
using System.Text.RegularExpressions;

namespace TrackIT2.LeaseApplications
{
	public partial class Add : System.Web.UI.Page
	{
        public lease_applications CurrentLeaseApplication
        {
            get
            {
                return (lease_applications)this.ViewState["CurrentLeaseApplication"];
            }
            set
            {
                this.ViewState["CurrentLeaseApplication"] = value;
            }
        }

        private Hashtable m_htVarCharInvalidField;

		protected void Page_Load(object sender, EventArgs e)
		{
            if (!Page.IsPostBack)
            {
                int iLeaseAppID = 0;
                Utility.ClearSubmitValidator();
                this.BindLookupLists();
                if (Request.QueryString["id"] != null && int.TryParse(Request.QueryString["id"], out iLeaseAppID))
                {
                    ViewState["id"] = iLeaseAppID;
                    LoadData();
                    btnSubmitCreateApps.Text = "Save";
                }
                else
                {
                    this.ResetForm();
                }             
            }
		}

		protected void btnCancel_Click(object sender, EventArgs e)
		{
			Response.Redirect("~/LeaseApplications/", false);
		}
      
        protected void btnAdd_Click(object sender, EventArgs e)
        {
           string status = null; 

            if (Page.IsValid)
            {
               m_htVarCharInvalidField = new Hashtable();
                lease_applications row = (btnSubmitCreateApps.Text != "Save" ? new lease_applications() : CurrentLeaseApplication);
                Boolean blnIsValid = true;  //Use for double-check data.
                Boolean blnUIDIsValid = true;
                
               //Application Details
                row.leaseapp_type_id = Utility.PrepareInt(this.ddlAppType.SelectedValue);
                
               if (row.leaseapp_type_id == null)
               {
                  blnIsValid = false;
               }               

               row.leaseapp_activity_id = Utility.PrepareInt(this.ddlActivityType.SelectedValue);

               if (row.leaseapp_activity_id == null)
               {
                  blnIsValid = false;
               }

                row.leaseapp_rcvd_date = Utility.PrepareDate(this.txtReceived.Text);
                row.leaseapp_status_id = Utility.PrepareInt(this.ddlAppStatus.SelectedValue);
                row.leaseapp_status_date = Utility.PrepareDate(this.txtAppStatusDate.Text);

               // Risk Level
                row.risk_level_id = Utility.PrepareInt(this.ddlRiskLevel.SelectedValue);
                row.risk_level_notes = Utility.PrepareString(this.txtRiskLevelNotes.Text);

                //Customer Details
                row.customer_id = Utility.PrepareInt(this.ddlCustomer.SelectedValue);

                blnIsValid = blnIsValid & Utility.ValidateVarchar(Utility.PrepareString(this.txtCustomerUID.Text), row, m_htVarCharInvalidField, "customer_site_uid", "Customer UID");
                if (blnIsValid) row.customer_site_uid = Utility.PrepareString(this.txtCustomerUID.Text);

                blnIsValid = blnIsValid & Utility.ValidateVarchar(Utility.PrepareString(this.txtCustomerSiteName.Text), row, m_htVarCharInvalidField, "customer_site_name", "Customer Site Name");
                if (blnIsValid) row.customer_site_name = Utility.PrepareString(this.txtCustomerSiteName.Text);

                blnIsValid = blnIsValid & Utility.ValidateVarchar(Utility.PrepareString(this.txtOriginalCarrier.Text), row, m_htVarCharInvalidField, "original_carrier", "Original Carrier");
                if (blnIsValid) row.original_carrier = Utility.PrepareString(this.txtOriginalCarrier.Text);

                row.leaseapp_carrier_project_id = Utility.PrepareInt(this.ddlCarrierProject.SelectedValue);

                row.leaseapp_priority = Utility.PrepareInt(this.ddlPriority.SelectedValue);

                //Team Assigned
                row.tmo_specialist_initial_emp_id = Utility.PrepareInt(this.ddlInitialSpecialist.SelectedValue);
                row.tmo_specialist_current_emp_id = Utility.PrepareInt(this.ddlCurrentSpecialist.SelectedValue);
                row.tmo_pm_emp_id = Utility.PrepareInt(this.ddlProjectManager.SelectedValue);
                row.colocation_coordinator_emp_id = Utility.PrepareInt(this.ddlColocationCoordinator.SelectedValue);
                row.am_emp_id = Utility.PrepareInt(this.ddlAccountManager.SelectedValue);
                
                //Payment Details
                row.leaseapp_fee_rcvd_date = Utility.PrepareDate(this.txtFeeReceived.Text);
                row.leaseapp_fee_amount = Utility.PrepareDecimal(this.txtFeeAmount.Text);
                blnIsValid = blnIsValid & Utility.ValidateVarchar(Utility.PrepareString(this.txtCheck.Text), row, m_htVarCharInvalidField, "leaseapp_fee_check_po_nbr", "Check Number");
                if (blnIsValid) row.leaseapp_fee_check_po_nbr = Utility.PrepareString(this.txtCheck.Text);
                

                //Site
                TextBox txtSiteUID = ctrSiteID.Controls[1] as TextBox;
                row.site_uid = Utility.PrepareString(txtSiteUID.Text.ToUpper());
                if (row.site_uid == null)
                { 
                    blnIsValid = false; 
                }
                else 
                {
                    if (BLL.Site.Search(row.site_uid) == null)
                    {
                        blnIsValid = false;
                        blnUIDIsValid = false;
                        Utility.SettingErrorColorForTextbox(txtSiteUID);
                    }
                    else
                    {
                        Utility.ClearErrorColorForTextbox(txtSiteUID);
                    }
                }

                // TMO Application
                TextBox TMO_AppID_auto = txtTMOAppId.Controls[1] as TextBox;
                string tmoAppID = Utility.PrepareString(TMO_AppID_auto.Text.ToUpper());
                
                //Check TMP AppID
                lease_applications leaseApp = CheckTmoAppID(tmoAppID);

                if (leaseApp == null || leaseApp.id == row.id)
                {
                    row.TMO_AppID = Utility.PrepareString(TMO_AppID_auto.Text.ToUpper());
                }
                else
                {
                    string link = string.Format("<a href='/LeaseApplications/Edit.aspx?id={0}' target='_blank' >{0}</a>.", leaseApp.id);
                    status = string.Format("TMO App Id {0} is already assigned to application {1}", leaseApp.TMO_AppID, link);
                }

                if (string.IsNullOrEmpty(status))
                {
                    if (btnSubmitCreateApps.Text != "Save")
                    {
                        //Add
                        if (blnIsValid)
                        {
                            status = LeaseApplication.Add(row);

                            // A null status message indicates success.
                            if (string.IsNullOrEmpty(status))
                            {
                                // Redirect to edit page with created id. Keep save button disabled
                                // to prevent double clicks.
                                ClientScript.RegisterStartupScript(GetType(), "Load",
                                String.Format("<script type='text/javascript'>" +
                                              "   $(\"#MainContent_btnSubmitCreateApps\").val(\"Processing...\");" +
                                              "   $(\"#MainContent_btnSubmitCreateApps\").attr(\"disabled\", true);" +
                                              "   window.parent.location.href = 'Edit.aspx?id={0}';" +
                                              "</script>",
                                              row.id.ToString()));
                            }
                            else
                            {
                                Master.StatusBox.showErrorMessage(status);
                            }
                        }
                        else
                        {
                            StringBuilder sErrorMsg = new StringBuilder(Globals.ERROR_HEADER);
                            if (!blnUIDIsValid)
                            {
                                //Invalid UID
                                sErrorMsg.AppendLine(Globals.ERROR_TXT_INVALID_UID + "<br//>");
                            }
                            foreach (string item in m_htVarCharInvalidField.Keys)
                            {
                                if (m_htVarCharInvalidField[item] != null)
                                {
                                    sErrorMsg.AppendLine(String.Format(Globals.ERROR_INVALID_LENGTH_FORMAT, item, m_htVarCharInvalidField[item]));
                                }
                            }
                            Master.StatusBox.showStatusMessage(sErrorMsg.ToString());
                            //Data not valid.
                        }
                    }
                    else
                    {
                        //Edit
                        if (blnIsValid)
                        {
                            //Update current object to entity. 
                            status = LeaseApplication.Update(row);

                            // A null status indicates success.
                            if (string.IsNullOrEmpty(status))
                            {
                                // Redirect to edit page with created id. Keep save button disabled
                                // to prevent double clicks.
                                ClientScript.RegisterStartupScript(GetType(), "Load",
                                String.Format("<script type='text/javascript'>" +
                                              "   $(\"#MainContent_btnSubmitCreateApps\").val(\"Processing\");" +
                                              "   $(\"#MainContent_btnSubmitCreateApps\").attr(\"disabled\", true);" +
                                              "   window.parent.location.href = 'Edit.aspx?id={0}';" +
                                              "</script>",
                                              CurrentLeaseApplication.id));
                            }
                            else
                            {
                                Master.StatusBox.showErrorMessage(status);
                            }
                        }
                        else
                        {
                            StringBuilder sErrorMsg = new StringBuilder(Globals.ERROR_HEADER);
                            if (!blnUIDIsValid)
                            {
                                //Invalid UID
                                sErrorMsg.AppendLine(Globals.ERROR_TXT_INVALID_UID + "<br//>");
                            }
                            foreach (string item in m_htVarCharInvalidField.Keys)
                            {
                                if (m_htVarCharInvalidField[item] != null)
                                {
                                    sErrorMsg.AppendLine(String.Format(Globals.ERROR_INVALID_LENGTH_FORMAT, item, m_htVarCharInvalidField[item]));
                                }
                            }
                            Master.StatusBox.showStatusMessage(sErrorMsg.ToString());
                            //Data not valid.
                        }
                    }
                }
                else
                {
                    Master.StatusBox.showStatusMessage(status);
                }
            }
        }

        public lease_applications CheckTmoAppID(string tmoAppID)
        {
            lease_applications returnResult = null;

            if (!string.IsNullOrEmpty(tmoAppID))
            {
                using (CPTTEntities ce = new CPTTEntities())
                {
                    returnResult = (from leaseApp in ce.lease_applications
                                    where leaseApp.TMO_AppID.Equals(tmoAppID)
                                    select leaseApp).FirstOrDefault();
                }
            }
            
            return returnResult;
        }

		public void ResetForm()
		{

			// Application Details
			this.ddlAppType.ClearSelection();
			this.ddlActivityType.ClearSelection();
			this.txtReceived.Text = string.Empty;
			this.ddlAppStatus.ClearSelection();
			this.txtAppStatusDate.Text = string.Empty;

         // Risk Level
         this.ddlRiskLevel.ClearSelection();
         this.txtRiskLevelNotes.Text = string.Empty;

			//Customer Details
			this.ddlCustomer.ClearSelection();
			this.txtCustomerUID.Text = string.Empty;
			this.txtCustomerSiteName.Text = string.Empty;
			this.txtOriginalCarrier.Text = string.Empty;
			this.ddlCarrierProject.ClearSelection();
         this.ddlPriority.ClearSelection();

			//Team Assigned
			this.ddlInitialSpecialist.ClearSelection();
			this.ddlCurrentSpecialist.ClearSelection();
			this.ddlProjectManager.ClearSelection();

			//Payment Details
			this.txtFeeReceived.Text = string.Empty;
			this.txtFeeAmount.Text = string.Empty;
			this.txtCheck.Text = string.Empty;

         // TMO Application
            ((TextBox)txtTMOAppId.Controls[1]).Text = string.Empty;

			//Site
            ((TextBox)ctrSiteID.Controls[1]).Text = string.Empty;
		}
        
		protected void BindLookupLists()
		{

			CPTTEntities ce = new CPTTEntities();

			//Application Details
            List<leaseapp_types> lstLeasApptyp = ce.leaseapp_types.ToList();
            var sortAlgorithm = new DynamicComparer<leaseapp_types>();
            sortAlgorithm.SortOrder(x => x.lease_application_type);
            lstLeasApptyp.Sort(sortAlgorithm);
			Utility.BindDDLDataSource(this.ddlAppType, lstLeasApptyp, "id", "lease_application_type", "- Select -");

            var sort_lease_app_activity = ce.leaseapp_activities.ToList();
            var dynamic_leaseapp_activities = new DynamicComparer<leaseapp_activities>();
            dynamic_leaseapp_activities.SortOrder(x => x.lease_app_activity);
            sort_lease_app_activity.Sort(dynamic_leaseapp_activities);
			Utility.BindDDLDataSource(this.ddlActivityType, sort_lease_app_activity,  "id", "lease_app_activity", "- Select -");

            var sort_lease_application_status = ce.leaseapp_statuses.ToList();
            var dynamic_lease_application_status = new DynamicComparer<leaseapp_statuses>();
            dynamic_lease_application_status.SortOrder(x => x.lease_application_status);
            sort_lease_application_status.Sort(dynamic_lease_application_status);
			Utility.BindDDLDataSource(this.ddlAppStatus, sort_lease_application_status, "id", "lease_application_status", "- Select -");

            // Risk Level
            var sort_risk_levels = ce.risk_levels.ToList();
            var dynamic_risk_levels = new DynamicComparer<risk_levels>();
            dynamic_risk_levels.SortOrder(x => x.risk_level);
            sort_risk_levels.Sort(dynamic_risk_levels);
            Utility.BindDDLDataSource(this.ddlRiskLevel, sort_risk_levels, "id", "risk_level", "- Select -");

			//Customer Details
            var customers = ce.customers.OrderBy(Utility.GetField<customer>("customer_name")).OrderBy(Utility.GetIntField<customer>("sort_order"));
            Utility.BindDDLDataSource(this.ddlCustomer, customers, "id", "customer_name", "- Select -");

            var sort_carrierProjects = ce.leaseapp_carrier_projects.ToList();
            var tmp_carrierProjects = new DynamicComparer<leaseapp_carrier_projects>();
            tmp_carrierProjects.SortOrder(x => x.carrier_project_name);
            sort_carrierProjects.Sort(tmp_carrierProjects);
            Utility.BindDDLDataSource(this.ddlCarrierProject, sort_carrierProjects, "id", "carrier_project_name", "- Select -");

            Utility.BindDDLDataSource(this.ddlPriority, 
                ce.leaseapp_priorities.OrderBy(Utility.GetIntField<leaseapp_priorities>("lease_app_priority")),
               "id", "lease_app_priority", "- Select -");

			//Team Assigned
            List<UserInfo> lstUser = BLL.User.GetUserInfo();

            var sort_employees = lstUser.ToList();
            var DynamicComparer_employees = new DynamicComparer<UserInfo>();
            DynamicComparer_employees.SortOrder(x => x.FullName);
            sort_employees.Sort(DynamicComparer_employees);

            Utility.BindDDLDataSource(this.ddlInitialSpecialist, sort_employees, "ID", "FullName", "- Select -");
            Utility.BindDDLDataSource(this.ddlCurrentSpecialist, sort_employees, "ID", "FullName", "- Select -");
            Utility.BindDDLDataSource(this.ddlProjectManager, sort_employees, "ID", "FullName", "- Select -");
            Utility.BindDDLDataSource(this.ddlColocationCoordinator, sort_employees, "ID", "FullName", "- Select -");
            Utility.BindDDLDataSource(this.ddlAccountManager, sort_employees, "ID", "FullName", "- Select -");

		}

      protected void LoadData()
      {
         lease_applications app = LeaseApplication.Search((int)ViewState["id"]);
         ProjectCategory category = LeaseApplication.GetProjectCategory(app);
         List<metadata> viewFields = MetaData.GetViewableFieldsByCategory(category);

         CurrentLeaseApplication = app;

         //Application Details
         if (Security.IsDisplayGroupVisible("Application Details", category))
         {
            if (Security.IsFieldViewable(viewFields, "lease_applications", "site_uid"))
            {
               Utility.ControlValueSetter(((TextBox)ctrSiteID.Controls[1]), app.site_uid);

               if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "site_uid"))
               {
                  ((TextBox)ctrSiteID.Controls[1]).Enabled = false;
               }
            }
            else
            {
               ctrSiteID.Visible = false;
            }

            if (Security.IsFieldViewable(viewFields, "lease_applications", "TMO_AppID"))
            {
                Utility.ControlValueSetter(((TextBox)txtTMOAppId.Controls[1]), Utility.PrepareString(app.TMO_AppID));

               if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "TMO_AppID"))
               {
                  ((TextBox)txtTMOAppId.Controls[1]).Enabled = false;
               }
            }
            
            if (Security.IsFieldViewable(viewFields, "lease_applications", "leaseapp_type_id"))
            {
               Utility.ControlValueSetter(ddlAppType, app.leaseapp_type_id);

               if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "leaseapp_type_id"))
               {
                  ddlAppType.Enabled = false;
               }
            }
            else
            {
               lblAppType.Visible = false;
               ddlAppType.Visible = false;
            }

            if (Security.IsFieldViewable(viewFields, "lease_applications", "leaseapp_activity_id"))
            {
               Utility.ControlValueSetter(ddlActivityType, app.leaseapp_activity_id);

               if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "leaseapp_activity_id"))
               {
                  ddlActivityType.Enabled = false;
               }
            }
            else
            {
               lblActivityType.Visible = false;
               ddlActivityType.Visible = false;
            }

            if (Security.IsFieldViewable(viewFields, "lease_applications", "leaseapp_rcvd_date"))
            {
               Utility.ControlValueSetter(txtReceived, app.leaseapp_rcvd_date);

               if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "leaseapp_rcvd_date"))
               {
                  txtReceived.Enabled = false;
               }
            }
            else
            {
               lblAppReceived.Visible = false;
               txtReceived.Visible = false;
            }

            if (Security.IsFieldViewable(viewFields, "lease_applications", "leaseapp_status_id"))
            {
               Utility.ControlValueSetter(ddlAppStatus, app.leaseapp_status_id);

               if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "leaseapp_status_id"))
               {
                  ddlAppStatus.Enabled = false;
               }
            }
            else
            {
               lblAppStatus.Visible = false;
               ddlAppStatus.Visible = false;
            }            
            
            if (Security.IsFieldViewable(viewFields, "lease_applications", "leaseapp_status_date"))
            {
               Utility.ControlValueSetter(txtAppStatusDate, app.leaseapp_status_date);

               if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "leaseapp_status_date"))
               {
                  txtAppStatusDate.Enabled = false;
               }
            }
            else
            {
               lblAppStatusDate.Visible = false;
               txtAppStatusDate.Visible = false;
            }
         }
         else
         {
            divApplicationDetails.Style.Add("display", "none");
         }

         // Risk Level
         // Risk level and notes function together.
         if (Security.IsFieldViewable(viewFields, "lease_applications", "risk_level_id"))
         {
            Utility.ControlValueSetter(ddlRiskLevel, app.risk_level_id);
            Utility.ControlValueSetter(txtRiskLevelNotes, app.risk_level_notes);

            if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "risk_level_id"))
            {
               ddlRiskLevel.Enabled = false;
               txtRiskLevelNotes.Enabled = false;
            }
         }
         else
         {
            divRiskLevel.Style.Add("display", "none");
         }

         //Customer Details
         if (Security.IsDisplayGroupVisible("Customer Details", category))
         {
            if (Security.IsFieldViewable(viewFields, "lease_applications", "customer_id"))
            {
               Utility.ControlValueSetter(ddlCustomer, app.customer_id);

               if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "customer_id"))
               {
                  ddlCustomer.Enabled = false;
               }
            }
            else
            {
               lblCustomer.Visible = false;
               ddlCustomer.Visible = false;
            }

            if (Security.IsFieldViewable(viewFields, "lease_applications", "customer_site_uid"))
            {
               Utility.ControlValueSetter(txtCustomerUID, app.customer_site_uid);

               if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "customer_site_uid"))
               {
                  txtCustomerUID.Enabled = false;
               }
            }
            else
            {
               lblCustomerUID.Visible = false;
               txtCustomerUID.Visible = false;
            }

            if (Security.IsFieldViewable(viewFields, "lease_applications", "customer_site_name"))
            {
               Utility.ControlValueSetter(txtCustomerSiteName, app.customer_site_name);

               if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "customer_site_name"))
               {
                  txtCustomerSiteName.Enabled = false;
               }
            }
            else
            {
               lblCustomerSiteName.Visible = false;
               txtCustomerSiteName.Visible = false;
            }

            if (Security.IsFieldViewable(viewFields, "lease_applications", "original_carrier"))
            {
               Utility.ControlValueSetter(txtOriginalCarrier, app.original_carrier);

               if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "original_carrier"))
               {
                  txtOriginalCarrier.Enabled = false;
               }
            }
            else
            {
               lblOriginalCarrier.Visible = false;
               txtOriginalCarrier.Visible = false;
            }

            if (Security.IsFieldViewable(viewFields, "lease_applications", "leaseapp_carrier_project_id"))
            {
               Utility.ControlValueSetter(ddlCarrierProject, app.leaseapp_carrier_project_id);

               if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "leaseapp_carrier_project_id"))
               {
                  ddlCarrierProject.Enabled = false;
               }
            }
            else
            {
               lblCarrierProject.Visible = false;
               ddlCarrierProject.Visible = false;
            }

            if (Security.IsFieldViewable(viewFields, "lease_applications", "leaseapp_priority"))
            {
                Utility.ControlValueSetter(ddlPriority, app.leaseapp_priority);

                if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "leaseapp_priority"))
                {
                    ddlPriority.Enabled = false;
                }
            }
            else
            {
                lblPriority.Visible = false;
                ddlPriority.Visible = false;
            }
         }
         else
         {
            divCustomerDetails.Style.Add("display", "none");
            divSite.Style.Add("display", "none");
         }

         //Team Assigned
         if (Security.IsDisplayGroupVisible("Team Assigned", category))
         {
            if (Security.IsFieldViewable(viewFields, "lease_applications", "tmo_specialist_initial_emp_id"))
            {
               Utility.ControlValueSetter(this.ddlInitialSpecialist, app.tmo_specialist_initial_emp_id);

               if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "tmo_specialist_initial_emp_id"))
               {
                  ddlInitialSpecialist.Enabled = false;
               }
            }
            else
            {
                this.lblInitSpecialist.Visible = false;
                this.ddlInitialSpecialist.Visible = false;
            }

            if (Security.IsFieldViewable(viewFields, "lease_applications", "tmo_specialist_current_emp_id"))
            {
               Utility.ControlValueSetter(this.ddlCurrentSpecialist, app.tmo_specialist_current_emp_id);

               if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "tmo_specialist_current_emp_id"))
               {
                  ddlCurrentSpecialist.Enabled = false;
               }
            }
            else
            {
                this.lblCurrentSpecialist.Visible = false;
                this.ddlCurrentSpecialist.Visible = false;
            }

            if (Security.IsFieldViewable(viewFields, "lease_applications", "tmo_pm_emp_id"))
            {
               Utility.ControlValueSetter(this.ddlProjectManager, app.tmo_pm_emp_id);

               if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "tmo_pm_emp_id"))
               {
                  ddlProjectManager.Enabled = false;
               }
            }
            else
            {
                this.lblProjectManager.Visible = false;
                this.ddlProjectManager.Visible = false;
            }

            if (Security.IsFieldViewable(viewFields, "lease_applications", "colocation_coordinator_emp_id") )
            {
                Utility.ControlValueSetter(this.ddlColocationCoordinator, app.colocation_coordinator_emp_id);

                if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "colocation_coordinator_emp_id"))
                {
                   ddlColocationCoordinator.Enabled = false;
                }
            }
            else
            {
                this.lblColocationCoordinator.Visible = false;
                this.ddlColocationCoordinator.Visible = false;
            }
            if (Security.IsFieldViewable(viewFields, "lease_applications", "am_emp_id"))
            {
                Utility.ControlValueSetter(this.ddlAccountManager, app.am_emp_id);

                if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "am_emp_id"))
                {
                    ddlAccountManager.Enabled = false;
                }
            }
            else
            {
                this.lblAccountManager.Visible = false;
                this.ddlAccountManager.Visible = false;
            }  
         }
         else
         {
            divTeamAssigned.Style.Add("display", "none");
         }                           

         //Payment Details
         if (Security.IsDisplayGroupVisible("Payment Details", category))
         {
            if (Security.IsFieldViewable(viewFields, "lease_applications", "leaseapp_fee_rcvd_date"))
            {
               Utility.ControlValueSetter(this.txtFeeReceived, app.leaseapp_fee_rcvd_date);

               if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "leaseapp_fee_rcvd_date"))
               {
                  this.txtFeeReceived.Enabled = false;
               }
            }
            else
            {
               this.lblFeeReceived.Visible = false;
               this.txtFeeReceived.Visible = false;
            }

            if (Security.IsFieldViewable(viewFields, "lease_applications", "leaseapp_fee_amount"))
            {
               Utility.ControlValueSetter(txtFeeAmount, app.leaseapp_fee_amount);

               if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "leaseapp_fee_amount"))
               {
                  txtFeeAmount.Enabled = false;
               }
            }
            else
            {
               lblFeeAmount.Visible = false;
               txtFeeAmount.Visible = false;
            }

            if (Security.IsFieldViewable(viewFields, "lease_applications", "leaseapp_fee_check_po_nbr"))
            {
               Utility.ControlValueSetter(txtCheck, app.leaseapp_fee_check_po_nbr);

               if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "leaseapp_fee_check_po_nbr"))
               {
                  txtCheck.Enabled = false;
               }
            }
            else
            {
               lblCheckNo.Visible = false;
               txtCheck.Visible = false;
            }            
         }
         else
         {
            divPaymentDetails.Style.Add("display", "none");
         }                  
      }
	}
}