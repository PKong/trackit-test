﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.Objects;
using TrackIT2.SiteMaster;

namespace TrackIT2
{
   public partial class ImageSource : System.Web.UI.Page
   {
      protected void Page_Load(object sender, EventArgs e)
      {
        string photoId = Request.QueryString["Photo_id"];
        string publicKey = Request.QueryString["PublicKey"];
        string timestamp = Request.QueryString["TimeStamp"];
        string inputSignature = Request.QueryString["Sig"];
        string siteId  = Request.QueryString["SiteID"];
        string thumbnail = Request.QueryString["Thumbnail"];

        List<string> ParametersExisting = new List<string>();
        ParametersExisting.Add(siteId);
        ParametersExisting.Add(publicKey);
        ParametersExisting.Add(timestamp);

        string SignatureExisting = new DMSHelper().CreateSignature(new DMSHelper().BuildSignatureString(ParametersExisting));

        List<string> ParametersRecvd = new List<string>();
        ParametersRecvd.Add(photoId);
        ParametersRecvd.Add(publicKey);
        ParametersRecvd.Add(timestamp);

        string SignatureRecvd = new DMSHelper().CreateSignature(new DMSHelper().BuildSignatureString(ParametersRecvd));

        try
        {
            // Compare Signatures
            if(SignatureExisting == inputSignature)
            {
               DmsSubscriptionContractClient test = new DmsSubscriptionContractClient();
                
               // return byte array to caller with image type
               Response.ContentType = "image/jpeg";
               System.Drawing.Image TempImage;

               if (thumbnail.ToLower() == "no")
               {    
                  TempImage = System.Drawing.Image.FromStream(test.StreamSitePhoto(photoId, publicKey, timestamp, Server.UrlEncode(SignatureRecvd)));
               }
               else
               {
                  TempImage = System.Drawing.Image.FromStream(test.StreamSitePhotoThumbnail(photoId, publicKey, timestamp, Server.UrlEncode(SignatureRecvd)));
               }

               TempImage.Save(Response.OutputStream, System.Drawing.Imaging.ImageFormat.Jpeg);
               TempImage.Dispose();
            }
         }
         catch(CommunicationException ex)
         {
            Response.Write(ex.Message.ToString());
         }
      }
   }
}