/*********************
* Utility - JS
**********************/

/** Constants **/

    /* Global */
    var ConstGlobal = {
        DATA:
        {
            noData: "", //"No Data",
            noDataHtml: "" //"<em>No Data<em>"
        },
        GENERAL:
        {
            projectCategoryQueryString: "pc"
        },
        DEFAULT:
        {
            returnPage: "DefaultFilterSession",
            applyClick: "ApplyClick"
        }
    };
    /* /Global */ 

    /* Message Types */
    var ConstMessageTypes = {
        INFO:
        {
            type: "info",
            defaultMessage: "<p>Read information!</p>"
        },
        WARN:
        {
            type: "warning",
            defaultMessage: "<p>Pay attention!</p>"
        },
        ERROR:
        {
            type: "error",
            defaultMessage: "<p>Error!</p>"
        },
        SUCCESS:
        {
            type: "success",
            defaultMessage: "<p>Success!</p>"
        }
    };
    /* /Message Types */

    /* App Type Aliases */
    var ConstAppTypeAliases = {
        Initial:
        {
            type: "Initial",
            aliases: "Initial"
        },
        LeaseRenewalONLY:
        {
            type: "Lease Renewal ONLY",
            aliases: "Renewal"
        },
        Modification:
        {
            type: "Modification",
            aliases: "Mod"
        },
        Modification1:
        {
            type: "Modification 1",
            aliases: "Mod 1"
        },
        Modification2:
        {
            type: "Modification 2",
            aliases: "Mod 2"
        },
        Modification3:
        {
            type: "Modification 3",
            aliases: "Mod 3"
        },
        Modification4:
        {
            type: "Modification 4",
            aliases: "Mod 4"
        },
        Modification5:
        {
            type: "Modification 5",
            aliases: "Mod 5"
        },
        Modification6:
        {
            type: "Modification 6",
            aliases: "Mod 6"
        },
        Modification7:
        {
            type: "Modification 7",
            aliases: "Mod 7"
        },
        Modification8:
        {
            type: "Modification 8",
            aliases: "Mod 8"
        },
        Modification9:
        {
            type: "Modification 9",
            aliases: "Mod 9"
        },
        Modification10:
        {
            type: "Modification 10",
            aliases: "Mod 10"
        },
        Temporary:
        {
            type: "Temporary",
            aliases: "Temp"
        }
    };
    /* App Type Aliases */ 

/** /Constants **/


/* Messaging */

    /*
    *  Show Message Alert 
    *
    *  This function will display the message box inside div "message_alert"  
    *  Note: Parameter: type = {info, warning, error, success }, message = Not specified
    *                   displayClose = true
    *
    *  Updated 4/17/ Sean Patterson: Added optional displayClose to either show or
    *                                hide the close button. Used for status boxes that
    *                                need to eliminate duplicate close buttons.
    */
    function showMessageAlert(type, message, displayClose) {
        if ((type == null && message == null) || (type == "" && message == "")) {
            type = ConstMessageTypes.INFO.type;
            message = ConstMessageTypes.INFO.defaultMessage;
         }

         // Set the displayClose parameter if not specified.
         if (displayClose === undefined) {
            displayClose = true;
         }

         if (displayClose) {
            $("div#message_alert").append(
               '<div class="ms ' + type + '" style="margin-bottom:15px;">' +
                   '<p>' + message + '</p>' +
		           '<a class="close" href="javascript:void(0)">close</a>' +
               '</div>' +
               '<div class="cb"></div>'
           );
         }
         else {
            $("div#message_alert").append(
               '<div class="ms ' + type + '" style="margin-bottom:15px;">' +
                   '<p>' + message + '</p>' +
               '</div>' +
               '<div class="cb"></div>'
           );
         }

        
    }
    //-->

    // Hide Message Alert
    function hideMessageAlert() {
        $("div#message_alert").empty();
    }
    //-->

/* /Messaging */


/* String */

    /* Is Null Or Empty */
    String.prototype.isNullOrEmpty = function () {

        var isNullOrEmpty = true;
        if (this)
            if ((this.length > 0) && (this.toLowerCase() != 'null')) {
                isNullOrEmpty = false;
            }
        return isNullOrEmpty;
    };
    //-->

    // Camel Case To Array
    String.prototype.camelCaseToArray = function () {

        var delimed = this.replace(/([A-Z]+)/g, ",$1").replace(/^,/, "");
        return delimed.split(",");
    };
    //-->

    // Camel Case To String With Spaces
    String.prototype.camelCaseToStringWithSpaces = function () {

        var spacedResult = this.replace(/([A-Z]+)/g, " $1").replace(/^,/, "");
        return spacedResult;
    };
    //-->

    // TEST BY NAM
    String.prototype.camelCaseToStringWithSpacesTest = function () {
        return this
        // insert a space between lower & upper 
        .replace(/([a-z])([A-Z])/g, '$1 $2')
        // space before last upper in a sequence followed by lower 
        .replace(/\b([A-Z]+)([A-Z])([a-z])/, '$1 $2$3')
        // uppercase the first character 
        .replace(/^./, function (str) { return str.toUpperCase(); });
    };
    
    // URL Param
    $.urlParam = function (name) {
        var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (!results) { return null; }
        return unescape(results[1]) || null;
    };
    //-->

/* /String */


/* Date */

    // Format MM/DD/YYYY
    Date.prototype.formatMMDDYYYY = function () {
        return (this.getMonth()+1) +
            "/" + this.getDate() +
                "/" + this.getFullYear();
    };
    //-->

    /* /Date */

    //Grabbing query string
    function GrabGETData(queryString,aryRef) {
        //        var query = window.location.search.substring(1);
        //        var queries = new Array();
        var blnHaveGET = false;
        var parms = queryString.split('&');
        for (var i = 0; i < parms.length; i++) {
            var pos = parms[i].indexOf('=');
            if (pos > 0) {
                var key = parms[i].substring(0, pos);
                var val = parms[i].substring(pos + 1);
                aryRef[key] = val;
                blnHaveGET = true;
            }
        }
        return blnHaveGET;

    }
    //-->

