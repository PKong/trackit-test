/************************
* Application - Main JS
*************************/

/** Doc Ready **/
$(function () {

    // Tabs	
    $(function () {
        $("#tabs, #tabs2, #tabs3").tabs({ selected: 0 });
    });

    // Toggle Button	
    $('.toggle-me').wrapInner('<div class="toggle-inner"></div>');
    $('.toggle-me').after('<a class="toggle-button" href="javascript:void(0)">toggle</a>');
    $('.toggle-button').click(function () {
        $(this).parent().find('.toggle-inner').toggle();
        $(this).toggleClass('toggle-button-colapsed');
    });

    // Modal Boxes
    $(function () {

        var options = {
            autoOpen: false,
            width: 740,
            modal: true,
            title: 'Edit'
        };

        var num = '';

        $([1, 2, 3, 4, 5, 6, 7]).each(function () {
            var num = this;
            var dlg = $('#modal' + num)
			  .dialog(options);
            $('#modalbtn' + num).click(function () {
                dlg.dialog("open");
                return false;
            });

        });

        $("#modal" + num).dialog({
            modal: true,
            autoOpen: false
        });

        $("#modalbtn" + num).click(function () {
            $("#modal" + num).dialog('open')
        });

    });

    // Modal Boxes 2

    $("#modal16").dialog({
        autoOpen: false,
        width: 369,
        height: 300,
        modal: true,
        title: 'Edit'
    });

    $("#modalbtn16").click(function () {
        $("#modal16").dialog('open');
    });

    $("#btnSubmitForm").click(function () {
        $("#modal16").dialog('close');
    });

    // Save Buttons 
    $(function () {
        $("input:submit").button();
    });

    // Date Picker
    $(function () {
        $('input').filter('.datepicker').datepicker({
            numberOfMonths: 2,
            showButtonPanel: false
        });
    });

    //Slider
    $('.slideshow')
	.before('<div id="nav">')
	.cycle({
	    fx: 'scrollLeft',
	    speed: 'fast',
	    timeout: 0,
	    pager: '#nav'
	});

    $('.slideshow2').cycle({
        fx: 'scrollLeft',
        speed: 'fast',
        timeout: 0,
        pager: '.pnlnav2'
    });


    $('#nav').append($('<a id="plus-1" href="#nogo">+</a>'));

    // Multiple Select Boxes
    /*$("select[multiple]:not(.standard-multi)").asmSelect({
    addItemTarget: 'top',
    animate: true,
    highlight: false,
    sortable: true
    });*/

    // Multiple Select Boxes with limit Kantorn J. 2012-08-03
    $("select[multiple]:not(.standard-multi)").asmSelect({
        addItemTarget: 'top',
        animate: true,
        highlight: false,
        sortable: true,
        maxSelect: -1
    });

    // Disable selectbox, check the 'aspNetDisabled' class

    if ($("#MainContent_lstLOEHoldup").hasClass("aspNetDisabled")) {
        $("select#asmSelect0").attr("disabled", "disabled");
        $("ol.asmList li.asmListItem").each(function () {
            $(this).find("a.asmListItemRemove").text('');
        });
    }

    // Close Message Boxes	
    $('.close').click(function () {
        var parent = $(this).parent();
        $(parent).fadeOut('slow', function () {
            // Animation complete.
        });
    });

    // Tooltips
    $(".nolink").qtip({
        style: {
            tip: 'leftMiddle',
            fontSize: '10pt',
            border: {
                width: 1,
                radius: 4
            }
        }
        , position: {
            corner: {
                target: 'rightMiddle',
                tooltip: 'leftMiddle'
            }
        }
    });

    // Close Message Boxes	
    $('.close').live("click", function () {
        var parent = $(this).parent();
        $(parent).fadeOut('slow', function () {
            // Animation complete.
        });
    });

    $("#modal8").dialog({
        autoOpen: false,
        width: 369,
        height: 390,
        modal: true,
        title: 'Edit'
    });

    $("#modalbtn8").click(function () {
        $("#modal8").dialog('open');
    });

    // Get current URL, in case of browse on this 4 categories pages and the another 2 comment page
    // - LeaseApplications
    // - Sites
    // - SiteDataPackages 
    // - TowerModifications

    // - All Comment Lease App
    // - All Comment Tower Mod 

    var pathname = window.location.pathname;
    var page_categories = ["LeaseApplications", "Sites", "SiteDataPackages", "TowerModifications"];
    var ct_default = ["la", "tm"];

    var current_ct = getParameterByName("ct");

    var i = 0;

    while (i < page_categories.length || i < 2) {
        if (pathname.indexOf(page_categories[i]) != -1) {
            var name = page_categories[i];
            var search_name;

            switch (name) {
                case "LeaseApplications":
                    search_name = "LeaseApp";
                    break;
                case "Sites":
                    search_name = "Site";
                    break;
                case "SiteDataPackages":
                    search_name = "SDP";
                    break;
                case "TowerModifications":
                    search_name = "TowerMod";
                    break;
            };

            $("div.search-box input").each(function () {
                $(this).attr("checked", false);
            });

            $("div.search-box input:radio").each(function () {
                if ($(this).val() == search_name) {
                    $(this).attr("checked", "checked");
                }
            });
            break;
        }
        else if (current_ct != '' && current_ct == ct_default[i] && i != 2) {
            switch (current_ct) {
                case "la":
                    search_name = "LeaseApp";
                    break;
                case "tm":
                    search_name = "TowerMod";
                    break;
            };
            $("div.search-box input").each(function () {
                $(this).attr("checked", false);
            });

            $("div.search-box input:radio").each(function () {
                if ($(this).val() == search_name) {
                    $(this).attr("checked", "checked");
                }
            });
            break;
        }
        i++;
    }

});
/** /Doc Ready **/


/** /For SA Edit Page And TM Edit Page**/
var pageSA = "SA_Edit_Page";
var pageTM = "TM_Edit_Page";

function PageRequestManagerMain() {
    if ($("#hidden_PageName").val() != null) {
        if (getPageName() == pageSA) {
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandlerSA);
        }
        else if (getPageName() == pageTM) {
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandlerTM);
        }
    }
}
/** /End SA Edit Page And TM Edit Page**/

var ConstComments = {
    textAreaDefaultMessage: "Write your comment here."
};

/* Comments */

    // Setup Comments
    function setupComments() {

        // Hide loading spinner
        hideLoadingSpinner('h2-comments');

        // Reset Comments TextArea
        prepareCommentsTextArea(true);

        // Add New Button
        $('.add-new').click(function () {
            var parentDiv = $(this).parent();
            $('.new-comment', parentDiv).toggle();
            $(this).hide();
            $('.post', parentDiv).show();
            $('.comment-textarea', parentDiv).height('80px');
            $('.blah-wrapper', parentDiv).animate({
                height: "190px"
            }, 500);
            $('#blah', parentDiv).animate({
                top: "0px"
            }, 500);
        });
        // Cancel Button
        $('.comment-cancel').click(function () {
            var parentDiv = $(".add-new").parent();
            $('.add-new', parentDiv).show();
            $('.post', parentDiv).hide();
            $('.new-comment', parentDiv).toggle();
            $('.comment-textarea', parentDiv).height('24px').width('100%');
            prepareCommentsTextArea(true);
            $('.blah-wrapper', parentDiv).animate({
                height: "0px"
            }, 500);
            $('#blah', parentDiv).animate({
                top: "-200px"
            }, 500);
        });
        //
        // Comment Text Area
        $('.comment-textarea').bind("focus", function () {
            if ($(this).val() == ConstComments.textAreaDefaultMessage) {
                $(this).val('');
                prepareCommentsTextArea(false);
            }
        });
        //
        $('.comment-textarea').bind("blur", function () {
            if (String($(this).val()).isNullOrEmpty()) {
                $(this).val(ConstComments.textAreaDefaultMessage);
                prepareCommentsTextArea(true);
            }
        });
        //
        // Delete Modal Dialog
        bindDeleteCommentButtonActions();
//        openDeleteAppButtonPopup('.btn-app-delete',"Lease Application");
    }
    //-->

    // Bind Delete Comment Button Actions
    function bindDeleteCommentButtonActions() {

        // Delete Modal Dialog
        var delDialogHtml = '<div id="dialog-confirm-comment-delete" title="Delete this comment?"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This comment will be permanently deleted and cannot be recovered. Are you sure?</p></div>';
        $('.btn-comment-delete').click(function (e) {
            e.preventDefault();
            $('#dialog-confirm-comment-delete').remove();
            $(delDialogHtml).dialog({
                open: function(type, data) {
                        $(this).parent().appendTo("form[0]");
                },
                buttons: {
                    'Confirm': function () {
                        
                        //debugger;

                        $(this).dialog('close');
                        showLoadingSpinner('h2-comments');
                        __doPostBack(e.target.name, '');
                        return true;
                    },
                    'Cancel': function () {
                        $(this).dialog('close');
                        return false;
                    }
                }
            });
            $('#dialog-confirm-comment-delete').dialog('open');
        });
        //>
    }
    //-->

    // Open pop-up dialog for delete comfirmation.
    function openDeleteAppButtonPopup(sName,e,check) {
        // Delete Modal Dialog
        var delDialogHtml = '<div id="dialog-confirm-comment-delete" title="Delete this ' + sName + '?"><p><span class="ui-icon ui-icon-alert"' +
        'style="float:left; margin:0 7px 20px 0;"></span>This ' + sName + ' will be permanently deleted and cannot be recovered. Are you sure?</p></div>';
        
            var sID;
            //Checking for event name for postback later.
            if (e.target != undefined) {
                if (e.target.name == null || e.target.name == "") {
                    sID = "ctl00$MainContent$" + e.target.id.replace('MainContent', '').replace('_', '');
                }
                else {
                    sID = e.target.name;
                }
            }
            else if(e.srcElement != undefined) {

                if (e.srcElement.id != null || e.srcElement.id != "") {
                    sID = "ctl00$MainContent$" + e.srcElement.id.replace('MainContent', '').replace('_', '');
                }
                else {
                    sID = e.srcElement.id;
                }
            }
            
            //Suspend current event action to wait for dialog confirmation.
            if (e.preventDefault) {
                e.preventDefault();
            }
            else {
                e.returnValue = false;
            }

            $('#dialog-confirm-comment-delete').remove();
            //Show dialog
            if (check == undefined ||check == null || check == "" || check != 0) {
            $(delDialogHtml).dialog({
                open: function (type, data) {
                    $(this).parent().appendTo("form[0]");
                },
                buttons: {
                    'Confirm': function () {
                        $(this).dialog('close');
                        __doPostBack(sID, '');          //Re postback after confirm.
                        return true;
                    },
                    'Cancel': function () {
                        $(this).dialog('close');
                        return false;
                    }
                }
            });
            
        }
        else {
            delDialogHtml = '<div id="dialog-message" title="Status">' +
                        '<p>Error: Cannot delete new record.</p></div>';
            $(delDialogHtml).dialog({
               
            });
        }
        $('#dialog-confirm-comment-delete').dialog('open');
    }
    //-->

    // Prepare Comments TextArea
    function prepareCommentsTextArea(blReset) {

        if (blReset) {
            $('.comment-textarea').val(ConstComments.textAreaDefaultMessage);
            $('.comment-textarea').addClass('italic');
            $('.comment-textarea').addClass('comment-text-unselected');
        } else {
            $('.comment-textarea').removeClass('italic');
            $('.comment-textarea').removeClass('comment-text-unselected');
        }
    }
    //-->

    // Show Loading Spinner
    function showLoadingSpinner(id) {

        $('#' + id).activity({ segments: 8, width: 2, space: 0, length: 3, speed: 1.5, align: 'right', valign: 'top' });
    }
    // Hide Loading Spinner
    function hideLoadingSpinner(id) {

        $('#' + id).activity(false);
    }
    //-->

    // Refresh Main Comments Panel
    function refreshMainCommentsPanel() {

        // Refresh main Comments
        try {
            showLoadingSpinner('h2-comments');
            var sButtonRefresh = $("#btnRefreshComment").attr("name");
            __doPostBack(sButtonRefresh, '');
        } catch (e) {
            hideLoadingSpinner('h2-comments');
        };
    }
    //-->

    // Show Comment Box
    function showCommentBox() {
        var parentDiv = $('.add-new').parent();
        $('.new-comment', parentDiv).toggle();
        $('.add-new').hide();
        $('.post', parentDiv).show();
        $('.comment-textarea', parentDiv).height('80px');
        $('.blah-wrapper', parentDiv).animate({
            height: "190px"
        }, 500);
        $('#blah', parentDiv).animate({
            top: "0px"
        }, 500);
    }
    //-->

    /* /Comments */

    // Function to Obtain Querystring by name at page

    function getParameterByName(name)
    {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(window.location.href);
        if (results == null)
            return "";
        else
            return decodeURIComponent(results[1].replace(/\+/g, " "));
    }

