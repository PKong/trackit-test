﻿
// LeaseApp Default page
function GetAjaxCallSiteWorkFlow(handlerName) {
    // Select process by handler name
    if (handlerName.indexOf("LeaseAppList.axd")) {
        var loadList = $('.ajaxLoad');
        loadList = loadList.toArray().getUniqueAttributesValue('siteid');

        if (loadList.length > 0) {
            for(i=0;i< loadList.length ;i++){
                AjaxCallSiteWalkFlow(loadList[i]);
            }
        }
    }
}


function AjaxCallSiteWalkFlow(siteId) {
    $.ajax({
        dataType: 'text',
        url: "/AjaxDelayLoad.axd?type=WorkFlow&siteid=" + siteId,
        cache: false,
        success: function (workflowData) {
            var completeObj = $(".ajaxLoad[siteId='" + siteId + "']");
            var towerObj = $(".towerMod[siteId='" + siteId + "']");
            var issueObj = $(".issue[siteId='" + siteId + "']");
            jsonData = jQuery.parseJSON(workflowData)
            $(completeObj).append(jsonData[0].Value[0]);
            $(towerObj).append(getColumnImageIcon(ColumnTypes.TOWER_MOD, jsonData[0].Value[1], siteId));
            $(issueObj).append(getColumnImageIcon(ColumnTypes.ISSUE_TRACKER, jsonData[0].Value[2], siteId));
            $(".loading-hide[siteId='" + siteId + "']").removeClass('loading-hide');
            $("img[siteId='" + siteId + "']").remove();
        },
        error: function (error) {
        }
    });
}
//-->

// custom method
Array.prototype.getUniqueAttributesValue = function (attributeName) {
    var u = [], a = [];
    for (var i = 0, l = this.length; i < l; ++i) {
        if ($.inArray(this[i].attributes[attributeName].value,a) < 0) {
            a.push(this[i].attributes[attributeName].value);
        }
    }
    return a;
}
//-->