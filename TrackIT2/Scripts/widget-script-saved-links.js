﻿$(document).ready(function ()
{
    $(".edit-widget-button").text("Edit Widget");
    $(".edit-widget-iframe").hide();
    $(".savedlinks_form").hide();

    $(".saved-links-edit-widget-button").bind("click", function ()
    {
        if ($(this).text() == "Edit Widget")
        {
            $(this).parent().prev().prev().hide();
            $(".edit-widget-iframe").show();
            $(this).text("Finish editing");
        }
        else
        {
            $(this).parent().parent().prev().find("li.widgetRefresh").trigger("click");
            $(this).parent().prev().prev().show();
            $(".edit-widget-iframe").hide();
            $(this).text("Edit Widget");
        }
    });

    $("a#default_edit").live("click", function ()
    {
        $(".edit-widget-iframe").show();
        $("#edit_mysavedlinks").text("Finish editing");
        $(this).parent().parent().parent().remove();
    });

});

