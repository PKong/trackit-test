﻿$(document).ready(function () {
    var pathname = window.location.pathname;
    var activate = false;

    // Verify the page name which allow only 'Edit' for safety where the 'onbeforeunload' apply to all Navigation.

    if (pathname.indexOf("Edit") != -1) {
        activate = true;
    }

    if (activate) {

        var dirty = false;
        // If Textbox value was changed, get dirty

        $(".application-process input[type='text']").each(function () {
            $(this).change(function () {
                dirty = true;
                $(window).bind('beforeunload', function () {
                    return "Do you really want to leave now?";
                });

            });
        });

        // If DropdownList value was changed, get dirty

        $(".application-process select").change(function () {
            dirty = true;
            $(window).bind('beforeunload', function () {
                return "Do you really want to leave now?";
            });
        });

        $(".comment-textarea").change(function () {
            dirty = true;
            $(window).bind('beforeunload', function () {
                return "Do you really want to leave now?";
            });
        });
        // Any link on Information tab

        $("ul.information-list li a").click(function () {
            if (dirty) {
                $(window).bind('beforeunload', function () {
                    return "Do you really want to leave now?";
                });
            }

        });

        $("input.ui-button").click(function () {
            dirty = false;
            $(window).unbind("beforeunload");
        });

        $("form").submit(function () {
            $(window).unbind("beforeunload");
        });
    }

});

