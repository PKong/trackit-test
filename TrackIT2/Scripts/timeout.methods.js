﻿var DoLogout = 1;
var WarnMills;
var TimeoutMills
var RedirectURL;

function StartTimeout(TimeoutValue, WarnValue, URLValue) 
{
    TimeoutMills = TimeoutValue;
    WarnMills = WarnValue;
    RedirectURL = URLValue;
    setTimeout('UserTimeout()', TimeoutMills);
    setTimeout('WarnTimeout()', WarnMills);
}

function UserTimeout() 
{

    if (DoLogout == 1) 
    {
        //top.location.href = RedirectURL;
        document.location.assign(RedirectURL);
    }
    else 
    {
        DoLogout = 1;
        setTimeout('UserTimeout()', TimeoutMills);
        setTimeout('WarnTimeout()', WarnMills);
    }
}

function WarnTimeout() 
{
    $('#TimeoutPanel').dialog({ modal: true, closeText: '', width: 500 });
}

function PreserveSession() 
{
    DoLogout = 0;
    __doPostBack('btnPreserveSession', '');
}

