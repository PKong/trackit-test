﻿var messageValidateError = "Please complete all required fields!<br/>";
var messageValidateCurrency = "Invalid currency input.<br/>";
var messageValidatePercent = "Invalid Percent input. Please input value between 5 to 500.<br/>";
var messageValidateState = "Invalid State input.<br/>";
var messageValidateDateTime = "Invalid date time format. {MM/DD/YYYY}<br/>";
var messageValidateInteger = "Invalid input: value must be a whole number";
var messageValidateDecimal = "Invalid input: value must be a decimal number<br/>";
var messageValidateLetter = " must be only letters.";
var messageValidateUID = "UID length must 8 characters or less.<br/>";
var messageValidate8CharUID = "UID length must 8 characters.<br/>";
var messageValidateUIDOnly = "Please enter a valid Site ID.<br/>";
var messageValidateFileName = "Invalid input: value can't contain any of the following characters: /, \\, ?, *, :, |, \", <, > <br/>";
var messageValidateFolderName = "Invalid input: value can only contain: letters, numbers, underscores and hyphens.<br/>";
var messageValidateFileNameMinimumLength = "File Name must be 5 characters or more.<br/>";
var messageValidateNewLocation = "Target location must not same as current folder.<br/>";
var messageValidateNewSubLocation = "The destination folder is a subfolder of the source folder.<br/>";
var messageValidateLatitude = "Invalid Latitude input. Please input a value between -90 and 90.<br/>";
var messageValidateLongitude = "Invalid Longitude input. Please input a value between -180 and 180.<br/>";
var messageValidateZip = "Zip must be a decimal number and 5 characters.<br/>";
var messageValidateTargetUploadFolder = "Documents should not be uploaded to the Site folder directly. Please choose a subfolder.<br/>";
//>

// Validate Create Folder
function validateCreateFolder() {

    var valid = true;
    var strError = messageValidateError;

    // Folder Name - Required
    if (!validateRequiredField("#txtFolderName")) valid = false;

    if (valid) {
        
        // Folder Name - Valid Name
        if (!validateFolderName("#txtFolderName", 30)) {
            strError = messageValidateFolderName;
            valid = false;
        }
    }

    // Throw Error
    if (!valid) {
        $("#message_alert").html("");
        showMessageAlert("error", strError);
    } else {
        // Show - Loading Spinner
        $('#spinner-cont').activity({ segments: 8, width: 3, space: 0, length: 3, speed: 1.5, align: 'left' });
    }

    return valid;
}
//-->

// Validate Move Folder
function validateMoveFolder() {

    var valid = true;
    var strError = messageValidateNewLocation;

    // Targat location and Current location must not same
    if (!validateNewLocation("#MainContent_lblCurrentFolderLocation", "#lblMovingFolderLocation")) valid = false;

    if (valid) {

        // Folder Name - Valid Name
        if (!validateFolderName("#txtFolderName", 30)) {
            strError = messageValidateFolderName;
            valid = false;
        }
    }

    // Throw Error
    if (!valid) {
        $("#message_alert").html("");
        showMessageAlert("error", strError);
    } else {
        // Show - Loading Spinner
        $('#spinner-cont').activity({ segments: 8, width: 3, space: 0, length: 3, speed: 1.5, align: 'left' });
    }

    return valid;
}
function DisplayMoveFolderWarring() {
    // Throw Error
        changeBGColorError("#lblMovingFolderLocation");
        changeBGColorError("#MainContent_lblCurrentFolderLocation");

    $("#message_alert").html("");
    showMessageAlert("error", messageValidateNewSubLocation);
}
//-->

// Validate Save Document
function validateSaveDocument() {

    var valid = true;
    var strError = messageValidateError;

    // File Name
    if (!validateRequiredField("#txtFileName")) {
        valid = false;
    } else if (!validateFileName("#txtFileName", 5)) {
        strError = messageValidateFileNameMinimumLength;
        valid = false;
    }
    if (valid) {
        if (!validateSpecialChar("#txtFileName"))
            valid = false;
            strError = messageValidateFileName;
    }

    // Throw Error
    if (!valid) {
        $("#message_alert").html("");
        showMessageAlert("error", strError);
        changeBGColorError("#txtFileName");

    } else {
        // Show - Loading Spinner
        $('#spinner-cont').activity({ segments: 8, width: 3, space: 0, length: 3, speed: 1.5, align: 'left' });
    }

    return valid;
}
//-->

// Validate Move Document
function validateMoveDocument() {

    var valid = true;
    var strError = messageValidateError;

    // File Name
    if (!validateRequiredField("#txtFileName")) {
        valid = false;
    } else if (!validateFileName("#txtFileName", 5)) {
        strError = messageValidateFileNameMinimumLength;
        valid = false;
    }

    // Throw Error
    if (!valid) {
        $("#message_alert").html("");
        showMessageAlert("error", strError);
    } else {
        // Show - Loading Spinner
        $('#spinner-cont').activity({ segments: 8, width: 3, space: 0, length: 3, speed: 1.5, align: 'left' });
    }
    
    return valid;
}
//-->

// Validate Upload Document
function validateUploadDocument() {

    var valid = true;
    var strError = "";
    // File
    if ($('.MultiFile-label').length == 0) {
        valid = false;
        strError = messageValidateError;
    }
    else {
        // check sub folder
        if (!validateTargetUploadDirectory("MainContent_lblUploadingFolderLocation")) {
            valid = false;
            strError = messageValidateTargetUploadFolder;
        }

        if (valid && $("#manualRenameCheckBox").prop('checked')) {
            // File Name
            if (!validateRequiredField("#manualRenameTextBox")) {
                valid = false;
                strError = messageValidateError;
            } else if (!validateFileName("#manualRenameTextBox", 5)) {
                strError = messageValidateFileNameMinimumLength;
                valid = false;
            }
            if (valid) {
                if (!validateSpecialChar("#manualRenameTextBox"))
                    valid = false;
                    strError = messageValidateFileName;
            }
        }
    }

    // Throw Error
    if (!valid) {
        $("#message_alert").html("");
        showMessageAlert("error", strError);
    }else {
        // Show - Loading Spinner
        $('#spinner-cont').activity({ segments: 8, width: 3, space: 0, length: 3, speed: 1.5, align: 'left' });
    }
    
    return valid;
}
//-->

// Validate not be uploaded to the Site folder directly
function validateTargetUploadDirectory(labelName) {
    var targetFolder = $("#" + labelName).text();
    if (targetFolder.split('/').length > 2 && targetFolder.split('/')[2] != '') {
        return true
    }
    return false;
}
//-->

// Validate Save File Name
function validateSpecialChar(itemID) {
    var splChars = "/\\?*:|<>\"";
    var data = $(itemID).val();
    for (var i = 0; i < data.length; i++) {
        if (splChars.indexOf(data.charAt(i)) != -1) {
            return false;
        }
    }
    return true;
}
//-->

// Validate Create Issue
function validateCreateIssue() {

    var valid = true;
    var uidError = false;

    // Priority
    if (!validateDropdownField("#ddlPriority")) valid = false;

    // Status
    if (!validateDropdownField("#ddlStatus")) valid = false;

    // Issue Summary
    if (!validateRequiredField("#txtIssueSummary")) valid = false;

    // Throw Error
    if (!valid) 
    {
       var strError = messageValidateError;
       if (uidError) 
       {
          strError += messageValidateUID;
       }
       
       $("#message_alert").html("");
       showMessageAlert("error", strError);
       return false;
    }
    else
    {
       $("#MainContent_btnSubmitCreateIssue").val("Processing...");
       $("#MainContent_btnSubmitCreateIssue").attr("disabled", true);
       return true;
    }
}
//-->

// Validate Create Lease Application
function validateCreateApp()
{
    var check = true;
    var UIDerror = false;    

    // Dropdown Check
    if (!validateDropdownField("#MainContent_ddlAppType")) check = false;
    if (!validateDropdownField("#MainContent_ddlActivityType")) check = false;
    if (!validateDropdownField("#MainContent_ddlAppStatus")) check = false;

    // Checking for the .length of the object is a quick jQuery way to check if an object exists.

    // Date Check
    if ($("#MainContent_txtReceived").length && $("#MainContent_txtReceived").val().toString() != "")
    {
       if (!isValidDate("#MainContent_txtReceived")) 
       {
          changeBGColorError("#MainContent_txtReceived");
          check = false;
       }
    }

    if ($("#MainContent_txtAppStatusDate").length && $("#MainContent_txtAppStatusDate").val().toString() != "") 
    {
       if (!isValidDate("#MainContent_txtAppStatusDate")) 
       {
          changeBGColorError("#MainContent_txtAppStatusDate");
          check = false;
       }
    }

    if ($("#MainContent_txtFeeReceived").length && $("#MainContent_txtFeeReceived").val().toString() != "") 
    {
       if (!isValidDate("#MainContent_txtFeeReceived")) 
       {
          changeBGColorError("#MainContent_txtFeeReceived");
          check = false;
       }
    }

    // Textbox Check
    if (!validateUID("#txtSiteID")) 
    {
        uiDerror = true;
        check = false; 
    } 
    
    //  Amount
    if (!$("#MainContent_txtFeeAmount").val().length || $("#MainContent_txtFeeAmount").val().toString() == "")
    {
        changeBGColorDefault("#MainContent_txtFeeAmount");
    }
    else
    {
        if (!validateCurrency("#MainContent_txtFeeAmount")) check = false;
    }

    if (!check) 
    {
       var strError = messageValidateError;
       if (UIDerror) 
       {
          strError += messageValidateUID;
       }
       
       $("#message_alert").html("");
       showMessageAlert("error", strError);
       return false;
    }
    else 
    {
       $("#MainContent_btnSubmitCreateApps").val("Processing...");
       $("#MainContent_btnSubmitCreateApps").attr("disabled", true);
       return true;
    }
}
//-->

//valid create app from T-Mobile web
function validateTransferApp() {
    var check = true;
    var UIDerror = false;
    var TMOerror = false;

    // Dropdown Check
    if (!validateDropdownField("#MainContent_ddlAppType")) check = false;
    if (!validateDropdownField("#MainContent_ddlActivityType")) check = false;
    if (!validateDropdownField("#MainContent_ddlAppStatus")) check = false;

    // Customer check
    if (!validateDropdownField("#MainContent_ddlCustomer")) check = false;

    // Textbox Check
    if (!validateUID("#MainContent_txt_Site")) {
        UIDerror = true;
        check = false;
    }
    if (!validateRequiredField("#MainContent_txt_TMOAppId")) {
        TMOerror = true;
        check = false;
    }

    if (!check) {
        var strError = messageValidateError;
        if (UIDerror) {
            strError += messageValidateUID;
        }

        $("#message_alert").html("");
        showMessageAlert("error", strError);
        return false;
    }
    else {
        $("#MainContent_btnSubmitCreateApps").val("Processing...");
        $("#MainContent_btnSubmitCreateApps").attr("disabled", true);
        return true;
    }
}
// validate Summary Edit Page
function validateEditSummary()
{
    var check = true;

    if (!validateOnlyNumber("#MainContent_txt_antenna_count", 0)) check = false;
    var elementname = getElementNameValidated("MainContent_txt_antenna_count");
    var message = elementname + ' ' + messageValidateInteger;
    if (!check)
    {
        $("#message_alert").html("");
        showMessageAlert("error", message);
    }
    return check;
}

// get Element name validated, 23 Dec 2011
function getElementNameValidated(targetValidate)
{
    var nameValidated = $('#' + targetValidate).prev().text();
    return nameValidated;
}


// validate Customer Contacts Edit Page
function validateEditCustomerContacts()
{
    var check = true;

    // Checking for the .length of the object is a quick jQuery way to check if an object exists.
    if ($("#MainContent_txt_site_acq_contact_state").length && $("#MainContent_txt_site_acq_contact_state").val().toString() != "")
    {
        if (!validateOnlyLetters("#MainContent_txt_site_acq_contact_state", 2)) check = false;
    }

    if (!check)
    {
       $("#message_alert").html("");
       showMessageAlert("error", messageValidateState);
       return false;
    }
    else 
    {
       $("#MainContent_btn_submit_edit_contact").val("Processing...");
       $("#MainContent_btn_submit_edit_contact").attr("disabled", true);
       return true;

    }
}

function validateSiteDataPackage()
{
   var check = true;
    
    // Check only textbox now, create require at least SiteID
    if (!validateUID("#txtSiteID")) check = false;

    if (!check) 
    {
       $("#message_alert").html("");
       showMessageAlert("error", messageValidateUIDOnly);
       return false;
    }
    else 
    {
       $("#MainContent_btnSubmitCreateSite").val("Processing...");
       $("#MainContent_btnSubmitCreateSite").attr("disabled", true);
       return true;
    }
}

function validateTowerModification()
{
    var check = true;

    // Check only textbox now, create require at least SiteID
    if (!validateUID("#txtSiteID")) check = false;

    if (!check) 
    {
       $("#message_alert").html("");
       showMessageAlert("error", messageValidateUIDOnly);
       return false;
    }
    else 
    {
       $("#MainContent_btnSubmitCreateTower").val("Processing...");
       $("#MainContent_btnSubmitCreateTower").attr("disabled", true);
       return true;
    }
}

function validateDropdownField(name)
{
    var data = $(name).val();
    if (data == null || data == "")
    {
        changeBGColorError(name);
        return false;
    }
    else
    {
        changeBGColorDefault(name);
        return true;
    }
}

function validateSelectFileName(hiddenField) {
    var fileName = "";
    if ($(hiddenField).length > 0) {
        fileName = $(hiddenField).val();
        var file = fileName.split('.');
        if (file.length > 1) {
            for (x = 0; x < file.length; x++) {
            }
            return true;
        }
        else {
            return false;
        }
    }
    else {
        return false;
    }
}

function validateNewLocation(oldPath, newPath) {
    var oldLocation = $(oldPath).text();
    var newLocation = $(newPath).text();

    if (oldLocation == newLocation) {
        changeBGColorError(oldPath);
        changeBGColorError(newPath);
        return false;
    }
    else {
        changeBGColorDefault(oldPath);
        changeBGColorDefault(newPath);
        return true;
    }
}
function validateRequiredField(name)
{
    var data = $(name).val();
    if (data == null || data == "")
    {
        changeBGColorError(name);
        return false;
    }
    else
    {
        changeBGColorDefault(name);
        return true;
    }
}
function validateDateField(name)
{

    if (name == null || data == "")
    {
        changeBGColorError(name);
        return false;
    }
    else
    {
        changeBGColorDefault(name);
        return true;
    }
}

function isValidDate(controlName)
{
    var isValid = true;
    var data = jQuery(controlName).val();
    if (data != null || data != "")
    {
        var buffer = data.split("/");
        var d = buffer[1];
        var m = buffer[0];
        var y = buffer[2];

        if (y == null || y > 3000 || y < 1900)
        {
            isValid = false;
        }

        if (m == null || m > 12 || m < 0)
        {
            isValid = false;
        }

        if (d == null || d > 31 || m < 0)
        {
            isValid = false;
        }
        if (isValid)
            changeBGColorDefault(controlName);
        else
            changeBGColorError(controlName);
    }
    else
    {
        changeBGColorError(controlName);
        isValid = false;
    }
    return isValid;
}

// Validate File Name
function validateFileName(name, length) {

    var data = $(name).val();
    var isValid = true;

    if (length > 0) {
        if (data.length <= length) {
            isValid = false;
        }
    }
    
    if (isValid)
        changeBGColorDefault(name);
    else
        changeBGColorError(name);

    return isValid;
}
//-->

// Validate Folder Name
function validateFolderName(name, length) {

    var data = $(name).val();
    var isValid = true;

    if (length > 0) {
        if (!(data.length <= length)) {
            isValid = false;
        }
    }
    
    if (isValid) {
        var myRegExp = /^[\w.-]+$/;
        var folderMatch = data.match(myRegExp);
        if (folderMatch == null) {
            isValid = false;
        }
    }
    
    if (isValid)
        changeBGColorDefault(name);
    else
        changeBGColorError(name);
    
    return isValid;
}
//-->

function validateOnlyLetters(name, length)
{
    var data = $(name).val();
    var isValid = true;
    if (length > 0)
    {
        if (!(data.length <= length))
        {
            isValid = false;
        }
    }
    if (isValid)
    {
        var myRegExp = /^[A-Za-z]+$/;
        var onlyLetters = data.match(myRegExp);
        if (onlyLetters == null)
        {
            isValid = false;
        }
    }
    if (isValid)
        changeBGColorDefault(name);
    else
        changeBGColorError(name);
    return isValid;
}

function validateOnlyNumber(name, length)
{
    var data = $(name).val();
    var isValid = true;
    if (length > 0)
    {
        if (!(data.length <= length))
        {
            isValid = false;
        }
    }
    if (isValid)
    {
        var myRegExp = /^\d+$/;
        var onlyNumber = data.match(myRegExp);
        if (onlyNumber == null)
        {
            isValid = false;
        }
    }
    if (isValid)
        changeBGColorDefault(name);
    else
        changeBGColorError(name);
    return isValid;
}

function validateDecimal(name, length) {
    var data = $(name).val();
    var isValid = true;
    if (length > 0) {
        if (!(data.length <= length)) {
            isValid = false;
        }
    }
    if (isValid) {
        var myRegExp = /^([0-9]*|\d*\.\d{1}?\d*)$/;
        var onlyDecimal = data.match(myRegExp);
        if (onlyDecimal == null) {
            isValid = false;
        }
    }
    if (isValid)
        changeBGColorDefault(name);
    else
        changeBGColorError(name);
    return isValid;
}

function changeBGColorError(name)
{
    $(name).css("background-color", "#FFBABA");
}

function changeBGColorDefault(name)
{
    $(name).css("background-color", "#FFFFFF");
}

function validatePercent(name)
{
    var data = $(name).val();
    var isValid = true;
    var valInt = parseInt(data);
    if (!(valInt >= 5 && valInt <= 500))
    {
        isValid = false;
    }
    if (isValid)
    {
        var myRegExp = /^\d{1,5}(\.\d{1,2})?$/;
        var onlyNumber = data.match(myRegExp);
        if (onlyNumber == null)
        {
            isValid = false;
        }
    }
    if (isValid)
        changeBGColorDefault(name);
    else
        changeBGColorError(name);
    return isValid;
}

function validateCurrency(name)
{
    var data = $(name).val();
    var isValid = true;
    var valInt = parseInt(data);

    if (isValid)
    {
        var myRegExp = /^\$?([1-9]{1}[0-9]{0,2}(\,[0-9]{3})*(\.[0-9]{0,2})?|[1-9]{1}[0-9]{0,}(\.[0-9]{0,2})?|0(\.[0-9]{0,2})?|(\.[0-9]{1,2})?)$/;
        var onlyNumber = data.match(myRegExp);
        if (onlyNumber == null)
        {
            isValid = false;
        }
    }
    if (isValid)
        changeBGColorDefault(name);
    else
        changeBGColorError(name);
    return isValid;
}

function validateUID(name) {
    var data = $(name).val();
    if (data == null || data == "" || data.length > 8) {
        changeBGColorError(name);
        return false;
    }
    else {
        changeBGColorDefault(name);
        return true;
    }
}

function validate8CharUID(name) {
    var data = $(name).val().trim();
    if (data.length != 8) {
        changeBGColorError(name);
        return false;
    }
    else {
        changeBGColorDefault(name);
        return true;
    }
}

function validateZipDecimal(name, length) {
    var data = $(name).val();
    var isValid = true;
    if (length > 0) {
        if (!(data.length == length)) {
            isValid = false;
        }
    }
    if (isValid) {
        var myRegExp = /^([0-9]*|\d*\.\d{1}?\d*)$/;
        var onlyDecimal = data.match(myRegExp);
        if (onlyDecimal == null) {
            isValid = false;
        }
    }
    if (isValid)
        changeBGColorDefault(name);
    else
        changeBGColorError(name);
    return isValid;
}

function validateLatitudeAndLongitude(name) {
    var data = $(name).val();
    var isValid = true;

    if (data == "") {
        isValid = false;
    }

    if (isValid) {
        myRegExp = /^[-,0-9]+([\.][0-9]+)?$/
        var onlyDecimal = data.match(myRegExp);
        if (onlyDecimal == null) {
            isValid = false;
        }
    }

    if (isValid)
        changeBGColorDefault(name);
    else
        changeBGColorError(name);
    return isValid;
}

function validateLatitude(name) {
    var data = parseInt($(name).val());
    var isTrue = true;
    if (validateLatitudeAndLongitude(name)) {
        if (!(-90 <= data && data <= 90)) {
            isTrue = false;
        }
    }
    else {
        isTrue = false;
    }

    if (isTrue) {
        changeBGColorDefault(name);

    }
    else {
        changeBGColorError(name);
    }

    return isTrue;
}

function validateLongitude(name) {
    var data = parseInt($(name).val());
    var isTrue = true;
    if (validateLatitudeAndLongitude(name)) {
        if (!(-180 <= data && data <= 180)) {
            isTrue = false;
        }
    }
    else {
        isTrue = false;
    }

    if (isTrue) {
        changeBGColorDefault(name);

    }
    else {
        changeBGColorError(name);
    }

    return isTrue;
}

function validateCreateTowerList() {
    var check = true;
    var UIDerror = false;
    var latError = false;
    var lngError = false;
    var zipError = false;

    // Dropdown Check
    if (!validateDropdownField("#MainContent_ddlRegionName")) check = false;
    if (!validateDropdownField("#MainContent_ddlMarketName")) check = false;
    if (!validateDropdownField("#MainContent_ddlState")) check = false;

    // Textbox UID Check
    if (!validate8CharUID("#MainContent_txtSiteID")) {
        UIDerror = true;
        check = false;
    }

    // City
    if (!validateRequiredField("#MainContent_txtCity")) check = false;

    // Zip
    if (validateRequiredField("#MainContent_txtZip")) {
        if (!validateZipDecimal("#MainContent_txtZip", 5)) {
            zipError = true;
            check = false;
        }
    }
    else {
        check = false;
    }

    // County
    if (!validateRequiredField("#MainContent_txtCounty")) check = false;

    // Site Name
    if (!validateRequiredField("#MainContent_txtSiteName")) check = false;

    // Latitude
    if (!validateLatitude("#MainContent_txtLatitude")) {
        latError = true;
        check = false;
    }

    // Longitude
    if (!validateLongitude("#MainContent_txtLongitude")) {
        lngError = true;
        check = false;
    }

    // Address
    if (!validateRequiredField("#MainContent_txtAddress")) check = false;

    if (!check) {
        var strError = messageValidateError;
        if (UIDerror) {
            strError += messageValidate8CharUID;
        }

        if (zipError) {
            strError += messageValidateZip;
        }

        if (latError) {
            strError += messageValidateLatitude;
        }

        if (lngError) {
            strError += messageValidateLongitude;
        }

        $("#message_alert").html("");
        showTowerListMessageAlertByName("error", strError, "#message_alert");
        return false;
    }
    else {
        $("#MainContent_btnSubmitCreateTowerList").val("Processing...");
        $("#MainContent_btnSubmitCreateTowerList").attr("disabled", true);
        return true;
    }
}


function validateSaveSite() {
    var check = true;
    var UIDerror = false;
    var latError = false;
    var lngError = false;
    var zipError = false;

    // City
    if (!validateRequiredField("#MainContent_txtCity")) check = false;

    // Zip
    if (validateRequiredField("#MainContent_txtZip")) {
        if (!validateZipDecimal("#MainContent_txtZip", 5)) {
            zipError = true;
            check = false;
        }
    }
    else {
        check = false;
    }

    // County
    if (!validateRequiredField("#MainContent_txtCounty")) check = false;

    // Latitude
    if (!validateLatitude("#MainContent_txtLatitude")) {
        latError = true;
        check = false;
    }

    // Longitude
    if (!validateLongitude("#MainContent_txtLongitude")) {
        lngError = true;
        check = false;
    }

    // Address
    if (!validateRequiredField("#MainContent_txtAddress")) check = false;

    if (!check) {
        var strError = messageValidateError;
        if (UIDerror) {
            strError += messageValidate8CharUID;
        }

        if (zipError) {
            strError += messageValidateZip;
        }

        if (latError) {
            strError += messageValidateLatitude;
        }

        if (lngError) {
            strError += messageValidateLongitude;
        }

        $("#message_alert").html("");
        showMessageAlert("error", strError);

        $('.close').click(function () {
            var msgalert = $(this).parent();
            $(msgalert).fadeOut('slow', function () {
                //resize dailog when show error message
                $(".ms").remove();
                resizeAddSiteDialog(0, true);
                $("#HiddenDialogHeightFlag").val('');
            });
        });
        //resize dailog when show error message
        var msgHeight = $("#message_alert").height();
        if ($("#HiddenDialogHeightFlag").val() != 'true') {
            resizeAddSiteDialog(msgHeight + 20);
        }
        $("#HiddenDialogHeightFlag").val('true');

        return false;
    }
    else {
        $("#MainContent_btn_Save_RogueEquip").val("Processing...");
        $("#MainContent_btn_Save_RogueEquip").attr("disabled", true);
        return true;
    }
}

// Validate Save Document
function validateRenameBeforeUpload(objId) {
    
    var valid = true;
    var strError = messageValidateError;

    // File Name
    if (!validateRequiredField("#" + objId)) {
        valid = false;
    } else if (!validateFileName("#" + objId, 5)) {
        strError = messageValidateFileNameMinimumLength;
        valid = false;
    }
    if (valid) {
        if (!validateSpecialChar("#" + objId))
            valid = false;
        strError = messageValidateFileName;
    }

    // Throw Error
    if (!valid) {
        $("#message_alert").html("");
        showMessageAlert("error", strError);
        changeBGColorError("#" + objId);

    } 

    return valid;
}
//-->

$(document).ready(function ()
{
    var siteid_length = $("#lblSiteID").text().length;
    if (siteid_length == 0)
    {
        $("#lblSiteID").hide();
    }
});

