﻿$(document).ready(function () {

    var url = window.location.toString();
    var query_string = url.split("?");
    var tagetUrl = "DMS/";
    var scriptPath = '/DMS/FolderTree.aspx';

    if (GetValueQueryString('action') == "Move") {
        var data = [];
        var currentFile = $('#MainContent_lblUploadingFolderLocation').text();
        if (GetValueQueryString('moveType') == "file") {
            //move file
            var locationPath = getCurrentLocationPath(currentFile);
            scriptPath = '/DMS/FolderTree.aspx?currentFile=' + currentFile + '&site=' + locationPath[0] + "&action=move";
            data = {
                root: '',
                script: scriptPath,
                hiddenObj: 'hidBasePath',
                LabelObj: 'lblMovingFolderLocation',
                expandedFolders: locationPath,
                site: locationPath[0],
                action: "move"
            };
        }
        else {
            //move folder
            scriptPath = "/DMS/FolderTree.aspx?currentFile=" + currentFile;
            data = {
                root: "",
                script: "/DMS/FolderTree.aspx?currentFile=" + currentFile,
                hiddenObj: 'hidBasePath',
                LabelObj: 'lblMovingFolderLocation'
            };
        }

        $('#jqueryFileTree').fileTree(
        {
            root: '',
            expandedFolders: locationPath,
            script: scriptPath,
            hiddenObj: 'hidBasePath',
            LabelObj: 'lblMovingFolderLocation',
            multiFolder: false
        });
    }
    else if (GetValueQueryString('action') == "Sel") {
        var currentFile = $('#MainContent_lblUploadingFolderLocation').text();
        var locationPath = GetValueQueryString('siteID').replace(/^\s+|\s+$/g, '');
        if (locationPath.length > 8) {
            locationPath = locationPath.substring(0, 8);
        }
        locationPath = locationPath + '/';
        var ajax_url = "/DMS/FolderTree.aspx";
        if (locationPath != '') {
            ajax_url += "?site=" + locationPath;
            var field = GetValueQueryString('field');
            if (locationPath != '') {
                ajax_url += "&field=" + field;
            }
        }

        $('#jqueryFileTree').fileTree({ root: '', script: ajax_url, hiddenObj: 'hidBasePath', LabelObj: 'lblMovingFolderLocation', multiFolder: false });
    }

    else if (GetValueQueryString('action') == "Upl") {
        if ($('#jqueryFileTree').length > 0) {
            var field = parent.parent.$('#hidField').val();
            var locationPath = GetValueQueryString('siteID').replace(/^\s+|\s+$/g, '');
            if (locationPath == '') {
                locationPath = GetValueQueryString('site').replace(/^\s+|\s+$/g, '');
            }
            if (locationPath.length > 8) {
                locationPath = locationPath.substring(0, 8)
            }
            locationPath = locationPath + '/';
            var ajax_url = "/DMS/FolderTree.aspx";
            if (locationPath != '') {
                ajax_url += "?site=" + locationPath;
                if (locationPath != '') {
                    ajax_url += "&field=" + field + "&action=Upl";
                }
            }

            $('#jqueryFileTree').fileTree({ root: '', script: ajax_url, hiddenObj: 'hidBasePath', LabelObj: 'lblUploadingFolderLocation', multiFolder: false });
        }
    }
});

function GetValueQueryString(key, default_) {
    if (default_ == null) default_ = "";
    key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + key + "=([^&#]*)");
    var qs = regex.exec(window.location.href);
    if (qs == null)
        return default_;
    else
        return qs[1];
}

function getCurrentLocationPath(str) {
    var tmp = str.split('/');
    var returnVal = new Array();
    var count = 0;
    if (tmp.length > 1) {
        for (x = 1; x < tmp.length; x++) {
            if (tmp[x] != "") {
                if (x == 1) {
                    returnVal[count] = tmp[x] + "/";
                    count++;
                }
                else {
                    var s = "";
                    for (y = 0; y <= x; y++) {
                        if (tmp[y] != "") {
                            s += tmp[y] + "/";
                        }
                    }
                    returnVal[count] = s;
                    count++;
                }
            }
        }
    }
    else {
        returnVal[0] = str;
    }
    return returnVal;
}

