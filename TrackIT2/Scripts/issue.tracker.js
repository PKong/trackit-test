﻿/** Issue Tracker **/

    // Global Var
    // TODO: refactor and tidy a little.
    var ADD_NEW_DISC_ITEM_DEFAULT_TEXT = "Enter your new discussion item text here.";
    var ADD_NEW_DISC_ITEM_ERROR_TEXT = "Please complete the discussion ";
    var _itemsTableClone;

    // Setup Issue Discussion Items
    function setupIssueDiscussionItems() {

        // Reset
        resetAddDiscussionItemInputs();

        // Bind Table Sorting
        _itemsTableClone = $(".all-comments-table").clone();
        $(".all-comments-table").tablesorter({ headers: { 3: { sorter: false}} });

        // Button - Add Discussion Item
        $('#add-discussion-item').bind('click', function () { handleAddDiscussionItemShow(); });

        // Bind Text Input Action Item
        bindTextInputActionItem();

        // Bind Delete Discussion Button Actions
        bindDeleteDiscussionButtonActions();
    }
    //-->

    // Reset Discussion Items Table (in case re-sorted)
    function resetDiscussionItemsTable(blSortable) {

        // Clone-zone
        $('#discussion-repeater-container').append(_itemsTableClone.clone());
        $('#discussion-repeater-container').children(":first").remove();

        if (blSortable) $('#discussion-repeater-container').children(":first").tablesorter({ headers: { 3: { sorter: false}} });

        // Bind Text Input Action Item
        bindTextInputActionItem();
    }
    //-->

    // Bind Text Input Action Item
    function bindTextInputActionItem() {

        // Text Input - Add Discussion Item
        $('textarea#txtNewDiscItemActionItem').bind('focus', function () {
            if ($(this).val() == ADD_NEW_DISC_ITEM_DEFAULT_TEXT) {
                changeBGColorDefault(this);
                $(this).val('');
                $(this).removeClass('comment-text-unselected');
                $(this).removeClass('italic');
            }
        });
        //
        $('textarea#txtNewDiscItemActionItem').bind('blur', function () {
            if ($(this).val() == '' || $(this).val().length == 0) {
                $(this).addClass('comment-text-unselected');
                $(this).addClass('italic');
                $(this).val(ADD_NEW_DISC_ITEM_DEFAULT_TEXT);
            }
        });
        //>
    }
    //-->

    // Handle Add Discussion Item Show
    function handleAddDiscussionItemShow() {

        // Reset table (in case re-sorted)
        resetDiscussionItemsTable(false);

        // Show Add Form & Setup Data
        $('#add-discussion-item').hide();
        $('#tr-new-discussion-item').show();
        $('.td-new-discussion-item').removeAttr('style');
        $('.div-new-discussion-item').slideDown(function () {
            $('.div-add-discussion-action-btns').fadeIn("fast");
        });
        $('#lblNewDiscItemDateCreated').text(new Date().formatMMDDYYYY());
        //>

    }
    //-->

    // Handle Add Discussion Item Hide
    function handleAddDiscussionItemHide() {

        $('.inline-error-add-disc-item').hide();
        $('.div-add-discussion-action-btns').hide();
        $('.div-new-discussion-item').slideUp(function () {
            $('#tr-new-discussion-item').hide();
            $('.td-new-discussion-item').attr('style', 'border-bottom: none;');
            resetAddDiscussionItemInputs();
            $('#add-discussion-item').fadeIn("fast", function () {
                // Re-enable table sorting
                resetDiscussionItemsTable(true);
            });
        });
    }
    //-->

    // handle Add Discussion Item Post
    function handleAddDiscussionItemPost() {

        // We have a Type?
        if ($('#ddlNewDiscItemType')[0].selectedIndex == 0) {

            changeBGColorError($('#ddlNewDiscItemType'));
            displayInlineItemError(ADD_NEW_DISC_ITEM_ERROR_TEXT + "Type select input.");
            return false;
        } else {
            changeBGColorDefault($('#ddlNewDiscItemType'));
            //hideInlineItemError();
        }
        //>

        // We have user text input?
        var txtActionItem = $('textarea#txtNewDiscItemActionItem');
        if (($(txtActionItem).val() == ADD_NEW_DISC_ITEM_DEFAULT_TEXT) || ($(txtActionItem).val() == '') || ($(txtActionItem).val().length == 0)) {

            changeBGColorError(txtActionItem);
            displayInlineItemError(ADD_NEW_DISC_ITEM_ERROR_TEXT + "Item text input.");
            return false;
        } else {
            changeBGColorDefault(txtActionItem);
            hideInlineItemError();
        }
        //>

        // Post the item.
        $('.div-add-discussion-action-btns').hide();
        showLoadingSpinner('h1-discussion');
        return true;
    }
    //-->

    // Reset Add Discussion Item Inputs
    function resetAddDiscussionItemInputs() {

        $('#add-discussion-item').show();
        $('#ddlNewDiscItemType')[0].selectedIndex = 0;
        var txtActionItem = $('textarea#txtNewDiscItemActionItem');
        $(txtActionItem).addClass('comment-text-unselected');
        $(txtActionItem).addClass('italic');
        $(txtActionItem).val(ADD_NEW_DISC_ITEM_DEFAULT_TEXT);
        changeBGColorDefault(txtActionItem);
        changeBGColorDefault($('#ddlNewDiscItemType'));
        hideLoadingSpinner('h1-discussion');
    }
    //-->

    // Display Inline Item Error
    function displayInlineItemError(msg) {

        var inlError = $('.inline-error-add-disc-item');
        $(inlError).text(msg);
        $(inlError).fadeIn();
    }
    //-->

    // Hide Inline Item Error
    function hideInlineItemError() {

        $('.inline-error-add-disc-item').hide();
    }
    //-->

    // Bind Delete Discussion Button Actions
    function bindDeleteDiscussionButtonActions() {

        // Delete Modal Dialog
        var delDialogHtml = '<div id="dialog-confirm-comment-delete" title="Delete this item?"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This discussion item will be permanently deleted and cannot be recovered. Are you sure?</p></div>';
        $('.btn-comment-delete').click(function (e) {
            e.preventDefault();
            $('#dialog-confirm-comment-delete').remove();
            $(delDialogHtml).dialog({
                open: function (type, data) {

                    $(this).parent().appendTo("form[0]");
                },
                buttons: {
                    'Confirm': function () {

                        //debugger;

                        $(this).dialog('close');
                        showLoadingSpinner('h1-discussion');
                        __doPostBack('upDiscussion', e.target.alt);
                        return true;

                    },
                    'Cancel': function () {
                        $(this).dialog('close');
                        return false;
                    }
                }
            });
            $('#dialog-confirm-comment-delete').dialog('open');
        });
        //>
    }

    // Setup List Create Modal
    function setupListCreateIssueModalWithID(title, id) {
        HasId = true;
        site_id = id;
        // Create New Dialog
        var optionsCreate = {
            autoOpen: false,
            width: ListViewConstants.dialogWidth,
            modal: true,
            title: title,
            open: function (event, ui) {
                $('#modal-loading').show();
                $('#modalIframeId').load(function () {
                    $('#modal-loading').fadeOut();
                });
            },
            close: function (event, ui) {
                generateInfoLinkDocument();
            }
        };
        //
        $("#modalExternal").dialog(optionsCreate);
        //
        $('#modalBtnExternal').click(function () {
            window.onbeforeunload = function () { };
            showCreateDialog();
        });
    }
    //-->
/** /Issue Tracker **/
