

function FrontExportClick(tableName) {
    var data = new Array();

    if (ValidateFilterBeforeExport()) {
        $("#MainContent_exportButton").val("Processing...");
        $("#MainContent_exportButton").attr("disabled", true);

        $('#hiddenDownloadFlag').val("processing");
        $('#exportLoadSpinner').attr('display', 'block');
        $('#exportLoadSpinner').activity({ segments: 8, width: 3, space: 0, length: 3, speed: 1.5, align: 'right' });
        var targetTable = '#MainContent_' + tableName;
        if ($(targetTable).length > 0) {
            switch (tableName) {
                case "fgApplications":
                    data.push({ name: "sortname", value: MainContent_fgApplications_fgtable.p.sortname });
                    data.push({ name: "sortdi", value: MainContent_fgApplications_fgtable.p.sortorder });
                    data.push({ name: "sortItem", value: MainContent_fgApplications_fgtable.p.rp.toString() });
                    data.push({ name: "fileName", value: "Lease Applications" });
                    break;
                case "fgSites":
                    data.push({ name: "sortname", value: MainContent_fgSites_fgtable.p.sortname });
                    data.push({ name: "sortdi", value: MainContent_fgSites_fgtable.p.sortorder });
                    data.push({ name: "sortItem", value: MainContent_fgSites_fgtable.p.rp.toString() });
                    data.push({ name: "fileName", value: "Sites" });
                    break;
                case "fgPackages":
                    data.push({ name: "sortname", value: MainContent_fgPackages_fgtable.p.sortname });
                    data.push({ name: "sortdi", value: MainContent_fgPackages_fgtable.p.sortorder });
                    data.push({ name: "sortItem", value: MainContent_fgPackages_fgtable.p.rp.toString() });
                    data.push({ name: "fileName", value: "Site Data Packages" });
                    break;
                case "fgModifications":
                    data.push({ name: "sortname", value: MainContent_fgModifications_fgtable.p.sortname });
                    data.push({ name: "sortdi", value: MainContent_fgModifications_fgtable.p.sortorder });
                    data.push({ name: "sortItem", value: MainContent_fgModifications_fgtable.p.rp.toString() });
                    data.push({ name: "fileName", value: "Tower Modifications" });
                    break;
                case "fgIssueTracker":
                    data.push({ name: "sortname", value: MainContent_fgIssueTracker_fgtable.p.sortname });
                    data.push({ name: "sortdi", value: MainContent_fgIssueTracker_fgtable.p.sortorder });
                    data.push({ name: "sortItem", value: MainContent_fgIssueTracker_fgtable.p.rp.toString() });
                    data.push({ name: "fileName", value: "Issue Tracker" });
                    break;
                case "fgLeaseAdmin":
                    data.push({ name: "sortname", value: MainContent_fgLeaseAdmin_fgtable.p.sortname });
                    data.push({ name: "sortdi", value: MainContent_fgLeaseAdmin_fgtable.p.sortorder });
                    data.push({ name: "sortItem", value: MainContent_fgLeaseAdmin_fgtable.p.rp.toString() });
                    data.push({ name: "fileName", value: "Lease Admin Tracker" });
                    break;
                case "fgTowerList":
                    data.push({ name: "sortname", value: MainContent_fgTowerList_fgtable.p.sortname });
                    data.push({ name: "sortdi", value: MainContent_fgTowerList_fgtable.p.sortorder });
                    data.push({ name: "sortItem", value: MainContent_fgTowerList_fgtable.p.rp.toString() });
                    data.push({ name: "fileName", value: "Tower List" });
                    break;
                default:
                    data.push({ name: "sortname", value: "site_uid" });
                    data.push({ name: "sortdi", value: "asc" });
                    data.push({ name: "sortItem", value: "20" });
                    data.push({ name: "fileName", value: "TrackIt Export" });
                    break;
            }

            GetColumn("MainContent_" + tableName);
        }
        else {
            if (tableName != "fgIssueTracker") {
                data.push({ name: "sortname", value: "site_uid" });
                data.push({ name: "sortdi", value: "asc" });
                data.push({ name: "sortItem", value: "20" });
                data.push({ name: "fileName", value: "TrackIt Export" });
            }
            else if ("fgTowerList") {
                data.push({ name: "sortname", value: MainContent_fgTowerList_fgtable.p.sortname });
                data.push({ name: "sortdi", value: MainContent_fgTowerList_fgtable.p.sortorder });
                data.push({ name: "sortItem", value: MainContent_fgTowerList_fgtable.p.rp.toString() });
                data.push({ name: "fileName", value: "Tower List Export" });
            }
            else {
                data.push({ name: "sortname", value: "id" });
                data.push({ name: "sortdi", value: "asc" });
                data.push({ name: "sortItem", value: "20" });
                data.push({ name: "fileName", value: "TrackIt Export" });
            }
        }

        $.merge(data, arrayOfFilters);
        var dataString = JSON.stringify(data);
        $('#hiddenSearchValue').val(dataString);
        startSpinner();
    }
    else {
        alert('Please fix invalid date(s) before continuing.');
    }
}

function ValidateFilterBeforeExport() {
    if (
           (typeof valAppReceivedFrom != 'undefined' && valAppReceivedFrom.isvalid == false) ||
           (typeof valAppReceivedTo != 'undefined' && valAppReceivedTo.isvalid == false) ||
           (typeof valAppFeeReceivedFrom != 'undefined' && valAppFeeReceivedFrom.isvalid == false) ||
           (typeof valAppFeeReceivedTo != 'undefined' && valAppFeeReceivedTo.isvalid == false) ||
           (typeof valSiteOnAirDateFrom != 'undefined' && valSiteOnAirDateFrom.isvalid == false) ||
           (typeof valSiteOnAirDateTo != 'undefined' && valSiteOnAirDateTo.isvalid == false) ||
           (typeof valSDPRequestDateFrom != 'undefined' && valSDPRequestDateFrom.isvalid == false) ||
           (typeof valSDPRequestDateTo != 'undefined' && valSDPRequestDateTo.isvalid == false) ||
           (typeof valCurrentPackedRcvdFrom != 'undefined' && valCurrentPackedRcvdFrom.isvalid == false) ||
           (typeof valCurrentPackedRcvdTo != 'undefined' && valCurrentPackedRcvdFrom.isvalid == false) ||
           (typeof valProjectCompleteFrom != 'undefined' && valProjectCompleteFrom.isvalid == false) ||
           (typeof valProjectCompleteTo != 'undefined' && valProjectCompleteTo.isvalid == false)
          ) {
        return false;
    }
    else {
        return true;
    }
}


var fileDownloadCheckTimer;
function startSpinner() {
    var token = new Date().getTime(); 
    $('#hiddenDownloadFlag').val(token);
    fileDownloadCheckTimer = window.setInterval(function () {
        var cookieValue = getCookie('fileDownloadToken');
        if (cookieValue == token) {
            finishDownload();
        }
    }, 1000);
}
function finishDownload() {
    window.clearInterval(fileDownloadCheckTimer);
    $('#exportLoadSpinner').activity(false);
    $('#exportLoadSpinner').attr('display', 'none');
    del_cookie('fileDownloadToken');
    $('#MainContent_exportButton').removeAttr("disabled");
    $("#MainContent_exportButton").val("Export to Excel");
    $('#hiddenDownloadFlag').val('null');
}

function getCookie(c_name) {
    var i, x, y, ARRcookies = document.cookie.split(";");
    for (i = 0; i < ARRcookies.length; i++) {
        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
        x = x.replace(/^\s+|\s+$/g, "");
        if (x == c_name) {
            return unescape(y);
        }
    }
}

function del_cookie(name) {
    document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

function GetColumn(targetTable) {
    var table = document.getElementById(targetTable);
    var headCells = table.getElementsByTagName("th");
    var data = new Array();
    if (targetTable == "MainContent_fgPackages") {
        for (var i = 0; i < headCells.length; i++) {
            if (headCells[i].style.display != "none") {
                if (headCells[i].innerText != undefined && headCells[i].abbr != "") {
                    if (headCells[i].abbr == "user6.last_name") {
                        data.push({ name: "user6_last_name", value: "TMO PM Emp" });
                    }
                    else if (headCells[i].abbr == "user7.last_name") {
                        data.push({ name: "user7_last_name", value: "TMO Spec Cur Emp" });
                    }
                    else {
                        data.push({ name: headCells[i].abbr, value: headCells[i].innerText });

                    }
                }
                else if (headCells[i].abbr != "") {
                    if (headCells[i].abbr == "user6.last_name") {
                        data.push({ name: "user6_last_name", value: "TMO PM Emp" });
                    }
                    else if (headCells[i].abbr == "user7.last_name") {
                        data.push({ name: "user7_last_name", value: "TMO Spec Cur Emp" });
                    }
                    else {
                        data.push({ name: headCells[i].abbr, value: headCells[i].textContent });
                    }
                }
            }
        }
    }
    else if (targetTable == "MainContent_fgModifications") {
        for (var i = 0; i < headCells.length; i++) {
            if (headCells[i].style.display != "none") {
                if (headCells[i].innerText != undefined && headCells[i].abbr != "") {
                    if (headCells[i].abbr == "customer.customer_name") {
                        data.push({ name: "customer_name", value: "Customer" });
                    }
                    else if (headCells[i].abbr == "tower_mod_statuses.tower_mod_status_name") {
                        data.push({ name: "tower_mod_status_name", value: "Tower Mod Status" });
                    }
                    else if (headCells[i].abbr == "tower_mod_types.tower_mod_type") {
                        data.push({ name: "tower_mod_type", value: "Tower Mod Type" });
                    }
                    else if (headCells[i].abbr == "tower_mod_pos.tower_mod_phase_types.phase_name") {
                        data.push({ name: "phase_name", value: "Current Phase" });
                    }
                    else if (headCells[i].abbr == "tower_mod_pm_user.last_name") {
                        data.push({ name: "tower_mod_pm_user_name", value: "Tower Mod PM" });
                    }
                    else if (headCells[i].abbr == "tower_mod_pos.ebid_req_rcvd_date") {
                        data.push({ name: "dateCurPackageRcvd", value: "Packet Rcvd" });
                    }
                    else if (headCells[i].abbr == "site.rogue_equipment_yn") {
                        data.push({ name: "rogue_equipment_yn", value: "RE" });
                    }
                    else {
                        data.push({ name: headCells[i].abbr, value: headCells[i].innerText });
                    }
                }
                else if (headCells[i].abbr != "") {
                    if (headCells[i].abbr == "customer.customer_name") {
                        data.push({ name: "customer_name", value: "customer_name" });
                    }
                    else if (headCells[i].abbr == "tower_mod_statuses.tower_mod_status_name") {
                        data.push({ name: "tower_mod_status_name", value: "tower_mod_status_name" });
                    }
                    else if (headCells[i].abbr == "tower_mod_types.tower_mod_type") {
                        data.push({ name: "tower_mod_type", value: "tower_mod_type" });
                    }
                    else if (headCells[i].abbr == "tower_mod_pos.tower_mod_phase_types.phase_name") {
                        data.push({ name: "phase_name", value: "phase_name" });
                    }
                    else if (headCells[i].abbr == "tower_mod_pm_user.last_name") {
                        data.push({ name: "tower_mod_pm_user_name", value: "tower_mod_pm_user_name" });
                    }
                    else if (headCells[i].abbr == "tower_mod_pos.ebid_req_rcvd_date") {
                        data.push({ name: "dateCurPackageRcvd", value: "dateCurPackageRcvd" });
                    }
                    else if (headCells[i].abbr == "site.rogue_equipment_yn") {
                        data.push({ name: "rogue_equipment_yn", value: "rogue_equipment_yn" });
                    }
                    else {
                        data.push({ name: headCells[i].abbr, value: headCells[i].textContent });
                    }
                }
            }
        }
    }
    else if (targetTable == "MainContent_fgIssueTracker") {
        for (var i = 0; i < headCells.length; i++) {
            if (headCells[i].style.display != "none") {
                if (headCells[i].innerText != undefined && headCells[i].abbr != "") {
                    if (headCells[i].abbr == "issue_priority_types.id") {
                        data.push({ name: "issue_priority_types_id", value: "Priority" });
                    }
                    else if (headCells[i].abbr == "issue_statuses.id") {
                        data.push({ name: "issue_statuses_id", value: "Status" });
                    }
                    else {
                        data.push({ name: headCells[i].abbr, value: headCells[i].innerText });
                    }
                }
                else if (headCells[i].abbr != "") {
                    if (headCells[i].abbr == "issue_priority_types.id") {
                        data.push({ name: "issue_priority_types_id", value: "Status" });
                    }
                    else if (headCells[i].abbr == "issue_statuses.id") {
                        data.push({ name: "issue_statuses_id", value: "Priority" });
                    }
                    else {
                        data.push({ name: headCells[i].abbr, value: headCells[i].textContent });
                    }
                }
            }
        }
    }
    else if (targetTable == "MainContent_fgTowerList") {
        for (var i = 0; i < headCells.length; i++) {
            if (headCells[i].style.display != "none") {
                if (headCells[i].innerText != undefined && headCells[i].abbr != "") {
                    if (headCells[i].abbr == "site.market_name") {
                        data.push({ name: "site_market_name", value: "Market Name" });
                    }
                    else if (headCells[i].abbr == "site.site_status_desc") {
                        data.push({ name: "site_status_desc", value: "Site Status Desc" });
                    }
                    else if (headCells[i].abbr == "trackit_list_statuses.list_status") {
                        data.push({ name: "trackit_list_statuses", value: "TrackiT" });
                    }
                    else if (headCells[i].abbr == "map_list_statuses.list_status") {
                        data.push({ name: "map_list_statuses", value: "Website Map" });
                    }
                    else {
                        data.push({ name: headCells[i].abbr, value: headCells[i].innerText });
                    }
                }
                else if (headCells[i].abbr != "") {
                    if (headCells[i].abbr == "site.market_name") {
                        data.push({ name: "site_market_name", value: "Market Name" });
                    }
                    else if (headCells[i].abbr == "site.site_status_desc") {
                        data.push({ name: "site_status_desc", value: "Site Status Desc" });
                    }
                    else if (headCells[i].abbr == "trackit_list_statuses.list_status") {
                        data.push({ name: "trackit_list_statuses", value: "TrackiT" });
                    }
                    else if (headCells[i].abbr == "map_list_statuses.list_status") {
                        data.push({ name: "map_list_statuses", value: "Website Map" });
                    }
                    else {
                        data.push({ name: headCells[i].abbr, value: headCells[i].textContent });
                    }
                }
            }
        }
    }
    else {
        for (var i = 0; i < headCells.length; i++) {
            if (headCells[i].style.display != "none") {
                if (headCells[i].innerText != undefined && headCells[i].abbr != "") {
                    data.push({ name: headCells[i].abbr, value: headCells[i].innerText });
                }
                else if (headCells[i].abbr != "") {
                    data.push({ name: headCells[i].abbr, value: headCells[i].textContent });
                }
            }
        }
    }
    var dataString = JSON.stringify(data);
    $('#hiddenColumnValue').val(dataString);
}

