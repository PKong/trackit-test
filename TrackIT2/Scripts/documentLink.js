﻿/** Constants **/
var DmlConstants = {
    dialogWidth: 430,
    dialogHeight: 550,
    dialogIframeMinWidth: 350,
    dialogIframeMinHeight: 550,
    dialogfolderSelectIframeIdMinWidth: 300,
    dialogfolderSelectIframeIdMinHeight: 350,
    modalLoadingMessage: "LOADING... PLEASE WAIT",
    modalUploadDocumentPageUrl: "/Dms/UploadDocument.aspx",
    modalRemoveDocumentPageUrl: "/Dms/RemoveDocument.aspx",
    actionDelete: "Delete",
    dialogDownloadingWidth: 200,
    dialogDownloadingHeight: 20,
    fileExistsErrorMessage: "Error: a file with this name already exists!",
    folderExistsErrorMessage: "Error: a folder with this name already exists!"
};
var DocLinkListViewConstants = {
    dialogWidth: 430,
    dialogHeight: 640,
    dialogIframeMinWidth: 350,
    dialogIframeMinHeight: 640,
    modalLoadingMessage: "LOADING... PLEASE WAIT",
    modalAddPageUrl: "/DMS/DocumentLinks.aspx"
};

var DocLinkItem = {
    type: "",
    typeID: 0,
    field: "",
    key: "",
    autoRename: false,
    newName: "",
    newNameDescription:"",
    newNameAppType:""
}

var ModalOpen = false;
var stepBtn2Link = "";
var messageWarringDupSelectFile = "Selected file has already been linked.";
var messageWarringNoSelectDel = "Please select document!";
var messageValidateSelectFile = "Target must be file.<br/>";
/** /Constants **/
$(document).ready(function () {
    createDocumentLink();
});


function createDocumentLink() {
    //Check multiple sites or not?
    var bIsMultiSites = false;
    if ($('#MainContent_spn_total_site').length > 0) {
        if ($('#MainContent_SiteID').length == 0) {
            bIsMultiSites = true;
        }
    }
    // check for issue tracker
    else if ($('#txtSiteID').length > 0) {
        if ($('#txtSiteID').val() == "") {
            bIsMultiSites = true;
        }
    }

    if (!bIsMultiSites) {
        $(".document-link").each(function () {
            var linkname = $(this).attr('id').replace(/ /g, '');

            // Reset dialog height
            if (linkname == "RogueEquipment") {
                resizeAddSiteDialog(0, true);
            }

            if (linkname != '') {
                var jsonData = [];
                //get hidden field data
                var jsonStr = $($(this).find('input')[$(this).find('input').length - 1]).val();

                if (jsonStr != null || jsonStr != "") {
                    jsonData = jQuery.parseJSON(jsonStr);
                }
                var prev_id = '';
                var listDocumentContents = null;
                //remove old elemnet 
                $(this).find("img").each(function () {
                    $(this).remove();
                });
                $(this).find("div").each(function () {
                    $(this).remove();
                });

                if (jsonData != null) {
                    if (jsonData.length > 0) {
                        for (i = 0; i < jsonData.length; i++) {
                            if ((jsonData[i].ID == 0) || (prev_id != jsonData[i].ID)) {
                                prev_id = jsonData[i].ID;
                                var margin_top_folder = '';
                                if (jsonData[i].ref_field == "EbidComment") {
                                    margin_top_folder = 'style="margin-bottom:70px;"';
                                }
                                var imageFolder = $('<img id="img_' + linkname + '" Title="Manage Document Links" AlternateText="Manage Document Links" ' +
                                  ' class="DocumentLinkFolder" src="/images/icons/DocumentLink.png" ' +
                                  ' href="javascript:void(0)" ' + margin_top_folder + ' />');

                                var magin_left = '120px';
                                if (parent.ModalOpen) {
                                    magin_left = '50px';
                                }
                                if ($('#lst_' + linkname + '_' + jsonData[i].ID).length == 0) {
                                    listDocumentContents = $('<div class="document-list" id="lst_' + linkname + '_' + jsonData[i].ID + '"  style="text-align:left;" ><ul style="margin-left:' + magin_left + ';list-style-type:disc" ></ul></div>');
                                }
                                else {
                                    listDocumentContents = $('#lst_' + linkname + '_' + jsonData[i].ID);
                                }

                                $(imageFolder).unbind("click");
                                var temp_field = jsonData[i].ref_field;
                                if ((temp_field != 'RogueEquipment') && (temp_field != 'Issue')) {
                                    $(imageFolder).bind("click", function () {
                                        showDocLinkDialog(temp_field);
                                    });
                                }
                                else {
                                    $(imageFolder).bind("click", function () {
                                        parent.showDocLinkDialog(temp_field);
                                    });
                                }
                                if ($("#img_" + linkname).length == 0) {
                                    $(this).append(imageFolder);
                                }
                                $(this).append(listDocumentContents);
                            }
                            var link_style = '';
                            if (parent.ModalOpen) {
                                link_style = "style='font-weight:normal;font-size:10pt;' ";
                            }
                            if ((jsonData[i].ref_key != "") && (jsonData[i].documentName != "")) {
                                if (listDocumentContents != null) {

                                    var tmpRefKey = jsonData[i].ref_key;
                                    var tmpRefField = decodeURIComponent(jsonData[i].ref_field);
                                    var checkObj = $('a[ref_key="' + tmpRefKey + '"][ref_field="' + tmpRefField + '"]');

                                    tmpRefKey = jsonData[i].ref_key.replace(/\//g, '%2f').replace(/ /g, '+');
                                    var checkObj1 = $('a[ref_key="' + tmpRefKey + '"][ref_field="' + tmpRefField + '"]');

                                    if (checkObj.length <= 0 && checkObj1.length <= 0) {

                                        var lidocument = $('<li style="word-wrap:break-word;margin-bottom:10px;font-weight:normal;">');
                                        var aDocument = '<a ' + link_style + ' href="#" onclick="OnDownloadDocClick(\'' + jsonData[i].ref_key + '\')" ref_field="' + jsonData[i].ref_field + '" ref_key="' + jsonData[i].ref_key + '"  >' + jsonData[i].documentName + '</a>';
                                        var imageEdit = '<span style="white-space:nowrap;width:auto;padding:0;"><span style="color:gray;width:auto;margin-left:5px;">(</span><a id="img_edit_' + tmpRefKey + '" class="EditDocumentLink" onclick="EditDocumentLinkClick(\''  + jsonData[i].ID + '\',\''+ jsonData[i].ref_key + '\',\'' + temp_field + '\')" ref_key="' + jsonData[i].ref_key + '">edit</a><span style="color:gray;width:auto;margin-left:5px;">)</span></span>';

                                        $(lidocument).append(aDocument);
                                        $(lidocument).append(imageEdit);
                                        $(listDocumentContents).find('ul').append(lidocument);

                                        // increase dialog height before close
                                        if (tmpRefField == "RogueEquipment") {
                                            resizeAddSiteDialog(30);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        });
    }
    
}

function EditDocumentLinkClick(id,ref_key, temp_field) {
    if ((temp_field != 'RogueEquipment') && (temp_field != 'Issue')) {
        showUploadDocumentDialogOnFieldEditClick(id,ref_key, temp_field);
    }
    else {
        parent.showUploadDocumentDialogOnFieldEditClick(id,ref_key, temp_field);
    }
}

function CreateSACommentsLinkDocument(jsonData) {
    var listDocumentContents = null;
    var prev_id = '';
    var bIsHasDocument = false;
    $("[id^=Comment_Field_inner_SA_]").each(function () {
        if ($(this).attr('sacommentid') == ('SA_' + jsonData[0].ID)) {
            var div_author = $(this).find('.author-info');
            var linkname = 'SA_' + jsonData[0].ref_field + '_' + jsonData[0].ID;
            $(div_author).find('#lst_' + linkname).each(function () {
                $(this).remove();
            });
            $(div_author).find('#img_' + linkname).each(function () {
                $(this).remove();
            });

            $(div_author).find('br').each(function () {
                $(this).remove();
            });

            for (i = 0; i < jsonData.length; i++) {
                if ((jsonData[i].ID == 0) || (prev_id != jsonData[i].ID)) {
                    prev_id = jsonData[i].ID;
                    var imageFolder = $('<img id="img_' + linkname + '"  ref_field="' + jsonData[i].ref_field + '"  ref_id="' + jsonData[i].ID + '" Title="Manage Document Links" AlternateText="Manage Document Links" ' +
                                                ' class="DocumentLinkFolder" style="margin-top:5px;margin-left:10px;cursor:pointer;" src="/images/icons/DocumentLink.png" ' +
                                                ' href="javascript:void(0)"  /> <br/>');
                    if ($('#lst_' + linkname).length == 0) {
                        listDocumentContents = $('<div class="document-list" id="lst_' + linkname + '" style="margin-top:10px;margin-left:30px;" ><ul style="margin-left:20px;" ></ul></div>');
                    }
                    else {
                        listDocumentContents = $('#lst_' + linkname);
                    }

                    $(imageFolder).unbind("click");
                    $(imageFolder).bind("click", function () {
                        showDocLinkDialog($(this).attr('ref_field'), $(this).attr('ref_id'));
                    });

                    if ($("#img_" + linkname).length == 0) {
                        $(div_author).append(imageFolder);
                    }
                    $(div_author).append(listDocumentContents);
                }
                if ((jsonData[i].ref_key != "") && (jsonData[i].documentName != "")) {
                    if (listDocumentContents != null) {
                        bIsHasDocument = true;

                        var tmpRefKey = jsonData[i].ref_key;
                        var tmpRefField = decodeURIComponent(jsonData[i].ref_field);
                        var checkObj = listDocumentContents.find('a[ref_key="' + tmpRefKey + '"][ref_field="' + tmpRefField + '"]');

                        tmpRefKey = jsonData[i].ref_key.replace(/\//g, '%2f').replace(/ /g, '+');
                        var checkObj1 = listDocumentContents.find('a[ref_key="' + tmpRefKey + '"][ref_field="' + tmpRefField + '"]');

                        if (checkObj.length <= 0 && checkObj1.length <= 0) {

                            var lidocument = $('<li style="word-wrap:break-word;margin-bottom:10px">');
                            var aDocument = '<a href="#" onclick="OnDownloadDocClick(\'' + jsonData[i].ref_key + ')" ref_field="' + jsonData[i].ref_field + '" ref_key="' + jsonData[i].ref_key + '">' + jsonData[i].documentName + '</a>';
                            var imageEdit = '<span style="white-space:nowrap;width:auto;padding:0;"><span style="color:gray;width:auto;margin-left:5px;margin-right:5px;">(</span><a id="img_edit_' + tmpRefKey + '" class="EditDocumentLink" onclick="showUploadDocumentDialogOnFieldEditClick(\'' + jsonData[i].ID + '\',\'' + jsonData[i].ref_key + '\',\'' + jsonData[i].ref_field + '\')" ref_key="' + jsonData[i].ref_key + '">edit</a><span style="color:gray;width:auto;margin-left:5px;">)</span></span>';

                            $(lidocument).append(aDocument);
                            $(lidocument).append(imageEdit);
                            $(listDocumentContents).find('ul').append(lidocument);
                        }
                    }
                }
            }
            if ((!bIsHasDocument) && ($(this).height() < 145)) {
                $(this).height(145);
            }
        }
    });
}

// Setup Link Document for comments
function CreateCommentLinkDocument(hidden_field, div_class, bottom_comments) {
    if (bottom_comments == null) {
        bottom_comments = false;

    }

    var jsonStr = $('#' + hidden_field).val();
    if (jsonStr != null || jsonStr != "") {
        jsonData = jQuery.parseJSON(jsonStr);
    }

    var currentID = "onload";

    if (jsonData != null) {
        if (jsonData.length > 0) {
            for (j = 0; j < jsonData.length; j++) {
                var prev_id = '';
                var listDocumentContents = null;
                var linkname = jsonData[j][0].ref_field + '_' + jsonData[j][0].ID;
                if (!bottom_comments) {
                    $('.' + div_class + jsonData[j][0].ID).empty();
                }
                if (jsonData[j][0].ref_field == "SA_Comments") {
                    CreateSACommentsLinkDocument(jsonData[j]);
                }
                if (!bottom_comments) {

                    for (i = 0; i < jsonData[j].length; i++) {
                        if ($('.' + div_class + jsonData[j][i].ID).length > 0) {

                            //check allcomments/right side comments
                            var image_float_style = '';
                            if ($('.' + div_class + jsonData[j][i].ID).parent().length > 0) {
                                if ($('.' + div_class + jsonData[j][i].ID).parent().hasClass('author-info')) {
                                    image_float_style = 'float:right;';
                                }
                                else {
                                    image_float_style = 'margin-left:10px';
                                }
                            }
                            if ((jsonData[j][i].ID == 0) || (prev_id != jsonData[j][i].ID)) {
                                prev_id = jsonData[j][i].ID;
                                var imageFolder = $('<img id="img_' + linkname + '"  ref_field="' + jsonData[j][i].ref_field + '"  ref_id="' + jsonData[j][i].ID + '" Title="Manage Document Links" AlternateText="Manage Document Links" ' +
                                                ' class="DocumentLinkFolder" src="/images/icons/DocumentLink.png" ' +
                                                ' href="javascript:void(0)"  style="cursor:pointer;' + image_float_style + '"  /><br/>');
                                if ($('#lst_' + linkname).length == 0) {
                                    listDocumentContents = $('<div class="document-list" id="lst_' + linkname + '" ><ul style="margin-left:40px;margin-top:10px;" ></ul></div>');
                                }
                                else {
                                    listDocumentContents = $('#lst_' + linkname);
                                }

                                $(imageFolder).unbind("click");
                                $(imageFolder).bind("click", function () {
                                    showDocLinkDialog($(this).attr('ref_field'), $(this).attr('ref_id'));
                                });

                                if ($("#img_" + linkname).length == 0) {
                                    $('.' + div_class + jsonData[j][i].ID).append(imageFolder);
                                }
                                $('.' + div_class + jsonData[j][i].ID).append(listDocumentContents);
                            }
                            if ((jsonData[j][i].ref_key != "") && (jsonData[j][i].documentName != "")) {
                                if (listDocumentContents != null) {
                                    // check old value

                                    var tmpRefKey = jsonData[j][i].ref_key;
                                    var tmpRefField = decodeURIComponent(jsonData[j][i].ref_field);
                                    var checkObj = $('.' + div_class + jsonData[j][0].ID).find('a[ref_key="' + tmpRefKey + '"][ref_field="' + tmpRefField + '"]');

                                    tmpRefKey = tmpRefKey.replace(/\//g, '%2f').replace(/ /g, '+');
                                    var checkObj1 = $('.' + div_class + jsonData[j][0].ID).find('a[ref_key="' + tmpRefKey + '"][ref_field="' + tmpRefField + '"]');

                                    if (checkObj.length <= 0 && checkObj1.length <= 0) {

                                        var lidocument = $('<li style="word-wrap:break-word;margin-bottom:10px;font-size:12px;">');
                                        var aDocument = '<a href="#" onclick="OnDownloadDocClick(\'' + jsonData[j][i].ref_key + '\')" ref_field="' + jsonData[j][i].ref_field + '" ref_key="' + jsonData[j][i].ref_key + '">' + jsonData[j][i].documentName + '</a>';
                                        var imageEdit = '<span style="white-space:nowrap;width:auto;padding:0;"><span style="color:gray;width:auto;margin-left:5px;margin-right:5px;">(</span><a id="img_edit_' + tmpRefKey + '" class="EditDocumentLink" onclick="showUploadDocumentDialogOnFieldEditClick(\''+ jsonData[j][i].ID + '\',\'' +jsonData[j][i].ref_key + '\',\'' + jsonData[j][i].ref_field + '\')" ref_key="' + jsonData[j][i].ref_key + '">edit</a><span style="color:gray;width:auto;margin-left:5px;">)</span></span>';

                                        $(lidocument).append(aDocument);
                                        $(lidocument).append(imageEdit);
                                        $(listDocumentContents).find('ul').append(lidocument);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

function setupFolderModal(title) {
    // Create New Dialog
    var optionsCreate = {
        autoOpen: false,
        width: DmlConstants.dialogWidth - 100,
        height:380,
        modal: true,
        title: title,
        open: function (event, ui) {
            $('#modal-loading-folderSelect').show();
            $('#modalfolderSelectIframeId').load(function () {
                $('#modal-loading-folderSelect').fadeOut();
            });
        }
    };
    //
    $("#modalFolderBrowse").dialog(optionsCreate);
    //
    parent.$('#stepbtn_1').css('display', 'none');

    $('#btnTargetBrowser').click(function () {
        $('#ui-dialog-title-folderSelect').html(title);
        parent.$('#hidUploadFlag').val("true");
        showFolderDialog(null);
    });
}
function showFolderDialog() {
    var basePath = $("#hidBasePath").val();
    $("#modalFolderBrowse").html('<iframe id="modalfolderSelectIframeId" scrolling="no" style="width:100%;height:40%;overflow:hidden;min-height:' + DmlConstants.dialogfolderSelectIframeIdMinHeight + 'px;min-width:' + DmlConstants.dialogfolderSelectIframeIdMinWidth + 'px;" marginWidth="0" marginHeight="0" frameBorder="0" scrolling="no" /><div id="modal-loading-folderSelect">' + DmlConstants.modalLoadingMessage + '</div>').dialog("open");

    var rootPath = "";
    var siteID = basePath;
    var document_type = parent.$('#hidType').val();
    if (document_type == "issue_tracker") {
        currentFile = $('#MainContent_lblUploadingFolderLocation').text() ;
        siteID = getQueryString(window.location.toString())[5];
        $('#hidUploadFlag').val("true");
    }
    else if (document_type == "sites") {
        siteID = $("#hidTypeID").val();
        $('#hidUploadFlag').val("true");
    }
    siteID = siteID.replace('/', '');
    $("#modalfolderSelectIframeId").attr("src", "/Dms/DocumentFolderLink.aspx?site=" + siteID + "&type=" + document_type + "&action=Upl");
    return false;}

// Setup Upload doc Modal
function setupDocLinkModal(bIsExist) {
    
    $('#uploadDocBtn').click(function () {
        showUploadDocumentLinkDialog(null);
    });

    $('#selectDocBtn').click(function () {
        showSelectDocumentLinkDialog(null);
    });

    // Check file
    if (bIsExist == "True") {
        $(".removelink").css('display', 'block');
        $('#removeDocBtn').click(function () {
            showRemoveDocumentLinkDialog(null);
        });
    }
    else {
        $(".removelink").css('display', 'none');
    }

    $('#stepbtn_1').css('display', 'none');
    $('#exitBtn').click(function () {
        parent.$('#modalDocument').dialog('close');
    });
}
//-->

function getSiteId() {
    var siteId = '';
    if (window.location.href.indexOf('allComment=true&type') > 0) {
        siteID = parent.$('#MainContent_lblSiteID').text();
    }
    else {
        if (parent.$('#MainContent_SiteID').length > 0) {
            siteID = parent.$('#MainContent_SiteID').text()
        }
        else if (parent.$('#MainContent_lblSiteID').length > 0) {
            siteID = parent.$('#MainContent_lblSiteID').text();
        }
        else if (parent.$('#MainContent_SiteID').length > 0) {
            siteID = parent.$('#MainContent_SiteID').text();
        }
    }
    return siteID;
}

function getSiteFromDocumentType(type) {
    var siteID = "";
    
    if (type == "lease_applications" || type == "structural_analyses" || type == "leaseapp_revision" || type == "lease_admin" ) {
        siteID = getSiteId();
    }
    else if (type == "site_data_packages" || type == "tower_modification" || type == "tower_modification_general") {
        siteID = getSiteId();
        $('#hidUploadFlag').val("true");
    }
    else if (type == "issue_tracker") {
        siteID = parent.$('#MainContent_lblSiteID').text();
        $('#hidUploadFlag').val("true");
    }
    else if (type == "issue_action_items") {
        siteID = $('#MainContent_lblSiteID').text();
        $('#hidUploadFlag').val("true");
    }
    else if (type == "sites") {
        siteID = $("#hidTypeID").val();
        $('#hidUploadFlag').val("true");
    }

    siteID= siteID.replace(/^\s+|\s+$/g, '');
    if (siteID.length > 8) {
        siteID = siteID.substring(0, 8);
    }
    return siteID;
}

// Show Upload Document Dialog
function showUploadDocumentLinkDialog() {

    var basePath = $("#hidBasePath").val();
    DocLinkItem.type = $("#hidType").val();
    DocLinkItem.typeID = $("#hidTypeID").val();
    DocLinkItem.ref_field = $("#hidField").val();
    DocLinkItem.autoRename = $("#hidAutoRename").val();
    DocLinkItem.newName = urlencode($("#hidNewName").val());
    DocLinkItem.newNameDescription = urlencode($("#hidNewNameDescription").val());

    var tmpAppType = $("#hidNewNameAppType").val();
    if (tmpAppType == "Initial") {
        tmpAppType = "Init";
    }
    else if (tmpAppType == "Temporary") {
        //do nothing
    }
    else if (ConstAppTypeAliases.hasOwnProperty(tmpAppType.replace(/ /g, ''))) {
        tmpAppType = ConstAppTypeAliases[tmpAppType.replace(/ /g, '')].aliases.replace(/ /g, '');
    }

    DocLinkItem.newNameAppType =  urlencode(tmpAppType);

     // DocLinkItem.newNameAppType
    var siteID = getSiteFromDocumentType(DocLinkItem.type);

    var link = DmlConstants.modalUploadDocumentPageUrl + "?basePath=" + basePath + "&action=Upl&type=" + DocLinkItem.type + "&typeID=" + DocLinkItem.typeID + "&field=" + DocLinkItem.ref_field + "&siteID=" + siteID + "&autoRename=" + DocLinkItem.autoRename + "&newName=" + DocLinkItem.newName + "&newNameDescription=" + DocLinkItem.newNameDescription + "&newNameAppType=" + DocLinkItem.newNameAppType;
    $("#mainDocLink").css({ 'margin-top': '0px' });
    $("#mainDocLink").html('<iframe id="modalDmlIframeId" scrolling="no" style="width:100%;height:550px;overflow:hidden;min-height:550px;min-width:' + DmlConstants.dialogIframeMinWidth + 'px;" marginWidth="0" marginHeight="0" frameBorder="0" scrolling="no" /><div id="modal-loading-Dml">' + DmlConstants.modalLoadingMessage + '</div>').dialog("open");
    $("#modalDmlIframeId").attr("src", link);
    $('#stepbtn_2').val('Upload Document');
    $('#stepbtn_2').show();
    $('#hidUploadFlag').val('true');

    $('#modal-loading-Dml').show();
    $('#modalDmlIframeId').load(function () {
        $('#modal-loading-Dml').fadeOut();
        $('#modalDmlIframeId').contents().find('.asmSelect').each(function () {
            $(this).change(function () {
                var messsage_height = $('#modalDmlIframeId').contents().find('#message_alert').height();
                if (messsage_height > 0) {
                    messsage_height += 20;
                }
                $('#modalDmlIframeId').height($('#modalDmlIframeId').height() + 32);
               parent.parent.resizemodal($('#modalDmlIframeId').height() + 74 + messsage_height);

            });

            $(this).blur(function () {
                $('#modalDmlIframeId').contents().find('.asmListItemRemove').each(function () {
                    $(this).unbind('click');
                    $(this).bind('click', function () {
                        var messsage_height = $('#modalDmlIframeId').contents().find('#message_alert').height();
                        $('#modalDmlIframeId').height($('#modalDmlIframeId').height() - 32);
                       if (messsage_height > 0) {
                            messsage_height += 20;
                        }
                        parent.parent.resizemodal($('#modalDmlIframeId').height() + 74 + messsage_height);
                    });
                });
            });
        });
    });


    $('#exitBtn').css('display', 'none');
    return false;
}
//-->
// Show Select Document Dialog
function showSelectDocumentLinkDialog() {

    var basePath = $("#hidBasePath").val();
    DocLinkItem.type = $("#hidType").val();
    DocLinkItem.typeID = $("#hidTypeID").val();
    DocLinkItem.ref_field = $("#hidField").val();
    var siteID = getSiteFromDocumentType(DocLinkItem.type);

    var link = DmlConstants.modalUploadDocumentPageUrl + "?basePath=" + basePath + "&type=" + DocLinkItem.type + "&typeID=" + DocLinkItem.typeID + "&field=" + DocLinkItem.ref_field + "&siteID=" + siteID +"&action=Sel";
    $("#mainDocLink").css({ 'margin-top': '0px' });
    $("#mainDocLink").html('<iframe id="modalDmlIframeId" scrolling="no" style="width:100%;height:480px;overflow:hidden;min-height:480px;min-width:' + DmlConstants.dialogIframeMinWidth + 'px;" marginWidth="0" marginHeight="0" frameBorder="0" scrolling="no" /><div id="modal-loading-Dml">' + DmlConstants.modalLoadingMessage + '</div>').dialog("open");
    $("#modalDmlIframeId").attr("src", link);

    $('#modal-loading-Dml').show();
    $('#modalDmlIframeId').load(function () {
        $('#modal-loading-Dml').fadeOut();
    });

    $('#stepbtn_2').val('Select Document');
    $('#stepbtn_2').show();

    $('#exitBtn').css('display', 'none');
    return false;
}
//-->
// Show Remove Document Dialog
function showRemoveDocumentLinkDialog() {
    var basePath = $("#hidBasePath").val();
    DocLinkItem.type = $("#hidType").val();
    DocLinkItem.typeID = $("#hidTypeID").val();
    DocLinkItem.ref_field = $("#hidField").val();
    if (DocLinkItem.typeID != 0) {
        var link = DmlConstants.modalRemoveDocumentPageUrl + "?&type=" + DocLinkItem.type + "&typeID=" + DocLinkItem.typeID + "&field=" + DocLinkItem.ref_field + "&action=Rme";
        $("#mainDocLink").html('<iframe id="modalDmlIframeId" scrolling="no" style="width:100%;height:480px;overflow:hidden;min-height:480px;min-width:' + DmlConstants.dialogIframeMinWidth + 'px;" marginWidth="0" marginHeight="0" frameBorder="0" scrolling="no" /><div id="modal-loading-Dml">' + DmlConstants.modalLoadingMessage + '</div>').dialog("open");
        $("#modalDmlIframeId").attr("src", link);
    }
    else {
        var itemString = parent.$('#MainContent_hidden_SA_document').val();
        var jsonParam = jQuery.parseJSON(itemString);
        var url = DmlConstants.modalRemoveDocumentPageUrl + "?&type=" + DocLinkItem.type + "&typeID=" + DocLinkItem.typeID + "&field=" + DocLinkItem.ref_field + "&action=Rme&jsonData=" + itemString;
        $("#mainDocLink").html('<iframe id="modalDmlIframeId" scrolling="no" style="width:100%;height:480px;overflow:hidden;min-height:480px;min-width:' + DmlConstants.dialogIframeMinWidth + 'px;" marginWidth="0" marginHeight="0" frameBorder="0" scrolling="no" /><div id="modal-loading-Dml">' + DmlConstants.modalLoadingMessage + '</div>').dialog("open");
            $("#modalDmlIframeId").attr("src", url);
    }
    $('#modal-loading-Dml').show();
    $('#modalDmlIframeId').load(function () {
        $('#modal-loading-Dml').fadeOut();
    });

    $("#mainDocLink").css({ 'margin-top': '0px' });

    $('#exitBtn').css('display', 'none');

    return false;
}

function EnableAndSetLink() {
    parent.$('#hidStep2Path').val(stepBtn2Link);

    $('#btnStepBack').click(function () {
        parent.$('#stepbtn_1').click();
        parent.parent.resizemodal(DocLinkListViewConstants.dialogIframeMinHeight);
    });
}

function stepBtn2Click() {
    if ($('#hidStep2Path').val() != "" || $('#hidStep2Path').val() != undefined) {
        var link = $('#hidStep2Path').val();
        $("#mainDocLink").css({ 'margin-top': '0px' });
        $("#mainDocLink").html('<iframe id="modalDmlIframeIdNext" scrolling="no" style="width:100%;height:480px;overflow:hidden;min-height:'+DocLinkListViewConstants.dialogIframeMinHeight+';min-width:' + DmlConstants.dialogIframeMinWidth + 'px;" marginWidth="0" marginHeight="0" frameBorder="0" scrolling="no" /><div id="modal-loading-Dml">' + DmlConstants.modalLoadingMessage + '</div>').dialog("open");
        $("#modalDmlIframeIdNext").attr("src", link);
        return false;
    }
}

function getQueryString(str)
{
    var pageParam = new Array();
    var url = str;
    var query_string = url.split("?");
    if (query_string.length > 0) {
        var params = query_string[1].split("&");
        for (x = 0; x < params.length; x++) {
            pageParam[x] = params[x].split("=")[1];
        }
    }
    return pageParam;
}
//jump or old process
function jumpToSelectedSaOrLeaseAppTab() {
    var pageParam = getQueryString(window.location.toString());
    if (pageParam.length > 1) {
        if ($('#HiddenSaID').val() != "null" && $("#HiddenSaID").val() != undefined) {
            var data = GetAllJsonData();
            if (data != null && data != "undefind") {
                $(function () {
                    $("#tabs2").tabs({ selected: 1 });
                });
                var intI = 0;
                for (var i = 0; i < data.length; i++) {
                    if (data[i].ID.toString() == $("#HiddenSaID").val()) {
                        intI = i;
                        setLabelPageCurrent(intI);
                       setBufferIndex(intI);
                        SetDataToUI(intI);
                        $("#MainContent_slideshow2").show("slide", { direction: "right" }, 220);
                        initComment(intI);
                    }
                    else {
                        $('#MainContent_SAWToSAC_' + data[i].ID).css('display', 'none');
                        $('#MainContent_SA_Received_' + data[i].ID).css('display', 'none');
                    }
                }
            }
        }
    }
}

function jumpToSelectedSdpTab() {
    var pageParam = getQueryString(window.location.toString());
    if (pageParam.length > 1) {
        //sdp tab
       if (pageParam[1] == "site_data_packages") {
           if (pageParam[2] == "TowerDrawing_DocStatus") {
                $(function () {
                    $("#tabs2").tabs({ selected: 1 });
                });
            }
            else if (pageParam[2] == "TowerTagPhoto_DocStatus") {
                $(function () {
                    $("#tabs2").tabs({ selected: 1 });
                });
            }
        }
    }
}
//-->

//-->

// On Download Click
function OnDownloadDocClick(key) {
    if ($('#MainContent_hidDocumentDownload').length > 0) {
        $('#MainContent_hidDocumentDownload').val(key);
    }
    if ($('#hidDocumentDownload').length > 0) {
        $('#hidDocumentDownload').val(key);
    }
    window.onbeforeunload = function () { };
    __doPostBack('false', 'DownloadDocument');
    return false;
}
//-->

function CreateRemoveDocumentLink() {

    $('#removeDocBtn').css('display', 'none');
    $('#MainContent_RemoveNewSaDocDiv').css('height', '320px');

    var allItemString = parent.parent.$('#MainContent_hidden_SA_document').val();
    if (allItemString != "" && allItemString != "[]") {
        var allItem = jQuery.parseJSON(allItemString);
        if (allItem.length > 0) {
            $('#documentUpdatePanel').css('display', 'none');
            $('#divNoDocData').css('display', 'none');
            $('#MainContent_documentDiv').css('display','none');
            $('#removenewSaDocbtn').css('display', 'inline-block');

            for (x = 0; x < allItem.length; x++) {
                var realKey = decodeURIComponent(allItem[x].ref_key);
                var field = allItem[x].ref_field;
                var name = realKey.split("/")[realKey.split("/").length - 1];
                //name = unescape(name.replace(/[+]/g, " "));

                var checkBox = document.getElementById('docCheckBox');
                if (parent.$('#hidField').val() == allItem[x].ref_field) {
                    $('#RemoveNewSaDoc').css('display', 'block');
                    if ($('#divDocHeader_NewSa').length == 0) {
                        $(".RemoveNewSaDoc").append("<h4 id='divDocHeader_NewSa' runat='server' style='width:100%;'>Existing Document Links</h4>");
                    }

                    $(".RemoveNewSaDoc").append("<input type='checkbox' class='reMovChk' id='MainContent_removeChk_" + x + "' ref_field=" + field + " ref_key=" + realKey + ">" +
                    "<label id='MainContent_lbl_" + x + "' class='RemoveItemLabel'>" + name + "</label>");
                }
            }
        }
    }
    else {
        $('#divNoDocData').css('display', 'block');
        $('#RemoveNewSaDocDiv').css('display', 'none');
        $('#removenewSaDocbtn').css('display', 'none');
    }
}

function RemoveDocumentLink() {
    var checkClick = true;
    $('input:checkbox').each(function (index) {
        if ($(this).attr('id').indexOf("removeChk_") != -1) {
            if ($(this).prop('checked')) {
                var targetDelKey = $(this).attr('ref_key');
                var targetField = $(this).attr('ref_field');
                if (parent.parent.$("li[ref_field='" + targetField + "'][ref_key='" + targetDelKey + "']").length > 0) {

                    //remove ui
                    parent.parent.$("li[ref_field='" + targetField + "'][ref_key='" + targetDelKey + "']").remove();
                    // remove json
                    var allItemString = parent.parent.$('#MainContent_hidden_SA_document').val();
                    var allItem = jQuery.parseJSON(allItemString);
                    var item = [];
                    var isError = false;
                    if (allItem != null) {
                        for (i = 0; i < allItem.length; i++) {
                            if (decodeURIComponent(allItem[i].ref_key) != targetDelKey) {
                                //skip same key 
                                item.push(allItem[i]);
                            }
                            else if (allItem[i].ref_field != targetField) {
                                item.push(allItem[i]);
                            }
                            else {
                                checkClick = false;
                            }
                        }
                    }
                    if (item == []) {
                        parent.parent.$('#MainContent_hidden_SA_document').val("");
                    }
                    else {
                        var jsonString = JSON.stringify(item);
                        parent.parent.$('#MainContent_hidden_SA_document').val(jsonString);
                    }
                }
            }
        }
    });
   if (checkClick) {
        DisplayWarringUncheck();
    }
    else {
        parent.parent.$('#modalDocument').dialog('close');
    }

    return false;
}

function closeDialogExisting(type, typeID, field, key, name, related_field ) {
    //update json data
    addJsonDoc(type, typeID, field, key, name);
    
    // case create new SA,function will automatic update after select document
    if ((typeID == "0") && (type == "structural_analyses")) {
        OnCreateNewSa();
        parent.parent.loadGridDocuments(parent.parent.$('#hidden_document_type').val());
    }
    else if ((typeID == "0") && (type == "leaseapp_revision")) {
        OnCreateNewLeaseRV();
        parent.parent.loadGridDocuments(parent.parent.$('#hidden_document_type').val());
    }
    else if ((typeID == "0") && (type == "tower_modification")) {
        OnCreateNewTMOPO();
    }
    else if ((type == "lease_applications") || (type == "structural_analyses") || (type == "leaseapp_revision")) {
        if (parent.parent.$('#hidden_document_type').length > 0) {
            parent.parent.loadGridDocuments(parent.parent.$('#hidden_document_type').val());
        }
    }

    if (type == "site_data_packages") {
        populateSDP(field);
    }

    if (related_field != "True") {
        parent.parent.$('#modalDocument').dialog('close');
    }
}

function closeDialogExistingWithJson(listData, related_field) {
    //update json data
    listData = jQuery.parseJSON(listData.split("|").join("'"));
    if (listData.length > 0) {
        var typeID = listData[0].ID;
        var type = listData[0].type;
        var field = listData[0].ref_field;

        for (iCount = 0; iCount < listData.length; iCount++) {
            addJsonDoc(type, typeID, field, listData[iCount].ref_key, listData[iCount].documentName);
        }
    }

    // case create new SA,function will automatic update after select document
    if ((typeID == "0") && (type == "structural_analyses")) {
        OnCreateNewSa();
        parent.parent.loadGridDocuments(parent.parent.$('#hidden_document_type').val());
    }
    else if ((typeID == "0") && (type == "leaseapp_revision")) {
        OnCreateNewLeaseRV();
        parent.parent.loadGridDocuments(parent.parent.$('#hidden_document_type').val());
    }
    else if ((typeID == "0") && (type == "tower_modification")) {
        OnCreateNewTMOPO();
    }
    else if ((type == "lease_applications") || (type == "structural_analyses") || (type == "leaseapp_revision")) {
        if (parent.parent.$('#hidden_document_type').length > 0) {
            parent.parent.loadGridDocuments(parent.parent.$('#hidden_document_type').val());
        }
    }

    if (type == "site_data_packages") {
        populateSDP(field);
    }

    if (related_field != "True") {
        parent.parent.$('#modalDocument').dialog('close');
    }
}

function closeDialogExistingWithUpdate(type, typeID, field, oldKey, newKey, newName, related_field) {
    //update json data
    updateJsonDoc(type, typeID, field, oldKey, newKey, newName);

    // case create new SA,function will automatic update after select document
    if ((typeID == "0") && (type == "structural_analyses")) {
        OnCreateNewSa();
        parent.parent.loadGridDocuments(parent.parent.$('#hidden_document_type').val());
    }
    else if ((typeID == "0") && (type == "leaseapp_revision")) {
        OnCreateNewLeaseRV();
        parent.parent.loadGridDocuments(parent.parent.$('#hidden_document_type').val());
    }
    else if ((typeID == "0") && (type == "tower_modification")) {
        OnCreateNewTMOPO();
    }
    else if ((type == "lease_applications") || (type == "structural_analyses") || (type == "leaseapp_revision")) {
        if (parent.parent.$('#hidden_document_type').length > 0) {
            parent.parent.loadGridDocuments(parent.parent.$('#hidden_document_type').val());
        }
    }

    if (type == "site_data_packages") {
        populateSDP(field);
    }

    if (related_field != "True") {
        parent.parent.$('#modalDocument').dialog('close');
    }
}

function setDllyn(ddlname) {
    $("#" + ddlname + " option:text='Yes' ").attr("selected", "selected");
}

function populateSDP(field){
    switch (field) {
        //left site
        case "TowerDrawing_DocStatus":
            setDllyn('MainContent_ddlTowerDrawing_DocStatus');
            break;
        case "TowerTagPhoto_DocStatus":
            setDllyn('MainContent_ddlTowerTagPhoto_DocStatus');
            break;
        case "StructuralCalcs_DocStatus":
            setDllyn('MainContent_ddlStructuralCalcs_DocStatus');
            break;
        case "StructuralAnalysis_DocStatus":
            setDllyn('MainContent_ddlStructuralAnalysis_DocStatus');
            break;
        case "TowerErectionDetail_DocStatus":
            setDllyn('MainContent_ddlTowerErectionDetail_DocStatus');
            break;
        case "FoundationDesign_DocStatus":
            setDllyn('MainContent_ddlFoundationDesign_DocStatus');
            break;
        case "ConstructionDrawings_DocStatus":
            setDllyn('MainContent_ddlConstructionDrawings_DocStatus');
            break;
        case "GeotechReport_DocStatus":
            setDllyn('MainContent_ddlGeotechReport_DocStatus');
            break;
        case "BuildingPermit_DocStatus":
            setDllyn('MainContent_ddlBuildingPermit_DocStatus');
            break;
        //right side
        case "ZoningApproval_DocStatus":
            setDllyn('MainContent_ddlZoningApproval_DocStatus');
            break;
        case "PhaseI_DocStatus":
            setDllyn('MainContent_ddlPhaseI_DocStatus');
            break;
        case "PhaseII_DocStatus":
            setDllyn('MainContent_ddlPhaseII_DocStatus');
            break;
        case "NEPA_DocStatus":
            setDllyn('MainContent_ddlNEPA_DocStatus');
            break;
        case "SHPO_DocStatus":
            setDllyn('MainContent_ddlSHPO_DocStatus');
            break;
        case "GPS1a2c_DocStatus":
            setDllyn('MainContent_ddlGPS1a2c_DocStatus');
            break;
        case "Airspace_DocStatus":
            setDllyn('MainContent_ddlAirspace_DocStatus');
            break;
        case "FAAStudyDet_DocStatus":
            setDllyn('MainContent_ddlFAAStudyDet_DocStatus');
            break;
        case "FCC_DocStatus":
            setDllyn('MainContent_ddlFCC_DocStatus');
            break;
        case "AMCertification_DocStatus":
            setDllyn('MainContent_ddlAMCertification_DocStatus');
            break;
        case "Towair_DocStatus":
            setDllyn('MainContent_ddlTowair_DocStatus');
            break;
        case "PrimeLease_DocStatus":
            setDllyn('MainContent_ddlPrimeLease_DocStatus');
            break;
        case "Memorandum_DocStatus":
            setDllyn('MainContent_ddlMemorandum_DocStatus');
            break;
        case "Title_DocStatus":
            setDllyn('MainContent_ddlTitle_DocStatus');
            break;
    }
}

function closeRemoveExistingDialog(type, field, leaveDialogOpen) {
    //update JSON Data
    var allItemString = null;
    var bIsComment = false;
    var jsonData = [];
    if ((field == 'RogueEquipment') || (field == 'Issue')) {
        if (field == "Issue") {
            parent.parent.$('#hidden_document_Issue').val($('#MainContent_hidden_CurrentDoc').val());
        }
        allItemString = parent.parent.$('#modalIframeId').contents().find('#hidden_document_' + field);
    }
    else if (field == "SA_Comments" || field == "LE_Comments" || field == "TMO_Comments" || field == "SDP_Comments") {
        allItemString = parent.parent.$('#MainContent_hidden_document_all_document_comments');
        var jsonString = allItemString.val();
        jsonData = jQuery.parseJSON(jsonString);

        var removedData = jQuery.parseJSON($('#MainContent_hidden_CurrentDoc').val());
        for (i = 0; i < jsonData.length; i++) {
            if (jsonData[i][0].ID == removedData[0].ID) {
                jsonData[i] = removedData;
            }
        }
        bIsComment = true;
        if (field == "SA_Comments" || field == "LE_Comments") {
            if (parent.parent.$('#hidden_document_type').length > 0) {
                parent.parent.loadGridDocuments(parent.parent.$('#hidden_document_type').val());
            }
        }
    }
    else if (field == "Issue_Action") {
        allItemString = parent.parent.$('#hidden_document_Issue_Action');
        var jsonString = allItemString.val();
        jsonData = jQuery.parseJSON(jsonString);

        var removedData = jQuery.parseJSON($('#MainContent_hidden_CurrentDoc').val());
        for (i = 0; i < jsonData.length; i++) {
            if (jsonData[i][0].ID == removedData[0].ID) {
                jsonData[i] = removedData;
            }
        }
        bIsComment = true;
    }
    else {
        if ((type == 'lease_applications') || (type == "structural_analyses") || (type == "leaseapp_revision")) {
            //check page from lease admin or not?
            if (parent.parent.document.location.href.indexOf('LeaseAdminTracker') !== -1) {
                field = 'LA_' + field;
            }
            else {
                parent.parent.loadGridDocuments(parent.parent.$('#hidden_document_type').val());
            }
        }
        allItemString = parent.parent.$('#hidden_document_' + field);
    }
    if (!bIsComment) {
       // If a reference field has been used, the JSON data ref_field will not
       // match the field being processed. In this case, we rewrite the JSON
       // and update the allItemString accordingly.
       var jsonTest = JSON.parse($('#MainContent_hidden_CurrentDoc').val());
       
       if (jsonTest[0].ref_field != field)
       {
          for (var i = 0; i <= jsonTest.length - 1; i++)
          {
             jsonTest[i].ref_field = field;
          }

          allItemString.val(JSON.stringify(jsonTest));
       }
       else
       {
          allItemString.val($('#MainContent_hidden_CurrentDoc').val());
       }       
    }
    else {
        var jsonString = JSON.stringify(jsonData);
        allItemString.val(jsonString);
    }
    if (leaveDialogOpen != "True") {
        parent.parent.$('#modalDocument').dialog('close');
    }
}

function addJsonDoc(type, typeID, field, key, name) {

    if (key != null) {
        var realKey = key;
        var newDocument = { 'type': type, 'ID': typeID, 'documentName': name, 'ref_field': field, 'ref_key': key };
        var hiddenItem = null;
        var bIsComment = false;
        if ((field == 'RogueEquipment') || (field == 'Issue')) {
            hiddenItem = parent.parent.$('#modalIframeId').contents().find('#hidden_document_' + field);
        }
        else if (field == "SA_Comments" || field == "LE_Comments" || field == "TMO_Comments" || field == "SDP_Comments") {
            hiddenItem = parent.parent.$('#MainContent_hidden_document_all_document_comments');
            bIsComment = true;
        }
        else if (field == "Issue_Action" ) {
            hiddenItem = parent.parent.$('#hidden_document_Issue_Action');
            bIsComment = true;
        }
        else {
            if (type == 'lease_applications') {
                //check page from lease admin or not?
                if (parent.parent.document.location.href.indexOf('LeaseAdminTracker') !== -1) {
                    field = 'LA_' + field;
                }
            }
            hiddenItem = parent.parent.$('#hidden_document_' + field);
        }
        var allItemString = $(hiddenItem).val();
        var allDocument = [];
        var bufferDocument = [];
        var bIsTemplate = false;

        if (!bIsComment) {
            if (allItemString != "" && allItemString != undefined) {
                allDocument = jQuery.parseJSON(allItemString);
                if (allDocument != null) {
                    for (i = 0; i < allDocument.length; i++) {
                        if (allDocument[i].type == "") {
                            if (typeID == 0 || allDocument[i].ref_key == "") {
                                bIsTemplate = true;
                            }
                        }
                        if (!bIsTemplate) {
                            bufferDocument.push(allDocument[i]);
                        }
                    }
                }
            }
            //if json value is templete then replace template with document data
            if (bIsTemplate) {
                allDocument = [];
            }
            else {
                allDocument = bufferDocument;
            }

            //insert new documnet data to array
            var isExist = false;
            for (i = 0; i < allDocument.length; i++) {
                if (allDocument[i].ref_key == newDocument.ref_key) {
                    isExist = true;
                }
            }
            if (!isExist) {
                allDocument.push(newDocument);
            }
        }
        else {
            if (allItemString != "" && allItemString != undefined) {
                allDocument = jQuery.parseJSON(allItemString);
                for (i = 0; i < allDocument.length; i++) {
                    bIsTemplate = false;
                    if (allDocument[i][0].ID == typeID) {
                        if (allDocument[i][0].type == "") {
                            allDocument[i] = [];
                        }

                        //insert new documnet data to array
                        var isExist = false;
                        for (j = 0; j < allDocument[i].length; j++) {
                            if (allDocument[i][j].ref_key == newDocument.ref_key) {
                                isExist = true;
                            }
                        }
                        if (!isExist) {
                            allDocument[i].push(newDocument);
                        }
                    }
                    if (!bIsTemplate) {
                        bufferDocument.push(allDocument[i]);
                    }
                }
            }
            allDocument = bufferDocument;
        }

        var jsonString = JSON.stringify(allDocument);
        hiddenItem.val(jsonString);

        if (field == "Issue") {
            parent.parent.$('#hidden_document_Issue').val(jsonString);
        }

        var now = new Date();
      //set current date
      if (field == "SA_Received") {
            if (parent.parent.$('#MainContent_txtProcessDates_Received_ColLeft1').length > 0) {
                if (parent.parent.$('#MainContent_txtProcessDates_Received_ColLeft1').val() == "" || parent.parent.$('#MainContent_txtProcessDates_Received_ColLeft1').val() == null) {
                    parent.parent.$('#MainContent_txtProcessDates_Received_ColLeft1').val(now.format("MM/dd/yyyy"));
                }
            }
        }
        //SDP
        //auto-set ddl to yes
        if (type == "site_data_packages") {
            var ddlObj = parent.parent.$('#MainContent_ddl' + field);
           if ($(ddlObj).length != 0) {
                $(ddlObj).val(1);
            }
        }
    }
}

function updateJsonDoc(type, typeID, field, oldKey, newKey, newName) {

    if (newName != "") {
        var hiddenItem = null;
        var bIsComment = false;
        if ((field == 'RogueEquipment') || (field == 'Issue')) {
            hiddenItem = parent.parent.$('#modalIframeId').contents().find('#hidden_document_' + field);
        }
        else if (field == "SA_Comments" || field == "LE_Comments" || field == "TMO_Comments" || field == "SDP_Comments") {
            hiddenItem = parent.parent.$('#MainContent_hidden_document_all_document_comments');
            bIsComment = true;
        }
        else if (field == "Issue_Action") {
            hiddenItem = parent.parent.$('#hidden_document_Issue_Action');
            bIsComment = true;
        }
        else {
            if (type == 'lease_applications') {
                //check page from lease admin or not?
                if (parent.parent.document.location.href.indexOf('LeaseAdminTracker') !== -1) {
                    field = 'LA_' + field;
                }
            }
            hiddenItem = parent.parent.$('#hidden_document_' + field);
        }
        var allItemString = $(hiddenItem).val();
        var allDocument = [];
        var bufferDocument = [];
        var bIsTemplate = false;

        if (!bIsComment) {
            if (allItemString != "" && allItemString != undefined) {
                allDocument = jQuery.parseJSON(allItemString);
                if (allDocument != null) {
                    for (i = 0; i < allDocument.length; i++) {
                        if (allDocument[i].ref_key == oldKey) {
                            allDocument[i].ref_key = newKey;
                            allDocument[i].documentName = newName;
                            i = allDocument.length;
                        }
                    }
                }
            }
        }
        else {
            if (allItemString != "" && allItemString != undefined) {
                allDocument = jQuery.parseJSON(allItemString);
                for (i = 0; i < allDocument.length; i++) {
                    bIsTemplate = false;
                    if (allDocument[i][0].ID == typeID) {
                        for (j = 0; j < allDocument[i].length; j++) {
                            if (allDocument[i][j].ref_key == oldKey) {
                                allDocument[i][j].ref_key = newKey;
                                allDocument[i][j].documentName = newName;
                                //i = allDocument.length;
                                j = allDocument[i].length;
                            }
                        }
                    }
                }
            }
        }

        var jsonString = JSON.stringify(allDocument);
        hiddenItem.val(jsonString);

        if (field == "Issue") {
            parent.parent.$('#hidden_document_Issue').val(jsonString);
        }

        var now = new Date();
        //set current date
        if (field == "SA_Received") {
            if (parent.parent.$('#MainContent_txtProcessDates_Received_ColLeft1').length > 0) {
                if (parent.parent.$('#MainContent_txtProcessDates_Received_ColLeft1').val() == "" || parent.parent.$('#MainContent_txtProcessDates_Received_ColLeft1').val() == null) {
                    parent.parent.$('#MainContent_txtProcessDates_Received_ColLeft1').val(now.format("MM/dd/yyyy"));
                }
            }
        }
        //SDP
        //auto-set ddl to yes
        if (type == "site_data_packages") {
            var ddlObj = parent.parent.$('#MainContent_ddl' + field);
            if ($(ddlObj).length != 0) {
                $(ddlObj).val(1);
            }
        }
    }
}

function AddWarrningMsg() {
    $(window).bind('beforeunload', function () {
        return "Do you really want to leave now?";
    });

    parent.parent.$("document-list.li.a").click(function () {
        dirty = false;
        $(window).unbind("beforeunload");
    });

    parent.parent.$("input.ui-button").click(function () {
        dirty = false;
        $(window).unbind("beforeunload");
    });

    parent.parent.$("form").submit(function () {
        $(window).unbind("beforeunload");
    });
}

function GetAllDocumentJsonData(hiddenID) {
    var jsonStr = parent.parent.$("#" + hiddenID).val();
    if (jsonStr == "") {

        return null;
    }
    else if (jsonStr == null) {
        return null;
    }
    else {
        var data = jQuery.parseJSON(jsonStr);
        return data;
    }
}
function GetBufferIndex(hiddenID) {
    var data = parent.parent.$("#" + hiddenID).html();
    return data;
}

function closeDialogAfterError(result, type, typeID, field) {
    parent.parent.$('#modalDocument').dialog('close');
}

function getIframeWindow(iframe_object) {
    var doc;

    if (iframe_object.contentWindow) {
        return iframe_object.contentWindow;
    }

    if (iframe_object.window) {
        return iframe_object.window;
    }

    if (!doc && iframe_object.contentDocument) {
        doc = iframe_object.contentDocument;
    }

    if (!doc && iframe_object.document) {
        doc = iframe_object.document;
    }

    if (doc && doc.defaultView) {
        return doc.defaultView;
    }

    if (doc && doc.parentWindow) {
        return doc.parentWindow;
    }

    return undefined;
}

// Setup Link Documnet Dialog
function setupDocumentLinkDialog(title, isModalOpen) {
    if (isModalOpen != null) {
        ModalOpen = isModalOpen;
    }
    // Create New Dialog
    var optionsCreate = {
        autoOpen: false,
        width: DocLinkListViewConstants.dialogWidth + 200,
        height: DocLinkListViewConstants.dialogIframeMinHeight,
        modal: true,
        title: title,
        open: function (event, ui) {
            $('#modal-loading-document-link').show();
            $('#modalDocumentIframeId').load(function () {
                $('#modal-loading-document-link').fadeOut();
            });
        },
        close: function (event, ui) {
            window.onbeforeunload = function () { };
            if (isModalOpen == null) {
                createDocumentLink();

                //display comments
                if ($('#MainContent_hidden_document_all_document_comments').length > 0) {
                    CreateCommentLinkDocument('MainContent_hidden_document_all_document_comments', 'document-comment-');
                }

                ///dispaly only current revision in SA and Lease App revision
                if ($("#MainContent_hidden_SA_ID").length > 0) {
                    setSA_RV_Document();
                }
                ///display only current version in TMO
                else if ($("#MainContent_hidden_TM").length > 0) {
                    setTMdocument();
                }
            }
            else {
                if ($('#modalIframeId').length > 0) {
                    var el = document.getElementById('modalIframeId');
                    getIframeWindow(el).createDocumentLink();
                    $('#modalIframeId').height($(el).contents().find('.modal-page-wrapper').outerHeight(true) + 30);
                }
                else {
                    if ($('#hidden_document_Issue_Action').length > 0) {
                        CreateCommentLinkDocument('hidden_document_Issue_Action', 'document-comment-');
                    }
                }
            }
        }
    };
    //
    $("#modalDocument").dialog(optionsCreate);
}

//-->


// On Delete Click
function OnDeleteDocumentClick(name) {
    // check element before display dialog
    if (CheckRemoveCheckBoxClick()) {
        var confirmDoc = 'dialog-confirm-document-delete';
        var delDialogHtml = '<div id="' + confirmDoc + '" title="Delete this link?"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0 width:309px;"></span>This will remove the link between this field and the selected document(s). This does not delete the document(s) from TrackiT Docs.</p></div>';

        $('#' + confirmDoc).remove();
        $(delDialogHtml).dialog({
            open: function () {
                $(this).parent().appendTo("form[0]");
            },
            buttons: {
                'Confirm': function () {
                    __doPostBack(name, '');
                    $(this).dialog('close');
                },
                'Cancel': function () {
                    $(this).dialog('close');
                }
            }
        });
        $('#dialog-confirm-document-delete').dialog('open');

    }
    else {
        //warrning msg
        DisplayWarringUncheck();
    }
    return false;
}
function CheckRemoveCheckBoxClick() {
    var checkClick = false;
    $('input:checkbox').each(function (index) {
        if ($(this).attr('removeChk').indexOf("removeChk_") != -1) {
            if ($(this).prop('checked')) {
                checkClick = true;
            }
        }
    });
   return checkClick;
}
//-->
// Show Create Dialog
function showDocLinkDialog(field, ref_id) {
    window.onbeforeunload = function () { };

    var basePath = $("#hidBasePath").val();
    var modalDiv = $("#modalDocument");

    $(modalDiv).html('<iframe id="modalDocumentIframeId" scrolling="no" style="width:100%;height:590px;overflow:hidden;min-height:' + DocLinkListViewConstants.dialogIframeMinHeight + 'px;min-width:' + DocLinkListViewConstants.dialogIframeMinWidth + 'px;" marginWidth="0" marginHeight="0" frameBorder="0" scrolling="no" /><div id="modal-loading-document-link">' + DocLinkListViewConstants.modalLoadingMessage + '</div>').dialog("open");
    var laID = 0;
    var saID = 0;
    var tmpLeaseAppID = getQueryString(window.location.toString());
    if (tmpLeaseAppID.length > 0) {
        laID = tmpLeaseAppID[0].replace('#','');
    }

    saID = laID;
    //check SA set Sa ID
    if ($("#MainContent_hidden_SA_ID").length > 0) {
        if (ref_id != null) {
            saID = ref_id;
        }
        else if ($("#MainContent_liRevisions").hasClass('ui-tabs-selected')) {
            saID = $("#MainContent_hidden_revision_id").val();
        }
        else {
            saID = $("#MainContent_hidden_SA_ID").val();
        }
    }
    else if ($("#MainContent_hidden_TM").length > 0) {
        var index = $("#BufferIndex").html();
        if (ref_id != null) {
            saID = ref_id;
        }
        else if (index != "") {
            var jsonTMOStr = $("#MainContent_hidden_TM").val();
            if (jsonTMOStr != "") {
                var tmo_data = jQuery.parseJSON(jsonTMOStr);
                if (index < tmo_data.length) {
                    saID = tmo_data[index].ID;
                }
                else {
                    saID = "0";
                }
            }
            else {
                saID = "0";
            }
        }
    }
    else if (ref_id != null) {
        saID = ref_id;
    }

    if ($("#MainContent_hidden_leaseapp_id").length > 0) {
        if ($("#MainContent_hidden_leaseapp_id").val() != "") {
            laID = $("#MainContent_hidden_leaseapp_id").val();
        }
    }
    var link = '';
    if (window.location.href.indexOf('AllComment') > 0) {
        link = DocLinkListViewConstants.modalAddPageUrl + "?id=" + laID + "&field=" + field + "&allComment=true&typeID=" + saID + "&basePath=&action=Doc";
    }
    else {
        link = DocLinkListViewConstants.modalAddPageUrl + "?id=" + laID + "&field=" + field + "&typeID=" + saID + "&basePath=&action=Doc";
    }
    $("#modalDocumentIframeId").attr("src", link);

    return false;
}
//-->

function ForceOpenDefault() {
    var pageParam = getQueryString(window.location.toString());
    if (parent.parent.$('#hidUploadFlag').val() == "true") {
        var siteID = pageParam[0].replace("/", "");
        if ($("#id_" + siteID + "_text").length != 0) {
            $('#MainContent_TreePanel').scrollTo($("#id_" + siteID + "_text"), 200);
        }
        parent.parent.$('#hidUploadFlag').val("false");
    }
}

function CheckValidUploadDocument() {
    if (!validateUploadDocument()) {
        //resize modal case long message 
        var messsage_height = $('#message_alert').height();
        dailog_height = $('.create-container').height() + 219;
        $('#message_alert').find('.close').each(function () {
            $(this).unbind('click');
            $(this).bind("click", function () {
                $(this).parent().fadeOut('slow', function () {
                    // Animation complete.
                    dailog_height = $('.create-container').height() + 219;
                    parent.parent.resizemodal(dailog_height);
                });
            });
        });
        parent.parent.resizemodal(dailog_height + messsage_height + 28);
        return false;
    }
    return true;
}

// Validate and Warring message
// Validate Select Document
function validateSelectDocument() {

    var valid = true;
    var strError = messageValidateError;
    valid = validateSelectFileName("#hidBasePath");

    // Throw Error
    if (!valid) {
        $("#message_alert").html("");
        showMessageWarrningAlert("error", messageValidateSelectFile);

        $("#hidBasePath").val('');

        $('.slow-alert').click(function () {
            var parent = $(this).parent();
            $(parent).fadeOut('slow', function () {
               
            });
        });

        //resize modal case long message 
        var messsage_height = $('#message_alert').height();
        dailog_height = $('.create-container').height() + 241;
        $('#message_alert').find('.slow-alert').each(function () {
            $(this).unbind('click');
            $(this).bind("click", function () {
                $(this).parent().fadeOut('slow', function () {
                    // Animation complete.
                    parent.parent.resizemodal(dailog_height);
                });
            });
        });
        parent.parent.resizemodal(dailog_height + messsage_height + 10);

    } else {
        // Show - Loading Spinner
        $('#spinner-cont').activity({ segments: 8, width: 3, space: 0, length: 3, speed: 1.5, align: 'left' });
    }

    return valid;
}

function resizeAddSiteDialog(value, isResetHeight) {
    if (isResetHeight) {
        parent.parent.$("#modalIframeId").css('min-height',parent.parent.SiteAddDialogConstants.iframe_minHeight+'px');
    }
    else {
        var newHeight = parent.parent.$("#modalIframeId").height()+ value;
        parent.parent.$("#modalIframeId").css('min-height',newHeight+'px');
    }
}

function resizemodal(newheight) {
    $('#modalDocumentIframeId').parent().parent().height(newheight);
    $('#modalDocumentIframeId').parent().height(newheight - 41);
    $('#modalDocumentIframeId').height(newheight - 41);
}

function IncreateOrresizemodal(value,isResetHeight) {
    var modalDocumentIframeId = $('#modalDocumentIframeId').parent().parent();
    var bodyHeight = parent.$("#modalDmsIframeId").height();

    if (modalDocumentIframeId.length > 0) {
        if (!isResetHeight) {
            // on Document link
            var parentHeight = $('#modalDocumentIframeId').parent().parent().height();
            var newheight = parentHeight + value;
            $('#modalDocumentIframeId').parent().parent().height(parentHeight + value);
            $('#modalDocumentIframeId').parent().height(newheight - 41);
            $('#modalDocumentIframeId').height(newheight - 41);
        }
        else {
            $('#modalDocumentIframeId').parent().parent().height(DocLinkListViewConstants.dialogHeight);
            $('#modalDocumentIframeId').parent().height(DocLinkListViewConstants.dialogIframeMinHeight);
            $('#modalDocumentIframeId').height(DocLinkListViewConstants.dialogIframeMinHeight);
        }
    }
    else {
        if (!isResetHeight) {
            var parentHeight = parent.$("#modalDmsIframeId").parent().parent().parent().height();
            var newheight = parentHeight + value;
            parent.$("#modalDmsIframeId").height(bodyHeight + value);
            parent.$("#modalDmsIframeId").parent().parent().parent().height(parentHeight + value);
            parent.$("#modalDmsIframeId").parent().height(newheight - 41);
            parent.$("#modalDmsIframeId").height(newheight - 41);
        }
        else {
            parent.$("#modalDmsIframeId").parent().parent().parent().height(DocLinkListViewConstants.dialogHeight);
            parent.$("#modalDmsIframeId").parent().height(DmsConstants.dialogIframeMinHeight);
            parent.$("#modalDmsIframeId").height(DmsConstants.dialogIframeMinHeight);
        }
    }
}

function DisplayWarringDupFile() {
    // Throw Error
    $("#message_alert").html("");
    showMessageWarrningAlert("error", messageWarringDupSelectFile);

    $('.slow-alert').click(function () {
        var parent = $(this).parent();
        $(parent).fadeOut('slow', function () {
            
        });
    });

    $('#btnStepBack').click(function () {
        parent.$('#stepbtn_1').click();
        parent.parent.resizemodal(574);
    });

    //resize modal case long message 
    var messsage_height = $('#message_alert').height();
    dailog_height = $('.create-container').height() + 241;
    $('#message_alert').find('.slow-alert').each(function () {
        $(this).unbind('click');
        $(this).bind("click", function () {
            $(this).parent().fadeOut('slow', function () {
                // Animation complete.
                parent.parent.resizemodal(dailog_height);
            });
        });
    });
    parent.parent.resizemodal(dailog_height + messsage_height + 10);

}
function DisplayWarringUncheck() {
    // Throw Error
    $("#message_alert").html("");
    showMessageWarrningAlert("error", messageWarringNoSelectDel);

    var saId = parent.parent.$('#MainContent_hidden_SA_ID').val();

    $('.slow-alert').click(function () {
        var parent = $(this).parent();
        $(parent).fadeOut('slow', function () {
            
        });
    });

    //resize modal case long message 
    var messsage_height = $('#message_alert').height();
    dailog_height = $('.create-container').height() + 215;
    $('#message_alert').find('.slow-alert').each(function () {
        $(this).unbind('click');
        $(this).bind("click", function () {
            $(this).parent().fadeOut('slow', function () {
                // Animation complete.
                parent.parent.resizemodal(dailog_height);
            });
        });
    });
    parent.parent.resizemodal(dailog_height + messsage_height + 36);

    $('#btnStepBack').click(function () {
        parent.$('#stepbtn_1').click();
        parent.parent.resizemodal(574);
    });
}
//-->


function unCheckAlert(saId) {
    if (saId != 0) {
        $("#MainContent_documentDiv").css('height', '320px');
    }
    else {
        $("#MainContent_RemoveNewSaDocDiv").css('height', '320px');
    }
}
//-->

function urlencode(inputString) {
    var outputString = '';
    if (inputString != null) {
        for (var i = 0; i < inputString.length; i++) {
            var charCode = inputString.charCodeAt(i);
            var tempText = "";
            if (charCode < 128) {
                var hexVal = charCode.toString(16);
                outputString += '%' + (hexVal.length < 2 ? '0' : '') + hexVal.toUpperCase();
            } else if ((charCode > 127) && (charCode < 2048)) {
                tempText += String.fromCharCode((charCode >> 6) | 192);
                tempText += String.fromCharCode((charCode & 63) | 128);
                outputString += escape(tempText);
            } else {
                tempText += String.fromCharCode((charCode >> 12) | 224);
                tempText += String.fromCharCode(((charCode >> 6) & 63) | 128);
                tempText += String.fromCharCode((charCode & 63) | 128);
                outputString += escape(tempText);
            }
        }
    }
    return outputString;
}
//#end DocumentFolderLinkLoad

//Force update Lease application
function OnCreateNewSa() {
    if (parent.parent.$('#MainContent_btnSubmit_StructuralAnalysis_Upper').length > 0) {
        parent.parent.$('#MainContent_btnSubmit_StructuralAnalysis_Upper').click();
    }
    else if(parent.parent.$('#MainContent_btnSubmit_StructuralAnalysis').length > 0){
        parent.parent.$('#MainContent_btnSubmit_StructuralAnalysis').click();
    }
}

function OnCreateNewLeaseRV() {
    if (parent.parent.$('#MainContent_btnSubmitRevisionTop').length > 0) {
        parent.parent.$('#MainContent_btnSubmitRevisionTop').click();
    }
    else if (parent.parent.$('#MainContent_btnSubmitRevisionBottom').length > 0) {
        parent.parent.$('#MainContent_btnSubmitRevisionBottom').click();
    }
    
}

function OnCreateNewTMOPO() {
    if (parent.parent.$('#MainContent_btnSummitTowerModPOs2').length > 0) {
        parent.parent.$('#MainContent_btnSummitTowerModPOs2').click();
    }
    else if (parent.parent.$('#MainContent_btnSummitTowerModPOs1').length > 0) {
        parent.parent.$('#MainContent_btnSummitTowerModPOs1').click();
    }
}

function showMessageWarrningAlert(type, message, displayClose) {
    if ((type == null && message == null) || (type == "" && message == "")) {
        type = ConstMessageTypes.INFO.type;
        message = ConstMessageTypes.INFO.defaultMessage;
    }

    // Set the displayClose parameter if not specified.
    if (displayClose === undefined) {
        displayClose = true;
    }

    if (displayClose) {
        $("div#message_alert").append(
               '<div class="ms ' + type + '" style="margin-bottom:15px;">' +
                   '<p>' + message + '</p>' +
		           '<a class="slow-alert"  href="javascript:void(0)">close</a>' +
               '</div>' +
               '<div class="cb"></div>'
           );
    }
    else {
        $("div#message_alert").append(
               '<div class="ms ' + type + '" style="margin-bottom:15px;">' +
                   '<p>' + message + '</p>' +
               '</div>' +
               '<div class="cb"></div>'
           );
    }
}

function onAutoRenameUploadClick(filename) {
    var value = filename.replace('"', '');
    var currentExt = "";

    if ($("#renameCheckBoxLable").text().length > 0 && $("#renameCheckBoxLable").text().split('.').length > 1) {
        currentExt = $("#renameCheckBoxLable").text().split('.')[$("#renameCheckBoxLable").text().split('.').length - 1];
    }
    else {
        currentExt = "";
    }

    if (value.indexOf('.') > 0) {
        var newExt = value.split('.')[value.split('.').length - 1];
        $("#hidExt").val(newExt);
        newExt = '.' + newExt;
    }
    else {
        $("#hidExt").val('');
        newExt = "";
    }

    if (currentExt != '') {
        currentExt = '.' + currentExt;
        $("#renameCheckBoxLable").text($("#renameCheckBoxLable").text().replace(currentExt, newExt));
        $("#manualRenameCheckBoxLable").text(newExt);
    }
    else {
        $("#renameCheckBoxLable").text($("#renameCheckBoxLable").text() + newExt);
        $("#manualRenameCheckBoxLable").text(newExt);
    }
}
