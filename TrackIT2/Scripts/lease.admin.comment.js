﻿
//--------------------Display Add comment button-------------------//
function linkAddNewLeaseCommentClick() {
    $("#MainContent_divBtnAddLeaseComment").hide();
    $("#MainContent_divAddnewLeaseComment").slideDown();
    return false;
}
//--------------------Display Add comment button-------------------//

//----------------------Hide Add comment button-------------------//
function linkCancelLeaseCommentClick() {
    $("#MainContent_divAddnewLeaseComment").hide();
    $("#MainContent_divBtnAddLeaseComment").show();
    return false;
}
//----------------------Hide Add comment button-------------------//

//-----------------Display a comment in UI---------------------//
function addUILeaseAdminComment(text_Msg, Creator, CreateTime, CreatorId, index, leaseAdminCommentId) {
    var defaultId = 'imgUserComment_inner_Default' + index;
    $(".application-comments-inner-lease").append("<div class='comment' id='Comment_Field_inner_Lease_" + index + "' leaseAdminCommentId='LeaseAdmin_"+leaseAdminCommentId+"'>" +
        "<div class='balloon'>" +
            "<p id='pComment_inner_" + index + "'>" + text_Msg + "</p>" +
            "<div class='balloon-bottom'></div>" +
         "</div>" +
         "<div class='author-info' style='margin-top: 15px; font-size: 10pt; color: #a1a1a1;'>" +
            "<img id='imgUserComment_inner_Png_" + index + "' title='profile-picture' src='/images/avatars/" + CreatorId + ".png' width='25' onload='document.getElementById(\"" + defaultId + "\").style.display=\"none\";' onerror='this.style.display=\"none\";return false;'>" +
            "<img id='imgUserComment_inner_Jpg_" + index + "' title='profile-picture' src='/images/avatars/" + CreatorId + ".jpg' width='25' onload='document.getElementById(\"" + defaultId + "\").style.display=\"none\";' onerror='this.style.display=\"none\";return false;'>" +
            "<img id='" + defaultId + "' title='profile-picture' src='/images/avatars/default.png' width='25'>" +
            "<p id='pCommenter_inner_" + index + "'>" + Creator + ", " + CreateTime + "</p>" +
            "<img type='image'  id='btnDeleteComment_inner_Lease_" + index + "' title='Delete Comment' class='btn-comment-delete-inner-lease' src='/images/icons/Erase-Greyscale.png' style='width:12px;' />" +
            "</div>" +
    "</div>");
}
//-----------------Display all comments in UI---------------------//

//-------------------Clear all comments in UI---------------------//
function clearLeaseComment() {
    $("[id^=Comment_Field_inner_Lease_]").remove();
}
//-------------------Clear all comments in UI---------------------//

//-------------------Display all comments in UI-------------------//
function createLeaseAdminCommentUI() {
    clearLeaseComment();
    var data = jQuery.parseJSON($("#MainContent_hidden_lease_comments").val());
    if ($('#MainContent_hidden_lease_admin_id').length > 0) {
        if ($('#MainContent_hidden_lease_admin_id').val() == '') {
            $("#MainContent_divAddnewLeaseComment").hide();
            $("#MainContent_divBtnAddLeaseComment").hide();
        }
        else {
            if (data.length > 0) {
                $("#MainContent_divAddnewLeaseComment").hide();
                $("#MainContent_divBtnAddLeaseComment").show();
            }
            else {
                $("#MainContent_divAddnewLeaseComment").show();
                $("#MainContent_divBtnAddLeaseComment").hide();
            
            }
        }
    }
    try {
        var count = parseInt(data.length);
        for (var i = 0; i < count; i++) {
            addUILeaseAdminComment(data[i].comment, data[i].updater, data[i].created_at, data[i].creator_id, i, data[i].comment_id);
        }
    }
    catch (err) {
    }
    bindDeleteLeaseAdminCommentButtonActions();
}
//-------------------Display all comments in UI-------------------//

//----------------------Get comment by Id-------------------------//
function getLeaseAdminCommentByID(ui_id) {
    var Comment_index = ui_id.replace("btnDeleteComment_inner_Lease_", "");
    var data = jQuery.parseJSON($("#MainContent_hidden_lease_comments").val());
    return data[parseInt(Comment_index)].comment_id;
}
//----------------------Get comment by Id-------------------------//

//-------------------Bind event on delete a comment---------------//
function bindDeleteLeaseAdminCommentButtonActions() {
    var delDialogHtml = '<div id="dialog-confirm-comment-delete" title="Delete this comment?"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This comment will be permanently deleted and cannot be recovered. Are you sure?</p></div>';
    $('.btn-comment-delete-inner-lease').unbind('click');
    $('.btn-comment-delete-inner-lease').click(function (e) {
        var ui_id = this.id;
        COMMENT_ID = getLeaseAdminCommentByID(this.id);
        e.preventDefault();
        $('#dialog-confirm-comment-delete').remove();
        $(delDialogHtml).dialog({
            buttons: {
                'Confirm': function () {
                    var id = window.location.toString().substr(window.location.toString().indexOf('=') + 1);
                    var url = getBaseURL() + "LeaseAdminTracker/LEComment/Delete.aspx?id=" + COMMENT_ID;
                    $.get(url.toString(), function (data) {
                        //refreshMainCommentsPanel();
                        var Comment_index = ui_id.replace("btnDeleteComment_inner_Lease_", "");
                        var data = jQuery.parseJSON($("#MainContent_hidden_lease_comments").val());
                        data.splice(parseInt(Comment_index), 1);
                        $("#MainContent_hidden_lease_comments").val(JSON.stringify(data));    //Save to content.

                        $("div[leaseadmincommentid='LeaseAdmin_" + COMMENT_ID + "']").remove();

                        if (window.location.pathname.indexOf("LeaseApplications") > 0) {
                            refreshMainCommentsPanel();
                        }

                        createLeaseAdminCommentUI();

                    });

                    $(this).dialog('close');
                    return true;
                },
                'Cancel': function () {
                    $(this).dialog('close');
                    return false;
                }
            }
        });
        $('#dialog-confirm-comment-delete').dialog('open');
    });
}
//-------------------Bind event on delete a comment---------------//

//------------------------Post a new comment---------------------//
function linkLeasePostClick() {
    window.onbeforeunload = function () { };
    var commenmt_data = $("#MainContent_txt_comment_lease").val();
    var check = false;
    if (commenmt_data != "") {
        if (commenmt_data.toString() != ConstComments.textAreaDefaultMessage.toString()) {
            check = true;
        }
        else {
            check = false;
        }
    }
    else {
        check = false;
    }

    if (check) {
        $.ajax({ type: 'POST',
            url: getBaseURL() + "LeaseAdminTracker/LEComment/Add.aspx?id=" + $('#MainContent_hidden_lease_admin_id').val(),
            data: { comment: commenmt_data },
            success: function (data) {
                $("#MainContent_hidden_lease_comments").val(data);
                $("#MainContent_txt_comment_lease").val("");
                createLeaseAdminCommentUI()

                if (window.location.pathname.indexOf("LeaseApplications") > 0) {
                    refreshMainCommentsPanel();
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                UpdateLeaseCommentDialog('Fail', "Comment on unregist Lease Admin page is not allow.");

            },
            dataType: "text"
        });
    }
}
//------------------------Post a new comment---------------------//

function UpdateLeaseCommentDialog(title, message) {
    // Delete Modal Dialog
    var delDialogHtml = '<div id="dialog-message" title="' + title + '">' +
                        '<p>' + message + '</p></div>';
    $('#dialog-message').remove();
    $(delDialogHtml).dialog({});
    $('#dialog-message').dialog('open');
}

function setShowLeaseCommentBox() {
    var mainCommentCount = $("#MainContent_hidden_count_main_comment").val();
    if (parseInt(mainCommentCount) == 0) {
        showCommentBox();
    }
}

function setupLeaseCommentsLocal() {
    setupComments();
    setShowLeaseCommentBox();
}

