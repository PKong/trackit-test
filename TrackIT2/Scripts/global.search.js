﻿/*********************
* Global Search - JS
**********************/

// Global Var
var timerSearchHover;
var timerSearchHoverTimeout = 800;
var radGlobalSearchType = "radGlobalSearchType";
var txtSearchId = "txtGlobalSearch";
var btnSearchId = "btnGlobalSearch";
var clsSearchBox = "search-box";
var sSearch = "Search";
var lblLeaseAppId = "lblLeaseApp";
var lblSiteId = "lblSite";
var lblTowerModId = "lblTowerMod";
var lblSdpId = "lblSdp";
var imgClearSearchId = "imgClearSearch";
var lblsArray = [lblLeaseAppId, lblSiteId, lblTowerModId, lblSdpId]; // un-used
//
var GlobalSearch = {
    searchQueryString: "gs",
    searchTypeQueryString: "gst",
    searchTypes:
    {
        LEASE_APP:
        {
            type: "LeaseApp",
            resultsUrl: "/LeaseApplications/"
        },
        SITE:
        {
            type: "Site",
            resultsUrl: "/sites/"
        },
        TOWER_MOD:
        {
            type: "TowerMod",
            resultsUrl: "/TowerModifications/"
        },
        SDP:
        {
            type: "SDP",
            resultsUrl: "/SiteDataPackages/"
        }
    }
}
//>

// Setup Global Search
function setupGlobalSearch() {

    //debugger;

//    // Nav Search Box
//    $('#' + txtSearchId).mouseenter(function () {
//        showSearch();
//    }).mouseleave(function () {
//        handleSearchHidingTimer();
//    });
//    //
//    $('.' + clsSearchBox).mouseenter(function () {
//        showSearch();
//    }).mouseleave(function () {
//        handleSearchHidingTimer();
//    });
    //    //>

    // Nav Search Box
    //Added the Only onfocus open search field 2012/02/28
    $('#' + txtSearchId).focus(function () {
        showSearch();
    });
    //
    $('.' + clsSearchBox).focus(function () {
        showSearch();
    }).mouseleave(function () {
        handleSearchHidingTimer();
    });
    //>

    // Clear Search - Image Button
    $('#' + imgClearSearchId).mouseenter(function () {
        showSearch();
    }).mouseleave(function () {
        handleSearchHidingTimer();
    });
    //
    $('#' + imgClearSearchId).click(function () {
        $('#' + txtSearchId).val('').focus();
        $(this).hide();
    });
    //>

    // Reinstate Saved Params
    var gs = $.urlParam(GlobalSearch.searchQueryString);
    var gst = $.urlParam(GlobalSearch.searchTypeQueryString);
    if ((!String(gs).isNullOrEmpty()) && (!String(gst).isNullOrEmpty())) {

        $('#' + txtSearchId).val(unescape(gs));

        $('input:radio').each(function () {
            if ($(this).val() == gst) {
                $(this).attr('checked', true);
            }
        });

        $('#' + imgClearSearchId).show();
    }
    //>

    // Binding - Search Box
    $('#' + txtSearchId).bind("focus", function () {
        if ($(this).val() == sSearch) {
            $(this).val('');
        }
    });
    //
    $('#' + txtSearchId).bind("blur", function () {
        if (String($(this).val()).isNullOrEmpty()) {
            $(this).val(sSearch);
        }
    });
    //
    $('#' + txtSearchId).bind("keydown", function (event) {
        if (event.keyCode == 13) {
            return initGlobalSearch();
        }
    });
    //
    $('#' + txtSearchId).bind("keyup", function (event) {
        if (event.keyCode != 13) {
            var sSearchTerm = $('#' + txtSearchId).val();
            if ((sSearchTerm != sSearch) && (!String(sSearchTerm).isNullOrEmpty())) {
                $('#' + imgClearSearchId).show();
            }
        }
    });
    //>

    // Binding - Labels
    // TO-DO: put into a loop assign bind function, in the future.
    $('#' + lblLeaseAppId).bind("click", function () {
        $('input[name=' + radGlobalSearchType + ']:nth(0)').attr('checked', true);
    });
    $('#' + lblSiteId).bind("click", function () {
        $('input[name=' + radGlobalSearchType + ']:nth(1)').attr('checked', true);
    });
    $('#' + lblTowerModId).bind("click", function () {
        $('input[name=' + radGlobalSearchType + ']:nth(2)').attr('checked', true);
    });
    $('#' + lblSdpId).bind("click", function () {
        $('input[name=' + radGlobalSearchType + ']:nth(3)').attr('checked', true);
    });
    //>

    // Binding - Global Search
    $('#' + btnSearchId).bind("click", function () {
        return initGlobalSearch();
    });
    //>
}
//-->

// Init Global Search
function initGlobalSearch() {

    var sSearchType = $('input[name=' + radGlobalSearchType + ']:checked').val();
    var sSearchTerm = escape($.trim($('#' + txtSearchId).val()));

    if ((!String(sSearchType).isNullOrEmpty()) && (sSearchTerm != sSearch) && (!String(sSearchTerm).isNullOrEmpty())) {

        var sQsKey = GlobalSearch.searchQueryString;
        var sTQsKey = GlobalSearch.searchTypeQueryString;

        switch (sSearchType) {

            // Lease Applications  
            case GlobalSearch.searchTypes.LEASE_APP.type:
                //                window.parent.location.href = GlobalSearch.searchTypes.LEASE_APP.resultsUrl + '?' + sTQsKey + '=' + sSearchType + '&' + sQsKey + '=' + sSearchTerm;
                window.parent.location.href = GlobalSearch.searchTypes.LEASE_APP.resultsUrl + '?' + sQsKey + '=' + sSearchTerm;
                break;

            // SDP    
            case GlobalSearch.searchTypes.SDP.type:
                window.parent.location.href = GlobalSearch.searchTypes.SDP.resultsUrl + '?' + sTQsKey + '=' + sSearchType + '&' + sQsKey + '=' + sSearchTerm;
                break;

            // Tower Modifcations     
            case GlobalSearch.searchTypes.TOWER_MOD.type:
                window.parent.location.href = GlobalSearch.searchTypes.TOWER_MOD.resultsUrl + '?' + sTQsKey + '=' + sSearchType + '&' + sQsKey + '=' + sSearchTerm;
                break;

            // Sites
            case GlobalSearch.searchTypes.SITE.type:
                window.parent.location.href = GlobalSearch.searchTypes.SITE.resultsUrl + '?' + sTQsKey + '=' + sSearchType + '&' + sQsKey + '=' + sSearchTerm;
                break;
        }
    }
    return false;
}
//-->

// Show Search
function showSearch() {

    resetSearchHidingTimer();
    $('.' + clsSearchBox).show();
}
//-->

// reset Search Hiding Timer
function resetSearchHidingTimer() {

    if (timerSearchHover) {
        clearTimeout(timerSearchHover);
        timerSearchHover = null;
    }
}

// Handle Search Hiding Timer
function handleSearchHidingTimer() {

    resetSearchHidingTimer();
    timerSearchHover = setTimeout(function () {
        $('.' + clsSearchBox).fadeOut("fast");
    }, timerSearchHoverTimeout);
}
//-->

