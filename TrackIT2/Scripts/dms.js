﻿/*********************
* DMS - JS
**********************/

// Global Var
var sSpinnerHeader = "";
var sBreadcrumbTrail = "";
var sRootBasePath = "";
var sHiddenBasePath = "";
var CurrentDir = "";
var old_data = [];
var changeDir = false;
var sortdata;
var bIsLoading = false;
/** Constants **/
var DmsConstants = {
    dialogWidth: 430,
    dialogIframeMinWidth: 350,
    dialogIframeMinHeight: 555,
    modalLoadingMessage: "LOADING... PLEASE WAIT",
    modalUploadDocumentPageUrl: "/DMS/UploadDocument.aspx",
    actionDownload: "Download",
    actionEdit: "Edit",
    actionMove: "Move",
    actionDelete: "Delete",
    dialogDownloadingWidth: 200,
    dialogDownloadingHeight: 20,
    hfLocationHashId: "hidLocationHash",
    emptyLocationHash: "#/",
    dmsHandlerUrl: "/DMSFlexiGrid.axd",
    dmsApplicationHandlerUrl: "/ApplicationDocuments.axd",
    actionsUnavailable: "<em>Actions Unavailable</em>",
    flexGridTableId: "MainContent_fgDMS_fgtable",
    flexGridRowIdPrefix: "_row_",
    modalCreateFolderPageUrl: "/DMS/CreateFolder.aspx",
    dialogCreateFolderHeight: 260,
    fileExistsErrorMessage: "Error: a file with this name already exists!",
    folderExistsErrorMessage: "Error: a folder with this name already exists!",
    fileLockedErrorMessage: "A new version of '{0}' cannot be uploaded since the current version is locked.<br> Please unlock '{0}' before uploading a new version.",
    invalidSiteErrorMessage: "The site '{0}' does not exist.<br> Please specify an existing site.",
    allDocument: "All Documents",
    folderNotExist: "The specified Site ID folder does not exist."
};
/** /Constants **/

// Setup Create Folder Modal
function setupCreateFolderModal(title) {

    // Create New Dialog
    var optionsCreate = {
        autoOpen: false,
        width: DmsConstants.dialogWidth,
        modal: true,
        title: title,
        open: function (event, ui) {
            $('#modal-loading-create-folder').show();
            $('#modal-IframeId-create-folder').load(function () {
                $('#modal-loading-create-folder').fadeOut();
            });
        }
    };
    //
    $("#modalExternal-create-folder").dialog(optionsCreate);
    //
    $('#modalBtnExternal-create-folder').click(function () {
        $('#ui-dialog-title-modalExternal-create-folder').html(title);
        showCreateFolderDialog();
    });
}
//-->

// Show Create Folder Dialog
function showCreateFolderDialog(key, rowClassSelector, action) {

    var basePath = $("#hidDMSBasePath").val();
    if (action == "Edit") {
        $("#modalExternal-create-folder").html('<iframe id="modal-IframeId-create-folder" scrolling="no" style="width:100%;height:auto;overflow:hidden;min-height:' + DmsConstants.dialogCreateFolderHeight + 'px;min-width:' + DmsConstants.dialogIframeMinWidth + 'px;" marginWidth="0" marginHeight="0" frameBorder="0" scrolling="no" /><div id="modal-loading-create-folder">' + DmsConstants.modalLoadingMessage + '</div>').dialog("open");
    }
    else {
        var newHeight = DmsConstants.dialogCreateFolderHeight + 250;
        $("#modalExternal-create-folder").html('<iframe id="modal-IframeId-create-folder" scrolling="no" style="width:100%;height:auto;overflow:hidden;min-height:' + newHeight + 'px;min-width:' + DmsConstants.dialogIframeMinWidth + 'px;" marginWidth="0" marginHeight="0" frameBorder="0" scrolling="no" /><div id="modal-loading-create-folder">' + DmsConstants.modalLoadingMessage + '</div>').dialog("open");
    }
    if ((key !== null) && (rowClassSelector != null)) {
        $("#modal-IframeId-create-folder").attr("src", DmsConstants.modalCreateFolderPageUrl + "?key=" + key + "&rowClassSelector=" + rowClassSelector + "&basePath=" + basePath + "&action=" + action);
    }else {
        $("#modal-IframeId-create-folder").attr("src", DmsConstants.modalCreateFolderPageUrl + "?basePath=" + basePath);
    }
    return false;
}
//-->

// Setup DMS Modal
function setupDmsModal(title) {

    // Create New Dialog
    var optionsCreate = {
        autoOpen: false,
        width: DmsConstants.dialogWidth,
        modal: true,
        title: title,
        open: function (event, ui) {
            $('#modal-loading-dms').show();
            $('#modalDmsIframeId').load(function () {
                $('#modal-loading-dms').fadeOut();
            });
        }
    };
    //
    $("#modalExternalDms").dialog(optionsCreate);
    //
    $('#modalBtnExternalDms').click(function () {
        $('#ui-dialog-title-modalExternalDms').html(title);
        showUploadDocumentDialog(null);
    });
}
//-->


// Show Upload Document Dialog
function showUploadDocumentDialog(key, rowClassSelector, action) {

    var basePath = $("#hidDMSBasePath").val();

    if (basePath == "" || ($("#hidIsllDocumentFolder").val() == 'true') && key != null) {
        basePath = "/";
        var tempPath = decodeURIComponent(key).split('/');
        for (i = 0; i < tempPath.length - 1; i++) {
            basePath += tempPath[i] + '/';
        }
    }

    $("#modalExternalDms").html('<iframe id="modalDmsIframeId" scrolling="no" style="width:100%;height:100%;overflow:hidden;min-height:' + DmsConstants.dialogIframeMinHeight + 'px;min-width:' + DmsConstants.dialogIframeMinWidth + 'px;" marginWidth="0" marginHeight="0" frameBorder="0" scrolling="no" /><div id="modal-loading-dms">' + DmsConstants.modalLoadingMessage + '</div>').dialog("open");

    if ((key !== null) && (rowClassSelector != null)) {
       if ($('#ui-dialog-title-modalExternalDms').text() == "Move Document") {
          $("#modalDmsIframeId").attr("src", DmsConstants.modalUploadDocumentPageUrl + "?key=" + key + "&rowClassSelector=" + rowClassSelector + "&basePath=" + basePath + "&moveType=file&action=" + action);
        }
        else {
            $("#modalDmsIframeId").attr("src", DmsConstants.modalUploadDocumentPageUrl + "?key=" + key + "&rowClassSelector=" + rowClassSelector + "&basePath=" + basePath + "&moveType=folder&action=" + action);
        }
    }
    else {
        $("#modalDmsIframeId").attr("src", DmsConstants.modalUploadDocumentPageUrl + "?basePath=" + basePath);
    }

    return false;
}

function showUploadDocumentDialogOnFieldEditClick(typeID,key,field) {

    var basePath = $("#hidDMSBasePath").val();
    $('#ui-dialog-title-modalDocument').html('Edit Document');
    
    if (key != null) {
        basePath = "/";
        var tempPath = decodeURIComponent(key).split('/');
        for (i = 0; i < tempPath.length - 1; i++) {
            basePath += tempPath[i] + '/';
        }
    }

    $("#modalDocument").html('<iframe id="modalDmsIframeId" scrolling="no" style="width:100%;height:100%;overflow:hidden;min-height:' + DmsConstants.dialogIframeMinHeight + 'px;min-width:' + DmsConstants.dialogIframeMinWidth + 'px;" marginWidth="0" marginHeight="0" frameBorder="0" scrolling="no" /><div id="modal-loading-dms">' + DmsConstants.modalLoadingMessage + '</div>').dialog("open");
    $("#modalDmsIframeId").attr("src", DmsConstants.modalUploadDocumentPageUrl + "?key=" + key + "&typeID=" + typeID + "&field=" + field + "&basePath=" + basePath + "&action=" + DmsConstants.actionEdit + "&fromField=true");

    return false;
}
//-->

    /** FlexiGrid **/

    // FG Before Send Data
    function fgDMSBeforeSendData(data) {

        HideColumnName();

        // Resize column to normal
        $("#MainContent_fgDMS").find(".hDivBox").find('th[axis="col11"]').children().width(90)
        $("#MainContent_fgDMS_fgtable").find("tr").each(function () {
            $(this).children().last().children().width(90)
        });

        // Show - Loading Spinner
        $(sSpinnerHeader).activity({ segments: 8, width: 2.5, space: 0, length: 3, speed: 1.5, align: 'right' });

        // Location Hash
        // Check Location Hash
        removeFilterPostParam('hash');
        if ((window.location.hash !== '') && (window.location.hash !== DmsConstants.emptyLocationHash)) {
            $('#' + DmsConstants.hfLocationHashId).val(window.location.hash);
        } else {
            $('#' + DmsConstants.hfLocationHashId).val('');
        }
        var hash = $('#' + DmsConstants.hfLocationHashId).val();
        if (hash !== '') addFilterPostParam('hash', hash);

        //check change sort only or not? if change sort will sort from static dat
        sortdata =  false;
        if (old_data.length >= data.length) {
            for (i = 1; i < data.length; i++) {
                if ((data[i].name == 'sortorder') && (data[i-1].name == 'sortname')) {
                    if ((old_data[i].value != data[i].value) || (old_data[i - 1].value != data[i - 1].value) || (old_data[0].value != data[0].value)) {
                        sortdata = true;
                    }
                }
            }
        }
        if (changeDir) {
            changeDir = false;
            sortdata = false;
        }
        old_data = data;
        if (!sortdata) {
            sendSearchFilterDataToHandler(data);
        } else {
            data.push({ name: 'sortdata', value: true });
            sendSearchFilterDataToHandler(data);
        }
    }

    //
    // FG Data Load
    function fgDMSDataLoad(sender, args) {

        // Hide - Loading Spinner 
        $(sSpinnerHeader).activity(false);

        // Bind Folder Actions Select
        $('.sel-folder-actions').bind("change", function () {

            // Handle Folder Select Actions Change
            handleFolderSelectActionsChange(this);
        });
        //>

        // Bind File Actions Select
        $('.sel-file-actions').bind("change", function () {

            // Handle Select Actions Change
            handleSelectActionsChange(this);
        });
        //>

        //initial resizing flexigrid function
        InitaialResizeFlexigrid(currentFlexiGridID);

        // AJAX Load Items META from Amazon
        //if (!sortdata) {
            setTimeout('ajaxLoadItemsMetaFromAmazon();', 100);
        //}
            bIsLoading = false;
            //ajaxLoadItemsMetaFromAmazon();
    }
    //
    // FG Row Click
    function fgDMSRowClick(sender, args) {
        // return column to normal
        return false;
    }
    //
    // FG Row Render
    function fgDMSRowRender(tdCell, data, dataIdx) {
    
        /* Update By Jib */
        var sHtml = "";
        var cVal = new String(data.cell[dataIdx]);
        tdCell.innerHTML = cVal;
    }
    //
    // FG No Data
    function fgDMSNoData() {
    
        // Hide - Loading Spinner 
        $(sSpinnerHeader).activity(false);
        bIsLoading = false;
    }
    //-->

    // Show Actions - Loading Spinner 
    function showActionsLoadingSpinner(obj, showParentObj) {

        var parent = $(obj).parent();

        if ((showParentObj != null) && (showParentObj))
        {
            // Show Parent
            attachLoadingSpinnerToObject(parent, 'center', 'position:absolute;overflow:visble;width:112px;top:23px;');

        } else {
            
            // Hide Parent
            $(obj).fadeOut('fast', function () {
                attachLoadingSpinnerToObject(parent, 'center');
            });   
        }
    }
    //-->

    // Attach Loading Spinner To Object
    function attachLoadingSpinnerToObject (obj, position, style) {

        if (position == null) position = 'center';
        if (style == null) style = '';
        
        var div = $('<div style="color:#333;' + style + '" class="spinner-cont"></div>');
        $(obj).append(div);
        $(div).activity({ segments: 8, width: 2.5, space: 0, length: 3, speed: 1.5, align: position });
    }
    //-->

/** /FlexiGrid **/

/** DMS Functions **/

    // Init DMS Flexi Related Controls
    function InitDMSFlexiRelatedControls(ctrlHeaderName, ctrlBreadCrumbTrail, ctrlHiddenBasePath, sFixedBasePath) {

        //debugger;

        // Setup Create Folder Modal
        setupCreateFolderModal("Create Folder");

        // Setup Upload Document Modal
        setupDmsModal("Upload Document");
    
        // Controls
        sSpinnerHeader = ctrlHeaderName;
        sBreadcrumbTrail = ctrlBreadCrumbTrail;
        sHiddenBasePath = ctrlHiddenBasePath;
        
        // Default Paths
        sRootBasePath = sFixedBasePath;
        CurrentDir = sFixedBasePath + "/";
        //
        if (sFixedBasePath != "") {
            $(sHiddenBasePath).val(CurrentDir);
        }
        else {
            $(sHiddenBasePath).val(sRootBasePath);
        }
        //>

        // Check Location Hash
        if ((window.location.hash !== '') && (window.location.hash !== DmsConstants.emptyLocationHash)) {
            
            // Setup Hash
            $('#hidLocationHash').val(window.location.hash);
            var hash = window.location.hash.replace(DmsConstants.emptyLocationHash, "");
            
            // Build Breadcrumb
            BuildBreadcrumbTrail(hash);
        } else {
            
            // Have a Fixed Base Path (Default Folder)?
            if (sFixedBasePath != "") {
                $(sBreadcrumbTrail).html("<a href=\"javascript:ChangeDirectory('" + sFixedBasePath + "');\">" + sFixedBasePath + "</a>");
            }
        }


    }
    //-->

    // Go To Directory
    function GoToDir() {
        var siteId = $("#txtBasePath").attr("value").toUpperCase().trim();
        var param = "?checkExist=true&key=" + siteId;

        // Show - Loading Spinner
        $(sSpinnerHeader).activity({ segments: 8, width: 2.5, space: 0, length: 3, speed: 1.5, align: 'right' });

        if (siteId) {
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "/Handlers/SiteIDHandler.axd",
                async: false,
                data: {
                    term: siteId
                },
                error: function (xhr, status, error) {
                    DisplayErrorSiteNotFoundDialog("Error validating site. Please try again.");
                },
                success: function (data) {
                    if (data != "[]") {
                        var msg = "Site Id " + siteId + " does not exist. Please specify an existing site.";
                        DisplayErrorSiteNotFoundDialog(msg);
                    }
                    else {
                        $.ajax({
                            dataType: 'json',
                            url: DmsConstants.dmsHandlerUrl + param,
                            cache: false,
                            success: function (data) {
                                var object = data.rows[0];
                                if (object.cell[0] == 'true') {
                                    ChangeDirectory(siteId);
                                }
                                else {
                                    param = "?createNewFolder=true&key=" + siteId;
                                    $.ajax({
                                        dataType: 'json',
                                        url: DmsConstants.dmsHandlerUrl + param,
                                        async: false,
                                        error: function (xhr, status, error) {
                                            alert("Error validating site. Please try again.");
                                        },
                                        success: function (data) {
                                            var object = data.rows[0];
                                            if (object.cell[0] == 'true') {
                                                ChangeDirectory(siteId);
                                            }
                                            else {
                                                DisplayErrorSiteNotFoundDialog(DmsConstants.folderNotExist);
                                            }
                                        }
                                    });

                                    $(sSpinnerHeader).activity(false);
                                }
                            }
                        });
                    }
                }
            });
        }
        else {
            DisplayErrorSiteNotFoundDialog('Please specify a Site Id.');
            $(sSpinnerHeader).activity(false);
        }              
        return false;
    }
    //-->

    function DisplayErrorSiteNotFoundDialog(msg) {
        var confirmDoc = 'dialog-confirm-document-delete';
        var delDialogHtml = '<div id="' + confirmDoc + '" title="Error"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>' + msg + '</p></div>';

        $('#' + confirmDoc).remove();
        $(delDialogHtml).dialog({
            open: function (type, data) {
                $(this).parent().appendTo("form[0]");
            }
        });
        $('#dialog-confirm-document-delete').dialog('open');
        $(sSpinnerHeader).activity(false);
    }

    // Change Directory
    function ChangeDirectory(sPath,isGetAllDocument) {
		changeDir =true;
		if (sPath.length > 1) {
            //Add remove if first char in sPath is '/'  
		    if (sPath.substring(0, 1) == '/') sPath = sPath.substring(1, sPath.length-1)
			if (sPath.length > 1) {
            	if (sPath.substring(sPath.length - 1, sPath.length) !== '/') sPath += '/';
			}
      }
	
      removeFilterPostParam('fd');
      addFilterPostParam('fd', sPath);

        reloadDMSDataGrid(true);
        BuildBreadcrumbTrail(sPath, isGetAllDocument);
    }
    //-->

    // Build Breadcrumb Trail
    function BuildBreadcrumbTrail(sPath, isGetAllDocument) {

        // Location Hash
        if (sPath !== "/") sPath = '/' + sPath;
        location.hash = sPath;
    
        //Add Home first
        var sDir = "";
        if (sRootBasePath == "") {
            sDir += "<a href=\"javascript:void(0);\" onclick=\"ChangeDirectory('/');\">Home</a>";
        }
        else {
            sDir += "<a href=\"javascript:void(0);\" onclick=\"ChangeDirectory('" + sRootBasePath + "');\">" + sRootBasePath + "</a>";
        }

        var sCompletePath = "";
        //link sub folder in case root path is not home
        if (sRootBasePath != "") {
            sCompletePath += sRootBasePath + "/";
        }

        var sSubDir = sPath.split("/");
        $("#hidIsllDocumentFolder").val('false');

        if ($("#modalBtnExternal-create-folder").hasClass('ui-state-disabled')){
            $("#modalBtnExternal-create-folder").removeClass('ui-state-disabled');
            $("#modalBtnExternal-create-folder").removeAttr('disabled');

        }
        if ($("#modalBtnExternalDms").hasClass('ui-state-disabled')) {
            $("#modalBtnExternalDms").removeClass('ui-state-disabled');
            $("#modalBtnExternalDms").removeAttr('disabled');

        }

        if (sSubDir.length > 0) {
            if (sSubDir.length > 2 && sSubDir[2] == DmsConstants.allDocument) {
                isGetAllDocument = true;
            }
            // check from SiteDashboard
            if (window.location.pathname.indexOf("SiteDashboard") > 0) {
                if (isGetAllDocument) {
                    $("#hidIsllDocumentFolder").val('true');
                    $("#modalBtnExternal-create-folder").addClass('ui-state-disabled');
                    $("#modalBtnExternal-create-folder").attr('disabled', true)
                    $("#modalBtnExternalDms").addClass('ui-state-disabled');
                    $("#modalBtnExternalDms").attr('disabled', true)
                }
                else {
                    $("#modalBtnExternal-create-folder").css('display', 'inline');
                    $("#modalBtnExternalDms").css('display', 'inline');
                    $("#hidIsllDocumentFolder").val('false');
                }
            }
            //-->

            for (var index in sSubDir) {
                if (sSubDir[index] != "" && sSubDir[index] != sRootBasePath) {
                    sCompletePath += sSubDir[index] + "/";
                    sDir += " / <a href=\"javascript:void(0);\" onclick=\"ChangeDirectory('" + sCompletePath +
                        "');\">" + sSubDir[index] + "</a>";
                    $("#hidIsllDocumentFolder").val('false');
                    $("#modalBtnExternal-create-folder").css('display', 'inline');
                    $("#modalBtnExternalDms").css('display', 'inline');

                    if (isGetAllDocument) {
                        $("#hidIsllDocumentFolder").val('true');

                        $("#modalBtnExternal-create-folder").addClass('ui-state-disabled');
                        $("#modalBtnExternal-create-folder").attr('disabled', true)
                        $("#modalBtnExternalDms").addClass('ui-state-disabled');
                        $("#modalBtnExternalDms").attr('disabled', true)
                    }
                }
            }
        }

        setTimeout('SetColumn();', 100);
        CurrentDir = sPath;
        $(sHiddenBasePath).val(sPath);
        $(sBreadcrumbTrail).html(sDir);
    }
    //-->

    // Clear All Previous Post Param
    function ClearAllPreviousPostParam(isExceptFolder) {
    
        if (isExceptFolder) {
            removeFilterPostParam('fd');
            removeFilterPostParam('del');
            removeFilterPostParam('chk');
            removeFilterPostParam('hash');
        }
        else {
            removeFilterPostParam('dl');
            removeFilterPostParam('del');
            removeFilterPostParam('chk');
            removeFilterPostParam('hash');
        }

    }
    //-->

    // On Edit Folder Click
    function OnEditFolderClick(key, rowClassSelector) {

        $('#ui-dialog-title-modalExternal-create-folder').html('Edit Folder');
        showCreateFolderDialog(key, rowClassSelector, DmsConstants.actionEdit);
    }

    // On Move Folder Click
    function OnMoveFolderClick(key, rowClassSelector) {

        $('#ui-dialog-title-modalExternal-create-folder').html('Move Folder');
        showCreateFolderDialog(key, rowClassSelector, DmsConstants.actionMove);
    }
    //-->

    // On Edit Click
    function OnEditClick(key, rowClassSelector) {

        $('#ui-dialog-title-modalExternalDms').html('Edit Document');
        showUploadDocumentDialog(key, rowClassSelector, DmsConstants.actionEdit);
    }

    // On Move Click
    function OnMoveClick(key, rowClassSelector) {

        $('#ui-dialog-title-modalExternalDms').html('Move Document');
        showUploadDocumentDialog(key, rowClassSelector, DmsConstants.actionMove);
    }
    //-->

    // On Check In / Out Click
    function OnCheckInOutClick(key, obj) {

        showActionsLoadingSpinner(obj);
        
        var rowClassSelector = $(obj).parent().parent().parent().attr('class').replace('erow', '').replace('trSelected', '').trim();
        loadDmsFlexiGridSingleFileRow(key, rowClassSelector, false, true);

    }
    //-->

    // On Delete Click
    function OnDeleteClick(key, obj) {

        // Delete Modal Dialog
        var confirmDoc = 'dialog-confirm-document-delete';
        var delDialogHtml = '<div id="' + confirmDoc + '" title="Delete this item?"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This item will be permanently deleted and cannot be recovered. Are you sure?</p></div>';
    
        $('#' + confirmDoc).remove();
        $(delDialogHtml).dialog({
            open: function (type, data) {

                $(this).parent().appendTo("form[0]");
            },
            buttons: {
                'Confirm': function () {
                
                    showActionsLoadingSpinner(obj);
                    addFilterPostParam('del', key);
                    reloadDMSDataGrid();
                    ClearAllPreviousPostParam(false);
                    $(this).dialog('close');
                    return true;

                },
                'Cancel': function () {
                
                    $(this).dialog('close');
                    return false;
                }
            }
        });
        $('#dialog-confirm-document-delete').dialog('open');

    }
    //-->

    // On Download Click
    function OnDownloadClick(key) {

        if ($('#MainContent_hidDocumentDownload').length > 0) {
            $('#MainContent_hidDocumentDownload').val(key);
        }
        if ($('#hidDocumentDownload').length > 0) {
            $('#hidDocumentDownload').val(key);
        }
        __doPostBack('', 'DownloadDocument');
    }
    //-->

    // AJAX Load Items META from Amazon
    function ajaxLoadItemsMetaFromAmazon() {

        //debugger;

        // Var
        var tableId = DmsConstants.flexGridTableId;
        var rowIdPrefix = tableId + DmsConstants.flexGridRowIdPrefix;
        var rowIdCounter = 0;
        var bHasfile = false;
        
        // Iterate FlexiGrid Rows - SHOW PRELOADER
        $('#' + tableId).find('tr').each(function () {

            // Check if File or Folder?
            var id = new String($(this).attr('id'));
            var lastChar = id.substr(id.length - 1, 1);
            if($(this).find('img[title]').length >0) {
                $(this).find('div').addClass('dms-overflow-control');
            }

            if (lastChar !== '/') {
                var td = $(this).children(':nth-child(2)');
                var doc = $(td).children(':first').children(':first');
                showActionsLoadingSpinner(doc, true);
            }
        });
        //>

        // Iterate FlexiGrid Rows - AJAX LOAD
        $('#' + tableId).find('tr').each(function () {

            // Setup
            var id = new String($(this).attr('id'));
            var lastChar = id.substr(id.length - 1, 1);
            var rowIdAsClass = rowIdPrefix + rowIdCounter.toString();
            rowIdCounter++;

            // Add a unique ID as Class for later use (row already has ID from Key)
            $(this).addClass(rowIdAsClass);

            // Check if File or Folder?
            if (lastChar !== '/') {

                // Load META
                var key = encodeURIComponent(id.substr(3, id.length));

                // Load Dms FlexiGrid Single File Row

                if (!checkApplicationDocument()) {
                    loadDmsFlexiGridSingleFileRow(key, rowIdAsClass);
                }
                else {
                    loadApplicationDocumentFlexiGridSingleFileRow(key, rowIdAsClass);
                }
                bHasfile = true;
            }
        });

        //>

        $('#MainContent_fgDMS').find('.ndcol1').css('display', '');
        // work around
        var location = false;
        if (window.location.href.indexOf('LeaseApplications') != -1) {
            location = true;
        }

        if ($('#MainContent_fgDMS_fgtable').width() > 0) {
            if ($('#MainContent_fgDMS_fgtable').width() > $('#MainContent_fgDMS').width()) {
                var width = $('#MainContent_fgDMS_fgtable').width() - $('#MainContent_fgDMS').width() + 1;
                var newObjWidth = $("th[abbr='Metadata.Description']").children().width() - width;
                $("th[abbr='Metadata.Description']").children().width(newObjWidth);
                $("tr").find("td[abbr='Metadata.Description']").each(function () {
                    $(this).children().width(newObjWidth);
                });

                $('#MainContent_fgDMS').find('.flexigrid').width($('#MainContent_fgDMS').width())
            }
            else if ($('#MainContent_fgDMS_fgtable').width() < $('#MainContent_fgDMS').width()) {
            
                var width = $('#MainContent_fgDMS').width() - $('#MainContent_fgDMS_fgtable').width() - 1;
                var newObjWidth = $("th[abbr='Metadata.Description']").children().width() + width;
                $("th[abbr='Metadata.Description']").children().width(newObjWidth);
                $("tr").find("td[abbr='Metadata.Description']").each(function () {
                    $(this).children().width(newObjWidth);
                });

                $('#MainContent_fgDMS').find('.flexigrid').width($('#MainContent_fgDMS').width())                    
            }
            else {
                var newWidth = $("th[abbr='Metadata.Description']").children().width() - 1;
                $("th[abbr='Metadata.Description']").children().width(newWidth);
                $("tr").find("td[abbr='Metadata.Description']").each(function () {
                    $(this).children().width(newWidth);
                });
            }
        }
        if (!location) {
            SetCDragLocation(currentFlexiGridID);
        }
        else {
            SetCDragLocationOnLeaseApplicationPage(currentFlexiGridID);
        }
        //}
    }
    //-->

    function loadApplicationDocumentFlexiGridSingleFileRow(key, rowIdClass) {

        // Default Action (Load Row META)
        var actions = '?requestType=loadApplicationDocument&key=' + key;

        var row = $('.' + rowIdClass);
        var td = $(row).children(':nth-child(2)');
        var doc = $(td).children(':first').children(':first');
        var tdoption = $(row).children(':first');
        var expander = $(tdoption).children(':first').children(':first');

        var isPseudoFolder = false;
        var isPsedu = $("#sBasePath").find('a[ispsedu_folder]');
        if (isPsedu.length > 0) {
            actions += "&pseudoFoler=true";
        }

        // Show loading spinner
        if ((showLoadingSpinner != undefined) && (showLoadingSpinner != null) && (showLoadingSpinner == true)) {

            showActionsLoadingSpinner(doc, true);
        }

        // Load JSON
        $.ajax({
            dataType: 'json',
            url: DmsConstants.dmsApplicationHandlerUrl + actions,
            cache: false,
            success: function (data) {

                // Process JSON / Response
                var objJson = data.rows[0];
                setupDmsFlexGridRowMeta(rowIdClass, objJson, true);

            }
        });
    
    }

    // Load Dms FlexiGrid Single Folder Row
    function loadDmsFlexiGridSingleFolderRow(key, rowIdAsClass, folderNameNew) {

        // Base Path
        var basePath = $(sHiddenBasePath).val();
        if (basePath.length > 0) {
            if (basePath.substr(0, 1) === '/') basePath = basePath.substr(1, basePath.length);
        }

        // Get Item Row, TD & Doc
        var row = $('.' + rowIdAsClass);
        var td = $(row).children(':first');
        var folder = $(td).children(':first').children(':first');

        // Update Folder Name
        var folderChildren = $(folder).children();
        $(folder).empty();
        $(folder).append($(folderChildren)).append(folderNameNew);

        // Update Folder Action
        $(folder).attr("onclick", "ChangeDirectory('" + basePath + key + "');showActionsLoadingSpinner(this, true);");
    }
    //-->

    // Load Dms FlexiGrid Single File Row
    function loadDmsFlexiGridSingleFileRow(key, rowIdAsClass, showLoadingSpinner, checkingOutOrIn, fileNameHasChanged, fileNameNew, oldfilename) {

        // Base Path
        var basePath = $(sHiddenBasePath).val();
        if (basePath.length > 0) {
            if (basePath.substr(0, 1) === '/') {
                if ($("#hidIsllDocumentFolder").val() != 'true') {
                    basePath = basePath.substr(1, basePath.length);
                }
                else {
                    basePath = "";
                    var tempPath = decodeURIComponent(key).split('/');
                    for (i = 0; i < tempPath.length - 1; i++) {
                        basePath += tempPath[i] + '/';
                    }
                }
            }
        }

        //remove expand and sho-revision status
        var bIsExpanded = false;
        for (i = 0; i < rowIdAsClass.split(' ').length; i++) {
            if (rowIdAsClass.split(' ')[i] == 'rev-expanded') {
                bIsExpanded = true;
            }
        }

        // Default Action (Load Row META)
        var actions = '?requestType=loadItemMeta&key=' + key;
        // Checking Out or In?
        if ((checkingOutOrIn != undefined) && (checkingOutOrIn != null) && (checkingOutOrIn == true)) {
            if (basePath.substr((basePath.length - 1), 1) === '/') basePath = basePath.substr(0, basePath.length - 1);
            var arrKey = key.split('|');
            actions = '?requestType=checkOutOrIn&key=' + arrKey[0] + '&checkVal=' + arrKey[1] + "&basePath=" + basePath;
        }

        var isPsedu = $("#sBasePath:contains('All Documents')");
        if (isPsedu.length > 0) {
            actions += "&pseudoFolder=true";
        }
        //debugger;

        // Get Item Row, TD & Doc
        rowIdAsClass = rowIdAsClass.replace(' rev-expanded', '');
        rowIdAsClass = rowIdAsClass.replace(' show-rev', '');

        var row = $('.' + rowIdAsClass);
        var td = $(row).children(':nth-child(2)');
        var doc = $(td).children(':first').children(':first');
        var tdoption = $(row).children(':first');
        var expander = $(tdoption).children(':first').children(':first');


        // File name has changed?
        if ((fileNameHasChanged != undefined) && (fileNameHasChanged != null) && (fileNameHasChanged == true)) {

            //remove expanded from class
            $(row).removeClass('rev-expanded');
            $(row).removeClass('show-rev');

            // Reload Select HTML Action from Handler
            actions += '&loadActions=true';
            
            // Update File Name
            var docChildren = $(doc).children();
            $(doc).empty();
            $(doc).append($(docChildren)).append(decodeURIComponent(fileNameNew).split('+').join(' '));

            // Update download action
            $(doc).attr("onclick", "OnDownloadClick('" + basePath + fileNameNew + "');");
            $(expander).attr("id", decodeURIComponent(key));
            $(expander).attr("onclick", "ExpandRevision(this,'" +  decodeURIComponent(key).replace(/ /g,"|") + "','" + basePath + "');");
            $(row).attr('id', 'row' + decodeURIComponent(key));
            //onclick="OnDownloadClick('1111/111.png');"

            //Remove expanded revision file
            if (bIsExpanded) {
                //remove old expanded from table
                if ($(row).children(':first').children(':first').children(':first').children(':first') != null) {
                    $(row).children(':first').children(':first').children(':first').children(':first').attr('src', '/images/icons/details_open.png');
                }
                //remove border expanded revsion
                $(row).children(':first').css('border-left', '');
                $(row).children(':nth-child(11)').css('border-right', '');

                if ((oldfilename != undefined) && (oldfilename != null) && (oldfilename != '')) {
                    $('tr').each(function () {
                        att = $(this).attr('name');
                        if (att != null) {
                            if (att.split('+').join(' ') == ('show_revision_' + decodeURIComponent(oldfilename)).split('+').join(' ')) {
                                $(this).remove();
                            }
                        }
                    });
                }
            }
        }
        //>


        //add action for revision
		if(rowIdAsClass.split('_').length > 1)
		{
        	if (rowIdAsClass.split('_')[rowIdAsClass.split('_').length - 1] == 'rev') {// Reload Select HTML Action from Handler
            	actions += '&loadActions=true';

            	actions += '&revison=true';
            	// Update File Name
            	$(doc).empty();
            	$(doc).append(fileNameNew);

            	// Update download action
            	$(doc).attr("onclick", "OnDownloadClick('" + decodeURIComponent(key) + "');");
            }
		}
        // Show loading spinner
        if ((showLoadingSpinner != undefined) && (showLoadingSpinner != null) && (showLoadingSpinner == true)) {
            
            showActionsLoadingSpinner(doc, true);
        }
        
        // Load JSON
        $.ajax({
            dataType: 'json',
            url: DmsConstants.dmsHandlerUrl + actions,
            cache: false,
            success: function (data) {
                //debugger;
                // Process JSON / Response
                var objJson = data.rows[0];

                setupDmsFlexGridRowMeta(rowIdAsClass, objJson);

                //>
                if (bIsExpanded) {
                    var revfilename = decodeURIComponent(key);
                    if (key.split('|').length > 1) {
                        revfilename = decodeURIComponent(key.split('|')[0]);
                    }
                    $('tr').each(function () {
                        att = $(this).attr('name');
                        if (att != null) {
                            if (att.split('+').join(' ') == ('show_revision_' + decodeURIComponent(revfilename)).split('+').join(' ')) {
                                var colAction = null;

                                if ($.browser.msie) {
                                    colAction = $(this).children(':nth-child(12)');
                                } else {
                                    if (isPsedu) {
                                        colAction = $(this).children(':nth-child(12)');
                                    }
                                    else {
                                        colAction = $(this).children(':nth-child(11)');
                                    }
                                }
                                $(colAction).each(function () {
                                    // if (objJson.cell.length >= 10) {
                                    if (objJson.cell[7] == true) {
                                        $(this).find('div').html(DmsConstants.actionsUnavailable);
                                    } else if (objJson.cell[9] != null && objJson.cell[9] !== '') {
                                        var id = $(this).parent().attr('id');
                                        if ($(this).parent().attr('id').split('_rev_').length > 0) {
                                            id = $(this).parent().attr('id').split('_rev_')[0];
                                        }
                                        var arguments = [];
                                        arguments.push(id);
                                        var actionHtml = StingFormat(objJson.cell[9], arguments);
                                        $(this).find('div').html(actionHtml);
                                        // Actions Select
                                        var select = $(colAction).children(':first').children();
                                        $(select).bind("change", function () {
                                            handleSelectActionsChange(select);
                                        });
                                        $(select).removeAttr('disabled');
                                    }
                                    //  }
                                });
                            }
                        }
                    });
                }
            }
        });
        //>
    }
    //-->
    
    // Setup DMS FlexGrid Row META
    function setupDmsFlexGridRowMeta(rowIdAsClass, objJson, applicatioData) {
        // Var
        if (applicatioData == null) {
            applicatioData = false;
        }
        var shipIndex = 0;

        var row = $('.' + rowIdAsClass);
        if (rowIdAsClass.split('_').length > 0) {
            if (!checkApplicationDocument()) {
                $(row).find('td').each(function (i) {
                    // Set Row Values
                    //Shift cell index+1 for expand/collapse icon by Kantorn J. 2012-08-15
                    switch (i) {
                        case 0:
                            //option
                            if (applicatioData) {
                                $(this).find('div').html(objJson.cell[11]);
                            }
                            break;
                        case 1:
                            //fileName
                            $(this).find('div').attr('title', objJson.cell[10]);
                            break;
                        case 2:
                            //folder
                            $(this).find('div').html(objJson.cell[11]);
                            $(this).find('div').attr('title', objJson.cell[11]);
                            break;
                        case 3:
                            // Classification
                            $(this).find('div').html(objJson.cell[0]);
                            $(this).find('div').attr('title', objJson.cell[0]);
                            break;
                        case 5:
                            // Desription
                            $(this).find('div').html(objJson.cell[3]);
                            $(this).find('div').attr('title', objJson.cell[3]);
                            break;
                        case 4:
                            // Keywords
                            $(this).find('div').html(objJson.cell[2]);
                            $(this).find('div').attr('title', objJson.cell[2]);
                            break;
                        case 6:
                            // Size
                            var a = 0;
                            $(this).find('div').attr('title', $(this).find('div').html())
                            if (applicatioData) {
                                $(this).find('div').html(objJson.cell[12]);
                            }
                            break;
                        case 7:
                            // Uploaded By
                            $(this).find('div').html(objJson.cell[4]);
                            $(this).find('div').attr('title', objJson.cell[4]);
                            break;
                        case 8:
                            // Rev
                            var a = 0;
                            $(this).find('div').attr('title', $(this).find('div').html())
                            if (applicatioData) {
                                $(this).find('div').html(objJson.cell[13]);
                            }
                            break;
                        case 9: //Shift cell index+1 for revision
                            // Upload Date
                            $(this).find('div').html(objJson.cell[5]);
                            $(this).find('div').attr('title', objJson.cell[5]);
                            break;
                        case 10: //Shift cell index+1 for revision
                            // Check In / Out
                            $(this).find('div').html(objJson.cell[6]);
                            break;
                        case 11: //Shift cell index+1 for revision
                            // Actions
                            if (objJson.cell[7] == true) {

                                // No Actions!
                                $(this).find('div').html(DmsConstants.actionsUnavailable);
                            } else if (objJson.cell[8] != null && objJson.cell[8] !== '') {

                                // Actions Select
                                var select = $(objJson.cell[8]);

                                // Bind Actions Select
                                $(select).bind("change", function () {

                                    // Handle Select Actions Change
                                    handleSelectActionsChange(this);
                                });
                                //>

                                $(this).find('div').html(select);
                            }
                            $(this).find('.sel-file-actions').removeAttr('disabled');
                            break;
                    }
                });
            }
            else {
                $(row).find('td').each(function (i) {
                    // Set Row Values
                    //Shift cell index+1 for expand/collapse icon by Kantorn J. 2012-08-15
                    switch (i) {
                        case 0:
                            //option
                            if (applicatioData) {
                                $(this).find('div').html(objJson.cell[11]);
                            }
                            break;
                        case 1:
                            //fileName
                            $(this).find('div').attr('title', objJson.cell[10]);
                            break;
                        case 2:
                            // Classification
                            $(this).find('div').html(objJson.cell[0]);
                            $(this).find('div').attr('title', objJson.cell[0]);
                            break;
                        case 4:
                            // Desription
                            $(this).find('div').html(objJson.cell[3]);
                            $(this).find('div').attr('title', objJson.cell[3]);
                            break;
                        case 3:
                            // Keywords
                            $(this).find('div').html(objJson.cell[2]);
                            $(this).find('div').attr('title', objJson.cell[2]);
                            break;
                        case 5:
                            // Size
                            var a = 0;
                            $(this).find('div').attr('title', $(this).find('div').html())
                            if (applicatioData) {
                                $(this).find('div').html(objJson.cell[12]);
                            }
                            break;
                        case 6:
                            // Uploaded By
                            $(this).find('div').html(objJson.cell[4]);
                            $(this).find('div').attr('title', objJson.cell[4]);
                            break;
                        case 7:
                            // Rev
                            var a = 0;
                            $(this).find('div').attr('title', $(this).find('div').html())
                            if (applicatioData) {
                                $(this).find('div').html(objJson.cell[13]);
                            }
                            break;
                        case 8: //Shift cell index+1 for revision
                            // Upload Date
                            $(this).find('div').html(objJson.cell[5]);
                            $(this).find('div').attr('title', objJson.cell[5]);
                            break;
                        case 9: //Shift cell index+1 for revision
                            // Check In / Out
                            $(this).find('div').html(objJson.cell[6]);
                            break;
                        case 10: //Shift cell index+1 for revision
                            // Actions
                            if (objJson.cell[7] == true) {

                                // No Actions!
                                $(this).find('div').html(DmsConstants.actionsUnavailable);
                            } else if (objJson.cell[8] != null && objJson.cell[8] !== '') {

                                // Actions Select
                                var select = $(objJson.cell[8]);

                                // Bind Actions Select
                                $(select).bind("change", function () {

                                    // Handle Select Actions Change
                                    handleSelectActionsChange(this);
                                });
                                //>

                                $(this).find('div').html(select);
                            }
                            $(this).find('.sel-file-actions').removeAttr('disabled');
                            break;
                    }
                });
            }
        } 
        //>
        // Remove Loading Spinner
        var td = $(row).children(':nth-child(2)');
        $(td).find('.spinner-cont').fadeOut('fast', function () { $(this).remove(); });

        //add css overflow control ref:619
        $('#MainContent_fgDMS_fgtable').find('div[title]').addClass("dms-overflow-control");
    }
    //-->

    // Handle Folder Select Actions Change
    function handleFolderSelectActionsChange(obj) {

        if ($(obj).prop("selectedIndex") > 0) {

            var actionType = $(obj).find('option:selected').text().toLowerCase();
            var selectedVal = $(obj).val();

            switch (actionType) {

                // Edit               
                case DmsConstants.actionEdit.toLowerCase():
                    var rowClassSelector = $(obj).parent().parent().parent().attr('class').replace('erow', '').replace('trSelected', '').trim();
                    OnEditFolderClick(selectedVal, rowClassSelector);
                    break;

                // Move                
                case DmsConstants.actionMove.toLowerCase():
                    var rowClassSelector = $(obj).parent().parent().parent().attr('class').replace('erow', '').replace('trSelected', '').trim();
                    OnMoveFolderClick(selectedVal, rowClassSelector);
                    break;

                // Delete               
                case DmsConstants.actionDelete.toLowerCase():
                    OnDeleteClick(selectedVal, obj);
                    break;
            }

            // Reset Select
            $(obj).prop('selectedIndex', 0);
        }
    }
    //-->
    
    // Handle Select Actions Change
    function handleSelectActionsChange(obj) {

        if ($(obj).prop("selectedIndex") > 0) {

            var actionType = $(obj).find('option:selected').text().toLowerCase();
            var selectedVal = $(obj).val();

            switch (actionType) {

                // Download              
                case DmsConstants.actionDownload.toLowerCase():
                    OnDownloadClick(selectedVal);
                    break;

                // Edit              
                case DmsConstants.actionEdit.toLowerCase():
                    var rowClassSelector = $(obj).parent().parent().parent().attr('class').replace('erow', '').replace('trSelected', '').trim();
                    OnEditClick(selectedVal, rowClassSelector);
                    break;

                // Move               
                case DmsConstants.actionMove.toLowerCase():
                    var rowClassSelector = $(obj).parent().parent().parent().attr('class').replace('erow', '').replace('trSelected', '').trim();
                    
                    OnMoveClick(selectedVal, rowClassSelector);
                    break;

                // Delete              
                case DmsConstants.actionDelete.toLowerCase():
                    OnDeleteClick(selectedVal, obj);
                    break;
            }

            // Reset Select
            $(obj).prop('selectedIndex', 0);
        }
    }
    //-->

    // Handle File Name Already Exists In Modal
    function handleFileNameAlreadyExistsInModal(objType) {

        var errorMessage = DmsConstants.fileExistsErrorMessage;
        if (objType == 'folder') errorMessage = DmsConstants.folderExistsErrorMessage;

        $("#message_alert").html("");
        showMessageWarrningAlert("error", errorMessage);

        if (objType != 'folder') {
            $('.slow-alert').click(function () {
                var parent = $(this).parent();
                $(parent).fadeOut('slow');
            });
            var currentBrowser = navigator.userAgent;
            if (currentBrowser.indexOf('MSIE') > 0) {
                $("#MainContent_TreePanel").css('height', '125px');
                $("#MainContent_localtion_tree").css('height', '200px');
            }
            else if (currentBrowser.indexOf('Firefox') > 0) {
                $("#MainContent_TreePanel").css('height', '123px');
                $("#MainContent_localtion_tree").css('height', '198px');
            }
            else {
                $("#MainContent_TreePanel").css('height', '123px');
                $("#MainContent_localtion_tree").css('height', '198px');
            } 
        }
        else {
            $('.slow-alert').click(function () {
                var parent = $(this).parent();
                $(parent).fadeOut('slow');
            });
            var currentBrowser = navigator.userAgent;
            if (currentBrowser.indexOf('MSIE') > 0) {
                $("#MainContent_TreePanel").css('height', '85px');
                $("#MainContent_localtion_tree").css('height', '160px');
            }
            else if (currentBrowser.indexOf('Firefox') > 0) {
                $("#MainContent_TreePanel").css('height', '83px');
                $("#MainContent_localtion_tree").css('height', '158px');
            }
            else {
                $("#MainContent_TreePanel").css('height', '83px');
                $("#MainContent_localtion_tree").css('height', '158px');
            } 
        }
    }
    //-->
    
    // Handle upload when recent version File was lock 
    function handleFileNameIsLockedInModal(filename) {
        var arguments = [];
        arguments.push(filename);
        var errorMessage = StingFormat(DmsConstants.fileLockedErrorMessage, arguments);        
        $("#message_alert").html("");
        showMessageAlert("error", errorMessage);

        //resize modal case long message 
        var messsage_height = $('#message_alert').height();
        dailog_height = $('.create-container').height() + 155 ;
        $('#message_alert').find('.close').each(function () {
            $(this).unbind('click');
            $(this).bind("click", function () {
                $(this).parent().fadeOut('slow', function () {
                    // Animation complete.
                    parent.resizemodal(dailog_height);
                });
            });
        });

        parent.resizemodal(dailog_height + messsage_height -30 );
    }
    //-->
    
    // Handle upload when recent version File was lock 
    function handleInvalidSiteInModal(siteId) {
       var arguments = [];
       arguments.push(siteId);
       var errorMessage = StingFormat(DmsConstants.invalidSiteErrorMessage, arguments);
       $("#message_alert").html("");
       showMessageAlert("error", errorMessage);

       //resize modal case long message 
       var messsage_height = $('#message_alert').height();
       dailog_height = $('.create-container').height() + 155;
       $('#message_alert').find('.close').each(function () {
          $(this).unbind('click');
          $(this).bind("click", function () {
             $(this).parent().fadeOut('slow', function () {
                // Animation complete.
                parent.resizemodal(dailog_height);
             });
          });
       });

       parent.resizemodal(dailog_height + messsage_height - 30);
    }
    //-->

    function resizemodal(newheight) {
        $('#modalDmsIframeId').parent().parent().height(newheight);
        $('#modalDmsIframeId').height(newheight);    
    }

    function redirectPage(fullLocation,url) {
        parent.window.location.href = fullLocation;
        if (url.length > 1) {
            if (url.substring(url.length - 1, url.length) !== '/') url += '/';
        }
        parent.removeFilterPostParam('fd');
        parent.addFilterPostParam('fd', url);

        parent.parent.reloadDMSDataGrid(true);
        parent.BuildBreadcrumbTrail(url);

    }

    // function expand document revision by Kantorn J.  2012-08-15
    function ExpandRevision(link, key, basepath) {
        var domelemt = document.getElementById('row' + decodeURIComponent(key).replace(/\|/g," "));
        if (domelemt != null) {
            var bIsExpanded = false;
            var bIsSelected = false;
            
            if($(domelemt).hasClass('rev-expanded')) {
                bIsExpanded = true;
            }
            if ($(domelemt).hasClass('show-rev')) {
                bIsSelected = true;
            }

            if (!bIsExpanded) {
                $(domelemt).find('img').each(function () {
                    if ($(this).hasClass('expand-row')) {
                        $(this).attr('src', '/images/icons/details_close.png');
                    }
                });
                $(domelemt).children(':first').css('border-left', '1px solid #e18eb9');
                $(domelemt).children(':nth-child(11)').css('border-right', '1px solid #e18eb9');
                getDocRevisionData(domelemt, decodeURIComponent(key).replace(/\|/g, " "), basepath);
            }
            else {
                if (bIsSelected) {
                    $(domelemt).find('img').each(function () {
                        if ($(this).attr('class') == 'expand-row') {
                            $(this).attr('src', '/images/icons/details_open.png');
                        }
                    });
                    $(domelemt).removeClass('show-rev');
                    $(domelemt).removeClass('rev-expanded');

                    var elem = document.getElementsByTagName('tr');
                    for (i = 0, iarr = 0; i < elem.length; i++) {
                        att = elem[i].getAttribute("name");
                        if (att == ('show_revision_' + decodeURIComponent(key).replace(/\|/g,' '))) {
                            $(elem[i]).hide();
                        }
                    }

                    $(domelemt).children(':first').css('border-left', '');
                    $(domelemt).children(':first').css('border-bottom', '');
                    $(domelemt).children(':nth-child(11)').css('border-right', '');
                    $(domelemt).children(':nth-child(11)').css('border-bottom', '');

                } else {
                    $(domelemt).find('img').each(function () {
                        if ($(this).attr('class') == 'expand-row') {
                            $(this).attr('src', '/images/icons/details_close.png');
                        }
                    });
                    $(domelemt).children(':first').css('border-left', '1px solid #e18eb9');

                    $(domelemt).children(':nth-child(11)').css('border-right', '1px solid #e18eb9');
                    domelemt.className += ' show-rev';

                    var elem = document.getElementsByTagName('tr');
                    for (i = 0, iarr = 0; i < elem.length; i++) {
                        att = elem[i].getAttribute("name");
                        if (att == ('show_revision_' + key)) {
                            $(elem[i]).show();
                        }
                    }
                }
            }
        }
    }

    //Load revison record with ajax by Kantorn J.  2012-08-15
    function getDocRevisionData(domelemt, key, basepath) {
        var actions = "?expandkey=" + urlencode(key) + "&basepath=" + urlencode(basepath);
        actions = (actions);
        var spinnerObject = null;
        $(domelemt).find('img').each(function () {
            if ($(this).attr('class') == "expand-row") {
                spinnerObject = $(this).parent();
                setTimeout( function(){
                    showActionsLoadingSpinner(spinnerObject, false);
                },0);
            }
        });

        if ($("#hidIsllDocumentFolder").val() == "true") {
            actions += "&pseudoFolder=true";
        }

        var keywordstyle = $(domelemt).children(':nth-child(4)').attr('style');
        setTimeout(function () {
            var classRowKey = '';
            $.ajax({
                dataType: 'json',
                url: DmsConstants.dmsHandlerUrl + actions,
                cache: false,
                success: function (data) {
                    $(domelemt).addClass('rev-expanded');
                    if (!$(domelemt).hasClass('show-rev')) {
                        $(domelemt).addClass('show-rev');
                    }
                    var rowKey = 0;
                    if (domelemt.className.split(' ')[0] == 'erow') {
                        rowKey = 1;
                        classRowKey = domelemt.className.split(' ')[1];
                    }
                    else {
                        classRowKey = domelemt.className.split(' ')[0];
                    }
                    var trow = createExpandRow(data, key, keywordstyle, rowKey, classRowKey);
                    var newrow = $(trow);
                    if ($('.' + domelemt.className.split(' ')[rowKey]) != null) {
                        $('.' + domelemt.className.split(' ')[rowKey]).after(newrow);

                        var tableId = DmsConstants.flexGridTableId;
                        var rowIdPrefix = tableId + DmsConstants.flexGridRowIdPrefix;
                        var rowIdCounter = 0;

                        $(newrow).each(function () {
                            var rowIdAsClass = domelemt.className.split(' ')[rowKey] + '_rev';
                            $(this).addClass(rowIdAsClass);
                            var key = encodeURIComponent(data.rows[rowIdCounter].id);
                            
                            if (data.rows[rowIdCounter].cell[8] == true) {
                                // No Actions!
                                $(this).find('div').html(DmsConstants.actionsUnavailable);
                            } else if (data.rows[rowIdCounter].cell[9] != null && data.rows[rowIdCounter].cell[9] != '') {
                                // Actions Select

                                var select = null;
                                select = $(this).children(':nth-child(12)').children(':first').children();

                                // Bind Actions Select
                                $(select).bind("change", function () {
                                    handleSelectActionsChange(select);
                                });
                                $(select).removeAttr('disabled');
                            }
                            rowIdCounter++;
                        });
                    }

                    setTimeout(function () {
                        if (spinnerObject != null) {
                            $('.' + classRowKey).parent().find('.spinner-cont').each(function () {
                                $(this).remove();
                            });

                            $('.' + classRowKey).find('img').each(function () {
                                if ($(this).attr('class') == "expand-row") {
                                    $(this).parent().show();
                                }
                            });
                        }
                    }, 200);
                }
            });
        }, 100);
    }

    ///Append revision row by Kantorn J.  2012-08-16
    function createExpandRow(objJson, key, keywordstyle, rowKey, classRowKey) {
        trow = '';
        for (var i = 0; i < objJson.rows.length; i++) {
            var cssrow = 'odd';
            if (rowKey == 1) {
                cssrow = 'even';
            }
            var lastRow = '';
            if (i == (objJson.rows.length - 1)) {
                lastRow = ' lastRow';
            }

            //create table row  of revision file
            trow += '<tr name= "show_revision_' + (key) + '"  class="revison_data" id="' + objJson.rows[i].id + '_rev_' + i + '" style></td>';
            var classset = 'tdrevison' + cssrow + lastRow;
            if (j == 0) {
                classset = 'tdrevisonHead' + cssrow;
            }

            var show_column = [];
            var div_with = [];
            //show only display collumn
            $('#' + currentFlexiGridID + ' .hDivBox thead tr th').each(function (k) {
                var isDisplay = false;
                var word = $(this).css('display');
                if (word != null) {
                    if (word.indexOf("none") == -1) {
                        isDisplay = true;
                    }
                }
                show_column[k] = isDisplay;
                k++;
            });

            //set width div from parent
            $('.' + classRowKey ).find('td').each(function (k) {
                var width = $(this).children().first().css('width');
                if (width != null) {
                    div_with[k] = 'width:' + width + ';';
                }
                else {
                    div_with[k] = '';
                }
                k++;
            });

            var styledisplay = '';
            var div_width = '';
            var k = 0;
            objJson.rows[i].cell[8] = '';
            for (var j = 0; j < 12; j++) {
                
                if (show_column[j] == false) {
                    styledisplay = 'display:none;';
                }
                else {
                    styledisplay = '';
                }
                if (div_with[j] == '') {
                    div_width = '';
                }
                else {
                    div_width = div_with[j];
                }

                if (!checkApplicationDocument()) {
                    switch (j) {
                        case 0:
                            //create first column of revision file
                            trow += '<td align abbr="Option" class="' + classset + '" style="border-left:solid 0.5px #e18eb9;' + styledisplay + '" ><div class="dms-overflow-control" style="' + div_width + '" ></div></td>';
                            break;
                        case 1:
                            // image
                            trow += '<td align abbr="FileName" class="' + classset + '" style="' + styledisplay + '" ><div class="dms-overflow-control" style="white-space: normal;' + div_width + '" ' + objJson.rows[i].cell[11] + '>' + objJson.rows[i].cell[0] + '<div style="display:none;"></td>';
                            break;
                        case 2:
                            // Folder
                            trow += '<td align abbr="Folder" class="' + classset + '" style="' + styledisplay + '" ><div class="dms-overflow-control" style="white-space: normal;' + div_width + ' " title="' + objJson.rows[i].cell[11] + '">' + objJson.rows[i].cell[11] + '<div style="display:none;"></td>';
                            break;
                        case 3:
                            // Classification
                            trow += '<td align abbr="Metadata.Classification" class="' + classset + '" style="' + styledisplay + '" ><div class="dms-overflow-control" style="white-space: normal;' + div_width + ' " title="' + objJson.rows[i].cell[1] + '">' + objJson.rows[i].cell[1] + '<div style="display:none;"></td>';
                            break;
                        case 5:
                            // Desription
                            trow += '<td align abbr="Metadata.Description" class="' + classset + '" style="' + styledisplay + '" ><div class="dms-overflow-control" style="white-space: normal;' + div_width + ' " title="' + objJson.rows[i].cell[3] + '">' + objJson.rows[i].cell[3] + '<div style="display:none;"></td>';
                            break;
                        case 4:
                            // Keywords
                            if ((keywordstyle != undefined) && (keywordstyle != null)) {
                                trow += '<td align abbr="Metadata.Keywords" class="' + classset + '" style="' + keywordstyle + '" ><div class="dms-overflow-control" style="white-space: normal;' + div_width + ' " title="' + objJson.rows[i].cell[2] + '">' + objJson.rows[i].cell[2] + '<div style="display:none;"></td>';
                            } else {
                                trow += '<td align abbr="Metadata.Keywords" class="' + classset + '" style="' + styledisplay + '" ><div class="dms-overflow-control" style="white-space: normal;' + div_width + ' " title="' + objJson.rows[i].cell[2] + '">' + objJson.rows[i].cell[2] + '<div style="display:none;"></td>';
                            }
                            break;
                        case 6:
                            // Size
                            trow += '<td align abbr="ItemObject.Size" class="' + classset + '" style="' + styledisplay + '"  ><div class="dms-overflow-control" style="white-space: normal;' + div_width + ' " title="' + objJson.rows[i].cell[4] + '">' + objJson.rows[i].cell[4] + '<div style="display:none;"></td>';
                            break;
                        case 7:
                            // Uploaded By
                            trow += '<td align abbr="Metadata.UploadedBy" class="' + classset + '" style="' + styledisplay + '"  ><div class="dms-overflow-control" style="white-space: normal;' + div_width + ' " title="' + objJson.rows[i].cell[5] + '">' + objJson.rows[i].cell[5] + '<div style="display:none;"></td>';
                            break;
                        case 8:
                            // Revison
                            trow += '<td align abbr="Revision" class="' + classset + '" style="' + styledisplay + '"  ><div class="dms-overflow-control" style="white-space: normal;' + div_width + ' " title="' + objJson.rows[i].cell[6] + '">' + objJson.rows[i].cell[6] + '<div style="display:none;"></td>';
                            break;
                        case 9:
                            // Upload Date
                            trow += '<td align abbr="Metadata.UploadDate" class="' + classset + '" style="' + styledisplay + '"  ><div class="dms-overflow-control" style="white-space: normal;' + div_width + ' " title="' + objJson.rows[i].cell[7] + '">' + objJson.rows[i].cell[7] + '<div style="display:none;"></td>';
                            break;
                        case 10: //Shift cell index+1 for revision
                            // Check In / Out
                            trow += '<td align abbr="Metadata.isCheckedOut" class="' + classset + '" style="' + styledisplay + '"  ><div style="white-space: normal;' + div_width + ' ">' + objJson.rows[i].cell[8] + '<div></td>';
                            break;
                        case 11: //Shift cell index+1 for revision
                            // Actions
                            trow += '<td align abbr="actions" class="' + classset + '" style="border-right:solid 1px #e18eb9;' + styledisplay + '" ><div style="white-space: normal;' + div_width + ' ">' + objJson.rows[i].cell[9] + '<div></td>';
                            break;
                    }
                }
                else {
                    switch (j) {
                        case 0:
                            //create first column of revision file
                            trow += '<td align abbr="Option" class="' + classset + '" style="border-left:solid 0.5px #e18eb9;' + styledisplay + '" ><div class="dms-overflow-control" style="' + div_width + '" ></div></td>';
                            break;
                        case 1:
                            // image
                            trow += '<td align abbr="FileName" class="' + classset + '" style="' + styledisplay + '" ><div class="dms-overflow-control" style="white-space: normal;' + div_width + '" ' + objJson.rows[i].cell[11] + '>' + objJson.rows[i].cell[0] + '<div style="display:none;"></td>';
                            break;
                        case 2:
                            // Classification
                            trow += '<td align abbr="Metadata.Classification" class="' + classset + '" style="' + styledisplay + '" ><div class="dms-overflow-control" style="white-space: normal;' + div_width + ' " title="' + objJson.rows[i].cell[1] + '">' + objJson.rows[i].cell[1] + '<div style="display:none;"></td>';
                            break;
                        case 4:
                            // Desription
                            trow += '<td align abbr="Metadata.Description" class="' + classset + '" style="' + styledisplay + '" ><div class="dms-overflow-control" style="white-space: normal;' + div_width + ' " title="' + objJson.rows[i].cell[3] + '">' + objJson.rows[i].cell[3] + '<div style="display:none;"></td>';
                            break;
                        case 3:
                            // Keywords
                            if ((keywordstyle != undefined) && (keywordstyle != null)) {
                                trow += '<td align abbr="Metadata.Keywords" class="' + classset + '" style="' + keywordstyle + '" ><div class="dms-overflow-control" style="white-space: normal;' + div_width + ' " title="' + objJson.rows[i].cell[2] + '">' + objJson.rows[i].cell[2] + '<div style="display:none;"></td>';
                            } else {
                                trow += '<td align abbr="Metadata.Keywords" class="' + classset + '" style="' + styledisplay + '" ><div class="dms-overflow-control" style="white-space: normal;' + div_width + ' " title="' + objJson.rows[i].cell[2] + '">' + objJson.rows[i].cell[2] + '<div style="display:none;"></td>';
                            }
                            break;
                        case 5:
                            // Size
                            trow += '<td align abbr="ItemObject.Size" class="' + classset + '" style="' + styledisplay + '"  ><div class="dms-overflow-control" style="white-space: normal;' + div_width + ' " title="' + objJson.rows[i].cell[4] + '">' + objJson.rows[i].cell[4] + '<div style="display:none;"></td>';
                            break;
                        case 6:
                            // Uploaded By
                            trow += '<td align abbr="Metadata.UploadedBy" class="' + classset + '" style="' + styledisplay + '"  ><div class="dms-overflow-control" style="white-space: normal;' + div_width + ' " title="' + objJson.rows[i].cell[5] + '">' + objJson.rows[i].cell[5] + '<div style="display:none;"></td>';
                            break;
                        case 7:
                            // Revison
                            trow += '<td align abbr="Revision" class="' + classset + '" style="' + styledisplay + '"  ><div class="dms-overflow-control" style="white-space: normal;' + div_width + ' " title="' + objJson.rows[i].cell[6] + '">' + objJson.rows[i].cell[6] + '<div style="display:none;"></td>';
                            break;
                        case 8:
                            // Upload Date
                            trow += '<td align abbr="Metadata.UploadDate" class="' + classset + '" style="' + styledisplay + '"  ><div class="dms-overflow-control" style="white-space: normal;' + div_width + ' " title="' + objJson.rows[i].cell[7] + '">' + objJson.rows[i].cell[7] + '<div style="display:none;"></td>';
                            break;
                        case 9: //Shift cell index+1 for revision
                            // Check In / Out
                            trow += '<td align abbr="Metadata.isCheckedOut" class="' + classset + '" style="' + styledisplay + '"  ><div style="white-space: normal;' + div_width + ' ">' + objJson.rows[i].cell[8] + '<div></td>';
                            break;
                        case 10: //Shift cell index+1 for revision
                            // Actions
                            trow += '<td align abbr="actions" class="' + classset + '" style="border-right:solid 1px #e18eb9;' + styledisplay + '" ><div style="white-space: normal;' + div_width + ' ">' + objJson.rows[i].cell[9] + '<div></td>';
                            break;
                    }
                }
            }
            trow += '</tr>';
        }
        return trow;
    }

    //URL encode for send key with ajax by Kantorn J.  2012-08-16
    function urlencode(inputString){
       var outputString = '';
       if (inputString != null){
         for (var i = 0; i < inputString.length; i++ ){
            var charCode = inputString.charCodeAt(i);
            var tempText = "";
            if (charCode < 128) {
                  var hexVal = charCode.toString(16);
                  outputString += '%' + ( hexVal.length < 2 ? '0' : '' ) + hexVal.toUpperCase();  
            } else if((charCode > 127) && (charCode < 2048)) {
                tempText += String.fromCharCode((charCode >> 6) | 192);
                tempText += String.fromCharCode((charCode & 63) | 128);
                outputString += escape(tempText);
            } else {
                tempText += String.fromCharCode((charCode >> 12) | 224);
                tempText += String.fromCharCode(((charCode >> 6) & 63) | 128);
                tempText += String.fromCharCode((charCode & 63) | 128);
                outputString += escape(tempText);
            }
         }
       }
       return outputString;
    }

    function StingFormat(text, arguments) {
        if(arguments != null){
            //check if there are two arguments in the arguments list
            if ( arguments.length <= 0 ){
                //if there are not 2 or more arguments there's nothing to replace
                return text;
            }
            //decrement to move to the second argument in the array
            var tokenCount = 1;
           if ( arguments.length > 1 ){
                tokenCount =arguments.length-1;            
            }
            for( var token = 0; token <= tokenCount; token++ ){
                //iterate through the tokens and replace their placeholders from the original text in order
                text = text.replace( new RegExp( "\\{" + token + "\\}", "gi" ),
                                                        arguments[ token ] );
            }
        }
        return text;
    }

    function HideColumnName() {
        $('th[axis="col0"]').children().first().text("")
//        $('th[axis="col10"]').children().first().text("")
    }


    // Re-load DataGrid
    function reloadDMSDataGrid(isNewSearchFilter) {
        // Get Current FlexiGrid
        fg = $find(currentFlexiGridID).getFlexigrid();

        if ((fg !== null) && (fg !== undefined)) {

            // Page number reset?
            if (isNewSearchFilter) {
                fg.flexOptions({ newp: 1 });
                isNewSearchFilter = false;
            }

            // Reload and filter the FlexiGrid
            fg.flexReload();
        }
    }

    function checkApplicationDocument() {
        return (window.location.href.indexOf('LeaseApplications') != -1);
    }
    function SetColumn() {
        if (!checkApplicationDocument()) {
            if (window.location.pathname.indexOf("SiteDashboard") > 0) {
                // on SiteDashboard
                if ($('#hidIsllDocumentFolder').val() == 'false') {
                    if ($(".togCol[value='7']").length > 0 && !$(".togCol[value='7']")[5].checked) {
                        $(".togCol[value='7']")[5].click();
                        $(".togCol[value='7']")[5].checked = true;
                    }

                    if ($(".togCol[value='2']").length > 0 && $(".togCol[value='2']")[5].checked) {
                        $(".togCol[value='2']")[5].click();
                        $(".togCol[value='2']")[5].checked = false;
                    }
                }
                else if ($('#hidIsllDocumentFolder').val() == 'true') {
                    if ($(".togCol[value='2']").length > 0 && !$(".togCol[value='2']")[5].checked) {
                        $(".togCol[value='2']")[5].click();
                        $(".togCol[value='2']")[5].checked = true;
                    }

                    if ($(".togCol[value='7']").length > 0 && $(".togCol[value='7']")[5].checked) {
                        $(".togCol[value='7']")[5].click();
                        $(".togCol[value='7']")[5].checked = false;
                    }
                }
            }
            else {
                // on DMS 
                if ($('#hidIsllDocumentFolder').val() == 'false') {
                    if ($(".togCol[value='7']").length > 0 && !$(".togCol[value='7']").prop("checked")) {
                        $(".togCol[value='7']").click();
                        $(".togCol[value='7']")[0].checked = true;
                    }

                    if ($(".togCol[value='2']").length > 0 && $(".togCol[value='2']").prop("checked")) {
                        $(".togCol[value='2']").click();
                        $(".togCol[value='2']")[0].checked = false;
                    }
                }
                else if ($('#hidIsllDocumentFolder').val() == 'true') {
                    if ($(".togCol[value='2']").length > 0 && !$(".togCol[value='2']").prop("checked")) {
                        $(".togCol[value='2']").click();
                        $(".togCol[value='2']")[0].checked = true;
                    }

                    if ($(".togCol[value='7']").length > 0 && $(".togCol[value='7']").prop("checked")) {
                        $(".togCol[value='7']").click();
                        $(".togCol[value='7']")[0].checked = false;
                    }
                }
            }
        }
    }
/** DMS Functions **/