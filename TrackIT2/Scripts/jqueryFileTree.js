// jQuery File Tree Plugin
//
// Version 1.01
//
// Cory S.N. LaViska
// A Beautiful Site (http://abeautifulsite.net/)
// 24 March 2008
//
// Visit http://abeautifulsite.net/notebook.php?article=58 for more information
//
// Usage: $('.fileTreeDemo').fileTree( options, callback )
//
// Options:  root           - root folder to display; default = /
//           script         - location of the serverside AJAX file to use; default = jqueryFileTree.php
//           folderEvent    - event to trigger expand/collapse; default = click
//           expandSpeed    - default = 500 (ms); use -1 for no animation
//           collapseSpeed  - default = 500 (ms); use -1 for no animation
//           expandEasing   - easing function to use on expand (optional)
//           collapseEasing - easing function to use on collapse (optional)
//           multiFolder    - whether or not to limit the browser to one subfolder at a time
//           loadMessage    - Message to display while initial tree loads (can be HTML)
//
// History:
//
// 1.01 - updated to work with foreign characters in directory/file names (12 April 2008)
// 1.00 - released (24 March 2008)
//
// TERMS OF USE
// 
// This plugin is dual-licensed under the GNU General Public License and the MIT License and
// is copyright 2008 A Beautiful Site, LLC.
//

//20120717
// add extraParam for change label text
//20120720
// add hightlight on click
//20120816
// add heightligh and change text on file click
//20120920
// add expandedFolders
//20120925
// add heightligh on default load

if (jQuery) (function ($) {

    $.extend($.fn, {
        fileTree: function (o, h) {
            // Defaults
            if (!o) var o = {};
            if (o.root == undefined) o.root = '/';
            if (o.script == undefined) o.script = 'jqueryFileTree.php';
            if (o.folderEvent == undefined) o.folderEvent = 'click';
            if (o.expandSpeed == undefined) o.expandSpeed = 500;
            if (o.collapseSpeed == undefined) o.collapseSpeed = 500;
            if (o.expandEasing == undefined) o.expandEasing = null;
            if (o.collapseEasing == undefined) o.collapseEasing = null;
            if (o.multiFolder == undefined) o.multiFolder = true;
            if (o.loadMessage == undefined) o.loadMessage = 'Loading...';
            if (o.hiddenObj == undefined) o.hiddenObj = null;
            if (o.LabelObj == undefined) o.LabelObj = null;
            if (o.expandedFolders == undefined) o.expandedFolders = null;

            $(this).each(function () {

                function showTree(c, t) {
                    $(c).addClass('wait');
                    $(".jqueryFileTree.start").remove();
                    $.post(o.script, { dir: t }, function (data) {
                        $(c).find('.start').html('');
                        $(c).removeClass('wait').append(data);
                        if ($('#treeLoad-spinner').length > 0) {
                            if (o.expandedFolders != null) {
                                if (o.expandedFolders.length == 1) {
                                    $('#treeLoad-spinner').remove();
                                }
                                else {
                                    $('#treeLoad-spinner').removeData();
                                }
                            }
                            else {
                                $('#treeLoad-spinner').remove();
                            }
                        }
                        if (o.root == t) $(c).find('UL:hidden').show(); else $(c).find('UL:hidden').slideDown({ duration: o.expandSpeed, easing: o.expandEasing });
                        bindTree(c);

                        if (o.expandedFolders != null) {
                            $(c).find(".directory.collapsed").each(function (i, f) {
                                if ($.inArray($(f).children().attr('rel'), $(o.expandedFolders)) != -1) {
                                    showTree($(f), escape($(f).children().attr('rel').match(/.*\//)));
                                    $(f).removeClass('collapsed').addClass('expanded');

                                    var id = 'id_' + o.expandedFolders[o.expandedFolders.length - 1].replace(/\//g, '_');
                                    id = id.substring(0, id.length - 1);
                                    var thisObj = $("#" + id + "_text");
                                    if (thisObj.length > 0) {
                                        ChangeLabelTextAndHightlight(o, thisObj);
                                        $('#MainContent_TreePanel').scrollTo($("#" + id + "_text"), 200);
                                    }

                                    var allCount
                                    if (parent.$('#MainContent_hiddenCurrentLocationCount').length > 0) {
                                        allCount = parent.$('#MainContent_hiddenCurrentLocationCount').val();

                                        if (allCount >= o.expandedFolders.length - 1) {
                                            o.expandedFolders = null;
                                            parent.$('#MainContent_hiddenCurrentLocationCount').val(0);

                                            if ($('#treeLoad-spinner').length > 0) {
                                                $('#treeLoad-spinner').remove();
                                            }
                                        }
                                        else {
                                            parent.$('#MainContent_hiddenCurrentLocationCount').val(++allCount);
                                        }
                                    }
                                    else {
                                        var id = $(this).attr('id');
                                        $("#" + id + "_text").css("background-color", "#cccccc");

                                        //get select id
                                        $('#hidTmpPath').val($(this).attr('id'));
                                        o.expandedFolders = null;
                                        if ($('#treeLoad-spinner').length > 0) {
                                            $('#treeLoad-spinner').remove();
                                        }
                                    }
                                }
                            });
                        }
                    });
                }

                function ChangeLabelTextAndHightlight(o, thisItem) {
                    if (o.hiddenObj != null && o.LabelObj != null) {

                        $("#" + o.hiddenObj).val("/" + $(thisItem).attr('rel'));
                        $("#" + o.LabelObj).text("/" + $(thisItem).attr('rel'));

                        $(thisItem).css("background-color", "#cccccc");
                        $('#hidTmpPath').val($(thisItem).parent().attr('id'));
                    }
                }

                function bindTree(t) {
                    $(t).find('LI A').bind(o.folderEvent, function () {
                        if ($('#hidTmpPath').val() != "") {
                            var oldID = $('#hidTmpPath').val();
                            $('#jqueryFileTree').find('a').each(function () {
                                if ($(this).attr('id') != null) {
                                    if ($(this).attr('id') == oldID + "_text") {
                                        $(this).css("background-color", "");
                                    }
                                }
                            })
                        }

                        if ($(this).parent().hasClass('directory')) {
                            if ($(this).parent().hasClass('collapsed')) {
                                // Expand
                                if (!o.multiFolder) {
                                    $(this).parent().parent().find('UL').slideUp({ duration: o.collapseSpeed, easing: o.collapseEasing });
                                    $(this).parent().parent().find('LI.directory').removeClass('expanded').addClass('collapsed');
                                }
                                $(this).parent().find('UL').remove(); // cleanup
                                showTree($(this).parent(), escape($(this).attr('rel').match(/.*\//)));
                                $(this).parent().removeClass('collapsed').addClass('expanded');
                                //change label text
                                ChangeLabelTextAndHightlight(o, this);
                            } else {
                                // Collapse
                                $(this).parent().find('UL').slideUp({ duration: o.collapseSpeed, easing: o.collapseEasing });
                                $(this).parent().removeClass('expanded').addClass('collapsed');
                                //change label text
                                ChangeLabelTextAndHightlight(o, this);
                            }
                        } else if ($(this).parent().hasClass('home')) {
                            //change label text
                            ChangeLabelTextAndHightlight(o, this);
                        }
                        else {
                            // file click
                            ChangeLabelTextAndHightlight(o, this);
                        }
                        return false;
                    });
                    // Prevent A from triggering the # on non-click events
                    if (o.folderEvent.toLowerCase != 'click') $(t).find('LI A').bind('click', function () { return false; });
                }
                // Loading message
                $(this).html('<ul class="jqueryFileTree start"><li class="wait">' + o.loadMessage + '<li></ul>');

                // Get the initial file list
                showTree($(this), escape(o.root));
            });
        }
    });

})(jQuery);