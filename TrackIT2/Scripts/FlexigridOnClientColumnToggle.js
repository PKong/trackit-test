﻿
var FlexigridParam = {
    TABLE_TYPE:"",
    MAX_WIDTH: 990,
    MAX_WIDTH_DISPLAY: 970
};
//paramter for set fixed collumn. format  that set on page
//eg. ['Site ID','Site Name','Actions']
var targetTable = currentFlexiGridID;
var originalTableWidth = FlexigridParam.MAX_WIDTH_DISPLAY;
var fixColumn = [];
var originalFlexiGrid = new Array();

//construction array on fgDMSDataLoad
function InitaialResizeFlexigrid(targetTable) {
    originalFlexiGrid = new Array();

   originalTableWidth = 0;
    $('#' + targetTable + ' .hDivBox thead tr th').each(function () {
        var isDisplay = false;
        var Abbr = this.abbr;
        var columnText = "";

        if (this.textContent == undefined) {
            columnText = this.innerText.toString();
        }
        else {
            columnText = this.textContent.toString();
        }
        var word = $(this).css('display');
        if (word != null) {
            if (word.indexOf("none") == -1) {
                isDisplay = true;
            }
        }

        var isFixWidth = false;
        if (fixColumn.length > 0) {
            if ($.inArray(columnText, fixColumn) != -1) {
                isFixWidth = true;
            }
        }
        var width = 0;
        $(this).find('div').each(function () {
            width = $(this).width();
        })
        if (isDisplay) {
            originalTableWidth += width;
        }
        originalFlexiGrid.push({ Name: columnText, Abbr: Abbr, CurrentWidth: width, isDisplay: isDisplay, isFix: isFixWidth, width: width });

    });
}

function DynamicColumnChange(targetTable,cid, visible) {
    if (originalFlexiGrid.length == 0) {
        return;
    }


    //get display/hide each collumn
    $('#' + targetTable + ' .hDivBox thead tr th').each(function (i) {
        var isDisplay = false;
        var word = $(this).css('display');
        if (word != null) {
            if (word.indexOf("none") == -1) {
                isDisplay = true;
            }
        }
        originalFlexiGrid[i].isDisplay = isDisplay;
        i++;
    });
    
    // get display width
    var displayColumn = 0; ;
    var fixedWith = 0;
    var dynamicWidthColumn = new Array();
    var totalWidth = 0;
    for (x = 0; x < originalFlexiGrid.length; x++) {
        if (originalFlexiGrid[x].isDisplay) {
            totalWidth += originalFlexiGrid[x].width;
            dynamicWidthColumn.push(originalFlexiGrid[x]);
            if (originalFlexiGrid[x].isFix) {
                fixedWith += originalFlexiGrid[x].width;
            }
        }
    }
    //calculate column redcue ratio 
    var ratio = ((originalTableWidth - fixedWith) / (totalWidth - fixedWith)).toFixed(2);

    //set minimum column size must not less than half
    if (ratio < 0.5) {
        ratio = 0.5;
    }
    ResizingColumnWidth(targetTable,ratio, dynamicWidthColumn);
    $('#' + targetTable + '_fgtable').width(FlexigridParam.MAX_WIDTH - 1);
    $('#' + targetTable + ' .hDiv table').width(FlexigridParam.MAX_WIDTH)
}

function SetCDragLocation(targetTable) {
    var index = 0;
    var cDragLocation = 0;

    $('#' + targetTable).find('.hDivBox thead tr th').each(function () {
        var word = $(this).css('display');
        if (word.indexOf("none") == -1) {
            if (index != 0) {
                cDragLocation += $(this).width() + 2;
            }
            else {
                cDragLocation += $(this).width();
            }
            var cDragObj = $('#' + targetTable).find('.cDrag').children()[index];
            $(cDragObj).css('left', cDragLocation + "px")
            index++;
        }
    });
}

function SetCDragLocationOnLeaseApplicationPage(targetTable) {
    var index = 0;
    var cDragLocation = 0;

    $('#' + targetTable).find('.hDivBox thead tr th').each(function () {
        var word = $(this).css('display');
        if (word.indexOf("none") == -1) {
            if (index != 0) {
                cDragLocation += $(this).width() + 1;
            }
            else {
                cDragLocation += $(this).width();
            }
            var cDragObj = $('#' + targetTable).find('.cDrag').children()[index];
            $(cDragObj).css('left', cDragLocation + "px")
            index++;
        }
    });
}

function ResizingColumnWidth(targetTable,ratio, dynamicWidthColumn) {
    var totalWidth = 0;
    var displayColumnCount = 0;
    for (i = 0; i < dynamicWidthColumn.length; i++) {
        if (!dynamicWidthColumn[i].isFix) {
            dynamicWidthColumn[i].CurrentWidth = (ratio * dynamicWidthColumn[i].width);
            if (dynamicWidthColumn[i].isDisplay) {
                displayColumnCount++;
            }
        }
        else {
            dynamicWidthColumn[i].CurrentWidth = dynamicWidthColumn[i].width;
        }
        totalWidth += dynamicWidthColumn[i].CurrentWidth;
    }
    var diffValue =   originalTableWidth - totalWidth -20 ;

    //recall cullate append diff data 
    for (i = 0; i < dynamicWidthColumn.length; i++) {
        if (!dynamicWidthColumn[i].isFix) {
            dynamicWidthColumn[i].CurrentWidth = dynamicWidthColumn[i].CurrentWidth + (diffValue / displayColumnCount)
        }
        else {
            dynamicWidthColumn[i].CurrentWidth = dynamicWidthColumn[i].width;
        }
    }

    for (x = 0; x < dynamicWidthColumn.length; x++) {
        SetNewHeaderColumnWidth(targetTable,dynamicWidthColumn[x], ratio);
        SetNewRowWidth(targetTable,dynamicWidthColumn[x]);
    }

}

function CheckTotalWidth(dynamicWidthColumn)
{
    var returnResult= false;
    var returnResult = 0;
    var value = 0;
    for (y = 0; y < dynamicWidthColumn.length; y++) {
        value += dynamicWidthColumn[y].NewWidth;
    }

    if(value > FlexigridParam.MAX_WIDTH_DISPLAY)
    {
        return [false,0];
    }
    else
    {
        returnResult = (FlexigridParam.MAX_WIDTH_DISPLAY - value);
        return [true, returnResult];
    }
}

function SetTableWidth(tableType, newWidth) {
    $(tableType).width(newWidth);
    $('.flexigrid').width(newWidth);
}


function SetNewRowWidth(targetTable,dynamicWidthColumn) {
    $("#" + targetTable + " .bDiv table td[abbr='" + dynamicWidthColumn.Abbr + "']").each(function () {
        $(this).find('div').each(function () {
            $(this).width(dynamicWidthColumn.CurrentWidth );
        });
    });
}

function SetNewHeaderColumnWidth(targetTable,dynamicWidthColumn, ratio) {
    $("#" + targetTable + " .hDiv table th[abbr='" + dynamicWidthColumn.Abbr + "']").each(function () {
        $(this).find('div').each(function () {
                $(this).width(dynamicWidthColumn.CurrentWidth);
        });
    });
}