﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="Sites | TrackiT2" Language="C#" MasterPageFile="~/Templates/TrackIT2.Master" Async="true"  AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TrackIT2.Sites._Default" %>
<%@ MasterType TypeName="TrackIT2.Templates.TrackIT2" %>
<%@ Register TagPrefix="TrackIT2" TagName="TextFieldAutoComplete" src="~/Controls/TextFieldAutoComplete.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   <!-- Styles -->
   <%: Styles.Render("~/Styles/sites") %>
    
   <!-- Scripts -->
   <%: Scripts.Render("~/Scripts/sites") %>

    <script type="text/javascript" language="javascript">
        // Global Var
        searchFilterFields = "txtSiteID,txtSiteName,lsbMarketName,lsbSiteClassDesc,lsbSiteStatusDesc,txtAddress,txtCity,lsbState,txtZip,txtCounty";
        currentFlexiGridID = '<%= fgSites.ClientID %>';

        // ADV Search Var
        var GETdatas = new Array();
        var blnHaveGET = false;
        //Global Search Added by Nam
        var blnExec = false;

        // Doc Ready
        $(function () {

            // Get query string.(query string, out arrayGET data)
            blnHaveGET = GrabGETData(window.location.search.substring(1), GETdatas);

            //Added 2012/02/28 for checking global search
            if (typeof GETdatas['filter'] == "string") {
                blnExec = blnExec | CheckGSFilter(GETdatas['filter']);
            }
            else {
                //Added 2012/02/28 for checking global search
                if (typeof GETdatas['gs'] == "string") {
                    var txtGS = GETdatas['gs'];
                    if (txtGS != undefined && txtGS != null) {
                        setCurrentGS(txtGS);
                        blnExec = true;
                    }
                }
            }

            if (GETdatas['filter'] == '') {
                $("#hiddenFilterParam").val('true');
                blnExec = false
            }
            // Setup Search Filter
            setupSearchFilter();

            // Setup Saved Filter Modal 
            setupSavedFilterModal(SavedFilterConstants.buttonCreateNew, SavedFilterConstants.listViews.sites.name, SavedFilterConstants.listViews.sites.url);

        });
        //-->

        // Flexigrid
        //
        // FG Before Send Data
        function fgBeforeSendData(data)
        {

            // Show - Loading Spinner
            $('#h1').activity({ segments: 8, width: 2, space: 0, length: 3, speed: 1.5, align: 'right' });

            // disable export button
            $("#MainContent_exportButton").val("Processing...");
            $("#MainContent_exportButton").attr("disabled", true);

            // Global Search
            var gs = $.urlParam(GlobalSearch.searchQueryString);
            //
            addGlobal:
            if (!String(gs).isNullOrEmpty())
            {
                for (i = 0; i < arrayOfFilters.length; i++)
                {
                    if (arrayOfFilters[i].name == GlobalSearch.searchQueryString) break addGlobal;
                }
                addFilterPostParam(GlobalSearch.searchQueryString, gs);
            }
            //>

            // Set Default filter
            removeFilterPostParam(ConstGlobal.DEFAULT.returnPage);
            addFilterPostParam(ConstGlobal.DEFAULT.returnPage, savedFiltersQueryString);

            // Search Filter Data
            sendSearchFilterDataToHandler(data);
        }
        //
        // FG Data Load
        function fgDataLoad(sender, args)
        {

            // Hide - Loading Spinner 
            $('#h1').activity(false);

            // Enable export button
            $("#MainContent_exportButton").val("Export to Excel");
            $('#MainContent_exportButton').removeAttr("disabled");
        }
        //
        // FG Row Click
        function fgRowClick(sender, args)
        {

            return false;
        }
        //
        // FG Row Render
        function fgRowRender(tdCell, data, dataIdx)
        {

            var cVal = new String(data.cell[dataIdx]);
            // Type?
            switch (Number(dataIdx))
            {

                // Tower Mod  
                case 5:
                    sHtml = getColumnImageIcon(ColumnTypes.TOWER_MOD, cVal, data.cell[0]);
                    tdCell.innerHTML = sHtml;
                    break;

                // Rogue Equipment   
                case 6:
                    sHtml = getColumnImageIcon(ColumnTypes.ROGUE_EQUIP, cVal, null);
                    tdCell.innerHTML = sHtml;
                    break;

                // Standard String 
                default:
                    if ((cVal.isNullOrEmpty()) || (cVal == "<undefined>"))
                    {
                        tdCell.innerHTML = ConstGlobal.DATA.noDataHtml;
                    } else
                    {
                        tdCell.innerHTML = cVal;
                    }
                    break;
            }
            //>
        }
        //
        // FG No Data
        function fgNoData()
        {

            // Hide - Loading Spinner 
            $('#h1').activity(false);
            
            // Disable export button
            $("#MainContent_exportButton").val("Export to Excel");
        }
        //-->

        // Window Load
        // Apply Search Filter?
        $(window).load(function () {

            // Apply Search Filter?
            if (blnExec) {

                // Wait For Last Field To Load
                var waitForLastFieldToLoadLimit = 0;
                var waitForLastFieldToLoad = setInterval(function () {
                    if (($('#hiddenLastFieldToCheckLoaded').val().length === 0) || (waitForLastFieldToLoadLimit === 50)) {
                        clearInterval(waitForLastFieldToLoad);

                        // Apply filters and reload the DataGrid
                        applySearchFilter(true);
                    }
                    waitForLastFieldToLoadLimit++;
                }, 100);
                //>

            } else {
                // Normal DataGrid Load
                if ($('.filter-tag').length > 0 || $("#hiddenFilterParam").val() == "true") {
                    reloadDataGrid();
                }
                else {
                    // Skip Normal load and show filter
                    $('.filter-box').slideToggle();
                    $("#MainContent_exportButton").attr("disabled", true);
                }
            }
        });
    </script>    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div id="message_alert" class="full-col"></div>

    <!-- Page Content Header -->
    <div class="o-1" style="margin:0;">
	    <h1 id="h1" style="float:left; padding-top:3px; padding-right:30px;">Sites</h1>
	        <div class="page-options-nav">
                <a class="fancy-button clear-filter-button" href="javascript:void(0)">Clear Filters</a> 
                <a class="fancy-button filter-button arrow-down" href="javascript:void(0)">Filter Results<span class="arrow-down-icon"></span></a> 
                <a id="modalBtnExternalSaveFilter" class="fancy-button save-filter-button" href="javascript:void(0)">Save Filter</a>
	        </div>
	    <div class="cb"></div>
    </div>
    <!-- /Page Content Header -->

    <!-- Search Filter Box -->
    <div class="filter-box">
	    <div style="padding:20px">
            <div class="filter-button-close"><a class="fancy-button close-filter" style="padding:2px 6px 2px 6px;" href="javascript:void(0)">x</a></div>
                <asp:Panel ID="panelFilterBox" runat="server">

                    <div class="left-col">
                
                        <TrackIT2:TextFieldAutoComplete ID="ctrSiteID"
                            LabelFieldClientID="lblSiteID"
                            LabelFieldValue="Site ID"
                            LabelFieldCssClass="label"
                            TextFieldClientID="txtSiteID"
                            TextFieldCssClass="input-autocomplete"
                            TextFieldWidth="188"
                            ScriptKeyName="ScriptAutoCompleteSiteID"
                            DataSourceUrl="SiteIdAutoComplete.axd"
                            ClientIDMode="Static"
                            runat="server" />
		                <br />
		
                        <TrackIT2:TextFieldAutoComplete ID="ctrSiteName"
                            LabelFieldClientID="lblSiteName"
                            LabelFieldValue="Site Name"
                            LabelFieldCssClass="label"
                            TextFieldClientID="txtSiteName"
                            TextFieldCssClass="input-autocomplete"
                            TextFieldWidth="188"
                            ScriptKeyName="ScriptAutoCompleteSiteName"
                            DataSourceUrl="SiteNameAutoComplete.axd"
                            IsWholeTextUsage="True"
                            ClientIDMode="Static"
                            runat="server" />
		                <br />
		
                        <label>Address</label>
                            <asp:TextBox ID="txtAddress" runat="server" ClientIDMode="Static" Width="186"></asp:TextBox>
                        <br />

                        <label>City</label>
                            <asp:TextBox ID="txtCity" runat="server" ClientIDMode="Static" Width="186"></asp:TextBox>
                        <br />

                        <label>State</label>
                        <div id="divState" style="width: 188px;margin-left: 188px; vertical-align:top;margin-top:-32px" >
                            <asp:ListBox ID="lsbState" SelectionMode="Multiple" ToolTip="- Select -" CssClass="multi-local-config" ClientIDMode="Static" runat="server" />
                        </div>
                        <br />

                        <label>Zip</label>
                            <asp:TextBox ID="txtZip" runat="server" ClientIDMode="Static" Width="186"></asp:TextBox>
                        <br />

                        <label>County</label>
                            <asp:TextBox ID="txtCounty" runat="server" ClientIDMode="Static" Width="186"></asp:TextBox>
                        <br />

                    </div>
                    <div class="right-col">
		                <label>Market Name</label>
                        <div id="divMarketName" style="width: 188px;margin-left: 188px; vertical-align:top;margin-top:-32px" >
                            <asp:ListBox ID="lsbMarketName" SelectionMode="Multiple" ToolTip="- Select -" CssClass="multi-local-config" ClientIDMode="Static" runat="server" />
                        </div>
                        <br />
	
		                <label>Site Class Desc</label>
                        <div id="divSiteClassDesc" style="width: 188px;margin-left: 188px; vertical-align:top;margin-top:-32px" >
                            <asp:ListBox ID="lsbSiteClassDesc" SelectionMode="Multiple" ToolTip="- Select -" CssClass="multi-local-config" ClientIDMode="Static" runat="server" />
                        </div>
                        <br />

		                <label>Site Status Desc</label>
                        <div id="divSiteStatusDesc" style="width: 188px;margin-left: 188px; vertical-align:top;margin-top:-32px" >
                            <asp:ListBox ID="lsbSiteStatusDesc" SelectionMode="Multiple" ToolTip="- Select -" CssClass="multi-local-config" ClientIDMode="Static" runat="server" />
                        </div>
                        <br />
		            </div>

                    <div class="cb"></div>
                    <div style="padding:20px 0 15px 0; overflow:visible;">
                        <div style="width:auto;float:left;"><a class="fancy-button apply-search-filter" href="javascript:void(0)">Apply Filter</a></div>
                        <div style="width:auto;float:right;"><a class="fancy-button apply-search-filter" href="javascript:void(0)">Apply Filter</a></div>
                    </div>
                </asp:Panel>
	    </div>
    </div>
    <!-- /Search Filter Box -->

    <div class="cb"></div>

    <!-- Filter Tags -->
    <div id="filterTagsContainer" class="filter-tags-container not-displayed"  runat="server"></div>
    <!-- /Filter Tags -->

    <div class="cb"></div>

    <!-- Sites Grid -->
    <div style="overflow:visible; margin-bottom: 20px; min-height:580px; position:relative;">

        <fx:Flexigrid ID="fgSites"
            AutoLoadData="False"
            Width="990"
            ResultsPerPage="20"
			ShowPager="true"
            Resizable="false"
            ShowToggleButton="false"
            ShowTableToggleButton="false"
            SearchEnabled="false"
            UseCustomTheme="true"
            CssClass="tmobile"
            WrapCellText="true"
            DoNotIncludeJQuery="true"
            OnClientBeforeSendData="fgBeforeSendData"
            OnClientDataLoad="fgDataLoad"
            OnClientRowClick="fgRowClick"
            OnClientNoDataLoad="fgNoData"
			HandlerUrl="~/SitesList.axd"
            runat="server">
		    <Columns>
			    <fx:FlexiColumn Code="site_uid" Text="Site ID" Width="80" />
                <fx:FlexiColumn Code="site_name" Text="Site Name" Width="341" OnRowRender="fgRowRender" />
                <fx:FlexiColumn Code="market_name" Text="Market Name" Width="180" OnRowRender="fgRowRender" />
                <fx:FlexiColumn Code="site_class_desc" Text="Site Class Desc" Width="120" OnRowRender="fgRowRender" />
                <fx:FlexiColumn Code="site_status_desc" Text="Site Status Desc" Width="120" OnRowRender="fgRowRender" />
                <fx:FlexiColumn Code="tower_modification_yn" Text="TM" Width="32" OnRowRender="fgRowRender" />
                <fx:FlexiColumn Code="rogue_equipment_yn" Text="RE" Width="32" OnRowRender="fgRowRender" />
                <fx:FlexiColumn Code="id" Text="Actions" Width="65" AllowSort="false"
                                Format="<a class='fancy-button' href='/Sites/SiteDashboard.aspx?id={0}'>Edit</a>" />
		    </Columns>
	    </fx:Flexigrid>

        <br />
        <div>
            <div id="exportLoadSpinner" style="display: block; width:25px; height:25px;margin-left:80%; float:left;"></div>
            <asp:Button ID="exportButton" CssClass="ui-button ui-widget ui-state-default ui-corner-all excelExportButton" runat="server" Text="Export to Excel" UseSubmitBehavior="false" OnClientClick="FrontExportClick('fgSites')" OnClick="ExportClick" />
        </div>
        <asp:HiddenField ID="hiddenSearchValue" runat="server" ClientIDMode="Static" Value="null" />
        <asp:HiddenField ID="hiddenColumnValue" runat="server" ClientIDMode="Static" Value="null" />
        <asp:HiddenField ID="hiddenDownloadFlag" runat="server" ClientIDMode="Static" Value="" />
        <asp:HiddenField ID="hiddenLastFieldToCheckLoaded" runat="server" ClientIDMode="Static" Value="" />
        <asp:HiddenField ID="hiddenFilterParam" runat="server" ClientIDMode="Static" Value="" />
        <div class="cb"></div>

    </div>
    <!-- /Sites Grid -->

    <!-- Modal -->
    <div id="modalExternal" class="modal-container" style="overflow:hidden;"></div>
    <div id="modalExternalSavedFilterManage" class="modal-container" style="overflow:hidden;"></div>
    <!-- /Modal -->

</asp:Content>
