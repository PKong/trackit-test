﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using TrackIT2.BLL;
using TrackIT2.DAL;
using TrackIT2.Objects;

namespace TrackIT2.Sites
{
    /// <summary>
    /// Site Dashboard
    /// </summary>
    public partial class SiteDashboard : System.Web.UI.Page
    {
        // Var
        private bool _siteFound = true;
 
        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                // Setup
                this.SetupFlexiGrids();

                if (
                    !User.IsInRole("TrackiT Docs User") && 
                    !User.IsInRole("TrackiT Docs Admin") && 
                    !User.IsInRole("Administrator")
                   )
                {
                   dms_grid_section.Style.Add("display", "none");
                }
                else
                {
                   dms_grid_unauthorized.Style.Add("display", "none");
                }

                // Have site ID?
                if (!string.IsNullOrEmpty(Request.QueryString["id"]))
                {
                    string siteUid = Request.QueryString["id"];

                    if (BLL.Site.Search(siteUid) != null)
                    {
                        Page.Title = "Site Information - " + siteUid + " | TrackiT2";
                        BindTowerImages(siteUid);
                        BindSite(siteUid);
                        pnlNoID.Visible = false;
                        pnlSectionContent.Visible = true;
                        _siteFound = true;
                    }
                    else
                    {
                        // Page render event will write the 404 response code. This part
                        // of the code will display the No Id panel and hide the edit
                        // panel.
                        pnlSectionContent.Visible = false;
                        pnlNoID.Visible = true;
                        _siteFound = false;
                    }
                }
                else
               {
                  // Page render event will write the 404 response code. This part
                  // of the code will display the No Id panel and hide the edit
                  // panel.
                  pnlSectionContent.Visible = false;
                  pnlNoID.Visible = true;
                  _siteFound = false;
               }

                GetSessionFilter();
            }
            else
            {
                // Initialise Download from Amazon
                if (Request.Params["__EVENTARGUMENT"] == "DownloadDocument")
                {
                    string keyName = hidDocumentDownload.Value;
                    AmazonDMS.DownloadObject(HttpUtility.UrlDecode(keyName));
                }
                //>
            }
        }
        //-->

        protected override void Render(HtmlTextWriter writer)
        {
           base.Render(writer);

           if (!_siteFound)
           {
              Response.StatusCode = 404;
           }
        }

        /// <summary>
        /// Setup Flexi Grids
        /// </summary>
        protected void SetupFlexiGrids()
        {
            var rowPerPageOptions = new[] { 10, 20, 30, 40, 50 };
            const string NO_ITEM_MESSAGE = "No Data";

            // Lease Applications
            fgApplications.SortColumn = fgApplications.Columns.Find(col => col.Code == "site_uid");
            fgApplications.SortDirection = FlexigridASPNET.GridSortOrder.Asc;
            fgApplications.RowPerPageOptions = rowPerPageOptions;
            fgApplications.NoItemMessage = NO_ITEM_MESSAGE;

            // Tower Modifications
            fgModifications.SortColumn = fgModifications.Columns.Find(col => col.Code == "site_uid");
            fgModifications.SortDirection = FlexigridASPNET.GridSortOrder.Asc;
            fgModifications.RowPerPageOptions = rowPerPageOptions;
            fgModifications.NoItemMessage = NO_ITEM_MESSAGE;

            // Site Data Packages
            fgPackages.SortColumn = fgPackages.Columns.Find(col => col.Code == "site_uid");
            fgPackages.SortDirection = FlexigridASPNET.GridSortOrder.Asc;
            fgPackages.RowPerPageOptions = rowPerPageOptions;
            fgPackages.NoItemMessage = NO_ITEM_MESSAGE;
      
            // Issue Tracker Items
            fgIssueTracker.SortColumn = fgIssueTracker.Columns.Find(col => col.Code == "id");
            fgIssueTracker.SortDirection = FlexigridASPNET.GridSortOrder.Asc;
            fgIssueTracker.RowPerPageOptions = rowPerPageOptions;
            fgIssueTracker.NoItemMessage = NO_ITEM_MESSAGE;

            //DMS Items
            fgDMS.RowPerPageOptions = new int[] { 10, 20, 30, 40, 50 };
            fgDMS.NoItemMessage = "No Data";
        }
        //-->

        /// <summary>
        /// Bind Tower Image
        /// </summary>
        /// <param name="siteId"></param>
        protected void BindTowerImages(string siteId)
        {          
           DMSHelper imageHelper = new DMSHelper();
           List<PhotoInfo> siteImages = new List<PhotoInfo>();           

           siteImages = imageHelper.GetSitePhotos(siteId, false, string.Empty);
           Image2.ImageUrl = siteImages[0].photoURL;
           Image2.AlternateText = siteImages[0].photoID;

           // TODO: Remove this if thumbnail images for site are not needed.
           //List<PhotoInfo> thumbImages = new List<PhotoInfo>();
           //thumbImages = imageHelper.GetSitePhotos(siteId, true, string.Empty);


           // TODO: Remove this if the document names are not needed.
           //DataTable displayDocumentNames;
           //displayDocumentNames = imageHelper.GetPublicDocuments(siteId);
        }

        /// <summary>
        /// Bind Site
        /// </summary>
        /// <param name="siteId"></param>
        protected void BindSite(string siteId)
        {
            site siteCurr = BLL.Site.Search(siteId);
            submarket siteSubmarket = SubmarketMapping.GetSubmarketBySite(siteId);
            string submarketInfo = "";

            if (siteSubmarket != null)
            {
               submarketInfo = siteSubmarket.code + " - " + siteSubmarket.name;
            }
            else
            {
               submarketInfo = "None.";
            }

            if (siteCurr != null)
            {
                //General Information
                Utility.ControlValueSetter(lblSiteID, siteCurr.site_uid);
                if (!string.IsNullOrEmpty(siteCurr.site_uid))
                {
                    lblSiteID.NavigateUrl = Globals.SITE_LINK_URL + siteCurr.site_uid;
                }

                Utility.ControlValueSetter(lblSiteName, siteCurr.site_name);
                Utility.ControlValueSetter(lblRegion, siteCurr.region_name);
                Utility.ControlValueSetter(lblMarket, siteCurr.market_name);
                Utility.ControlValueSetter(lblSubmarket, submarketInfo);
                Utility.ControlValueSetter(lblTowerMod, BLL.Site.HasTowerModifications(siteCurr.site_uid));
                Utility.ControlValueSetter(lblRogueEquip, siteCurr.rogue_equipment_yn);
                Utility.ControlValueSetter(lblPreviousUID, siteCurr.previous_site_uid,"N/A");
                //Other Information
                Utility.ControlValueSetter(lblFAAReq, siteCurr.faa_lighting_req_yn,"N/A");
                Utility.ControlValueSetter(lblManagementStatus, siteCurr.management_status_id.HasValue ? BLL.ManagementStatus.Search(siteCurr.management_status_id.Value).name : "N/A");
                //Structure Details
                Utility.ControlValueSetter(lblStructureDetails_Type, siteCurr.site_class_desc);
                Utility.ControlValueSetter(lblStructureDetails_Status, siteCurr.site_status_desc);
                if (BLL.Site.IsNotOnAir(siteCurr.site_on_air_date, siteCurr.site_status_desc))
                {
                    imgStructureDetails_Construction.ImageUrl = "/images/icons/Construction.png";
                }
                else
                {
                    imgStructureDetails_Construction.Visible = false;
                }

                Utility.ControlValueSetter(lblStructureDetails_Height, siteCurr.structure_ht, (0).ToString());
                Utility.ControlValueSetter(lblStructureDetails_Status, siteCurr.site_status_desc);
                Utility.ControlValueSetter(lblStructureDetails_siteOnAirDate, siteCurr.site_on_air_date);
                if (siteCurr.site_off_air_date != null)
                {
                    Utility.ControlValueSetter(lblStructureDetails_siteOffAirDate, siteCurr.site_off_air_date);
                }
                else
                {
                    siteOffAirDatePanel.Visible = false;
                }
                //Address
                //Link 0 = address,1 = city,2 = state,3 = zip,4 = country
                
                if (!string.IsNullOrEmpty(siteCurr.address))
                {
                string sAddressFormat = "{0}<br />{1}, {2}, {3}<br />{4}";
                link_googlemaps.Text = string.Format(sAddressFormat
                    , Utility.PrepareString(siteCurr.address)
                    , Utility.PrepareString(siteCurr.city)
                    , Utility.PrepareString(siteCurr.state)
                    , Utility.PrepareString(siteCurr.zip)
                    , Utility.PrepareString(siteCurr.county));

                // Google Maps Link
                StringBuilder sbGoogleSearch = new StringBuilder();
                sbGoogleSearch.Append(siteCurr.address.ToString());
                if (!String.IsNullOrWhiteSpace(siteCurr.city)) sbGoogleSearch.Append(", " + siteCurr.city);
                if (!String.IsNullOrWhiteSpace(siteCurr.state)) sbGoogleSearch.Append(", " + siteCurr.state);
                if (!String.IsNullOrWhiteSpace(siteCurr.zip)) sbGoogleSearch.Append(", " + siteCurr.zip);
                if (!String.IsNullOrWhiteSpace(siteCurr.county)) sbGoogleSearch.Append(", " + siteCurr.county);
                link_googlemaps.NavigateUrl = Utility.GetGoogleMapLink(sbGoogleSearch.ToString());
                }
                else
                {
                  link_googlemaps.Visible = false;
                }                                                          

                // Add Google Maps link to Latitude and Logitude                
                if (siteCurr.site_latitude.HasValue || siteCurr.site_longitude.HasValue)
                {
                   Utility.ControlValueSetter(lblSiteLatitude, siteCurr.site_latitude, (0.0).ToString());
                   Utility.ControlValueSetter(lblDegreeLatitude, Utility.ConvertLatLongCoorToDegree(siteCurr.site_latitude), (0.0).ToString());
                   Utility.ControlValueSetter(lblSiteLongitude, siteCurr.site_longitude, (0.0).ToString());
                   Utility.ControlValueSetter(lblDegreeLongtitude, Utility.ConvertLatLongCoorToDegree(siteCurr.site_longitude), (0.0).ToString());
                   hypSiteLatLong.NavigateUrl = Utility.GetGooleMapLatLong(lblSiteLatitude.Text, lblSiteLongitude.Text);
                   lblSiteLatLongHeader.Visible = true;
                   hypSiteLatLong.Visible = true;                   
                }
                else
                {
                   lblSiteLatLongHeader.Visible = false;
                   hypSiteLatLong.Visible = false;
                }

                //Owner
                Utility.ControlValueSetter(lblStructureOwner,HttpUtility.HtmlEncode( siteCurr.struct_owner),"None");
                Utility.ControlValueSetter(lblGroundOwner,HttpUtility.HtmlEncode( siteCurr.ground_owner), "None");

                ulRelatedWorkflows.InnerHtml = RelatedWorkFlow.GetSiteWorkflowsList(siteCurr.site_uid);
            }
        }
        //-->

        private void GetSessionFilter()
        {
            // Check session filter
            DefaultFilterSession filterSession = new DefaultFilterSession();

            if (HttpContext.Current.Session != null && HttpContext.Current.Session[DefaultFilterSession.SessionName] != null)
            {
                filterSession = (DefaultFilterSession)HttpContext.Current.Session[DefaultFilterSession.SessionName];

                string hiddenFilter = DefaultFilterSession.GetFilterSession(filterSession, DefaultFilterSession.FilterType.SitesFilter);
                backToList.NavigateUrl += "?filter=" + hiddenFilter;
            }
        }
    }
}