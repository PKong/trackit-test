﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.BLL;
using TrackIT2.DAL;
using System.Collections.Specialized;
using System.Web.Script.Serialization;
using System.IO;
using TrackIT2.Handlers;
using System.ComponentModel;

namespace TrackIT2.Sites
{
    /// <summary>
    /// List
    /// </summary>
    public partial class _Default : System.Web.UI.Page
    {

        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindLookupLists();
                PageUtility.SetupSearchResultFilters(this, panelFilterBox);
                this.SetupFlexiGrid();

                // Clear Session
                DefaultFilterSession.ClearSession(HttpContext.Current, DefaultFilterSession.FilterType.SitesFilter);
            }
        }
        //-->

        /// <summary>
        /// Bind Lookup Lists
        /// </summary>
        protected void BindLookupLists()
        {

            CPTTEntities ce = new CPTTEntities();

            // State
            var sort_state = ce.state_provinces.Distinct().Where("it.country_id == 1").ToList();
            var tmp_state_provinces = new DynamicComparer<state_provinces>();
            tmp_state_provinces.SortOrder(x=>x.state_province);
            sort_state.Sort(tmp_state_provinces);
            Utility.BindDDLDataSource(this.lsbState, sort_state, "state_province", "state_province", null);

            // Market Name
            var marketNames = ce.sites.Select(x => new { market_name = x.market_name }).Distinct().ToList();
            marketNames.Sort((x,y) => new AlphanumComparatorFast().Compare(x.market_name,y.market_name));
            Utility.BindDDLDataSource(this.lsbMarketName, marketNames, "market_name", "market_name", null);

            // Site Class Desc
            var siteClassDescs = ce.sites.Select(x => new { site_class_desc = x.site_class_desc }).Distinct().Where(x => x.site_class_desc != null && x.site_class_desc != "<undefined>").ToList();
            siteClassDescs.Sort((x, y) => new AlphanumComparatorFast().Compare(x.site_class_desc, y.site_class_desc));
            Utility.BindDDLDataSource(this.lsbSiteClassDesc, siteClassDescs, "site_class_desc", "site_class_desc",null);

            // Site Status Desc
            var siteStatusDescs = ce.sites.Select(x => new { site_status_desc = x.site_status_desc }).Distinct().ToList();
            siteStatusDescs.Sort((x, y) => new AlphanumComparatorFast().Compare(x.site_status_desc, y.site_status_desc));
            Utility.BindDDLDataSource(this.lsbSiteStatusDesc, siteStatusDescs, "site_status_desc", "site_status_desc", null);
        }
        //-->

        /// <summary>
        /// Setup Flexi Grid
        /// </summary>
        protected void SetupFlexiGrid()
        {
            fgSites.SortColumn = fgSites.Columns.Find(col => col.Code == "site_uid");
            fgSites.SortDirection = FlexigridASPNET.GridSortOrder.Asc;
            fgSites.RowPerPageOptions = new int[] { 10, 20, 30, 40, 50 };
            fgSites.NoItemMessage = "No Data";
        }
        //-->

        public void ExportClick(object s, EventArgs e)
        {
            
            ExportFile ef = new ExportFile();
            var searchData = hiddenSearchValue.Value;
            var data = new NameValueCollection();
            if (!string.IsNullOrEmpty(searchData))
            {
                var jss = new JavaScriptSerializer();
                var table = jss.Deserialize<dynamic>(searchData);

                for (int x = 0; x < table.Length; x++)
                {
                    data.Add(table[x]["name"], table[x]["value"]);
                }
            }

            const int startIndex = 4;
            var parameters = new NameValueCollection();

            // If we have form values, extract them to build the object query.
            if (data.Count > startIndex)
            {
               for (var i = startIndex; i < data.Count; i++)
               {
                  var itemKey = data.GetKey(i);
                  var itemValue = data.Get(i);

                  parameters.Add(itemKey, itemValue);
               }
            }

            NameValueCollection columnValue = ExportFile.GetColumn(hiddenColumnValue.Value);
            ExportFileHandler extHand;
            extHand = new ExportFileHandler(ExportFile.eExportType.eSite, data, columnValue, parameters);
            extHand.Load_Data_OnComplete += new ExportFileHandler.Load_Data_OnComplete_Handler(Load_Data_OnComplete);
        }

        protected void Load_Data_OnComplete(List<ExportClass> loadDataResult, NameValueCollection data, NameValueCollection column, RunWorkerCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                ExportFile ef = new ExportFile();
                MemoryStream mem = ef.WriteReport(loadDataResult, column, ExportFile.eExportType.eSite, Server.MapPath("/"));

                if (mem != null)
                {
                    string fileName = ExportFile.GenarateFileName(data["fileName"]);
                    DownloadFile(mem, fileName);
                }
                else
                {
                    Master.StatusBox.showStatusMessage("Export report error.");
                }
            }
            else
            {
                throw e.Error;
            }
        }

        private void DownloadFile(MemoryStream mstream, string reportName)
        {
            try
            {
                byte[] byteArray = mstream.ToArray();
                Response.Clear();

                if (Response.Cookies.Count > 0)
                {
                    bool flag = true;
                    foreach (string c in Response.Cookies)
                    {
                        if (c == "fileDownloadToken")
                        {
                            Response.Cookies["fileDownloadToken"].Value = hiddenDownloadFlag.Value;
                            flag = false;
                        }
                    }
                    if (flag)
                    {
                        Response.AppendCookie(new HttpCookie("fileDownloadToken", hiddenDownloadFlag.Value));
                    }
                }
                else
                {
                    Response.AppendCookie(new HttpCookie("fileDownloadToken", hiddenDownloadFlag.Value));
                }
                Response.AddHeader("Content-Disposition", "attachment; filename=" + reportName + ".xls");
                Response.AddHeader("Content-Length", byteArray.Length.ToString());
                Response.ContentType = "application/vnd.msexcel";
                Response.BinaryWrite(byteArray);
                Response.Flush();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                Master.StatusBox.showStatusMessage("Export report error.");
            }
        }
    }
}