﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="Add Sites | TrackiT2" Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs"
 Inherits="TrackIT2.Sites.Add" MasterPageFile="~/Templates/Modal.Master" %>
 <%@ MasterType TypeName="TrackIT2.Templates.Modal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   <!-- Styles -->
   <%: Styles.Render("~/Styles/sites_add") %>

   <!-- Scripts -->
   <%: Scripts.Render("~/Scripts/sites_add")%>  

   <script type="text/javascript">
       $(document).ready(function () {
           parent.$('#modalIframeId').height(parent.$('#modalIframeId').contents().find('.modal-page-wrapper').outerHeight(true) + 30);
           // Setup Upload Document Modal
           parent.setupDmsModal("Upload Document");
       });
   </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="message_alert"></div>
    <div style="width:720px;">
        <h4>Select Equipment</h4>
        <br/>
        <asp:Label ID="lbl_RogueEquip" runat="server" Text="Rogue Equipment"></asp:Label>&nbsp;&nbsp;&nbsp;
            <asp:DropDownList ID="ddl_RogueEquip" runat="server">
            <asp:ListItem Text=" - Select - " value=""></asp:ListItem>
            <asp:ListItem Text="Yes" Value="yes"></asp:ListItem>
            <asp:ListItem Text="No" Value="no"></asp:ListItem>
        </asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <div class="document-link" id="RogueEquipment" >
        <asp:HiddenField  ID="hidden_document_RogueEquipment" ClientIDMode="Static" runat="server" />
    </div>

    <div id="divTowerAddress" runat="server" style="width:100%;" >
        <h4 style="margin-top: 19px;">Tower Address</h4>  
        <div class="left-col form_element">
            <label ID="Label8" runat="server">Address</label>
            <asp:TextBox ID="txtAddress" runat="server"></asp:TextBox><br />

            <label ID="Label12" runat="server">City</label>
            <asp:TextBox ID="txtCity" runat="server"></asp:TextBox><br />

            <label ID="Label6" runat="server">County</label>
            <asp:TextBox ID="txtCounty" runat="server"></asp:TextBox><br />
                
            <label ID="Label7" runat="server">State</label>
            <asp:DropDownList ID="ddlState" runat="server" CssClass="select-field"></asp:DropDownList><br />

            <label ID="Label11" runat="server">Zip</label>
            <asp:TextBox ID="txtZip" runat="server"></asp:TextBox><br />

        </div>
        <div class="right-col form_element" style="margin-bottom:30px;">
            <label ID="Label13" runat="server">Latitude</label>
            <asp:TextBox ID="txtLatitude" runat="server" Width="200px"></asp:TextBox><br />
                
            <label ID="Label14" runat="server">Longitude</label>
            <asp:TextBox ID="txtLongitude" runat="server" Width="200px"></asp:TextBox><br />
        </div>
    </div>
        <asp:Button ID="btn_Save_RogueEquip" runat="server" CssClass="ui-button ui-widget ui-state-default ui-corner-all buttonLASubmit" Text="Save" UseSubmitBehavior="false" OnClick="btn_Save_RogueEquip_Click" OnClientClick="if (!validateSaveSite()) { return false;}" /> 
    </div>

    <asp:HiddenField ID="hidDocumentDownload" runat="server" ClientIDMode="Static" />
    <div id="modalExternalDms" class="modal-container" style="overflow:hidden;"></div>
    <asp:HiddenField id="hidBasePath" ClientIDMode="Static" runat="server"/>
    <asp:HiddenField ID="HiddenDialogHeightFlag" ClientIDMode="Static" runat="server" />
</asp:Content>
