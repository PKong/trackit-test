﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="Site Information | TrackiT2" Language="C#" MasterPageFile="~/Templates/TrackIT2.Master" AutoEventWireup="true" CodeBehind="SiteDashboard.aspx.cs" Inherits="TrackIT2.Sites.SiteDashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   <!-- Styles -->
   <%: Styles.Render("~/Styles/sites_dashboard")%>
    
   <!-- Scripts -->
   <%: Scripts.Render("~/Scripts/sites_dashboard")%>
    <script type="text/javascript" language="javascript">
        // Global Var
        currentFlexiGridID = '<%= this.fgDMS.ClientID %>';
        var fixColumn = ['Actions'];
        
        // Doc Ready
        $(function () {

            // Site ID
            var id = $.urlParam('id');
            if (!String(id).isNullOrEmpty()) {
                addFilterPostParam("SiteID", id);
            }

            // Init DMS Controls
            InitDMSFlexiRelatedControls("#h2-sitedoc", "#sBasePath", "#hidBasePath", id);
            
            // Set the modal size
            setModalSize(SiteAddDialogConstants.dWidth, SiteAddDialogConstants.iframe_minWidth, SiteAddDialogConstants.iframe_minHeight);
            
            // Setup List Create Modal
            setupListCreateModalWithID("Edit Site", window.location.toString().substr(window.location.toString().indexOf('=') + 1));
            //Setup Document link Dialog
            setupDocumentLinkDialog("Document Links",true);
            
            SetCDragLocation();
        });
        //-->

        // Flexigrid
        //
        // FG Before Send Data
        function fgBeforeSendData(data) {

            // Show - Loading Spinner
            $('#h1').activity({ segments: 8, width: 2, space: 0, length: 3, speed: 1.5, align: 'right' });

            // Search Filter Data
            sendSearchFilterDataToHandler(data);
        }
        //
        // FG No Data
        function fgNoData() {

            // Hide - Loading Spinner
            hideLoadingSpinner();
        }
        //
        // FG Lease Apps - Data Load
        function fgLeaseAppsDataLoad(sender) {

            // Hide - Loading Spinner
            hideLoadingSpinner();

            // Update Grid Results Total
            updateGridResultsTotal(sender, "la-rel", "la-rel-s");

            // Added by thong on 10 Nov 2011, Update Related workflows, site information
            updateRelatedWorkflows(sender, "la-lbl", "la-lbl-s");

            // Workflow Button
            $('.workflow-button').click(function () {

                var a = $(this).parent().find('.workflow-menu');
                $('.tmobile .workflow-menu').not(a).slideUp();
                $(a).slideToggle();
            });
        }
        //
        // FG Tower Mods - Data Load
        function fgTowerModsDataLoad(sender, args) {

            // Hide - Loading Spinner
            hideLoadingSpinner();

            // Update Grid Results Total
            updateGridResultsTotal(sender, "tm-rel", "tm-rel-s");

            // Added by thong on 10 Nov 2011, Update Related workflows, site information
            updateRelatedWorkflows(sender, "tm-lbl", "tm-lbl-s");
        }
        //
        // FG SDP - Data Load
        function fgSdpDataLoad(sender, args) {

            // Hide - Loading Spinner
            hideLoadingSpinner();

            // Update Grid Results Total
            updateGridResultsTotal(sender, "sdp-rel", "sdp-rel-s");

            // Added by thong on 10 Nov 2011, Update Related workflows, site information
            updateRelatedWorkflows(sender, "sdp-lbl", "sdp-lbl-s");

        }
        //
        // Update Grid Results Total
        function updateGridResultsTotal(sender, h2Id, h2SpanId) {

            var total = !String($(sender._fx).get(0).p.total).isNullOrEmpty() ? Number($(sender._fx).get(0).p.total) : 0;

            $('#' + h2Id).html(total);
            if (total == 1) {
                $('#' + h2SpanId).hide();
            } else {
                $('#' + h2SpanId).show();
            }
        }
        //
        // FG Lease Apps - Row Render
        function fgLeaseAppsRowRender(tdCell, data, dataIdx) {

            var sHtml = "";
            var cVal = new String(data.cell[dataIdx]);

            // Type?
            switch (Number(dataIdx)) {

                // Date 
                case 4:
                    if (cVal.isNullOrEmpty()) {
                        tdCell.innerHTML = ConstGlobal.DATA.noDataHtml;
                    } else {
                        tdCell.innerHTML = cVal;
                    }
                    break;

            // Tower Mod 
            case 6:
                sHtml = getColumnImageIcon(ColumnTypes.TOWER_MOD, cVal, data.cell[0]);
                tdCell.innerHTML = sHtml;
                break;

            // Rogue Equipment  
            case 7:
                sHtml = getColumnImageIcon(ColumnTypes.ROGUE_EQUIP, cVal);
                tdCell.innerHTML = sHtml;
                break;

            // Standard String 
            default:
                if (cVal.isNullOrEmpty()) {
                    tdCell.innerHTML = ConstGlobal.DATA.noDataHtml;
                } else {
                    tdCell.innerHTML = cVal;
                }
                break;
            }
            //>
        }
        //
        // FG Tower Modifications - Row Render
        function fgTowerModsRowRender(tdCell, data, dataIdx) {

            var cVal = new String(data.cell[dataIdx]);

            if (cVal.isNullOrEmpty()) {
                tdCell.innerHTML = ConstGlobal.DATA.noDataHtml;
            } else {

                // Type?
                switch (Number(dataIdx)) {

                    // Date 
                    case 5:
                        tdCell.innerHTML = cVal;
                        break;

                    // Date  
                    case 6:
                        tdCell.innerHTML = cVal;
                        break;

                // Rogue Equipment    
                case 7:
                    sHtml = getColumnImageIcon(ColumnTypes.ROGUE_EQUIP, cVal, null);
                    tdCell.innerHTML = sHtml;
                    break;

                // Standard String  
                default:
                    tdCell.innerHTML = cVal;
                    break;
                }
                //>
            }
        }
        //
        // FG SDP - Row Render
        function fgSdpRowRender(tdCell, data, dataIdx) {

            var cVal = new String(data.cell[dataIdx]);

            if (cVal.isNullOrEmpty()) {
                tdCell.innerHTML = ConstGlobal.DATA.noDataHtml;
            } else {

                // Type?
                switch (Number(dataIdx)) {

                    // Date 
                    case 3:
                        tdCell.innerHTML = cVal;
                        break;

                    // Date  
                    case 5:
                        tdCell.innerHTML = cVal;
                        break;

                // Standard String  
                default:
                    tdCell.innerHTML = cVal;
                    break;
                }
                //>
            }
        }
        //
        // FG Issue Tracker - Data Load
        function fgIssueTrackerDataLoad(sender) {

            // Hide - Loading Spinner
            hideLoadingSpinner();

            // Update Grid Results Total
            updateGridResultsTotal(sender, "issue-tracker-rel", "issue-tracker-rel-s");
        }
        //
        //
        // FG Issue Tracker Row Render
        function fgIssueTrackerRowRender(tdCell, data, dataIdx) {

            var cVal = new String(data.cell[dataIdx]);

            // Type?
            switch (Number(dataIdx)) {

                // Date 
                case 5:
                    if (cVal.isNullOrEmpty()) {
                        tdCell.innerHTML = ConstGlobal.DATA.noDataHtml;
                    } else {
                        //alert(cVal);
                        tdCell.innerHTML = cVal;
                    }
                    break;

            // Standard String  
            default:
                if (cVal.isNullOrEmpty()) {
                    tdCell.innerHTML = ConstGlobal.DATA.noDataHtml;
                } else {
                    tdCell.innerHTML = cVal;
                }
                break;
            }
            //>
        }
        //
        //
        // Hide Loading Spinner
        function hideLoadingSpinner() {
            $('#h1').activity(false);
        }
        //
        //
        // Update Related Workflow in Site Information, top part
        function updateRelatedWorkflows(sender, idtotal, idPostfix_s) {

            var total = !String($(sender._fx).get(0).p.total).isNullOrEmpty() ? Number($(sender._fx).get(0).p.total) : 0;

            $('#' + idtotal).html(total);
            if (total == 1) {
                $('#' + idPostfix_s).hide();
            } else {
                $('#' + idPostfix_s).show();
            }

        }
        //-->

        // FG Column Change
        function fgColumnChange(cid, visible) {
            DynamicColumnChange(currentFlexiGridID,cid, visible);
        }
        //-->

        //Lease Admin Tracker Grid 
        //--------------------Grid Sites Row Render-----------------------//
        function fgLeaseAdminRowRender(tdCell, data, dataIdx) {

            var cVal = new String(data.cell[dataIdx]);

            // Type?
            switch (Number(dataIdx)) {
                //Display revenue share  
                case 5:
                    if (cVal.isNullOrEmpty()) {
                        tdCell.innerHTML = ConstGlobal.DATA.noDataHtml;
                    } else {
                        //alert(cVal);
                        tdCell.innerHTML = cVal;
                    }
                    break;

                // Standard String   
                default:
                    if (cVal.isNullOrEmpty()) {
                        tdCell.innerHTML = ConstGlobal.DATA.noDataHtml;
                    } else {
                        tdCell.innerHTML = cVal;
                    }
                    break;
            }
            //>
        }


        //--------------------FG Row Click-------------------------------//
        function fgLeaseAdminRowClick(sender, args) {

            return false;
        }

        //--------------------FG Data Load-------------------------------//
        function fgLeaseAdminDataLoad(sender, args) {

            // Hide - Loading Spinner 
            $('.h1').activity(false);

            updateGridResultsTotal(sender, "lease-admin-rel", "lease-admin-rel-s");
        }
    </script>    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   <asp:Panel ID="pnlNoID" runat="server" CssClass="align-center">
      <h1>Not Found (404)</h1>

      <p>The site specified is invalid or no longer exists.</p>

      <p>&nbsp;</p>

      <p>
         <a href="Default.aspx">&laquo; Go back to list page.</a>
      </p>

      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
    </asp:Panel>

    <asp:Panel ID="pnlSectionContent" runat="server">        
       <div class="section content">
           <div class="wrapper">
               <div class="action-tab">
                    <div class="left-side">
                        <asp:HyperLink ID="backToList" runat="server" Text="« Return to List" NavigateUrl="~/Sites/" CssClass="site-link-no-underline"></asp:HyperLink>
                    </div>
               </div>
               <div class="site-information">
                   <h1 id="h1">Site Information <span></span>
                       <asp:HyperLink ID="lblSiteID" runat="server" Text="" CssClass="h1-span-site-link site-link-no-underline"></asp:HyperLink>
                       <span> / </span>
                       <asp:Label ID="lblSiteName" runat="server" Text=""></asp:Label>
                   </h1>
                   <div id="tabs" class="">
                       <ul class="tabs">
                           <li>
                               <a href="#general-site-information">General Site Information</a>
                           </li>
                       </ul>
                       <div id="general-site-information">
                           <div style="padding: 20px;">
                               <div class="i-2-3">
                                   <ul class="information-list">
                                       <li>
                                           General Information</li>
                                       <li>
                                           Region:
                                           <asp:Label ID="lblRegion" runat="server" Text=""></asp:Label></li>
                                       <li>
                                           Market:
                                           <asp:Label ID="lblMarket" runat="server" Text=""></asp:Label></li>
                                       <li>
                                           Submarket:
                                           <asp:Label ID="lblSubmarket" runat="server" Text=""></asp:Label></li>
                                       <li>
                                           <asp:Label ID="lblPreviousUIDFront" runat="server">Previous UID:</asp:Label>
                                           <asp:Label ID="lblPreviousUID" runat="server" Text=""></asp:Label></li>
                                       <li>
                                           Tower Modifications:
                                           <asp:Label ID="lblTowerMod" runat="server" Text=""></asp:Label></li>
                                       <li>
                                           Rogue Equipment:
                                           <asp:Label ID="lblRogueEquip" runat="server" Text=""></asp:Label></li>
                                   </ul>
                                   <ul class="information-list">
                                       <li>
                                           Other Information</li>
                                       <li>
                                           FAA Lighting Required:
                                           <asp:Label ID="lblFAAReq" runat="server" Text="Yes"></asp:Label>
                                       </li>
                                       <li>
                                           Management Status:
                                           <asp:Label ID="lblManagementStatus" runat="server" Text=""></asp:Label>
                                       </li>
                                   </ul>
                                   <ul class="information-list">
                                       <li>Tower Address</li>
                                       <li>
                                           <asp:HyperLink ID="link_googlemaps" NavigateUrl="javascript:void(0)" Target="_blank" runat="server"> </asp:HyperLink>
                                           <br /><br/>
                                           <asp:Label ID="lblSiteLatLongHeader" runat="server">Latitude, Longitude:<br /></asp:Label>
                                           <asp:HyperLink ID="hypSiteLatLong" Target="_blank" runat="server">
                                               <asp:Label ID="lblDegreeLatitude" runat="server" ></asp:Label>,&nbsp;<asp:Label ID="lblDegreeLongtitude" runat="server"></asp:Label><br/>
                                               <asp:Label ID="lblSiteLatitude" runat="server" Text="Label"></asp:Label>,&nbsp;<asp:Label ID="lblSiteLongitude" runat="server" Text="Label"></asp:Label>
                                           </asp:HyperLink>
                                       </li>                  
                                      <li>&nbsp;</li>
                                   </ul>
                                   <ul class="information-list">
                                       <li>
                                           Owners</li>
                                       <li>
                                           <asp:Label ID="lblStructureOwner" runat="server" Text="Structure Owner"></asp:Label></li>
                                       <li>
                                           <asp:Label ID="lblGroundOwner" runat="server" Text="Ground Owner"></asp:Label></li>
                                   </ul>
                                   <ul class="information-list" id="ulRelatedWorkflows" runat="server">
                                       <li>
                                           Related Workflows</li>
                                       <li>
                                           <a id="link_la" href="" runat="server">
                                               <span id="la-lbl"></span>&nbsp;Lease Application<span id="la-lbl-s">s</span>
                                           </a>
                                       </li>
                                       <li>
                                           <a id="link_tw" href="" runat="server">
                                               <span id="tm-lbl"></span>&nbsp;Tower Modification<span id="tm-lbl-s">s</span>
                                           </a>
                                       </li>
                                       <li>
                                           <a id="link_sdp" href="" runat="server">
                                               <span id="sdp-lbl"></span>&nbsp;Site Data Package<span id="sdp-lbl-s">s</span>
                                           </a>
                                       </li>
                                   </ul>
                                   <ul class="information-list">
                                       <li>
                                           Structure Details</li>
                                       <li>
                                           Type:
                                           <asp:Label ID="lblStructureDetails_Type" runat="server" Text="Monopole"></asp:Label></li>
                                       <li>
                                           Height:
                                           <asp:Label ID="lblStructureDetails_Height" runat="server" Text="100"></asp:Label></li>
                                       <li>
                                           Status:
                                           <asp:Label ID="lblStructureDetails_Status" runat="server" Text="On-Air"></asp:Label>
                                           <asp:Image ID="imgStructureDetails_Construction" runat="server" AlternateText="Construction" CssClass="construction-img" />
                                           </li>
                                       <li>
                                           On Air Date:
                                           <asp:Label ID="lblStructureDetails_siteOnAirDate" runat="server" Text="On-Air-Date"></asp:Label></li>

                                       <li>
                                           <asp:Panel ID="siteOffAirDatePanel" runat="server">
                                               Off Air Date:
                                               <asp:Label ID="lblStructureDetails_siteOffAirDate" runat="server" Text="Off-Air-Date"></asp:Label>
                                           </asp:Panel>
                                        </li>
                                   </ul>
                               </div>
                               <div class="i-1-3">
                                   <div class="map">
                                       <asp:Image ID="Image2" Width="310" Height="235" scrolling="no" marginheight="0" runat="server" />
                                   </div>
                                   <!-- .map -->
                               </div>
                               <div class="cb">
                               </div>
                           </div>
                            <a id="modalBtnExternal" class="edit-button" href="javascript:void(0)">edit</a>
                            <!-- Modal -->
                            <div id="modalExternal" class="modal-container" style="overflow: hidden;"></div>
                            <!-- /Modal -->
                       </div>
                   </div>
                   <!-- #tab -->
                   <div class="cb">
                   </div>
               </div>

               <div class="site-list-information">

                   <!-- Lease Applications Grid -->
                   <div class="site-list">
                       <h2><span id="la-rel">0</span> Related Lease Application<span id="la-rel-s">s</span></h2>
                       <div style="overflow:visible; margin-bottom:30px; position:relative;">
                           <fx:Flexigrid ID="fgApplications"
                               Width="990"
                               ResultsPerPage="20"
			                   ShowPager="true"
                               Resizable="false"
                               ShowToggleButton="false"
                               ShowTableToggleButton="false"
                               SearchEnabled="false"
                               UseCustomTheme="true"
                               CssClass="tmobile"
                               WrapCellText="true"
                               DoNotIncludeJQuery="true"
                               OnClientBeforeSendData="fgBeforeSendData"
                               OnClientDataLoad="fgLeaseAppsDataLoad"
                               OnClientNoDataLoad="fgNoData"
			                   HandlerUrl="~/LeaseAppList.axd"
                               runat="server">
		                       <Columns>
			                       <fx:FlexiColumn Code="site_uid" Text="Site ID" Width="80" />
                                   <fx:FlexiColumn Code="customer_site_name" Text="Site Name" Width="282" OnRowRender="fgLeaseAppsRowRender" />
                                   <fx:FlexiColumn Code="customer.customer_name" Text="Customer" Width="160" OnRowRender="fgLeaseAppsRowRender" />
                                   <fx:FlexiColumn Code="leaseapp_types.lease_application_type" Text="App Type" Width="100" />
                                   <fx:FlexiColumn Code="leaseapp_rcvd_date" Text="Received" Width="80" OnRowRender="fgLeaseAppsRowRender" />
                                   <fx:FlexiColumn Code="leaseapp_statuses.lease_application_status" Text="Status" Width="70" />
                                   <fx:FlexiColumn Code="site.tower_modification_yn" Text="TM" Width="32" OnRowRender="fgLeaseAppsRowRender" />
                                   <fx:FlexiColumn Code="site.rogue_equipment_yn" Text="RE" Width="32" OnRowRender="fgLeaseAppsRowRender" />
                                   <fx:FlexiColumn Code="workflows" Text="Workflows" Width="70" AllowSort="false"
                                       Format="<div class='workflow-container' style='position:relative;'><a href='javascript:void(0)' class='fancy-button workflow-button'>menu<div class='arrow-down-icon'></div><div class='cog-icon'></div></a><ul class='workflow-menu'>{0}</ul></div>" />
                                   <fx:FlexiColumn Code="id" Text="Actions" Width="65" AllowSort="false"
                                       Format="<a class='fancy-button' href='/LeaseApplications/Edit.aspx?id={0}'>Edit</a>" />
		                       </Columns>
	                       </fx:Flexigrid>
                           <div class="cb"></div>
                       </div>
                   </div>
                   <!-- /Lease Applications Grid -->

                   <!-- Tower Modifications Grid -->
                   <div class="site-list">
                       <h2><span id="tm-rel">0</span> Related Tower Modification<span id="tm-rel-s">s</span></h2>
                       <div style="overflow:visible; margin-bottom:30px;position:relative;">
                           <fx:Flexigrid ID="fgModifications"
                               Width="990"
                               ResultsPerPage="20"
			                   ShowPager="true"
                               Resizable="false"
                               ShowToggleButton="false"
                               ShowTableToggleButton="false"
                               SearchEnabled="false"
                               UseCustomTheme="true"
                               CssClass="tmobile"
                               WrapCellText="true"
                               DoNotIncludeJQuery="true"
                               OnClientBeforeSendData="fgBeforeSendData"
                               OnClientDataLoad="fgTowerModsDataLoad"
                               OnClientNoDataLoad="fgNoData"
			                   HandlerUrl="~/TowerModList.axd"
                               runat="server">
		                       <Columns>
			                       <fx:FlexiColumn Code="site_uid" Text="Site ID" Width="80" />
                                   <fx:FlexiColumn Code="customer.customer_name" Text="Customer" Width="160"  />
                                   <fx:FlexiColumn Code="tower_mod_statuses.tower_mod_status_name" Text="Tower Mod Status" Width="160" />
                                   <fx:FlexiColumn Code="tower_mod_types.tower_mod_type" Text="Tower Mod Type" Width="126" />
                                   <fx:FlexiColumn Code="tower_mod_pos.tower_mod_phase_types.phase_name" Text="Current Phase" Width="175" OnRowRender="fgTowerModsRowRender" />
                                   <fx:FlexiColumn Code="tower_mod_pos.ebid_req_rcvd_date" Text="Packet Rcvd" Width="85" OnRowRender="fgTowerModsRowRender" />
                                   <fx:FlexiColumn Code="project_complete_date" Text="Complete" Width="85" OnRowRender="fgTowerModsRowRender" />
                                   <fx:FlexiColumn Code="site.rogue_equipment_yn" Text="RE" Width="32" OnRowRender="fgTowerModsRowRender" />
                                   <fx:FlexiColumn Code="id" Text="Actions" Width="65" AllowSort="false"
                                                   Format="<a class='fancy-button' href='/TowerModifications/Edit.aspx?id={0}'>Edit</a>" />
		                       </Columns>
	                       </fx:Flexigrid>
                           <div class="cb"></div>
                       </div>
                   </div>
                   <!-- /Tower Modifications Grid -->

                   <!-- Site Data Packages Grid -->
                   <div class="site-list">
                       <h2><span id="sdp-rel">0</span> Related Site Data Package<span id="sdp-rel-s">s</span></h2>
                       <div style="overflow:visible; margin-bottom: 30px;position:relative;">
                           <fx:Flexigrid ID="fgPackages"
                               Width="990"
                               ResultsPerPage="20"
			                      ShowPager="true"
                               Resizable="false"
                               ShowToggleButton="false"
                               ShowTableToggleButton="false"
                               SearchEnabled="false"
                               UseCustomTheme="true"
                               CssClass="tmobile"
                               WrapCellText="true"
                               DoNotIncludeJQuery="true"
                               OnClientBeforeSendData="fgBeforeSendData"
                               OnClientDataLoad="fgSdpDataLoad"
                               OnClientNoDataLoad="fgNoData"
			                      HandlerUrl="~/SdpList.axd"
                               runat="server">
		                       <Columns>
			                       <fx:FlexiColumn Code="site_uid" Text="Site ID" Width="80" />
                                   <fx:FlexiColumn Code="site.region_name" Text="Site Region Name" Width="110" OnRowRender="fgSdpRowRender"  />
                                   <fx:FlexiColumn Code="site.market_code" Text="Site Mkt Code" Width="81" OnRowRender="fgSdpRowRender" />
                                   <fx:FlexiColumn Code="site.site_on_air_date" Text="Site On Air Date" Width="90" OnRowRender="fgSdpRowRender" />
                                   <fx:FlexiColumn Code="sdp_priorities.sdp_priority" Text="SDP Priority" Width="70" OnRowRender="fgSdpRowRender" />
                                   <fx:FlexiColumn Code="sdp_request_date" Text="SDP Req Date" Width="100" OnRowRender="fgSdpRowRender" />
                                   <fx:FlexiColumn Code="sdp_request_types.sdp_request_type" Text="SDP Req Type" Width="90" OnRowRender="fgSdpRowRender" />
                                   <fx:FlexiColumn Code="user6.tmo_userid" Text="SDP TMO PM Emp" Width="140" OnRowRender="fgSdpRowRender" />
                                   <fx:FlexiColumn Code="user7.tmo_userid" Text="SDP TMO Spec Cur Emp" Width="140" OnRowRender="fgSdpRowRender" />
                                   <fx:FlexiColumn Code="id" Text="Actions" Width="65" AllowSort="false"
                                                   Format="<a class='fancy-button' href='/SiteDataPackages/Edit.aspx?id={0}'>Edit</a>" />
		                       </Columns>
	                       </fx:Flexigrid>
                           <div class="cb"></div>
                       </div>
                   </div>
                   <!-- /Site Data Packages Grid -->
                
                   <!-- Issue Tracker Grid -->
                   <div class="site-list">
                       <h2><span id="issue-tracker-rel">0</span> Related Issue Tracker Item<span id="issue-tracker-rel-s">s</span></h2>
                       <div style="overflow:visible; margin-bottom: 30px;position:relative;">
                           <fx:Flexigrid ID="fgIssueTracker"
                               Width="990"
                               ResultsPerPage="20"
			                   ShowPager="true"
                               Resizable="false"
                               ShowToggleButton="false"
                               ShowTableToggleButton="false"
                               SearchEnabled="false"
                               UseCustomTheme="true"
                               CssClass="tmobile"
                               WrapCellText="true"
                               DoNotIncludeJQuery="true"
                               OnClientBeforeSendData="fgBeforeSendData"
                               OnClientDataLoad="fgIssueTrackerDataLoad"
                               OnClientNoDataLoad="fgNoData"
			                   HandlerUrl="~/IssueTrackerList.axd"
                               runat="server">
		                       <Columns>
		                           <fx:FlexiColumn Code="id" Text="Issue ID" Width="70" Format="TMIT-{0}" />
			                       <fx:FlexiColumn Code="site.site_uid" Text="Site ID" Width="80" />
                                   <fx:FlexiColumn Code="site.site_name" Text="Site Name" Width="200"  />
		                           <fx:FlexiColumn Code="issue_summary" Text="Issue Summary" Width="290" Format="<span style='display:block;padding:0 5px 0 5px; text-align:left;'>{0}</span>" />
                                   <fx:FlexiColumn Code="issue_priority_types.id" Text="Priority" Width="120" />
                                   <fx:FlexiColumn Code="created_at" Text="Created" Width="80" OnRowRender="fgIssueTrackerRowRender" />
                                   <fx:FlexiColumn Code="issue_statuses.id" Text="Status" Width="70" />
                                   <fx:FlexiColumn Code="id" Text="Actions" Width="60" AllowSort="false"
                                                   Format="<a class='fancy-button' href='/IssueTracker/Edit.aspx?id={0}'>Edit</a>" />
		                       </Columns>
	                       </fx:Flexigrid>
                           <div class="cb"></div>
                       </div>
                   </div>
                   <!-- /Issue Tracker Grid -->

                   <!-- Lease Admin Tracker Grid -->
                   <div class="lease-admin-list">
                       <h2><span id="lease-admin-rel">0</span> Related Lease Admin Tracker<span id="lease-admin-rel-s">s</span></h2>
                       <div style="overflow:visible; margin-bottom:30px;position:relative;">
                           <fx:Flexigrid ID="fgLeaseAdmin"
                                Width="990"
                                ResultsPerPage="20"
			                    ShowPager="true"
                                Resizable="false" 
                                ShowToggleButton="false"
                                ShowTableToggleButton="false"
                                SearchEnabled="false"
                                UseCustomTheme="true"
                                CssClass="tmobile"
                                WrapCellText="true"
                                DoNotIncludeJQuery="true"
                                OnClientBeforeSendData="fgBeforeSendData"
                                OnClientDataLoad="fgLeaseAdminDataLoad"
                                OnClientRowClick="fgLeaseAdminRowClick"
                                OnClientNoDataLoad="fgNoData"
			                    HandlerUrl="~/LeaseAdminTrackerList.axd"
                                runat="server">
		                       <Columns>
                                    <fx:FlexiColumn Code="CustomerName" Text="Customer" Width="108" OnRowRender="fgLeaseAdminRowRender" />
			                        <fx:FlexiColumn Code="SiteId" Text="Site ID" Width="75" />
                                    <fx:FlexiColumn Code="SiteName" Text="Site Name" Width="150" OnRowRender="fgLeaseAdminRowRender" />
                                    <fx:FlexiColumn Code="Phase" Text="Lease Phase" Width="108" OnRowRender ="fgLeaseAdminRowRender" />
                                    <fx:FlexiColumn Code="LeaseExecutionDate" Text="Execution Date" Width="125" />                                
                                    <fx:FlexiColumn Code="CommencementDate" Text="Commencement Date" Width="125" OnRowRender="fgLeaseAdminRowRender" />
                                    <fx:FlexiColumn Code="RevShare" Text="Rev Share" Width="80" OnRowRender="fgLeaseAdminRowRender" />
                                    <fx:FlexiColumn Code="DocumentType" Text="Document Type" Width="135" />
                                    <fx:FlexiColumn Code="id" Text="Actions" Width="65" AllowSort="false"
                                                    Format="<a class='fancy-button' href='/LeaseAdminTracker/Edit.aspx?id={0}'>Edit</a>" />
		                        </Columns>
	                       </fx:Flexigrid>
                           <div class="cb"></div>
                       </div>
                   </div>
                   <!-- Lease Admin Tracker Grid -->
                   
                   <!-- DMS Grid -->
                   <div class="site-list" id="dms_grid_section" runat="server" clientidmode="Static">
                        <!-- Header -->
                        <div class="o-1" style="margin:0;">
	                        <h2 id="h2" style="float:left; padding-top:0px; padding-right:30px;">Site Documents</h2>
	                            <div class="page-options-nav">
                                <!--
	                                <span class="dms-specify">Specify Folder:</span> <asp:TextBox ID="txtBasePath" runat="server" ClientIDMode="Static" />
                                    <a id="aGoToDir" class="fancy-button" href="javascript:void(0)" onclick="GoToDir();">Go</a>
                                    &nbsp;&nbsp;
                                    -->
                                    <input type="button" id="modalBtnExternal-create-folder" class="ui-button ui-widget ui-state-default ui-corner-all" value="Create Folder" style="margin-left:16px;"  />
                                    <input type="button" id="modalBtnExternalDms" class="ui-button ui-widget ui-state-default ui-corner-all" value="Upload Document" />

	                            </div>
	                        <div class="cb"></div>
                        </div>
                        <!-- /Header -->
                        
                        <div class="dms-folder-bar-cont">
                            Current Path:&nbsp;<span id="sBasePath"><a href="javascript:void(0);" onclick="ChangeDirectory('/');">Home</a></span>
                        </div>

                        <div style="overflow:visible; margin-bottom: 20px; position:relative;">
                           
                            <fx:Flexigrid ID="fgDMS"
                                Width="990"
                                ResultsPerPage="20"
		                        ShowPager="true"
                                Resizable="false"
                                ShowToggleButton="false"
                                ShowTableToggleButton="false"
                                SearchEnabled="false"
                                UseCustomTheme="true"
                                CssClass="tmobile"
                                WrapCellText="true"
                                DoNotIncludeJQuery="true"
                                OnClientBeforeSendData="fgDMSBeforeSendData"
                                OnClientDataLoad="fgDMSDataLoad"
                                OnClientRowClick="fgDMSRowClick"
                                OnClientNoDataLoad="fgDMSNoData"
                                OnClientColumnToggle="fgColumnChange"
		                        HandlerUrl="~/DMSFlexiGrid.axd"
                                runat="server">
		                        <Columns>
			                        <fx:FlexiColumn Code="Option" Text=" " Width="25"/>
			                        <fx:FlexiColumn Code="FileName" Text="Name" Width="197" AllowSort="false"  />
                                    <fx:FlexiColumn Code="Folder" Text="Folder" Width="110" IsVisible="false" AllowSort="false"  />
                                    <fx:FlexiColumn Code="Metadata.Classification" Text="Classification" Width="160" AllowSort="false" />
                                    <fx:FlexiColumn Code="Metadata.Keywords" Text="Keywords" Width="120" IsVisible="False" AllowSort="false"  />
                                    <fx:FlexiColumn Code="Metadata.Description" Text="Description" Width="165" AllowSort="false" />                        
                                    <fx:FlexiColumn Code="ItemObject.Size" Text="Size" Width="70" AllowSort="false"  />
                                    <fx:FlexiColumn Code="Metadata.UploadedBy" Text="Uploaded By" Width="110" AllowSort="false"   />
                                    <fx:FlexiColumn Code="Revision" Text="Rev" Width="30" AllowSort="false"   />
                                    <fx:FlexiColumn Code="Metadata.UploadDate" Text="Upload Date" Width="90" AllowSort="false"  />
                                    <fx:FlexiColumn Code="Metadata.isCheckedOut" Text=" " Width="32" AllowSort="false" IsVisible="true"  />
                                    <fx:FlexiColumn Code="actions" Text="Actions" Width="90" AllowSort="False" />
		                        </Columns>
	                        </fx:Flexigrid>

                        </div>
                        <div class="cb"></div>
                    </div>

                   <!-- DMS Unauthorized Access -->
                   <div class="site-list" id="dms_grid_unauthorized" runat="server" clientidmode="Static">
                        <!-- Header -->
                        <div class="o-1" style="margin:0;">
	                        <h2 id="h3" style="float:left; padding-top:0px; padding-right:30px;">Site Documents</h2>
	                            <div class="page-options-nav">
                               &nbsp;
	                            </div>
	                        <div class="cb"></div>
                        </div>
                        <!-- /Header -->
                        
                        <div class="dms-folder-bar-cont">
                           <p>
                              You are currently unauthorized to view this content.
                           </p>

                           <p>
                              Please login with a different account if you wish to access this content.
                           </p>
   
                           <p>
                              If you have any questions, please contact the site administrator.
                           </p>
                        </div>
                    </div>

                    <div id="modalExternal-create-folder" class="modal-container" style="overflow:hidden;"></div>
                    <div id="modalExternalDms" class="modal-container" style="overflow:hidden;"></div>
                    <asp:HiddenField ID="hidBasePath" runat="server" Value="" ClientIDMode="Static"/>
                    <asp:HiddenField id="hidDocumentDownload" ClientIDMode="Static" runat="server"/>
                    <asp:HiddenField id="hidLocationHash" ClientIDMode="Static" runat="server"/>
                    <%--Fix size column simple value : { Fix: ['Site ID','Site Name','Actions']}--%>
                    <asp:HiddenField ID="hiddenFixColumn" runat="server" ClientIDMode="Static" Value="{ Fix: ['Actions']}" />
                    <asp:HiddenField ID="hiddenTmpFlexiColumn" runat="server" ClientIDMode="Static" Value="" />
                    <asp:HiddenField id="hidIsllDocumentFolder" ClientIDMode="Static" runat="server"/>
                    <!-- /DMS Grid -->
                    
                    <div id="modalDocument" class="modal-container" style="overflow:hidden;"></div>
               </div>
               <!-- site information -->
           </div>
           <!-- .wrapper -->
       </div>
    </asp:Panel>
    <!-- .section content -->
</asp:Content>