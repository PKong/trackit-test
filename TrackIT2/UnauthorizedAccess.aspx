﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/TrackIT2.Master" AutoEventWireup="true" CodeBehind="UnauthorizedAccess.aspx.cs" Inherits="TrackIT2.UnauthorizedAccess" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   <div style="text-align: center; min-height: 300px; padding: 50px 0 0 0; line-height: 180%;">
      <h2>Unauthorized Access</h2>
      
      <p>
         You have attempted to access a page that you are not authorized to view.
      </p>

      <p>
         Please login with a different account if you wish to access this page.
      </p>
   
      <p>
         If you have any questions, please contact the site administrator.
      </p>
   </div>
</asp:Content>