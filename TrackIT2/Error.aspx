﻿<%@ Page Title="Error | TrackiT2" Language="C#" MasterPageFile="~/Templates/TrackIT2.Master" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="TrackIT2.Error" %>
<%@ MasterType TypeName="TrackIT2.Templates.TrackIT2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div style="text-align: center; min-height: 300px; padding: 50px 0 0 0; line-height: 180%;">        
        <div id="div404" Visible="False" runat="server">
            <h1>Not Found (404)</h1>

            <p>The request Page / URL was not found on this server.</p>

            <p>&nbsp;</p>

            <p>
               <a href="javascript:back(-1)">&laquo; Go back.</a>
            </p>

            <p>
               <a href="/">Go to the Dashboard &raquo;</a>
            </p>
        </div>

        <div id="divgeneric" runat="server">
            <h1>Sorry, but an unexpected error occurred!</h1>

            <p>
                The <a href="elmah.axd">error log</a> will have more details on what has happened.
            </p>

            <p>Please contact a system administrator if this error / situation continues.</p>
            
            <p>
                <a href="javascript:history.back();">&laquo; Go back.</a>
            </p>
        </div>
    </div>
</asp:Content>
