﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using TrackIT2.BLL;
using TrackIT2.DAL;
using UserInfo = TrackIT2.BLL.UserInfo;
using Utility = TrackIT2.BLL.Utility;

namespace TrackIT2.IssueTracker
{
    /// <summary>
    /// 
    /// </summary>
    public partial class AddIssueTrackerItem : System.Web.UI.Page
    {
        // Var
        private int _issueId;
        private Hashtable _htVarCharInvalidField;
        private const string BUTTON_SAVE_TEXT = "Save Issue";
        //>

        #region Properties

        /// <summary>
        /// Issue Tracker
        /// </summary>
        public issue_tracker IssueTracker
        {
            get
            {
                return (issue_tracker)ViewState["IssueTracker"];
            }
            set
            {
                ViewState["IssueTracker"] = value;
            }
        }
        //>

        #endregion

        /// <summary>
        /// Page Init
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Init(object sender, EventArgs e)
        {
            // TODO: code something!
        }
        //-->

        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                // Bind Lookup Lists
                BindLookupLists();

                // Editing?
                if ((Request.QueryString["id"] == null) || (!int.TryParse(Request.QueryString["id"], out _issueId)))
                    return;

                // Yes - Bind Issue
                BindIssue();
                //get document links
                GetLinkDocument();
            }
            else
            {
                if (Request.Params["__EVENTARGUMENT"] == "DownloadDocument")
                {
                    string keyName = hidDocumentDownload.Value;
                    if (!string.IsNullOrEmpty(keyName))
                    {
                        try
                        {
                            AmazonDMS.DownloadObject(Server.UrlDecode(keyName));
                        }
                        catch (Amazon.S3.AmazonS3Exception ex)
                        {
                            Master.StatusBox.showErrorMessage(ex.Message);
                        }
                    }
                }
            }
        }
        //-->

        #region Binding

        /// <summary>
        /// Bind Lookup Lists
        /// </summary>
        protected void BindLookupLists()
        {

            using (var ce = new CPTTEntities())
            {

                // Priority
                var priorities = ce.issue_priority_types;
                foreach (var prs in priorities)
                {
                    var prItem = new ListItem("(" + prs.issue_priority_number + ") " + prs.issue_priority_type, prs.id.ToString(CultureInfo.InvariantCulture));
                    ddlPriority.Items.Add(prItem);
                }
                //>

                // Status
                var statuses = ce.issue_statuses;
                var sort_issue_statuses = statuses.ToList();
                var dynamic_issue_statuses = new DynamicComparer<issue_statuses>();
                dynamic_issue_statuses.SortOrder(x => x.issue_status);
                sort_issue_statuses.Sort(dynamic_issue_statuses);
                Utility.BindDDLDataSource(ddlStatus, sort_issue_statuses, "id", "issue_status", "- Select -");

                // Tenants (Customers)
                var customers = ce.customers.OrderBy(Utility.GetField<customer>("customer_name")).OrderBy(Utility.GetIntField<customer>("sort_order"));
                Utility.BindDDLDataSource(lsbCustomer, customers, "id", "customer_name", "- None -");

                // Current Specialist
                List<UserInfo> employees = BLL.User.GetUserInfo();
                var sort_employees = employees.ToList();
                var DynamicComparer_employees = new DynamicComparer<UserInfo>();
                DynamicComparer_employees.SortOrder(x => x.FullName);
                sort_employees.Sort(DynamicComparer_employees);
                Utility.BindDDLDataSource(ddlCurrentSpecialist, sort_employees, "ID", "FullName", "- Select -");
                // Project Manager
                Utility.BindDDLDataSource(ddlProjectManager, sort_employees, "ID", "FullName", "- Select -");
            }
        }
        //-->

        /// <summary>
        /// 
        /// </summary>
        protected void BindIssue()
        {
            // Setup
            btnSubmitCreateIssue.Text = BUTTON_SAVE_TEXT;

            using (var ce = new CPTTEntities())
            {
                // Get the Entity Record
                var issue = BLL.IssueTracker.Search(_issueId);
                IssueTracker = issue;
                ce.Attach(issue);

                // Site ID
                if (issue.site != null)
                {
                    Utility.ControlValueSetter(ctrSiteID.Controls[1], issue.site.site_uid);
                }

                // Priority & Status
                Utility.ControlValueSetter(ddlPriority, issue.issue_priority_type_id);
                Utility.ControlValueSetter(ddlStatus, issue.issue_status_id);

                // Team & Tenants
                Utility.ControlValueSetter(ddlCurrentSpecialist, issue.specialist_user_id);
                Utility.ControlValueSetter(ddlProjectManager, issue.senior_pm_user_id);

                // Tenants
                if (issue.issue_tracker_tenants != null)
                {
                    foreach (var item in issue.issue_tracker_tenants.SelectMany(tenant1 => lsbCustomer.Items.Cast<ListItem>().Where(item => item.Value == tenant1.customer.id.ToString(CultureInfo.InvariantCulture))))
                    {
                        item.Selected = true;
                    }
                }
                //>

                // Issue Summary
                Utility.ControlValueSetter(txtIssueSummary, issue.issue_summary);
            }
        }
        //-->

        #endregion

        #region Data Actions

        /// <summary>
        /// Btn Submit Create Issue Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnSubmitCreateIssueClick(object sender, EventArgs e)
        {
            // Setup
            _htVarCharInvalidField = new Hashtable(); // server-side field length validation
            var blnIsValid = true;
            var blnSiteIDIsValid = true;

            // Create or Get Issue
            //var issue = new issue_tracker();
            var issue = (btnSubmitCreateIssue.Text != BUTTON_SAVE_TEXT ? new issue_tracker() : IssueTracker);

            // Get Form Values
            //-------------------

            // Site ID
            var txtSiteId = ctrSiteID.Controls[1] as TextBox;
            if (txtSiteId != null) issue.site_uid = Utility.PrepareString(txtSiteId.Text.ToUpper());

            // Priority (required)
            issue.issue_priority_type_id = Utility.PrepareInt(ddlPriority.SelectedValue);
            if (issue.issue_priority_type_id == null) blnIsValid = false;

            // Current Specialist
            issue.specialist_user_id = Utility.PrepareInt(ddlCurrentSpecialist.SelectedValue);

            // Project Manager
            issue.senior_pm_user_id = Utility.PrepareInt(ddlProjectManager.SelectedValue);

            // Issue Summary (required)
            issue.issue_summary = Utility.PrepareString(txtIssueSummary.Text);
            if (string.IsNullOrEmpty(issue.issue_summary)) blnIsValid = false;

            // Status (required)
            issue.issue_status_id = Utility.PrepareInt(ddlStatus.SelectedValue);
            if (issue.issue_status_id == null) blnIsValid = false;

            // Site ID
            var txtSiteUid = ctrSiteID.Controls[1] as TextBox;
            if (txtSiteUid != null)
            {
                issue.site_uid = Utility.PrepareString(txtSiteUid.Text.ToUpper());

                // Not specifying a Site ID is allowed. If one is specified, 
                // verify that it is a valid site.
                if (issue.site_uid != null)
                {
                    if (BLL.Site.Search(issue.site_uid) == null)
                    {
                        blnIsValid = false;
                        blnSiteIDIsValid = false;
                        Utility.SettingErrorColorForTextbox(txtSiteUid);
                    }
                    else
                    {
                        Utility.ClearErrorColorForTextbox(txtSiteUid);
                    }
                }
            }
            //>

            // Tenants
            issue.issue_tracker_tenants.Clear(); // clear existing and re-populate from ListBox
            var selTenants = lsbCustomer.Items.Cast<ListItem>().Where(item => item.Selected);
            foreach (var item in selTenants)
            {
                if (string.IsNullOrEmpty(item.Value)) continue;
                issue.issue_tracker_tenants.Add(new issue_tracker_tenants { customer_id = Convert.ToInt32(item.Value), issue_tracker = issue});
            }
            //>

            // Valid?
            //-------------------
            if (blnIsValid)
            {
                // Create or save?
                if (btnSubmitCreateIssue.Text != BUTTON_SAVE_TEXT)
                {

                    // Create Issue
                    //-------------------
                    string result = BLL.IssueTracker.Add(issue);

                    // Success?
                    if ((result == null) && (issue.id != 0))
                    {
                        // Yes - Redirect to Edit
                        RedirectToMainIssueScreen(issue.id);
                    }
                    else
                    {
                        // No - Throw Error
                        Master.StatusBox.showStatusMessage("Error: a problem occurred while creating the Issue, this operation has been aborted.");
                    }
                    //>

                }
                else
                {
                    // Update Issue
                    //-------------------
                    string result = BLL.IssueTracker.Update(issue);

                    // Success?
                    if ((result == null))
                    {
                        // Yes - Redirect to Edit
                        RedirectToMainIssueScreen(issue.id);
                    }
                    else
                    {
                        // No - Throw Error
                        Master.StatusBox.showStatusMessage("Error: a problem occurred while updating the Issue, this operation has been aborted.");
                    }
                    //>
                }
                //>
            }
            else
            {
                // No - Throw Error
                var sbErrorMessage = new StringBuilder(Globals.ERROR_HEADER);

                // Site ID Valid?
                if (!blnSiteIDIsValid)
                {
                    //Invalid UID
                    sbErrorMessage.AppendLine(Globals.ERROR_TXT_INVALID_UID + "<br//>");
                }

                // Iterate Errors
                foreach (string item in _htVarCharInvalidField.Keys)
                {
                    if (_htVarCharInvalidField[item] != null)
                    {
                        sbErrorMessage.AppendLine(String.Format(Globals.ERROR_INVALID_LENGTH_FORMAT, item, _htVarCharInvalidField[item]));
                    }
                }
                Master.StatusBox.showStatusMessage(sbErrorMessage.ToString());
            }
            //>
        }
        //-->

        /// <summary>
        /// Redirect To Main Issue Screen
        /// </summary>
        protected void RedirectToMainIssueScreen(int id)
        {

            ClientScript.RegisterStartupScript(GetType(), "LoadAndRedirect",
                String.Format("<script type='text/javascript'>" +
                                         "   $(\"#MainContent_btnSubmitCreateIssue\").val(\"Processing...\");" +
                                         "   $(\"#MainContent_btnSubmitCreateIssue\").attr(\"disabled\", true);" +
                                         "   window.parent.location.href = 'Edit.aspx?id={0}';" +
                                         "</script>",
                id.ToString(CultureInfo.InvariantCulture)));
        }
        //-->

        #endregion

        #region Documnent Link
        private void GetLinkDocument()
        {

            List<Dms_Document> Issue = DMSDocumentLinkHelper.GetDocumentLink(IssueTracker.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.Issue.ToString()));
            SetLinkDocument(IssueTracker.id,DMSDocumentLinkHelper.eDocumentField.Issue, Issue);

        }

        private void SetLinkDocument(int ID, DMSDocumentLinkHelper.eDocumentField field ,List<Dms_Document> docData)
        {
            //insert template data in case no documents link
            if (docData.Count == 0)
            {
                Dms_Document doc = new Dms_Document();
                doc.ID = IssueTracker.id;
                doc.ref_field = field.ToString();
                docData.Add(doc);
            }

            this.hidden_document_Issue.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
        }
        #endregion
    }
}
