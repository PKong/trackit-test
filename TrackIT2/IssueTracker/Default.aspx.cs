﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using TrackIT2.BLL;
using TrackIT2.DAL;
using System.Collections.Specialized;
using System.Web.Script.Serialization;
using System.IO;
using System.Web;
using System.Text;
using TrackIT2.Handlers;
using System.ComponentModel;

namespace TrackIT2.IssueTracker
{
    /// <summary>
    /// List
    /// </summary>
    public partial class DefaultIssueTracker : System.Web.UI.Page
    {

        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindLookupLists();
                PageUtility.SetupSearchResultFilters(this, panelFilterBox);
                SetupFlexiGrid();

                // Clear Session
                DefaultFilterSession.ClearSession(HttpContext.Current, DefaultFilterSession.FilterType.IssueFilter);
            }
        }
        //-->

        /// <summary>
        /// Bind Lookup Lists
        /// </summary>
        protected void BindLookupLists()
        {

            using (var ce = new CPTTEntities())
            {
                // Issue ID
                var issues = ce.issue_tracker;
                foreach (var iT in issues)
                {
                    var item = new ListItem(Globals.ISSUE_TRACKER_ID_PREFIX + iT.id.ToString(CultureInfo.InvariantCulture), iT.id.ToString(CultureInfo.InvariantCulture));
                    ddlIssueID.Items.Add(item);
                }
                //>

                // Priority
                var priorities = ce.issue_priority_types;
                foreach (var prs in priorities)
                {
                    var prItem = new ListItem("(" + prs.issue_priority_number + ") " + prs.issue_priority_type, prs.id.ToString(CultureInfo.InvariantCulture));
                    lsbPriority.Items.Add(prItem);
                }
                //>

                // Tenants (Customers)
                var customers = ce.customers.OrderBy(Utility.GetField<customer>("customer_name")).OrderBy(Utility.GetIntField<customer>("sort_order"));
                Utility.BindDDLDataSource(lsbCustomer, customers, "id", "customer_name", null);
                //>

                // Status
                var sort_statuses = ce.issue_statuses.ToList();
                var DynamicComparer_status = new DynamicComparer<issue_statuses>();
                DynamicComparer_status.SortOrder(x => x.issue_status);
                sort_statuses.Sort(DynamicComparer_status);
                Utility.BindDDLDataSource(lsbStatus, sort_statuses, "id", "issue_status", null);

                // Create Users
                List<UserInfo> employees = BLL.User.GetUserInfo();
                var sort_employees = employees.ToList();
                var DynamicComparer_employees = new DynamicComparer<UserInfo>();
                DynamicComparer_employees.SortOrder(x => x.FullName);
                sort_employees.Sort(DynamicComparer_employees);

                Utility.BindDDLDataSource(this.lsbCreateUser, sort_employees, "ID", "FullName", null);

                // Current Specialist
                Utility.BindDDLDataSource(this.lsbCurrentSpecialist, sort_employees, "ID", "FullName", null);

                // Project Manager
                Utility.BindDDLDataSource(this.lsbProjectManager, sort_employees, "ID", "FullName", null);
            }            
        }
        //-->

        /// <summary>
        /// Setup Flexi Grid
        /// </summary>
        protected void SetupFlexiGrid()
        {
            fgIssueTracker.SortColumn = fgIssueTracker.Columns.Find(col => col.Code == "id");
            fgIssueTracker.SortDirection = FlexigridASPNET.GridSortOrder.Asc;
            fgIssueTracker.RowPerPageOptions = new[] { 10, 20, 30, 40, 50 };
            fgIssueTracker.NoItemMessage = "No Data";
        }
        //-->

        public void ExportClick(object s, EventArgs e)
        {
            try
            {
                ExportFile ef = new ExportFile();
                var searchData = hiddenSearchValue.Value;
                NameValueCollection data = new NameValueCollection();
                if (!string.IsNullOrEmpty(searchData))
                {
                    var jss = new JavaScriptSerializer();
                    var table = jss.Deserialize<dynamic>(searchData);

                    for (int x = 0; x < table.Length; x++)
                    {
                        data.Add(table[x]["name"], table[x]["value"]);
                    }
                }

                const int startIndex = 4;
                var parameters = new NameValueCollection();

                // If we have form values, extract them to build the object query.
                if (data.Count > startIndex)
                {
                   for (var i = startIndex; i < data.Count; i++)
                   {
                      var itemKey = data.GetKey(i);
                      var itemValue = data.Get(i);

                      parameters.Add(itemKey, itemValue);
                   }
                }

                NameValueCollection columnValue = ExportFile.GetColumn(hiddenColumnValue.Value);
                ExportFileHandler extHand;
                extHand = new ExportFileHandler(TrackIT2.BLL.ExportFile.eExportType.eIssueTracker, data, columnValue, parameters);
                extHand.Load_Data_OnComplete += new ExportFileHandler.Load_Data_OnComplete_Handler(Load_Data_OnComplete);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                Master.StatusBox.showStatusMessage("Export report error.");
            }
        }

        protected void Load_Data_OnComplete(List<ExportClass> loadDataResult, NameValueCollection data, NameValueCollection column, RunWorkerCompletedEventArgs e)
        {
            try
            {
                if (e.Error == null)
                {
                    ExportFile ef = new ExportFile();
                    MemoryStream mem = ef.WriteReport(loadDataResult, column, ExportFile.eExportType.eIssueTracker, Server.MapPath("/"));

                    if (mem != null)
                    {
                        string fileName = ExportFile.GenarateFileName(data["fileName"]);
                        DownloadFile(mem, fileName);
                    }
                    else
                    {
                        Master.StatusBox.showStatusMessage("Export report error.");
                    }
                }
                else
                {
                    throw e.Error;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                Master.StatusBox.showStatusMessage("Export report error.");
            }
        }

        private void DownloadFile(MemoryStream mstream, string reportName)
        {
            try
            {
                byte[] byteArray = mstream.ToArray();
                Response.Clear();

                if (Response.Cookies.Count > 0)
                {
                    bool flag = true;
                    foreach (string c in Response.Cookies)
                    {
                        if (c == "fileDownloadToken")
                        {
                            Response.Cookies["fileDownloadToken"].Value = hiddenDownloadFlag.Value;
                            flag = false;
                        }
                    }
                    if (flag)
                    {
                        Response.AppendCookie(new HttpCookie("fileDownloadToken", hiddenDownloadFlag.Value));
                    }
                }
                else
                {
                    Response.AppendCookie(new HttpCookie("fileDownloadToken", hiddenDownloadFlag.Value));
                }
                Response.AddHeader("Content-Disposition", "attachment; filename=" + reportName + ".xls");
                Response.AddHeader("Content-Length", byteArray.Length.ToString());
                Response.ContentType = "application/vnd.msexcel";
                Response.BinaryWrite(byteArray);
                Response.Flush();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                Master.StatusBox.showStatusMessage("Export report error.");
            }
        }
    }
}