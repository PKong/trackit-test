<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="Add Issue | TrackiT2" Language="C#" MasterPageFile="~/Templates/Modal.Master" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="TrackIT2.IssueTracker.AddIssueTrackerItem" %>
<%@ MasterType TypeName="TrackIT2.Templates.Modal" %>
<%@ Register TagPrefix="TrackIT2" TagName="TextFieldAutoComplete" Src="~/Controls/TextFieldAutoComplete.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">   
   <!-- Styles -->
   <%: Styles.Render("~/Styles/issue_tracker_add") %>
    
   <!-- Scripts -->
   <%: Scripts.Render("~/Scripts/issue_tracker_add") %>
    
    <script type="text/javascript">
        // Doc ready!
        $().ready(function () {
            parent.$('#modalIframeId').height(parent.$('#modalIframeId').contents().find('.modal-page-wrapper').outerHeight(true) + 30);

            // Setup Upload Document Modal
            setupDmsModal("Upload Document");
        });
        //-->
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <div id="message_alert"></div>

    <asp:Panel ID="pnlCreateIssueTrackerItemContainer" CssClass="create-container form_element" runat="server" style="width:710px;margin: 0 auto 0 auto;">
            
            <div class="left-col form_element">
            
                <div id="divSite" runat="server">
                    <h4>Site ID</h4>
                    <label>Select Site</label>
                    <TrackIT2:TextFieldAutoComplete
                        ID="ctrSiteID"
                        LabelFieldClientID="lblSiteID" 
                        LabelFieldValue=""
                        LabelFieldCssClass="label" 
                        TextFieldClientID="txtSiteID" 
                        TextFieldCssClass="input-autocomplete"
                        TextFieldWidth="190" 
                        TextFieldValue="" 
                        ScriptKeyName="ScriptAutoCompleteSiteID"
                        DataSourceUrl="SiteIdAutoComplete.axd" 
                        ClientIDMode="Static" runat="server" />
                    <br />
                    
                    <p style="float: left; width: 90%; padding: 5px 5px 0; text-align: left; font-style: italic; font-size: 12px; font-style:italic; margin-bottom:20px;color:#666;">
                    Note: Site ID, Name, Address and Structure Details will show up as a result of selecting the Site.
                    </p>
                    
                    <br />

                    <h4>Priority & Status</h4>
                    <label>Priority</label>
                    <asp:DropDownList ID="ddlPriority" ClientIDMode="Static" CssClass="select-field" runat="server">
                        <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                    </asp:DropDownList>
		            <br />
                    
                    <label>Status</label>
                    <asp:DropDownList ID="ddlStatus" ClientIDMode="Static" CssClass="select-field" runat="server">
                        <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                    </asp:DropDownList>
		            <br />
                </div>
        
            </div>
        
            <div class="right-col form_element">
                
                <h4>Team & Tenants</h4>
                <label ID="lblCurrentSpecialist" runat="server">Current Specialist</label>
                <asp:DropDownList ID="ddlCurrentSpecialist" runat="server" CssClass="select-field" />
                <br />

                <label ID="lblProjectManager" runat="server">Project Manager</label>
                <asp:DropDownList ID="ddlProjectManager" runat="server" CssClass="select-field" />
                <br />
            
                <label style="display: inline-block; height: 80px; vertical-align:top;">Tenants</label>
                <asp:ListBox ID="lsbCustomer"
                                ClientIDMode="Static"
                                SelectionMode="Multiple"
                                CssClass="standard-multi"
                                Rows="7"
                                runat="server">
                    <asp:ListItem Text="- None -" Value=""></asp:ListItem>
                </asp:ListBox>
                <br />

            </div>

        <div class="cb"></div>
        
        <div class="form_element" style="padding: 0 10px 0 0;">
            <h4>Issue</h4>
            <div style="padding: 0 18px 0 5px;">
                <label style="width: auto;">Enter an Issue Summary</label>
                <asp:TextBox ID="txtIssueSummary"
                        ClientIDMode="Static"
                        TextMode="MultiLine"
                        Rows="7"
                        CssClass="select-field standard-multi"
                        style="border: 1px solid #E1E1E1;padding: 5px;"
                        runat="server" />
            </div>
            <br />
            
            <div class="document-link" id="Issue" >
                <asp:HiddenField  ID="hidden_document_Issue" ClientIDMode="Static" runat="server" />
            </div>
            <asp:HiddenField ID="hidDocumentDownload" runat="server" ClientIDMode="Static" /> 
            <div id="modalDocument" class="modal-container" style="overflow:hidden;"></div>
            <asp:Button ID="btnSubmitCreateIssue" runat="server"
                        CssClass="buttonSubmit ui-button ui-widget ui-state-default ui-corner-all"
                        Text="Create Issue"
                        OnClick="BtnSubmitCreateIssueClick"
                        OnClientClick="if (!validateCreateIssue()) {return false;}"
                        UseSubmitBehavior="false" />
        </div>
        
        <div class="cb"></div>

    </asp:Panel>
    <div id="modalExternalDms" class="modal-container" style="overflow:hidden;"></div>
    <asp:HiddenField id="hidBasePath" ClientIDMode="Static" runat="server"/>
</asp:Content>
