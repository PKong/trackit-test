﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.BLL;

namespace TrackIT2.Controls
{
   public partial class StatusBox : System.Web.UI.UserControl
   {
      protected void Page_Load(object sender, EventArgs e)
      {

      }

      public void show()
      {
         Page.ClientScript.RegisterStartupScript
                           (this.GetType(), "showDialog",
                           "$(function() {$('#statusBox').dialog();});", true);
      }

      public void hide()
      {
         Page.ClientScript.RegisterStartupScript
                           (this.GetType(), "showDialog",
                           "$(function() {$('#statusBox').hide();});", true);
      }

      public void showErrorMessage(string message)
      {
         lblStatus.Text = message;
         lblStatus.CssClass = "errorMessage";

         Page.ClientScript.RegisterStartupScript
                           (this.GetType(), "showDialog",
                           "$(function() {$('#statusBox').dialog();});", true);
      }

      public void showErrorMessageWithRedirect(string message, string url)
      {
         lblStatus.Text = message;
         lblStatus.CssClass = "errorMessage";

         Page.ClientScript.RegisterStartupScript
                           (this.GetType(),
                            "showDialog",
                            "$(function() {$('#statusBox').dialog({" +
                                                          " modal: true, " +
                                                          " close: function(event, ui){window.parent.location.href='" + url + "';}" +
                                                          "});});",
                            true);
      }

      public void showStatusMessage(string message)
      {
          lblStatus.Text = message;
          lblStatus.CssClass = "statusMessage";

          if (Utility.GetSubmitValidator().HasValue)
          {
              if (Utility.GetSubmitValidator().Value)
              {
                  Page.ClientScript.RegisterStartupScript
                                    (this.GetType(), "showDialog",
                                    "$(function() {$('#statusBox').dialog();});", true);
                  Utility.ClearSubmitValidator();
              }
              else
              {
                  showErrorMessage("Error occur on submitting data. Only valid data will update.");
                  Utility.ClearSubmitValidator();
              }
          }
          else
          {
              //Didn't submit data to db.
              Page.ClientScript.RegisterStartupScript
                                 (this.GetType(), "showDialog",
                                 "$(function() {$('#statusBox').dialog();});", true);
          }
         
      }

      public void showStatusMessageWithRedirect(string message, string url)
      {
         lblStatus.Text = message;
         lblStatus.CssClass = "statusMessage";

         if (Utility.GetSubmitValidator().HasValue)
         {
            if (Utility.GetSubmitValidator().Value)
            {
               Page.ClientScript.RegisterStartupScript
                                 (this.GetType(), 
                                  "showDialog",
                                  "$(function() {$('#statusBox').dialog({" +
                                                                  " modal: true, " +
                                                                  " close: function(event, ui){window.parent.location.href='" + url + "';}" +
                                                                  "});});", 
                                  true);

               Utility.ClearSubmitValidator();
            }
            else
            {
               showErrorMessage("Error occur on submitting data. Only valid data will update.");
               Utility.ClearSubmitValidator();
            }
         }
         else
         {
            //Didn't submit data to db.
            Page.ClientScript.RegisterStartupScript
                              (this.GetType(),
                               "showDialog",
                               "$(function() {$('#statusBox').dialog({" +
                                                               " modal: true, " +
                                                               " close: function(event, ui){window.parent.location.href='" + url + "';}" +
                                                               "});});",
                               true);
         }
      }
   }
}