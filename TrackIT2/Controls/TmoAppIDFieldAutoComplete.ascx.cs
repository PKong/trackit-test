﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.BLL;
using System.Text;

namespace TrackIT2.Controls
{
    /// <summary>
    /// TextFieldAutoComplete
    /// </summary>
    public partial class TmoAppIDFieldAutoComplete : UserControl
    {
        // Private Var
        private string[] _arrayjQueryAutocompleteArgs;
        private readonly string[] _arrayjQueryAutocompleteArgsDefaults = new[]
                                                                    {
                                                                        "select: function(event, ui) { try { $(event.target).val(ui.item.value.slice(0, ui.item.value.indexOf(' '))); return false; }catch(e){} }",
                                                                        "select: function(event, ui) { try { $(event.target).val(ui.item.value); return false; }catch(e){} }"
                                                                    };
        //>

        // Properties
        public string LabelFieldClientID = "lblAutoComplete";
        public string LabelFieldValue = "Auto-complete";
        public string LabelFieldCssClass = "label";
        //
        public string TextFieldClientID = "txtAutoComplete";
        public int TextFieldWidth = 100;
        public string TextFieldCssClass = "input";
        //
        public string DataSourceUrl;
        public string TargetTextField;
        public string ScriptKeyName = "ScriptAutoComplete";
        //
        public Boolean IsWholeTextUsage = false;
        private string _jQueryAutoCompleteScript;
        public string JQueryAutoCompleteScript
        {
            // Get
            get
            {
                if (string.IsNullOrEmpty(_jQueryAutoCompleteScript))
                {
                    return !IsWholeTextUsage ? _arrayjQueryAutocompleteArgsDefaults[0] : _arrayjQueryAutocompleteArgsDefaults[1];
                }
                return _jQueryAutoCompleteScript;
            }
            // Set
            set { _jQueryAutoCompleteScript = value; }
        }
        //>

        /// <summary>
        /// Page Init
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Init(object sender, EventArgs e)
        {
            CreateAndBindControls();
        }
        //-->

        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //TODO: add!
        }
        //-->

        /// <summary>
        /// Create and Bind Controls
        /// </summary>
        protected void CreateAndBindControls()
        {
            // Label
            var lblAutoComplete = new Label { ID = LabelFieldClientID, Text = LabelFieldValue, CssClass = LabelFieldCssClass };
            Controls.Add(lblAutoComplete);

            // TextBox
            var txtAutoComplete = new TextBox { ID = TextFieldClientID, CssClass = TextFieldCssClass, Width = TextFieldWidth };
            Controls.Add(txtAutoComplete);

            // Build jQuery Script
            var sbJs = new StringBuilder();

            sbJs.Append("$(function () { $('#" + TextFieldClientID + "').autocomplete({source:  function(request, response) {$.ajax({url: \""+DataSourceUrl+"\",dataType: \"json\",data: {term : request.term,selectSite : $('#"+TargetTextField+"').val()},success: function(data) {response(data);}});}");

            if (!string.IsNullOrEmpty(JQueryAutoCompleteScript))
            {
                _arrayjQueryAutocompleteArgs = JQueryAutoCompleteScript.Split('|');
                foreach (string t in _arrayjQueryAutocompleteArgs)
                {
                    sbJs.Append(", " + t);
                }
            }

            sbJs.Append("}); });");

            // Register jQuery Script
            PageUtility.RegisterClientsideStartupScript(Page, ScriptKeyName, sbJs.ToString());
        }
        //-->

        public void OnTextChange(object sender, EventArgs e)
        {

        }
    }
}