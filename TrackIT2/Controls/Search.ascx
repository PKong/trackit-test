﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Search.ascx.cs" Inherits="TrackIT2.Controls.Search" %>
   <!-- Global Search Control -->

   <!-- Scripts -->
   <%: Scripts.Render("~/Scripts/search_control") %>

                        <script type="text/javascript" language="javascript">
                            // Doc Ready
                            $(function () {
                                // Setup Global Search
                                setupGlobalSearch();
                            });
                            //-->
                        </script>
                        <div class="search-container">
                            <input id="txtGlobalSearch" type="text" value="Search" style="padding:0 0 0 6px;width:196px;height:22px;margin:0;border:0px none;font-size:12px;margin:0;" />
                            <img id="imgClearSearch" src="/images/icons/Erase-Greyscale.png" width="12" alt="Clear search criterea?" title="Clear search criterea?" style="display:none;" />
                        </div>
                        <div class="search-box" style="padding-top: 60px; font-size: 10pt;">
                            <div style="float: left; text-align: left; padding: 0 0 0 20px; width: 117px">
                                <input type="radio" name="radGlobalSearchType" value="LeaseApp" checked="checked" />
                                <label id="lblLeaseApp">Lease App</label><br />
                                <input type="radio" name="radGlobalSearchType" value="Site" />
                                <label id="lblSite">Site</label><br />
                            </div>
                            <div style="float: left; text-align: left; padding: 0 20px 0 0; width: 117px;">
                                <input type="radio" name="radGlobalSearchType" value="TowerMod" />
                                <label id="lblTowerMod">Tower Mod</label><br />
                                <input type="radio" name="radGlobalSearchType" value="SDP" />
                                <label id="lblSdp">SDP</label><br />
                            </div>
                            <div style="margin-left: 20px; text-align: left; margin-top: 20px; float: left;">
                                <input id="btnGlobalSearch" type="submit" value="Search" />
                            </div>
                        </div>
                        <!-- /Global Search Control -->
