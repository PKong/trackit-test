﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using TrackIT2.DAL;

namespace TrackIT2.Controls
{
    public partial class TopNav : System.Web.UI.UserControl
    {
        public event StatusChangedEventHandler ImpersonationChanged;
        public delegate void StatusChangedEventHandler(String Role);

        protected void Page_Load(object sender, EventArgs e)
        {
            HyperLink hypImportApplication = ((HyperLink)HeadLoginView.FindControl("hypImportApplication"));
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                HyperLink hypAdminLink = ((HyperLink)HeadLoginView.FindControl("hypAdmin"));
                HyperLink hypTowerListLink = ((HyperLink)HeadLoginView.FindControl("hypTowerList"));
                Label lblDisplayName = ((Label)HeadLoginView.FindControl("lblDisplayName"));
                Label lblAdminSpacer = ((Label)HeadLoginView.FindControl("lblAdminSpacer"));
                Label lblSeparater = ((Label)HeadLoginView.FindControl("lblSeparater"));
                Label lblTowerListSpacer = ((Label)HeadLoginView.FindControl("lblTowerListSpacer"));

                if (Session["userDisplayName"] != null)
                {
                    lblDisplayName.Text = Session["userDisplayName"].ToString();
                }

                if (HttpContext.Current.User.IsInRole("Administrator"))
                {
                    hypAdminLink.Visible = true;
                    lblAdminSpacer.Visible = true;
                    lblImpersonateRole.Visible = true;
                    ddlImpersonateRole.Visible = true;
                    hypImportApplication.Visible = true;
                    lblSeparater.Visible = true;
                    hypTowerListLink.Visible = true;
                    lblTowerListSpacer.Visible = true;

                    if (!Page.IsPostBack)
                    {
                        BindRoles();
                    }
                }
                else if (HttpContext.Current.User.IsInRole("Application Import"))
                {
                    hypAdminLink.Visible = false;
                    lblAdminSpacer.Visible = false;
                    lblImpersonateRole.Visible = false;
                    ddlImpersonateRole.Visible = false;
                    hypImportApplication.Visible = true;
                    lblSeparater.Visible = true;
                }
                else if (HttpContext.Current.User.IsInRole("Tower List"))
                {
                    hypAdminLink.Visible = false;
                    lblAdminSpacer.Visible = false;
                    lblImpersonateRole.Visible = false;
                    ddlImpersonateRole.Visible = false;
                    hypImportApplication.Visible = false;
                    lblSeparater.Visible = false;
                    hypTowerListLink.Visible = true;
                    lblTowerListSpacer.Visible = true;

                }
                else
                {
                    hypAdminLink.Visible = false;
                    lblAdminSpacer.Visible = false;
                    lblImpersonateRole.Visible = false;
                    ddlImpersonateRole.Visible = false;
                    hypImportApplication.Visible = false;
                    lblSeparater.Visible = false;
                }
            }
            else
            {
                lblImpersonateRole.Visible = false;
                ddlImpersonateRole.Visible = false;
                hypImportApplication.Visible = false;
            }
        }

        protected void BindRoles()
        {
            List<string> userRoles = new List<string>(Roles.GetAllRoles());

            ddlImpersonateRole.DataSource = userRoles;
            ddlImpersonateRole.DataBind();

            if (Session["ImpersonatedRole"] != null)
            {
                string role = Session["ImpersonatedRole"].ToString();
                ddlImpersonateRole.ClearSelection();
                ddlImpersonateRole.Items.FindByText(role).Selected = true;
            }
        }

        protected void ddlImpersonateRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            string newRole;

            HyperLink ImportApplication = ((HyperLink)HeadLoginView.FindControl("hypImportApplication"));
            Label Separater = ((Label)HeadLoginView.FindControl("lblSeparater"));

            HyperLink hypTowerListLink = ((HyperLink)HeadLoginView.FindControl("hypTowerList"));
            Label lblTowerListSpacer = ((Label)HeadLoginView.FindControl("lblTowerListSpacer"));

            ImportApplication.Visible = false;
            Separater.Visible = false;
            hypTowerListLink.Visible = false;
            lblTowerListSpacer.Visible = false;
            if (ddlImpersonateRole.SelectedValue == "None")
            {
                Session["ImpersonatedRole"] = null;
                newRole = "None";
                if (HttpContext.Current.User.IsInRole("Application Import"))
                {
                    ImportApplication.Visible = true;
                    Separater.Visible = true;
                }
                else if (HttpContext.Current.User.IsInRole("Tower List"))
                {
                    hypTowerListLink.Visible = true;
                    lblTowerListSpacer.Visible = true;
                }
                else if (HttpContext.Current.User.IsInRole("Administrator"))
                {
                    ImportApplication.Visible = true;
                    Separater.Visible = true;
                    hypTowerListLink.Visible = true;
                    lblTowerListSpacer.Visible = true;
                }
            }
            else
            {
                Session["ImpersonatedRole"] = ddlImpersonateRole.SelectedValue;
                newRole = ddlImpersonateRole.SelectedValue;
                if ((newRole == "Application Import"))
                {
                    ImportApplication.Visible = true;
                    Separater.Visible = true;
                }
                else if (newRole == "Tower List")
                {
                    hypTowerListLink.Visible = true;
                    lblTowerListSpacer.Visible = true;
                }
                else if (newRole == "Administrator")
                {
                    ImportApplication.Visible = true;
                    Separater.Visible = true;
                    hypTowerListLink.Visible = true;
                    lblTowerListSpacer.Visible = true;
                }
            }

            // If no event handler has been registered on the current page, 
            // don't raise the event.
            if (ImpersonationChanged != null)
            {
                ImpersonationChanged(newRole);
            }
        }
    }
}