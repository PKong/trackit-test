﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Collections.Specialized;
using System.ComponentModel;

namespace TrackIT2.BLL
{
    /// <summary>
    /// Page Utility
    /// </summary>
    public static class PageUtility
    {
        // Var
        private const string STARTOF_SCRIPT = "<script type=text/javascript>";
        private const string ENDOF_SCRIPT = "</script>";
        //>

        #region Search Result Filters

            /// <summary>
            /// Search Filter Control Types
            /// </summary>
            public struct SearchFilterControlTypes
            {
                public struct TextBoxControl
                {
                    public const String PREFIX = "txt";
                }

                public struct DropDownListControl
                {
                    public const String PREFIX = "ddl";
                }

                public struct ListBoxControl
                {
                    public const String PREFIX = "lsb";
                }
            }
            //-->

            /// <summary>
            /// Setup Search Result Filters
            /// </summary>
            public static void SetupSearchResultFilters(Page page, Control control)
            {
                try
                {
                    // Get the filter QS
                    var filter = GetTypedValueFromString<String>(page.Request.QueryString, "filter");

                    // Lists for storing filters and processed filters 
                    var listFilters = new List<string>(HttpUtility.UrlDecode(filter).Replace("ddlLeaseAppType","lsbLeaseAppType").Split('|'));
                    var listFiltersProcessed = new List<string>();

                    // Iterate and process
                    for (var i = 0; i < listFilters.Count; i += 2)
                    {
                        // Already processed? If so skip inner processeing loop..
                        if (listFiltersProcessed.Contains(listFilters[i])) continue;

                        // Var
                        String fieldID = listFilters[i];
                        ////This will use for change the ctrl name for the filter in previous version only.
                        String fieldType = fieldID.Substring(0, 3);
                        String fieldValue = listFilters[i + 1];

                        // Control type?
                        switch (fieldType)
                        {
                            // TextBox
                            case SearchFilterControlTypes.TextBoxControl.PREFIX:

                                var textBox = (TextBox)FindControlRecursive(control, fieldID);
                                if (textBox != null)
                                {
                                    textBox.Text = fieldValue;

                                    // Store in Processed List
                                    listFiltersProcessed.Add(fieldID);
                                }
                                break;

                            // DropDownList
                            case SearchFilterControlTypes.DropDownListControl.PREFIX:

                                var dropDownList = (DropDownList)FindControlRecursive(control, fieldID);
                                if (dropDownList != null)
                                {
                                    dropDownList.SelectedValue = fieldValue;

                                    // Store in Processed List
                                    listFiltersProcessed.Add(fieldID);
                                }
                                break;

                            // DropDownList
                            case SearchFilterControlTypes.ListBoxControl.PREFIX:

                                var listBox = (ListBox)FindControlRecursive(control, fieldID);
                                if (listBox != null)
                                {
                                    // Iterate filters and Process matching items
                                    for (var j = 0; j < listFilters.Count; j += 2)
                                    {
                                        // Get current Field and Value
                                        var fld = listFilters[j];
                                        var val = listFilters[j + 1];

                                        // Skip if wrong control ID
                                        if (fld != listBox.ID) continue;

                                        // Search and destroy... er select!
                                        foreach (var item in listBox.Items.Cast<ListItem>().Where(item => item.Value == val))
                                        {
                                            item.Selected = true;
                                        }
                                    }
                                    //>

                                    // Store in Processed List
                                    listFiltersProcessed.Add(fieldID);
                                }
                                break;
                        }//>
                    }
                    //>

                    // Register Client-side Start-up Script
                    RegisterClientsideStartupScript(page, "SetupSearchResultFilters", "applySearchFilter(false);");
                }
                catch { }
            }
            //-->

        #endregion

        #region Controls

            /// <summary>
            /// Find Control Recursive
            /// </summary>
            /// <param name="container">Control container is the starting point for the search.</param>
            /// <param name="name">Name / ID of the Control you are trying to find.</param>
            /// <returns></returns>
            public static Control FindControlRecursive(Control container, string name)
            {
                if ((container.ID != null) && (container.ID.Equals(name)))
                    return container;

                foreach (Control ctrl in container.Controls)
                {
                    Control foundCtrl = FindControlRecursive(ctrl, name);
                    if (foundCtrl != null)
                        return foundCtrl;
                }
                return null;
            }

        #endregion

        #region Scripts

            /// <summary>
            /// Register Client-side Start-up Script
            /// </summary>
            /// <param name="page">The Page object to register the script.</param>
            /// <param name="key">String key that identifies the script.</param>
            /// <param name="script">The JavaScript to register as a String.</param>
            public static void RegisterClientsideStartupScript(Page page, String key, String script)
            {
                page.ClientScript.RegisterStartupScript(page.GetType(), key, (STARTOF_SCRIPT + script + ENDOF_SCRIPT));
            }

            /// <summary>
            /// Register Client-side Script Block
            /// </summary>
            /// <param name="page">The Page object to register the script.</param>
            /// <param name="key">String key that identifies the script.</param>
            /// <param name="script">The JavaScript to register as a String.</param>
            public static void RegisterClientsideScriptBlock(Page page, String key, String script)
            {
                page.ClientScript.RegisterClientScriptBlock(page.GetType(), key, (STARTOF_SCRIPT + script + ENDOF_SCRIPT));
            }

        #endregion

        #region String / Util

            /// <summary>
            /// Get Typed Value From String
            /// </summary>
            /// <typeparam name="T"></typeparam>
            /// <param name="collection"></param>
            /// <param name="key"></param>
            /// <returns></returns>
            public static T GetTypedValueFromString<T>(this NameValueCollection collection, string key)
            {
                if (collection == null)
                {
                    throw new ArgumentNullException("collection");
                }

                var value = collection[key];

                if (value == null)
                {
                    throw new ArgumentOutOfRangeException("key");
                }

                var converter = TypeDescriptor.GetConverter(typeof(T));

                if (!converter.CanConvertFrom(typeof(string)))
                {
                    throw new ArgumentException(String.Format("Cannot convert '{0}' to {1}", value, typeof(string)));
                }

                return (T)converter.ConvertFrom(value);
            }

        #endregion
    
    }
}