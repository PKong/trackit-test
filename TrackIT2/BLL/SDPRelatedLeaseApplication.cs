﻿using System;
using TrackIT2.DAL;

namespace TrackIT2.BLL
{
   public class SDPRelatedLeaseApplication
   {
      public const string PATH_LA_EDIT_PAGE = @"../LeaseApplications/Edit.aspx?id=";      
      public string SiteID { get; set; }
      public customer CustomerInfo { get; set; }
      public DateTime? LAReceived { get; set; }
      public int LAID { get; set; }
      public string CustomerName
      {
         get
         {
            string ret = null;
            if (CustomerInfo != null)
            {
               ret = CustomerInfo.customer_name;
            }
            return ret;
         }
      }
      public string Link
      {
         get
         {
            return PATH_LA_EDIT_PAGE + LAID;
         }
      }
   }
}