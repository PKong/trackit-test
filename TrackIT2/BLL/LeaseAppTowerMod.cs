﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TrackIT2.DAL;

namespace TrackIT2.BLL
{
    public static class LeaseAppTowerMod
    {
        // Returns a list of lease application records corresponding
        // to the Tower Mod id specified.
        public static List<lease_applications> GetLeaseAppTM(int? towerModID)
        {
           List<lease_applications> listRet = new List<lease_applications>();
            if (towerModID.HasValue)
            {
                using (CPTTEntities context = new CPTTEntities())
                {
                   var query = from towermod in context.tower_modifications
                               from leaseapp in towermod.lease_applications
                               where towermod.id == towerModID.Value
                               select leaseapp;

                   listRet.AddRange(query.ToList<lease_applications>());
                }
            }
            return listRet;
        }

        // Returns a list of tower modification records that corresponds to
        // the lease application id specified.
        public static List<tower_modifications> GetTowerModLA(int? leaseAppID)
        {
           List<tower_modifications> listRet = new List<tower_modifications>();
           
           if (leaseAppID.HasValue)
           {
              using (CPTTEntities context = new CPTTEntities())
              {
                 var query = from leaseapp in context.lease_applications
                             from towermod in leaseapp.tower_modifications
                             where leaseapp.id == leaseAppID.Value
                             select towermod;

                 listRet.AddRange(query.ToList<tower_modifications>());
              }
           }

           return listRet;
        }

        public static void Add(int laID, int tmID)
        {
            try
            {
                using (CPTTEntities ce = new CPTTEntities())
                {
                   lease_applications leaseapp = ce.lease_applications
                                                   .Where(la => la.id.Equals(laID))
                                                   .First();
                   
                   tower_modifications towermod = ce.tower_modifications
                                                    .Where(tm => tm.id.Equals(tmID))
                                                    .First();                   

                   towermod.lease_applications.Add(leaseapp);

                   ce.AttachUpdated(towermod);
                   ce.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }           
        }

        public static string AddWithinTransaction(int laID, int tmID, CPTTEntities ceRef)
        {
           string results = null;

           try
           {
              lease_applications leaseapp = ceRef.lease_applications
                                              .Where(la => la.id.Equals(laID))
                                              .First();

              tower_modifications towermod = ceRef.tower_modifications
                                                  .Where(tm => tm.id.Equals(tmID))
                                                  .First();

              towermod.lease_applications.Add(leaseapp);

              ceRef.AttachUpdated(towermod);             
           }
           catch (Exception ex)
           {
              // Log error in ELMAH for any diagnostic needs.
              Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
              results = "An error occurred adding lease application / tower mod reference.";
           }

           return results;
        }

        public static int DeleteAllByTowerMod(int? towerModID)
        {
            int ret = 0;
            if (towerModID.HasValue)
            {
                using (CPTTEntities ce = new CPTTEntities())
                {
                    try
                    {
                       var towerMod = ce.tower_modifications.First(tm => tm.id == towerModID.Value);
                       towerMod.lease_applications.Clear();
                       ce.SaveChanges();
                       ret = 1;
                    }
                    catch (Exception ex)
                    {
                       Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                       throw ex;
                    }
                }
            }
            return ret;
        }

        public static string DeleteAllByTowerModWithinTransaction(int? towerModID, CPTTEntities ceRef)
        {
           string results = null;

           if (towerModID.HasValue)
           {
              try
              {
                 var towerMod = ceRef.tower_modifications.First(tm => tm.id == towerModID.Value);
                 towerMod.lease_applications.Clear();
              }
              catch (Exception ex)
              {
                 // Log error in ELMAH for any diagnostic needs.
                 Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                 results = "An error occurred removing tower mod records.";
              }
           }

           return results;
        }

        public static int DeleteAllByLeaseApp(int? leaseAppID)
        {
           int ret = 0;
           if (leaseAppID.HasValue)
           {
              using (CPTTEntities ce = new CPTTEntities())
              {
                 try
                 {
                    var leaseApp = ce.lease_applications.First(la => la.id == leaseAppID.Value);
                    leaseApp.tower_modifications.Clear();
                    ce.SaveChanges();
                    ret = 1;
                 }
                 catch (Exception ex)
                 {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    throw ex;
                 }
              }
           }
           return ret;
        }

        public static string DeleteAllByLeaseAppWithinTransaction(int? leaseAppID, CPTTEntities ceRef)
        {
           string results = null;

           if (leaseAppID.HasValue)
           {
              try
              {
                 var leaseApp = ceRef.lease_applications.First(la => la.id == leaseAppID.Value);
                 leaseApp.tower_modifications.Clear();
              }
              catch (Exception ex)
              {
                 // Log error in ELMAH for any diagnostic needs.
                 Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                 results = "An error occurred removing tower mod records.";
              }
           }

           return results;
        }

        public static List<LeaseAppTowerModInfo> GetRalatedLeaseApp(int TWMod_id)
        {
            List<LeaseAppTowerModInfo> lstRet = new List<LeaseAppTowerModInfo>();
            using (CPTTEntities ce = new CPTTEntities())
            {
               var result = from towermod in ce.tower_modifications
                           from leaseapp in towermod.lease_applications
                           where towermod.id == TWMod_id
                             select new LeaseAppTowerModInfo
                             {
                                 SiteID = leaseapp.site_uid,
                                 CustomerInfo = leaseapp.customer,
                                 LAReceived = leaseapp.leaseapp_rcvd_date,
                                 LAID = leaseapp.id
                             };
                if (result.ToList().Count > 0)
                {
                    lstRet.AddRange(result.ToList<LeaseAppTowerModInfo>());
                }
            }
            return lstRet;
        }

        //Add function get relate leaseapp and towermod by customer id by Kantorn J. 2012-09-96
        public static List<tower_modifications> GetRalatedLeaseApp(int[] arrCustomerId)
        {
            List<tower_modifications> lstRet = new List<tower_modifications>();
                using (CPTTEntities ce = new CPTTEntities())
                {
                    var result = from leaseapp in ce.lease_applications
                                   from towemod in leaseapp.tower_modifications
                                   where arrCustomerId.Contains((int)leaseapp.customer_id)
                                   select towemod;
                    if (result.ToList().Count > 0)
                    {
                        lstRet.AddRange(result.ToList<tower_modifications>());
                    }
                }
                return lstRet;
        }
    }

    public class LeaseAppTowerModInfo
    {
        public const string PATH_LA_EDIT_PAGE = @"../LeaseApplications/Edit.aspx?id=" ;
        public LeaseAppTowerModInfo()
        {
 
        }
        public string SiteID { get; set; }
        public customer CustomerInfo { get; set; }
        public DateTime? LAReceived { get; set; }
        public int LAID { get; set; }

        public string CustomerName {
            get
            {
                string ret = null;
                if (CustomerInfo != null)
                {
                    ret = CustomerInfo.customer_name.ToString();
                }
                return ret;
            }
        }
        public string Link
        {
            get
            {
                return PATH_LA_EDIT_PAGE + LAID.ToString();
            }
        }
    }
}