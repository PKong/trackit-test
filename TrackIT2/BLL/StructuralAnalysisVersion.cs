﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using TrackIT2.DAL;

namespace TrackIT2.BLL
{
   public class StructuralAnalysisVersion
   {
      public static void Add(structural_analysis_versions version)
      {
         using (CPTTEntities ce = new CPTTEntities())
         {
            ce.structural_analysis_versions.AddObject(version);
            ce.SaveChanges();
         }
      }

      public static void Delete(int analysisId, int lockVersion)
      {
         using (CPTTEntities ce = new CPTTEntities())
         {
            structural_analysis_versions version;

            version = ce.structural_analysis_versions
                        .Where(ver => ver.structural_analysis_id.Equals(analysisId))
                        .Where(ver => ver.lock_version.Equals(lockVersion))
                        .First();

            ce.DeleteObject(version);
            ce.SaveChanges();
         }
      }

      public static void Delete(int id)
      {
         using (CPTTEntities ce = new CPTTEntities())
         {
            structural_analysis_versions version;

            version = ce.structural_analysis_versions
                        .Where(ver => ver.id.Equals(id))
                        .First();

            ce.DeleteObject(version);
            ce.SaveChanges();
         }
      }

      public static List<VersionLog> GenerateVersionLog(int id, int groupId)
      {
         List<VersionLog> results = new List<VersionLog>();
         List<structural_analysis_versions> versions;
         List<user> userList;
         structural_analyses currAnalysis;

         using (CPTTEntities ce = new CPTTEntities())
         {
            versions = ce.structural_analysis_versions
                      .Where(ver => ver.structural_analysis_id.Equals(id))
                      .OrderBy(Utility.GetIntField<structural_analysis_versions>("id"))
                      .ToList();

            userList = ce.users.ToList();

            if (versions.Count > 0)
            {
               // If any history records were found, add the current version of the 
               // structural analysis to properly track differences up to the most 
               // recent copy.
               currAnalysis = ce.structural_analyses
                                .Where(la => la.id.Equals(id))
                                .First();

               versions.Add(StructuralAnalysis.ToStructuralAnalysisVersion(currAnalysis));

               for (int i = 1; i < versions.Count; i++)
               {
                  structural_analysis_versions currVersion = versions.ElementAt(i);
                  structural_analysis_versions prevVersion = versions.ElementAt(i - 1);
                  user userInfo;

                  // Create a "point of creation" record to help establish the timeline of
                  // changes.
                  if (i == 1)
                  {
                     VersionLog initialLog = new VersionLog();
                     initialLog.groupId = groupId;
                     initialLog.oldValue = "-";
                     initialLog.newValue = "-";
                     initialLog.fieldChanged = "Record Created";

                     if (prevVersion.created_at.HasValue)
                     {
                        initialLog.updatedAt = prevVersion.created_at.Value;
                     }

                     var initialQuery = from ul in userList
                                        where ul.id == prevVersion.creator_id
                                        select ul;

                     userInfo = initialQuery.FirstOrDefault();

                     if (userInfo != null)
                     {
                        initialLog.updatedBy = userInfo.last_name + ", " +
                                               userInfo.first_name;
                        initialLog.UserAvatarImageUrl = BLL.User.GetUserAvatarImage(userInfo.tmo_userid);
                     }
                     else
                     {
                        initialLog.updatedBy = "";
                        initialLog.UserAvatarImageUrl = BLL.User.GetUserAvatarImage("");
                     }

                     results.Add(initialLog);
                  }

                  // Use reflection to get all modifiyable properties
                  var properties = from p in typeof(structural_analysis_versions).GetProperties()
                                   where p.CanRead &&
                                         p.CanWrite &&
                                         p.Name != "id" &&
                                         p.Name != "created_at" &&
                                         p.Name != "updated_at" &&
                                         p.Name != "updater_id" &&
                                         p.Name != "creator_id" &&
                                         p.Name != "lock_version" &&
                                         p.Name != "EntityKey"
                                   select p;

                  foreach (var property in properties)
                  {
                     // Ignore any values where the field changed ends in "_id" since
                     // the entity associations handle the foreign key lookups. There
                     // are a couple of columns that are exceptions to the rule.
                     if (!property.Name.EndsWith("_id") && !property.Name.EndsWith("Reference") &&
                         property.Name != "creator_user" && property.Name != "updater_user")
                     {
                        var currValue = property.GetValue(currVersion, null);
                        var prevValue = property.GetValue(prevVersion, null);

                        if (HasValueChanges(currValue, prevValue))
                        {
                           // Compare values based on object type.
                           VersionLog log = new VersionLog();

                           log.version = currVersion.lock_version;

                           if (currVersion.updater_id != null)
                           {
                              var query = from ul in userList
                                          where ul.id == currVersion.updater_id
                                          select ul;

                              userInfo = query.FirstOrDefault();

                              if (userInfo != null)
                              {
                                 log.updatedBy = userInfo.last_name + ", " +
                                                 userInfo.first_name;
                                 log.UserAvatarImageUrl = BLL.User.GetUserAvatarImage(userInfo.tmo_userid);
                              }
                              else
                              {
                                 log.updatedBy = "None";
                                 log.UserAvatarImageUrl = BLL.User.GetUserAvatarImage("");
                              }

                           }

                           if (currVersion.updated_at != null)
                           {
                              log.updatedAt = currVersion.updated_at.Value;
                           }

                           log.oldValue = GetFieldValue(prevValue);

                           // Get friendly name for column
                           string tempField = "";
                           List<metadata> columnInfo = ce.metadatas
                                                         .Where(md => md.table.Equals("structural_analyses"))
                                                         .Where(md => md.column.Equals(property.Name))
                                                         .ToList();
                           if (columnInfo.Count == 1)
                           {
                              tempField = columnInfo[0].friendly_name;
                           }
                           else if (columnInfo.Count > 1)
                           {
                              foreach (metadata currItem in columnInfo)
                              {
                                 if (!String.IsNullOrEmpty(currItem.friendly_name))
                                 {
                                    tempField = currItem.friendly_name;
                                    break;
                                 }
                                 else
                                 {
                                    tempField = "No friendly name: " + property.Name;
                                 }
                              }
                           }
                           else
                           {
                              tempField = property.Name;
                           }

                           // If the current value is null, use the previous value in
                           // order to determine the field changed friendly name.
                           if (currValue != null)
                           {
                               log.fieldChanged = GetFieldChangedName(currValue, tempField);
                           }
                           else
                           {
                               log.fieldChanged = GetFieldChangedName(prevValue, tempField);
                           }

                           log.newValue = GetFieldValue(currValue);
                           log.groupId = groupId;

                           results.Add(log);
                        }
                     }
                  }
               }
            }
         }

         return results;
      }

      public static List<VersionLog> GenerateVersionLogByLeaseApplication(int applicationId)
      {
         List<VersionLog> results = new List<VersionLog>();
         List<VersionLog> recordVersion = new List<VersionLog>();
         List<structural_analyses> saRecords;

         using (CPTTEntities ce = new CPTTEntities())
         {
            // A lease application may have more than one structural analysis 
            // record assigned to it. Each SA record needs to have the full
            // history pulled for it. The SA record id is used as a grouping 
            // value in the version log to allow end users to identify which
            // SA record was modified.
            saRecords = ce.structural_analyses
                          .Where(ver => ver.lease_application_id.Equals(applicationId))
                          .ToList();

            foreach (structural_analyses currRecord in saRecords)
            {
               recordVersion = new List<VersionLog>();
               recordVersion = GenerateVersionLog(currRecord.id, currRecord.id);
               results.AddRange(recordVersion);
            }
         }

         return results;
      }

      private static bool HasValueChanges(object value1, object value2)
      {
         bool results = false;
         object testObject;

         if (value1 == null && value2 == null)
         {
            results = false;
         }
         // If one value is null and the other is not, there is a change.
         else if ((value1 == null && value2 != null) || (value1 != null && value2 == null))
         {
            results = true;
         }
         // Otherwise we do a comparison based off of object type. Simple
         // object types can do a "ToString" comparison, others need to 
         // compare at the object level.
         else
         {
            if (value1 != null)
            {
               testObject = value1;
            }
            else
            {
               testObject = value2;
            }
            switch (testObject.GetType().Name)
            {
               case "String":
               case "Int32":
               case "Boolean":
               case "Float":
               case "Decimal":
               case "Double":

                  if (value1.ToString() != value2.ToString())
                  {
                     results = true;
                  }

                  break;

               case "DateTime":

                  if (((DateTime)value1).Date != ((DateTime)value2).Date)
                  {
                     results = true;
                  }

                  break;

               default:

                  if (value1 != value2)
                  {
                     results = true;
                  }

                  break;
            }
         }

         return results;
      }

      private static string GetFieldValue(object item)
      {
         string results = "";

         if (item != null)
         {
            switch (item.GetType().Name)
            {
               // While the entity model and already retrieved the 
               // foreign key objects, we need to ignore those and
               // only process the ID or primitive types so that we 
               // can properly set the friendly column name.
               case "DateTime":
                  results = ((DateTime)item).ToString("MM/dd/yyyy");
                  break;

               case "insite4_lease_applications":
                  results = ((insite4_lease_applications)item).id.ToString();
                  break;

               case "po_release_statuses":
                  results = ((po_release_statuses)item).po_release_status;
                  break;

               case "sa_vendors":
                  results = ((sa_vendors)item).sa_vendor;
                  break;
              
               case "sac_descriptions":
                  results = ((sac_descriptions)item).description;
                  break;

               case "sac_priorities":
                  results = ((sac_priorities)item).priority;
                  break;

               case "structural_analysis_fee_payor_types":
                  results = ((structural_analysis_fee_payor_types)item).sa_fee_payor_desc;
                  break;

               case "structural_analysis_types":
                  results = ((structural_analysis_types)item).structural_analysis_type;
                  break;

               case "lease_applications":
                  results = ((lease_applications)item).id.ToString();
                  break;

               case "user":
                  results = ((user)item).first_name + " " + ((user)item).last_name;
                  break;

               default:
                  results = item.ToString();
                  break;
            }
         }
         else
         {
            results = "-";
         }

         return results;
      }

      private static string GetFieldChangedName(object item, string columnName)
      {
         string results = "";

         if (item != null)
         {
            switch (item.GetType().Name)
            {
               case "insite4_lease_applications":
                  results = "Insite 4 Lease Application ID";
                  break;

               case "po_release_statuses":
                  results = "PO Release Status";
                  break;

               case "sa_vendors":
                  results = "SA Vendor";
                  break;

               case "sac_descriptions":
                  results = "SAC Description";
                  break;

               case "sac_priorities":
                  results = "SAC Priority";
                  break;

               case "structural_analysis_fee_payor_types":
                  results = "SA Fee Payor Type";
                  break;

               case "structural_analysis_types":
                  results = "SA Type";
                  break;

               case "lease_applications":
                  results = "Lease Application ID";
                  break;

               case "user":

                  // There are multiple references from strcutral analysis version records 
                  // to user records, we set the field changed value based off the name
                  // column name of the association object.
                  switch (columnName)
                  {
                     case "creator_user":
                        results = "Created By User";
                        break;
                     
                     case "sac_name_emp_user":
                        results = "SAC Name Employee User";
                        break;

                     case "updater_user":
                        results = "Updated by User";
                        break;

                     default:
                        results = columnName + " User";
                        break;
                  }

                  break;

               default:
                  results = columnName;
                  break;
            }
         }

         return results;
      }
   }
}