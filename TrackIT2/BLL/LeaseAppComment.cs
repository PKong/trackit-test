﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TrackIT2.DAL;

namespace TrackIT2.BLL
{
    public static class LeaseAppComment
    {
        public static leaseapp_comments Search(int id)
        {
            leaseapp_comments ret = null;

            using (var ce = new CPTTEntities())
            {
                ret = ce.leaseapp_comments.First(c => c.id.Equals(id));
                if (ret != null) ce.Detach(ret);
            }

            return ret;
        }

        public static List<leaseapp_comments> GetComments(int? lease_app_id)
        {
            List<leaseapp_comments> listRet = new List<leaseapp_comments>();
            if (lease_app_id.HasValue)
            {
                using (CPTTEntities context = new CPTTEntities())
                {
                    var query = from sa in context.leaseapp_comments
                                where sa.lease_application_id == lease_app_id.Value
                                select sa;
                    listRet.AddRange(query.ToList<leaseapp_comments>());
                }
            }
            return listRet;
        }

        public static void Add(leaseapp_comments objComment)
        {
            // TODO: Add additional security/business logic in here as needed.
            // TODO: Add additional fields into database insert.
            using (CPTTEntities ce = new CPTTEntities())
            {
                HttpContext context = HttpContext.Current;
                ce.leaseapp_comments.AddObject(objComment);
                ce.SaveChanges();
            }
        }

        public static void Delete(int id)
        {
            using (CPTTEntities ce = new CPTTEntities())
            {

                //remove any relate link document
                var documents = from doc in ce.documents_links
                                where doc.record_id == id
                                select doc;

                if (documents.Count() > 0)
                {
                    foreach (documents_links item in documents.ToList<documents_links>())
                    {
                        ce.DeleteObject(item);
                    }
                }

                var query = from c in ce.leaseapp_comments
                             where c.id == id
                             select c;
                if (query.ToList().Count > 0)
                {
                    leaseapp_comments result = query.ToList()[0];
                    ce.leaseapp_comments.DeleteObject(result);

                    // Change DocLog
                    DocumentsChangeLog.UpdateIsLinkRecordFlag(id, DMSDocumentLinkHelper.eDocumentType.lease_applications.ToString(), false, ce, 
                        DMSDocumentLinkHelper.eDocumentField.LE_Comments.ToString());
                    ce.SaveChanges();
                }
            }
        }
    }
}