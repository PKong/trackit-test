﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TrackIT2.DAL;

namespace TrackIT2.BLL
{
    public static class SACDescriptions
    {
        public static List<SACDescriptionsInfo> GetDescriptions()
        {
            List<SACDescriptionsInfo> lstDesc = new List<SACDescriptionsInfo>();
            using (CPTTEntities ce = new CPTTEntities())
            {
                var result = (from desc in ce.sac_descriptions
                             where desc.is_preferred == true
                             orderby desc.description
                             select new SACDescriptionsInfo
                             {
                                 ID = desc.id,
                                 IsPreferred = desc.is_preferred,
                                 Description = desc.description
                             }).ToList();
                if (result.Count > 0)
                {
                    lstDesc.AddRange(result.ToList<SACDescriptionsInfo>());
                }
            }
            return lstDesc;
        }
        public static List<SACDescriptionsInfo> GetAllDescriptions()
        {
            List<SACDescriptionsInfo> lstDesc = new List<SACDescriptionsInfo>();
            using (CPTTEntities ce = new CPTTEntities())
            {
                var result = (from desc in ce.sac_descriptions
                             orderby desc.description
                             select new SACDescriptionsInfo
                             {
                                 ID = desc.id,
                                 IsPreferred = desc.is_preferred,
                                 Description = desc.description
                             }).ToList();
                if (result.Count > 0)
                {
                    lstDesc.AddRange(result.ToList<SACDescriptionsInfo>());
                }
            }
            return lstDesc;
        }
    }

    public class SACDescriptionsInfo
    {
        public SACDescriptionsInfo()
        {

        }

        public int ID { get; set; }
        public Boolean? IsPreferred { get; set; }
        public string Description { get; set; }
        public string DescriptionName
        {
            get
            {
                string ret = Description;
                if (IsPreferred.HasValue && !(IsPreferred.Value))
                {
                    ret += " (historical)";
                }
                return ret;
            }
        }
    }
}