﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TrackIT2.DAL;

namespace TrackIT2.BLL
{
    public static class LeaseAppSAComment
    {
        public static void Delete(int id)
        {
            using (CPTTEntities ce = new CPTTEntities())
            {
                var query = from c in ce.structural_analysis_comments
                            where c.id == id
                            select c;
                if (query.ToList().Count > 0)
                {
                    structural_analysis_comments result = query.ToList()[0];
                    ce.structural_analysis_comments.DeleteObject(result);

                    // Change DocLog
                    DocumentsChangeLog.UpdateIsLinkRecordFlag(id, DMSDocumentLinkHelper.eDocumentType.structural_analyses.ToString(), false, ce, DMSDocumentLinkHelper.eDocumentField.SA_Comments.ToString());
                    ce.SaveChanges();
                }
                
            }
        }
    }
}