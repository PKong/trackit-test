﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using TrackIT2.DAL;
using TrackIT2.Objects;

namespace TrackIT2.BLL
{
    /// <summary>
    /// Issue Tracker
    /// </summary>
    public static class IssueTracker
    {

        #region Issue Tracker

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="id">int</param>
        /// <returns>issue_tracker</returns>
        public static issue_tracker Search(int id)
        {
            issue_tracker ret;

            using (var ce = new CPTTEntities())
            {
               if (ce.issue_tracker
                     .Where(it => it.id.Equals(id))
                     .Count() > 0)
               {
                  ret = ce.issue_tracker
                          .Where(it => it.id.Equals(id))
                          .First();
               }
               else
               {
                  ret = null;
               }
            }

            return ret;
        }
        //-->

        public static issue_tracker SearchWithinTransaction(int id, CPTTEntities ceRef)
        {
           issue_tracker ret;

           if (ceRef.issue_tracker
                 .Where(it => it.id.Equals(id))
                 .Count() > 0)
           {
              ret = ceRef.issue_tracker
                      .Where(it => it.id.Equals(id))
                      .First();
           }
           else
           {
              ret = null;
           }

           return ret;
        }

        /// <summary>
        /// Add
        /// </summary>
        /// <param name="objIssue">issue_tracker</param>
        /// <returns>string</returns>
        public static string Add(issue_tracker objIssue)
        {
            string sResult = null;
            bool isSuccess = false;

            using (var ce = new CPTTEntities())
            {
                try
                {
                    // Transaction - Start
                    using (var trans = TransactionUtils.CreateTransactionScope())
                    {

                        // Create New Issue Tracker item
                        ce.issue_tracker.AddObject(objIssue);
                        
                        //Transaction complete
                        isSuccess = true;
                        ce.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                        trans.Complete(); 
                    }
                    //>
                }
                catch (Exception ex)
                {
                   // Log error in ELMAH for any diagnostic needs.
                   Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
 
                   sResult = ex.ToString();
                }
                finally
                {
                    ce.Connection.Dispose();
                }

                // Accept All Changes
                if (isSuccess)
                {
                    ce.AcceptAllChanges();
                }
            }
            return sResult;
        }
        //-->

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="objIssue">issue_tracker</param>
        /// <returns>string</returns>
        public static string Update(issue_tracker objIssue)
        {
            string sResult = null;
            bool isSuccess = false;
            CPTTEntities ce = new CPTTEntities();
            
            try
            {
               // Manually open connection to prevent any issues with the
               // entity framework and transaction scope.
               ce.Connection.Open();

               // Transaction - Start
               using (var trans = TransactionUtils.CreateTransactionScope())
               {

                  // Tenants - Delete Existing
                  var tennants = from t in ce.issue_tracker_tenants
                                 where t.issue_tracker_id == objIssue.id
                                 select t;
                  if (tennants.Any())
                  {
                        foreach (var t in tennants)
                        {
                           ce.DeleteObject(t);
                        }
                  }
                  //>

                  // Tenants - Add New
                  if (objIssue.issue_tracker_tenants.Any())
                  {
                        foreach (var newTennant in objIssue.issue_tracker_tenants)
                        {
                           // Create new Tennant in current Context
                           var t = ce.CreateObject<issue_tracker_tenants>();
                           t.issue_tracker_id = newTennant.issue_tracker.id;
                           t.customer_id = newTennant.customer_id;
                           ce.issue_tracker_tenants.AddObject(t);
                        }
                  }
                  //>

                  // Update Issue Tracker item
                  ce.AttachUpdated(objIssue);

                  //Transaction complete
                  isSuccess = true;
                  ce.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                  trans.Complete();
               }
               //>
            }
            catch (Exception ex)
            {
               // Log error in ELMAH for any diagnostic needs.
               Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
               
               sResult = ex.ToString();
            }
            finally
            {
               ce.Connection.Close();
            }

            // Accept All Changes
            if (isSuccess)
            {
               ce.AcceptAllChanges();
            }

            ce.Dispose();

            return sResult;
        }
        //-->

        #endregion

        #region Issue Action Items

        /// <summary>
        /// Add Issue Action Item
        /// </summary>
        /// <param name="actionItem"></param>
        public static string AddIssueActionItem(issue_action_items actionItem)
        {
            string sResult = null;

            try
            {
               using (var ce = new CPTTEntities())
               {
                  ce.issue_action_items.AddObject(actionItem);
                  ce.SaveChanges();
               }
            }
            catch (Exception ex)
            {
               // Error!
               // Log error in ELMAH for any diagnostic needs.
               Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

               sResult = ex.ToString();
            }

            return sResult;
        }
        //-->

        public static issue_action_items SearchIssueAction(int id)
        {
            issue_action_items ret = null;

            using (var ce = new CPTTEntities())
            {

                ret = ce.issue_action_items
                        .Where(it => it.id.Equals(id))
                        .FirstOrDefault();
            }

            return ret;
        }
        /// <summary>
        /// Delete Issue Action Item
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string DeleteIssueActionItem(int id)
        {
            string sResult = null;

            using (CPTTEntities ce = new CPTTEntities())
            {
                // Delete Issue Action Item
                var actionItem = ce.issue_action_items.First(ai => ai.id == id);
                ce.DeleteObject(actionItem);

                // Change DocLog
                DocumentsChangeLog.UpdateIsLinkRecordFlag(id, DMSDocumentLinkHelper.eDocumentType.issue_tracker.ToString(), false, ce,
                    DMSDocumentLinkHelper.eDocumentField.Issue_Action.ToString());

                try
                {
                    //remove any relate link document
                    var documents = from doc in ce.documents_links
                                    where doc.record_id == id
                                    select doc;
                    if (documents.Count() > 0)
                    {
                        foreach (documents_links item in documents.ToList<documents_links>())
                        {
                            ce.DeleteObject(item);
                        }
                    }

                    // Delete Issue Action Item
                    ce.DeleteObject(actionItem);

                    ce.SaveChanges();
                }
                catch (Exception ex)
                {
                    // Error!
                    // Log error in ELMAH for any diagnostic needs.
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                    sResult = ex.ToString();
                }
            }
            return sResult;
        }
        //-->

        #endregion

        // All site search criteria in a dictionary for easy lookup/manipulation
        // when dynamically building the query.
        public static Dictionary<string, SearchParameter> GetSearchCriteria()
        {
           var results = new Dictionary<string, SearchParameter>
            {
               {
                  "IssueID", new SearchParameter
                     {                        
                        VariableName = "id",
                        ParameterType = typeof(int),
                        WhereClause = "(it.id = {0})"
                     }
               },
               {
                  "SiteID", new SearchParameter
                     {                        
                        VariableName = "SiteID",
                        ParameterType = typeof(string),
                        WhereClause = "(it.site_uid LIKE {0})",
                        IsWildcardSearch = true
                     }
               },
               {
                  "SiteName", new SearchParameter
                     {
                        VariableName = "SiteName",
                        ParameterType = typeof(string),
                        WhereClause = "(it.site.site_name LIKE {0})",
                        IsWildcardSearch = true
                     }
               },
               {
                  "Priority", new SearchParameter
                     {
                        VariableName = "Priority",
                        ParameterType = typeof(int),
                        WhereClause = "(it.issue_priority_types.id = {0})",
                        AllowsMultipleValues = true
                     }
               },
               {
                  "IssueCreatedFrom", new SearchParameter
                     {
                        VariableName = "IssueCreatedFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.created_at >= {0})"
                     }
               },
               {
                  "IssueCreatedTo", new SearchParameter
                     {
                        VariableName = "IssueCreatedTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.created_at <= {0})"
                     }
               },
               {
                  "IssueCreatedRange", new SearchParameter
                     {
                        VariableName = "IssueCreatedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.created_at >= {0} AND it.created_at <= {1})",
                        IsRangeSearch = true
                     }
               },
               {
                  "CreateUser", new SearchParameter
                     {
                        VariableName = "CreateUser",
                        ParameterType = typeof(int),
                        WhereClause = "(it.creator_id = {0})",
                        AllowsMultipleValues = true
                     }
               },
               {
                  "CurrentSpecialist", new SearchParameter
                     {
                        VariableName = "CurrentSpecialist",
                        ParameterType = typeof(int),
                        WhereClause = "(it.specialist_user_id = {0})",
                        AllowsMultipleValues = true
                     }
               },
               {
                  "ProjectManager", new SearchParameter
                     {
                        VariableName = "ProjectManager",
                        ParameterType = typeof(int),
                        WhereClause = "(it.senior_pm_user_id = {0})",
                        AllowsMultipleValues = true
                     }
               },
               {
                  "Status", new SearchParameter
                     {
                        VariableName = "Status",
                        ParameterType = typeof(int),
                        WhereClause = "(it.issue_status_id = {0})",
                        AllowsMultipleValues = true
                     }
               },
               {
                  "Customer", new SearchParameter
                     {
                        VariableName = "Customer",
                        ParameterType = typeof(int),
                        WhereClause = "(EXISTS(SELECT VALUE itts FROM [CPTTEntities].[issue_tracker_tenants] AS itts " +
                                      "WHERE itts.issue_tracker_id = it.id AND itts.customer_id = {0}))",
                        AllowsMultipleValues = true
                     }
               }
            };
           
           return results;
        }
    }
    //>
}