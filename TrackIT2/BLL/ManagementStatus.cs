﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TrackIT2.DAL;

namespace TrackIT2.BLL
{
   public class ManagementStatus
   {
      public static management_statuses Search(int id)
      {

         management_statuses results = null;

         using (CPTTEntities ce = new CPTTEntities())
         {
            var query = ce.management_statuses.Where(ms => ms.id.Equals(id));

            if (query.Any())
            {
               results = query.First();
            }
         }

         return results;
      }
   }
}