﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TrackIT2.DAL;

namespace TrackIT2.BLL
{
   public static class SubmarketMapping
   {
      public static submarket_mapping Search(string site_uid)
      {
         submarket_mapping results = null;

         using (CPTTEntities ce = new CPTTEntities())
         {
            if (ce.submarket_mapping
                  .Where(sm => sm.site_id.Equals(site_uid))
                  .Count() > 0)
            {
               results = ce.submarket_mapping
                           .Where(sm => sm.site_id.Equals(site_uid))
                           .First();
            }
            else
            {
               results = null;
            }
         }

         return results;
      }

      public static submarket_mapping SearchWithinTransaction(string site_uid, CPTTEntities ceRef)
      {
         submarket_mapping results = null;

         if (ceRef.submarket_mapping
               .Where(sm => sm.site_id.Equals(site_uid))
               .Count() > 0)
         {
            results = ceRef.submarket_mapping
                        .Where(sm => sm.site_id.Equals(site_uid))
                        .First();
         }
         else
         {
            results = null;
         }

         return results;
      }

      public static submarket GetSubmarketBySite(string site_uid)
      {
         submarket results = null;

         using (CPTTEntities ce = new CPTTEntities())
         {
            if (ce.submarket_mapping
                  .Where(sm => sm.site_id.Equals(site_uid))
                  .Count() > 0)
            {
               results = ce.submarket_mapping
                           .Where(sm => sm.site_id.Equals(site_uid))
                           .First().submarket;
            }
            else
            {
               results = null;
            }
         }

         return results;
      }
   }
}