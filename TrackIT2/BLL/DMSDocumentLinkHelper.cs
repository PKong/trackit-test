﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TrackIT2.DAL;
using System.Transactions;
using System.Reflection;
using System.Data;

namespace TrackIT2.BLL
{
    public static class DMSDocumentLinkHelper
    {
        public const string UNKNOW_TYPE = "Unknow Type";

        #region enum

            public enum eDocumentLocation
            {
                Lease,
                Construction,
                Tenant,
                Permitting,
                Regulatory
            }

            public enum eDocumentType
            {
                structural_analyses,
                site_data_packages,
                lease_applications,
                leaseapp_revision,
                sites,
                issue_tracker,
                tower_modification,
                tower_modification_general,
                lease_admin
            }

            public enum eDocumentField
            {
                #region SA
                SAWToSAC,
                SAWRequest,
                SA_Received,
                Structural_Decision,
                DelayedOrder_Cancelled,
                DelayedOrder_OnHold,
                PurchaseOrder_Received,
                SA_Comments,
                #endregion SA

                #region LeaseApp
                //PR
                PreliminaryDecision,
                SDPtoCustomer,
                PreliminarySitewalk,
                LandlordConsentType,
                //DR
                LeaseExhibit_Received,
                LeaseExhibit_Decision,
                LandlordConsent_LandlordNotice,
                LandlordConsentRequested,
                LandlordConsentReceived,
                ConstructionDrawingsReceived,
                ConstructionDrawingsDecision,
                //LE
                ExecutablesSenttoCustomer,
                ExecutablesToCrown,
                LOEHoldupReasonTypes_DocsBackfromCustomer,
                LeaseExecution,
                AmendmentExecution,
                Financial_RentForecastAmount,
                NTPtoCust,
                RegCommencementLetterDate,
                //CO
                Preconstruction_PreconstructionSitewalk,
                NTPIssuedDateStatus,
                EarlyInstalltion_ViolationLetterSent,
                EarlyInstallation_ViolationInvoiceCreatedByFinops,
                PostConstruction_Sitewalk,
                CloseOut_FinalSiteApproval,
                CloseOut_AsBuilt,
                MarketHandoff_Status,
                //RE
                RevisionReveivedDate,
                //Comments
                LE_Comments,
                #endregion LeaseApp

                #region SDP
                // General
                SDPCheckList,
                SDPFirstNoticeToMarket,
                SDPSecondNoticeToMarket,
                SDPThirdNoticeToMarket,
                AdditionalPrimeDocuments,
                //Document Status
                TowerDrawing_DocTypes,
                TowerDrawing_DocStatus,
                TowerTagPhoto_DocStatus,
                StructuralCalcs_DocStatus,
                StructuralAnalysis_DocStatus,
                TowerErectionDetail_DocStatus,
                FoundationDesign_DocStatus,
                ConstructionDrawings_DocStatus,
                GeotechReport_DocStatus,
                BuildingPermit_DocStatus,
                ZoningApproval_DocStatus,
                PhaseI_DocStatus,
                PhaseII_DocStatus,
                NEPA_DocStatus,
                SHPO_DocStatus,
                GPS1a2c_DocStatus,
                Airspace_DocStatus,
                FAAStudyDet_DocStatus,
                FCC_DocStatus,
                AMCertification_DocStatus,
                Towair_DocStatus,
                PrimeLease_DocStatus,
                Memorandum_DocStatus,
                Title_DocStatus,
                //Abstract
                AbstractCompleted,
                //SDP Comments
                SDP_Comments,
                #endregion SDP

                #region Sites
                RogueEquipment,
                #endregion

                #region Issue Tracker
                Issue,
                #endregion
                
                #region Issue Action
                Issue_Action,
                #endregion

                #region Lease Admin
                LA_ExecutablesSenttoCustomer,
                LA_ExecutablesToCrown,
                LA_LOEHoldupReasonTypes_DocsBackfromCustomer,
                LA_LeaseExecution,
                LA_NTPtoCust,
                LA_RegCommencementLetterDate,
                LA_Comments,
                LA_RentForecastAmount,
                #endregion

                #region Tower Modification
                //Genral Tab
                TowerModPacketSentDate,
                BuildingPermitRcvdDate,
                //Tower Mod PO
                EbidComment,
                BidAmount,
                //TMO Comments
                TMO_Comments
                #endregion
            }

            public enum eListDocumentType
            {
                Application,
                Customer,
                Site,
                SiteDataPackage
            }

            public enum eDocumentRenameType
            {
                Tenant,
                SlaOrAmend,
                ApplicationType,
                Description,
            }

            public enum eDocumentAction
            {
                All,
                Upload,
                Link
            }

        #endregion

        #region "Mapping Enum"
        public static DmsLinkDocumentStructure DocumentField(string filed)
        {
            DmsLinkDocumentStructure ret = new DmsLinkDocumentStructure();
            switch (filed)
            {
                #region SA
                case "Structural_Decision":
                    ret.Field = eDocumentField.Structural_Decision;
                    ret.Type = eDocumentType.structural_analyses;
                    ret.Location = eDocumentLocation.Construction;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "SA";
                    break;
                case "SAWToSAC":
                    ret.Field = eDocumentField.SAWToSAC;
                    ret.Type = eDocumentType.structural_analyses;
                    ret.Location = eDocumentLocation.Construction;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.ApplicationType;
                    ret.NewName = "SAW";
                    ret.RenameDescription = "For Customer Approval";
                    break;
                case "SAWRequest":
                    ret.Field = eDocumentField.SAWRequest;
                    ret.Type = eDocumentType.structural_analyses;
                    ret.Location = eDocumentLocation.Construction;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.ApplicationType;
                    ret.NewName = "SAW";
                    ret.RenameDescription = "Approved by Customer";
                    break;
                case "SA_Received":
                    ret.Field = eDocumentField.SA_Received;
                    ret.Type = eDocumentType.structural_analyses;
                    ret.Location = eDocumentLocation.Construction;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.ApplicationType;
                    ret.NewName = "SA";
                    break;
                // DelayedOrder
                case "DelayedOrder_OnHold":
                    ret.Field = eDocumentField.DelayedOrder_OnHold;
                    ret.Type = eDocumentType.structural_analyses;
                    ret.Location = eDocumentLocation.Construction;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.ApplicationType;
                    ret.NewName = "SA Hold";
                    break;
                case "DelayedOrder_Cancelled":
                    ret.Field = eDocumentField.DelayedOrder_Cancelled;
                    ret.Type = eDocumentType.structural_analyses;
                    ret.Location = eDocumentLocation.Construction;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.ApplicationType;
                    ret.NewName = "SA Cancellation";
                    break;
                //Purchase Order
                case "PurchaseOrder_Received":
                    ret.Field = eDocumentField.PurchaseOrder_Received;
                    ret.Type = eDocumentType.structural_analyses;
                    ret.Location = eDocumentLocation.Construction;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "PO";
                    break;
                case "SA_Comments":
                    ret.Field = eDocumentField.SA_Comments;
                    ret.Type = eDocumentType.structural_analyses;
                    ret.Location = eDocumentLocation.Construction;
                    ret.AutoRename = false;
                    break;
                #endregion 

                #region LeaseApp

                #region PR
                case "PreliminaryDecision":
                    ret.Field = eDocumentField.PreliminaryDecision;
                    ret.Type = eDocumentType.lease_applications;
                    ret.Location = eDocumentLocation.Construction;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "Prelim Decision";
                    break;
                case "SDPtoCustomer":
                    ret.Field = eDocumentField.SDPtoCustomer;
                    ret.Type = eDocumentType.lease_applications;
                    ret.Location = eDocumentLocation.Tenant;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "SDP Transmittal";
                    break;
                case "PreliminarySitewalk":
                    ret.Field = eDocumentField.PreliminarySitewalk;
                    ret.Type = eDocumentType.lease_applications;
                    ret.Location = eDocumentLocation.Construction;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "Prelim SW";
                    break;
                case "LandlordConsentType":
                    ret.Field = eDocumentField.LandlordConsentType;
                    ret.Type = eDocumentType.lease_applications;
                    ret.Location = eDocumentLocation.Lease;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "LL Consent Req";
                    break;
                #endregion

                #region DR
                case "LeaseExhibit_Received":
                    ret.Field = eDocumentField.LeaseExhibit_Received;
                    ret.Type = eDocumentType.lease_applications;
                    ret.Location = eDocumentLocation.Lease;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "LE";
                    break;
                case "LeaseExhibit_Decision":
                    ret.Field = eDocumentField.LeaseExhibit_Decision;
                    ret.Type = eDocumentType.lease_applications;
                    ret.Location = eDocumentLocation.Lease;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "LE";
                    break;
                case "LandlordConsent_LandlordNotice":
                    ret.Field = eDocumentField.LandlordConsent_LandlordNotice;
                    ret.Type = eDocumentType.lease_applications;
                    ret.Location = eDocumentLocation.Lease;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "LL Notice";
                    break;
                case "LandlordConsentRequested":
                    ret.Field = eDocumentField.LandlordConsentRequested;
                    ret.Type = eDocumentType.lease_applications;
                    ret.Location = eDocumentLocation.Lease;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "LL Consent Req";
                    break;
                case "LandlordConsentReceived":
                    ret.Field = eDocumentField.LandlordConsentReceived;
                    ret.Type = eDocumentType.lease_applications;
                    ret.Location = eDocumentLocation.Lease;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "LL Consent";
                    break;
                case "ConstructionDrawingsReceived":
                    ret.Field = eDocumentField.ConstructionDrawingsReceived;
                    ret.Type = eDocumentType.lease_applications;
                    ret.Location = eDocumentLocation.Construction;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "CD";
                    break;
                case "ConstructionDrawingsDecision":
                    ret.Field = eDocumentField.ConstructionDrawingsDecision;
                    ret.Type = eDocumentType.lease_applications;
                    ret.Location = eDocumentLocation.Construction;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "CD";
                    break;
                #endregion

                #region LE
                case "ExecutablesSenttoCustomer":
                    ret.Field = eDocumentField.ExecutablesSenttoCustomer;
                    ret.Type = eDocumentType.lease_applications;
                    ret.Location = eDocumentLocation.Lease;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.SlaOrAmend;
                    ret.NewName = "{0}";
                    ret.RenameDescription = "Executables OFE";
                    break;
                case "ExecutablesToCrown":
                    ret.Field = eDocumentField.ExecutablesToCrown;
                    ret.Type = eDocumentType.lease_applications;
                    ret.Location = eDocumentLocation.Lease;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.SlaOrAmend;
                    ret.NewName = "{0}";
                    ret.RenameDescription = "Sent to Crown";
                    break;
                case "LOEHoldupReasonTypes_DocsBackfromCustomer":
                    ret.Field = eDocumentField.LOEHoldupReasonTypes_DocsBackfromCustomer;
                    ret.Type = eDocumentType.lease_applications;
                    ret.Location = eDocumentLocation.Lease;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.SlaOrAmend;
                    ret.NewName = "{0}";
                    ret.RenameDescription = "Tenant Signed";
                    break;
                case "LeaseExecution":
                    ret.Field = eDocumentField.LeaseExecution;
                    ret.Type = eDocumentType.lease_applications;
                    ret.Location = eDocumentLocation.Lease;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Description;
                    ret.NewName = "SLA";
                    ret.RenameDescription = "Fully Executed";
                    break;
                case "AmendmentExecution":
                    ret.Field = eDocumentField.AmendmentExecution;
                    ret.Type = eDocumentType.lease_applications;
                    ret.Location = eDocumentLocation.Lease;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Description;
                    ret.NewName = "Amend";
                    ret.RenameDescription = "Fully Executed";
                    break;
                case "Financial_RentForecastAmount":
                    ret.Field = eDocumentField.Financial_RentForecastAmount;
                    ret.Type = eDocumentType.lease_applications;
                    ret.Location = eDocumentLocation.Lease;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "DM Rent Approval";
                    break;
                case "NTPtoCust":
                    ret.Field = eDocumentField.NTPtoCust;
                    ret.Type = eDocumentType.lease_applications;
                    ret.Location = eDocumentLocation.Construction;
                    ret.RelatedField = eDocumentField.NTPIssuedDateStatus;
                    ret.MasterField = true;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "NTP";
                    break;
                case "RegCommencementLetterDate":
                    ret.Field = eDocumentField.RegCommencementLetterDate;
                    ret.Type = eDocumentType.lease_applications;
                    ret.Location = eDocumentLocation.Lease;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "Commencement";
                    break;
                #endregion

                #region CO
                case "Preconstruction_PreconstructionSitewalk":
                    ret.Field = eDocumentField.Preconstruction_PreconstructionSitewalk;
                    ret.Type = eDocumentType.lease_applications;
                    ret.Location = eDocumentLocation.Construction;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "Precon SW";
                    break;
                case "NTPIssuedDateStatus":
                    ret.Field = eDocumentField.NTPIssuedDateStatus;
                    ret.Type = eDocumentType.lease_applications;
                    ret.Location = eDocumentLocation.Construction;
                    ret.RelatedField = eDocumentField.NTPtoCust;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "NTP";
                    break;
                case "EarlyInstalltion_ViolationLetterSent":
                    ret.Field = eDocumentField.EarlyInstalltion_ViolationLetterSent;
                    ret.Type = eDocumentType.lease_applications;
                    ret.Location = eDocumentLocation.Construction;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "Violation Notice";
                    break;
                case "EarlyInstallation_ViolationInvoiceCreatedByFinops":
                    ret.Field = eDocumentField.EarlyInstallation_ViolationInvoiceCreatedByFinops;
                    ret.Type = eDocumentType.lease_applications;
                    ret.Location = eDocumentLocation.Construction;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "Violation Invoice";
                    break;
                case "PostConstruction_Sitewalk":
                    ret.Field = eDocumentField.PostConstruction_Sitewalk;
                    ret.Type = eDocumentType.lease_applications;
                    ret.Location = eDocumentLocation.Construction;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "Postcon SW";
                    break;
                case "CloseOut_AsBuilt":
                    ret.Field = eDocumentField.CloseOut_AsBuilt;
                    ret.Type = eDocumentType.lease_applications;
                    ret.Location = eDocumentLocation.Construction;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "As Built";
                    break;
                case "MarketHandoff_Status":
                    ret.Field = eDocumentField.MarketHandoff_Status;
                    ret.Type = eDocumentType.lease_applications;
                    ret.Location = eDocumentLocation.Construction;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "MHO";
                    break;
                case "CloseOut_FinalSiteApproval":
                    ret.Field = eDocumentField.CloseOut_FinalSiteApproval;
                    ret.Type = eDocumentType.lease_applications;
                    ret.Location = eDocumentLocation.Construction;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "FSA";
                    break;
                #endregion

                #region Comments
                case "LE_Comments":
                    ret.Field = eDocumentField.LE_Comments;
                    ret.Type = eDocumentType.lease_applications;
                    ret.AutoRename = false;
                    break;
                #endregion
                #endregion

                #region Lease App Revision
                case "RevisionReveivedDate":
                    ret.Field = eDocumentField.RevisionReveivedDate;
                    ret.Type = eDocumentType.leaseapp_revision;
                    ret.Location = eDocumentLocation.Tenant;
                    ret.AutoRename = false;
                    break;

                #endregion

                #region SDP
                #region General
                case "SDPCheckList":
                    ret.Field = eDocumentField.SDPCheckList;
                    ret.Type = eDocumentType.site_data_packages;
                    ret.Location = eDocumentLocation.Tenant;
                    ret.AutoRename = false;
                    break;
                case "AdditionalPrimeDocuments":
                    ret.Field = eDocumentField.AdditionalPrimeDocuments;
                    ret.Type = eDocumentType.site_data_packages;
                    ret.Action = eDocumentAction.Link;
                    ret.AutoRename = false;
                    break;
                case "SDPFirstNoticeToMarket":
                    ret.Field = eDocumentField.SDPFirstNoticeToMarket;
                    ret.Type = eDocumentType.site_data_packages;
                    ret.Location = eDocumentLocation.Tenant;
                    ret.AutoRename = false;
                    break;
                case "SDPSecondNoticeToMarket":
                    ret.Field = eDocumentField.SDPSecondNoticeToMarket;
                    ret.Type = eDocumentType.site_data_packages;
                    ret.Location = eDocumentLocation.Tenant;
                    ret.AutoRename = false;
                    break;
                case "SDPThirdNoticeToMarket":
                    ret.Field = eDocumentField.SDPThirdNoticeToMarket;
                    ret.Type = eDocumentType.site_data_packages;
                    ret.Location = eDocumentLocation.Tenant;
                    ret.AutoRename = false;
                    break;
                #endregion

                #region Document Status
                case "TowerDrawing_DocTypes":
                    ret.Field = eDocumentField.TowerDrawing_DocTypes;
                    ret.Type = eDocumentType.site_data_packages;
                    ret.Location = eDocumentLocation.Construction;
                    ret.AutoRename = false;
                    break;
                case "TowerDrawing_DocStatus":
                    ret.Field = eDocumentField.TowerDrawing_DocStatus;
                    ret.Type = eDocumentType.site_data_packages;
                    ret.Location = eDocumentLocation.Construction;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "TD";
                    break;
                case "TowerTagPhoto_DocStatus":
                    ret.Field = eDocumentField.TowerTagPhoto_DocStatus;
                    ret.Type = eDocumentType.site_data_packages;
                    ret.Location = eDocumentLocation.Construction;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "Tower ID";
                    break;
                case "StructuralCalcs_DocStatus":
                    ret.Field = eDocumentField.StructuralCalcs_DocStatus;
                    ret.Type = eDocumentType.site_data_packages;
                    ret.Location = eDocumentLocation.Construction;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "SC";
                    break;
                case "StructuralAnalysis_DocStatus":
                    ret.Field = eDocumentField.StructuralAnalysis_DocStatus;
                    ret.Type = eDocumentType.site_data_packages;
                    ret.Location = eDocumentLocation.Construction;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "SA";
                    break;
                case "TowerErectionDetail_DocStatus":
                    ret.Field = eDocumentField.TowerErectionDetail_DocStatus;
                    ret.Type = eDocumentType.site_data_packages;
                    ret.Location = eDocumentLocation.Construction;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "TE";
                    break;
                case "FoundationDesign_DocStatus":
                    ret.Field = eDocumentField.FoundationDesign_DocStatus;
                    ret.Type = eDocumentType.site_data_packages;
                    ret.Location = eDocumentLocation.Construction;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "FD";
                    break;
                case "ConstructionDrawings_DocStatus":
                    ret.Field = eDocumentField.ConstructionDrawings_DocStatus;
                    ret.Type = eDocumentType.site_data_packages;
                    ret.Location = eDocumentLocation.Construction;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "CD";
                    break;
                case "GeotechReport_DocStatus":
                    ret.Field = eDocumentField.GeotechReport_DocStatus;
                    ret.Type = eDocumentType.site_data_packages;
                    ret.Location = eDocumentLocation.Construction;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "Geo";
                    break;
                case "BuildingPermit_DocStatus":
                    ret.Field = eDocumentField.BuildingPermit_DocStatus;
                    ret.Type = eDocumentType.site_data_packages;
                    ret.Location = eDocumentLocation.Permitting;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "BP";
                    break;
                case "ZoningApproval_DocStatus":
                    ret.Field = eDocumentField.ZoningApproval_DocStatus;
                    ret.Type = eDocumentType.site_data_packages;
                    ret.Location = eDocumentLocation.Permitting;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "ZA";
                    break;
                case "PhaseI_DocStatus":
                    ret.Field = eDocumentField.PhaseI_DocStatus;
                    ret.Type = eDocumentType.site_data_packages;
                    ret.Location = eDocumentLocation.Permitting;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "Phase I";
                    break;
                case "PhaseII_DocStatus":
                    ret.Field = eDocumentField.PhaseII_DocStatus;
                    ret.Type = eDocumentType.site_data_packages;
                    ret.Location = eDocumentLocation.Permitting;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "Phase II";
                    break;
                case "NEPA_DocStatus":
                    ret.Field = eDocumentField.NEPA_DocStatus;
                    ret.Type = eDocumentType.site_data_packages;
                    ret.Location = eDocumentLocation.Regulatory;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "NEPA";
                    break;
                case "SHPO_DocStatus":
                    ret.Field = eDocumentField.SHPO_DocStatus;
                    ret.Type = eDocumentType.site_data_packages;
                    ret.Location = eDocumentLocation.Regulatory;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "SHPO";
                    break;
                case "GPS1a2c_DocStatus":
                    ret.Field = eDocumentField.GPS1a2c_DocStatus;
                    ret.Type = eDocumentType.site_data_packages;
                    ret.Location = eDocumentLocation.Regulatory;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "1A or 2C";
                    break;
                case "Airspace_DocStatus":
                    ret.Field = eDocumentField.Airspace_DocStatus;
                    ret.Type = eDocumentType.site_data_packages;
                    ret.Location = eDocumentLocation.Regulatory;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "Airspace";
                    break;
                case "FAAStudyDet_DocStatus":
                    ret.Field = eDocumentField.FAAStudyDet_DocStatus;
                    ret.Type = eDocumentType.site_data_packages;
                    ret.Location = eDocumentLocation.Regulatory;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "FAA";
                    break;
                case "FCC_DocStatus":
                    ret.Field = eDocumentField.FCC_DocStatus;
                    ret.Type = eDocumentType.site_data_packages;
                    ret.Location = eDocumentLocation.Regulatory;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "FCC";
                    break;
                case "AMCertification_DocStatus":
                    ret.Field = eDocumentField.AMCertification_DocStatus;
                    ret.Type = eDocumentType.site_data_packages;
                    ret.Location = eDocumentLocation.Regulatory;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "AM Cert";
                    break;
                case "Towair_DocStatus":
                    ret.Field = eDocumentField.Towair_DocStatus;
                    ret.Type = eDocumentType.site_data_packages;
                    ret.Location = eDocumentLocation.Regulatory;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "Towair";
                    break;
                case "PrimeLease_DocStatus":
                    ret.Field = eDocumentField.PrimeLease_DocStatus;
                    ret.Type = eDocumentType.site_data_packages;
                    ret.Location = eDocumentLocation.Lease;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "Lease";
                    break;
                case "Memorandum_DocStatus":
                    ret.Field = eDocumentField.Memorandum_DocStatus;
                    ret.Type = eDocumentType.site_data_packages;
                    ret.Location = eDocumentLocation.Lease;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "MOL";
                    break;
                case "Title_DocStatus":
                    ret.Field = eDocumentField.Title_DocStatus;
                    ret.Type = eDocumentType.site_data_packages;
                    ret.Location = eDocumentLocation.Lease;
                    ret.AutoRename = true;
                    ret.RenameType = eDocumentRenameType.Tenant;
                    ret.NewName = "Title";
                    break;
                #endregion

                #region Abstract
                case "AbstractCompleted":
                    ret.Field = eDocumentField.AbstractCompleted;
                    ret.Type = eDocumentType.site_data_packages;
                    ret.Location = eDocumentLocation.Lease;
                    ret.AutoRename = false;
                    break;
                #endregion
                #endregion
                    
                #region Sites
                case "RogueEquipment":
                    ret.Field = eDocumentField.RogueEquipment;
                    ret.Type = eDocumentType.sites;
                    ret.Location = eDocumentLocation.Construction;
                    ret.AutoRename = false;
                    break;
                #endregion

                #region Issue Tracker
                case "Issue":
                    ret.Field = eDocumentField.Issue;
                    ret.Type = eDocumentType.issue_tracker;
                    ret.AutoRename = false;
                    break;

                case "Issue_Action":
                    ret.Field = eDocumentField.Issue_Action;
                    ret.Type = eDocumentType.issue_tracker;
                    ret.AutoRename = false;
                    break;
                #endregion

                #region Lease Admin
                //TODO: No type when leaseAdmin not associated with leaseAdd, disable auto rename feature.
                case "LA_ExecutablesSenttoCustomer":
                    ret.Field = eDocumentField.LA_ExecutablesSenttoCustomer;
                    ret.Type = eDocumentType.lease_admin;
                    ret.Location = eDocumentLocation.Lease;
                    ret.AutoRename = false;
                    //ret.AutoRename = true;
                    //ret.RenameType = eDocumentRenameType.SlaOrAmend;
                    //ret.NewName = "{0}";
                    //ret.RenameDescription = "Executables OFE";
                    break;
                case "LA_LOEHoldupReasonTypes_DocsBackfromCustomer":
                    ret.Field = eDocumentField.LA_LOEHoldupReasonTypes_DocsBackfromCustomer;
                    ret.Type = eDocumentType.lease_admin;
                    ret.Location = eDocumentLocation.Lease;
                    ret.AutoRename = false;
                    //ret.AutoRename = true;
                    //ret.RenameType = eDocumentRenameType.SlaOrAmend;
                    //ret.NewName = "{0}";
                    //ret.RenameDescription = "Tenant Signed";
                    break;
                case "LA_LeaseExecution":
                    ret.Field = eDocumentField.LA_LeaseExecution;
                    ret.Type = eDocumentType.lease_admin;
                    ret.Location = eDocumentLocation.Lease;
                    ret.AutoRename = false;
                    //ret.AutoRename = true;
                    //ret.RenameType = eDocumentRenameType.Description;
                    //ret.NewName = "SLA";
                    //ret.RenameDescription = "Fully Executed";
                    break;
                case "LA_NTPtoCust":
                    ret.Field = eDocumentField.LA_NTPtoCust;
                    ret.Type = eDocumentType.lease_admin;
                    ret.Location = eDocumentLocation.Construction;
                    ret.AutoRename = false;
                    //ret.AutoRename = true;
                    //ret.RenameType = eDocumentRenameType.Tenant;
                    //ret.NewName = "NTP";
                    break;
                case "LA_RegCommencementLetterDate":
                    ret.Field = eDocumentField.LA_RegCommencementLetterDate;
                    ret.Type = eDocumentType.lease_admin;
                    ret.Location = eDocumentLocation.Lease;
                    ret.AutoRename = false;
                    //ret.AutoRename = true;
                    //ret.RenameType = eDocumentRenameType.Tenant;
                    //ret.NewName = "Commencement";
                    break;
                case "LA_Comments":
                    ret.Field = eDocumentField.LA_Comments;
                    ret.Type = eDocumentType.lease_admin;
                    ret.AutoRename = false;
                    break;
                case "LA_RentForecastAmount":
                    ret.Field = eDocumentField.LA_RentForecastAmount;
                    ret.Type = eDocumentType.lease_admin;
                    ret.Location = eDocumentLocation.Lease;
                    ret.AutoRename = false;
                    //ret.AutoRename = true;
                    //ret.RenameType = eDocumentRenameType.Tenant;
                    //ret.NewName = "DM Rent Approval";
                    break;
                #endregion

                #region Tower Modification
                #region General
                case "TowerModPacketSentDate":
                    ret.Field = eDocumentField.TowerModPacketSentDate;
                    ret.Type = eDocumentType.tower_modification_general;
                    ret.Location = eDocumentLocation.Construction;
                    ret.AutoRename = false;
                    break;    
                case "BuildingPermitRcvdDate":
                    ret.Field = eDocumentField.BuildingPermitRcvdDate;
                    ret.Type = eDocumentType.tower_modification_general;
                    ret.Location = eDocumentLocation.Permitting;
                    ret.AutoRename = false;
                    break;
                #endregion

                #region PO
                case "EbidComment" :
                    ret.Field = eDocumentField.EbidComment;
                    ret.Type = eDocumentType.tower_modification;
                    ret.Location = eDocumentLocation.Construction;
                    ret.AutoRename = false;
                    break;
                case "BidAmount" :
                    ret.Field = eDocumentField.BidAmount;
                    ret.Type = eDocumentType.tower_modification;
                    ret.Location = eDocumentLocation.Construction;
                    ret.AutoRename = false;
                    break;
                #endregion
                #endregion

                #region Comments
                case "TMO_Comments":
                    ret.Field = eDocumentField.TMO_Comments;
                    ret.Type = eDocumentType.tower_modification;
                    ret.AutoRename = false;
                    break;

                case "SDP_Comments":
                    ret.Field = eDocumentField.SDP_Comments;
                    ret.Type = eDocumentType.site_data_packages;
                    ret.AutoRename = false;
                    break;
                #endregion

                default :
                    throw new NullReferenceException();
            }
            return ret;
        }
        #endregion

        #region Widget Document
        public static DmsWidgetDocument[] widgetList = new DmsWidgetDocument[]{
                //Tower Mod : Permit Received
                new DmsWidgetDocument (DocumentField(eDocumentField.BuildingPermitRcvdDate.ToString()),false),
                //Lease Application : Preliminary Decision
                new DmsWidgetDocument (DocumentField(eDocumentField.PreliminaryDecision.ToString()),false),
                //Lease Application : Preliminary Sitewalk
                new DmsWidgetDocument (DocumentField(eDocumentField.PreliminarySitewalk.ToString()),false),
                //Lease Application : Process Dates Received
                new DmsWidgetDocument (DocumentField(eDocumentField.SA_Received.ToString()),false),
                //Lease Application : Executables Sent to Customer
                new DmsWidgetDocument (DocumentField(eDocumentField.ExecutablesSenttoCustomer.ToString()),false),
                //Lease Application : Lease Application : Executables to Crown
                new DmsWidgetDocument (DocumentField(eDocumentField.ExecutablesToCrown.ToString()),false),
                //Lease Application : Docs Back from Customer
                new DmsWidgetDocument (DocumentField(eDocumentField.LOEHoldupReasonTypes_DocsBackfromCustomer.ToString()),false),
                //Lease Application : Lease Execution
                new DmsWidgetDocument (DocumentField(eDocumentField.LeaseExecution.ToString()),false),
                //Lease Application : Preconstruction Sitewalk
                new DmsWidgetDocument (DocumentField(eDocumentField.Preconstruction_PreconstructionSitewalk.ToString()),false),
                //Lease Application : NTP Issued Date
                new DmsWidgetDocument (DocumentField(eDocumentField.NTPIssuedDateStatus.ToString()),false),
                //Lease Application : Sitewalk
                new DmsWidgetDocument (DocumentField(eDocumentField.PostConstruction_Sitewalk.ToString()),false),
                //Lease Application : Final Site Approval
                new DmsWidgetDocument (DocumentField(eDocumentField.CloseOut_FinalSiteApproval.ToString()),false),
                //Lease Application : Market Handoff
                new DmsWidgetDocument (DocumentField(eDocumentField.MarketHandoff_Status.ToString()),false),

                //SDP : Tower Drawing Doc Status
                new DmsWidgetDocument (DocumentField(eDocumentField.TowerDrawing_DocStatus.ToString()),true),
                //SDP : Tower Tag Photo Doc Status
                new DmsWidgetDocument (DocumentField(eDocumentField.TowerTagPhoto_DocStatus.ToString()),true),
                //SDP : Structural Calcs Doc Status
                new DmsWidgetDocument (DocumentField(eDocumentField.StructuralCalcs_DocStatus.ToString()),true),
                //SDP : Structural Analysis Current Doc Status
                new DmsWidgetDocument (DocumentField(eDocumentField.StructuralAnalysis_DocStatus.ToString()),true),
                //SDP : Tower Erection Detail Doc Status
                new DmsWidgetDocument (DocumentField(eDocumentField.TowerErectionDetail_DocStatus.ToString()),true),
                //SDP : Foundation Design Doc Status
                new DmsWidgetDocument (DocumentField(eDocumentField.FoundationDesign_DocStatus.ToString()),true),
                //SDP : Construction Drawings Doc Status
                new DmsWidgetDocument (DocumentField(eDocumentField.ConstructionDrawings_DocStatus.ToString()),true),
                //SDP : Geotech Report Doc Status
                new DmsWidgetDocument (DocumentField(eDocumentField.GeotechReport_DocStatus.ToString()),true),
                //SDP : Building Permit Doc Status
                new DmsWidgetDocument (DocumentField(eDocumentField.BuildingPermit_DocStatus.ToString()),true),
                //SDP : Zoning Approval Doc Status
                new DmsWidgetDocument (DocumentField(eDocumentField.ZoningApproval_DocStatus.ToString()),true),
                //SDP : Phase I Doc Status
                new DmsWidgetDocument (DocumentField(eDocumentField.PhaseI_DocStatus.ToString()),true),
                //SDP : Phase II Doc Status
                new DmsWidgetDocument (DocumentField(eDocumentField.PhaseII_DocStatus.ToString()),true),
                //SDP : NEPA Doc Status
                new DmsWidgetDocument (DocumentField(eDocumentField.NEPA_DocStatus.ToString()),true),
                //SDP : SHPO Doc Status
                new DmsWidgetDocument (DocumentField(eDocumentField.SHPO_DocStatus.ToString()),true),
                //SDP : GPS 1a2c Doc Status
                new DmsWidgetDocument (DocumentField(eDocumentField.GPS1a2c_DocStatus.ToString()),true),
                //SDP : Airspace Doc Status
                new DmsWidgetDocument (DocumentField(eDocumentField.Airspace_DocStatus.ToString()),true),
                //SDP : FAA Study Determination Doc Status
                new DmsWidgetDocument (DocumentField(eDocumentField.FAAStudyDet_DocStatus.ToString()),true),
                //SDP : FCC Doc Status
                new DmsWidgetDocument (DocumentField(eDocumentField.FCC_DocStatus.ToString()),true),
                //SDP : AMCertification Doc Status
                new DmsWidgetDocument (DocumentField(eDocumentField.AMCertification_DocStatus.ToString()),true),
                //SDP : Towair Document Status
                new DmsWidgetDocument (DocumentField(eDocumentField.Towair_DocStatus.ToString()),true),
                //SDP : Prime Lease Doc Status
                new DmsWidgetDocument (DocumentField(eDocumentField.PrimeLease_DocStatus.ToString()),true),
                //SDP : Memorandum of Lease Doc Status
                new DmsWidgetDocument (DocumentField(eDocumentField.Memorandum_DocStatus.ToString()),true),
                //SDP : Title Doc Status
                new DmsWidgetDocument (DocumentField(eDocumentField.Title_DocStatus.ToString()),true)};

        #endregion

        //Insert prime site links to db
        #region prime site document

        public static string AddPrimeDocumentLink(string trackitFolder,string siteId)
        {
            string result = "";
            bool isSuccess = false;

            using (CPTTEntities ce = new CPTTEntities())
            {
                try
                {
                    using (TransactionScope trans = TransactionUtils.CreateTransactionScope())
                    {
                        documents_prime_site_links siteDocument = new documents_prime_site_links();
                        siteDocument.site_uid = siteId;
                        siteDocument.trackit_docs_folder = trackitFolder;

                        // Manually open connection to prevent EF from auto closing
                        // connection and causing issues with future queries.
                        ce.Connection.Open();
                        ce.documents_prime_site_links.AddObject(siteDocument);
                        ce.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                        isSuccess = true;
                        trans.Complete();     // Transaction complete
                    }
                }
                catch (Exception ex)
                {
                    // Log error in ELMAH for any diagnostic needs.
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    isSuccess = false;
                    result = "Error Saving TrackiT Prime Site Docs Link record(s). Please try again.";
                }
                finally
                {
                    ce.Connection.Dispose();
                }
                // Finally accept all changes from the transaction.
                if (isSuccess)
                {
                    ce.AcceptAllChanges();
                }
            }

            return result;
        }

        public static string UpdatePrimeDocumentLink(documents_prime_site_links documents_prime_site_links)
        {
            string result = "";
            bool isSuccess = false;

            using (CPTTEntities ce = new CPTTEntities())
            {
                try
                {
                    using (TransactionScope trans = TransactionUtils.CreateTransactionScope())
                    {
                        // Manually open connection to prevent EF from auto closing
                        // connection and causing issues with future queries.
                        ce.Connection.Open();
                        ce.AttachUpdated(documents_prime_site_links);
                        ce.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                        isSuccess = true;
                        trans.Complete();     // Transaction complete
                    }
                }
                catch (Exception ex)
                {
                    // Log error in ELMAH for any diagnostic needs.
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    isSuccess = false;
                    result = "Error Saving TrackiT Prime Site Docs Link record(s). Please try again.";
                }
                finally
                {
                    ce.Connection.Dispose();
                }
                // Finally accept all changes from the transaction.
                if (isSuccess)
                {
                    ce.AcceptAllChanges();
                }
            }

            return result;
        }
        public static List<documents_prime_site_links> GetPrimeDocumentLink(string trackitFolder, string siteId, CPTTEntities ceRef = null)
        {
            List<documents_prime_site_links> primedoc = new List<documents_prime_site_links>();
            bool isRootTransaction = true;
            CPTTEntities ce;
            if (ceRef == null)
            {
                //using new entity object
                ce = new CPTTEntities();
            }
            else
            {
                //using current entity object
                ce = ceRef;
                isRootTransaction = false;
            }

            if (isRootTransaction)
            {
                ce.Connection.Open();
            }

            try
            {
                if (!String.IsNullOrEmpty(siteId) && !String.IsNullOrEmpty(siteId))
                {
                    primedoc = (from doc in ce.documents_prime_site_links
                                where doc.site_uid == siteId
                                   && doc.trackit_docs_folder == trackitFolder
                                   select doc).ToList<documents_prime_site_links>();

                }
            }
            catch (Exception ex)
            {
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            finally
            {
                if (isRootTransaction)
                {
                    ce.Connection.Dispose();
                }
            }
            return primedoc;
        }

        #endregion

        //Insert new link to db
        #region Insert New document
            public static string AddDocumentLink(int currentID, int ID, DmsLinkDocumentStructure document, string key, bool isSave)
            {
                string result = "";
                if (document.RelatedField.HasValue)
                {
                    if (!document.MasterField)
                    {
                        document = DocumentField(document.RelatedField.ToString());
                    }
                }
                documents_links doc = new documents_links();
                doc.record_id = ID;
                doc.linked_field = document.Field.ToString();
                doc.record_type = document.Type.ToString();;
                doc.trackit_docs_folder = key;
                result = AddDocumentLink(doc);
                return result;
            }

            public static string AddDocumentLink(documents_links_site siteDocument)
            {
                string result = "";
                bool isSuccess = false;

                using (CPTTEntities ce = new CPTTEntities())
                {
                    try
                    {
                        using (TransactionScope trans = TransactionUtils.CreateTransactionScope())
                        {
                            // Manually open connection to prevent EF from auto closing
                            // connection and causing issues with future queries.
                            ce.Connection.Open();

                            ce.documents_links_site.AddObject(siteDocument);
                            ce.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                            isSuccess = true;
                            trans.Complete();     // Transaction complete
                        }
                    }
                    catch (Exception ex)
                    {
                        // Log error in ELMAH for any diagnostic needs.
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        isSuccess = false;
                        result = "Error Saving TrackiT Docs Link record(s). Please try again.";
                    }
                    finally
                    {
                        ce.Connection.Dispose();
                    }
                    // Finally accept all changes from the transaction.
                    if (isSuccess)
                    {
                        ce.AcceptAllChanges();
                    }
                }

                return result;
            }

            public static string AddCustomerDocument(documents_customer_links document, CPTTEntities ceRef = null)
            {
                string result = "";
                bool isSuccess = false;

                using (CPTTEntities ce = new CPTTEntities())
                {
                    try
                    {
                        using (TransactionScope trans = TransactionUtils.CreateTransactionScope())
                        {
                            // Manually open connection to prevent EF from auto closing
                            // connection and causing issues with future queries.
                            ce.Connection.Open();

                            ce.documents_customer_links.AddObject(document);

                            ce.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                            isSuccess = true;
                            trans.Complete();     // Transaction complete
                        }
                    }
                    catch (Exception ex)
                    {
                        // Log error in ELMAH for any diagnostic needs.
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        isSuccess = false;
                        result = "Error Saving TrackiT Docs Link record(s). Please try again.";
                    }
                    finally
                    {
                        ce.Connection.Dispose();
                    }
                    // Finally accept all changes from the transaction.
                    if (isSuccess)
                    {
                        ce.AcceptAllChanges();
                    }
                }

                return result;
            }
            private static string AddDocumentLink(documents_links document)
            {
                string result = "";
                bool isSuccess = false;

                using (CPTTEntities ce = new CPTTEntities())
                {
                    try
                    {
                        using (TransactionScope trans = TransactionUtils.CreateTransactionScope())
                        {
                            // Manually open connection to prevent EF from auto closing
                            // connection and causing issues with future queries.
                            ce.Connection.Open();

                            ce.documents_links.AddObject(document);

                            ce.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                            isSuccess = true;
                            trans.Complete();     // Transaction complete
                        }
                    }
                    catch (Exception ex)
                    {
                        // Log error in ELMAH for any diagnostic needs.
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        isSuccess = false;
                        result = "Error Saving TrackiT Docs Link record(s). Please try again.";
                    }
                    finally
                    {
                        ce.Connection.Dispose();
                    }
                    // Finally accept all changes from the transaction.
                    if (isSuccess)
                    {
                        ce.AcceptAllChanges();
                    }
                }

                return result;
            }

        #endregion

        //Remove link from db
        #region Remove Document
            public static string RemoveDocumentLink(int ID, DmsLinkDocumentStructure document, string key)
            {
                if (document.RelatedField.HasValue)
                {
                    if (!document.MasterField)
                    {
                        document = DocumentField(document.RelatedField.ToString());
                    }
                }
                string result = RemoveDocument(ID, document.Type.ToString(), document.Field.ToString(), key);
                return result;
            }

            public static string RemoveDocumentLink(String ID, string field, string key)
            {
                string result = "";
                using (CPTTEntities ce = new CPTTEntities())
                {
                    try
                    {
                        documents_links_site doc = (from item in ce.documents_links_site
                                                     where item.record_id == ID
                                                     && item.linked_field == field
                                                     && item.trackit_docs_folder == key
                                                     select item).FirstOrDefault();
                        if (doc != null)
                        {
                            ce.DeleteObject(doc);
                            ce.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        // Log error in ELMAH for any diagnostic needs.
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        result = "Error Remove Site Data Packages Document Link record(s). Please try again.";
                    }
                }
                return result;
            }

            private static string RemoveDocument(int typeID, string type, string field, string key)
            {
                string result = "";

                using (CPTTEntities ce = new CPTTEntities())
                {
                    try
                    {
                        documents_links doc = (from item in ce.documents_links
                                  where item.record_id == typeID
                                  && item.linked_field == field
                                  && item.trackit_docs_folder == key
                                  select item).FirstOrDefault();
                        if (doc != null)
                        {
                            ce.DeleteObject(doc);
                            DocumentsChangeLog.UpdateIsLinkRecordFlag(typeID, type, false, ce, field);
                            ce.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        // Log error in ELMAH for any diagnostic needs.
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        result = "Error Remove Site Data Packages Document Link record(s). Please try again.";
                    }
                }
                return result;
            }


            public static string RemoveDocument(string key)
            {
                string result = "";

                using (CPTTEntities ce = new CPTTEntities())
                {
                    try
                    {
                        var docs = from item in ce.documents_links
                                  where item.trackit_docs_folder == key
                                  select item;
                        if (docs.Any())
                        {
                            List<documents_links> lstDocument = docs.ToList();
                            for (int i = 0; i < lstDocument.Count(); i++)
                            {
                                if (lstDocument[i] != null)
                                {
                                    ce.DeleteObject(lstDocument[i]);
                                }
                            }
                            ce.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        // Log error in ELMAH for any diagnostic needs.
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        result = "Error Remove Site Data Packages Document Link record(s). Please try again.";
                    }
                }
                return result;
            }
        #endregion

        //Get all docLink from db
        #region Get Document Data
            public static List<Dms_Document> GetDocumentLink(int ID, DmsLinkDocumentStructure document, string key = "",  CPTTEntities ceRef = null , eDocumentType? documentType = null ,List<documents_links> releateDoc = null)
            {
               List<Dms_Document> docLinkObj = new List<Dms_Document>();
               var isReferenceLookup = false;
 
               if (document.RelatedField.HasValue)
               {
                  if (!document.MasterField)
                  {
                     document = DocumentField(document.RelatedField.ToString());
                     isReferenceLookup = true;
                  }
               }


                if (key != "")
                {
                    docLinkObj = GetDocumentLink(ID, document.Type.ToString(), document.Field.ToString(), key, ceRef,releateDoc);
                }
                else
                {
                    docLinkObj = GetDocumentLink(ID, document.Type.ToString(), document.Field.ToString(), ceRef, releateDoc);
                }

                // Finally, if a reference lookup was used, we need to reapply 
                // the original reference key to prevent search collisions on
                // the client side.
                if (isReferenceLookup)
                {
                   foreach (var doc in docLinkObj)
                   {
                      doc.ref_field = document.RelatedField.ToString();
                   }
                }

                return docLinkObj;
            }


            public static List<Dms_Document_Site> GetDocumentLink(String ID, string key = "", CPTTEntities ceRef = null)
            {
                List<Dms_Document_Site> docLinkObj = new List<Dms_Document_Site>();
                bool isRootTransaction = true;
                CPTTEntities ce;
                if (ceRef == null)
                {
                    //using new entity object
                    ce = new CPTTEntities();
                }
                else
                {
                    //using current entity object
                    ce = ceRef;
                    isRootTransaction = false;
                }

                if (isRootTransaction)
                {
                    ce.Connection.Open();
                }

                try
                {
                    List<Dms_Document_Site> obj = new List<Dms_Document_Site>();
                    if (key != "")
                    {
                        obj = (from docLinkTable in ce.documents_links_site
                                  where docLinkTable.record_id == ID
                                  && docLinkTable.trackit_docs_folder == key
                                  select new Dms_Document_Site
                                  {
                                      ID = docLinkTable.record_id,
                                      ref_field = docLinkTable.linked_field,
                                      ref_key = docLinkTable.trackit_docs_folder,
                                      type = docLinkTable.record_type
                                  }).ToList<Dms_Document_Site>();

                    } 
                    else
                    {
                        obj = (from docLinkTable in ce.documents_links_site
                               where docLinkTable.record_id == ID
                               select new Dms_Document_Site
                               {
                                   ID = docLinkTable.record_id,
                                   ref_field = docLinkTable.linked_field,
                                   ref_key = docLinkTable.trackit_docs_folder,
                                   type = docLinkTable.record_type
                               }).ToList<Dms_Document_Site>();

                    }
                    foreach (Dms_Document_Site doc in obj)
                    {
                        Dms_Document_Site current = doc;
                        current.documentName = HttpUtility.UrlDecode(current.ref_key).Split('/')[HttpUtility.UrlDecode(current.ref_key).Split('/').Length - 1];
                        docLinkObj.Add(current);
                    }
                }
                catch (Exception ex)
                {
                    // Log error in ELMAH for any diagnostic needs.
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                }
                finally
                {
                    if (isRootTransaction)
                    {
                        ce.Connection.Dispose();
                    }
                }
                return docLinkObj;
            }

            private static List<Dms_Document> GetDocumentLink(int ID, string type, string field, CPTTEntities ceRef = null, List<documents_links> releateDoc = null)
            {
                List<Dms_Document> docLinkObj = new List<Dms_Document>();
                bool isRootTransaction = true;
                CPTTEntities ce;

                if (ceRef == null)
                {
                    //using new entity object
                    ce = new CPTTEntities();
                }
                else
                {
                    //using current entity object
                    ce = ceRef;
                    isRootTransaction = false;
                }

                if (isRootTransaction)
                {
                    ce.Connection.Open();
                }

                try
                {
                    List<Dms_Document> returnList;
                    if (releateDoc == null)
                    {
                        returnList = (from docLinkTable in ce.documents_links
                                  where docLinkTable.record_id == ID
                                  && docLinkTable.record_type == type
                                  && docLinkTable.linked_field == field
                                  select new Dms_Document
                                  {
                                      ID = docLinkTable.record_id,
                                      ref_field = docLinkTable.linked_field,
                                      ref_key = docLinkTable.trackit_docs_folder,
                                      type = docLinkTable.record_type
                                  }).ToList();
                    }
                    else
                    {
                        returnList = (from docLinkTable in releateDoc
                                      where docLinkTable.record_id == ID
                                      && docLinkTable.record_type == type
                                      && docLinkTable.linked_field == field
                                      select new Dms_Document
                                      {
                                          ID = docLinkTable.record_id,
                                          ref_field = docLinkTable.linked_field,
                                          ref_key = docLinkTable.trackit_docs_folder,
                                          type = docLinkTable.record_type
                                      }).ToList();
                    }

                    foreach (Dms_Document doc in returnList)
                    {
                        Dms_Document current = doc;
                        current.documentName = HttpUtility.UrlDecode(current.ref_key).Split('/')[HttpUtility.UrlDecode(current.ref_key).Split('/').Length - 1];
                        docLinkObj.Add(current);
                    }
                }
                catch (Exception ex)
                {
                    // Log error in ELMAH for any diagnostic needs.
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                }
                finally
                {
                    // Only close connection if this is an independent query.           
                    if (isRootTransaction)
                    {
                        ce.Connection.Dispose();
                    }
                }
                return docLinkObj;
            }

            private static List<Dms_Document> GetDocumentLink(int ID, string type, string field, string key, CPTTEntities ceRef = null, List<documents_links> releateDoc = null)
            {
                List<Dms_Document> docLinkObj = new List<Dms_Document>();
                bool isRootTransaction = true;
                CPTTEntities ce;

                if (ceRef == null)
                {
                    //using new entity object
                    ce = new CPTTEntities();
                }
                else
                {
                    //using current entity object
                    ce = ceRef;
                    isRootTransaction = false;
                }

                if (isRootTransaction)
                {
                    ce.Connection.Open();
                }
                try
                {
                    List<Dms_Document> returnList;
                    if (releateDoc == null)
                    {
                        returnList = (from docLinkTable in ce.documents_links
                                  where docLinkTable.record_id == ID
                                  && docLinkTable.record_type == type
                                  && docLinkTable.linked_field == field
                                  && docLinkTable.trackit_docs_folder == key
                                  select new Dms_Document
                                  {
                                      ID = docLinkTable.record_id,
                                      ref_field = docLinkTable.linked_field,
                                      ref_key = docLinkTable.trackit_docs_folder,
                                      type = docLinkTable.record_type
                                  }).ToList();
                    }
                    else
                    {
                        returnList = (from docLinkTable in releateDoc
                                  where docLinkTable.record_id == ID
                                  && docLinkTable.record_type == type
                                  && docLinkTable.linked_field == field
                                  && docLinkTable.trackit_docs_folder == key
                                  select new Dms_Document
                                  {
                                      ID = docLinkTable.record_id,
                                      ref_field = docLinkTable.linked_field,
                                      ref_key = docLinkTable.trackit_docs_folder,
                                      type = docLinkTable.record_type
                                  }).ToList();
                    }

                    foreach (Dms_Document doc in returnList)
                    {
                        Dms_Document current = doc;
                        current.documentName = HttpUtility.UrlDecode(current.ref_key).Split('/')[HttpUtility.UrlDecode(current.ref_key).Split('/').Length - 1];
                        docLinkObj.Add(current);
                    }
                }
                catch (Exception ex)
                {
                    // Log error in ELMAH for any diagnostic needs.
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                }
                finally
                {
                    // Only close connection if this is an independent query.           
                    if (isRootTransaction)
                    {
                        ce.Connection.Dispose();
                    }
                }
                return docLinkObj;
            }
            //get documents_customer_links 
            public static documents_customer_links GetCustomerDocument(int customerId, string site_uid, string key, CPTTEntities ceRef = null)
            {
                documents_customer_links docCustomer = null;
                bool isRootTransaction = true;
                CPTTEntities ce;

                if (ceRef == null)
                {
                    //using new entity object
                    ce = new CPTTEntities();
                }
                else
                {
                    //using current entity object
                    ce = ceRef;
                    isRootTransaction = false;
                }

                if (isRootTransaction)
                {
                    ce.Connection.Open();
                }
                try
                {
                    docCustomer = (from docCust in ce.documents_customer_links
                              where docCust.customer_id == customerId
                              && docCust.site_uid == site_uid
                              && docCust.trackit_docs_folder == key
                              select docCust).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    // Log error in ELMAH for any diagnostic needs.
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                }
                finally
                {
                    // Only close connection if this is an independent query.           
                    if (isRootTransaction)
                    {
                        ce.Connection.Dispose();
                    }
                }
                return docCustomer;
            }

        #endregion

        public static List<Dms_Document> GetDocumentLinkList(List<AllComments> lstComment, CPTTEntities ceRef = null)
        {
            List<Dms_Document> docLinkObj = new List<Dms_Document>();
            bool isRootTransaction = true;
            CPTTEntities ce;

            if (ceRef == null)
            {
                //using new entity object
                ce = new CPTTEntities();
            }
            else
            {
                //using current entity object
                ce = ceRef;
                isRootTransaction = false;
            }

            if (isRootTransaction)
            {
                ce.Connection.Open();
            }

            try
            {
                var allDocLinkTable = (from table in ce.documents_links
                                        where (table.linked_field == "SA_Comments"
                                            || table.linked_field == "LE_Comments"
                                            || table.linked_field == "LA_Comments"
                                            || table.linked_field == "TMO_Comments"
                                            || table.linked_field == "SDP_Comments")
                                        select table).ToList();

                var obj = (from lst in lstComment
                            from docLinkTable in allDocLinkTable
                            where (lst.CommentID == docLinkTable.record_id)
                            select new Dms_Document
                            {
                                ID = docLinkTable.record_id,
                                ref_field = docLinkTable.linked_field,
                                ref_key = docLinkTable.trackit_docs_folder,
                                type = docLinkTable.record_type
                            }).ToList();

                foreach (Dms_Document doc in obj)
                {
                    Dms_Document current = doc;
                    current.documentName = HttpUtility.UrlDecode(current.ref_key).Split('/')[HttpUtility.UrlDecode(current.ref_key).Split('/').Length - 1];
                    docLinkObj.Add(current);
                }
            }
            catch (Exception ex)
            {
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            finally
            {
                // Only close connection if this is an independent query.           
                if (isRootTransaction)
                {
                    ce.Connection.Dispose();
                }
            }
            return docLinkObj;
        }

        public static List<Dms_Document> GetDocumentLinkFromList(AllComments commentItem, DmsLinkDocumentStructure document, List<Dms_Document> all_comment_document_list, eDocumentType? documentType = null)
        {
            List<Dms_Document> docLinkObj = new List<Dms_Document>();
            var isReferenceLookup = false;

            if (document.RelatedField.HasValue)
            {
                if (!document.MasterField)
                {
                    document = DocumentField(document.RelatedField.ToString());
                    isReferenceLookup = true;
                }
            }

            docLinkObj = (from docObj in all_comment_document_list
                            where (docObj.ID == commentItem.CommentID)
                            && (docObj.ref_field == document.Field.ToString())
                            select docObj).ToList();

            if (isReferenceLookup)
            {
                foreach (var doc in docLinkObj)
                {
                    doc.ref_field = document.RelatedField.ToString();
                }
            }

            return docLinkObj;
        }

        public static List<documents_links> GetDocumentLinkListLeaseAppBinding(CPTTEntities ceRef = null)
        {
            CPTTEntities ce;
            bool isRootTransaction = true;
            List<documents_links> docLinkObj = new List<documents_links>();

            if (ceRef == null)
            {
                //using new entity object
                ce = new CPTTEntities();
            }
            else
            {
                //using current entity object
                ce = ceRef;
                isRootTransaction = false;
            }

            if (isRootTransaction)
            {
                ce.Connection.Open();
            }

            try
            {
                docLinkObj = (from docLinkTable in ce.documents_links
                              where docLinkTable.record_type.Equals("structural_analyses")
                              || docLinkTable.record_type.Equals("lease_applications")
                              || docLinkTable.record_type.Equals("leaseapp_revision")
                           select docLinkTable).ToList();
            }
            catch (Exception ex)
            {
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            finally
            {
                if (isRootTransaction)
                {
                    ce.Connection.Dispose();
                }
            }
            return docLinkObj;
        }

        //Rename link
        public static bool RenameAllDocumentLink(CPTTEntities ce,string newKey, string oldkey)
        {
            IQueryable<documents_links> document = null;
            IQueryable<documents_links_site> document_site = null;
            bool result = false;
                try
                {

                    document = from docLinkTable in ce.documents_links
                            where docLinkTable.trackit_docs_folder == oldkey
                            select docLinkTable;

                    foreach (var item in document)
                    {
                        item.trackit_docs_folder = newKey;
                        result = true;
                    }

                    document_site = from docLinkTable in ce.documents_links_site
                             where docLinkTable.trackit_docs_folder == oldkey
                             select docLinkTable;

                    foreach (var item in document_site)
                    {
                        item.trackit_docs_folder = newKey;
                        result = true;
                    }
                }
                catch (Exception ex)
                {
                    // Log error in ELMAH for any diagnostic needs.
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                }
            return result;
        }

        public static bool RenameFolder(CPTTEntities ce, string newKey, string oldkey)
        {
            IQueryable<documents_links> document = null;
            IQueryable<documents_links_site> document_site = null;
            bool result = false;
            try
            {
                document = from docLinkTable in ce.documents_links
                        where docLinkTable.trackit_docs_folder.StartsWith(oldkey)
                        select docLinkTable;

                foreach (var item in document)
                {
                    item.trackit_docs_folder = item.trackit_docs_folder.Replace(oldkey, newKey);
                    result = true;
                }

                document_site = from docLinkTable in ce.documents_links_site
                                where docLinkTable.trackit_docs_folder.StartsWith(oldkey)
                                select docLinkTable;

                foreach (var item in document_site)
                {
                    item.trackit_docs_folder = item.trackit_docs_folder.Replace(oldkey, newKey);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return result;
        }

        public static List<T> MakePropToList<T, TSource>(this List<TSource> source, String propName)
        {
            List<T> lstReturn = new List<T>();
            PropertyInfo property = typeof(TSource).GetProperty(propName);
            if (typeof(T) != property.PropertyType)
            {
                throw new InvalidCastException();
            }
            foreach (var item in source)
            {
                lstReturn.Add((T)property.GetValue(item, null));
            }
            return lstReturn;
        }

        public static customer GetTenant(int id, eDocumentType? type)
        {
            customer objCus = null;
            using (CPTTEntities ce = new CPTTEntities())
            {
                lease_applications leaseApp = null;

                switch (type.Value)
                {
                    case eDocumentType.lease_applications:
                    case eDocumentType.structural_analyses:
                        leaseApp = LeaseApplication.SearchWithinTransaction(id, ce);
                        if (leaseApp != null)
                        {
                            objCus = Customer.Search(leaseApp.customer_id);
                        }
                        break;

                    case eDocumentType.site_data_packages:
                        site_data_packages sdpObj = SiteDataPackage.SearchWithinTransaction(id, ce);
                        if (sdpObj != null)
                        {
                            if (sdpObj.lease_applications != null)
                            {
                                objCus = Customer.Search(sdpObj.lease_applications.customer_id);
                            }
                            else
                            {
                                objCus = Customer.Search(sdpObj.customer_id);
                            }
                        }

                        break;
                    default:
                        break;
                }
            }
            return objCus;
        }

        public static string GetApplicationType(int id, eDocumentType? type)
        {
            string returnType = UNKNOW_TYPE;

            using (CPTTEntities ce = new CPTTEntities())
            {
                lease_applications leaseApp = null;
                leaseapp_types leaseAppType = null;

                switch (type.Value)
                {
                    // leaseApp
                    case eDocumentType.lease_applications:
                    case eDocumentType.structural_analyses:
                        leaseApp = LeaseApplication.SearchWithinTransaction(id, ce);
                        leaseAppType = leaseApp.leaseapp_types;
                        if (leaseAppType != null)
                        {
                            returnType = leaseAppType.lease_application_type;
                        }
                        break;
                    // sdp
                    case eDocumentType.site_data_packages:
                        site_data_packages sdpObj = SiteDataPackage.SearchWithinTransaction(id, ce);
                        if (sdpObj != null)
                        {
                            if (sdpObj.lease_applications != null)
                            {
                                int? leaseAppId = sdpObj.lease_application_id;
                                if (leaseAppId.HasValue)
                                {
                                    leaseApp = LeaseApplication.SearchWithinTransaction(leaseAppId.Value, ce);
                                    leaseAppType = leaseApp.leaseapp_types;
                                    if (leaseAppType != null)
                                    {
                                        returnType = leaseAppType.lease_application_type;
                                    }
                                }
                            }
                        }

                        break;
                    default:
                        // do nothing
                        break;
                }
            }
            return returnType;
        }

        public static documents_links SearchDocumentsLinksByKey(string key)
        {
            using (CPTTEntities ce = new CPTTEntities())
            {
                var log = (from obj in ce.documents_links
                           where obj.trackit_docs_folder.Equals(key)
                           select obj).FirstOrDefault();

                return log;
            }
        }
        
        #region Get List link document by LeaseApplication

        public static List<string> GetListDocumentByApp(int id, eListDocumentType? ListDocumenType)
        {
            List<string> result = new List<string>();
            using( CPTTEntities ce = new CPTTEntities())
            {
                switch (ListDocumenType.Value)
                {
                    case DMSDocumentLinkHelper.eListDocumentType.Application:
                        var leaseapp = from doc in ce.documents_links
                                       where (((from la in ce.lease_applications
                                                where la.id == id
                                                select la.id).Contains(doc.record_id)
                                               ) && (doc.record_type == "lease_applications"))
                                               ||
                                               (((
                                                from sa in ce.structural_analyses
                                                join la1 in ce.lease_applications
                                                on sa.lease_application_id equals la1.id
                                                where la1.id == id
                                                select sa.id).Contains(doc.record_id)
                                               ) && (doc.record_type == "structural_analyses"))
                                               ||
                                               (((
                                                from sac in ce.structural_analysis_comments
                                                join sa in ce.structural_analyses
                                                on sac.structural_analysis_id equals sa.id
                                                join la in ce.lease_applications
                                                on sa.lease_application_id equals la.id
                                                where la.id == id
                                                select sac.id).Contains(doc.record_id)
                                               ) && (doc.record_type == "structural_analyses"))
                                               ||
                                               (((
                                                from lac in ce.leaseapp_comments
                                                join la in ce.lease_applications
                                                on lac.lease_application_id equals la.id
                                                where la.id == id
                                                select lac.id).Contains(doc.record_id)
                                               ) && (doc.record_type == "lease_applications"))
                                               ||
                                               (((
                                                from lav in ce.leaseapp_revisions
                                                join la in ce.lease_applications
                                                on lav.lease_application_id equals la.id
                                                where la.id == id
                                                select lav.id).Contains(doc.record_id)
                                               ) && (doc.record_type == "leaseapp_revision"))
                                       group doc.trackit_docs_folder by doc.trackit_docs_folder into g
                                       select new
                                       {
                                           key = g.Key
                                       };
                            foreach (var doc in leaseapp.ToList())
                            {
                               result.Add(HttpUtility.UrlDecode(doc.key));
                            }
                            
                        break;
                    case DMSDocumentLinkHelper.eListDocumentType.Customer:
                        lease_applications lease = LeaseApplication.Search(id);
                        if (lease.customer_id.HasValue)
                        {
                            var customer = (from doc in ce.documents_links
                                            where (((from la in ce.lease_applications
                                                     where (la.customer_id == lease.customer_id.Value
                                                           && la.site_uid == lease.site_uid)
                                                     select la.id).Contains(doc.record_id)
                                                    ) && (doc.record_type == "lease_applications"))
                                                    ||
                                                    (((
                                                     from sa in ce.structural_analyses
                                                     join la1 in ce.lease_applications
                                                     on sa.lease_application_id equals la1.id
                                                     where (la1.customer_id == lease.customer_id.Value
                                                            && la1.site_uid == lease.site_uid)
                                                     select sa.id).Contains(doc.record_id)
                                                    ) && (doc.record_type == "structural_analyses"))
                                                    ||
                                                    (((
                                                     from sac in ce.structural_analysis_comments
                                                     join sa in ce.structural_analyses
                                                     on sac.structural_analysis_id equals sa.id
                                                     join la in ce.lease_applications
                                                     on sa.lease_application_id equals la.id
                                                     where (la.customer_id == lease.customer_id.Value
                                                            && la.site_uid == lease.site_uid)
                                                     select sac.id).Contains(doc.record_id)
                                                    ) && (doc.record_type == "structural_analyses"))
                                                    ||
                                                    (((
                                                     from lac in ce.leaseapp_comments
                                                     join la in ce.lease_applications
                                                     on lac.lease_application_id equals la.id
                                                     where (la.customer_id == lease.customer_id.Value
                                                            && la.site_uid == lease.site_uid)
                                                     select lac.id).Contains(doc.record_id)
                                                    ) && (doc.record_type == "lease_applications"))
                                                    ||
                                                    (((
                                                     from lav in ce.leaseapp_revisions
                                                     join la in ce.lease_applications
                                                     on lav.lease_application_id equals la.id
                                                     where (la.customer_id == lease.customer_id.Value
                                                            && la.site_uid == lease.site_uid)
                                                     select lav.id).Contains(doc.record_id)
                                                    ) && (doc.record_type == "leaseapp_revision"))
                                                    ||
                                                    (((
                                                     from sdp in ce.site_data_packages
                                                     join la in ce.lease_applications
                                                     on sdp.lease_application_id equals la.id
                                                     where (la.customer_id == lease.customer_id.Value
                                                            && la.site_uid == lease.site_uid)
                                                     select sdp.id).Contains(doc.record_id)
                                                    ) && (doc.record_type == "site_data_packages"))
                                                    ||
                                                    (((
                                                    from la in ce.lease_applications
                                                    from tm in la.tower_modifications                                                    
                                                    where ( la.id == lease.id
                                                            && la.customer_id == lease.customer_id.Value
                                                            && la.site_uid == lease.site_uid)
                                                     select tm.id).Contains(doc.record_id)
                                                    ) && (doc.record_type == "tower_modification" ||
                                                          doc.record_type == "tower_modification_general"))
                                            group doc.trackit_docs_folder by doc.trackit_docs_folder into g
                                            select new
                                            {
                                                key = g.Key
                                            }).Union(
                                                from customerLink in ce.documents_customer_links
                                                    where customerLink.customer_id == lease.customer_id
                                                    group customerLink.trackit_docs_folder by customerLink.trackit_docs_folder into g
                                                    select new
                                                    {
                                                        key = g.Key
                                                    }).GroupBy( item => item.key);

                            foreach (var doc in customer.ToList())
                            {
                                result.Add(HttpUtility.UrlDecode(doc.Key));
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
            return result;
        }
        public static List<string[]> GetListDocumentByApp(List<int> id, eListDocumentType? ListDocumenType)
        {
            List<string[]> result = new List<string[]>();
            using (CPTTEntities ce = new CPTTEntities())
            {
                switch (ListDocumenType.Value)
                {
                    // Get List Site Data package in Document Status Tab
                    case eListDocumentType.SiteDataPackage:
                        List<String> lstDocumentStatus = (new[] { "AdditionalPrimeDocuments",
                                                                       "TowerDrawing_DocTypes",
                                                                       "TowerDrawing_DocStatus",
                                                                       "TowerTagPhoto_DocStatus",
                                                                       "StructuralCalcs_DocStatus",
                                                                       "StructuralAnalysis_DocStatus",
                                                                       "TowerErectionDetail_DocStatus",
                                                                       "FoundationDesign_DocStatus",
                                                                       "ConstructionDrawings_DocStatus",
                                                                       "GeotechReport_DocStatus",
                                                                       "BuildingPermit_DocStatus",
                                                                       "ZoningApproval_DocStatus",
                                                                       "PhaseI_DocStatus",
                                                                       "PhaseII_DocStatus",
                                                                       "NEPA_DocStatus",
                                                                       "SHPO_DocStatus",
                                                                       "GPS1a2c_DocStatus",
                                                                       "Airspace_DocStatus",
                                                                       "FAAStudyDet_DocStatus",
                                                                       "FCC_DocStatus",
                                                                       "AMCertification_DocStatus",
                                                                       "Towair_DocStatus",
                                                                       "PrimeLease_DocStatus",
                                                                       "Memorandum_DocStatus",
                                                                       "Title_DocStatus" }).ToList();


                        var sitedatapackage = from doc in ce.documents_links
                                              where (id.Contains(doc.record_id)) && (doc.record_type == "site_data_packages")
                                                    && (lstDocumentStatus.Contains(doc.linked_field))
                                              group doc by new {doc.trackit_docs_folder ,doc.record_id} into g
                                              select new
                                              {
                                                  key = g.Key.record_id,
                                                  value = g.Key.trackit_docs_folder  
                                              };

                        foreach (var doc in sitedatapackage.ToList())
                        {
                            result.Add(new string[] { doc.key.ToString(), HttpUtility.UrlDecode(doc.value).Replace('+','|') });
                        }
                        break;
                    default:
                        break;
                }
            }
            return result;
        }

        #endregion
    }
        
    public class Dms_Document
    {
        public Dms_Document()
        {
            this.type = String.Empty;
            this.ID = -1;
            this.documentName = String.Empty;
            this.ref_field = String.Empty;
            this.ref_key = String.Empty;
        }
        public Dms_Document(string _type, int _ID, string _field, string _key)
        {
            this.type = _type;
            this.ID = _ID;
            this.documentName = HttpUtility.UrlDecode(_key).Split('/')[HttpUtility.UrlDecode(_key).Split('/').Length - 1];
            this.ref_field = _field;
            this.ref_key = _key;
        }

        public string type { get; set; }
        public int ID { get; set; }
        public string documentName { get; set; }
        public string ref_field { get; set; }
        public string ref_key { get; set; }
    }

    public class Dms_Document_Site
    {
        public Dms_Document_Site()
        {
            this.type = String.Empty;
            this.ID = "";
            this.documentName = String.Empty;
            this.ref_field = String.Empty;
            this.ref_key = String.Empty;
        }
        public Dms_Document_Site(string _type, String  _ID, string _field, string _key)
        {
            this.type = _type;
            this.ID = _ID;
            this.documentName = HttpUtility.UrlDecode(_key).Split('/')[HttpUtility.UrlDecode(_key).Split('/').Length - 1];
            this.ref_field = _field;
            this.ref_key = _key;
        }

        public string type { get; set; }
        public String ID { get; set; }
        public string documentName { get; set; }
        public string ref_field { get; set; }
        public string ref_key { get; set; }
    }

    public class DmsLinkDocumentStructure
    {
        public DMSDocumentLinkHelper.eDocumentField Field { get; set; }

        // Integer Typed Getter/Setter for Field enum to be used with EF
        // queries.
        internal int FieldCode
        {
           get { return (int)this.Field; }
           set { this.Field = (DMSDocumentLinkHelper.eDocumentField)value; }
        }
        public DMSDocumentLinkHelper.eDocumentLocation? Location { get; set; }
        public DMSDocumentLinkHelper.eDocumentType Type { get; set; }

        // Integer Typed Getter/Setter for Type enum to be used with EF
        // queries.
        internal int TypeCode
        {
           get { return (int)this.Type; } 
           set { this.Type = (DMSDocumentLinkHelper.eDocumentType)value; }
        }

        public DMSDocumentLinkHelper.eDocumentField? RelatedField { get; set; }
        public bool MasterField { get; set; }
        public bool AutoRename { get; set; }
        public string siteID { get; set; }
        public DMSDocumentLinkHelper.eDocumentRenameType RenameType { get; set; }
        public DMSDocumentLinkHelper.eDocumentAction Action {get;set;}
        public string LabelType
        {
            get
            {
                String result = String.Empty;
                switch (this.Type)
                {
                    case DMSDocumentLinkHelper.eDocumentType.structural_analyses:
                        result = "Structural Analysis";
                        break;
                    case DMSDocumentLinkHelper.eDocumentType.lease_applications:
                        result = "Lease Application";
                        break;
                    case DMSDocumentLinkHelper.eDocumentType.leaseapp_revision:
                        result = "Lease Application Revision";
                        break;
                    case DMSDocumentLinkHelper.eDocumentType.site_data_packages:
                        result = "Site Data Package";
                        break;
                    case DMSDocumentLinkHelper.eDocumentType.sites:
                        result = "Site";
                        break;
                    case DMSDocumentLinkHelper.eDocumentType.issue_tracker:
                        result = "Issue Tracker";
                        break;
                    case DMSDocumentLinkHelper.eDocumentType.tower_modification:
                        result = "Tower Modification";
                        break;
                    case DMSDocumentLinkHelper.eDocumentType.tower_modification_general:
                        result = "Tower Modification";
                        break;
                    case DMSDocumentLinkHelper.eDocumentType.lease_admin:
                        result = "Lease Admin Tracker";
                        break;
                    default:
                        break;
                }
                return result;
            }
        }
        public String LabelField
        {
            get
            {
                string result = String.Empty;
                switch (this.Field)
                {
                    #region SA
                    case DMSDocumentLinkHelper.eDocumentField.Structural_Decision:
                        result = "Structural Decision";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.SAWToSAC:
                        result = "SAW To SAC";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.SAWRequest:
                        result = "SAW Received";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.SA_Received:
                        result = "Process Date Received";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.DelayedOrder_OnHold:
                        result = "Delayed Order Onhold";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.DelayedOrder_Cancelled:
                        result = "Delayed Order Cancelled";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.PurchaseOrder_Received:
                        result = "Purchase Order Received";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.SA_Comments:
                        result = "Structural Analysis Comment";
                        break;
                    #endregion SA

                    #region LeaseApp
                    #region PR
                    case DMSDocumentLinkHelper.eDocumentField.PreliminaryDecision:
                        result = "Preliminary Decision";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.SDPtoCustomer:
                        result = "SDP To Customer";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.PreliminarySitewalk:
                        result = "Preliminary Sitewalk";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.LandlordConsentType:
                        result = "Landlord Consent Type";
                        break;
                    #endregion

                    #region DR
                    case DMSDocumentLinkHelper.eDocumentField.LeaseExhibit_Received:
                        result = "Lease Exhibit Received";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.LeaseExhibit_Decision:
                        result = "Lease Exhibit Decision";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.LandlordConsent_LandlordNotice:
                        result = "Landlord Notice";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.LandlordConsentRequested:
                        result = "Landlord Consent Requested";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.LandlordConsentReceived:
                        result = "Landlord Consent Received";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.ConstructionDrawingsReceived:
                        result = "Construction Drawing Received";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.ConstructionDrawingsDecision:
                        result = "Construction Drawing Decision";
                        break;
                    #endregion

                    #region LE
                    case DMSDocumentLinkHelper.eDocumentField.ExecutablesSenttoCustomer:
                        result = "Executables Sent To Customer";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.ExecutablesToCrown:
                        result = "Executables To Crown";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.LOEHoldupReasonTypes_DocsBackfromCustomer:
                        result = "Docs Back From Customer";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.LeaseExecution:
                        result = "Lease Execution";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.AmendmentExecution:
                        result = "Amendment Execution";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.Financial_RentForecastAmount:
                        result = "Rent Forecast Amount";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.NTPtoCust:
                        result = "NTP To Customer";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.RegCommencementLetterDate:
                        result = "Reg Commencement Letter Created";
                        break;
                    #endregion

                    #region CO
                    case DMSDocumentLinkHelper.eDocumentField.Preconstruction_PreconstructionSitewalk:
                        result = "Preconstruction Sitewalk";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.NTPIssuedDateStatus:
                        result = "NTP Issued Date";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.EarlyInstalltion_ViolationLetterSent:
                        result = "Violation Letter Sent";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.EarlyInstallation_ViolationInvoiceCreatedByFinops:
                        result = "Violation Invoice Created By Finops";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.PostConstruction_Sitewalk:
                        result = "Post Construction Sitewalk";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.CloseOut_AsBuilt:
                        result = "Close Out As Built";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.MarketHandoff_Status:
                        result = "Market Handoff";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.CloseOut_FinalSiteApproval:
                        result = "Close Out Final Site Approval";
                        break;
                    #endregion
                      
                    #region LE Comments
                    case DMSDocumentLinkHelper.eDocumentField.LE_Comments:
                        result = "Lease Application Comment";
                        break;
                    #endregion
                    #endregion

                    #region Lease App Revision
                    case DMSDocumentLinkHelper.eDocumentField.RevisionReveivedDate:
                        result = "Revision Reveived Date";
                        break;
                    #endregion

                    #region SDP
                    #region General
                    case DMSDocumentLinkHelper.eDocumentField.SDPCheckList:
                        result = "SDP Checklist";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.SDPFirstNoticeToMarket:
                        result = "First Notice to Market";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.SDPSecondNoticeToMarket:
                        result = "Second Notice to Market";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.SDPThirdNoticeToMarket:
                        result = "Third Notice to Market";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.AdditionalPrimeDocuments:
                        result = "Additional Prime Documents";
                        break;
                    #endregion

                    #region Document Status
                    case DMSDocumentLinkHelper.eDocumentField.TowerDrawing_DocTypes:
                        result = "Tower Drawing Doc Types";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.TowerDrawing_DocStatus:
                        result = "Tower Drawing Doc Status";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.TowerTagPhoto_DocStatus:
                        result = "Tower Tag Photo Doc Status";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.StructuralCalcs_DocStatus:
                        result = "Structural Calcs Doc Status";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.StructuralAnalysis_DocStatus:
                        result = "Structural Analysis Current Doc Status";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.TowerErectionDetail_DocStatus:
                        result = "Tower Erection Detail Doc Status";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.FoundationDesign_DocStatus:
                        result = "Foundation Design Doc Status";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.ConstructionDrawings_DocStatus:
                        result = "Construction Drawings Doc Status";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.GeotechReport_DocStatus:
                        result = "Geotech Report Doc Status";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.BuildingPermit_DocStatus:
                        result = "Building Permit Doc Status";
                        break;

                    case DMSDocumentLinkHelper.eDocumentField.ZoningApproval_DocStatus:
                        result = "Zoning Approval Doc Status";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.PhaseI_DocStatus:
                        result = "Phase I Doc Status";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.PhaseII_DocStatus:
                        result = "Phase II Doc Status";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.NEPA_DocStatus:
                        result = "NEPA Doc Status";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.SHPO_DocStatus:
                        result = "SHPO Doc Status";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.GPS1a2c_DocStatus:
                        result = "GPS 1a2c Doc Status";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.Airspace_DocStatus:
                        result = "Airspace Doc Status";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.FAAStudyDet_DocStatus:
                        result = "FAA Study Determination Doc Status";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.FCC_DocStatus:
                        result = "FCC Doc Status";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.AMCertification_DocStatus:
                        result = "AMCertification Doc Status";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.Towair_DocStatus:
                        result = "Towair Document Status";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.PrimeLease_DocStatus:
                        result = "Prime Lease Doc Status";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.Memorandum_DocStatus:
                        result = "Memorandum of Lease Doc Status";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.Title_DocStatus:
                        result = "Title Doc Status";
                        break;
                    #endregion

                    #region Abstract
                    case DMSDocumentLinkHelper.eDocumentField.AbstractCompleted:
                        result = "Abstract Completed";
                        break;
                    #endregion

                    #region Comments
                    case DMSDocumentLinkHelper.eDocumentField.SDP_Comments:
                        result = "Site Data Package Comment";
                        break;
                    #endregion
                    #endregion 

                    #region Sites
                    case DMSDocumentLinkHelper.eDocumentField.RogueEquipment:
                        result = "Rogue Equipment";
                        break;
                    #endregion

                    #region Lease Admin 
                    case DMSDocumentLinkHelper.eDocumentField.LA_Comments:
                        result = "Lease Admin Comment";
                        break;
                    #endregion

                    #region Issue Tracker
                    case DMSDocumentLinkHelper.eDocumentField.Issue:
                        result = "Issue Tracker";
                        break;
                    #endregion

                    #region Issue Action Items
                    case DMSDocumentLinkHelper.eDocumentField.Issue_Action:
                        result = "Issue Comment";
                        break;
                    #endregion

                    #region Tower Modification
                    #region General
                    case DMSDocumentLinkHelper.eDocumentField.TowerModPacketSentDate:
                        result = "Packet Sent Date";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.BuildingPermitRcvdDate:
                        result = "Permit Received Date";
                        break;
                    #endregion

                    #region PO
                    case DMSDocumentLinkHelper.eDocumentField.EbidComment:
                        result = "eBid Comment";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.BidAmount:
                        result = "Bid Amount";
                        break;
                    #endregion

                    #region Comments
                    case DMSDocumentLinkHelper.eDocumentField.TMO_Comments:
                        result = "Tower Modification Comment";
                        break;
                    #endregion
                    #endregion

                    #region Lease Admin
                    case DMSDocumentLinkHelper.eDocumentField.LA_ExecutablesSenttoCustomer:
                        result = "Executables Sent To Customer";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.LA_LOEHoldupReasonTypes_DocsBackfromCustomer:
                        result = "Docs Back From Customer";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.LA_LeaseExecution:
                        result = "Lease Execution";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.LA_NTPtoCust:
                        result = "NTP To Cust";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.LA_RegCommencementLetterDate:
                        result = "Reg Commencement Letter Date";
                        break;
                    case DMSDocumentLinkHelper.eDocumentField.LA_RentForecastAmount:
                        result = "Rent Forecast Amount";
                        break;
                    #endregion
                    default:
                        break;
                }
                return result;
            }
        }

        public int RecordId { get; set; }
        public DateTime? Update_AT { get; set; }
        public string Id { get; set; }

        public string NewName { get; set; }
        public string RenameDescription { get; set; }
    }

    public class DmsWidgetDocument 
    {
        private DmsLinkDocumentStructure _document;
        public DmsLinkDocumentStructure document 
        {
            get 
            {
               return _document;
            }
            set 
            {
                _document = value;
            }
        }
        private bool _requireFiled;
        public bool requireFiled 
        {
            get 
            {
               return _requireFiled;
            }
            set 
            {
                _requireFiled = value;
            }
        }
        public DmsWidgetDocument(DmsLinkDocumentStructure objDocument ,bool isRequire)
        {
            _document = objDocument;
            _requireFiled = isRequire;
        }
    }
}
