﻿using System;

namespace TrackIT2.BLL
{
    /// <summary>
    /// File Size Format Provider
    /// </summary>
    public class FileSizeFormatProvider : IFormatProvider, ICustomFormatter
    {
        // Var
        public object GetFormat(Type formatType)
        {
            if (formatType == typeof(ICustomFormatter)) return this;
            return null;
        }
        //
        private const string FILE_SIZE_FORMAT = "fs";
        private const Decimal ONE_KILO_BYTE = 1024M;
        private const Decimal ONE_MEGA_BYTE = ONE_KILO_BYTE * 1024M;
        private const Decimal ONE_GIGA_BYTE = ONE_MEGA_BYTE * 1024M;
        private const string SUFFIX_BYTES = " bytes";
        //>

        /// <summary>
        /// Format
        /// </summary>
        /// <param name="format">string</param>
        /// <param name="arg">object</param>
        /// <param name="formatProvider">IFormatProvider</param>
        /// <returns>string</returns>
        public string Format(string format, object arg, IFormatProvider formatProvider)
        {
            if (format == null || !format.StartsWith(FILE_SIZE_FORMAT))
            {
                return DefaultFormat(format, arg, formatProvider);
            }

            if (arg is string)
            {
                return DefaultFormat(format, arg, formatProvider);
            }

            Decimal size;

            try
            {
                size = Convert.ToDecimal(arg);
            }
            catch (InvalidCastException)
            {
                return DefaultFormat(format, arg, formatProvider);
            }

            string suffix;
            if (size > ONE_GIGA_BYTE)
            {
                size /= ONE_GIGA_BYTE;
                suffix = " GB";
            }
            else if (size > ONE_MEGA_BYTE)
            {
                size /= ONE_MEGA_BYTE;
                suffix = " MB";
            }
            else if (size > ONE_KILO_BYTE)
            {
                size /= ONE_KILO_BYTE;
                suffix = " KB";
            }
            else
            {
                suffix = SUFFIX_BYTES;
            }

            string precision = format.Substring(2);
            if(suffix.Equals(SUFFIX_BYTES))
            {
                precision = "0";
            }else
            {
                if (String.IsNullOrEmpty(precision)) precision = "2";   
            }
            
            return String.Format("{0:N" + precision + "}{1}", size, suffix);

        }
        //-->

        /// <summary>
        /// Default Format
        /// </summary>
        /// <param name="format">string</param>
        /// <param name="arg">object</param>
        /// <param name="formatProvider">IFormatProvider</param>
        /// <returns>string</returns>
        private static string DefaultFormat(string format, object arg, IFormatProvider formatProvider)
        {
            var formattableArg = arg as IFormattable;
            if (formattableArg != null)
            {
                return formattableArg.ToString(format, formatProvider);
            }
            return arg.ToString();
        }
        //-->
    }
}