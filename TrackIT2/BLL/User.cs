﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using TrackIT2.DAL;

namespace TrackIT2.BLL
{
   public class User
   {
      public static void Add(string tmoUserId, string email, string firstName, 
                             string lastName, bool isActive, int jobId, 
                             int employeeType, int managerId, Guid aspnetId)
      {
         // TODO: Add additional security/business logic in here as needed.
         CPTTEntities ce = new CPTTEntities();
         user user = new user();

         if(!string.IsNullOrEmpty(tmoUserId))
         {
            user.tmo_userid = tmoUserId;  
         }
         user.email = email;
         user.first_name = firstName;
         user.last_name = lastName;
         user.active_emp_yn = isActive;
         user.job_role_id = jobId;
         user.emp_type_id = employeeType;
         user.dir_mgr_user_uid = managerId;
         user.password_salt = "none";
         user.encrypted_password = "none";
         user.aspnet_user_id = aspnetId;

         ce.users.AddObject(user);

         // Exceptions are thrown up to the calling method to handle. Elmah will log
         // unhandled execptions.
         try
         {
            ce.SaveChanges();
         }
         catch (EntitySqlException)
         {
            throw;
         }
      }

      public static void Add(user user)
      {
         // TODO: Add additional security/business logic in here as needed.
         using (CPTTEntities ce = new CPTTEntities())
         {
            HttpContext context = HttpContext.Current;

            ce.users.AddObject(user);

            // Exceptions are thrown up to the calling method to handle. Elmah will log
            // unhandled execptions.
            ce.SaveChanges();
         }

      }

      public static void Update(user user, string tmoUserId, string email, 
                                string firstName, string lastName, 
                                bool isActive, int jobId,
                                int employeeType, int managerId)
      {
         // TODO: Add additional security/business logic in here as needed.
         CPTTEntities ce = new CPTTEntities();      

         ce.users.Attach(user);
         if (!string.IsNullOrEmpty(tmoUserId))
         {
            user.tmo_userid = tmoUserId;
         }
         user.email = email;
         user.first_name = firstName;
         user.last_name = lastName;
         user.active_emp_yn = isActive;
         user.job_role_id = jobId;
         user.emp_type_id = employeeType;
         user.dir_mgr_user_uid = managerId;

         // Exceptions are thrown up to the calling method to handle. Elmah will log
         // unhandled execptions.
         try
         {
            ce.SaveChanges();
         }
         catch (OptimisticConcurrencyException)
         {
            throw;
         }
         catch (EntitySqlException)
         {
            throw;
         }
      }

      public static void Delete(int id)
      {
         // TODO: Add additional security/business logic in here as needed.
         CPTTEntities ce = new CPTTEntities();

         user user;

         user = ce.users.Where(u => u.id.Equals(id)).First();
         ce.DeleteObject(user);
         ce.SaveChanges();
      }

      public static user Search(int id)
      {
          user ret = null;
          using (CPTTEntities ce = new CPTTEntities())
          {
              ret = ce.users.Where(u => u.id.Equals(id)).First();
          }
          return ret;
      }
      public static List<UserInfo> GetUserInfo(int? id = null)
      {
          List<UserInfo> lstUserInfo = new List<UserInfo>();
          using (CPTTEntities ce = new CPTTEntities())
          {
              if (id.HasValue)
              {
                  //Get Single.
                  var users = (from u in ce.users
                              where u.id == id.Value
                              select new UserInfo
                              {
                                  ID = u.id,
                                  UserUID = u.tmo_userid,
                                  FirstName = u.first_name,
                                  LastName = u.last_name
                              }).ToList();
                  if (users.Count > 0)
                  {
                      lstUserInfo = users;
                  }
              }
              else
              {
                  //Get all.
                  var users = (from u in ce.users
                              orderby u.last_name
                              select new UserInfo
                              {
                                  ID = u.id,
                                  UserUID = u.tmo_userid,
                                  FirstName = u.first_name,
                                  LastName = u.last_name
                              }).ToList();
                  if (users.Count > 0)
                  {
                      lstUserInfo = users;
                  }
              }
          }
          return lstUserInfo;
      }

      public static String GetUserAvatarImage(String sUsername)
      {
         String avatarDefault = "~/images/avatars/default.png";
         String ret = avatarDefault;

         if (!string.IsNullOrEmpty(sUsername))
         {
          HttpContext currentContext = HttpContext.Current;
          String avatarPng = currentContext.Server.MapPath("~/images/avatars/" + sUsername.ToLower() + ".png");
          String avatarJpg = currentContext.Server.MapPath("~/images/avatars/" + sUsername.ToLower() + ".jpg");          
          try
          {
              if (System.IO.File.Exists(avatarPng))
              {
                 ret = "~/images/avatars/" + sUsername.ToLower() + ".png";
              }
              else if (System.IO.File.Exists(avatarJpg))
              {
                 ret = "~/images/avatars/" + sUsername.ToLower() + ".jpg";
              }
          }
          catch (Exception)
          {
                //Do nothing and return default image.
          }
         }
          return ret;
      }

   }

    /// <summary>
    /// User Info
    /// </summary>
    public class UserInfo
    {
        public UserInfo() { }

        public UserInfo(user user)
        {
            if (user == null) return;
            ID = user.id;
            UserUID = user.tmo_userid;
            FirstName = user.first_name;
            LastName = user.last_name;
        }

        public int ID { get; set; }
        public string UserUID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName 
        {
            get
            {
                string fullName = "";
                if (!string.IsNullOrEmpty(LastName)) fullName += LastName;
                if ((!string.IsNullOrEmpty(fullName)) && (!string.IsNullOrEmpty(FirstName))) fullName += ", ";
                if (!string.IsNullOrEmpty(FirstName)) fullName += FirstName;
                return fullName;
            }
        }

        public string FirstFullName
        {
            get
            {
                string fullName = "";
                if (!string.IsNullOrEmpty(FirstName)) fullName += FirstName;
                if ((!string.IsNullOrEmpty(fullName)) && (!string.IsNullOrEmpty(LastName))) fullName += ", ";
                if (!string.IsNullOrEmpty(LastName)) fullName += LastName;
                return fullName;
            }
        }

    }
    //-->
}