﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrackIT2.BLL
{
    public class DmsMetaData
    {
        public String key { set; get; }
        public String fileName { set; get;}
        public String classification { set; get;}
        public String description { set; get; }
        public String Keyword { set; get; }
        public String Size { set; get;}
        public String uploadedBy { set; get;}
        public int Revison { set; get; }
        public String uploadDate { set; get; }
        public bool checkedInOutButton { set; get;}
        public String actionHtml { set; get;}
        public string title { set; get; }
        public DmsMetaData(String _key, String _fileName, String _classification, String _keyword,
                           String _description,String _Size,
                           String _uploadedBy, int _revison,
                           String _uploadDate, bool _checkedInOutButton, String _actionHtml, String _title)
        { 
            key = _key;
            fileName = _fileName;
            classification = _classification;
            description = _description;
            Keyword = _keyword;
            Size = _Size;
            uploadedBy = _uploadedBy;
            Revison = _revison;
            uploadDate = _uploadDate;
            checkedInOutButton = _checkedInOutButton;
            actionHtml = _actionHtml;
            title = _title;
        }
    }
}