﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TrackIT2.DAL;

namespace TrackIT2.BLL
{
    public static class Customer
    {
        public static customer Search(int? customer_id)
        {
            customer ret = null;
            
           if (customer_id.HasValue)
            {
                using (CPTTEntities ce = new CPTTEntities())
                {

                    ret = ce.customers.Where(c => c.id.Equals(customer_id.Value)).FirstOrDefault();

                }
            }

            return ret;
        }

        public static customer SearchCustomerName(string customerName)
        {
            customer ret = null;
            using (CPTTEntities ce = new CPTTEntities())
            {
                ret = ce.customers.Where(c => customerName.Contains(c.customer_name)).FirstOrDefault();
            }

            return ret;
        }

        public static customer SearchWithinTransaction(int? customer_id, CPTTEntities ceRef)
        {
           customer ret = null;
           
           if (customer_id.HasValue)
           {
               ret = ceRef.customers.Where(c => c.id.Equals(customer_id.Value)).FirstOrDefault();
           }

           return ret;
        }
    }
}