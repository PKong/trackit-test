﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using TrackIT2.DAL;
using System.Transactions;
using TrackIT2.Objects;

namespace TrackIT2.BLL
{
    public static class SiteDataPackage
    {
        public static site_data_packages Search(int id)
        {
            site_data_packages results = null;

            using (var ce = new CPTTEntities())
            {
               if (ce.site_data_packages.Any(sdp => sdp.id.Equals(id)))
               {
                   results = ce.site_data_packages
                          .Where(la => la.id.Equals(id))
                          .FirstOrDefault();
               }
               else
               {
                   results = null;
               }
            }

            return results;
        }

        public static site_data_packages SearchWithinTransaction(int id, CPTTEntities ceRef)
        {
           site_data_packages results = null;

           if (ceRef.site_data_packages.Any(sdp => sdp.id.Equals(id)))
           {
               results = ceRef.site_data_packages
                      .Where(la => la.id.Equals(id))
                      .FirstOrDefault();
           }

           return results;
        }


        public static List<site_data_packages> SearchByTMPAppID(String TMO_AppID)
        {
            List<site_data_packages> ret = new List<site_data_packages>();

            using (var ce = new CPTTEntities())
            {
                var obj = from sdp in ce.site_data_packages
                          join app in ce.lease_applications
                          on sdp.lease_application_id equals app.id
                          where app.TMO_AppID.Equals(TMO_AppID)
                          select sdp;
                if (obj.Any())
                {
                    ret = obj.ToList<site_data_packages>();
                }
                
            }
            return ret;
        }

        public static string Add(site_data_packages package)
        {
           String results = null;
           bool isSuccess = false;

           // Start transaction for add.
           using (CPTTEntities ce = new CPTTEntities())
           {
              try
              {
                 using (TransactionScope trans = TransactionUtils.CreateTransactionScope())
                 {
                    // Manually open connection to prevent EF from auto closing
                    // connection and causing issues with future queries.
                    ce.Connection.Open();

                    ce.site_data_packages.AddObject(package);
                    ce.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                    isSuccess = true;
                    trans.Complete();     // Transaction complete
                 }
              }
              catch (Exception ex)
              {
                 // Log error in ELMAH for any diagnostic needs.
                 Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                 isSuccess = false;
                 results = "An error occurred saving the record. Please try again.";
              }
              finally
              {
                 ce.Connection.Dispose();
              }

              // Finally accept all changes from the transaction.
              if (isSuccess)
              {
                 ce.AcceptAllChanges();
              }
           }

           return results;
        }

        public static string Update(site_data_packages sdp, List<sdp_tower_drawings> towerDrawings)
        {
           String results = null;
           bool isSuccess = false;

           // Start transaction for update.
           using (CPTTEntities ce = new CPTTEntities())
           {
              try
              {
                 using (TransactionScope trans = TransactionUtils.CreateTransactionScope())
                 {
                    // Manually open connection to prevent EF from auto closing
                    // connection and causing issues with future queries.
                    ce.Connection.Open();

                    // A null tower drawing list indicates no tower drawings
                    // were processed (when creating a new tower drawing or 
                    // modifying package info in the popup window). In that
                    // case we ignore processing tower drawings.
                    if (towerDrawings != null)
                    {
                       // Since there is no "update" to a tower drawing record,
                       // we remove all records associated with the site data
                       // package and then add any found in the list.
                       results = SDPDrawingTypes.deleteAllWithinTransaction(sdp.id, ce);

                       // If an error occurred removing tower drawings, throw
                       // an exception to break the loop.
                       if (!String.IsNullOrEmpty(results))
                       {
                          throw new Exception(results);
                       }

                       if (towerDrawings.Count > 0)
                       {
                          results = SDPDrawingTypes.AddWithinTransaction(towerDrawings, sdp.id, ce);

                          // If an error occurred adding tower drawings, throw
                          // an exception to break the loop.
                          if (!String.IsNullOrEmpty(results))
                          {
                             throw new Exception(results);
                          }
                       }
                    }                    

                    // Update lease application.
                    ce.AttachUpdated(sdp);

                    // Only add version record if modifications were detected
                    // for that entity.
                    if (ce.ObjectStateManager.GetObjectStateEntry(sdp.EntityKey).State == EntityState.Modified)
                    {
                       ce.site_data_package_versions.AddObject(SiteDataPackage.ToVersionFromEntityOriginal(ce, sdp));
                    }
                                        
                    ce.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                    isSuccess = true;
                    trans.Complete();     // Transaction complete
                 }
              }
              catch (OptimisticConcurrencyException oex)
              {
                 // Log error in ELMAH for any diagnostic needs.
                 Elmah.ErrorSignal.FromCurrentContext().Raise(oex);

                 isSuccess = false;
                 results = "The current record has been modifed since you have " +
                           "last retrieved it. Please reload the record and " + 
                           "attempt to save it again.";
              }
              catch (Exception ex)
              {
                 // Log error in ELMAH for any diagnostic needs.
                 Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                 isSuccess = false;
                 results = "An error occurred saving the record. Please try again.";
              }
              finally
              {
                 ce.Connection.Dispose();
              }

              // Finally accept all changes from the transaction.
              if (isSuccess)
              {
                 ce.AcceptAllChanges();
              }
           }

           return results;            
        }

        public static string UpdatePublishDocumentStatus(int id,bool publishDocument)
        {
            String results = null;
            bool isSuccess = false;

            // Start transaction for update.
            using (CPTTEntities ce = new CPTTEntities())
            {
                try
                {
                    using (TransactionScope trans = TransactionUtils.CreateTransactionScope())
                    {
                        // Manually open connection to prevent EF from auto closing
                        // connection and causing issues with future queries.
                        ce.Connection.Open();

                        // A null tower drawing list indicates no tower drawings
                        // were processed (when creating a new tower drawing or 
                        // modifying package info in the popup window). In that
                        // case we ignore processing tower drawings.
                        site_data_packages sdp = (from obj in ce.site_data_packages
                                                  where obj.id == id
                                                  select obj).FirstOrDefault();
                        sdp.publish_document = publishDocument;
                        ce.AttachUpdated(sdp);

                        ce.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                        isSuccess = true;
                        trans.Complete();     // Transaction complete
                    }
                }
                catch (OptimisticConcurrencyException oex)
                {
                    // Log error in ELMAH for any diagnostic needs.
                    Elmah.ErrorSignal.FromCurrentContext().Raise(oex);

                    isSuccess = false;
                    results = "The current record has been modifed since you have " +
                              "last retrieved it. Please reload the record and " +
                              "attempt to save it again.";
                }
                catch (Exception ex)
                {
                    // Log error in ELMAH for any diagnostic needs.
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                    isSuccess = false;
                    results = "An error occurred saving the record. Please try again.";
                }
                finally
                {
                    ce.Connection.Dispose();
                }

                // Finally accept all changes from the transaction.
                if (isSuccess)
                {
                    ce.AcceptAllChanges();
                }
            }

            return results;
        }


        public static String Delete(int id)
        {
           String results = null;
           bool isSuccess = false;
            
           // Start transaction for delete.
           using (CPTTEntities ce = new CPTTEntities())
           {
             try
             {
                using (TransactionScope trans = TransactionUtils.CreateTransactionScope())
                {
                   // Manually open connection to prevent EF from auto closing
                   // connection and causing issues with future queries.
                   ce.Connection.Open();
                      
                   // Checking for version
                   var queryVer = from sdp_ver in ce.site_data_package_versions
                                  where sdp_ver.site_data_package_id == id
                                  select sdp_ver;
                   foreach (site_data_package_versions item in queryVer.ToList<site_data_package_versions>())
                   {
                     ce.DeleteObject(item);
                   }

                   //Remove any related sdp document records
                   var queryDocument = from sdp_doc in ce.documents_links
                                       where sdp_doc.record_id == id
                                       select sdp_doc;

                   if (queryDocument.Count() > 0)
                   {
                       foreach (documents_links item in queryDocument.ToList<documents_links>())
                       {
                           ce.DeleteObject(item);
                       }
                   }

                   // Delete comment
                   var queryComment = from sdp in ce.sdp_comments
                                  where sdp.site_data_package_id == id
                                  select sdp;


                   if (queryComment.Count() > 0)
                   {
                       foreach (sdp_comments item in queryComment.ToList<sdp_comments>())
                       {
                           ce.DeleteObject(item);
                       }
                   }

                   // Delete record
                   var querySDP = from sdp in ce.site_data_packages
                                  where sdp.id == id
                                  select sdp;
                   if (querySDP.ToList().Count > 0)
                   {
                        site_data_packages item = querySDP.ToList<site_data_packages>()[0];
                        ce.DeleteObject(item);
                   }

                   // Change DocLog
                   DocumentsChangeLog.UpdateIsLinkRecordFlag(id, DMSDocumentLinkHelper.eDocumentType.site_data_packages.ToString(), false, ce);

                   ce.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                   isSuccess = true;
                   trans.Complete();     // Transaction complete
                 }
                }
                catch (Exception ex)
                {
                   // Error occurs
                   // Log error in ELMAH for any diagnostic needs.
                   Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                   
                   isSuccess = false; 
                   results = ex.ToString();
                }
                finally
                {
                   ce.Connection.Dispose();
                }

                // Finally accept all changes from the transaction.
                if (isSuccess)
                {
                   ce.AcceptAllChanges();
                }
            }

            return results;
        }

        /// <summary>
        ///    Creates a version of an existing site data pakcage record by
        ///    using the entity context's original values.
        /// </summary>
        /// <param name="context">Context in which entity resides.</param>
        /// <param name="sdp">The site data package entity to version.</param>
        /// <returns>Site Data Package Version object.</returns>
        public static site_data_package_versions ToVersionFromEntityOriginal(CPTTEntities context, site_data_packages sdp)
        {
           site_data_package_versions results = new site_data_package_versions();
           site_data_packages origSDP = new site_data_packages();
           ObjectStateEntry objState = context.ObjectStateManager.GetObjectStateEntry(sdp.EntityKey);
           DbDataRecord origSDPRecord = objState.OriginalValues;
           List<string> origRecordFields = new List<string>();

           // Build list of property names for validation check later.
           for(int c = 0; c <= origSDPRecord.FieldCount - 1; c++)
           {
               if (!origRecordFields.Contains(origSDPRecord.GetName(c)))
               {
                  origRecordFields.Add(origSDPRecord.GetName(c));
               }
           }

           origSDP.id = sdp.id;

           // Use reflection to get all modifiyable properties
           var srcProperties = from p in typeof(site_data_packages).GetProperties()
                               where p.CanRead &&
                                     p.CanWrite &&
                                     p.Name != "id" &&
                                     p.Name != "EntityKey"
                               select p;

           // Iterate through all properites and assign them to the version copy.
           foreach (var property in srcProperties)
           {
              // Some properties in the entity object are foreign keys, and will
              // not have a record in the original values list.
              if (origRecordFields.Contains(property.Name))
              {
                 var currValue = origSDPRecord[property.Name];

                 if (currValue != null && currValue.GetType().Name != "DBNull")
                 {
                    PropertyInfo destProperty = typeof(site_data_packages)
                                                       .GetProperty(property.Name);
                    if (destProperty != null)  //Checking for null destination properties.
                    {
                       //Checking for EntityReference to not create the existing reference again.
                       if (currValue.GetType().BaseType.Name == "EntityReference")
                       {
                          //Simply copy the EntityKey to the new entity object’s EntityReference
                          ((EntityReference)destProperty.GetValue(results, null)).EntityKey =
                              ((EntityReference)currValue).EntityKey;
                       }
                       else
                       {
                          //Common task, just copy the simple type property into the new entity object
                          destProperty.SetValue(origSDP, currValue, null);
                       }
                    }
                 }
              }
           }

           results = ToSDPVersion(origSDP);
           return results;
        }

        /// <summary>
        /// Clones the lease application into a lease_application_version
        /// </summary>
        /// <param name="sdp">The current lease application.</param>
        /// <returns>Lease Application Version object with all values.</returns>
        public static site_data_package_versions ToSDPVersion
                                                 (site_data_packages sdp)
        {
            site_data_package_versions results = new site_data_package_versions();

            // Use reflection to get all modifiyable properties
            var srcProperties = from p in typeof(site_data_packages).GetProperties()
                                where p.CanRead &&
                                      p.CanWrite &&
                                      p.Name != "id" &&
                                      p.Name != "EntityKey"
                                select p;

            results.site_data_package_id = sdp.id;

            // Iterate through all properites and assign them to the version copy.
            foreach (var property in srcProperties)
            {
                var currValue = property.GetValue(sdp, null);

                if (currValue != null)
                {
                    PropertyInfo destProperty = typeof(site_data_package_versions)
                                                       .GetProperty(property.Name);
                    if (destProperty != null)   //Checking for null destination properties.
                    {
                        //Checking for EntityReference to not create the existing reference again.
                        if (currValue.GetType().BaseType.Name == "EntityReference")
                        {
                            //Simply copy the EntityKey to the new entity object’s EntityReference
                            ((EntityReference)destProperty.GetValue(results, null)).EntityKey =
                                ((EntityReference)currValue).EntityKey;
                        }
                        else
                        {
                            //Common task, just copy the simple type property into the new entity object
                            destProperty.SetValue(results, currValue, null);
                        }
                    }

                }
            }

            return results;
        }

        public static List<SDPRelatedLeaseApplication> GetRelatedLeaseApplication(int? la_id, int? sdp_id)
        {
            List<SDPRelatedLeaseApplication> ret = null;
            try
            {
                using (CPTTEntities ce = new CPTTEntities())
                {
                    IQueryable<SDPRelatedLeaseApplication> querySDPRelated = null;

                    if (la_id.HasValue)
                    {
                        querySDPRelated = from la in ce.lease_applications
                                          where la.id == la_id.Value
                                          select new SDPRelatedLeaseApplication
                                          {
                                              SiteID = la.site_uid,
                                              CustomerInfo = la.customer,
                                              LAReceived = la.leaseapp_rcvd_date,
                                              LAID = la.id
                                          };
                    }
                    else if (sdp_id.HasValue)
                    {
                        querySDPRelated = from sdp in ce.site_data_packages
                                          from la in ce.lease_applications
                                          where sdp.id == sdp_id.Value && sdp.lease_application_id == la.id 
                                          select new SDPRelatedLeaseApplication
                                          {
                                              SiteID = la.site_uid,
                                              CustomerInfo = la.customer,
                                              LAReceived = la.leaseapp_rcvd_date,
                                              LAID = la.id
                                          };
                    }

                    if (querySDPRelated.ToList().Count > 0)
                    {
                        ret = new List<SDPRelatedLeaseApplication>();
                        ret.AddRange(querySDPRelated.ToList<SDPRelatedLeaseApplication>());
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ret;
        }

        // All site search criteria in a dictionary for easy lookup/manipulation
        // when dynamically building the query.
        public static Dictionary<string, SearchParameter> GetSearchCriteria()
        {
           var results = new Dictionary<string, SearchParameter>
            {
                {
                  "General", new SearchParameter
                     {                        
                        VariableName = "General",
                        ParameterType = typeof(string),
                        WhereClause = "(it.site_uid LIKE {0})",
                        IsWildcardSearch = true
                     }
               },
               {
                  "SiteID", new SearchParameter
                     {                        
                        VariableName = "site_uid",
                        ParameterType = typeof(string),
                        WhereClause = "(it.site_uid LIKE {0})",
                        IsWildcardSearch = true
                     }
               },
               {
                  "SiteRegionName", new SearchParameter
                     {                        
                        VariableName = "SiteRegionName",
                        ParameterType = typeof(string),
                        WhereClause = "(it.site.region_name = {0})",
                        AllowsMultipleValues = true
                     }
               },
               {
                  "SiteMarketCode", new SearchParameter
                     {
                        VariableName = "SiteMarketCode",
                        ParameterType = typeof(string),
                        WhereClause = "(it.site.market_code = {0})",
                        AllowsMultipleValues = true
                     }
               },
               {
                  "SDPPriority", new SearchParameter
                     {
                        VariableName = "SDPPriority",
                        ParameterType = typeof(int),
                        WhereClause = "(it.sdp_priority_id = {0})",
                        AllowsMultipleValues = true
                     }
               },
               {
                  "SDPRequestType", new SearchParameter
                     {
                        VariableName = "SDPRequestType",
                        ParameterType = typeof(int),
                        WhereClause = "(it.sdp_request_type_id = {0})",
                        AllowsMultipleValues = true
                     }
               },
               {
                  "SDPSpecialProjectType", new SearchParameter
                     {
                        VariableName = "SDPSpecialProjectType",
                        ParameterType = typeof(int),
                        WhereClause = "(it.sdp_special_project_id = {0})",
                        AllowsMultipleValues = true
                     }
               },
               {
                  "SDPTMOPMEmp", new SearchParameter
                     {
                        VariableName = "SDPTMOPMEmp",
                        ParameterType = typeof(int),
                        WhereClause = "(it.tmo_pm_emp_id = {0})",
                        AllowsMultipleValues = true
                     }
               },
               {
                  "SDPTMOSpecialistCurrentEmp", new SearchParameter
                     {
                        VariableName = "SDPTMOSpecialistCurrentEmp",
                        ParameterType = typeof(int),
                        WhereClause = "((it.tmo_specialist_current_emp_id = {0}) || (it.lease_applications.user_lease_applications_tmo_specialist_current.id = {0}))",
                        AllowsMultipleValues = true
                     }
               },
               {
                  "SiteOnAirDateFrom", new SearchParameter
                     {
                        VariableName = "SiteOnAirDateFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.site.site_on_air_date >= {0})"
                     }
               },
               {
                  "SiteOnAirDateTo", new SearchParameter
                     {
                        VariableName = "SiteOnAirDateTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.site.site_on_air_date <= {0})"
                     }
               },
               {
                  "SiteOnAirDateRange", new SearchParameter
                     {
                        VariableName = "SiteOnAirDateRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.site.site_on_air_date >= {0} AND it.site.site_on_air_date <= {1})",
                        IsRangeSearch = true
                     }
               },
               {
                  "SDPRequestDateFrom", new SearchParameter
                     {
                        VariableName = "SDPRequestDateFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.sdp_request_date >= {0})"
                     }
               },
               {
                  "SDPRequestDateTo", new SearchParameter
                     {
                        VariableName = "SDPRequestDateTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.sdp_request_date <= {0})"
                     }
               },
               {
                  "SDPRequestDateRange", new SearchParameter
                     {
                        VariableName = "SDPRequestDateRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.sdp_request_date >= {0} AND it.sdp_request_date <= {1})",
                        IsRangeSearch = true
                     }
               },
               {
                  "SDPRequestFrom", new SearchParameter
                     {
                        VariableName = "SDPRequestDateFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.sdp_request_date >= {0})"
                     }
               },
               {
                  "SDPRequestTo", new SearchParameter
                     {
                        VariableName = "SDPRequestDateTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.sdp_request_date <= {0})"
                     }
               },
               {
                  "SDPRequestRange", new SearchParameter
                     {
                        VariableName = "SDPRequestDateRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.sdp_request_date >= {0} AND it.sdp_request_date <= {1})",
                        IsRangeSearch = true
                     }
               },
               {
                  "SDPDueByFrom", new SearchParameter
                     {
                        VariableName = "SDPDueByFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.sdp_due_by_date >= {0})"
                     }
               },
               {
                  "SDPDueByTo", new SearchParameter
                     {
                        VariableName = "SDPDueByTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.sdp_due_by_date <= {0})"
                     }
               },
               {
                  "SDPDueByRange", new SearchParameter
                     {
                        VariableName = "SDPDueByRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.sdp_due_by_date >= {0} AND it.sdp_due_by_date <= {1})",
                        IsRangeSearch = true
                     }
               },
               {
                  "SDPDueDateNotes", new SearchParameter
                     {
                        VariableName = "SDPDueDateNotes",
                        ParameterType = typeof(string),
                        WhereClause = "(it.sdp_due_date_notes LIKE {0})",
                        IsWildcardSearch = true
                     }
               },
               {
                  "FirstSweepFrom", new SearchParameter
                     {
                        VariableName = "FirstSweepFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.first_sweep_completed_date >= {0})"
                     }
               },
               {
                  "FirstSweepTo", new SearchParameter
                     {
                        VariableName = "FirstSweepTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.first_sweep_completed_date <= {0})"
                     }
               },
               {
                  "FirstSweepRange", new SearchParameter
                     {
                        VariableName = "FirstSweepRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.first_sweep_completed_date >= {0} AND it.first_sweep_completed_date <= {1})",
                        IsRangeSearch = true
                     }
               },
               {
                  "NoticeOfCompletionFrom", new SearchParameter
                     {
                        VariableName = "NoticeOfCompletionFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.sdp_notice_of_completion_date >= {0})"
                     }
               },
               {
                  "NoticeOfCompletionTo", new SearchParameter
                     {
                        VariableName = "NoticeOfCompletionTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.sdp_notice_of_completion_date <= {0})"
                     }
               },
               {
                  "NoticeOfCompletionRange", new SearchParameter
                     {
                        VariableName = "NoticeOfCompletionRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.sdp_notice_of_completion_date >= {0} AND it.sdp_notice_of_completion_date <= {1})",
                        IsRangeSearch = true
                     }
               },
               {
                  "SDPCompleteFrom", new SearchParameter
                     {
                        VariableName = "SDPCompleteFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.sdp_completed_date >= {0})"
                     }
               },
               {
                  "SDPCompleteTo", new SearchParameter
                     {
                        VariableName = "SDPCompleteTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.sdp_completed_date <= {0})"
                     }
               },
               {
                  "SDPCompleteRange", new SearchParameter
                     {
                        VariableName = "SDPCompleteRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.sdp_completed_date >= {0} AND it.sdp_completed_date <= {1})",
                        IsRangeSearch = true
                     }
               },
               {
                  "FirstMarketFrom", new SearchParameter
                     {
                        VariableName = "FirstMarketFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.first_notice_to_market_date >= {0})"
                     }
               },
               {
                  "FirstMarketTo", new SearchParameter
                     {
                        VariableName = "FirstMarketTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.first_notice_to_market_date <= {0})"
                     }
               },
               {
                  "FirstMarketRange", new SearchParameter
                     {
                        VariableName = "FirstMarketRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.first_notice_to_market_date >= {0} AND it.first_notice_to_market_date <= {1})",
                        IsRangeSearch = true
                     }
               },
               {
                  "SecondMarketFrom", new SearchParameter
                     {
                        VariableName = "SecondMarketFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.second_notice_to_market_date >= {0})"
                     }
               },
               {
                  "SecondMarketTo", new SearchParameter
                     {
                        VariableName = "SecondMarketTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.second_notice_to_market_date <= {0})"
                     }
               },
               {
                  "SecondMarketRange", new SearchParameter
                     {
                        VariableName = "SecondMarketRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.second_notice_to_market_date >= {0} AND it.second_notice_to_market_date <= {1})",
                        IsRangeSearch = true
                     }
               },
               {
                  "ThirdMarketFrom", new SearchParameter
                     {
                        VariableName = "ThirdMarketFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.third_notice_to_market_date >= {0})"
                     }
               },
               {
                  "ThirdMarketTo", new SearchParameter
                     {
                        VariableName = "ThirdMarketTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.third_notice_to_market_date <= {0})"
                     }
               },
               {
                  "ThirdMarketRange", new SearchParameter
                     {
                        VariableName = "ThirdMarketRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.third_notice_to_market_date >= {0} AND it.third_notice_to_market_date <= {1})",
                        IsRangeSearch = true
                     }
               },
               {
                  "EscalatedByEMP", new SearchParameter
                     {
                        VariableName = "EscalatedByEMP",
                        ParameterType = typeof(int),
                        WhereClause = "(it.escalated_by_emp_id = {0})"
                     }
               },
               {
                  "EscalationFrom", new SearchParameter
                     {
                        VariableName = "EscalationFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.escalation_date >= {0})"
                     }
               },
               {
                  "EscalationTo", new SearchParameter
                     {
                        VariableName = "EscalationTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.escalation_date <= {0})"
                     }
               },
               {
                  "EscalationRange", new SearchParameter
                     {
                        VariableName = "EscalationRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.escalation_date >= {0} AND it.escalation_date <= {1})",
                        IsRangeSearch = true
                     }
               },
               {
                  "EscalationCompleteByEMP", new SearchParameter
                     {
                        VariableName = "EscalationCompleteByEMP",
                        ParameterType = typeof(int),
                        WhereClause = "(it.escalation_completed_by_emp_id = {0})"
                     }
               },
               {
                  "EscalationCompleteFrom", new SearchParameter
                     {
                        VariableName = "EscalationCompleteFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.escalation_completed_date >= {0})"
                     }
               },
               {
                  "EscalationCompleteTo", new SearchParameter
                     {
                        VariableName = "EscalationCompleteTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.escalation_completed_date <= {0})"
                     }
               },
               {
                  "EscalationCompleteRange", new SearchParameter
                     {
                        VariableName = "EscalationCompleteRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.escalation_completed_date >= {0} AND it.escalation_completed_date <= {1})",
                        IsRangeSearch = true
                     }
               },
               {
                  "VendorOrderReceivedFrom", new SearchParameter
                     {
                        VariableName = "VendorOrderReceivedFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.po_for_sa_ordered_date >= {0})"
                     }
               },
               {
                  "VendorOrderReceivedTo", new SearchParameter
                     {
                        VariableName = "VendorOrderReceivedTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.po_for_sa_ordered_date <= {0})"
                     }
               },
               {
                  "VendorOrderReceivedRange", new SearchParameter
                     {
                        VariableName = "VendorOrderReceivedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.po_for_sa_ordered_date >= {0} AND it.po_for_sa_ordered_date <= {1})",
                        IsRangeSearch = true
                     }
               },
               {
                  "VendorOrderPOReceivedFrom", new SearchParameter
                     {
                        VariableName = "VendorOrderPOReceivedFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.po_for_sa_rcvd_date >= {0})"
                     }
               },
               {
                  "VendorOrderPOReceivedTo", new SearchParameter
                     {
                        VariableName = "VendorOrderPOReceivedTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.po_for_sa_rcvd_date <= {0})"
                     }
               },
               {
                  "VendorOrderPOReceivedRange", new SearchParameter
                     {
                        VariableName = "VendorOrderPOReceivedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.po_for_sa_rcvd_date >= {0} AND it.po_for_sa_rcvd_date <= {1})",
                        IsRangeSearch = true
                     }
               },
               {
                  "CostApprovalFrom", new SearchParameter
                     {
                        VariableName = "CostApprovalFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.hard_cost_approval_date >= {0})"
                     }
               },
               {
                  "CostApprovalTo", new SearchParameter
                     {
                        VariableName = "CostApprovalTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.hard_cost_approval_date <= {0})"
                     }
               },
               {
                  "CostApprovalRange", new SearchParameter
                     {
                        VariableName = "CostApprovalRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.hard_cost_approval_date >= {0} AND it.hard_cost_approval_date <= {1})",
                        IsRangeSearch = true
                     }
               },
               {
                  "PrimeLeaseExecFrom", new SearchParameter
                     {
                        VariableName = "PrimeLeaseExecFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.prime_lease_exec_date >= {0})"
                     }
               },
               {
                  "PrimeLeaseExecTo", new SearchParameter
                     {
                        VariableName = "PrimeLeaseExecTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.prime_lease_exec_date <= {0})"
                     }
               },
               {
                  "PrimeLeaseExecRange", new SearchParameter
                     {
                        VariableName = "PrimeLeaseExecRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.prime_lease_exec_date >= {0} AND it.prime_lease_exec_date <= {1})",
                        IsRangeSearch = true
                     }
               },
               {
                  "PermitAppliedFrom", new SearchParameter
                     {
                        VariableName = "PermitAppliedFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.permit_applied_date >= {0})"
                     }
               },
               {
                  "PermitAppliedTo", new SearchParameter
                     {
                        VariableName = "PermitAppliedTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.permit_applied_date <= {0})"
                     }
               },
               {
                  "PermitAppliedRange", new SearchParameter
                     {
                        VariableName = "PermitAppliedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.permit_applied_date >= {0} AND it.permit_applied_date <= {1})",
                        IsRangeSearch = true
                     }
               },
               {
                  "ConstructionStartFrom", new SearchParameter
                     {
                        VariableName = "ConstructionStartFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.construction_start_date >= {0})"
                     }
               },
               {
                  "ConstructionStartTo", new SearchParameter
                     {
                        VariableName = "ConstructionStartTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.construction_start_date <= {0})"
                     }
               },
               {
                  "ConstructionStartRange", new SearchParameter
                     {
                        VariableName = "ConstructionStartRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.construction_start_date >= {0} AND it.construction_start_date <= {1})",
                        IsRangeSearch = true
                     }
               },
               {
                  "TowerDrawingDocStatus", new SearchParameter
                     {
                        VariableName = "TowerDrawingDocStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.tower_drawings_doc_status_id = {0})"
                     }
               },
               {
                  "TowerTagPhotoDocStatus", new SearchParameter
                     {
                        VariableName = "TowerTagPhotoDocStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.tower_tag_photo_doc_status_id = {0})"
                     }
               },
               {
                  "StructuralCalcsDocStatus", new SearchParameter
                     {
                        VariableName = "StructuralCalcsDocStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.structural_calcs_doc_status_id = {0})"
                     }
               },
               {
                  "StructuralAnalysisDocStatus", new SearchParameter
                     {
                        VariableName = "StructuralAnalysisDocStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.structural_analysis_current_doc_status_id = {0})"
                     }
               },
               {
                  "TowerErectionDetailDocStatus", new SearchParameter
                     {
                        VariableName = "StructuralAnalysisDocStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.tower_erection_detail_doc_status_id = {0})"
                     }
               },
               {
                  "FoundationDesignDocStatus", new SearchParameter
                     {
                        VariableName = "FoundationDesignDocStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.foundation_design_doc_status_id = {0})"
                     }
               },
               {
                  "ConstructionDrawingDocStatus", new SearchParameter
                     {
                        VariableName = "ConstructionDrawingDocStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.construction_drawings_doc_status_id = {0})"
                     }
               },
               {
                  "GeotechReportDocStatus", new SearchParameter
                     {
                        VariableName = "GeotechReportDocStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.geotech_report_doc_status_id = {0})"
                     }
               },
               {
                  "BuildingPermitDocStatus", new SearchParameter
                     {
                        VariableName = "BuildingPermitDocStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.building_permit_doc_status_id = {0})"
                     }
               },
               {
                  "ZoningApprovalDocStatus", new SearchParameter
                     {
                        VariableName = "ZoningApprovalDocStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.zoning_approval_doc_status_id = {0})"
                     }
               },
               {
                  "PhaseIDocStatus", new SearchParameter
                     {
                        VariableName = "PhaseIDocStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.phase_i_doc_status_id = {0})"
                     }
               },
               {
                  "PhaseIIDocStatus", new SearchParameter
                     {
                        VariableName = "PhaseIIDocStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.phase_ii_doc_status_id = {0})"
                     }
               },
               {
                  "NepaDocStatus", new SearchParameter
                     {
                        VariableName = "NepaDocStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.nepa_doc_status_id = {0})"
                     }
               },
               {
                  "ShpoDocStatus", new SearchParameter
                     {
                        VariableName = "ShpoDocStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.shpo_doc_status_id = {0})"
                     }
               },
               {
                  "GPS1a2cDocStatus", new SearchParameter
                     {
                        VariableName = "GPS1a2cDocStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.gps_1a2c_doc_status_id = {0})"
                     }
               },
               {
                  "AirspaceDocStatus", new SearchParameter
                     {
                        VariableName = "AirspaceDocStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.airspace_doc_status_id = {0})"
                     }
               },
               {
                  "FAAStudyDeterminationDocStatus", new SearchParameter
                     {
                        VariableName = "FAAStudyDeterminationDocStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.faa_study_determination_doc_status_id = {0})"
                     }
               },
               {
                  "FCCDocStatus", new SearchParameter
                     {
                        VariableName = "FCCDocStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.fcc_doc_status_id = {0})"
                     }
               },
               {
                  "AMCertificationDocStatus", new SearchParameter
                     {
                        VariableName = "AMCertificationDocStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.am_certification_doc_status_id = {0})"
                     }
               },
               {
                  "TowairDocStatus", new SearchParameter
                     {
                        VariableName = "TowairDocStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.towair_doc_status_id = {0})"
                     }
               },
               {
                  "PrimeLeaseDocStatus", new SearchParameter
                     {
                        VariableName = "PrimeLeaseDocStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.prime_lease_doc_status_id = {0})"
                     }
               },
               {
                  "MemorandumDocStatus", new SearchParameter
                     {
                        VariableName = "MemorandumDocStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.memorandum_of_lease_doc_status_id = {0})"
                     }
               },
               {
                  "TitleDocStatus", new SearchParameter
                     {
                        VariableName = "TitleDocStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.title_doc_status_id = {0})"
                     }
               },
               {
                  "ABSEmployeeAssigned", new SearchParameter
                     {
                        VariableName = "ABSEmployeeAssigned",
                        ParameterType = typeof(int),
                        WhereClause = "(it.abstract_assigned_to_emp_id = {0})"
                     }
               },
               {
                  "ABSAssignedFrom", new SearchParameter
                     {
                        VariableName = "ABSAssignedFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.abstract_assigned_date >= {0})"
                     }
               },
               {
                  "ABSAssignedTo", new SearchParameter
                     {
                        VariableName = "ABSAssignedTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.abstract_assigned_date <= {0})"
                     }
               },
               {
                  "ABSAssignedRange", new SearchParameter
                     {
                        VariableName = "ABSAssignedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.abstract_assigned_date >= {0} AND it.abstract_assigned_date <= {1})",
                        IsRangeSearch = true
                     }
               },
               {
                  "ABSCompleteFrom", new SearchParameter
                     {
                        VariableName = "ABSCompleteFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.abstract_completed_date >= {0})"
                     }
               },
               {
                  "ABSCompleteTo", new SearchParameter
                     {
                        VariableName = "ABSCompleteTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.abstract_completed_date <= {0})"
                     }
               },
               {
                  "ABSCompleteRange", new SearchParameter
                     {
                        VariableName = "ABSCompleteRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.abstract_completed_date >= {0} AND it.abstract_completed_date <= {1})",
                        IsRangeSearch = true
                     }
               },
               {
                  "ABSNotes", new SearchParameter
                     {
                        VariableName = "ABSNotes",
                        ParameterType = typeof(string),
                        WhereClause = "(it.abstract_notes LIKE {0})",
                        IsWildcardSearch = true
                     }
               },
               {
                  "ABSVendor", new SearchParameter
                     {
                        VariableName = "ABSVendor",
                        ParameterType = typeof(int),
                        WhereClause = "(it.abstract_vendor_id = {0})"
                     }
               },
               {
                  "SDPAssignedToEMP", new SearchParameter
                     {
                        VariableName = "SDPAssignedToEMP",
                        ParameterType = typeof(int),
                        WhereClause = "(it.sdp_assigned_to_emp_id = {0})"
                     }
               }
            };

           return results;
        }
    }    
}