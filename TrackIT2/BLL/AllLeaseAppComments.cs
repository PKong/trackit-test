﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TrackIT2.DAL;
using System.Threading;
using System.ComponentModel;

namespace TrackIT2.BLL
{
    public static class AllCommentsHelper
    {
        public  const string STRUCTURAL_ANALYSIS = "sa";
        public  const string LEASE_APPLICATION = "la";

        //EventWaitHandle & Thread
        private static EventWaitHandle MainThread;
        static List<Boolean> IsAllComplete = new List<bool>();

        static List<AllComments> AllCommentList = new List<AllComments>();
        static List<AllComments> AllSAComment = new List<AllComments>();
        static List<AllComments> AllLAComment = new List<AllComments>();
        static List<AllComments> AllLEComment = new List<AllComments>();
        static List<AllComments> AllTMComment = new List<AllComments>();
        static List<AllComments> AllSDPComment = new List<AllComments>();
        static int allThreadCount = 5; // max = 5: all of eCommentMode, else = 3; eCommentMode 1-3
        #region Enum Mode
        public enum eCommentMode
        {
            eSA = 1,    //StructuralAnalysis
            eLA = 2,    //LeaseApplication
            eLE = 3,    //LeaseAdmin
            eTM = 4,    //TowerMod
            eSdp = 5    //SDP
        }
        #endregion

        private static void GetAllComment_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            GetAllComment_Complete(sender, e);
        }

        private static void GetAllComment_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            int[] args = (int[])e.Argument;

            try
            {
                switch (args[0])
                {
                    case 1:
                        AllSAComment.Clear();
                        AllSAComment = GetStructuralAnaComment(args[1]);
                        AllCommentList.AddRange(AllSAComment);
                        break;
                    case 2:
                        AllLAComment.Clear();
                        AllLAComment = GetLeaseAppComment(args[1]);
                        AllCommentList.AddRange(AllLAComment);
                        break;
                    case 3:
                        AllLEComment.Clear();
                        AllLEComment = GetLeaseAdminComment(args[1]);
                        AllCommentList.AddRange(AllLEComment);
                        break;
                    case 4:
                        AllTMComment.Clear();
                        AllTMComment = GetTowerModComment(args[1], args[2]);
                        AllCommentList.AddRange(AllTMComment);
                        break;
                    case 5:
                        AllSDPComment.Clear();
                        AllSDPComment = GetSdpComment(args[1], args[2]);
                        AllCommentList.AddRange(AllSDPComment);
                        break;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(null).Log(new Elmah.Error(ex));
            }
        }

        public static void GetAllComment_Complete(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            ThreadUtils.HandleThreadDone(ref MainThread, allThreadCount, ref IsAllComplete);
        }

        #region get comment by lease app id
        public static List<AllComments> GetAllCommentLeaseApp(int lease_app_id,int? source, Boolean blnIsReverseOrder = false, Boolean isFromAllCommentPage = false)
        {
            List<AllComments> lstResult = new List<AllComments>();
            AllCommentList.Clear();

            if (isFromAllCommentPage)
            {
                allThreadCount = 5;
            }
            else
            {
                allThreadCount = 3;
            }

            MainThread = new EventWaitHandle(false, EventResetMode.AutoReset);
            Thread t;
            lstResult.Clear();

            for (int x = 1; x <= allThreadCount; x++)
            {
                switch (x)
                {
                    case 1:
                        t = new Thread(
                            delegate()
                            {
                                BackgroundWorker _objWorker = new BackgroundWorker();
                                _objWorker.DoWork += new DoWorkEventHandler(GetAllComment_DoWork);
                                _objWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(GetAllComment_Complete);
                                int[] args = { (int)eCommentMode.eSA, lease_app_id };
                                _objWorker.RunWorkerAsync(args);
                            });
                        t.Name = "eSAComment";
                        t.Start();

                        break;
                    case 2:
                        t = new Thread(
                            delegate()
                            {
                                BackgroundWorker _objWorker = new BackgroundWorker();
                                _objWorker.DoWork += new DoWorkEventHandler(GetAllComment_DoWork);
                                _objWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(GetAllComment_Complete);
                                int[] args = { (int)eCommentMode.eLA, lease_app_id };
                                _objWorker.RunWorkerAsync(args);
                            });
                        t.Name = "eLAComment";
                        t.Start();

                        break;
                    case 3:
                        t = new Thread(
                            delegate()
                            {
                                BackgroundWorker _objWorker = new BackgroundWorker();
                                _objWorker.DoWork += new DoWorkEventHandler(GetAllComment_DoWork);
                                _objWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(GetAllComment_Complete);
                                int[] args = { (int)eCommentMode.eLE, lease_app_id };
                                _objWorker.RunWorkerAsync(args);
                            });
                        t.Name = "eLEComment";
                        t.Start();

                        break;
                    case 4:
                        if (isFromAllCommentPage)
                        {
                            t = new Thread(
                                delegate()
                                {
                                    BackgroundWorker _objWorker = new BackgroundWorker();
                                    _objWorker.DoWork += new DoWorkEventHandler(GetAllComment_DoWork);
                                    _objWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(GetAllComment_Complete);
                                    int id = 0;
                                    if (source.HasValue)
                                    {
                                        id = source.Value;
                                    }

                                    int[] args = { (int)eCommentMode.eTM, lease_app_id, id };
                                    _objWorker.RunWorkerAsync(args);
                                });
                            t.Name = "eTMComment";
                            t.Start();
                        }
                        break;
                    case 5:
                        if (isFromAllCommentPage)
                        {
                            t = new Thread(
                                delegate()
                                {
                                    BackgroundWorker _objWorker = new BackgroundWorker();
                                    _objWorker.DoWork += new DoWorkEventHandler(GetAllComment_DoWork);
                                    _objWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(GetAllComment_Complete);
                                    int id = 0;
                                    if (source.HasValue)
                                    {
                                        id = source.Value;
                                    }
                                    int[] args = { (int)eCommentMode.eSdp, lease_app_id, id };
                                    _objWorker.RunWorkerAsync(args);
                                });
                            t.Name = "eSdpComment";
                            t.Start();
                        }
                        break;
                }
            }

            MainThread.WaitOne(180000);

            var checkList = from tmp in AllCommentList
                            where tmp == null
                            select tmp;

            if (checkList.Count() > 0)
            {
                AllCommentList.Clear();
                AllCommentList.AddRange(AllSAComment);
                AllCommentList.AddRange(AllLAComment);
                AllCommentList.AddRange(AllLEComment);
                AllCommentList.AddRange(AllTMComment);
                AllCommentList.AddRange(AllSDPComment);
            }

            if (blnIsReverseOrder)
            {
                lstResult = AllCommentList.AsQueryable().OrderBy(a => a.CreateAt).ToList();
            }
            else
            {
                lstResult = AllCommentList.AsQueryable().OrderByDescending(a => a.CreateAt).ToList();
            }

            return lstResult;
        }

        public static List<AllComments> GetLeaseAppComment(int lease_app_id)
        {
            List<AllComments> returnResult = new List<AllComments>();
            using (CPTTEntities ce = new CPTTEntities())
            {
                var leaseApp = from la in ce.leaseapp_comments
                               where la.lease_application_id == lease_app_id
                               select new AllComments
                               {
                                   CommentID = la.id,
                                   LeaseApplicationID = lease_app_id,
                                   StructuralAnalysisID = null,
                                   LeaseAddminID = null,
                                   SdpID = null,
                                   TowerModID = null,
                                   Comments = la.comments,
                                   CreatorID = la.creator_id,
                                   CreatorName = la.creator_user.last_name + ", " + la.creator_user.first_name,
                                   CreateAt = la.created_at,
                                   CreatorUsername = la.creator_user.tmo_userid,
                                   UpdaterID = la.updater_id,
                                   UpdaterName = la.updater_user.last_name + ", " + la.updater_user.first_name,
                                   UpdateUsername = la.updater_user.tmo_userid,
                                   UpdateAt = la.updated_at,
                                   SiteID = la.lease_applications.site_uid,
                                   SiteName = la.lease_applications.site.site_name,
                                   CustomerName = la.lease_applications.customer.customer_name,
                                   Mode = "eLA"
                               };

                if (leaseApp != null)
                {
                    returnResult = leaseApp.ToList();
                }

                return returnResult;
            }
        }

        public static List<AllComments> GetStructuralAnaComment(int lease_app_id)
        {
            List<AllComments> returnResult = new List<AllComments>();
            using (CPTTEntities ce = new CPTTEntities())
            {
                var structuralAna = from saComment in ce.structural_analysis_comments
                                    join sa in ce.structural_analyses on saComment.structural_analysis_id equals sa.id
                                    where sa.lease_application_id == lease_app_id
                                    select new AllComments
                                    {
                                        CommentID = saComment.id,
                                        LeaseApplicationID = lease_app_id,
                                        StructuralAnalysisID = saComment.structural_analysis_id,
                                        LeaseAddminID = null,
                                        SdpID = null,
                                        TowerModID = null,
                                        Comments = saComment.comments,
                                        CreatorID = saComment.creator_id,
                                        CreatorName = saComment.creator_user.last_name + ", " + saComment.creator_user.first_name,
                                        CreateAt = saComment.created_at,
                                        CreatorUsername = saComment.creator_user.tmo_userid,
                                        UpdaterID = saComment.updater_id,
                                        UpdaterName = saComment.updater_user.last_name + ", " + saComment.updater_user.first_name,
                                        UpdateUsername = saComment.updater_user.tmo_userid,
                                        UpdateAt = saComment.updated_at,
                                        SiteID = saComment.structural_analyses.lease_applications.site_uid,
                                        SiteName = saComment.structural_analyses.lease_applications.site.site_name,
                                        CustomerName = saComment.structural_analyses.lease_applications.customer.customer_name,
                                        Mode = "eSA"
                                    };

                if (structuralAna != null)
                {
                    returnResult = structuralAna.ToList();
                }

                return returnResult;
            }
        }

        public static List<AllComments> GetLeaseAdminComment(int lease_app_id)
        {
            List<AllComments> returnResult = new List<AllComments>();
            using (CPTTEntities ce = new CPTTEntities())
            {
                var leaseAdmin = from la in ce.lease_admin_comments
                                 join rc in ce.lease_admin_records on la.lease_admin_record_id equals rc.id
                                 where rc.lease_application_id == lease_app_id
                                 select new AllComments
                                 {
                                     CommentID = la.id,
                                     LeaseApplicationID = lease_app_id,
                                     StructuralAnalysisID = null,
                                     LeaseAddminID = la.lease_admin_record_id,
                                     SdpID = null,
                                     TowerModID = null,
                                     Comments = la.comments,
                                     CreatorID = la.creator_id,
                                     CreatorName = la.creator_user.last_name + ", " + la.creator_user.first_name,
                                     CreateAt = la.created_at,
                                     CreatorUsername = la.creator_user.tmo_userid,
                                     UpdaterID = la.updater_id,
                                     UpdaterName = la.updater_user.last_name + ", " + la.updater_user.first_name,
                                     UpdateUsername = la.updater_user.tmo_userid,
                                     UpdateAt = la.updated_at,
                                     SiteID = rc.sites.Count() > 0 ? "Various" : "",
                                     SiteName = rc.sites.Count() > 0 ? "Various" : "",
                                     CustomerName = rc.lease_applications == null ? String.Empty : rc.lease_applications.customer.customer_name,
                                     Mode = "eLE"
                                 };

                if (leaseAdmin != null)
                {
                    returnResult = leaseAdmin.ToList();
                }

                return returnResult;
            }
        }

        public static List<AllComments> GetTowerModComment(int lease_app_id, int currentTmId)
        {
            List<AllComments> returnResult = new List<AllComments>();
            using (CPTTEntities ce = new CPTTEntities())
            {
                var towerMod = from modComment in ce.tower_mod_comments
                           from leaseapp in ce.lease_applications
                           from tower in leaseapp.tower_modifications
                           where leaseapp.id == lease_app_id && modComment.tower_modification_id == tower.id
                           select new AllComments
                             {
                                 CommentID = modComment.id,
                                 LeaseApplicationID = lease_app_id,
                                 StructuralAnalysisID = null,
                                 LeaseAddminID = null,
                                 SdpID = null,
                                 TowerModID = modComment.tower_modification_id,
                                 Comments = modComment.comment,
                                 CreatorID = modComment.creator_id,
                                 CreatorName = modComment.user.last_name + ", " + modComment.user.first_name,
                                 CreateAt = modComment.created_at,
                                 CreatorUsername = modComment.user.tmo_userid,
                                 UpdaterID = modComment.updater_id,
                                 UpdaterName = modComment.user1.last_name + ", " + modComment.user1.first_name,
                                 UpdateUsername = modComment.user1.tmo_userid,
                                 UpdateAt = modComment.updated_at,
                                 SiteID = tower.site_uid,
                                 SiteName = tower.site != null ? tower.site.site_name : "",
                                 CustomerName = tower.customer.customer_name,
                                 Mode = "eTM"
                             };

                if (towerMod != null)
                {
                    if (currentTmId != 0)
                    {
                        returnResult = (from mod in towerMod
                                        where mod.TowerModID.Value == currentTmId
                                        select mod).ToList();
                    }
                    else
                    {
                        
                        returnResult = towerMod.ToList();
                    }
                }
                return returnResult;
            }
        }

        public static List<AllComments> GetSdpComment(int lease_app_id, int currentSdpId)
        {
            List<AllComments> returnResult = new List<AllComments>();
            using (CPTTEntities ce = new CPTTEntities())
            {
                var sdpAllComment = from sdpComment in ce.sdp_comments
                               from leaseapp in ce.lease_applications
                               from sdp in leaseapp.site_data_packages
                               where leaseapp.id == lease_app_id && sdpComment.site_data_package_id == sdp.id
                               select new AllComments
                               {
                                   CommentID = sdpComment.id,
                                   LeaseApplicationID = lease_app_id,
                                   StructuralAnalysisID = null,
                                   LeaseAddminID = null,
                                   SdpID = sdpComment.site_data_package_id,
                                   TowerModID = null,
                                   Comments = sdpComment.comments,
                                   CreatorID = sdpComment.creator_id,
                                   CreatorName = sdpComment.user.last_name + ", " + sdpComment.user.first_name,
                                   CreateAt = sdpComment.created_at,
                                   CreatorUsername = sdpComment.user.tmo_userid,
                                   UpdaterID = sdpComment.updater_id,
                                   UpdaterName = sdpComment.user1.last_name + ", " + sdpComment.user1.first_name,
                                   UpdateUsername = sdpComment.user1.tmo_userid,
                                   UpdateAt = sdpComment.updated_at,
                                   SiteID = sdp.site_uid,
                                   SiteName = sdp.site != null ? sdp.site.site_name : "",
                                   CustomerName = sdp.customer.customer_name,
                                   Mode = "eSDP"
                               };

                if (sdpAllComment != null)
                {
                    if (currentSdpId != 0)
                    {
                        returnResult = (from sdp in sdpAllComment
                                        where sdp.SdpID.Value == currentSdpId
                                        select sdp).ToList();
                    }
                    else
                    {
                        returnResult = sdpAllComment.ToList();
                    }
                }
                return returnResult;
            }
        }

        #endregion get comment by lease app id

        public static List<AllComments> GetAllReleateTowerModComment(int towerModId, Boolean blnIsReverseOrder = false, Boolean isFromAllCommentPage = false)
        {
            // Tower Mods Comments
            List<AllComments> allComment = new List<AllComments>();
            using (CPTTEntities ce = new CPTTEntities())
            {
                    List<tower_mod_comments> towerModComments = ce.tower_mod_comments
                                                                    .Where(tmc => tmc.tower_modification_id == towerModId)
                                                                    .OrderByDescending(Utility.GetDateField<tower_mod_comments>("created_at"))
                                                                    .ToList();
                    List<LeaseAppTowerModInfo> lstResult = LeaseAppTowerMod.GetRalatedLeaseApp(towerModId);
                    if (lstResult.Count > 0)
                    {
                        foreach (LeaseAppTowerModInfo item in lstResult)
                        {
                            allComment.AddRange(AllCommentsHelper.GetAllCommentLeaseApp(item.LAID,towerModId, false, true));
                        }
                    }

                    if (allComment.Count == 0)
                    {
                        allComment = TowerModComment.GetTowerModCommentByTowerModId(towerModId);
                    }
            }

            return allComment;
        }

        public static List<AllComments> GetAllReleateSdpComment(int sdpId, Boolean blnIsReverseOrder = false, Boolean isFromAllCommentPage = false)
        {
            // Sdp Comments
            List<AllComments> allComment = new List<AllComments>();
            using (CPTTEntities ce = new CPTTEntities())
            {
                    List<sdp_comments> sdpComments = ce.sdp_comments
                                                                    .Where(sdp => sdp.site_data_package_id == sdpId)
                                                                    .OrderByDescending(Utility.GetDateField<sdp_comments>("created_at"))
                                                                    .ToList();
                    List<SDPRelatedLeaseApplication> lstResult = SiteDataPackage.GetRelatedLeaseApplication(null, sdpId);
                    if (lstResult != null && lstResult.Count > 0)
                    {
                        foreach (SDPRelatedLeaseApplication item in lstResult)
                        {
                            allComment.AddRange(AllCommentsHelper.GetAllCommentLeaseApp(item.LAID,sdpId, false, true));
                        }
                    }

                    if (allComment.Count == 0)
                    {
                        allComment = SdpComment.GetSdpCommentBySdp(sdpId);
                    }
            }
            return allComment;
        }
    }

    public class AllComments
    {
        public const string STRUCTURAL_ANALYSIS = "sa";
        public const string LEASE_APPLICATION = "la";
        public const string LEASE_ADMIN = "le";
        public const string TOWER_MOD = "tm";
        public const string SDP = "sdp";

        public AllComments()
        { }

        public int CommentID { get; set; }
        public int? LeaseApplicationID { get; set; }
        public int? StructuralAnalysisID { get; set; }
        public int? LeaseAddminID { get; set; }
        public int? TowerModID { get; set; }
        public int? SdpID { get; set; }
        public string Comments { get; set; }
        public int? CreatorID { get; set; }
        public string CreatorName { get; set; }
        public DateTime? CreateAt { get; set; }
        public string CreatorUsername { get; set; }
        public int? UpdaterID { get; set; }
        public string UpdaterName { get; set; }
        public DateTime? UpdateAt { get; set; }
        public string UpdateUsername { get; set; }
        public string SiteID { get; set; }
        public string SiteName { get; set; }
        public string CustomerName { get; set; }
        public String Mode { get; set; }

        public DateTime TimeStamp
        {
            get
            {
                DateTime? ret = UpdateAt;
                if (!ret.HasValue)
                {
                    ret = CreateAt;
                }
                return ret.Value;
            }
        }

        public string Source
        {
            get
            {
                string sResult = LEASE_APPLICATION;
                switch (Mode)
                { 
                    case "eLA":
                        sResult = LEASE_APPLICATION;
                        break;
                    case "eSA":
                        sResult = STRUCTURAL_ANALYSIS;
                        break;
                    case "eLE":
                        sResult = LEASE_ADMIN;
                        break;
                    case "eTM":
                        sResult = TOWER_MOD;
                        break;
                    case "eSDP":
                        sResult = SDP;
                        break;
                }
                return sResult;
            }
        }

        public string CheckSA
        {
            get 
            {
                
                string sResult = "";
                switch (Mode)
                {
                    case "eSA":
                        if (StructuralAnalysisID.HasValue)
                        {
                            sResult = "<a href=\"javascript:void(0)\" onclick=\"javascript:SACommentClick(" + StructuralAnalysisID.Value.ToString() + ");\">From SA</a>: " + Comments;
                        }
                        else
                        {
                            sResult = Comments;
                        }
                        break;
                    case "eLA":
                        sResult = Comments;
                        break;
                    case "eLE":
                        if (LeaseAddminID.HasValue)
                        {
                            sResult = "<a href=\"javascript:void(0)\" onclick=\"javascript:LECommentClick(" + LeaseAddminID.Value.ToString() + ");\">From LAdmin</a>: " + Comments;
                        }
                        else
                        {
                            sResult = Comments;
                        }
                        break;
                    case "eTM":
                        if (TowerModID.HasValue)
                        {
                            sResult = "<a href=\"/TowerModifications/Edit.aspx?id="+TowerModID.Value.ToString()+"\">From TowerMod</a>: " + Comments;
                        }
                        else
                        {
                            sResult = Comments;
                        }
                        break;

                    case "eSDP":
                        if (SdpID.HasValue)
                        {
                            sResult = "<a href=\"/SiteDataPackages/Edit.aspx?id="+ SdpID.Value.ToString() + "\">From SDP</a>: " + Comments;
                        }
                        else
                        {
                            sResult = Comments;
                        }
                        break;
                }
                return sResult;
               
            }
        }

        public string CheckTMAndSdp
        {
            get
            {

                string sResult = "";
                switch (Mode)
                {
                    case "eSA":
                        if (StructuralAnalysisID.HasValue)
                        {

                            sResult = "<a href=\"/LeaseApplications/Edit.aspx?id=" + LeaseApplicationID.Value.ToString() + "&saId=" + StructuralAnalysisID.Value.ToString() + "\">From SA</a>: " + Comments;
                        }
                        else
                        {
                            sResult = Comments;
                        }
                        break;
                    case "eLA":
                        if (LeaseApplicationID.HasValue)
                        {
                            sResult = "<a href=\"/LeaseApplications/Edit.aspx?id=" + LeaseApplicationID.Value.ToString() + "\">From LeaseApp</a>: " + Comments;
                        }
                        else
                        {
                            sResult = Comments;
                        }
                        break;
                    case "eLE":
                        if (LeaseAddminID.HasValue)
                        {
                            sResult = "<a href=\"/LeaseAdminTracker/Edit.aspx?id=" + LeaseAddminID.Value.ToString() + "\">From LAdmin</a>: " + Comments;
                        }
                        else
                        {
                            sResult = Comments;
                        }
                        break;
                    case "eTM":
                        if (TowerModID.HasValue)
                        {
                            sResult = "<a href=\"/TowerModifications/Edit.aspx?id=" + TowerModID.Value.ToString() + "\">From TowerMod</a>: " + Comments;
                        }
                        else
                        {
                            sResult = Comments;
                        }
                        break;

                    case "eSDP":
                        if (SdpID.HasValue)
                        {
                            sResult = "<a href=\"/SiteDataPackages/Edit.aspx?id=" + SdpID.Value.ToString() + "\">From SDP</a>: " + Comments;
                        }
                        else
                        {
                            sResult = Comments;
                        }
                        break;
                }
                return sResult;

            }
        }


    }
}
