﻿using System.Web.Optimization;

namespace TrackIT2.BLL
{
   public class BundleConfig
   {
      public static void CreateBundles()
      {
         // TrackiT Master Template
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/trackit")
            .Include("~/Styles/jquery-ui-1.8.16.custom.css")
            .Include("~/Styles/jquery.asmselect.css")
            .Include("~/Styles/Style.css")
            .Include("~/Styles/StyleSheetVariable.css")
         );

         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/trackit")
            .Include("~/Scripts/jquery-1.6.2.min.js")
            .Include("~/Scripts/jquery-ui-1.8.16.custom.min.js")
            .Include("~/Scripts/jquery.asmselect-forked-limit.js")
            .Include("~/Scripts/iphone-style-checkboxes.js")
            .Include("~/Scripts/jquery.cycle.all.latest.js")
            .Include("~/Scripts/jquery.tablesorter.js")
            .Include("~/Scripts/jquery.qtip-1.0.0-rc3.min.js")
            .Include("~/Scripts/activity-indicator.js")
            .Include("~/Scripts/timeout.methods.js")
            .Include("~/Scripts/utility.js")
            .Include("~/Scripts/application.js")
            .Include("~/Scripts/list.view.js")
            .Include("~/Scripts/less-1.6.3.min.js")
         );

         // Site Master Template
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/site")
            .Include("~/Styles/jquery-ui-1.8.16.custom.css")
            .Include("~/Styles/Site.css")
            .Include("~/Styles/StyleSheetVariable.css")
         );

         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/site")
            .Include("~/Scripts/jquery-1.6.2.min.js")
            .Include("~/Scripts/jquery-ui-1.8.16.custom.min.js")
            .Include("~/Scripts/less-1.6.3.min.js")
         );

         // Modal Master Template
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/modal")
            .Include("~/Styles/jquery-ui-1.8.16.custom.css")
            .Include("~/Styles/jquery.asmselect.css")
            .Include("~/Styles/Style.css")
            .Include("~/Styles/StyleSheetVariable.css")
         );


         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/modal")
            .Include("~/Scripts/jquery-1.6.2.min.js")
            .Include("~/Scripts/jquery-ui-1.8.16.custom.min.js")
            .Include("~/Scripts/jquery.asmselect-forked-limit.js")
            .Include("~/Scripts/iphone-style-checkboxes.js")
            .Include("~/Scripts/jquery.cycle.all.latest.js")
            .Include("~/Scripts/jquery.tablesorter.js")
            .Include("~/Scripts/jquery.qtip-1.0.0-rc3.min.js")
            .Include("~/Scripts/utility.js")
            .Include("~/Scripts/application.js")
            .Include("~/Scripts/activity-indicator.js")
            .Include("~/Scripts/less-1.6.3.min.js")
         );

         // Login Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/login")
            .Include("~/Styles/Style.css")
            .Include("~/Styles/Login.css")
         );

         // Profile Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/profile")
            .Include("~/Styles/create.edit.css")
         );

         // Reset Password Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/reset_password")
            .Include("~/Styles/Style.css")
            .Include("~/Styles/Login.css")
         );

         // Manage Saved Filters Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/saved_filters_manage")
            .Include("~/Styles/Style.css")
            .Include("~/Styles/create.edit.css")
         );

         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/saved_filters_manage")
            .Include("~/Scripts/validate.create.modal.js")
            .Include("~/Scripts/saved.filters.js")
            .Include("~/Scripts/jquery.asmselect.js")
         );

         // Saved Links Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/saved_links")
            .Include("~/Styles/dashboard.css")
         );

         // All Comments Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/all_comment")
            .Include("~/Styles/workflow.css")
            .Include("~/Styles/all.comment.css")
         );

         // Page Section Header Control
         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/page_section_header")
            .Include("~/Scripts/warning-edit.js")
         );

         // Search Control
         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/search_control")
            .Include("~/Scripts/global.search.js")
         );

         // Name Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/recent_comments")
            .Include("~/Styles/all.comment.css")
         );

         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/recent_comments")
            .Include("~/Scripts/page-dynamic-tab.js")
         );

         // Recent document
         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/recent_documents")
             .Include("~/Scripts/page-dynamic-tab.js")
         );

         // Dashboard Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/dashboard")
            .Include("~/Styles/dashboard.css")
         );

         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/dashboard")
            .Include("~/Scripts/jquery.dashboard.js")
            .Include("~/Scripts/jquery.ui.widget.js")
            .Include("~/Scripts/highcharts.js")
            .Include("~/Scripts/widget-script.js")
            .Include("~/Scripts/widget-script.my-saved-filters-list-view.js")
         );

         // Issue Tracker Add Record Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/issue_tracker_add")
            .Include("~/Styles/create.edit.css")
         );

         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/issue_tracker_add")
            .Include("~/Scripts/validate.create.modal.js")
            .Include("~/Scripts/dms.js")
            .Include("~/Scripts/documentLink.js")
         );

         // Issue Tracker Main Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/issue_tracker")
            .Include("~/Styles/list.view.css")
            .Include("~/Styles/datagrid.css")
         );

         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/issue_tracker")
            .Include("~/Scripts/list.view.js")
            .Include("~/Scripts/saved.filters.js")
            .Include("~/Scripts/adv.search.js")
            .Include("~/Scripts/ExportFile.js")
         );

         // Issue Tracker Edit Record Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/issue_tracker_edit")
            .Include("~/Styles/Site.css")
            .Include("~/Styles/workflow.css")
            .Include("~/Styles/all.comment.css")
         );

         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/issue_tracker_edit")
            .Include("~/Scripts/list.view.js")
            .Include("~/Scripts/validate.create.modal.js")
            .Include("~/Scripts/issue.tracker.js")
            .Include("~/Scripts/dms.js")
            .Include("~/Scripts/documentLink.js")
         );

         // Lease Admin Tracker Add Record Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/lease_admin_add")
            .Include("~/Styles/create.edit.css")
         );

         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/lease_admin_add")
            .Include("~/Scripts/jquery.formatCurrency-1.4.0.min.js")
            .Include("~/Scripts/validate.create.modal.js")
            .Include("~/Scripts/lease.admin.validation.js")
            .Include("~/Scripts/lease.admin.modal.js")
            .Include("~/Scripts/lease.admin.js")
         );

         // Lease Admin Tracker Main Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/lease_admin")
            .Include("~/Styles/list.view.css")
            .Include("~/Styles/datagrid.css")
         );

         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/lease_admin")
            .Include("~/Scripts/jquery.formatCurrency-1.4.0.min.js")
            .Include("~/Scripts/lease.admin.validation.js")
            .Include("~/Scripts/lease.admin.js")
            .Include("~/Scripts/lease.admin.modal.js")
            .Include("~/Scripts/list.view.js")
            .Include("~/Scripts/saved.filters.js")
            .Include("~/Scripts/adv.search.js")
            .Include("~/Scripts/ExportFile.js")
         );

         // Lease Admin Tracker Edit Record Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/lease_admin_edit")
            .Include("~/Styles/list.view.css")
            .Include("~/Styles/datagrid.css")
            .Include("~/Styles/create.edit.css")
         );

         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/lease_admin_edit")
            .Include("~/Scripts/jquery.formatCurrency-1.4.0.min.js")
            .Include("~/Scripts/warning-edit.js")
            .Include("~/Scripts/list.view.js")
            .Include("~/Scripts/lease.admin.modal.js")
            .Include("~/Scripts/lease.admin.validation.js")
            .Include("~/Scripts/lease.admin.js")
            .Include("~/Scripts/lease.admin.comment.js")
            .Include("~/Scripts/validate.create.modal.js")
            .Include("~/Scripts/dms.js")
            .Include("~/Scripts/documentLink.js")
         );

         // Lease Application Add Record Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/lease_application_add")
            .Include("~/Styles/create.edit.css")
         );

         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/lease_application_add")
            .Include("~/Scripts/jquery.formatCurrency-1.4.0.min.js")
            .Include("~/Scripts/validate.create.modal.js")
            .Include("~/Scripts/leaseapp.validation.js")
         );

         // Lease Application LASummaryInfo Record Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/lease_application_summary")
            .Include("~/Styles/create.edit.css")
         );

         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/lease_application_summary")
            .Include("~/Scripts/jquery.formatCurrency-1.4.0.min.js")
            .Include("~/Scripts/validate.create.modal.js")
            .Include("~/Scripts/leaseapp.validation.js")
         );

         // Lease Application Main Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/lease_application")
            .Include("~/Styles/list.view.css")
            .Include("~/Styles/datagrid.css")
         );

         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/lease_application")
            .Include("~/Scripts/jquery.formatCurrency-1.4.0.min.js")
            .Include("~/Scripts/leaseapp.modal.js")
            .Include("~/Scripts/list.view.js")
            .Include("~/Scripts/saved.filters.js")
            .Include("~/Scripts/adv.search.js")
            .Include("~/Scripts/ExportFile.js")
            .Include("~/Scripts/leaseapp-ajax-load.js")
         );

         // Lease Application Edit Record Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/lease_application_edit")
            .Include("~/Styles/list.view.css")
            .Include("~/Styles/datagrid.css")
            .Include("~/Styles/create.edit.css")
         );

         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/lease_application_edit")
            .Include("~/Scripts/jquery.formatCurrency-1.4.0.min.js")
            .Include("~/Scripts/validate.create.modal.js")
            .Include("~/Scripts/structural.analysis.js")
            .Include("~/Scripts/lease.admin.comment.js")
            .Include("~/Scripts/leaseapp.validation.js")
            .Include("~/Scripts/leaseapp.modal.js")
            .Include("~/Scripts/warning-edit.js")
            .Include("~/Scripts/json.js")
            .Include("~/Scripts/page-dynamic-tab.js")
            .Include("~/Scripts/dms.js")
            .Include("~/Scripts/documentLink.js")
            .Include("~/Scripts/FlexigridOnClientColumnToggle.js")
            .Include("~/Scripts/leaseapp-ajax-load.js")
         );
             
         // Lease Application History Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/lease_application_history")
            .Include("~/Styles/workflow.css")
            .Include("~/Styles/all.comment.css")
         );

         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/lease_application_history")
            .Include("~/Scripts/jquery.dashboard.js")
            .Include("~/Scripts/jquery.ui.widget.js")
            .Include("~/Scripts/highcharts.js")
            .Include("~/Scripts/leaseapp.modal.js")             
         );

         // Lease Application Client Information Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/lease_application_client_info")
            .Include("~/Styles/create.edit.css")
         );

         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/lease_application_client_info")
            .Include("~/Scripts/validate.create.modal.js")
         );

         // T-Mobile Towers Transfer Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/tmt_transfer")
            .Include("~/Styles/create.edit.css")
         );

         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/tmt_transfer")
            .Include("~/Scripts/jquery.formatCurrency-1.4.0.min.js")
            .Include("~/Scripts/validate.create.modal.js")
            .Include("~/Scripts/leaseapp.validation.js")
            .Include("~/Scripts/jquery.ba-postmessage.js")
         );

         // Site Data Package Add Records Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/site_data_package_add")
            .Include("~/Styles/create.edit.css")
         );

         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/site_data_package_add")
            .Include("~/Scripts/validate.create.modal.js")
            .Include("~/Scripts/dms.js")
         );

         // Site Data Package Main Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/site_data_package")
            .Include("~/Styles/list.view.css")
            .Include("~/Styles/datagrid.css")
         );

         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/site_data_package")
            .Include("~/Scripts/jquery.formatCurrency-1.4.0.min.js")
            .Include("~/Scripts/list.view.js")
            .Include("~/Scripts/saved.filters.js")
            .Include("~/Scripts/adv.search.js")
            .Include("~/Scripts/ExportFile.js")            
         );

         // Site Data Package Edit Record Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/site_data_package_edit")
            .Include("~/Styles/create.edit.css")
         );

         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/site_data_package_edit")
            .Include("~/Scripts/validate.create.modal.js")
            .Include("~/Scripts/tower.mods.modal.js")
            .Include("~/Scripts/sdp.validation.js")
            .Include("~/Scripts/warning-edit.js")
            .Include("~/Scripts/dms.js")
            .Include("~/Scripts/documentLink.js")
         );

         // Sites Add Record Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/sites_add")
            .Include("~/Styles/create.edit.css")
         );

         // Sites Add Record Page
         BundleTable.Bundles.Add(new StyleBundle("~/Scripts/sites_add")
            .Include("~/Scripts/validate.create.modal.js")
            .Include("~/Scripts/dms.js")
            .Include("~/Scripts/documentLink.js")
         );

         // Sites Main Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/sites")
            .Include("~/Styles/list.view.css")
            .Include("~/Styles/datagrid.css")
         );

         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/sites")
            .Include("~/Scripts/list.view.js")
            .Include("~/Scripts/saved.filters.js")
            .Include("~/Scripts/adv.search.js")
            .Include("~/Scripts/ExportFile.js")
         );

         // Sites Dashboard Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/sites_dashboard")
            .Include("~/Styles/Site.css")
            .Include("~/Styles/list.view.css")
            .Include("~/Styles/datagrid.css")
         );

         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/sites_dashboard")
            .Include("~/Scripts/list.view.js")
            .Include("~/Scripts/dms.js")
            .Include("~/Scripts/FlexigridOnClientColumnToggle.js")
            .Include("~/Scripts/documentLink.js")
         );

         // Tower Modification Add Record Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/tower_modification_add")
            .Include("~/Styles/create.edit.css")
         );

         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/tower_modification_add")
            .Include("~/Scripts/validate.create.modal.js")
         );

         // Tower Modification Main Page Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/tower_modification")
            .Include("~/Styles/list.view.css")
            .Include("~/Styles/datagrid.css")
         );

         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/tower_modification")
            .Include("~/Scripts/list.view.js")
            .Include("~/Scripts/saved.filters.js")
            .Include("~/Scripts/adv.search.js")
            .Include("~/Scripts/ExportFile.js")
         );

         // Tower Modification Edit Record Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/tower_modification_edit")
            .Include("~/Styles/create.edit.css")
         );

         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/tower_modification_edit")
            .Include("~/Scripts/jquery.formatCurrency-1.4.0.min.js")
            .Include("~/Scripts/validate.create.modal.js")
            .Include("~/Scripts/tower.mods.js")
            .Include("~/Scripts/tower.mods.modal.js")
            .Include("~/Scripts/warning-edit.js")
            .Include("~/Scripts/dms.js")
            .Include("~/Scripts/documentLink.js")
         );

         // Sites Dashboard Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/trackit_import")
            .Include("~/Styles/Site.css")
            .Include("~/Styles/list.view.css")
            .Include("~/Styles/datagrid.css")
         );

         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/trackit_import")
            .Include("~/Scripts/list.view.js")
         );         

         // Tower List Main Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/tower_list")
            .Include("~/Styles/list.view.css")
            .Include("~/Styles/datagrid.css")
         );

         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/tower_list")
            .Include("~/Scripts/towerlist.js")
            .Include("~/Scripts/jquery.formatCurrency-1.4.0.min.js")
            .Include("~/Scripts/leaseapp.modal.js")
            .Include("~/Scripts/list.view.js")
            .Include("~/Scripts/saved.filters.js")
            .Include("~/Scripts/adv.search.js")
            .Include("~/Scripts/ExportFile.js")
         );

         // Tower List Add Record Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/tower_list_add")
            .Include("~/Styles/create.edit.css")
         );

         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/tower_list_add")
            .Include("~/Scripts/validate.create.modal.js")
            .Include("~/Scripts/towerlist.js")
            .Include("~/Scripts/lease.admin.modal.js")
            .Include("~/Scripts/list.view.js")
         );

         // DMS Main Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/dms")
            .Include("~/Styles/list.view.css")
            .Include("~/Styles/datagrid.css")
         );

         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/dms")
            .Include("~/Scripts/list.view.js")
            .Include("~/Scripts/dms.js")
            .Include("~/Scripts/FlexigridOnClientColumnToggle.js")
         );

         // DMS Create Folder Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/dms_create_folder")
            .Include("~/Styles/create.edit.css")
            .Include("~/Styles/jqueryFileTree.css")
         );

         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/dms_create_folder")
            .Include("~/Scripts/validate.create.modal.js")
            .Include("~/Scripts/dms.js")
            .Include("~/Scripts/jquery.easing.1.2.js")
            .Include("~/Scripts/jqueryFileTree.js")
            .Include("~/Scripts/dmsLocationTree.js")
         );
        
         // DMS Document Folder Link Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/dms_document_folder")
            .Include("~/Styles/create.edit.css")
            .Include("~/Styles/jqueryFileTree.css")
         );

         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/dms_document_folder")
            .Include("~/Scripts/jquery.easing.1.2.js")
            .Include("~/Scripts/jqueryFileTree.js")
            .Include("~/Scripts/documentLink.js")
            .Include("~/Scripts/dmsLocationTree.js")
            .Include("~/Scripts/jquery.scrollTo-1.4.3.1-min.js")  
         );

         // DMS Document Links Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/dms_document_link")
            .Include("~/Styles/create.edit.css")
            .Include("~/Styles/jqueryFileTree.css")
         );

         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/dms_document_link")
            .Include("~/Scripts/validate.create.modal.js")
            .Include("~/Scripts/dms.js")
            .Include("~/Scripts/jquery.easing.1.2.js")
            .Include("~/Scripts/jqueryFileTree.js")
            .Include("~/Scripts/dmsLocationTree.js")
            .Include("~/Scripts/documentLink.js")
            .Include("~/Scripts/jquery.scrollTo-1.4.3.1-min.js")
         );

         // DMS Folder Tree Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/dms_folder_tree")
            .Include("~/Styles/jqueryFileTree.css")
         );

         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/dms_folder_tree")
            .Include("~/Scripts/jqueryFileTree.js")
            .Include("~/Scripts/documentLink.js")
            .Include("~/Scripts/jquery.scrollTo-1.4.3.1-min.js")
         );

         // DMS Remove Document Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/dms_remove_document")
            .Include("~/Styles/create.edit.css")
         );

         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/dms_remove_document")
            .Include("~/Scripts/documentLink.js")
         );

         // DMS Upload Document Page
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/dms_upload_document")
            .Include("~/Styles/create.edit.css")
            .Include("~/Styles/jqueryFileTree.css")
         );

         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/dms_upload_document")
            .Include("~/Scripts/validate.create.modal.js")
            .Include("~/Scripts/dms.js")
            .Include("~/Scripts/jquery.easing.1.2.js")
            .Include("~/Scripts/jquery.scrollTo-1.4.3.1-min.js")
            .Include("~/Scripts/jqueryFileTree.js")
            .Include("~/Scripts/dmsLocationTree.js")
            .Include("~/Scripts/documentLink.js")
            .Include("~/Scripts/jquery.MultiFile.js")
         );

         // Modal No Form Template
         BundleTable.Bundles.Add(new StyleBundle("~/Styles/no_form")
            .Include("~/Styles/jquery-ui-1.8.16.custom.css")
            .Include("~/Styles/jquery.asmselect.css")
            .Include("~/Styles/Style.css")
            .Include("~/Styles/StyleSheetVariable.css")
         );

         BundleTable.Bundles.Add(new ScriptBundle("~/Scripts/no_form")
            .Include("~/Scripts/jquery-1.6.2.min.js")
            .Include("~/Scripts/jquery-ui-1.8.16.custom.min.js")
            .Include("~/Scripts/jquery.asmselect-forked-limit.js")
            .Include("~/Scripts/iphone-style-checkboxes.js")
            .Include("~/Scripts/jquery.cycle.all.latest.js")
            .Include("~/Scripts/jquery.tablesorter.js")
            .Include("~/Scripts/jquery.qtip-1.0.0-rc3.min.js")
            .Include("~/Scripts/utility.js")
            .Include("~/Scripts/application.js")
            .Include("~/Scripts/activity-indicator.js")
            .Include("~/Scripts/less-1.6.3.min.js")
         );

      }       
   }
}