﻿using System.Data.Objects;
using System.IO;
using FlexigridASPNET;
using System.Collections.Generic;
using TrackIT2.DAL;
using System;
using System.Collections.Specialized;
using System.Text;
using System.Linq;
using System.Web.Script.Serialization;
using TrackIT2.Handlers;


namespace TrackIT2.BLL
{

   #region class Export File
   public class ExportFile : BaseFlexiHandler
   {      
      #region Enumeration
      public enum eExportType
      {
         eLeaseApp,
         eSite,
         eSiteDataPackage,
         eTowerMod,
         eIssueTracker,
         eLeaseAdmin,
         eTowerlist,
         eElse
      };
      #endregion

      public static bool SubFilterHasNoResults = false;

      #region LeaseApp
      public List<ExportClass> LeaseAppGetFilteredData(NameValueCollection data, NameValueCollection column, object sender, NameValueCollection formData)
      {
         var ce = new CPTTEntities();
         var resultApplications = new List<ExportClass>();
         var applications = new List<lease_applications>();

         var sortName = !String.IsNullOrWhiteSpace(data[0]) ? data[0] : "site_uid";
         const int startIndex = 0;
         GridSortOrder? customSorting = null;

         // Find sorting direction property in data
         if (data.Count > startIndex)
         {
            for (var i = startIndex; i < data.Count; i++)
            {
               if (data.GetKey(i) != Globals.QS_SORT_DIRECTION) continue;

               customSorting = data.Get(i) == Globals.SORT_DIRECTION_VALUE_ASC
                                  ? GridSortOrder.Asc
                                  : GridSortOrder.Desc;
            }
         }

         ObjectQuery<lease_applications> queryLeaseApplications = ce.lease_applications;
         var searchCriteria = LeaseApplication.GetSearchCriteria();
         queryLeaseApplications = Utility.BuildObjectQuery(queryLeaseApplications, formData, searchCriteria);

         this.VitualItemsCount = queryLeaseApplications.Count();

         if (customSorting == null) customSorting = this.SortDirection;
         if (customSorting == GridSortOrder.Asc)
         {
            var a = Utility.OrderBy(queryLeaseApplications, sortName);
            applications = a.ToList();
         }
         else
         {
            var b = Utility.OrderByDescending(queryLeaseApplications, sortName);
            applications = b.ToList();
         }

         if (VitualItemsCount > 0)
         {
            resultApplications = (from aData in applications
                              select new ExportClass
                              {
                                 sdata = aData.site_uid.ToString() + "|" +
                                         (aData.site == null ? String.Empty : string.Format("\"{0}\"", aData.site.site_name)) + "|" +
                                         (aData.customer == null ? String.Empty : string.Format("\"{0}\"", aData.customer.customer_name.ToString())) + "|" +
                                         string.Format("\"{0}\"", aData.original_carrier) + "|" +
                                         (aData.leaseapp_types == null ? String.Empty : string.Format("\"{0}\"", aData.leaseapp_types.lease_application_type)) + "|" +
                                         (aData.leaseapp_activities == null ? String.Empty : string.Format("\"{0}\"", aData.leaseapp_activities.lease_app_activity)) + "|" +
                                         (aData.leaseapp_rcvd_date == null ? string.Empty : aData.leaseapp_rcvd_date.ToString().Split(' ')[0]) + "|" +
                                         (aData.leaseapp_statuses == null ? String.Empty : string.Format("\"{0}\"", aData.leaseapp_statuses.lease_application_status)) + "|" +
                                         (aData.tower_modifications == null ? String.Empty : (aData.site.tower_modifications.Count != 0).ToString()) + "|" +
                                         (aData.site == null ? "false" : aData.site.rogue_equipment_yn == null ? "false" : "true") + "|" +
                                         (aData.site == null ? String.Empty : (aData.site.issue_tracker.Count != 0).ToString()) + "|"
                              }).ToList();
         }

         return resultApplications;         
      }

      #endregion LeaseApp

      #region Site

      public List<ExportClass> SiteGetFilteredData(NameValueCollection data, NameValueCollection column, object sender, NameValueCollection formData)
      {
         var ce = new CPTTEntities();
         var resultSite = new List<ExportClass>();
         var sites = new List<site>();

         var sortName = !String.IsNullOrWhiteSpace(data[0]) ? data[0] : "site_uid";
         const int startIndex = 0;
         GridSortOrder? customSorting = null;

         // Find sorting direction property in data
         if (data.Count > startIndex)
         {
            for (var i = startIndex; i < data.Count; i++)
            {
                if (data.GetKey(i) == Globals.QS_SORT_DIRECTION)
                {
                    customSorting = data.Get(i) == Globals.SORT_DIRECTION_VALUE_ASC
                                       ? GridSortOrder.Asc
                                       : GridSortOrder.Desc;
                }
                else if (data.GetKey(i) == "State")
                {
                    formData["State"] = Site.GetStateShortName(data["State"]);
                }
            }
         }

         ObjectQuery<site> querySites = ce.sites;
         var searchCriteria = Site.GetSearchCriteria();
         querySites = Utility.BuildObjectQuery(querySites, formData, searchCriteria);

         this.VitualItemsCount = querySites.Count();

         if (customSorting == null) customSorting = this.SortDirection;
         if (customSorting == GridSortOrder.Asc)
         {
            var a = Utility.OrderBy(querySites, sortName);
            sites = a.ToList();
         }
         else
         {
            var b = Utility.OrderByDescending(querySites, sortName);
            sites = b.ToList();
         }

         if (VitualItemsCount > 0)
         {
            resultSite = (from aData in sites
                          select new ExportClass
                          {
                             sdata = aData.site_uid + "|" +
                               (string.Format("\"{0}\"", aData.site_name.Replace("\n", "").Replace("\r", ""))) + "|" +
                               (string.Format("\"{0}\"", aData.market_name.Replace("\n", "").Replace("\r", ""))) + "|" +
                               (string.Format("\"{0}\"", aData.site_class_desc.Replace("\n", "").Replace("\r", ""))) + "|" +
                               (string.Format("\"{0}\"", aData.site_status_desc.Replace("\n", "").Replace("\r", ""))) + "|" +
                               (aData.tower_modifications == null ? "false" : (aData.tower_modifications.Count() != 0).ToString()) + "|" +
                               (aData.rogue_equipment_yn == null ? "false" : aData.rogue_equipment_yn.ToString()) + "|"
                          }).ToList();
         }

         return resultSite;
      }

      #endregion Site

      #region SiteDataPackage

      public List<ExportClass> SiteDataPackageGetFilteredData(NameValueCollection data, NameValueCollection column, object sender, NameValueCollection formData)
      {
         var ce = new CPTTEntities();
         var resultSdp = new List<ExportClass>();
         var siteDataPackages = new List<site_data_packages>();

         var sortName = !String.IsNullOrWhiteSpace(data[0]) ? data[0] : "site_uid";
         const int startIndex = 0;
         GridSortOrder? customSorting = null;

         // Find sorting direction property in data
         if (data.Count > startIndex)
         {
            for (var i = startIndex; i < data.Count; i++)
            {
               if (data.GetKey(i) != Globals.QS_SORT_DIRECTION) continue;

               customSorting = data.Get(i) == Globals.SORT_DIRECTION_VALUE_ASC
                                  ? GridSortOrder.Asc
                                  : GridSortOrder.Desc;
            }
         }

         ObjectQuery<site_data_packages> querySdp = ce.site_data_packages;
         var searchCriteria = BLL.SiteDataPackage.GetSearchCriteria();
         querySdp = Utility.BuildObjectQuery(querySdp, formData, searchCriteria);

         VitualItemsCount = querySdp.Count();

         if (customSorting == null) customSorting = this.SortDirection;
         if (customSorting == GridSortOrder.Asc)
         {
            var a = Utility.OrderBy(querySdp, sortName);
            siteDataPackages = a.ToList();
         }
         else
         {
            var b = Utility.OrderByDescending(querySdp, sortName);
            siteDataPackages = b.ToList();
         }         

         if (VitualItemsCount > 0)
         {
            resultSdp = (from aData in siteDataPackages
                         join sdpro in ce.sdp_special_projects on aData.sdp_special_project_id equals sdpro.id into j1
                         from j2 in j1.DefaultIfEmpty()
                         orderby sortName
                         select new ExportClass
                         {
                            sdata = aData.site_uid + "|" +
                                   (aData.site == null ? String.Empty : string.Format("\"{0}\"", aData.site.region_name.Replace("\n", "").Replace("\r", ""))) + "|" +
                                   (aData.site == null ? String.Empty : string.Format("\"{0}\"", aData.site.market_code.Replace("\n", "").Replace("\r", ""))) + "|" +
                                   (aData.site == null ? String.Empty : aData.site.site_on_air_date.ToString().Split(' ')[0]) + "|" +
                                   (aData.sdp_priorities == null ? String.Empty : string.Format("\"{0}\"", aData.sdp_priorities.sdp_priority.Replace("\n", "").Replace("\r", ""))) + "|" +
                                   (aData.sdp_request_date == null ? string.Empty : aData.sdp_request_date.ToString().Split(' ')[0]) + "|" +
                                   (j2 == null ? String.Empty : j2.sdp_special_project) + "|" +
                                   (GenSdpPm(aData)) + "|" +
                                   (GenSdpCurrent(aData)) + "|"
                         }).ToList();   
         }
         
         return resultSdp;
      }

      public string GenSdpPm(site_data_packages aData)
      {
         string returnResult = "";

         if (aData.lease_applications != null)
         {
            if (aData.lease_applications.user_lease_applications_tmo_specialist_pm != null)
            {
               returnResult = string.Format("\"{0}\"", aData.lease_applications.user_lease_applications_tmo_specialist_pm.last_name + ", " + aData.lease_applications.user_lease_applications_tmo_specialist_pm.first_name);
            }
         }
         else if (aData.user6 != null)
         {
            returnResult = string.Format("\"{0}\"", aData.user6.last_name + ", " + aData.user6.first_name);
         }

         return returnResult;
      }

      public string GenSdpCurrent(site_data_packages aData)
      {
         string returnResult = "";

         if (aData.lease_applications != null)
         {
            if (aData.lease_applications.user_lease_applications_tmo_specialist_current != null)
            {
               returnResult = string.Format("\"{0}\"", aData.lease_applications.user_lease_applications_tmo_specialist_current.last_name + ", " + aData.lease_applications.user_lease_applications_tmo_specialist_current.first_name);
            }
         }
         else if (aData.user7 != null)
         {
            returnResult = string.Format("\"{0}\"", aData.user7.last_name + ", " + aData.user7.first_name);
         }

         return returnResult;
      }

      #endregion SiteDataPackage

      #region Tower Modifications

      public List<ExportClass> TowerModificationsGetFilteredData(NameValueCollection data, NameValueCollection column, object sender, NameValueCollection formData)
      {
         var ce = new CPTTEntities();
         List<ExportClass> resultTowerMods = new List<ExportClass>();
         var sites = new List<tower_modifications>();

         string sortName = !String.IsNullOrWhiteSpace(data[0]) ? data[0] : "site_uid";
         const int startIndex = 0;
         GridSortOrder? customSorting = null;                  
         List<ExportTowerMod> lstEXTobj = new List<ExportTowerMod>();

         // Find sorting direction property in data
         if (data.Count > startIndex)
         {
            for (var i = startIndex; i < data.Count; i++)
            {
               if (data.GetKey(i) != Globals.QS_SORT_DIRECTION) continue;

               customSorting = data.Get(i) == Globals.SORT_DIRECTION_VALUE_ASC
                                  ? GridSortOrder.Asc
                                  : GridSortOrder.Desc;
            }
         }

         ObjectQuery<tower_modifications> queryTowerMods = ce.tower_modifications;
         var searchCriteria = TowerMod.GetSearchCriteria();         
         queryTowerMods = Utility.BuildObjectQuery(queryTowerMods, formData, searchCriteria);

         this.VitualItemsCount = queryTowerMods.Count();

         IQueryable<ExportTowerMod> exportObj = from m in queryTowerMods
                                                select new ExportTowerMod
                                                {
                                                   id = m.id,
                                                   customer = m.customer,
                                                   site_uid = m.site_uid,
                                                   tower_mod_statuses = m.tower_mod_statuses,
                                                   tower_mod_types = m.tower_mod_types,
                                                   tower_mod_pm_user = m.tower_mod_pm_user,
                                                   project_complete_date = m.project_complete_date,
                                                   site = m.site,
                                                   tower_mod_pos = m.tower_mod_pos.OrderByDescending(d => d.created_at).FirstOrDefault(),
                                                   tower_mod_status_name = m.tower_mod_statuses == null ? string.Empty : m.tower_mod_statuses.tower_mod_status_name,
                                                   tower_mod_type = m.tower_mod_types == null ? string.Empty : m.tower_mod_types.tower_mod_type,
                                                   rogue_equipment_yn = m.site == null ? string.Empty : m.site.rogue_equipment_yn == null ? string.Empty : m.site.rogue_equipment_yn == true ? "True" : "False",
                                                   tower_mod_pm_user_name = m.tower_mod_pm_user == null ? string.Empty : m.tower_mod_pm_user.last_name + ", " + m.tower_mod_pm_user.first_name
                                                };

         int VitualItemsCount = exportObj.Count();
         
         if (customSorting == null) customSorting = this.SortDirection;

         lstEXTobj = new List<ExportTowerMod>();

         if (customSorting == GridSortOrder.Asc)
         {
            var a = Utility.OrderBy(exportObj, sortName);
            lstEXTobj = a.ToList<ExportTowerMod>();
         }
         else
         {
            var b = Utility.OrderByDescending(exportObj, sortName);
            lstEXTobj = b.ToList<ExportTowerMod>();
         }

         List<ExportClass> exportMod = new List<ExportClass>();

         if (VitualItemsCount > 0)
         {
            foreach (var mod in lstEXTobj)
            {
               ExportClass ex = new ExportClass();

               List<LeaseAppTowerModInfo> lstResult = LeaseAppTowerMod.GetRalatedLeaseApp(mod.id);
               if (lstResult.Count == 0)
               {
                  if (mod.customer != null)
                  {
                     mod.customer_name = mod.customer.customer_name;
                  }
               }
               else
               {
                  String sCustomerNames = "";
                  foreach (LeaseAppTowerModInfo item in lstResult)
                  {
                     if (item.CustomerName != null)
                     {
                        if (sCustomerNames == "")
                        {
                           sCustomerNames += item.CustomerName;
                        }
                        else
                        {
                           sCustomerNames += ", " + item.CustomerName;
                        }
                     }
                  }
                  mod.customer_name = sCustomerNames;
               }

               if (mod.tower_mod_pos != null)
               {
                  if (mod.tower_mod_pos.tower_mod_phase_types != null)
                  {
                     mod.phase_name = mod.tower_mod_pos.tower_mod_phase_types.phase_name.ToString();
                  }

                  mod.dateCurPackageRcvd = mod.tower_mod_pos.ebid_req_rcvd_date;
               }
               string siteID = mod.site_uid.Replace("\n", "").Replace("\r", "");
               siteID = string.Format("\"{0}\"", siteID);
               string customer_name = mod.customer_name == null ? string.Empty : mod.customer_name.Replace("\n", "").Replace("\r", "");
               customer_name = string.Format("\"{0}\"", customer_name);
               string tower_mod_status_name = mod.tower_mod_statuses == null ? string.Empty : mod.tower_mod_statuses.tower_mod_status_name.Replace("\n", "").Replace("\r", "");
               tower_mod_status_name = string.Format("\"{0}\"", tower_mod_status_name);
               string tower_mod_type = mod.tower_mod_type == null ? string.Empty : mod.tower_mod_type.Replace("\n", "").Replace("\r", "");
               tower_mod_type = string.Format("\"{0}\"", tower_mod_type);
               string phase_name = mod.phase_name == null ? string.Empty : mod.phase_name.Replace("\n", "").Replace("\r", "");
               phase_name = string.Format("\"{0}\"", phase_name);
               string tower_mod_pm_user_name = mod.tower_mod_pm_user_name == null ? string.Empty : mod.tower_mod_pm_user_name.Replace("\n", "").Replace("\r", "");
               tower_mod_pm_user_name = string.Format("\"{0}\"", tower_mod_pm_user_name);
               string dateCurPackageRcvd = mod.dateCurPackageRcvd == null ? string.Empty : mod.dateCurPackageRcvd.Value.ToString().Split(' ')[0];
               string project_complete_date = mod.project_complete_date == null ? string.Empty : mod.project_complete_date.Value.ToString().Split(' ')[0];
               string rogue_equipment_yn = mod.rogue_equipment_yn;

               ex.sdata = siteID + "|" + customer_name + "|" + tower_mod_status_name + "|" + tower_mod_type + "|" + phase_name + "|" + tower_mod_pm_user_name + "|" + dateCurPackageRcvd + "|" + project_complete_date + "|" + rogue_equipment_yn + "|";
               exportMod.Add(ex);
            }
         }
         return exportMod;
      }

      #endregion Tower Modifications

      #region Issue Tracker

      public List<ExportClass> IssueTrackerGetFilteredData(NameValueCollection data, NameValueCollection column, object sender, NameValueCollection formData)
      {
         var ce = new CPTTEntities();
         var resultIssues = new List<ExportClass>();
         var issues = new List<issue_tracker>();

         var sortName = !String.IsNullOrWhiteSpace(data[0]) ? data[0] : "site_uid";
         const int startIndex = 0;
         GridSortOrder? customSorting = null;

         // Find sorting direction property in data
         if (data.Count > startIndex)
         {
            for (var i = startIndex; i < data.Count; i++)
            {
               if (data.GetKey(i) != Globals.QS_SORT_DIRECTION) continue;

               customSorting = data.Get(i) == Globals.SORT_DIRECTION_VALUE_ASC
                                  ? GridSortOrder.Asc
                                  : GridSortOrder.Desc;
            }
         }

         ObjectQuery<issue_tracker> queryIssueTracker = ce.issue_tracker;
         var searchCriteria = IssueTracker.GetSearchCriteria();
         queryIssueTracker = Utility.BuildObjectQuery(queryIssueTracker, formData, searchCriteria);

         this.VitualItemsCount = queryIssueTracker.Count();

         if (customSorting == null) customSorting = this.SortDirection;
         if (customSorting == GridSortOrder.Asc)
         {
            var a = Utility.OrderBy(queryIssueTracker, sortName);
            issues = a.ToList();
         }
         else
         {
            var b = Utility.OrderByDescending(queryIssueTracker, sortName);
            issues = b.ToList();
         }

         if (VitualItemsCount > 0)
         {
            foreach (var issue in issues)
            {
               ExportClass exData = new ExportClass();               

               var id = "TMIT-" + issue.id;
               var siteUid = issue.site == null ? string.Empty : issue.site.site_uid;
               var siteName = issue.site == null ? string.Empty : string.Format("\"{0}\"", issue.site.site_name);

               var summary = "";
               var issueSummaryData = issue.issue_summary.Replace("\n", "").Replace("\r", "");
               issueSummaryData = string.Format("\"{0}\"", issueSummaryData);
               summary = issueSummaryData;

               var issuePriority = issue.issue_priority_types == null ? string.Empty : issue.issue_priority_types.issue_priority_number.Value.ToString();
               var createAt = issue.created_at == null ? string.Empty : issue.created_at.Value.ToString().Split(' ')[0];
               var status = issue.issue_statuses == null ? string.Empty : issue.issue_statuses.issue_status;

               exData.sdata = id + "|" + siteUid + "|" + siteName + "|" + summary + "|" + issuePriority + "|" + createAt + "|" + status + "|";

               resultIssues.Add(exData);
            }                       
         }
         
         return resultIssues;
      }

      #endregion Issue Tracker

      #region LeaseAdmin

      public const string LINK_SITE = "{0}";

      public List<ExportClass> LeaseAdminGetFilteredData(NameValueCollection data, NameValueCollection column, object sender, NameValueCollection formData)
      {
         var ce = new CPTTEntities();
         var result = new List<ExportClass>();

         var sortName = !String.IsNullOrWhiteSpace(data[0]) ? data[0] : "CustomerName";
         const int startIndex = 0;
         GridSortOrder? customSorting = null;

         // Find sorting direction property in data
         if (data.Count > startIndex)
         {
            for (var i = startIndex; i < data.Count; i++)
            {
               if (data.GetKey(i) != Globals.QS_SORT_DIRECTION) continue;

               customSorting = data.Get(i) == Globals.SORT_DIRECTION_VALUE_ASC
                                  ? GridSortOrder.Asc
                                  : GridSortOrder.Desc;
            }
         }

         ObjectQuery<lease_admin_records> queryRecords = ce.lease_admin_records;
         var searchCriteria = LeaseAdminRecord.GetSearchCriteria();
         queryRecords = Utility.BuildObjectQuery(queryRecords, formData, searchCriteria);

         // Since sites are linked through a many to many relationship that the
         // object query cannot load together, we do one final filter manually.
         var filteredList = queryRecords.ToList();

         if (formData.AllKeys.Contains("SiteID"))
         {
            // Reinitialize and build based off of filter.
            filteredList = new List<lease_admin_records>();

            var recordList = queryRecords.ToList();
            var parameterValue = formData["SiteID"];

            foreach (var record in recordList)
            {
               if (!record.sites.Any()) continue;

               if (record.sites.First().site_uid.ToUpper()
                         .Contains(parameterValue.ToUpper()))
               {
                  filteredList.Add(record);
               }
            }
         }

         // Execute query and get count.
         VitualItemsCount = filteredList.Count();

         // Sort Order
         if (VitualItemsCount > 0)
         {
            // Get additional details about lease admin record (site, etc).
            var leaseAdminObj = LeaseAdminTrackerListHandler.GenNewLeaseAdminRecord(filteredList.AsQueryable(), false);
            var iqLeaseAdmin = leaseAdminObj.AsQueryable();
            var records = new List<LeaseAdminList>();

            if (customSorting == null) customSorting = SortDirection;
            if (customSorting == GridSortOrder.Asc)
            {
               var a = Utility.OrderBy(iqLeaseAdmin, sortName);
               records = a.ToList();
            }
            else
            {
               var b = Utility.OrderByDescending(iqLeaseAdmin, sortName);
               records = b.ToList();
            }

            // Convert Result         
            foreach (var leaseAdmin in records)
            {
               var executionDate = "";
               var commencementDate = "";
               var sSiteUid = "";
               var sSiteName = "";

               if (leaseAdmin.SiteId != null)
               {
                  if (leaseAdmin.SiteId.IndexOf("Sites") > 0)
                  {
                     sSiteUid = String.Format(LINK_SITE, leaseAdmin.SiteId);
                     sSiteName = String.Format(LINK_SITE, leaseAdmin.SiteName);
                  }
                  else
                  {
                     sSiteUid = leaseAdmin.SiteId;
                     sSiteName = leaseAdmin.SiteName;
                  }
               }
               else
               {
                  sSiteUid = String.Empty;
                  sSiteName = String.Empty;
               }

               executionDate = Utility.DateToString(leaseAdmin.LeaseExecutionDate);
               commencementDate = Utility.DateToString(leaseAdmin.CommencementDate);
               
               // Build Result
               var objData = new ExportClass();

               objData.sdata = string.Format("\"{0}\"", leaseAdmin.CustomerName) + "|" +
                   string.Format("\"{0}\"", sSiteUid) + "|" +
                   string.Format("\"{0}\"", sSiteName) + "|" +
                   leaseAdmin.Phase + "|" +
                   executionDate + "|" +
                   commencementDate + "|" +
                   leaseAdmin.RevShare + "|" +
                   string.Format("\"{0}\"", leaseAdmin.LeaseAdminExecutionDocumentType) + "|";

               result.Add(objData);
            }
         }

         return result;                  
      }

      public static IQueryable<lease_admin_records> FilterSiteId(IQueryable<lease_admin_records> iqLeaseAdmin, String siteSearch)
      {
         List<lease_admin_records> returnObj = new List<lease_admin_records>();

         foreach (var leaseAdmin in iqLeaseAdmin.ToList())
         {
            if (leaseAdmin.lease_applications != null)
            {
               if (leaseAdmin.lease_applications.site != null)
               {
                  if (siteSearch == leaseAdmin.lease_applications.site.site_uid)
                  {
                     returnObj.Add(leaseAdmin);
                  }
               }
            }
            else
            {
               List<site> lstLeaseAdminSite = leaseAdmin.sites.ToList<site>();
               lstLeaseAdminSite = leaseAdmin.sites.ToList<site>();
               foreach (var site in lstLeaseAdminSite)
               {
                  if (siteSearch == site.site_uid)
                  {
                     returnObj.Add(leaseAdmin);
                     break;
                  }
               }
            }
         }
         return returnObj.AsQueryable();
      }

      #endregion LeaseAdmin

      #region Tower List

      public CPTTEntities CeTower = new CPTTEntities();

      public List<ExportClass> TowerListGetFilteredData(NameValueCollection data, NameValueCollection column, object sender)
      {
         var Ce = new CPTTEntities();
         
         const string GLOBAL_SEARCH_QUERY = "{0}";

         // Paging / Sorting
         string sortName = !String.IsNullOrWhiteSpace(data[0]) ? data[0] : "site_uid";
         if (sortName.IndexOf('.') > -1)
         {
            sortName = sortName.Replace('.', '_');
         }
         string globalSearch = null;
         GridSortOrder? customSorting = null;

         const int startIndex = 0;
         var sb = new StringBuilder();

         String siteSearch = String.Empty;
         bool bIsSearchSite = false;
         PreapareFilter(eExportType.eTowerlist, data, startIndex, ref customSorting, ref globalSearch, ref sb, ref bIsSearchSite, ref siteSearch);

         List<ExportClass> exportData = new List<ExportClass>();

         var result = new List<KeyValuePair<string, object[]>>();
         List<ExportTowerList> lstTower = new List<ExportTowerList>();

         if (!SubFilterHasNoResults)
         {
            IQueryable<tower_list> iqTower;
            if (sb.Length > 0)
            {
               iqTower = globalSearch == null ? CeTower.tower_list.Where(sb.ToString()) : Ce.tower_list.Where(String.Format(GLOBAL_SEARCH_QUERY, globalSearch)).Where(sb.ToString());
            }
            else
            {
               iqTower = globalSearch == null ? CeTower.tower_list : Ce.tower_list.Where(String.Format(GLOBAL_SEARCH_QUERY, globalSearch));
            }

            VitualItemsCount = iqTower.Count();

            IQueryable<ExportTowerList> tmp = from i in iqTower
                                              select new ExportTowerList
                                              {
                                                 site_uid = i.site_uid,
                                                 site_name = i.site_name,
                                                 site_class_desc = i.site_class_desc,
                                                 market_name = i.market_name,
                                                 status_desc = i.site_status_desc,
                                                 trackit_list_statuses = i.trackit_override_list_status_id == null ? i.trackit_default_list_status == null ? string.Empty : i.trackit_default_list_status.list_status : i.trackit_override_list_status.list_status,
                                                 map_list_statuses = i.map_override_list_status_id == null ? i.map_default_list_status == null ? string.Empty : i.map_default_list_status.list_status : i.map_override_list_status.list_status
                                              };

            if (customSorting == null) customSorting = this.SortDirection;
            if (customSorting == GridSortOrder.Asc)
            {
               var a = Utility.OrderBy(tmp, sortName);
               lstTower = a.ToList<ExportTowerList>();
            }
            else
            {
               var b = Utility.OrderByDescending(tmp, sortName);
               lstTower = b.ToList<ExportTowerList>();
            }

            try
            {
               foreach (var tower in lstTower)
               {
                  ExportClass exData = new ExportClass();

                  string siteUid = tower.site_uid == null ? string.Empty : tower.site_uid;
                  string siteName = tower.site_name == null ? string.Empty : string.Format("\"{0}\"", tower.site_name);
                  string site_class_desc = tower.site_class_desc == null ? string.Empty : string.Format("\"{0}\"", tower.site_class_desc);
                  string market_name = tower.market_name == null ? string.Empty : string.Format("\"{0}\"", tower.market_name);
                  string status_desc = tower.status_desc == null ? string.Empty : string.Format("\"{0}\"", tower.status_desc);
                  string trackit_list_statuses = tower.trackit_list_statuses == null ? string.Empty : string.Format("\"{0}\"", tower.trackit_list_statuses);
                  string map_list_statuses = tower.map_list_statuses == null ? string.Empty : string.Format("\"{0}\"", tower.map_list_statuses);


                  exData.sdata = siteUid + "|" + siteName + "|" + site_class_desc + "|" + market_name + "|" + status_desc + "|" + trackit_list_statuses + "|" + map_list_statuses + "|";

                  exportData.Add(exData);
               }
            }
            catch (Exception ex)
            {
               throw ex;
            }
         }
         return exportData;
      }

      #endregion Tower List

      #region Private Export function
      public override IEnumerable<KeyValuePair<string, object[]>> GetFilteredData()
      {
         var result = new List<KeyValuePair<string, object[]>>();

         return result;
      }

      private void PreapareFilter(eExportType etype, NameValueCollection data, int startIndex, ref GridSortOrder? customSorting, ref string globalSearch, ref StringBuilder sb, ref bool bisSiteSearch, ref String searchSiteId)
      {
         if (data.Count > startIndex)
         {
            for (int i = startIndex; i < data.Count; i++)
            {
               if (data.GetKey(i) == Globals.QS_PROJECT_CATEGORY)
               {
                  //Do nothing. 
               }
               else if (data.GetKey(i) == Globals.QS_GLOBAL_SEARCH)
               {
                  globalSearch = Utility.PrepareSqlSearchString(data.Get(i));
               }
               else if (data.GetKey(i) == Globals.QS_SORT_DIRECTION)
               {
                  if (data.Get(i) == Globals.SORT_DIRECTION_VALUE_ASC)
                  {
                     customSorting = GridSortOrder.Asc;
                  }
                  else
                  {
                     customSorting = GridSortOrder.Desc;
                  }
               }
               else
               {
                  switch (etype)
                  {
                     case eExportType.eTowerlist:
                        TowerListHandler.buildSearchFilterWhereClause(sb, data.GetKey(i), Utility.PrepareSqlSearchString(data.Get(i)));
                        break;
                  }
               }
            }
         }
         //>
      }

      #endregion

      #region Pulic Export Function
      public static NameValueCollection GetColumn(string hiddenColumnValue, eExportType type = eExportType.eElse)
      {
         NameValueCollection data = new NameValueCollection();
         var column = hiddenColumnValue;
         if (!string.IsNullOrEmpty(column))
         {
            var jss = new JavaScriptSerializer();
            var table = jss.Deserialize<dynamic>(column);

            if (type != eExportType.eTowerlist)
            {
               for (int x = 0; x < table.Length; x++)
               {
                  string name = table[x]["name"];
                  if (name.Split('.').Length > 1)
                  {
                     name = name.Split('.')[name.Split('.').Length - 1];
                  }
                  data.Add(name, table[x]["value"]);
               }
            }
            else
            {
               for (int x = 0; x < table.Length; x++)
               {
                  string name = table[x]["name"];
                  if (name.Split('.').Length > 1)
                  {
                     if (name == "trackit_override_list_status.list_status")
                     {
                        name = "trackit_list_statuses";
                     }
                     else if (name == "map_override_list_status.list_status")
                     {
                        name = "map_list_statuses";
                     }
                     else
                     {
                        name = name.Split('.')[name.Split('.').Length - 1];
                     }
                  }
                  data.Add(name, table[x]["value"]);
               }
            }
         }

         return data;
      }

      public string GetLeaseAppRowFormat(NameValueCollection column)
      {
         string currentRow = "";
         foreach (string c in column)
         {
            switch (c)
            {
               case "site_uid":
                  currentRow += "{0},";
                  break;
               case "site_name":
                  currentRow += "{1},";
                  break;
               case "customer_name":
                  currentRow += "{2},";
                  break;
               case "original_carrier":
                  currentRow += "{3},";
                  break;
               case "lease_application_type":
                  currentRow += "{4},";
                  break;
               case "lease_app_activity":
                  currentRow += "{5},";
                  break;
               case "leaseapp_rcvd_date":
                  currentRow += "{6},";
                  break;
               case "lease_application_status":
                  currentRow += "{7},";
                  break;
               case "tower_modification_yn":
                  currentRow += "{8},";
                  break;
               case "rogue_equipment_yn":
                  currentRow += "{9},";
                  break;
               case "Count":
                  currentRow += "{10},";
                  break;
            }
         }
         return currentRow;
      }

      public string GetSiteRowFormat(NameValueCollection column)
      {
         string currentRow = "";
         foreach (string c in column)
         {
            switch (c)
            {
               case "site_uid":
                  currentRow += "{0},";
                  break;
               case "site_name":
                  currentRow += "{1},";
                  break;
               case "market_name":
                  currentRow += "{2},";
                  break;
               case "site_class_desc":
                  currentRow += "{3},";
                  break;
               case "site_status_desc":
                  currentRow += "{4},";
                  break;
               case "tower_modification_yn":
                  currentRow += "{5},";
                  break;
               case "rogue_equipment_yn":
                  currentRow += "{6},";
                  break;                
            }
         }
         return currentRow;
      }

      public string GetSiteDataPackageRowFormat(NameValueCollection column)
      {
         string currentRow = "";
         foreach (string c in column)
         {
            switch (c)
            {
               case "site_uid":
                  currentRow += "{0},";
                  break;
               case "region_name":
                  currentRow += "{1},";
                  break;
               case "market_code":
                  currentRow += "{2},";
                  break;
               case "site_on_air_date":
                  currentRow += "{3},";
                  break;
               case "sdp_priority":
                  currentRow += "{4},";
                  break;
               case "sdp_request_date":
                  currentRow += "{5},";
                  break;
               case "sdp_special_project":
                  currentRow += "{6},";
                  break;
               case "user6_last_name":
                  currentRow += "{7},";
                  break;
               case "user7_last_name":
                  currentRow += "{8},";
                  break;
            }
         }
         return currentRow;
      }

      public string GetTowerModRowFormat(NameValueCollection column)
      {
         string currentRow = "";
         foreach (string c in column)
         {
            switch (c)
            {
               case "site_uid":
                  currentRow += "{0},";
                  break;
               case "customer_name":
                  currentRow += "{1},";
                  break;
               case "tower_mod_status_name":
                  currentRow += "{2},";
                  break;
               case "tower_mod_type":
                  currentRow += "{3},";
                  break;
               case "phase_name":
                  currentRow += "{4},";
                  break;
               case "tower_mod_pm_user_name":
                  currentRow += "{5},";
                  break;
               case "dateCurPackageRcvd":
                  currentRow += "{6},";
                  break;
               case "project_complete_date":
                  currentRow += "{7},";
                  break;
               case "rogue_equipment_yn":
                  currentRow += "{8},";
                  break;
            }
         }
         return currentRow;
      }

      public string GetIssueRowFormat(NameValueCollection column)
      {
         string currentRow = "";
         foreach (string c in column)
         {
            switch (c)
            {
               case "id":
                  currentRow += "{0},";
                  break;
               case "site_uid":
                  currentRow += "{1},";
                  break;
               case "site_name":
                  currentRow += "{2},";
                  break;
               case "issue_summary":
                  currentRow += "{3},";
                  break;
               case "issue_priority_types_id":
                  currentRow += "{4},";
                  break;
               case "created_at":
                  currentRow += "{5},";
                  break;
               case "issue_statuses_id":
                  currentRow += "{6},";
                  break;
            }
         }
         return currentRow;
      }

      public string GetLeaseAdminRowFormat(NameValueCollection column)
      {
         string currentRow = "";
         foreach (string c in column)
         {
            switch (c)
            {
               case "CustomerName":
                  currentRow += "{0},";
                  break;
               case "SiteId":
                  currentRow += "{1},";
                  break;
               case "SiteName":
                  currentRow += "{2},";
                  break;
               case "Phase":
                  currentRow += "{3},";
                  break;
               case "LeaseExecutionDate":
                  currentRow += "{4},";
                  break;
               case "CommencementDate":
                  currentRow += "{5},";
                  break;
               case "RevShare":
                  currentRow += "{6},";
                  break;
               case "DocumentType":
                  currentRow += "{7},";
                  break;
            }
         }

         return currentRow;
      }

      public string GetTowerListRowFormat(NameValueCollection column)
      {
         string currentRow = "";
         foreach (string c in column)
         {
            switch (c)
            {
               case "site_uid":
                  currentRow += "{0},";
                  break;
               case "site_name":
                  currentRow += "{1},";
                  break;
               case "site_class_desc":
                  currentRow += "{2},";
                  break;
               case "site_market_name":
                  currentRow += "{3},";
                  break;
               case "site_status_desc":
                  currentRow += "{4},";
                  break;
               case "trackit_list_statuses":
                  currentRow += "{5},";
                  break;
               case "map_list_statuses":
                  currentRow += "{6},";
                  break;
            }
         }
         return currentRow;
      }

      public MemoryStream WriteReport(List<ExportClass> data, NameValueCollection column, eExportType exportType, string location)
      {
         MemoryStream memoryStream = new MemoryStream();
         StreamWriter tw = new StreamWriter(memoryStream, Encoding.Unicode);
         try
         {
            string line = "";
            string rowData = "";

            switch (exportType)
            {
               case eExportType.eLeaseApp:
                  rowData = GetLeaseAppRowFormat(column);
                  break;
               case eExportType.eSite:
                  rowData = GetSiteRowFormat(column);
                  break;
               case eExportType.eSiteDataPackage:
                  rowData = GetSiteDataPackageRowFormat(column);
                  break;
               case eExportType.eTowerMod:
                  rowData = GetTowerModRowFormat(column);
                  break;
               case eExportType.eIssueTracker:
                  rowData = GetIssueRowFormat(column);
                  break;
               case eExportType.eLeaseAdmin:
                  rowData = GetLeaseAdminRowFormat(column);
                  break;
               case eExportType.eTowerlist:
                  rowData = GetTowerListRowFormat(column);
                  break;
            }

            if (rowData != "")
            {
               rowData = rowData.Substring(0, rowData.Length - 1);
            }

            foreach (var c in column)
            {
               if (column[c.ToString()] != "none" && c.ToString() != "")
               {
                  line += column[c.ToString()] + ",";
               }
            }
            if (line.IndexOf("\n") != -1)
            {
               line = line.Replace("\n", "");
            }

            if (line != "")
            {
               line = line.Substring(0, line.Length - 1);
            }

            tw.WriteLine(line);

            foreach (ExportClass reformatData in data)
            {
               tw.WriteLine(string.Format(rowData, reformatData.sdata.Split('|')));
            }

         }
         catch (Exception ex)
         {
            // Log error in ELMAH for any diagnostic needs.
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            memoryStream = null;
         }
         finally
         {
            tw.Close();
            memoryStream.Close();
         }
         return memoryStream;
      }

      public static string GenarateFileName(string str)
      {
         string result = "TrackiT2-{0}-Export-{1}";

         str = str.Replace(" ", "-");
         DateTime dateNow = DateTime.Now;

         string dateTime = String.Format("{0:yyyy-MM-dd-HH-mm-ss}", dateNow);
         result = result.Replace("{0}", str).Replace("{1}", dateTime);
         return result;
      }

      #endregion
   }

   #endregion

   #region model Export Class
   public class ExportClass
   {
      public string sdata { set; get; }
      public ExportClass()
      {

      }
   }
   #endregion

   #region model Export Tower Mod
   public class ExportTowerMod
   {
      public int id { get; set; }
      public customer customer { get; set; }
      public string site_uid { get; set; }
      public tower_mod_statuses tower_mod_statuses { get; set; }
      public tower_mod_types tower_mod_types { get; set; }
      public user tower_mod_pm_user { get; set; }
      public DateTime? project_complete_date { get; set; }
      public site site { get; set; }
      public DateTime? ebid_req_rcvd_date { get; set; }
      public tower_mod_phase_types tower_mod_phase_types { get; set; }
      public tower_mod_pos tower_mod_pos { get; set; }
      public string customer_name { get; set; }
      public string tower_mod_status_name { get; set; }
      public string tower_mod_type { get; set; }
      public string phase_name { get; set; }
      public string tower_mod_pm_user_name { get; set; }
      public DateTime? dateCurPackageRcvd { get; set; }
      public string rogue_equipment_yn { get; set; }

      public ExportTowerMod()
      {

      }
   }
   #endregion model Export Tower Mod

   #region model Export Issue

   public class ExportIssue
   {
      public int id { get; set; }
      public string site_uid { get; set; }
      public string site_name { get; set; }
      public string issue_summary { get; set; }
      public int? issue_priority_types_id { get; set; }
      public DateTime? created_at { get; set; }
      public string issue_statuses_id { get; set; }

      public ExportIssue()
      {

      }
   }
   #endregion model Export Issue

   #region model Export Tower List

   public class ExportTowerList
   {
      public string site_uid { get; set; }
      public string site_name { get; set; }
      public string site_class_desc { get; set; }
      public string market_name { get; set; }
      public string status_desc { get; set; }
      public string trackit_list_statuses { get; set; }
      public string map_list_statuses { get; set; }

      public ExportTowerList()
      {

      }
   }
   #endregion model Export Tower List

}