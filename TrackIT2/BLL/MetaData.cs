﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using TrackIT2.DAL;
using TrackIT2.Objects;

namespace TrackIT2.BLL
{
   public class MetaData
   {
      public static void Add(string table, string column, string friendlyName, int groupId)
      {
         // TODO: Add additional security/business logic in here as needed.
         CPTTEntities ce = new CPTTEntities();

         metadata field = new metadata();

         field.table = table;
         field.column = column;
         field.friendly_name = friendlyName;
         field.metagroup_id = groupId;

         ce.metadatas.AddObject(field);

         // Exceptions are thrown up to the calling method to handle. Elmah will log
         // unhandled execptions.
         try
         {
            ce.SaveChanges();
         }
         catch (EntitySqlException)
         {            
            throw;
         }
      }

      public static void Update(metadata field, string table, string column, string friendlyName)
      {
         // TODO: Add additional security/business logic in here as needed.

         // The current meta group is passed in before applying edits in order
         // to do any additional validation/processing.
         CPTTEntities ce = new CPTTEntities();

         ce.metadatas.Attach(field);

         field.table = table;
         field.column = column;
         field.friendly_name = friendlyName;

         // Exceptions are thrown up to the calling method to handle. Elmah will log
         // unhandled execptions.
         try
         {
            ce.SaveChanges();
         }
         catch (OptimisticConcurrencyException)
         {
            throw;
         }
         catch (EntitySqlException)
         {
            throw;
         }
      }

      public static void Delete(int id)
      {
         // TODO: Add additional security/business logic in here as needed.
         CPTTEntities ce = new CPTTEntities();

         metadata field;

         field = ce.metadatas
                   .Where(md => md.id.Equals(id))
                   .First();

         ce.DeleteObject(field);
         ce.SaveChanges();
      }

      public static List<metadata> GetViewableFieldsByCategory(ProjectCategory category)
      {
         CPTTEntities ce = new CPTTEntities();
         List<metadata> results = new List<metadata>();
         string metaGroup = "";

         switch (category)
         {
            //case ProjectCategory.CostShareOnly:
            //   metaGroup = "Cost Project Category Fields";
            //   break;

            case ProjectCategory.GroundModFiber:
               metaGroup = "Ground Project Category Fields";
               break;

            case ProjectCategory.Initial:
               metaGroup = "Initial Project Category Fields";
               break;

            case ProjectCategory.Mod:
               metaGroup = "Mod Project Category Fields";
               break;

            case ProjectCategory.SprintNV:
               metaGroup = "Sprint Project Category Fields";
               break;

            case ProjectCategory.TMOPremod:
               metaGroup = "TMO Premod Project Category Fields";
               break;                             
         }

         results = ce.metadatas
                     .Where(m => m.metagroup.name.Equals(metaGroup))
                     .ToList();        

         return results;
      }

      public static List<metadata> GetViewableFieldsByCategory(ProjectCategory category, List<metadata> metadataList)
      {
          List<metadata> results = new List<metadata>();
          string metaGroup = "";

          switch (category)
          {
              //case ProjectCategory.CostShareOnly:
              //   metaGroup = "Cost Project Category Fields";
              //   break;

              case ProjectCategory.GroundModFiber:
                  metaGroup = "Ground Project Category Fields";
                  break;

              case ProjectCategory.Initial:
                  metaGroup = "Initial Project Category Fields";
                  break;

              case ProjectCategory.Mod:
                  metaGroup = "Mod Project Category Fields";
                  break;

              case ProjectCategory.SprintNV:
                  metaGroup = "Sprint Project Category Fields";
                  break;

              case ProjectCategory.TMOPremod:
                  metaGroup = "TMO Premod Project Category Fields";
                  break;
          }

          results = metadataList
                      .Where(m => m.metagroup.name.Equals(metaGroup))
                      .ToList();

          return results;
      }


       public static List<metadata> GetAllMetadatasList()
       {
           CPTTEntities ce = new CPTTEntities();
           List<metadata> metadatasList = new List<metadata>();
           metadatasList = ce.metadatas.ToList();
           return metadatasList;
       }

   }
}