﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TrackIT2.DAL;
using TrackIT2.Objects;
using System.Text;
// update master
namespace TrackIT2.BLL
{
    public static class RelatedWorkFlow
    {
        public const string SOURCE_STRING_LEASE_APP = "LeaseApp";
        public const string SOURCE_STRING_TOWER_MOD = "TowerMod";
        public const string SOURCE_STRING_SDP = "SDP";
        public const string TEXT_STRING_LEASE_APP = "Lease Application";
        public const string TEXT_STRING_TOWER_MOD = "Tower Modification";
        public const string TEXT_STRING_SDP = "Site Data Package";
        public const string TEXT_STRING_ISSUE_TRACKER = "Open Issues";
        public const string TEXT_STRING_LEASE_ADMIN = "Lease Admin Tracker";
        public const string TEXT_STRING_DMS = "Site Documents";

        public const string PATH_LIST_LEASE_APP = @"../LeaseApplications/?filter=txtSiteID|";
        public const string PATH_EDIT_LEASE_APP = @"../LeaseApplications/Edit.aspx?id=";
        public const string PATH_LIST_TOWER_MOD = @"../TowerModifications/?filter=txtSiteID|";
        public const string PATH_EDIT_TOWER_MOD = @"../TowerModifications/Edit.aspx?id=";
        public const string PATH_LIST_SDP = @"../SiteDataPackages/?filter=txtSiteID|";
        public const string PATH_EDIT_SDP = @"../SiteDataPackages/Edit.aspx?id=";
        public const string PATH_LIST_ISSUE_TRACKER = @"../IssueTracker/?filter=txtSiteID|";
        public const string PATH_EDIT_ISSUE_TRACKER = @"../IssueTracker/Edit.aspx?id=";
        public const string PATH_LIST_LEASE_ADMIN = @"../LeaseAdminTracker/?filter=txtSiteID|";
        public const string PATH_EDIT_LEASE_ADMIN = @"../LeaseAdminTracker/Edit.aspx?id=";
        public const string PATH_DMS = @"../DMS/#/{0}/";

        public const string LIST_HEADER = "<li>Site Workflows</li>";
        public const string LIST_HEADER_APPLICATION = "<li>Application Workflows</li>";
        public const string LIST_LEASE_APP = "<li class=\"work-flow-item\"><a id=\"link_la\" href=\"{0}\" >{1}</a></li>";
        public const string LIST_LEASE_APP_NONE = "<li>No Lease Application(s) Assigned.</li>";
        public const string LIST_TOWER_MOD = "<li class=\"work-flow-item\"><a id=\"link_tw\" href=\"{0}\" >{1}</a></li>";
        public const string LIST_TOWER_MOD_NONE = "<li>No Tower Modification(s) Assigned.</li>";
        public const string LIST_SDP = "<li class=\"work-flow-item\"><a id=\"link_sdp\" href=\"{0}\" >{1}</a></li>";
        public const string LIST_SDP_NONE = "<li>No Site Data Package(s) Assigned.</li>";
        public const string LIST_ISSUE_TRACKER = "<li class=\"work-flow-item\"><a id=\"link_issue\" href=\"{0}\" >{1}</a></li>";
        public const string LIST_ISSUE_TRACKER_NONE = "<li>No Issue(s) Assigned.</li>";
        public const string LIST_LEASE_ADMIN = "<li class=\"work-flow-item\"><a id=\"link_lease_admin\" href=\"{0}\" >{1}</a></li>";
        public const string LIST_LEASE_ADMIN_NONE = "<li>No Lease Admin(s) Assigned.</li>";
        public const string LIST_DMS = "<li><a id=\"link_dms\" href=\"{0}\" >{1}</a></li>";

        public const string GRID_FORMAT = "<div><ul>{0}</ul></div>";

        /// <summary>
        /// Get the innerHTML of ul control to create list of links for site workflows
        /// </summary>
        /// <param name="site_uid">Site ID</param>
        /// <returns>html control in string</returns>
        public static string GetSiteWorkflowsList(string site_uid,string sFirstLineText = LIST_HEADER,CPTTEntities ceRef = null)
        {
            StringBuilder ret = new StringBuilder(sFirstLineText);
            bool hasRecords = false;

            bool isRootTransaction = true;
            CPTTEntities ce;
            if (ceRef == null)
            {
                //using new entity object
                ce = new CPTTEntities();
            }
            else
            {
                //using current entity object
                ce = ceRef;
                isRootTransaction = false;
            }
            if (isRootTransaction)
            {
                ce.Connection.Open();
            }

            try
            {
               //Lease Applications
               var queryLeaseApp = (from app in ce.lease_applications
                                   where app.site_uid == site_uid
                                   select app).ToList();
               if (queryLeaseApp.Count > 0)
               {
                  hasRecords = true;
                  if (queryLeaseApp.Count > 1)
                  {
                     //More than one
                     ret.AppendFormat(LIST_LEASE_APP, PATH_LIST_LEASE_APP + site_uid, "(" + queryLeaseApp.Count + ") " + TEXT_STRING_LEASE_APP + "s");
                  }
                  else
                  {
                     //Excatly 1 
                     ret.AppendFormat(LIST_LEASE_APP, PATH_EDIT_LEASE_APP + ((lease_applications)queryLeaseApp[0]).id, "(1) " + TEXT_STRING_LEASE_APP);
                  }
               }
               //--> End Lease Applications

               //Tower Modifications
               var queryTwMod = (from tw in ce.tower_modifications
                                where tw.site_uid == site_uid
                                select tw).ToList();
               if (queryTwMod.Count > 0)
               {
                  hasRecords = true;
                  if (queryTwMod.Count > 1)
                  {
                     //More than one
                     ret.AppendFormat(LIST_TOWER_MOD, PATH_LIST_TOWER_MOD + site_uid, "(" + queryTwMod.Count + ") " + TEXT_STRING_TOWER_MOD + "s");
                  }
                  else
                  {
                     //Excatly 1 
                     ret.AppendFormat(LIST_TOWER_MOD, PATH_EDIT_TOWER_MOD + ((tower_modifications)queryTwMod[0]).id, "(1) " + TEXT_STRING_TOWER_MOD);
                  }
               }
               //--> End Tower Modifications

               //SDP
               var querySDP = (from sdp in ce.site_data_packages
                              where sdp.site_uid == site_uid
                              select sdp).ToList();
               if (querySDP.Count > 0)
               {
                  hasRecords = true;
                  if (querySDP.Count > 1)
                  {
                     //More than one
                     ret.AppendFormat(LIST_SDP, PATH_LIST_SDP + site_uid, "(" + querySDP.Count + ") " + TEXT_STRING_SDP + "s");
                  }
                  else
                  {
                     //Excatly 1 
                     ret.AppendFormat(LIST_SDP, PATH_EDIT_SDP + ((site_data_packages)querySDP[0]).id, "(1) " + TEXT_STRING_SDP);
                  }
               }
               //--> End SDP

               //Issue Tracker
               var queryIssue = (from issue in ce.issue_tracker
                                where issue.site_uid == site_uid
                                select issue).ToList();
               if (queryIssue.Count > 0)
               {
                  hasRecords = true;
                  if (queryIssue.Count > 1)
                  {
                     //More than one
                     ret.AppendFormat(LIST_ISSUE_TRACKER, PATH_LIST_ISSUE_TRACKER + site_uid, "(" + queryIssue.Count + ") " + TEXT_STRING_ISSUE_TRACKER + "s");
                  }
                  else
                  {
                     //Excatly 1 
                     ret.AppendFormat(LIST_ISSUE_TRACKER, PATH_EDIT_ISSUE_TRACKER + ((issue_tracker)queryIssue[0]).id, "(1) " + TEXT_STRING_ISSUE_TRACKER);
                  }
               }
               //Issue Tracker

               //Lease Admin
               var queryLeaseAdmin = (from le in ce.lease_admin_records
                                     from s in le.sites
                                     where s.site_uid == site_uid
                                     select le).ToList();
               if (queryLeaseAdmin.Any())
               {
                  hasRecords = true;
                  if (queryLeaseAdmin.Count > 1)
                  {
                     //More than one
                     ret.AppendFormat(LIST_LEASE_ADMIN, PATH_LIST_LEASE_ADMIN + site_uid, "(" + queryLeaseAdmin.Count + ") " + TEXT_STRING_LEASE_ADMIN + "s");
                  }
                  else
                  {
                     //Excatly 1 
                     ret.AppendFormat(LIST_LEASE_ADMIN, PATH_EDIT_LEASE_ADMIN + ((lease_admin_records)queryLeaseAdmin[0]).id, "(1) " + TEXT_STRING_LEASE_ADMIN);
                  }
               }
               //Lease Admin
                
               //DMS
                ret.AppendFormat(LIST_DMS, PATH_DMS.Replace("{0}", site_uid), TEXT_STRING_DMS);

               //--> End DMS

               // If no records were found, clear out the Application Workflows header
               // auto appendedin the start of the method.
               if (!hasRecords)
               {
                  ret.Clear();
               }
               else
               {
                  ret.Append("<li>&nbsp;</li>");
               }
            }
            catch (Exception ex)
            {
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                ret = null;
            }
            finally
            {
                if (isRootTransaction)
                {
                    ce.Connection.Dispose();
                }
            }
            if (isRootTransaction)
            {
                ce.Dispose();
            } 
   
            return ret.ToString();
        }

        public static string GetSiteWorkflowsForGrid(string site_uid)
        {
            return string.Format(GRID_FORMAT,GetSiteWorkflowsList(site_uid,""));
        }

        public static string GetApplicationWorkflowsForLA(int la_id)
        {
            StringBuilder ret = new StringBuilder(LIST_HEADER_APPLICATION);
            bool hasRecords = false;

            using (CPTTEntities ce = new CPTTEntities())
            {
                var querySDP = from sdp in ce.site_data_packages
                               where sdp.lease_application_id == la_id
                               select sdp;
                if (querySDP.ToList().Count > 0)
                {
                   hasRecords = true; 
                   if (querySDP.ToList().Count > 1)
                    {
                        //More than one
                        ret.AppendFormat(LIST_SDP, PATH_LIST_SDP + la_id, "(" + querySDP.ToList().Count + ") " + TEXT_STRING_SDP + "s");
                    }
                    else
                    {
                        //Excatly 1 
                        ret.AppendFormat(LIST_SDP, PATH_EDIT_SDP + ((site_data_packages)querySDP.ToList()[0]).id, "(1) " + TEXT_STRING_SDP);
                    }
                }
                //--> End SDP

                // Tower Modifications
               List<tower_modifications> lstTowerMod = new List<tower_modifications>();
               lstTowerMod = LeaseAppTowerMod.GetTowerModLA(la_id);
              
               if (lstTowerMod.Count > 0)
               {
                  hasRecords = true;
                  if (lstTowerMod.ToList().Count > 1)
                  {
                     //More than one
                     ret.AppendFormat(LIST_TOWER_MOD, PATH_LIST_TOWER_MOD + la_id, "(" + lstTowerMod.Count + ") " + TEXT_STRING_TOWER_MOD + "s");
                  }
                  else
                  {
                     //Excatly 1 
                     ret.AppendFormat(LIST_TOWER_MOD, PATH_EDIT_TOWER_MOD + lstTowerMod[0].id, "(1) " + TEXT_STRING_TOWER_MOD);
                  }
               }
               //--> End Tower Modification

                //Issue Tracker
                List<issue_tracker> queryIssue = (from issue in ce.issue_tracker
                                                 join la in ce.lease_applications on la_id equals la.id
                                                 where issue.site_uid == la.site_uid
                                                 select issue).ToList();
                if (queryIssue.Count > 0)
                {
                    hasRecords = true;
                    if (queryIssue.Count > 1)
                    {
                        //More than one
                        issue_tracker it = (issue_tracker)queryIssue.ToList()[0];
                        ret.AppendFormat(LIST_ISSUE_TRACKER, PATH_LIST_ISSUE_TRACKER + it.site_uid, "(" + queryIssue.ToList().Count + ") " + TEXT_STRING_ISSUE_TRACKER + "s");
                    }
                    else
                    {
                        //Excatly 1 
                        ret.AppendFormat(LIST_ISSUE_TRACKER, PATH_EDIT_ISSUE_TRACKER + ((issue_tracker)queryIssue.ToList()[0]).id, "(1) " + TEXT_STRING_ISSUE_TRACKER);
                    }
                }
                //Issue Tracker

                //Lease Admin
                List<lease_admin_records> queryLeaseAdmin = (from le in ce.lease_admin_records
                                                             where le.lease_application_id == la_id
                                                             select le).ToList();
                if (queryLeaseAdmin.Any())
                {
                    hasRecords = true;
                    if (queryLeaseAdmin.Count > 1)
                    {
                        //More than one * Abnormal case should have only 1
                        ret.AppendFormat(LIST_LEASE_ADMIN, PATH_LIST_LEASE_ADMIN + ((lease_admin_records)queryLeaseAdmin.ToList()[0]).id, "(" + queryLeaseAdmin.ToList().Count + ") " + TEXT_STRING_LEASE_ADMIN + "s");
                    }
                    else
                    {
                        //Excatly 1 
                        ret.AppendFormat(LIST_LEASE_ADMIN, PATH_EDIT_LEASE_ADMIN + ((lease_admin_records)queryLeaseAdmin.ToList()[0]).id, "(1) " + TEXT_STRING_LEASE_ADMIN);
                    }
                }
            }
           // If no records were found, clear out the Application Workflows header
           // auto appendedin the start of the method.
            if (!hasRecords)
            {
               ret.Clear();
            }
            else
            {
               ret.Append("<li>&nbsp;</li>");
            }

            return ret.ToString();
        }
    }


    public class RelatedWorkeFlowWrapper
    {
       public IEnumerable <lease_applications> LeaseApp { get; set; }
       public IEnumerable<tower_modifications> TWMod { get; set; }
       public IEnumerable<site_data_packages> SDP { get; set; }
       public IEnumerable<issue_tracker> Issue { get; set; }
       public IEnumerable<lease_admin_records> LeaseAdmin { get; set; }
    }

}