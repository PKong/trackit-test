﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TrackIT2.DAL;

namespace TrackIT2.BLL
{
    public class SdpComment
    {
        public static List<sdp_comments> GetComments(int? sdp_id)
        {
            List<sdp_comments> listRet = new List<sdp_comments>();
            if (sdp_id.HasValue)
            {
                using (CPTTEntities context = new CPTTEntities())
                {
                    var query = from sdp in context.sdp_comments
                                where sdp.site_data_package_id == sdp_id.Value
                                orderby sdp.created_at descending
                                select sdp;
                    listRet.AddRange(query.ToList<sdp_comments>());
                }
            }
            return listRet;
        }

        public static void Add(sdp_comments objComment)
        {
            // TODO: Add additional security/business logic in here as needed.
            // TODO: Add additional fields into database insert.
            using (CPTTEntities ce = new CPTTEntities())
            {
                HttpContext context = HttpContext.Current;
                ce.sdp_comments.AddObject(objComment);
                ce.SaveChanges();
            }
        }

        public static void Delete(int id)
        {
            using (CPTTEntities ce = new CPTTEntities())
            {
                var query = from c in ce.sdp_comments
                            where c.id == id
                            select c;
                if (query.ToList().Count > 0)
                {
                    sdp_comments result = query.ToList()[0];
                    ce.sdp_comments.DeleteObject(result);
                    ce.SaveChanges();
                }
            }
        }

        public static List<AllComments> GetSdpCommentBySdp(int sdpId)
        {
            List<AllComments> returnResult = new List<AllComments>();
            using (CPTTEntities ce = new CPTTEntities())
            {
                var sdpAllComment = (from sdpComment in ce.sdp_comments
                                from sdp in ce.site_data_packages
                                where sdpComment.site_data_package_id == sdp.id && sdp.id == sdpId
                                select new AllComments
                                {
                                    CommentID = sdpComment.id,
                                    LeaseApplicationID = null,
                                    StructuralAnalysisID = null,
                                    LeaseAddminID = null,
                                    SdpID = sdpComment.site_data_package_id,
                                    TowerModID = null,
                                    Comments = sdpComment.comments,
                                    CreatorID = sdpComment.creator_id,
                                    CreatorName = sdpComment.user.last_name + ", " + sdpComment.user.first_name,
                                    CreateAt = sdpComment.created_at,
                                    CreatorUsername = sdpComment.user.tmo_userid,
                                    UpdaterID = sdpComment.updater_id,
                                    UpdaterName = sdpComment.user1.last_name + ", " + sdpComment.user1.first_name,
                                    UpdateUsername = sdpComment.user1.tmo_userid,
                                    UpdateAt = sdpComment.updated_at,
                                    SiteID = sdp.site_uid,
                                    SiteName = sdp.site != null ? sdp.site.site_name : "",
                                    CustomerName = sdp.customer.customer_name,
                                    Mode = "eSDP"
                                }).OrderByDescending(Utility.GetDateField<AllComments>("CreateAt"));

                if (sdpAllComment != null)
                {
                    returnResult = sdpAllComment.ToList();
                }
                return returnResult;
            }
        }
    }
}