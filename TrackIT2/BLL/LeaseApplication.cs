﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Linq;
using TrackIT2.DAL;
using TrackIT2.Objects;

namespace TrackIT2.BLL
{
   public static class LeaseApplication
   {      
		 public static lease_applications Search(int id)
		 {
          lease_applications ret = null;

			 using (var ce = new CPTTEntities())
			 {
             if (ce.lease_applications.Any(la => la.id.Equals(id)))
             {
                ret = ce.lease_applications.First(la => la.id.Equals(id));
             }             
			 }

			 return ret;
		 }

       public static lease_applications SearchWithinTransaction(int id, CPTTEntities ceRef)
       {
          lease_applications ret = null;

          if (ceRef.lease_applications.Any(la => la.id.Equals(id)))
          {
             ret = ceRef.lease_applications.FirstOrDefault(la => la.id.Equals(id));
          }

          return ret;
       }

       public static lease_applications SearchByTMOAppId(String TMOAppId)
       {
           lease_applications ret = null;

           using (var ce = new CPTTEntities())
           {
               if (ce.lease_applications.Any(la => la.TMO_AppID.Equals(TMOAppId)))
               {
                   ret = ce.lease_applications.First(la => la.TMO_AppID.Equals(TMOAppId));
               }
           }
           return ret;
       }

		 public static string Add(lease_applications application)
		 {
          String results = null;

		    // Start transaction for update.
           using (var ce = new CPTTEntities())
           {
              var isSuccess = false;

              try
              {
                 using (var trans = TransactionUtils.CreateTransactionScope())
                 {
                    // Manually open connection to prevent EF from auto closing
                    // connection and causing issues with future queries.
                    ce.Connection.Open();

				        ce.lease_applications.AddObject(application);
                    ce.SaveChanges(SaveOptions.DetectChangesBeforeSave);
                    isSuccess = true;
                    trans.Complete();     // Transaction complete
                 }
              }
              catch (Exception ex)
              {
                 // Log error in ELMAH for any diagnostic needs.
                 Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                 isSuccess = false;
                 results = "An error occurred adding the record. Please try again.";
              }
              finally
              {
                 ce.Connection.Dispose();
              }

              // Finally accept all changes from the transaction.
              if (isSuccess)
              {
                 ce.AcceptAllChanges();
              }
           }

           return results;
		 }

       public static string Update(lease_applications application, CPTTEntities ceRef = null)
		 {
		     String results = null;
           var isSuccess = false;
           var isRootTransaction = true;

           CPTTEntities ce;
           if (ceRef == null)
           {
              //using new entity object
              ce = new CPTTEntities();
           }
           else
           {
              //using current entity object
              ce = ceRef;
              isRootTransaction = false;
           }

           // Start transaction for update.
            try
            {
               using (var trans = TransactionUtils.CreateTransactionScope())
               {
                  if (isRootTransaction)
                  {
                     // Manually open connection to prevent EF from auto closing
                     // connection and causing issues with future queries.
                     ce.Connection.Open();
                  }

                  // Since attached objects lose their previous history context, we must manually
                  // test the updated_at field for an optimistic concurrency exception.
                  var testApplication = ce.lease_applications.First(la => la.id.Equals(application.id));

                  if (testApplication.updated_at != application.updated_at)
                  {
                     throw new OptimisticConcurrencyException();
                  }
 
                  ce.AttachUpdated(application);

                  // Only add version record if modifications were detected
                  // for that entity.
                  if (ce.ObjectStateManager.GetObjectStateEntry(application.EntityKey).State == EntityState.Modified)
                  {
                     ce.lease_application_versions.AddObject(LeaseApplication.ToVersionFromEntityOriginal(ce, application));
                  }

                  if (isRootTransaction)
                  {
                     // We tentatively save the changes so that we can finish 
                     // processing the transaction.
                     ce.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                     trans.Complete();     // Transaction complete
                  }

                  isSuccess = true;                  
               }
            }
            catch (OptimisticConcurrencyException oex)
            {
               // Log error in ELMAH for any diagnostic needs.
               Elmah.ErrorSignal.FromCurrentContext().Raise(oex);

               isSuccess = false;
               results = "The current record has been modifed since you have " +
                        "last retrieved it. Please reload the record and " +
                        "attempt to save it again.";
            }
            catch (Exception ex)
            {
               // Log error in ELMAH for any diagnostic needs.
               Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

               isSuccess = false;
               results = "An error occurred saving the record. Please try again.";
            }
            finally
            {
               if (isRootTransaction)
               {
                  ce.Connection.Dispose();
               }
            }

            // Finally accept all changes from the transaction.
            if (isSuccess && isRootTransaction)
            {
               ce.AcceptAllChanges();
               ce.Dispose();
            }

           return results;
		 }

      /// <summary>
      ///    Updates lease application object within an existing context.
      ///    This is needed when doing updates as part of a full lease
      ///    application update. It is assumed that the call to the method is
      ///    within a transaction block outside this method. This method will
      ///    not make a call to trans.complete or ce.SaveChanges since it will
      ///    be the responsibility of the calling method to make the final
      ///    save and allow for a full rollback if an error occurs.
      /// </summary>
      /// <param name="application">Lease Application to update.</param>
      /// <param name="ceRef">Entity Context Reference.</param>
      /// <returns></returns> 
      public static string UpdateWithinTransaction(lease_applications application, CPTTEntities ceRef)
       {
          String results = null;

          try
          {
             // Since attached objects lose their previous history context, we must manually
             // test the updated_at field for an optimistic concurrency exception.
             var testApplication = ceRef.lease_applications.First(la => la.id.Equals(application.id));

             if (testApplication.updated_at != application.updated_at)
             {
                throw new OptimisticConcurrencyException();
             }
             
             ceRef.AttachUpdated(application);

            // Only add version record if modifications were detected
            // for that entity.
            if (ceRef.ObjectStateManager.GetObjectStateEntry(application.EntityKey).State == EntityState.Modified)
            {
               ceRef.lease_application_versions.AddObject(LeaseApplication.ToVersionFromEntityOriginal(ceRef, application));
            }
          }
          catch (OptimisticConcurrencyException oex)
          {
             // Log error in ELMAH for any diagnostic needs.
             Elmah.ErrorSignal.FromCurrentContext().Raise(oex);

             results = "The current record has been modifed since you have " +
                       "last retrieved it. Please reload the record and " +
                       "attempt to save it again.";
          }
          catch (Exception ex)
          {
             // Log error in ELMAH for any diagnostic needs.
             Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
             results = "An error occurred saving the record. Please try again.";
          }

          return results;
       }

       /// <summary>
       /// Delete lease application and its related record by lease application id
       /// </summary>
       /// <param name="id">Id of record that want to delete.</param>
       /// <returns>"Null" string if deletion is complete without error, error text otherwise.</returns>
      public static String Delete(int id)
      {
          String sResult = null;
          var isSuccess = false;

          //Start transaction for delete.
          using (var ce = new CPTTEntities())
          {
             try
             {
                using (var trans = TransactionUtils.CreateTransactionScope())
                {
                   // Manually open connection to prevent EF from auto closing
                   // connection and causing issues with future queries.
                   ce.Connection.Open();

                   // Remove any structural analysis records.
                   var querySA = from sa in ce.structural_analyses
                                 where sa.lease_application_id == id
                                 select sa;

                   if (querySA.Any())
                   {
                      foreach (var item in querySA.ToList<structural_analyses>())
                      {
                         // Calling the object's delete method will properly remove
                         // any dependencies as well.
                         StructuralAnalysis.DeleteWithinTransaction(item.id, ce);
                         // Change DocLog
                         DocumentsChangeLog.UpdateIsLinkRecordFlag(item.id, DMSDocumentLinkHelper.eDocumentType.structural_analyses.ToString(), false, ce);
                      }
                   }

                   // Remove any revision records.
                   var queryRV = from rv in ce.leaseapp_revisions
                                 where rv.lease_application_id == id
                                 select rv;

                   if (queryRV.Any())
                   {
                      foreach (var item in queryRV.ToList<leaseapp_revisions>())
                      {
                         ce.DeleteObject(item);

                         DocumentsChangeLog.UpdateIsLinkRecordFlag(item.id, DMSDocumentLinkHelper.eDocumentType.leaseapp_revision.ToString(), false, ce,
                             DMSDocumentLinkHelper.eDocumentField.RevisionReveivedDate.ToString());
                      }
                   }

                   // Remove any comment records.
                   var queryComment = from com in ce.leaseapp_comments
                                      where com.lease_application_id == id
                                      select com;

                   if (queryComment.Any())
                   {
                      foreach (var item in queryComment.ToList<leaseapp_comments>())
                      {
                         ce.DeleteObject(item);
                         DocumentsChangeLog.UpdateIsLinkRecordFlag(item.id, DMSDocumentLinkHelper.eDocumentType.lease_applications.ToString(), false, ce,
                            DMSDocumentLinkHelper.eDocumentField.LE_Comments.ToString());
                      }
                   }

                   // Remove any version records.
                   var queryVersion = from ver in ce.lease_application_versions
                                      where ver.lease_application_id == id
                                      select ver;

                   if (queryVersion.Any())
                   {
                      foreach (var item in queryVersion.ToList<lease_application_versions>())
                      {
                         ce.DeleteObject(item);
                      }
                   }

                   // Remove any LOE holdup reason records.
                   var queryLOE = from loe in ce.loe_holdup_reasons
                                  where loe.lease_application_id == id
                                  select loe;

                   if (queryLOE.Any())
                   {
                      foreach (var item in queryLOE.ToList<loe_holdup_reasons>())
                      {
                         ce.DeleteObject(item);
                      }
                   }
                   // Remove any tower modification join records.
                   LeaseAppTowerMod.DeleteAllByLeaseAppWithinTransaction(id, ce);

                   // Site Data Package records are used for multiple lease
                   // applications. Instead of removing the record, it is
                   // updated instead to not reference the lease application
                   // being removed.
                   var querySDP = from sdp in ce.site_data_packages
                                  where sdp.lease_application_id == id
                                  select sdp;
                   foreach (var item in querySDP.ToList<site_data_packages>())
                   {
                      ce.site_data_package_versions.AddObject(SiteDataPackage.ToSDPVersion(item));
                      item.lease_application_id = null;

                      DocumentsChangeLog.UpdateIsLinkRecordFlag(item.id, DMSDocumentLinkHelper.eDocumentType.site_data_packages.ToString(), false, ce);
                   }

                   // Remove any documnt records.
                   var queryDocument = from doc in ce.documents_links
                                       where doc.record_id == id
                                       select doc;

                   if (queryDocument.Any())
                   {
                       foreach (var item in queryDocument.ToList<documents_links>())
                       {
                           ce.DeleteObject(item);
                       }
                   }

                   // Delete lease application.
                   var queryLeaseApplication = from la in ce.lease_applications
                                               where la.id == id
                                               select la;

                   if (queryLeaseApplication.Any())
                   {
                      var app = queryLeaseApplication.ToList<lease_applications>()[0];
                      ce.DeleteObject(app);
                   }
                   else
                   {
                      throw new NullReferenceException();
                   }

                    // Update lease admin
                   var queryLeaseAdmin = from la in ce.lease_admin_records
                                         where la.lease_application_id == id
                                         select la;

                   if (queryLeaseAdmin.Any())
                   {
                       var leaseAdmin = queryLeaseAdmin.First();
                       leaseAdmin.lease_application_id = null;
                   }

                   // Update document changeLog
                   DocumentsChangeLog.UpdateIsLinkRecordFlag(id, DMSDocumentLinkHelper.eDocumentType.lease_applications.ToString(), false, ce);

                   ce.SaveChanges(SaveOptions.DetectChangesBeforeSave);
                   isSuccess = true;
                   trans.Complete();     //Transaction complete
                }
             }
             catch (Exception ex)
             {
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                sResult = ex.ToString();
             }
             finally
             {
                ce.Connection.Dispose();
             }

             // Finally accept all changes from the transaction.
             if (isSuccess)
             {
                ce.AcceptAllChanges();
             }
          }
          return sResult;
      }

      /// <summary>
      ///    Creates a version of an existing structural analysis record by
      ///    using the entity context's original values.
      /// </summary>
      /// <param name="context">Context in which entity resides.</param>
      /// <param name="la">The structural analysis entity to version.</param>
      /// <returns>Structural Analysis Version object.</returns>
      public static lease_application_versions ToVersionFromEntityOriginal(CPTTEntities context, lease_applications la)
      {
         var results = new lease_application_versions();
         var origLA = new lease_applications();
         var objState = context.ObjectStateManager.GetObjectStateEntry(la.EntityKey);
         var origLARecord = objState.OriginalValues;
         var origRecordFields = new List<string>();

         // Build list of property names for validation check later.
         for (var c = 0; c <= origLARecord.FieldCount - 1; c++)
         {
            if (!origRecordFields.Contains(origLARecord.GetName(c)))
            {
               origRecordFields.Add(origLARecord.GetName(c));
            }
         }

         origLA.id = la.id;

         // Use reflection to get all modifiyable properties
         var srcProperties = from p in typeof(lease_applications).GetProperties()
                             where p.CanRead &&
                                   p.CanWrite &&
                                   p.Name != "id" &&
                                   p.Name != "EntityKey"
                             select p;

         // Iterate through all properites and assign them to the version copy.
         foreach (var property in srcProperties)
         {
            // Some properties in the entity object are foreign keys, and will
            // not have a record in the original values list.
            if (!origRecordFields.Contains(property.Name)) continue;
            
            var currValue = origLARecord[property.Name];

            if (currValue == null || currValue.GetType().Name == "DBNull") continue;

            var destProperty = typeof(lease_applications).GetProperty(property.Name);

            // Check for null destination properties.
            if (destProperty == null) continue;
            
            // Check for EntityReference to not create the existing reference again.
            var baseType = currValue.GetType().BaseType;
            
            if (baseType != null && baseType.Name == "EntityReference")
            {
               // Simply copy the EntityKey to the new entity object’s EntityReference
               ((EntityReference)destProperty.GetValue(results, null)).EntityKey =
                  ((EntityReference)currValue).EntityKey;
            }
            else
            {
               // Common task, just copy the simple type property into the new entity object
               destProperty.SetValue(origLA, currValue, null);
            }
         }

         results = ToLeaseApplicationVersion(origLA);
         return results;
      }

      /// <summary>
      /// Clones the lease application into a lease_application_version
      /// </summary>
      /// <param name="application">The current lease application.</param>
      /// <returns>Lease Application Version object with all values.</returns>
      public static lease_application_versions ToLeaseApplicationVersion
                                               (lease_applications application)
      {
         var results = new lease_application_versions();

         // Use reflection to get all modifiyable properties
         var srcProperties =  from p in typeof(lease_applications).GetProperties()
                             where p.CanRead &&
                                   p.CanWrite &&
                                   p.Name != "id" &&
                                   p.Name != "EntityKey"
                            select p;

         results.lease_application_id = application.id;

         // Iterate through all properites and assign them to the version copy.
         foreach (var property in srcProperties)
         {
            var currValue = property.GetValue(application, null);

            if (currValue == null) continue;
            
            var destProperty = typeof(lease_application_versions).GetProperty(property.Name);

            // Check for null destination property.
            if (destProperty == null) continue;
            
            // Check for EntityReference to not create the existing reference again.
            var baseType = currValue.GetType().BaseType;
               
            if (baseType != null && baseType.Name == "EntityReference") 
            {
               //Simply copy the EntityKey to the new entity object’s EntityReference
               ((EntityReference)destProperty.GetValue(results, null)).EntityKey =
                  ((EntityReference)currValue).EntityKey;
            }
            else
            {
               //Common task, just copy the simple type property into the new entity object
               destProperty.SetValue(results, currValue, null);
            }
         }

         return results;
      }

      public static ProjectCategory GetProjectCategory(lease_applications application)
      {
         ProjectCategory results;

         if (application.leaseapp_carrier_project_id == 1)
         {                                                      
            results = ProjectCategory.SprintNV;
         }
         else if (application.leaseapp_activity_id == 7)
         {
            results = ProjectCategory.TMOPremod;
         }
         else if (
                  (application.leaseapp_activity_id == 2) ||
                  (application.leaseapp_activity_id == 5 && application.leaseapp_type_id != 7)
                 )
         {
            results = ProjectCategory.GroundModFiber;
         }
         else if (application.leaseapp_type_id == 7)
         {
            results = ProjectCategory.Initial;
         }
         else
         {
            results = ProjectCategory.Mod;
         }

         return results;
      }

      public static string GetProjectCategoryFriendlyName(ProjectCategory category)
      {
         string results;

         switch (category)
         {
            case ProjectCategory.GroundModFiber:
               results = "Ground Mod / Fiber";
               break;

            case ProjectCategory.Initial:
               results = "Initial";
               break;

            case ProjectCategory.Mod:
               results = "Mod";
               break;

            case ProjectCategory.SprintNV:
               results = "Sprint NV";
               break;

            case ProjectCategory.TMOPremod:
               results = "TMO PreMod";
               break;

            default:
               results = "No Project Category";
               break;
         }         

         return results;
      }

      public static string GetLeaseAppTypeAliases(string type)
      {
          string results = "";

          switch (type)
          {

              case "Initial":
                  results = "Init";
                  break;

              case "Modification":
                  results = "Mod";
                  break;

              case "Modification 1":
                  results = "Mod1";
                  break;

              case "Modification 2":
                  results = "Mod2";
                  break;

              case "Modification 3":
                  results = "Mod3";
                  break;

              case "Modification 4":
                  results = "Mod4";
                  break;

              case "Modification 5":
                  results = "Mod5";
                  break;

              case "Modification 6":
                  results = "Mod6";
                  break;

              case "Modification 7":
                  results = "Mod7";
                  break;

              case "Modification 8":
                  results = "Mod8";
                  break;

              case "Modification 9":
                  results = "Mod9";
                  break;

              case "Modification 10":
                  results = "Mod10";
                  break;

              default:
                  results = string.Empty;
                  break;
          }

          return results;
      }

      // Dynamic antenna line
      public static IQueryable GetLine(int id)
      {
          var enEquip = new EquipmentEntities();
          var antLine =  from tx in enEquip.t_AntennaTxLineType 
                        where tx.ApplicationEquipmentID == id 
                       select tx;

          return antLine.Any() ? antLine : null;
      }

      // All site search criteria in a dictionary for easy lookup/manipulation
      // when dynamically building the query.
      public static Dictionary<string, SearchParameter> GetSearchCriteria()
      {
         var results = new Dictionary<string, SearchParameter>
            {
                {
                  "General", new SearchParameter
                     {                        
                        VariableName = "General",
                        ParameterType = typeof(string),
                        WhereClause = "(it.site_uid LIKE {0})",
                        IsWildcardSearch = true
                     }
               },
               {
                  "IssueID", new SearchParameter
                     {                        
                        VariableName = "id",
                        ParameterType = typeof(int),
                        WhereClause = "(it.id = {0})"
                     }
               },
               {
                  "SiteID", new SearchParameter
                     {                        
                        VariableName = "SiteID",
                        ParameterType = typeof(string),
                        WhereClause = "(it.site_uid LIKE {0})",
                        IsWildcardSearch = true
                     }
               },
               {
                  "Customer", new SearchParameter
                     {
                        VariableName = "Customer",
                        ParameterType = typeof(int),
                        WhereClause = "(it.customer_id = {0})",
                        AllowsMultipleValues = true
                     }
               },
               {
                  "ActivityType", new SearchParameter
                     {
                        VariableName = "ActivityType",
                        ParameterType = typeof(int),
                        WhereClause = "(it.leaseapp_activity_id = {0})",
                        AllowsMultipleValues = true
                     }
               },
               {
                  "LeaseAppType", new SearchParameter
                     {
                        VariableName = "LeaseAppType",
                        ParameterType = typeof(int),
                        WhereClause = "(it.leaseapp_type_id = {0})",
                        AllowsMultipleValues = true
                     }
               },
               {
                  "LeaseAppStatus", new SearchParameter
                     {
                        VariableName = "LeaseAppStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.leaseapp_status_id = {0})",
                        AllowsMultipleValues = true
                     }
               },
               {
                  "AppReceivedFrom", new SearchParameter
                     {
                        VariableName = "AppReceivedFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.leaseapp_rcvd_date >= {0})"
                     }                  
               },
               {
                  "AppReceivedTo", new SearchParameter
                     {
                        VariableName = "AppReceivedTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.leaseapp_rcvd_date <= {0})"
                     }                  
               },
               {
                  "AppReceivedRange", new SearchParameter
                     {
                        VariableName = "AppReceivedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.leaseapp_rcvd_date >= {0} AND it.leaseapp_rcvd_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "AppFeeReceivedFrom", new SearchParameter
                     {
                        VariableName = "AppFeeReceivedFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.leaseapp_fee_rcvd_date >= {0})"
                     }                  
               },
               {
                  "AppFeeReceivedTo", new SearchParameter
                     {
                        VariableName = "AppFeeReceivedTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.leaseapp_fee_rcvd_date <= {0})"
                     }                  
               },
               {
                  "AppFeeReceivedRange", new SearchParameter
                     {
                        VariableName = "AppFeeReceivedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.leaseapp_fee_rcvd_date >= {0} AND it.leaseapp_fee_rcvd_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "MarketName", new SearchParameter
                     {
                        VariableName = "MarketName",
                        ParameterType = typeof(string),
                        WhereClause = "(it.site.market_name = {0})",
                        AllowsMultipleValues = true
                     }
               },
               {
                  "InitialSpecialist", new SearchParameter
                     {
                        VariableName = "InitialSpecialist",
                        ParameterType = typeof(int),
                        WhereClause = "(it.tmo_specialist_initial_emp_id = {0})",
                        AllowsMultipleValues = true
                     }
               },
               {
                  "CurrentSpecialist", new SearchParameter
                     {
                        VariableName = "CurrentSpecialist",
                        ParameterType = typeof(int),
                        WhereClause = "(it.tmo_specialist_current_emp_id = {0})",
                        AllowsMultipleValues = true
                     }
               },
               {
                  "ProjectManager", new SearchParameter
                     {
                        VariableName = "ProjectManager",
                        ParameterType = typeof(int),
                        WhereClause = "(it.tmo_pm_emp_id = {0})",
                        AllowsMultipleValues = true
                     }
               },
               {
                  "CarrierProject", new SearchParameter
                     {
                        VariableName = "CarrierProject",
                        ParameterType = typeof(int),
                        WhereClause = "(it.leaseapp_carrier_project_id = {0})",
                        AllowsMultipleValues = true
                     }
               },
               {
                  "CarrierProjectName", new SearchParameter
                     {
                        VariableName = "CarrierProjectName",
                        ParameterType = typeof(string),
                        WhereClause = "(it.leaseapp_carrier_projects.carrier_project_name = {0})",
                        AllowsMultipleValues = true
                     }
               },
               {
                  "SiteManagementStatus", new SearchParameter
                     {
                        VariableName = "SiteManagementStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(EXISTS(SELECT VALUE sites FROM [CPTTEntities].[sites] AS sites " +
                                      "WHERE sites.site_uid = it.site_uid AND sites.management_status_id = {0}))",
                        AllowsMultipleValues = true
                     }
               },
               {
                  // Project Category is a special calculated field based on
                  // various factors, so a special variable comparison where
                  // clause is generated.
                  "ProjectCategory", new SearchParameter
                     {
                        VariableName = "ProjectCategory",
                        ParameterType = typeof(string),
                        WhereClause = "(" + 
                                      "    ({0} = 'SprintNV' AND it.leaseapp_carrier_project_id = 1)" + 
                                      "    OR" + 
                                      "    ({0} = 'TMOPremod' AND it.leaseapp_activity_id = 7)" + 
                                      "    OR" + 
                                      "    ({0} = 'GroundModFiber' AND (it.leaseapp_activity_id = 2 OR (it.leaseapp_activity_id = 5 AND it.leaseapp_type_id != 7)))" + 
                                      "    OR" + 
                                      "    ({0} = 'Initial' AND it.leaseapp_type_id = 7)" + 
                                      "    OR" + 
                                      "    ({0} = 'Mod' AND it.leaseapp_type_id BETWEEN 17 AND 21)" + 
                                      ")",
                        AllowsMultipleValues = true
                     }
               },
               {
                  "CustomerSiteUID", new SearchParameter
                     {
                        VariableName = "CustomerSiteUID",
                        ParameterType = typeof(string),
                        WhereClause = "(it.customer_site_uid LIKE {0})",
                        IsWildcardSearch = true
                     }
               },
               {
                  "StreetAddress", new SearchParameter
                     {
                        VariableName = "StreetAddress",
                        ParameterType = typeof(string),
                        WhereClause = "(it.site.address LIKE {0})",
                        IsWildcardSearch = true
                     }
               },
               {
                  "City", new SearchParameter
                     {
                        VariableName = "City",
                        ParameterType = typeof(string),
                        WhereClause = "(it.site.city LIKE {0})",
                        IsWildcardSearch = true
                     }
               },
               {
                  "State", new SearchParameter
                     {
                        VariableName = "State",
                        ParameterType = typeof(string),
                        WhereClause = "(it.site.state LIKE {0})",
                        IsWildcardSearch = true
                     }
               },
               {
                  "County", new SearchParameter
                     {
                        VariableName = "County",
                        ParameterType = typeof(string),
                        WhereClause = "(it.site.county LIKE {0})",
                        IsWildcardSearch = true
                     }
               },
               {
                  "SDPToCustomerFrom", new SearchParameter
                     {
                        VariableName = "SDPToCustomerFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.sdp_to_customer_date >= {0})"
                     }                  
               },
               {
                  "SDPToCustomerTo", new SearchParameter
                     {
                        VariableName = "SDPToCustomerTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.sdp_to_customer_date <= {0})"
                     }                  
               },
               {
                  "SDPToCustomerRange", new SearchParameter
                     {
                        VariableName = "SDPToCustomerRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.sdp_to_customer_date >= {0} AND it.sdp_to_customer_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "PrelimDecision", new SearchParameter
                     {
                        VariableName = "PrelimDecision",
                        ParameterType = typeof(int),
                        WhereClause = "(it.prelim_decision_id = {0})"
                     }                  
               },
               {
                  "PrelimDecisionDateFrom", new SearchParameter
                     {
                        VariableName = "PrelimDecisionDateFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.prelim_approval_date >= {0})"
                     }                  
               },
               {
                  "PrelimDecisionDateTo", new SearchParameter
                     {
                        VariableName = "PrelimDecisionDateTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.prelim_approval_date <= {0})"
                     }                  
               },
               {
                  "PrelimDecisionDateRange", new SearchParameter
                     {
                        VariableName = "PrelimDecisionDateRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.prelim_approval_date >= {0} AND it.prelim_approval_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "PrelimSiteWalkFrom", new SearchParameter
                     {
                        VariableName = "PrelimSiteWalkFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.prelim_sitewalk_date >= {0})"
                     }                  
               },
               {
                  "PrelimSiteWalkTo", new SearchParameter
                     {
                        VariableName = "PrelimSiteWalkTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.prelim_sitewalk_date <= {0})"
                     }                  
               },
               {
                  "PrelimSiteWalkRange", new SearchParameter
                     {
                        VariableName = "PrelimSiteWalkRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.prelim_sitewalk_date >= {0} AND it.prelim_sitewalk_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "PrelimSiteWalkStatus", new SearchParameter
                     {
                        VariableName = "PrelimSiteWalkStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.prelim_sitewalk_date_status_id = {0})"
                     }                  
               },
               {
                  "LandlordConsentType", new SearchParameter
                     {
                        VariableName = "LandlordConsentType",
                        ParameterType = typeof(int),
                        WhereClause = "(it.landlord_consent_type_id = {0})"
                     }                  
               },
               {
                  "LandlordConsentTypeNote", new SearchParameter
                     {
                        VariableName = "LandlordConsentTypeNote",
                        ParameterType = typeof(string),
                        WhereClause = "(it.landlord_consent_type_notes LIKE {0})",
                        IsWildcardSearch = true
                     }                  
               },
               {
                  "CohostTenant", new SearchParameter
                     {
                        VariableName = "CohostTenant",
                        ParameterType = typeof(int),
                        WhereClause = "(it.cohost_tenant_id = {0})"
                     }                  
               },
               {
                  "LEOrderedFrom", new SearchParameter
                     {
                        VariableName = "LEOrderedFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.leaseexhbt_ordered_date >= {0})"
                     }                  
               },
               {
                  "LEOrderedTo", new SearchParameter
                     {
                        VariableName = "LEOrderedTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.leaseexhbt_ordered_date <= {0})"
                     }                  
               },
               {
                  "LEOrderedRange", new SearchParameter
                     {
                        VariableName = "LEOrderedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.leaseexhbt_ordered_date >= {0} AND it.leaseexhbt_ordered_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "LEOrderedStatus", new SearchParameter
                     {
                        VariableName = "LEOrderedStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.leaseexhbt_ordered_date_status_id = {0})"
                     }                  
               },
               {
                  "LEReceivedFrom", new SearchParameter
                     {
                        VariableName = "LEReceivedFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.leaseexhbt_rcvd_date >= {0})"
                     }                  
               },
               {
                  "LEReceivedTo", new SearchParameter
                     {
                        VariableName = "LEReceivedTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.leaseexhbt_rcvd_date <= {0})"
                     }                  
               },
               {
                  "LEReceivedRange", new SearchParameter
                     {
                        VariableName = "LEReceivedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.leaseexhbt_rcvd_date >= {0} AND it.leaseexhbt_rcvd_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "LEDecisionDateFrom", new SearchParameter
                     {
                        VariableName = "LEDecisionDateFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.leaseexhbt_decision_date >= {0})"
                     }                  
               },
               {
                  "LEDecisionDateTo", new SearchParameter
                     {
                        VariableName = "LEDecisionDateTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.leaseexhbt_decision_date <= {0})"
                     }                  
               },
               {
                  "LEDecisionDateRange", new SearchParameter
                     {
                        VariableName = "LEDecisionDateRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.leaseexhbt_decision_date >= {0} AND it.leaseexhbt_decision_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "LEDecision", new SearchParameter
                     {
                        VariableName = "LEDecision",
                        ParameterType = typeof(int),
                        WhereClause = "(it.leaseexhbt_decision_id = {0})"
                     }                  
               },
               {
                  "LandlordNoticeFrom", new SearchParameter
                     {
                        VariableName = "LandlordNoticeFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.landlord_notice_sent_date >= {0})"
                     }                  
               },
               {
                  "LandlordNoticeTo", new SearchParameter
                     {
                        VariableName = "LandlordNoticeTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.landlord_notice_sent_date <= {0})"
                     }                  
               },
               {
                  "LandlordNoticeRange", new SearchParameter
                     {
                        VariableName = "LandlordNoticeRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.landlord_notice_sent_date >= {0} AND it.landlord_notice_sent_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "LandlordConsentRequestedFrom", new SearchParameter
                     {
                        VariableName = "LandlordConsentRequestedFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.landlord_consent_sent_date >= {0})"
                     }                  
               },
               {
                  "LandlordConsentRequestedTo", new SearchParameter
                     {
                        VariableName = "LandlordConsentRequestedTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.landlord_consent_sent_date <= {0})"
                     }                  
               },
               {
                  "LandlordConsentRequestedRange", new SearchParameter
                     {
                        VariableName = "LandlordConsentRequestedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.landlord_consent_sent_date >= {0} AND it.landlord_consent_sent_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "LandlordConsentReceivedFrom", new SearchParameter
                     {
                        VariableName = "LandlordConsentReceivedFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.landlord_consent_rcvd_date >= {0})"
                     }                  
               },
               {
                  "LandlordConsentReceivedTo", new SearchParameter
                     {
                        VariableName = "LandlordConsentReceivedTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.landlord_consent_rcvd_date <= {0})"
                     }                  
               },
               {
                  "LandlordConsentReceivedRange", new SearchParameter
                     {
                        VariableName = "LandlordConsentReceivedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.landlord_consent_rcvd_date >= {0} AND it.landlord_consent_rcvd_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "CDReceivedFrom", new SearchParameter
                     {
                        VariableName = "CDReceivedFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.cd_rcvd_date >= {0})"
                     }                  
               },
               {
                  "CDReceivedTo", new SearchParameter
                     {
                        VariableName = "CDReceivedTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.cd_rcvd_date <= {0})"
                     }                  
               },
               {
                  "CDReceivedRange", new SearchParameter
                     {
                        VariableName = "CDReceivedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.cd_rcvd_date >= {0} AND it.cd_rcvd_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "CDDecisionDateFrom", new SearchParameter
                     {
                        VariableName = "CDDecisionDateFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.cd_decision_date >= {0})"
                     }                  
               },
               {
                  "CDDecisionDateTo", new SearchParameter
                     {
                        VariableName = "CDDecisionDateTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.cd_decision_date <= {0})"
                     }                  
               },
               {
                  "CDDecisionDateRange", new SearchParameter
                     {
                        VariableName = "CDDecisionDateRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.cd_decision_date >= {0} AND it.cd_decision_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "CDDecision", new SearchParameter
                     {
                        VariableName = "CDDecision",
                        ParameterType = typeof(int),
                        WhereClause = "(it.cd_decision_id <= {0})"
                     }                  
               },
               {
                  "ExecutableSentToCustomerFrom", new SearchParameter
                     {
                        VariableName = "ExecutableSentToCustomerFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.leasedoc_sent_to_cust_date >= {0})"
                     }                  
               },
               {
                  "ExecutableSentToCustomerTo", new SearchParameter
                     {
                        VariableName = "ExecutableSentToCustomerTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.leasedoc_sent_to_cust_date <= {0})"
                     }                  
               },
               {
                  "ExecutableSentToCustomerRange", new SearchParameter
                     {
                        VariableName = "ExecutableSentToCustomerRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.leasedoc_sent_to_cust_date >= {0} AND it.leasedoc_sent_to_cust_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "LOESentFrom", new SearchParameter
                     {
                        VariableName = "LOESentFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.leasedoc_sent_to_cust_date >= {0})"
                     }                  
               },
               {
                  "LOESentTo", new SearchParameter
                     {
                        VariableName = "LOESentTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.leasedoc_sent_to_cust_date <= {0})"
                     }                  
               },
               {
                  "LOESentRange", new SearchParameter
                     {
                        VariableName = "LOESentRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.leasedoc_sent_to_cust_date >= {0} AND it.leasedoc_sent_to_cust_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "LOESentStatus", new SearchParameter
                     {
                        VariableName = "LOESentStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.leasedoc_sent_to_cust_date_status_id = {0})"
                     }                  
               },
               {
                  "LOEHoldupComments", new SearchParameter
                     {
                        VariableName = "LOEHoldupComments",
                        ParameterType = typeof(string),
                        WhereClause = "(it.loe_holdup_comments LIKE {0})"
                     }                  
               },
               {
                  "DocsBackFromCustomerFrom", new SearchParameter
                     {
                        VariableName = "DocsBackFromCustomerFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.lease_back_from_cust_date >= {0})"
                     }                  
               },
               {
                  "DocsBackFromCustomerTo", new SearchParameter
                     {
                        VariableName = "DocsBackFromCustomerTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.lease_back_from_cust_date <= {0})"
                     }                  
               },
               {
                  "DocsBackFromCustomerRange", new SearchParameter
                     {
                        VariableName = "DocsBackFromCustomerRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.lease_back_from_cust_date >= {0} AND it.lease_back_from_cust_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "DocsBackFromCustomerStatus", new SearchParameter
                     {
                        VariableName = "DocsBackFromCustomerStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.lease_back_from_cust_date_status_id = {0})"
                     }                  
               },
               {
                  "SignedByCustomerFrom", new SearchParameter
                     {
                        VariableName = "SignedByCustomerFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.signed_by_cust_date >= {0})"
                     }                  
               },
               {
                  "SignedByCustomerTo", new SearchParameter
                     {
                        VariableName = "SignedByCustomerTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.signed_by_cust_date <= {0})"
                     }                  
               },
               {
                  "SignedByCustomerRange", new SearchParameter
                     {
                        VariableName = "SignedByCustomerRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.signed_by_cust_date >= {0} AND it.signed_by_cust_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "DocsToFSCFrom", new SearchParameter
                     {
                        VariableName = "DocsToFSCFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.leasedoc_to_fsc_date >= {0})"
                     }                  
               },
               {
                  "DocsToFSCTo", new SearchParameter
                     {
                        VariableName = "DocsToFSCTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.leasedoc_to_fsc_date <= {0})"
                     }                  
               },
               {
                  "DocsToFSCRange", new SearchParameter
                     {
                        VariableName = "DocsToFSCRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.leasedoc_to_fsc_date >= {0} AND it.leasedoc_to_fsc_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "LeaseExecFrom", new SearchParameter
                     {
                        VariableName = "LeaseExecFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.lease_execution_date >= {0})"
                     }                  
               },
               {
                  "LeaseExecTo", new SearchParameter
                     {
                        VariableName = "LeaseExecTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.lease_execution_date <= {0})"
                     }                  
               },
               {
                  "LeaseExecRange", new SearchParameter
                     {
                        VariableName = "LeaseExecRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.lease_execution_date >= {0} AND it.lease_execution_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "LeaseExecStatus", new SearchParameter
                     {
                        VariableName = "LeaseExecStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.lease_execution_date_status_id = {0})"
                     }                  
               },
               {
                  "AmendExecFrom", new SearchParameter
                     {
                        VariableName = "AmendExecFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.amendment_execution_date >= {0})"
                     }                  
               },
               {
                  "AmendExecTo", new SearchParameter
                     {
                        VariableName = "AmendExecTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.amendment_execution_date <= {0})"
                     }                  
               },
               {
                  "AmendExecRange", new SearchParameter
                     {
                        VariableName = "AmendExecRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.amendment_execution_date >= {0} AND it.amendment_execution_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "AmendExecStatus", new SearchParameter
                     {
                        VariableName = "AmendExecStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.amendment_execution_date_status_id = {0})"
                     }                  
               },
               {
                  "RentForecastAmountFrom", new SearchParameter
                     {
                        VariableName = "RentForecastAmountFrom",
                        ParameterType = typeof(decimal),
                        WhereClause = "(it.rent_forecast_amount >= {0})"
                     }                  
               },
               {
                  "RentForecastAmountTo", new SearchParameter
                     {
                        VariableName = "RentForecastAmountTo",
                        ParameterType = typeof(decimal),
                        WhereClause = "(it.rent_forecast_amount <= {0})"
                     }                  
               },
               {
                  "RentForecastAmountRange", new SearchParameter
                     {
                        VariableName = "RentForecastAmountRange",
                        ParameterType = typeof(decimal),
                        WhereClause = "(it.rent_forecast_amount >= {0} AND it.rent_forecast_amount <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "RevenueShare", new SearchParameter
                     {
                        VariableName = "RevenueShare",
                        ParameterType = typeof(bool),
                        WhereClause = "(it.revenue_share_yn = {0})"
                     }                  
               },
               {
                  "CommencementForecastFrom", new SearchParameter
                     {
                        VariableName = "CommencementForecastFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.commencement_forcast_date >= {0})"
                     }                  
               },
               {
                  "CommencementForecastTo", new SearchParameter
                     {
                        VariableName = "CommencementForecastTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.commencement_forcast_date <= {0})"
                     }                  
               },
               {
                  "CommencementForecastRange", new SearchParameter
                     {
                        VariableName = "CommencementForecastRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.commencement_forcast_date >= {0} AND it.commencement_forcast_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "REMLeaseSequenceFrom", new SearchParameter
                     {
                        VariableName = "REMLeaseSequenceFrom",
                        ParameterType = typeof(int),
                        WhereClause = "(it.rem_lease_seq >= {0})"
                     }                  
               },
               {
                  "REMLeaseSequenceTo", new SearchParameter
                     {
                        VariableName = "REMLeaseSequenceTo",
                        ParameterType = typeof(int),
                        WhereClause = "(it.rem_lease_seq <= {0})"
                     }                  
               },
               {
                  "REMLeaseSequenceRange", new SearchParameter
                     {
                        VariableName = "REMLeaseSequenceRange",
                        ParameterType = typeof(int),
                        WhereClause = "(it.rem_lease_seq >= {0} AND it.rem_lease_seq <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "CostShareAgreement", new SearchParameter
                     {
                        VariableName = "CostShareAgreement",
                        ParameterType = typeof(bool),
                        WhereClause = "(it.cost_share_agreement_yn = {0})"
                     }                  
               },
               {
                  "CostShareAgreementAmountFrom", new SearchParameter
                     {
                        VariableName = "CostShareAgreementAmountFrom",
                        ParameterType = typeof(decimal),
                        WhereClause = "(it.cost_share_agreement_amount >= {0})"
                     }                  
               },
               {
                  "CostShareAgreementAmountTo", new SearchParameter
                     {
                        VariableName = "CostShareAgreementAmountTo",
                        ParameterType = typeof(decimal),
                        WhereClause = "(it.cost_share_agreement_amount <= {0})"
                     }                  
               },
               {
                  "CostShareAgreementAmountRange", new SearchParameter
                     {
                        VariableName = "CostShareAgreementAmountRange",
                        ParameterType = typeof(decimal),
                        WhereClause = "(it.cost_share_agreement_amount >= {0} AND it.cost_share_agreement_amount <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "CapCostRecovery", new SearchParameter
                     {
                        VariableName = "CapCostRecovery",
                        ParameterType = typeof(bool),
                        WhereClause = "(it.cap_cost_recovery_yn = {0})"
                     }                  
               },
               {
                  "CapCostRecoveryAmountFrom", new SearchParameter
                     {
                        VariableName = "CapCostRecoveryAmountFrom",
                        ParameterType = typeof(decimal),
                        WhereClause = "(it.cap_cost_amount >= {0})"
                     }                  
               },
               {
                  "CapCostRecoveryAmountTo", new SearchParameter
                     {
                        VariableName = "CapCostRecoveryAmountTo",
                        ParameterType = typeof(decimal),
                        WhereClause = "(it.cap_cost_amount <= {0})"
                     }                  
               },
               {
                  "CapCostRecoveryAmountRange", new SearchParameter
                     {
                        VariableName = "CapCostRecoveryAmountRange",
                        ParameterType = typeof(decimal),
                        WhereClause = "(it.cap_cost_amount >= {0} AND it.cap_cost_amount <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "BulkPOAvailable", new SearchParameter
                     {
                        VariableName = "BulkPOAvailable",
                        ParameterType = typeof(bool),
                        WhereClause = "(it.bulk_po_availible_yn = {0})"
                     }                  
               },
               {
                  "TerminationDateFrom", new SearchParameter
                     {
                        VariableName = "TerminationDateFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.termination_date >= {0})"
                     }                  
               },
               {
                  "TerminationDateTo", new SearchParameter
                     {
                        VariableName = "TerminationDateTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.termination_date <= {0})"
                     }                  
               },
               {
                  "TerminationDateRange", new SearchParameter
                     {
                        VariableName = "TerminationDateRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.termination_date >= {0} AND it.termination_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "PreconSiteWalkFrom", new SearchParameter
                     {
                        VariableName = "PreconSiteWalkFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.preconstr_sitewalk_date >= {0})"
                     }                  
               },
               {
                  "PreconSiteWalkTo", new SearchParameter
                     {
                        VariableName = "PreconSiteWalkTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.preconstr_sitewalk_date <= {0})"
                     }                  
               },
               {
                  "PreconSiteWalkRange", new SearchParameter
                     {
                        VariableName = "PreconSiteWalkRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.preconstr_sitewalk_date >= {0} AND it.preconstr_sitewalk_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "PreconSiteWalkStatus", new SearchParameter
                     {
                        VariableName = "PreconSiteWalkStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.preconstr_sitewalk_date_status_id = {0})"
                     }                  
               },
               {
                  "NTPIssuedFrom", new SearchParameter
                     {
                        VariableName = "NTPIssuedFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.ntp_issued_date >= {0})"
                     }                  
               },
               {
                  "NTPIssuedTo", new SearchParameter
                     {
                        VariableName = "NTPIssuedTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.ntp_issued_date <= {0})"
                     }                  
               },
               {
                  "NTPIssuedRange", new SearchParameter
                     {
                        VariableName = "NTPIssuedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.ntp_issued_date >= {0} AND it.ntp_issued_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "NTPIssuedStatus", new SearchParameter
                     {
                        VariableName = "NTPIssuedStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.ntp_issued_date_status_id = {0})"
                     }                  
               },
               {
                  "NTPVerifiedFrom", new SearchParameter
                     {
                        VariableName = "NTPVerifiedFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.ntp_verified_date >= {0})"
                     }                  
               },
               {
                  "NTPVerifiedTo", new SearchParameter
                     {
                        VariableName = "NTPVerifiedTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.ntp_verified_date <= {0})"
                     }                  
               },
               {
                  "NTPVerifiedRange", new SearchParameter
                     {
                        VariableName = "NTPVerifiedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.ntp_verified_date >= {0} AND it.ntp_verified_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "EarlyInstDiscoveredFrom", new SearchParameter
                     {
                        VariableName = "EarlyInstDiscoveredFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.early_installation_discovered_date >= {0})"
                     }                  
               },
               {
                  "EarlyInstDiscoveredTo", new SearchParameter
                     {
                        VariableName = "EarlyInstDiscoveredTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.early_installation_discovered_date <= {0})"
                     }                  
               },
               {
                  "EarlyInstDiscoveredRange", new SearchParameter
                     {
                        VariableName = "EarlyInstDiscoveredRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.early_installation_discovered_date >= {0} AND it.early_installation_discovered_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "ViolationLetterSentFrom", new SearchParameter
                     {
                        VariableName = "ViolationLetterSentFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.early_installation_violation_letter_sent_date >= {0})"
                     }                  
               },
               {
                  "ViolationLetterSentTo", new SearchParameter
                     {
                        VariableName = "ViolationLetterSentTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.early_installation_violation_letter_sent_date <= {0})"
                     }                  
               },
               {
                  "ViolationLetterSentRange", new SearchParameter
                     {
                        VariableName = "ViolationLetterSentRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.early_installation_violation_letter_sent_date >= {0} AND it.early_installation_violation_letter_sent_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "ViolationInvoiceCreatedByFinopsFrom", new SearchParameter
                     {
                        VariableName = "ViolationInvoiceCreatedByFinopsFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.early_installation_violation_invoice_created_by_finops_date >= {0})"
                     }                  
               },
               {
                  "ViolationInvoiceCreatedByFinopsTo", new SearchParameter
                     {
                        VariableName = "ViolationInvoiceCreatedByFinopsTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.early_installation_violation_invoice_created_by_finops_date <= {0})"
                     }                  
               },
               {
                  "ViolationInvoiceCreatedByFinopsRange", new SearchParameter
                     {
                        VariableName = "ViolationInvoiceCreatedByFinopsRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.early_installation_violation_invoice_created_by_finops_date >= {0} AND it.early_installation_violation_invoice_created_by_finops_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "ViolationInvoiceNumberFrom", new SearchParameter
                     {
                        VariableName = "ViolationInvoiceNumberFrom",
                        ParameterType = typeof(int),
                        WhereClause = "(it.early_installation_violation_invoice_number >= {0})"
                     }                  
               },
               {
                  "ViolationInvoiceNumberTo", new SearchParameter
                     {
                        VariableName = "ViolationInvoiceNumberTo",
                        ParameterType = typeof(int),
                        WhereClause = "(it.early_installation_violation_invoice_number <= {0})"
                     }                  
               },
               {
                  "ViolationInvoiceNumberRange", new SearchParameter
                     {
                        VariableName = "ViolationInvoiceNumberRange",
                        ParameterType = typeof(int),
                        WhereClause = "(it.early_installation_violation_invoice_number >= {0} AND it.early_installation_violation_invoice_number <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "ViolationFeeReceivedFrom", new SearchParameter
                     {
                        VariableName = "ViolationFeeReceivedFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.early_installation_violation_fee_rcvd_date >= {0})"
                     }                  
               },
               {
                  "ViolationFeeReceivedTo", new SearchParameter
                     {
                        VariableName = "ViolationFeeReceivedTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.early_installation_violation_fee_rcvd_date <= {0})"
                     }                  
               },
               {
                  "ViolationFeeReceivedRange", new SearchParameter
                     {
                        VariableName = "ViolationFeeReceivedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.early_installation_violation_fee_rcvd_date >= {0} AND it.early_installation_violation_fee_rcvd_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "ExtensionNoticeReceivedFrom", new SearchParameter
                     {
                        VariableName = "ExtensionNoticeReceivedFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.extension_notice_rcvd_date >= {0})"
                     }                  
               },
               {
                  "ExtensionNoticeReceivedTo", new SearchParameter
                     {
                        VariableName = "ExtensionNoticeReceivedTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.extension_notice_rcvd_date <= {0})"
                     }                  
               },
               {
                  "ExtensionNoticeReceivedRange", new SearchParameter
                     {
                        VariableName = "ExtensionNoticeReceivedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.extension_notice_rcvd_date >= {0} AND it.extension_notice_rcvd_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "ExtensionNoticeFeeReceivedFrom", new SearchParameter
                     {
                        VariableName = "ExtensionNoticeFeeReceivedFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.extension_notice_fee_rcvd_date >= {0})"
                     }                  
               },
               {
                  "ExtensionNoticeFeeReceivedTo", new SearchParameter
                     {
                        VariableName = "ExtensionNoticeReceivedTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.extension_notice_fee_rcvd_date <= {0})"
                     }                  
               },
               {
                  "ExtensionNoticeFeeReceivedRange", new SearchParameter
                     {
                        VariableName = "ExtensionNoticeFeeReceivedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.extension_notice_fee_rcvd_date >= {0} AND it.extension_notice_fee_rcvd_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "ExtensionNoticeInvoiceCreatedByFinopsFrom", new SearchParameter
                     {
                        VariableName = "ExtensionNoticeInvoiceCreatedByFinopsFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.extension_notice_invoice_created_by_finops_date >= {0})"
                     }                  
               },
               {
                  "ExtensionNoticeInvoiceCreatedByFinopsTo", new SearchParameter
                     {
                        VariableName = "ExtensionNoticeInvoiceCreatedByFinopsTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.extension_notice_invoice_created_by_finops_date <= {0})"
                     }                  
               },
               {
                  "ExtensionNoticeInvoiceCreatedByFinopsRange", new SearchParameter
                     {
                        VariableName = "ExtensionNoticeInvoiceCreatedByFinopsRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.extension_notice_invoice_created_by_finops_date >= {0} AND it.extension_notice_invoice_created_by_finops_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "ExtensionNoticeFeeInvoiceNumberFrom", new SearchParameter
                     {
                        VariableName = "ExtensionNoticeFeeInvoiceNumberFrom",
                        ParameterType = typeof(int),
                        WhereClause = "(it.extension_notice_fee_invoice_number >= {0})"
                     }                  
               },
               {
                  "ExtensionNoticeFeeInvoiceNumberTo", new SearchParameter
                     {
                        VariableName = "ExtensionNoticeFeeInvoiceNumberTo",
                        ParameterType = typeof(int),
                        WhereClause = "(it.extension_notice_fee_invoice_number <= {0})"
                     }                  
               },
               {
                  "ExtensionNoticeFeeInvoiceNumberRange", new SearchParameter
                     {
                        VariableName = "ExtensionNoticeFeeInvoiceNumberRange",
                        ParameterType = typeof(int),
                        WhereClause = "(it.extension_notice_fee_invoice_number >= {0} AND it.extension_notice_fee_invoice_number <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "EquipmentRemovedFrom", new SearchParameter
                     {
                        VariableName = "EquipmentRemovedFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.equipment_removed_date >= {0})"
                     }                  
               },
               {
                  "EquipmentRemovedTo", new SearchParameter
                     {
                        VariableName = "EquipmentRemovedTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.equipment_removed_date <= {0})"
                     }                  
               },
               {
                  "EquipmentRemovedRange", new SearchParameter
                     {
                        VariableName = "EquipmentRemovedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.equipment_removed_date >= {0} AND it.equipment_removed_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "FailureToRemoveViolation", new SearchParameter
                     {
                        VariableName = "FailureToRemoveViolation",
                        ParameterType = typeof(bool),
                        WhereClause = "(it.failure_to_remove_violation = {0})"
                     }                  
               },
               {
                  "FailureToRemoveViolationLetterSentFrom", new SearchParameter
                     {
                        VariableName = "FailureToRemoveViolationLetterSentFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.failure_to_remove_violation_letter_sent_date >= {0})"
                     }                  
               },
               {
                  "FailureToRemoveViolationLetterSentTo", new SearchParameter
                     {
                        VariableName = "FailureToRemoveViolationLetterSentTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.failure_to_remove_violation_letter_sent_date <= {0})"
                     }                  
               },
               {
                  "FailureToRemoveViolationLetterSentRange", new SearchParameter
                     {
                        VariableName = "FailureToRemoveViolationLetterSentRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.failure_to_remove_violation_letter_sent_date >= {0} AND it.failure_to_remove_violation_letter_sent_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "FailureToRemoveViolationLetterSubmittedToCustomerFrom", new SearchParameter
                     {
                        VariableName = "FailureToRemoveViolationLetterSubmittedToCustomerFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.failure_to_remove_violation_letter_submitted_to_cst_date >= {0})"
                     }                  
               },
               {
                  "FailureToRemoveViolationLetterSubmittedToCustomerTo", new SearchParameter
                     {
                        VariableName = "FailureToRemoveViolationLetterSubmittedToCustomerTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.failure_to_remove_violation_letter_submitted_to_cst_date <= {0})"
                     }                  
               },
               {
                  "FailureToRemoveViolationLetterSubmittedToCustomerRange", new SearchParameter
                     {
                        VariableName = "FailureToRemoveViolationLetterSubmittedToCustomerRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.failure_to_remove_violation_letter_submitted_to_cst_date >= {0} AND it.failure_to_remove_violation_letter_submitted_to_cst_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "ViolationEquipmentRemovalConfirmedSubmittedToCustomerFrom", new SearchParameter
                     {
                        VariableName = "ViolationEquipmentRemovalConfirmedSubmittedToCustomerFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.violation_equipment_removal_confirmed_submitted_to_cst_date >= {0})"
                     }                  
               },
               {
                  "ViolationEquipmentRemovalConfirmedSubmittedToCustomerTo", new SearchParameter
                     {
                        VariableName = "ViolationEquipmentRemovalConfirmedSubmittedToCustomerTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.violation_equipment_removal_confirmed_submitted_to_cst_date <= {0})"
                     }                  
               },
               {
                  "ViolationEquipmentRemovalConfirmedSubmittedToCustomerRange", new SearchParameter
                     {
                        VariableName = "ViolationEquipmentRemovalConfirmedSubmittedToCustomerRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.violation_equipment_removal_confirmed_submitted_to_cst_date >= {0} AND it.violation_equipment_removal_confirmed_submitted_to_cst_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "PostConstructSiteWalkFrom", new SearchParameter
                     {
                        VariableName = "PostConstructSiteWalkFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.postconstr_sitewalk_date >= {0})"
                     }                  
               },
               {
                  "PostConstructSiteWalkTo", new SearchParameter
                     {
                        VariableName = "PostConstructSiteWalkTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.postconstr_sitewalk_date <= {0})"
                     }                  
               },
               {
                  "PostConstructSiteWalkRange", new SearchParameter
                     {
                        VariableName = "PostConstructSiteWalkRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.postconstr_sitewalk_date >= {0} AND it.postconstr_sitewalk_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "PostConstructSiteWalkStatus", new SearchParameter
                     {
                        VariableName = "PostConstructSiteWalkStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.postconstr_sitewalk_date_status_id = {0})"
                     }                  
               },
               {
                  "PostConstructOrderedFrom", new SearchParameter
                     {
                        VariableName = "PostConstructOrderedFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.postconstr_ordered_date >= {0})"
                     }                  
               },
               {
                  "PostConstructOrderedTo", new SearchParameter
                     {
                        VariableName = "PostConstructOrderedTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.postconstr_ordered_date <= {0})"
                     }                  
               },
               {
                  "PostConstructOrderedRange", new SearchParameter
                     {
                        VariableName = "PostConstructOrderedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.postconstr_ordered_date >= {0} AND it.postconstr_ordered_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "PostConstructOrderedStatus", new SearchParameter
                     {
                        VariableName = "PostConstructOrderedStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.postconstr_ordered_date_status_id = {0})"
                     }                  
               },
               {
                  "ConstructionCompletedFrom", new SearchParameter
                     {
                        VariableName = "ConstructionCompletedFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.constr_cmplt_date >= {0})"
                     }                  
               },
               {
                  "ConstructionCompletedTo", new SearchParameter
                     {
                        VariableName = "ConstructionCompletedTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.constr_cmplt_date <= {0})"
                     }                  
               },
               {
                  "ConstructionCompletedRange", new SearchParameter
                     {
                        VariableName = "ConstructionCompletedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.constr_cmplt_date >= {0} AND it.constr_cmplt_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "ConstructionCompletedStatus", new SearchParameter
                     {
                        VariableName = "ConstructionCompletedStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.constr_cmplt_date_status_id = {0})"
                     }                  
               },
               {
                  "CloseOutReceivedFrom", new SearchParameter
                     {
                        VariableName = "CloseOutReceivedFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.all_closeout_docs_rcvd_date >= {0})"
                     }                  
               },
               {
                  "CloseOutReceivedTo", new SearchParameter
                     {
                        VariableName = "CloseOutReceivedTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.all_closeout_docs_rcvd_date <= {0})"
                     }                  
               },
               {
                  "CloseOutReceivedRange", new SearchParameter
                     {
                        VariableName = "CloseOutReceivedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.all_closeout_docs_rcvd_date >= {0} AND it.all_closeout_docs_rcvd_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "CloseOutDocFeeRequired", new SearchParameter
                     {
                        VariableName = "CloseOutDocFeeRequired",
                        ParameterType = typeof(bool),
                        WhereClause = "(it.closeout_docs_fee_required = {0})"
                     }                  
               },
               {
                  "CloseOutDocNoticeSentFrom", new SearchParameter
                     {
                        VariableName = "CloseOutDocNoticeSentFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.closeout_docs_notice_sent_date >= {0})"
                     }                  
               },
               {
                  "CloseOutDocNoticeSentTo", new SearchParameter
                     {
                        VariableName = "CloseOutDocNoticeSentTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.closeout_docs_notice_sent_date <= {0})"
                     }                  
               },
               {
                  "CloseOutDocNoticeSentRange", new SearchParameter
                     {
                        VariableName = "CloseOutDocNoticeSentRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.closeout_docs_notice_sent_date >= {0} AND it.closeout_docs_notice_sent_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "CloseOutDocFeeInvoiceSentFrom", new SearchParameter
                     {
                        VariableName = "CloseOutDocFeeInvoiceSentFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.closeout_docs_fee_invoice_sent_date >= {0})"
                     }                  
               },
               {
                  "CloseOutDocFeeInvoiceSentTo", new SearchParameter
                     {
                        VariableName = "CloseOutDocFeeInvoiceSentTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.closeout_docs_fee_invoice_sent_date <= {0})"
                     }                  
               },
               {
                  "CloseOutDocFeeInvoiceSentRange", new SearchParameter
                     {
                        VariableName = "CloseOutDocFeeInvoiceSentRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.closeout_docs_fee_invoice_sent_date >= {0} AND it.closeout_docs_fee_invoice_sent_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "CloseOutDocFeeInvoiceCreatedByFinopsFrom", new SearchParameter
                     {
                        VariableName = "CloseOutDocFeeInvoiceCreatedByFinopsFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.closeout_doc_fee_invoice_created_by_finops_date >= {0})"
                     }                  
               },
               {
                  "CloseOutDocFeeInvoiceCreatedByFinopsTo", new SearchParameter
                     {
                        VariableName = "CloseOutDocFeeInvoiceCreatedByFinopsTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.closeout_doc_fee_invoice_created_by_finops_date <= {0})"
                     }                  
               },
               {
                  "CloseOutDocFeeInvoiceCreatedByFinopsRange", new SearchParameter
                     {
                        VariableName = "CloseOutDocFeeInvoiceCreatedByFinopsRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.closeout_doc_fee_invoice_created_by_finops_date >= {0} AND it.closeout_doc_fee_invoice_created_by_finops_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "CloseOutDocFeeInvoiceNumberFrom", new SearchParameter
                     {
                        VariableName = "CloseOutDocFeeInvoiceNumberFrom",
                        ParameterType = typeof(int),
                        WhereClause = "(it.closeout_docs_fee_invoice_number >= {0})"
                     }                  
               },
               {
                  "CloseOutDocFeeInvoiceNumberTo", new SearchParameter
                     {
                        VariableName = "CloseOutDocFeeInvoiceNumberTo",
                        ParameterType = typeof(int),
                        WhereClause = "(it.closeout_docs_fee_invoice_number <= {0})"
                     }                  
               },
               {
                  "CloseOutDocFeeInvoiceNumberRange", new SearchParameter
                     {
                        VariableName = "CloseOutDocFeeInvoiceNumberRange",
                        ParameterType = typeof(int),
                        WhereClause = "(it.closeout_docs_fee_invoice_number >= {0} AND it.closeout_docs_fee_invoice_number <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "CloseOutDocFeeReceivedFrom", new SearchParameter
                     {
                        VariableName = "CloseOutDocFeeReceivedFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.closeout_docs_fee_rcvd_date >= {0})"
                     }                  
               },
               {
                  "CloseOutDocFeeReceivedTo", new SearchParameter
                     {
                        VariableName = "CloseOutDocFeeReceivedTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.closeout_docs_fee_rcvd_date <= {0})"
                     }                  
               },
               {
                  "CloseOutDocFeeReceivedRange", new SearchParameter
                     {
                        VariableName = "CloseOutDocFeeReceivedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.closeout_docs_fee_rcvd_date >= {0} AND it.closeout_docs_fee_rcvd_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "FinalSiteApprovalFrom", new SearchParameter
                     {
                        VariableName = "FinalSiteApprovalFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.final_site_apprvl_date >= {0})"
                     }                  
               },
               {
                  "FinalSiteApprovalTo", new SearchParameter
                     {
                        VariableName = "FinalSiteApprovalTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.final_site_apprvl_date <= {0})"
                     }                  
               },
               {
                  "FinalSiteApprovalRange", new SearchParameter
                     {
                        VariableName = "FinalSiteApprovalRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.final_site_apprvl_date >= {0} AND it.final_site_apprvl_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "FinalSiteApprovalStatus", new SearchParameter
                     {
                        VariableName = "FinalSiteApprovalStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.final_site_apprvl_date_status_id = {0})"
                     }                  
               },
               {
                  "AsBuiltFrom", new SearchParameter
                     {
                        VariableName = "AsBuiltFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.as_built_date >= {0})"
                     }                  
               },
               {
                  "AsBuiltTo", new SearchParameter
                     {
                        VariableName = "AsBuiltTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.as_built_date <= {0})"
                     }                  
               },
               {
                  "AsBuiltRange", new SearchParameter
                     {
                        VariableName = "AsBuiltRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.as_built_date >= {0} AND it.as_built_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "MarketHandoffFrom", new SearchParameter
                     {
                        VariableName = "MarketHandoffFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.market_handoff_date >= {0})"
                     }                  
               },
               {
                  "MarketHandoffTo", new SearchParameter
                     {
                        VariableName = "MarketHandoffTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.market_handoff_date <= {0})"
                     }                  
               },
               {
                  "MarketHandoffRange", new SearchParameter
                     {
                        VariableName = "MarketHandoffRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.market_handoff_date >= {0} AND it.market_handoff_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "MarketHandoffStatus", new SearchParameter
                     {
                        VariableName = "MarketHandoffStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.market_handoff_date_status_id = {0})"
                     }                  
               },
               {
                  "SADecision", new SearchParameter
                     {
                        VariableName = "SADecision",
                        ParameterType = typeof(int),
                        WhereClause = "(it.struct_decision_id = {0})"
                     }                  
               },
               {
                  "SADecisionDateFrom", new SearchParameter
                     {
                        VariableName = "SADecisionDateFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.struct_decision_date >= {0})"
                     }                  
               },
               {
                  "SADecisionDateTo", new SearchParameter
                     {
                        VariableName = "SADecisionDateTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.struct_decision_date <= {0})"
                     }                  
               },
               {
                  "SADecisionDateRange", new SearchParameter
                     {
                        VariableName = "SADecisionDateRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.struct_decision_date >= {0} AND it.struct_decision_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "SAPoOrderedFrom", new SearchParameter
                     {
                        VariableName = "SAPoOrderedFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.po_ordered_date >= {0}))"
                     }                  
               },
               {
                  "SAPoOrderedTo", new SearchParameter
                     {
                        VariableName = "SAPoOrderedTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.po_ordered_date <= {0}))"
                     }                  
               },
               {
                  "SAPoOrderedRange", new SearchParameter
                     {
                        VariableName = "SAPoOrderedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND (sa.po_ordered_date >= {0} AND sa.po_ordered_date <= {1})))",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "SAPoReceivedFrom", new SearchParameter
                     {
                        VariableName = "SAPoReceivedFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.po_received_date >= {0}))"
                     }                  
               },
               {
                  "SAPoReceivedTo", new SearchParameter
                     {
                        VariableName = "SAPoReceivedTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.po_received_date <= {0}))"
                     }                  
               },
               {
                  "SAPoReceivedRange", new SearchParameter
                     {
                        VariableName = "SAPoReceivedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND (sa.po_received_date >= {0} AND sa.po_received_date <= {1})))",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "SAPoAmountFrom", new SearchParameter
                     {
                        VariableName = "SAPoAmountFrom",
                        ParameterType = typeof(decimal),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.po_amount >= {0}))"
                     }                  
               },
               {
                  "SAPoAmountTo", new SearchParameter
                     {
                        VariableName = "SAPoAmountTo",
                        ParameterType = typeof(decimal),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.po_amount <= {0}))"
                     }                  
               },
               {
                  "SAPoAmountRange", new SearchParameter
                     {
                        VariableName = "SAPoAmountRange",
                        ParameterType = typeof(decimal),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND (sa.po_amount >= {0} AND sa.po_amount <= {1})))",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "SAPoNumber", new SearchParameter
                     {
                        VariableName = "SAPoNumber",
                        ParameterType = typeof(string),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.po_number LIKE {0}))",
                                      IsWildcardSearch = true
                     }                  
               },
               {
                  "SAType", new SearchParameter
                     {
                        VariableName = "SAType",
                        ParameterType = typeof(int),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.structural_analysis_type_id = {0}))"
                     }                  
               },
               {
                  "SAOrderedFrom", new SearchParameter
                     {
                        VariableName = "SAOrderedFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.sa_ordered_date >= {0}))"
                     }                  
               },
               {
                  "SAOrderedTo", new SearchParameter
                     {
                        VariableName = "SAOrderedTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.sa_ordered_date <= {0}))"
                     }                  
               },
               {
                  "SAOrderedRange", new SearchParameter
                     {
                        VariableName = "SAOrderedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND (sa.sa_ordered_date >= {0} AND sa.sa_ordered_date <= {1})))",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "SAOrderedStatus", new SearchParameter
                     {
                        VariableName = "SAOrderedStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.sa_ordered_date_status_id = {0}))"
                     }                  
               },
               {
                  "SAReceivedFrom", new SearchParameter
                     {
                        VariableName = "SAReceivedFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.sa_received_date >= {0}))"
                     }                  
               },
               {
                  "SAReceivedTo", new SearchParameter
                     {
                        VariableName = "SAReceivedTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.sa_received_date <= {0}))"
                     }                  
               },
               {
                  "SAReceivedRange", new SearchParameter
                     {
                        VariableName = "SAReceivedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND (sa.sa_received_date >= {0} AND sa.sa_received_date <= {1})))",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "SAReceivedStatus", new SearchParameter
                     {
                        VariableName = "SAReceivedStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.sa_received_date_status_id = {0}))"
                     }                  
               },
               {
                  "SAOrderOnholdFrom", new SearchParameter
                     {
                        VariableName = "SAOrderOnholdFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.sa_order_onhold_date >= {0}))"
                     }                  
               },
               {
                  "SAOrderOnholdTo", new SearchParameter
                     {
                        VariableName = "SAOrderOnholdTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.sa_order_onhold_date <= {0}))"
                     }                  
               },
               {
                  "SAOrderOnholdRange", new SearchParameter
                     {
                        VariableName = "SAOrderOnholdRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND (sa.sa_order_onhold_date >= {0} AND sa.sa_order_onhold_date <= {1})))",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "SAOrderCancelledFrom", new SearchParameter
                     {
                        VariableName = "SAOrderCancelledFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.sa_order_cancelled_date >= {0}))"
                     }                  
               },
               {
                  "SAOrderCancelledTo", new SearchParameter
                     {
                        VariableName = "SAOrderCancelledTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.sa_order_cancelled_date <= {0}))"
                     }                  
               },
               {
                  "SAOrderCancelledRange", new SearchParameter
                     {
                        VariableName = "SAOrderCancelledRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND (sa.sa_order_cancelled_date >= {0} AND sa.sa_order_cancelled_date <= {1})))",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "SAFeeReceivedFrom", new SearchParameter
                     {
                        VariableName = "SAFeeReceivedFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.sa_fee_received_date >= {0}))"
                     }                  
               },
               {
                  "SAFeeReceivedTo", new SearchParameter
                     {
                        VariableName = "SAFeeReceivedTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.sa_fee_received_date <= {0}))"
                     }                  
               },
               {
                  "SAFeeReceivedRange", new SearchParameter
                     {
                        VariableName = "SAFeeReceivedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND (sa.sa_fee_received_date >= {0} AND sa.sa_fee_received_date <= {1})))",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "SAFeeAmountFrom", new SearchParameter
                     {
                        VariableName = "SAFeeAmountFrom",
                        ParameterType = typeof(decimal),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.sa_fee_amount >= {0}))"
                     }                  
               },
               {
                  "SAFeeAmountTo", new SearchParameter
                     {
                        VariableName = "SAFeeAmountTo",
                        ParameterType = typeof(decimal),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.sa_fee_amount <= {0}))"
                     }                  
               },
               {
                  "SAFeeAmountRange", new SearchParameter
                     {
                        VariableName = "SAFeeAmountRange",
                        ParameterType = typeof(decimal),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND (sa.sa_fee_amount >= {0} AND sa.sa_fee_amount <= {1})))",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "SACheckNumber", new SearchParameter
                     {
                        VariableName = "SACheckNumber",
                        ParameterType = typeof(string),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.sa_check_nbr LIKE {0}))",
                                      IsWildcardSearch = true
                     }                  
               },
               {
                  "SAFeePayorType", new SearchParameter
                     {
                        VariableName = "SAFeePayorType",
                        ParameterType = typeof(int),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.structural_analysis_fee_payor_type_id = {0}))"
                     }                  
               },
               {
                  "SAVendor", new SearchParameter
                     {
                        VariableName = "SAVendor",
                        ParameterType = typeof(int),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.sa_vendor_id = {0}))"
                     }                  
               },
               {
                  "SASacNameEMP", new SearchParameter
                     {
                        VariableName = "SASacNameEMP",
                        ParameterType = typeof(int),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.sac_name_emp_id = {0}))"
                     }                  
               },
               {
                  "SASawToCustomerFrom", new SearchParameter
                     {
                        VariableName = "SASawToCustomerFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.saw_to_customer_date >= {0}))"
                     }                  
               },
               {
                  "SASawToCustomerTo", new SearchParameter
                     {
                        VariableName = "SASawToCustomerTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.saw_to_customer_date <= {0}))"
                     }                  
               },
               {
                  "SASawToCustomerRange", new SearchParameter
                     {
                        VariableName = "SASawToCustomerRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND (sa.saw_to_customer_date >= {0} AND sa.saw_to_customer_date <= {1})))",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "SASawApprovedFrom", new SearchParameter
                     {
                        VariableName = "SASawApprovedFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.saw_approved_date >= {0}))"
                     }                  
               },
               {
                  "SASawApprovedTo", new SearchParameter
                     {
                        VariableName = "SASawApprovedTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.saw_approved_date <= {0}))"
                     }                  
               },
               {
                  "SASawApprovedRange", new SearchParameter
                     {
                        VariableName = "SASawApprovedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND (sa.saw_approved_date >= {0} AND sa.saw_approved_date <= {1})))",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "SASawToSACFrom", new SearchParameter
                     {
                        VariableName = "SASawToSACFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.saw_to_sac_date >= {0}))"
                     }                  
               },
               {
                  "SASawToSACTo", new SearchParameter
                     {
                        VariableName = "SASawToSACTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.saw_to_sac_date <= {0}))"
                     }                  
               },
               {
                  "SASawToSACRange", new SearchParameter
                     {
                        VariableName = "SASawToSACRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND (sa.saw_to_sac_date >= {0} AND sa.saw_to_sac_date <= {1})))",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "SARequestFrom", new SearchParameter
                     {
                        VariableName = "SARequestFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.saw_request_date >= {0}))"
                     }                  
               },
               {
                  "SARequestTo", new SearchParameter
                     {
                        VariableName = "SARequestTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.saw_request_date <= {0}))"
                     }                  
               },
               {
                  "SARequestRange", new SearchParameter
                     {
                        VariableName = "SARequestRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND (sa.saw_request_date >= {0} AND sa.saw_request_date <= {1})))",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "SASacPriority", new SearchParameter
                     {
                        VariableName = "SASacPriority",
                        ParameterType = typeof(int),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.sac_priority_id = {0}))"
                     }                  
               },
               {
                  "SASacDescription", new SearchParameter
                     {
                        VariableName = "SASacDescription",
                        ParameterType = typeof(int),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.sac_description_id = {0}))"
                     }                  
               },
               {
                  "SAShoppingCartNumber", new SearchParameter
                     {
                        VariableName = "SAShoppingCartNumber",
                        ParameterType = typeof(string),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.shopping_cart_number LIKE {0}))",
                        IsWildcardSearch = true
                     }                  
               },
               {
                  "SAPoReleaseStatus", new SearchParameter
                     {
                        VariableName = "SAPoReleaseStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.po_release_status_id = {0}))"
                     }                  
               },
               {
                  "SAPoReleaseFrom", new SearchParameter
                     {
                        VariableName = "SAPoReleaseFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.po_release_date >= {0}))"
                     }                  
               },
               {
                  "SAPoReleaseTo", new SearchParameter
                     {
                        VariableName = "SAPoReleaseTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.po_release_date <= {0}))"
                     }                  
               },
               {
                  "SAPoReleaseRange", new SearchParameter
                     {
                        VariableName = "SAPoReleaseRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND (sa.po_release_date >= {0} AND sa.po_release_date <= {1})))",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "SANotes", new SearchParameter
                     {
                        VariableName = "SANotes",
                        ParameterType = typeof(string),
                        WhereClause = "(EXISTS(SELECT VALUE sa FROM [CPTTEntities].[structural_analyses] AS sa " +
                                      "WHERE sa.lease_application_id = it.id AND sa.po_notes LIKE {0}))",
                        IsWildcardSearch = true
                     }                  
               },
               {
                  "RevisionReceivedFrom", new SearchParameter
                     {
                        VariableName = "RevisionReceivedFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE larv FROM [CPTTEntities].[leaseapp_revisions] AS larv " +
                                      "WHERE larv.lease_application_id = it.id AND larv.revision_rcvd_date >= {0}))"
                     }                  
               },
               {
                  "RevisionReceivedTo", new SearchParameter
                     {
                        VariableName = "RevisionReceivedTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE larv FROM [CPTTEntities].[leaseapp_revisions] AS larv " +
                                      "WHERE larv.lease_application_id = it.id AND larv.revision_rcvd_date <= {0}))"
                     }                  
               },
               {
                  "RevisionReceivedRange", new SearchParameter
                     {
                        VariableName = "RevisionReceivedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE larv FROM [CPTTEntities].[leaseapp_revisions] AS larv " +
                                      "WHERE larv.lease_application_id = it.id AND (larv.revision_rcvd_date >= {0} AND larv.revision_rcvd_date <= {1})))",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "RevisionFeeReceivedFrom", new SearchParameter
                     {
                        VariableName = "RevisionFeeReceivedFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE larv FROM [CPTTEntities].[leaseapp_revisions] AS larv " +
                                      "WHERE larv.lease_application_id = it.id AND larv.revision_fee_rcvd_date >= {0}))"
                     }                  
               },
               {
                  "RevisionFeeReceivedTo", new SearchParameter
                     {
                        VariableName = "RevisionFeeReceivedTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE larv FROM [CPTTEntities].[leaseapp_revisions] AS larv " +
                                      "WHERE larv.lease_application_id = it.id AND larv.revision_fee_rcvd_date <= {0}))"
                     }                  
               },
               {
                  "RevisionFeeReceivedRange", new SearchParameter
                     {
                        VariableName = "RevisionFeeReceivedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE larv FROM [CPTTEntities].[leaseapp_revisions] AS larv " +
                                      "WHERE larv.lease_application_id = it.id AND (larv.revision_fee_rcvd_date >= {0} AND larv.revision_fee_rcvd_date <= {1})))",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "RevisionFeeCheckPONumber", new SearchParameter
                     {
                        VariableName = "RevisionFeeCheckPONumber",
                        ParameterType = typeof(int),
                        WhereClause = "(EXISTS(SELECT VALUE larv FROM [CPTTEntities].[leaseapp_revisions] AS larv " +
                                      "WHERE larv.lease_application_id = it.id AND larv.revision_fee_check_po_nbr = {0}))"
                     }                  
               },
               {
                  "RevisionFeeAmountFrom", new SearchParameter
                     {
                        VariableName = "RevisionFeeAmountFrom",
                        ParameterType = typeof(decimal),
                        WhereClause = "(EXISTS(SELECT VALUE larv FROM [CPTTEntities].[leaseapp_revisions] AS larv " +
                                      "WHERE larv.lease_application_id = it.id AND larv.revision_fee_amount >= {0}))"
                     }                  
               },
               {
                  "RevisionFeeAmountTo", new SearchParameter
                     {
                        VariableName = "RevisionFeeAmountTo",
                        ParameterType = typeof(decimal),
                        WhereClause = "(EXISTS(SELECT VALUE larv FROM [CPTTEntities].[leaseapp_revisions] AS larv " +
                                      "WHERE larv.lease_application_id = it.id AND larv.revision_fee_amount <= {0}))"
                     }                  
               },
               {
                  "RevisionFeeAmountRange", new SearchParameter
                     {
                        VariableName = "RevisionFeeAmountRange",
                        ParameterType = typeof(decimal),
                        WhereClause = "(EXISTS(SELECT VALUE larv FROM [CPTTEntities].[leaseapp_revisions] AS larv " +
                                      "WHERE larv.lease_application_id = it.id AND (larv.revision_fee_amount >= {0} AND larv.revision_fee_amount <= {1})))",
                        IsRangeSearch = true
                     }                  
               }
            };

         return results;
      }
   }
}