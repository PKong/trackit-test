﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using TrackIT2.DAL;
using TrackIT2.Objects;
using System.Transactions;
namespace TrackIT2.BLL
{
    public class LeaseAdminComments
    {
        #region ExecuteQuery

        public static List<lease_admin_comments> GetComments(int lease_admin_id,CPTTEntities ceRef = null)
        {
            bool isSuccess = false;
            bool isRootTransaction = true;
            List<lease_admin_comments> listRet = new List<lease_admin_comments>();

            CPTTEntities ce;
            if (ceRef == null)
            {
                //using new entity object
                ce = new CPTTEntities();
            }
            else
            {
                //using current entity object
                ce = ceRef;
                isRootTransaction = false;
            }

            // Start transaction for get all comments.
            try
            {
                if (isRootTransaction)
                {
                    // Manually open connection to prevent EF from auto closing
                    // connection and causing issues with future queries.
                    ce.Connection.Open();
                }

               
                var query = from sa in ce.lease_admin_comments
                            where sa.lease_admin_record_id == lease_admin_id
                            orderby sa.created_at descending
                            select sa;
                listRet.AddRange(query.ToList<lease_admin_comments>());
                isSuccess = true;
            }
            catch (OptimisticConcurrencyException oex)
            {
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(oex);
                isSuccess = false;
                listRet = null;
            }
            catch (Exception ex)
            {
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                isSuccess = false;
                listRet = null;
            }
            finally
            {
                if (isRootTransaction)
                {
                    ce.Connection.Dispose();
                }
            }

            // Finally accept all changes from the transaction.
            if (isSuccess && isRootTransaction)
            {
                ce.Dispose();
            }

            return listRet;
        }
       
        #endregion

        #region ExecuteNonquery

        public static string Add(lease_admin_comments leaseadmincomment, CPTTEntities ceRef = null)
        {
            String results = null;
            bool isSuccess = false;
            bool isRootTransaction = true;

            CPTTEntities ce;
            if (ceRef == null)
            {
                //using new entity object
                ce = new CPTTEntities();
            }
            else
            {
                //using current entity object
                ce = ceRef;
                isRootTransaction = false;
            }

            // Start transaction for insert a comment.
            try
            {
                using (TransactionScope trans = TransactionUtils.CreateTransactionScope())
                {
                    if (isRootTransaction)
                    {
                        // Manually open connection to prevent EF from auto closing
                        // connection and causing issues with future queries.
                        ce.Connection.Open();
                    }

                    // Checking for version
                    ce.lease_admin_comments.AddObject(leaseadmincomment);

                    ce.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                    isSuccess = true;
                    trans.Complete();     // Transaction complete
                }
            }
            catch (OptimisticConcurrencyException oex)
            {
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(oex);

                results = "The current record has been modifed since you have " +
                          "last retrieved it. Please reload the record and " +
                          "attempt to save it again.";
            }
            catch (Exception ex)
            {
                // Error occurs
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                isSuccess = false;
                results = "An error occurred saving the record. Please try again.";
            }
            finally
            {
                if (isRootTransaction)
                {
                    ce.Connection.Dispose();
                }
            }

            // Finally accept all changes from the transaction.
            if (isSuccess && isRootTransaction)
            {
                ce.Dispose();
            }

            return results;
        }

        public static string Update(lease_admin_comments leaseadmincomment, CPTTEntities ceRef = null)
        {
            String results = null;
            bool isSuccess = false;
            bool isRootTransaction = true;

            CPTTEntities ce;
            if (ceRef == null)
            {
                //using new entity object
                ce = new CPTTEntities();
            }
            else
            {
                //using current entity object
                ce = ceRef;
                isRootTransaction = false;
            }

            // Start transaction for update a comment.
            try
            {
                using (TransactionScope trans = TransactionUtils.CreateTransactionScope())
                {
                    if (isRootTransaction)
                    {
                        // Manually open connection to prevent EF from auto closing
                        // connection and causing issues with future queries.
                        ce.Connection.Open();
                    }

                    // Checking for version
                    ce.AttachUpdated(leaseadmincomment);
                    ce.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);

                    isSuccess = true;
                    trans.Complete();     // Transaction complete
                }
            }
            catch (OptimisticConcurrencyException oex)
            {
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(oex);

                results = "The current record has been modifed since you have " +
                          "last retrieved it. Please reload the record and " +
                          "attempt to save it again.";
            }
            catch (Exception ex)
            {
                // Error occurs
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                isSuccess = false;
                results = "An error occurred saving the record. Please try again.";
            }
            finally
            {
                if (isRootTransaction)
                {
                    ce.Connection.Dispose();
                }
            }

            // Finally accept all changes from the transaction.
            if (isSuccess && isRootTransaction)
            {
                ce.Dispose();
            }

            return results;
        }

        public static string Delete(int id, CPTTEntities ceRef = null)
        {
            String results = null;
            bool isSuccess = false;
            bool isRootTransaction = true;

            CPTTEntities ce;
            if (ceRef == null)
            {
                //using new entity object
                ce = new CPTTEntities();
            }
            else
            {
                //using current entity object
                ce = ceRef;
                isRootTransaction = false;
            }

            // Start transaction for delete a comment.
            try
            {
                using (TransactionScope trans = TransactionUtils.CreateTransactionScope())
                {
                    if (isRootTransaction)
                    {
                        // Manually open connection to prevent EF from auto closing
                        // connection and causing issues with future queries.
                        ce.Connection.Open();
                    }

                    // Checking for version
                    var query = from c in ce.lease_admin_comments
                                where c.id == id
                                select c;
                    if (query.ToList().Count > 0)
                    {
                        lease_admin_comments result = query.ToList()[0];
                        ce.lease_admin_comments.DeleteObject(result);

                        // Change DocLog
                        DocumentsChangeLog.UpdateIsLinkRecordFlag(id, DMSDocumentLinkHelper.eDocumentType.lease_admin.ToString(), false, ce,
                            DMSDocumentLinkHelper.eDocumentField.LA_Comments.ToString());
                        ce.SaveChanges();
                    }

                    isSuccess = true;
                    trans.Complete();     // Transaction complete
                }
            }
            catch (OptimisticConcurrencyException oex)
            {
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(oex);

                results = "The current record has been modifed since you have " +
                          "last retrieved it. Please reload the record and " +
                          "attempt to save it again.";
            }
            catch (Exception ex)
            {
                // Error occurs
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                isSuccess = false;
                results = "An error occurred saving the record. Please try again.";
            }
            finally
            {
                if (isRootTransaction)
                {
                    ce.Connection.Dispose();
                }
            }

            // Finally accept all changes from the transaction.
            if (isSuccess && isRootTransaction)
            {
                ce.Dispose();
            }

            return results;
        }
        #endregion
    }

    public class LeaseAdminCommentData
    {
        public void LeaseAdminComment()
        { 
        
        }
        public int comment_id;
        public int leaseadmin_id;
        public string comment;
        public string created_at;
        public int? creator_id;
        public string updater;
        public string update_at;
    }
}