﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using TrackIT2.DAL;
using TrackIT2.Objects;
using System.Transactions;

namespace TrackIT2.BLL
{
    public static class LeaseAdminRecord
    {
        #region Search       

        public static lease_admin_records Search(int id, CPTTEntities ceRef = null)
		{
             bool isSuccess = false;
             bool isRootTransaction = true;
             lease_admin_records ret;

             CPTTEntities ce;
             if (ceRef == null)
             {
                 //using new entity object
                 ce = new CPTTEntities();
             }
             else
             {
                 //using current entity object
                 ce = ceRef;
                 isRootTransaction = false;
             }

             try
             {
                 if (isRootTransaction)
                 {
                     // Manually open connection to prevent EF from auto closing
                     // connection and causing issues with future queries.
                     ce.Connection.Open();
                 }

                 ret = ce.lease_admin_records.FirstOrDefault(it => it.id.Equals(id));
                 isSuccess = true;
             }
             catch (OptimisticConcurrencyException oex)
             {
                 // Log error in ELMAH for any diagnostic needs.
                 Elmah.ErrorSignal.FromCurrentContext().Raise(oex);
                 isSuccess = false;
                 ret = null;
             }
             catch (Exception ex)
             {
                 // Log error in ELMAH for any diagnostic needs.
                 Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                 isSuccess = false;
                 ret = null;
             }
             finally
             {
                 if (isRootTransaction)
                 {
                     ce.Connection.Dispose();
                 }
             }

             if (isSuccess && isRootTransaction)
             {
                 ce.Dispose();
             }

             return ret;
		}

        #endregion

        #region ExecuteNonquery

        public static string Add(lease_admin_records leaseadmin, CPTTEntities ceRef = null)
        {
            String results = null;
            bool isSuccess = false;
            bool isRootTransaction = true;

            CPTTEntities ce;
            if (ceRef == null)
            {
                //using new entity object
                ce = new CPTTEntities();
            }
            else
            {
                //using current entity object
                ce = ceRef;
                isRootTransaction = false;
            }

            // Start transaction for insert a Lease admin record.
            try
            {
                using (TransactionScope trans = TransactionUtils.CreateTransactionScope())
                {
                    if (isRootTransaction)
                    {
                        // Manually open connection to prevent EF from auto closing
                        // connection and causing issues with future queries.
                        ce.Connection.Open();
                    }

                    // Checking for version
                    ce.lease_admin_records.AddObject(leaseadmin);
                    ce.SaveChanges();
                    isSuccess = true;
                    trans.Complete();     // Transaction complete
                }
            }
            catch (OptimisticConcurrencyException oex)
            {
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(oex);

                results = "The current record has been modifed since you have " +
                          "last retrieved it. Please reload the record and " +
                          "attempt to save it again.";
            }
            catch (Exception ex)
            {
                // Error occurs
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                isSuccess = false;
                results = "An error occurred saving the record. Please try again.";
            }
            finally
            {
                if (isRootTransaction)
                {
                    ce.Connection.Dispose();
                }
            }

            // Finally accept all changes from the transaction.
            if (isSuccess && isRootTransaction)
            {
                ce.Dispose();
            }

            return results;
        }

        public static string Update(lease_admin_records leaseadmin, CPTTEntities ceRef = null)
        {
            String results = null;
            bool isSuccess = false;
            bool isRootTransaction = true;

            CPTTEntities ce;
            if (ceRef == null)
            {
                //using new entity object
                ce = new CPTTEntities();
            }
            else
            {
                //using current entity object
                ce = ceRef;
                isRootTransaction = false;
            }

            // Start transaction for update a Lease Admin record.
            try
            {
                using (TransactionScope trans = TransactionUtils.CreateTransactionScope())
                {
                    if (isRootTransaction)
                    {
                        // Manually open connection to prevent EF from auto closing
                        // connection and causing issues with future queries.
                        ce.Connection.Open();
                    }

                    // Checking for version
                    ce.AttachUpdated(leaseadmin);
                    ce.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);

                    isSuccess = true;
                    trans.Complete();     // Transaction complete
                }
            }
            catch (OptimisticConcurrencyException oex)
            {
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(oex);

                results = "The current record has been modifed since you have " +
                          "last retrieved it. Please reload the record and " +
                          "attempt to save it again.";
            }
            catch (Exception ex)
            {
                // Error occurs
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                isSuccess = false;
                results = "An error occurred saving the record. Please try again.";
            }
            finally
            {
                if (isRootTransaction)
                {
                    ce.Connection.Dispose();
                }
            }

            // Finally accept all changes from the transaction.
            if (isSuccess && isRootTransaction)
            {
                ce.Dispose();
            }

            return results;

        }

        public static String Delete(int id, CPTTEntities ceRef = null)
        {
            String sResult = null;
            bool isSuccess = false;
            bool isRootTransaction = true;

            CPTTEntities ce;
            if (ceRef == null)
            {
                //using new entity object
                ce = new CPTTEntities();
            }
            else
            {
                //using current entity object
                ce = ceRef;
                isRootTransaction = false;
            }

            try
            {
                using (TransactionScope trans = TransactionUtils.CreateTransactionScope())
                {
                    if (isRootTransaction)
                    {
                        // Manually open connection to prevent EF from auto closing
                        // connection and causing issues with future queries.
                        ce.Connection.Open();
                    }
                    
                    //remove any relate link document
                    var documents = from doc in ce.documents_links
                                    where doc.record_id == id
                                    select doc;

                    if (documents.Count() > 0)
                    {
                        foreach (documents_links item in documents.ToList<documents_links>())
                        {
                            ce.DeleteObject(item);
                        }
                    }

                    var queryComment = from com in ce.lease_admin_comments
                                       where com.lease_admin_record_id == id
                                       select com;

                    if (queryComment.Count() > 0)
                    {
                        foreach (lease_admin_comments item in queryComment.ToList<lease_admin_comments>())
                        {
                            ce.DeleteObject(item);
                        }
                    }

                    //Delete record
                    var queryLeaseAdmin = from revision in ce.lease_admin_records
                                        where revision.id == id
                                        select revision;

                    if (queryLeaseAdmin.Any())
                    {
                        lease_admin_records leaseAdmin = queryLeaseAdmin.FirstOrDefault();
                        for (int i = 0; i < leaseAdmin.sites.Count; i++ )
                        {
                            leaseAdmin.sites.Remove(leaseAdmin.sites.ToList()[i]);
                        }
                        ce.lease_admin_records.DeleteObject(leaseAdmin);
                    }
                    else
                    {
                        throw new NullReferenceException();
                    }

                    if (isRootTransaction)
                    {
                        // We tentatively save the changes so that we can finish 
                        // processing the transaction.foreach (var bk in books)
                        ce.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                    }

                    isSuccess = true;
                    trans.Complete();     //Transaction complete.
                }
            }
            catch (Exception ex)
            {
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                sResult = ex.ToString();
                isSuccess = false;
            }
            finally
            {
                if (isRootTransaction)
                {
                    ce.Connection.Dispose();
                }
            }

            // Finally accept all changes from the transaction.
            if (isSuccess && isRootTransaction)
            {
                ce.AcceptAllChanges();
                ce.Dispose();
            }

            return sResult;
        }
        #endregion

        // All issue tracker search criteria in a dictionary for easy lookup/manipulation
        // when dynamically building the query.
        public static Dictionary<string, SearchParameter> GetSearchCriteria()
        {
           var results = new Dictionary<string, SearchParameter>
            {
                {
                  "General", new SearchParameter
                     {                        
                        VariableName = "General",
                        ParameterType = typeof(string),
                        WhereClause = "(it.sites.site_uid LIKE {0})",
                        IsWildcardSearch = true,
                        IgnoreProcessing = true
                     }
               },
               {
                  "SiteID", new SearchParameter
                     {                        
                        VariableName = "SiteID",
                        ParameterType = typeof(string),
                        WhereClause = "(it.sites.site_uid LIKE {0})",
                        IsWildcardSearch = true,
                        IgnoreProcessing = true
                     }
               },
               {
                  "Customer", new SearchParameter
                     {                        
                        VariableName = "Customer",
                        ParameterType = typeof(int),
                        WhereClause = "((it.lease_applications.customer_id = {0}) OR (it.customer_id = {0}))",
                        AllowsMultipleValues = true
                     }
               },
               {
                  // Lease Phase is a special calculated field based on
                  // various factors, so a special variable comparison where
                  // clause is generated.
                  "LeasePhase", new SearchParameter
                     {
                        VariableName = "LeasePhase",
                        ParameterType = typeof(string),
                        WhereClause = "(" + 
                                      "    ({0} = 'Execution' AND ((it.lease_execution_date is null || it.lease_execution_date_status_id != 1) || (it.lease_applications.lease_execution_date is null || it.lease_applications.lease_execution_date_status_id != 1)))" + 
                                      "    OR" + 
                                      "    ({0} = 'Commencement' AND ((it.lease_execution_date is not null && it.lease_execution_date_status_id = 1) || (it.lease_applications.lease_execution_date is not null && it.lease_applications.lease_execution_date_status_id = 1)))" +                                       
                                      ")",
                        AllowsMultipleValues = true
                     }
               },
               {
                  "ExecutionDateFrom", new SearchParameter
                     {
                        VariableName = "ExecutionDateFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.lease_execution_date >= {0} OR it.lease_applications.lease_execution_date >= {0})"
                     }
               },
               {
                  "ExecutionDateTo", new SearchParameter
                     {
                        VariableName = "ExecutionDateTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.lease_execution_date <= {0} OR it.lease_applications.lease_execution_date <= {0})"
                     }
               },
               {
                  "ExecutionDateRange", new SearchParameter
                     {
                        VariableName = "ExecutionDateRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "((it.lease_execution_date >= {0} OR it.lease_applications.lease_execution_date >= {0}) AND (it.lease_execution_date <= {1} OR it.lease_applications.lease_execution_date <= {1}))",
                        IsRangeSearch = true
                     }
               },
               {
                  "CommencementDateFrom", new SearchParameter
                     {
                        VariableName = "CommencementDateFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.commencement_date >= {0})"
                     }
               },
               {
                  "CommencementDateTo", new SearchParameter
                     {
                        VariableName = "CommencementDateTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.commencement_date <= {0})"
                     }
               },
               {
                  "CommencementDateRange", new SearchParameter
                     {
                        VariableName = "CommencementDateRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.commencement_date >= {0} AND it.commencement_date <= {1})",
                        IsRangeSearch = true
                     }
               },
               {
                  "CreateUser", new SearchParameter
                     {
                        VariableName = "CreateUser",                                                                  
                        ParameterType = typeof(int),
                        WhereClause = "(it.creator_id = {0})",
                        AllowsMultipleValues = true
                     }
               },
               {
                  "RevenueShare", new SearchParameter
                     {
                        VariableName = "RevenueShare",
                        ParameterType = typeof(bool),
                        WhereClause = "((it.revenue_share_yn = {0}) or (it.lease_applications.revenue_share_yn = {0}))",
                        AllowsMultipleValues = true
                     }
               },
               {
                  "DocumentType__Exec__", new SearchParameter
                     {
                        VariableName = "DocumentType__Exec__",
                        ParameterType = typeof(int),
                        WhereClause = "(it.lease_admin_execution_document_type_id = {0})",
                        AllowsMultipleValues = true
                     }
               },
               {
                  "DocumentType__Comm__", new SearchParameter
                     {
                        VariableName = "DocumentType__Comm__",
                        ParameterType = typeof(int),
                        WhereClause = "(it.lease_admin_commencement_document_type_id = {0})",
                        AllowsMultipleValues = true
                     }
               },
               {
                  "LeaseAppID", new SearchParameter
                     {
                        VariableName = "LeaseAppID",
                        ParameterType = typeof(int),
                        WhereClause = "(it.lease_application_id = {0})"
                     }
               },
               {
                  "LockVersion", new SearchParameter
                     {
                        VariableName = "LockVersion",
                        ParameterType = typeof(int),
                        WhereClause = "(it.lock_version = {0})"
                     }
               },
               {
                  "LeaseApplicationID", new SearchParameter
                     {
                        VariableName = "LeaseApplicationID",
                        ParameterType = typeof(int),
                        WhereClause = "(it.lease_application_id = {0})"
                     }
               },
               {
                  "ExecutablesSentToCustomerDateFrom", new SearchParameter
                     {
                        VariableName = "ExecutablesSentToCustomerDateFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.leasedoc_sent_to_cust_date >= {0} OR it.lease_applications.leasedoc_sent_to_cust_date >= {0})"
                     }
               },
               {
                  "ExecutablesSentToCustomerDateTo", new SearchParameter
                     {
                        VariableName = "ExecutablesSentToCustomerDateTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.leasedoc_sent_to_cust_date <= {0} OR it.lease_applications.leasedoc_sent_to_cust_date <= {0})"
                     }
               },
               {
                  "ExecutablesSentToCustomerDateRange", new SearchParameter
                     {
                        VariableName = "ExecutablesSentToCustomerDateRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "((it.leasedoc_sent_to_cust_date >= {0} OR it.lease_applications.leasedoc_sent_to_cust_date >= {0}) AND (it.leasedoc_sent_to_cust_date <= {1} OR it.lease_applications.leasedoc_sent_to_cust_date <= {1}))",
                        IsRangeSearch = true
                     }
               },
               {
                  "ExecutablesSentToCustomer", new SearchParameter
                     {
                        VariableName = "ExecutablesSentToCustomer",
                        ParameterType = typeof(int),
                        WhereClause = "(it.leasedoc_sent_to_cust_date_status_id = {0} OR it.lease_applications.leasedoc_sent_to_cust_date_status_id = {0})"
                     }
               },
               {
                  "DocsBackFromCustomerDateFrom", new SearchParameter
                     {
                        VariableName = "DocsBackFromCustomerDateFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.lease_back_from_cust_date >= {0} OR it.lease_applications.lease_back_from_cust_date >= {0})"
                     }
               },
               {
                  "DocsBackFromCustomerDateTo", new SearchParameter
                     {
                        VariableName = "DocsBackFromCustomerDateTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.lease_back_from_cust_date <= {0} OR it.lease_applications.lease_back_from_cust_date <= {0})"
                     }
               },
               {
                  "DocsBackFromCustomerDateRange", new SearchParameter
                     {
                        VariableName = "DocsBackFromCustomerDateRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "((it.lease_back_from_cust_date >= {0} OR it.lease_applications.lease_back_from_cust_date >= {0}) AND (it.lease_back_from_cust_date <= {1} OR it.lease_applications.lease_back_from_cust_date <= {1}))",
                        IsRangeSearch = true
                     }
               },
               {
                  "DocsBackFromCustomer", new SearchParameter
                     {
                        VariableName = "DocsBackFromCustomer",
                        ParameterType = typeof(int),
                        WhereClause = "(it.lease_back_from_cust_date_status_id = {0} OR it.lease_applications.lease_back_from_cust_date_status_id = {0})"
                     }
               },
               {
                  "SignedByCustomerFrom", new SearchParameter
                     {
                        VariableName = "SignedByCustomerFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.signed_by_cust_date >= {0} OR it.lease_applications.signed_by_cust_date >= {0})"
                     }
               },
               {
                  "SignedByCustomerTo", new SearchParameter
                     {
                        VariableName = "SignedByCustomerTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.signed_by_cust_date <= {0} OR it.lease_applications.signed_by_cust_date <= {0})"
                     }
               },
               {
                  "SignedByCustomerRange", new SearchParameter
                     {
                        VariableName = "SignedByCustomerRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "((it.signed_by_cust_date >= {0} OR it.lease_applications.signed_by_cust_date >= {0}) AND (it.signed_by_cust_date <= {1} OR it.lease_applications.signed_by_cust_date <= {1}))",
                        IsRangeSearch = true
                     }
               },
               {
                  "DocsToFSCFrom", new SearchParameter
                     {
                        VariableName = "DocsToFSCFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.leasedoc_to_fsc_date >= {0} OR it.lease_applications.leasedoc_to_fsc_date >= {0})"
                     }
               },
               {
                  "DocsToFSCTo", new SearchParameter
                     {
                        VariableName = "DocsToFSCTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.leasedoc_to_fsc_date <= {0} OR it.lease_applications.leasedoc_to_fsc_date <= {0})"
                     }
               },
               {
                  "DocsToFSCRange", new SearchParameter
                     {
                        VariableName = "DocsToFSCRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "((it.leasedoc_to_fsc_date >= {0} OR it.lease_applications.leasedoc_to_fsc_date >= {0}) AND (it.leasedoc_to_fsc_date <= {1} OR it.lease_applications.leasedoc_to_fsc_date <= {1}))",
                        IsRangeSearch = true
                     }
               },
               {
                  "DateReceivedAtFSC(Exec)From", new SearchParameter
                     {
                        VariableName = "DateReceivedAtFSCExecFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.date_received_at_fsc >= {0})"
                     }
               },
               {
                  "DateReceivedAtFSC(Exec)To", new SearchParameter
                     {
                        VariableName = "DateReceivedAtFSCExecTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.date_received_at_fsc <= {0})"
                     }
               },
               {
                  "DateReceivedAtFSC(Exec)Range", new SearchParameter
                     {
                        VariableName = "DateReceivedAtFSCExecRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.date_received_at_fsc >= {0} AND it.date_received_at_fsc <= {1})",
                        IsRangeSearch = true
                     }
               },
               {
                  "LeaseExecutionStatus", new SearchParameter
                     {
                        VariableName = "LeaseExecutionStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.lease_execution_date_status_id = {0} OR it.lease_applications.lease_execution_date_status_id = {0})"
                     }
               },
               {
                  "CommencementForecastFrom", new SearchParameter
                     {
                        VariableName = "CommencementForecastFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.commencement_forcast_date >= {0} OR it.lease_applications.commencement_forcast_date >= {0})"
                     }
               },
               {
                  "CommencementForecastTo", new SearchParameter
                     {
                        VariableName = "CommencementForecastTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.commencement_forcast_date <= {0} OR it.lease_applications.commencement_forcast_date <= {0})"
                     }
               },
               {
                  "CommencementForecastRange", new SearchParameter
                     {
                        VariableName = "CommencementForecastRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "((it.commencement_forcast_date >= {0} OR it.lease_applications.commencement_forcast_date >= {0}) AND (it.commencement_forcast_date <= {1} OR it.lease_applications.commencement_forcast_date <= {1}))",
                        IsRangeSearch = true
                     }
               },
               {
                  "CarrierSignatureNotarizedDateFrom", new SearchParameter
                     {
                        VariableName = "CarrierSignatureNotarizedDateFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.carrier_signature_notarized_date >= {0})"
                     }
               },
               {
                  "CarrierSignatureNotarizedDateTo", new SearchParameter
                     {
                        VariableName = "CarrierSignatureNotarizedDateTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.carrier_signature_notarized_date <= {0})"
                     }
               },
               {
                  "CarrierSignatureNotarizedDateRange", new SearchParameter
                     {
                        VariableName = "CarrierSignatureNotarizedDateRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.carrier_signature_notarized_date >= {0} AND it.carrier_signature_notarized_date <= {1})",
                        IsRangeSearch = true
                     }
               },
               {
                  "CarrierSignatureNotarized", new SearchParameter
                     {
                        VariableName = "CarrierSignatureNotarized",
                        ParameterType = typeof(bool),
                        WhereClause = "(it.carrier_signature_notarized_yn = {0})"
                     }
               },
               {
                  "SentToSignatoryFrom", new SearchParameter
                     {
                        VariableName = "SentToSignatoryFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.sent_to_signatory_date >= {0})"
                     }
               },
               {
                  "SentToSignatoryTo", new SearchParameter
                     {
                        VariableName = "SentToSignatoryTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.sent_to_signatory_date <= {0})"
                     }
               },
               {
                  "SentToSignatoryRange", new SearchParameter
                     {
                        VariableName = "SentToSignatoryRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.sent_to_signatory_date >= {0} AND it.sent_to_signatory_date <= {1})",
                        IsRangeSearch = true
                     }
               },
               {
                  "BackFromSignatoryFrom", new SearchParameter
                     {
                        VariableName = "BackFromSignatoryFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.back_from_signatory_date >= {0})"
                     }
               },
               {
                  "BackFromSignatoryTo", new SearchParameter
                     {
                        VariableName = "BackFromSignatoryTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.back_from_signatory_date <= {0})"
                     }
               },
               {
                  "BackFromSignatoryRange", new SearchParameter
                     {
                        VariableName = "BackFromSignatoryRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.back_from_signatory_date >= {0} AND it.back_from_signatory_date <= {1})",
                        IsRangeSearch = true
                     }
               },
               {
                  "OriginalSentToUPSFrom", new SearchParameter
                     {
                        VariableName = "OriginalSentToUPSFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.original_sent_to_ups_date >= {0})"
                     }
               },
               {
                  "OriginalSentToUPSTo", new SearchParameter
                     {
                        VariableName = "OriginalSentToUPSTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.original_sent_to_ups_date <= {0})"
                     }
               },
               {
                  "OriginalSentToUPSRange", new SearchParameter
                     {
                        VariableName = "OriginalSentToUPSRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.original_sent_to_ups_date >= {0} AND it.original_sent_to_ups_date <= {1})",
                        IsRangeSearch = true
                     }
               },
               {
                  "DocLoadedToCST", new SearchParameter
                     {
                        VariableName = "DocLoadedToCST",
                        ParameterType = typeof(bool),
                        WhereClause = "(it.doc_loaded_to_cst_yn = {0})"
                     }
               },
               {
                  "AmendmentRentAffecting", new SearchParameter
                     {
                        VariableName = "AmendmentRentAffecting",
                        ParameterType = typeof(bool),
                        WhereClause = "(it.amendment_rent_affecting_yn = {0})"
                     }
               },
               {
                  "OneTimeFee", new SearchParameter
                     {
                        VariableName = "OneTimeFee",
                        ParameterType = typeof(bool),
                        WhereClause = "(it.one_time_fee_yn = {0})"
                     }
               },
               {
                  "RentForecastAmountFrom", new SearchParameter
                     {
                        VariableName = "RentForecastAmountFrom",
                        ParameterType = typeof(decimal),
                        WhereClause = "(it.rent_forecast_amount >= {0} OR it.lease_applications.rent_forecast_amount >= {0})"
                     }
               },
               {
                  "RentForecastAmountTo", new SearchParameter
                     {
                        VariableName = "RentForecastAmountTo",
                        ParameterType = typeof(decimal),
                        WhereClause = "(it.rent_forecast_amount <= {0} OR it.lease_applications.rent_forecast_amount <= {0})"
                     }
               },
               {
                  "RentForecastAmountRange", new SearchParameter
                     {
                        VariableName = "RentForecastAmountRange",
                        ParameterType = typeof(decimal),
                        WhereClause = "((it.rent_forecast_amount >= {0} OR it.lease_applications.rent_forecast_amount >= {0}) AND (it.rent_forecast_amount <= {1} OR it.lease_applications.rent_forecast_amount <= {1}))",
                        IsRangeSearch = true
                     }
               },
               {
                  "RevshareLoadedToCSTReceivable", new SearchParameter
                     {
                        VariableName = "RevshareLoadedToCSTReceivable",
                        ParameterType = typeof(bool),
                        WhereClause = "(it.revshare_loaded_to_cst_receivable_yn = {0})"
                     }
               },
               {
                  "RevshareLoadedToCSTPayable", new SearchParameter
                     {
                        VariableName = "RevshareLoadedToCSTPayable",
                        ParameterType = typeof(bool),
                        WhereClause = "(it.revshare_loaded_to_cst_payable_yn = {0})"
                     }
               },
               {
                  "REMLeaseSequenceFrom", new SearchParameter
                     {
                        VariableName = "REMLeaseSequenceFrom",
                        ParameterType = typeof(int),
                        WhereClause = "(it.rem_lease_seq >= {0} OR it.lease_applications.rem_lease_seq >= {0})"
                     }
               },
               {
                  "REMLeaseSequenceTo", new SearchParameter
                     {
                        VariableName = "REMLeaseSequenceTo",
                        ParameterType = typeof(int),
                        WhereClause = "(it.rem_lease_seq <= {0} OR it.lease_applications.rem_lease_seq <= {0})"
                     }
               },
               {
                  "REMLeaseSequenceRange", new SearchParameter
                     {
                        VariableName = "REMLeaseSequenceRange",
                        ParameterType = typeof(int),
                        WhereClause = "((it.rem_lease_seq >= {0} OR it.lease_applications.rem_lease_seq >= {0}) AND (it.rem_lease_seq <= {1} OR it.lease_applications.rem_lease_seq <= {1}))",
                        IsRangeSearch = true
                     }
               },
               {
                  "CostShareAgreement", new SearchParameter
                     {
                        VariableName = "CostShareAgreement",
                        ParameterType = typeof(bool),
                        WhereClause = "((it.cost_share_agreement_yn = {0}) or (it.lease_applications.cost_share_agreement_yn = {0}))"
                     }
               },
               {
                  "CostShareAgreementAmountFrom", new SearchParameter
                     {
                        VariableName = "CostShareAgreementAmountFrom",
                        ParameterType = typeof(decimal),
                        WhereClause = "(it.cost_share_agreement_amount >= {0} OR it.lease_applications.cost_share_agreement_amount >= {0})"
                     }
               },
               {
                  "CostShareAgreementAmountTo", new SearchParameter
                     {
                        VariableName = "CostShareAgreementAmountTo",
                        ParameterType = typeof(decimal),
                        WhereClause = "(it.cost_share_agreement_amount <= {0} OR it.lease_applications.cost_share_agreement_amount <= {0})"
                     }
               },
               {
                  "CostShareAgreementAmountRange", new SearchParameter
                     {
                        VariableName = "CostShareAgreementAmountRange",
                        ParameterType = typeof(decimal),
                        WhereClause = "((it.cost_share_agreement_amount >= {0} OR it.lease_applications.cost_share_agreement_amount >= {0}) AND (it.cost_share_agreement_amount <= {1} OR it.lease_applications.cost_share_agreement_amount <= {1}))",
                        IsRangeSearch = true
                     }
               },
               {
                  "CapCostRecovery", new SearchParameter
                     {
                        VariableName = "CapCostRecovery",
                        ParameterType = typeof(bool),
                        WhereClause = "((it.cap_cost_recovery_yn = {0}) or (it.lease_applications.cap_cost_recovery_yn = {0}))"
                     }
               },
               {
                  "CapCostRecoveryAmountFrom", new SearchParameter
                     {
                        VariableName = "CapCostRecoveryAmountFrom",
                        ParameterType = typeof(decimal),
                        WhereClause = "(it.cap_cost_amount >= {0} OR it.lease_applications.cap_cost_amount >= {0})"
                     }
               },
               {
                  "CapCostRecoveryAmountTo", new SearchParameter
                     {
                        VariableName = "CapCostRecoveryAmountTo",
                        ParameterType = typeof(decimal),
                        WhereClause = "(it.cap_cost_amount <= {0} OR it.lease_applications.cap_cost_amount <= {0})"
                     }
               },
               {
                  "CapCostRecoveryAmountRange", new SearchParameter
                     {
                        VariableName = "CapCostRecoveryAmountRange",
                        ParameterType = typeof(decimal),
                        WhereClause = "((it.cap_cost_amount >= {0} OR it.lease_applications.cap_cost_amount >= {0}) AND (it.cap_cost_amount <= {1} OR it.lease_applications.cap_cost_amount <= {1}))",
                        IsRangeSearch = true
                     }
               },
               {
                  "TerminationDateFrom", new SearchParameter
                     {
                        VariableName = "TerminationDateFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.termination_date >= {0} OR it.lease_applications.termination_date >= {0})"
                     }
               },
               {
                  "TerminationDateTo", new SearchParameter
                     {
                        VariableName = "TerminationDateTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.termination_date <= {0} OR it.lease_applications.termination_date <= {0})"
                     }
               },
               {
                  "TerminationDateRange", new SearchParameter
                     {
                        VariableName = "TerminationDateRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "((it.termination_date >= {0} OR it.lease_applications.termination_date >= {0}) AND (it.termination_date <= {1} OR it.lease_applications.termination_date <= {1}))",
                        IsRangeSearch = true
                     }
               },
               {
                  "CommencementNTPToCustomerFrom", new SearchParameter
                     {
                        VariableName = "CommencementNTPToCustomerFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.commencement_ntp_to_customer_date >= {0})"
                     }
               },
               {
                  "CommencementNTPToCustomerTo", new SearchParameter
                     {
                        VariableName = "CommencementNTPToCustomerTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.commencement_ntp_to_customer_date <= {0})"
                     }
               },
               {
                  "CommencementNTPToCustomerRange", new SearchParameter
                     {
                        VariableName = "CommencementNTPToCustomerRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.commencement_ntp_to_customer_date >= {0} AND it.commencement_ntp_to_customer_date <= {1})",
                        IsRangeSearch = true
                     }
               },
               {
                  "DateReceivedAtFSC(Comm)From", new SearchParameter
                     {
                        VariableName = "DateReceivedAtFSCCommFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.date_received_at_fsc_date >= {0})"
                     }
               },
               {
                  "DateReceivedAtFSC(Comm)To", new SearchParameter
                     {
                        VariableName = "DateReceivedAtFSCCommTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.date_received_at_fsc_date <= {0})"
                     }
               },
               {
                  "DateReceivedAtFSC(Comm)Range", new SearchParameter
                     {
                        VariableName = "DateReceivedAtFSCCommRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.date_received_at_fsc_date >= {0} AND it.date_received_at_fsc_date <= {1})",
                        IsRangeSearch = true
                     }
               },
               {
                  "ReceivedBy", new SearchParameter
                     {
                        VariableName = "ReceivedBy",
                        ParameterType = typeof(int),
                        WhereClause = "(it.document_received_by_emp_id = {0})"
                     }
               },
               {
                  "RegCommencementLetterCreatedFrom", new SearchParameter
                     {
                        VariableName = "RegCommencementLetterCreatedFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.reg_commencement_letter_created_date >= {0})"
                     }
               },
               {
                  "RegCommencementLetterCreatedTo", new SearchParameter
                     {
                        VariableName = "RegCommencementLetterCreatedTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.reg_commencement_letter_created_date <= {0})"
                     }
               },
               {
                  "RegCommencementLetterCreatedRange", new SearchParameter
                     {
                        VariableName = "RegCommencementLetterCreatedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.reg_commencement_letter_created_date >= {0} AND it.reg_commencement_letter_created_date <= {1})",
                        IsRangeSearch = true
                     }
               },
               {
                  "SavedToDMS(Exec)From", new SearchParameter
                     {
                        VariableName = "SavedToDMSExecFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.saved_to_dms_date >= {0})"
                     }
               },
               {
                  "SavedToDMS(Exec)To", new SearchParameter
                     {
                        VariableName = "SavedToDMSExecTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.saved_to_dms_date <= {0})"
                     }
               },
               {
                  "SavedToDMS(Exec)Range", new SearchParameter
                     {
                        VariableName = "SavedToDMSExecRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.saved_to_dms_date >= {0} AND it.saved_to_dms_date <= {1})",
                        IsRangeSearch = true
                     }
               },
               {
                  "SavedToDMS(Comm)From", new SearchParameter
                     {
                        VariableName = "SavedToDMSCommFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.date_saved_to_dms_date >= {0})"
                     }
               },
               {
                  "SavedToDMS(Comm)To", new SearchParameter
                     {
                        VariableName = "SavedToDMSCommTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.date_saved_to_dms_date <= {0})"
                     }
               },
               {
                  "SavedToDMS(Comm)Range", new SearchParameter
                     {
                        VariableName = "SavedToDMSCommRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.date_saved_to_dms_date >= {0} AND it.date_saved_to_dms_date <= {1})",
                        IsRangeSearch = true
                     }
               },
               {
                  "REMAlert", new SearchParameter
                     {
                        VariableName = "REMAlert",
                        ParameterType = typeof(string),
                        WhereClause = "(it.rem_alert_comments LIKE {0})",
                        IsWildcardSearch = true
                     }
               },
               {
                  "CreatedAtFrom", new SearchParameter
                     {
                        VariableName = "CreatedAtFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.created_at >= {0})"
                     }
               },
               {
                  "CreatedAtTo", new SearchParameter
                     {
                        VariableName = "CreatedAtTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.created_at <= {0})"
                     }
               },
               {
                  "CreatedAtRange", new SearchParameter
                     {
                        VariableName = "CreatedAtRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.created_at >= {0} AND it.created_at <= {1})",
                        IsRangeSearch = true
                     }
               },
               {
                  "UpdatedAtFrom", new SearchParameter
                     {
                        VariableName = "UpdatedAtFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.updated_at >= {0})"
                     }
               },
               {
                  "UpdatedAtTo", new SearchParameter
                     {
                        VariableName = "UpdatedAtTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.updated_at <= {0})"
                     }
               },
               {
                  "UpdatedAtRange", new SearchParameter
                     {
                        VariableName = "UpdatedAtRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.updated_at >= {0} AND it.updated_at <= {1})",
                        IsRangeSearch = true
                     }
               },
               {
                  "CreatedBy", new SearchParameter
                     {
                        VariableName = "CreatedBy",
                        ParameterType = typeof(int),
                        WhereClause = "(it.creator_id = {0})"
                     }
               },
               {
                  "UpdatedBy", new SearchParameter
                     {
                        VariableName = "UpdatedBy",
                        ParameterType = typeof(int),
                        WhereClause = "(it.updater_id = {0})"
                     }
               }
            };

           return results;
        }
    }
}