﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using TrackIT2.DAL;

namespace TrackIT2.BLL
{
   public class MetaGroup
   {
      public static void Add(string name, string description)
      {
         // TODO: Add additional security/business logic in here as needed.
         CPTTEntities ce = new CPTTEntities();

         metagroup group = new metagroup();

         group.name = name;
         group.description = description;

         ce.metagroups.AddObject(group);

         // Exceptions are thrown up to the calling method to handle. Elmah will log
         // unhandled execptions.
         try
         {
            ce.SaveChanges();
         }
         catch (EntitySqlException)
         {
            throw;
         }
      }

      public static void Update(metagroup group, string name, string description)
      {
         // TODO: Add additional security/business logic in here as needed.

         // The current meta group is passed in before applying edits in order
         // to do any additional validation/processing.
         CPTTEntities ce = new CPTTEntities();

         ce.metagroups.Attach(group);

         group.name = name;
         group.description = description;

         // Exceptions are thrown up to the calling method to handle. Elmah will log
         // unhandled execptions.
         try
         {
            ce.SaveChanges();
         }
         catch (OptimisticConcurrencyException)
         {
            throw;
         }
         catch (EntitySqlException)
         {
            throw;
         }
      }

      public static void Delete(int id)
      {
         // TODO: Add additional security/business logic in here as needed.
         CPTTEntities ce = new CPTTEntities();

         metagroup group;

         group = ce.metagroups
                   .Where(mg => mg.id.Equals(id))
                   .First();

         ce.DeleteObject(group);
         ce.SaveChanges();
      }
   }
}