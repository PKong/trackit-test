﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TrackIT2.DAL;
using System.Transactions;
using System.Data;
using System.Data.Objects;

namespace TrackIT2.BLL
{
    public class DocumentsChangeLog
    {
        public static double RecentUploadCutoffTime
        {
            get
            {
                return double.Parse(System.Configuration.ConfigurationManager.AppSettings["RecentUploadsDocumentCutoffTime"]);
            }
        }

        public static string GenarateRecordLink(string type, string id, string field = "")
        {
            string returnRecordLink = "/{0}/Edit.aspx?id="+id;
            switch(type)
            {
                case "structural_analyses":

                    if (!string.IsNullOrEmpty(field) && field.ToLower().Contains("comment"))
                    {
                        // is comment
                        var saComment = StructuralAnalysis.SearchComment(int.Parse(id));
                        var leaseAppIdFromComment = StructuralAnalysis.Search(saComment.structural_analysis_id.Value);
                        returnRecordLink = "/LeaseApplications/Edit.aspx?id=" + leaseAppIdFromComment.lease_application_id;
                    }
                    else
                    {
                        var sa = StructuralAnalysis.Search(int.Parse(id));
                        var leaseAppIdFromSa = sa.lease_application_id;
                        returnRecordLink = "/LeaseApplications/Edit.aspx?id=" + leaseAppIdFromSa;
                    }
                break;
                case "site_data_packages":
                    returnRecordLink = string.Format(returnRecordLink, "SiteDataPackages");
                break;
                case "lease_applications":
                    if (!string.IsNullOrEmpty(field) && field.ToLower().Contains("comment"))
                    {
                        // is comment
                        var leComment = LeaseAppComment.Search(int.Parse(id));
                        var leaseAppIdFromComment = leComment.lease_application_id;
                        returnRecordLink = "/LeaseApplications/Edit.aspx?id=" + leaseAppIdFromComment;
                    }
                    else
                    {
                        returnRecordLink = string.Format(returnRecordLink, "LeaseApplications");
                    }
                break;
                case "leaseapp_revision":
                    var leaseAppRev = LeaseAppRevisions.Search(int.Parse(id));
                    var leaseAppIdFromRev = leaseAppRev.lease_application_id;
                    returnRecordLink = "/LeaseApplications/Edit.aspx?id=" + leaseAppIdFromRev;
                break;
                case "sites":
                    returnRecordLink = "/Sites/SiteDashboard.aspx?id="+id; 
                break;
                case "issue_tracker":
                    if (!string.IsNullOrEmpty(field) && field.ToLower().Contains("issue_action"))
                    {
                        // is issue_action_items
                        var issueAction = IssueTracker.SearchIssueAction(int.Parse(id));
                        var issueIdFromAction = IssueTracker.Search(issueAction.issue_tracker_id.Value);
                        returnRecordLink = "/IssueTracker/Edit.aspx?id=" + issueIdFromAction.id;
                    }
                    else
                    {
                        returnRecordLink = string.Format(returnRecordLink, "IssueTracker"); 
                    }
                break;
                case "tower_modification":
                    if (!string.IsNullOrEmpty(field) && field.ToLower().Contains("tmo_comments"))
                    {
                        // is comment
                        var modComment = TowerModComment.Search(int.Parse(id));
                        var towerModIdFromComment = modComment.tower_modification_id;
                        returnRecordLink = "/TowerModifications/Edit.aspx?id=" + towerModIdFromComment;
                    }
                    else
                    {
                        // is tmo po
                        var modPo = TowerModPhase.Search(int.Parse(id));
                        var towerModIdFromModPo = modPo.tower_modification_id;
                        returnRecordLink = "/TowerModifications/Edit.aspx?id=" + towerModIdFromModPo;
                    }
                break;
                case "tower_modification_general":
                    returnRecordLink = string.Format(returnRecordLink, "TowerModifications");
                break;
                case "lease_admin":
                    returnRecordLink = string.Format(returnRecordLink, "LeaseAdminTracker");
                break;
            }

            return returnRecordLink;
        }

        public enum eDocumentLogAction
        {
            Upload,
            Edit,
            Link
        }

        public static IQueryable<documents_changelog> SearchWithinTransaction(int link_record_id, string link_record_type, CPTTEntities ceRef)
        {
            IQueryable<documents_changelog> ret = null;

            ret = from log in ceRef.documents_changelog 
                  where log.link_record_id == link_record_id
                  && log.link_record_type == link_record_type
                  select log;

            return ret;
        }
        public static IQueryable<documents_changelog> SearchWithinTransaction(string old_ref_key, CPTTEntities ceRef)
        {
            IQueryable<documents_changelog> ret = null;

            ret = from log in ceRef.documents_changelog
                  where log.ref_key == old_ref_key
                  select log;

            return ret;
        }

        public static List<documents_changelog> GetDocumentsChangelog()
        {
            using (CPTTEntities ce = new CPTTEntities())
            {
                var recentOfTime = DateTime.Now.AddHours(-RecentUploadCutoffTime);
                var log = (from obj in ce.documents_changelog
                           where obj.created_at >= recentOfTime
                           select obj).ToList();

                return log;
            }
        }

        public static string Add(documents_changelog log, CPTTEntities ceRef = null)
        {
            String results = null;
            bool isSuccess = false;
            bool isRootTransaction = true;

            CPTTEntities ce;
            if (ceRef == null)
            {
                //using new entity object
                ce = new CPTTEntities();
            }
            else
            {
                //using current entity object
                ce = ceRef;
                isRootTransaction = false;
            }

            try
            {
                if (isRootTransaction)
                {
                    using (TransactionScope trans = TransactionUtils.CreateTransactionScope())
                    {
                        ce.Connection.Open();
                        ce.documents_changelog.AddObject(log);
                        ce.SaveChanges();
                        isSuccess = true;
                        trans.Complete();     // Transaction complete
                    }
                }
                else
                {
                    ce.documents_changelog.AddObject(log);
                }
            }
            catch (OptimisticConcurrencyException oex)
            {
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(oex);

                results = "The current record has been modifed since you have " +
                          "last retrieved it. Please reload the record and " +
                          "attempt to save it again.";
            }
            catch (Exception ex)
            {
                // Error occurs
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                isSuccess = false;
                results = "An error occurred saving the record. Please try again.";
            }
            finally
            {
                if (isRootTransaction)
                {
                    ce.Connection.Dispose();
                }
            }

            // Finally accept all changes from the transaction.
            if (isSuccess && isRootTransaction)
            {
                ce.Dispose();
            }

            return results;
        }

        public static string AddLog(DmsLinkDocumentStructure document, string siteID, string fileName, string fileKey, eDocumentLogAction action, HttpRequest httpRequest, bool isLinkFiled = false, bool isFromUploadPage = false, CPTTEntities ceRef = null)
        {
            string returnResult = string.Empty;

            documents_changelog log = new documents_changelog();
            string currentIp = GetUserIP(httpRequest);

            log.site_uid = siteID;
            log.document_name = fileName;
            log.action = action.ToString();
            log.creator_ip = currentIp;
            string encodeKey = HttpUtility.UrlEncode(fileKey);
            log.ref_key = encodeKey;

            log.is_link_field = isLinkFiled;

            string recordType = document.Type.ToString();
            string recordId = httpRequest.QueryString["typeID"];
            string field = document.Field.ToString();
            log.link_record = GenarateRecordLink(recordType, recordId, field);

            if (isLinkFiled)
            {
                log.link_record_type = document.Type.ToString();
                log.link_record_id = int.Parse(recordId);
                log.link_record_field = field;
                if (!isFromUploadPage && action != eDocumentLogAction.Link)
                {
                    log.ref_key = fileKey;
                    string Id = httpRequest.QueryString["id"];
                    log.link_record = httpRequest.Path + "?id=" + Id;
                }
            }

            returnResult = Add(log, ceRef);
            return returnResult;
        }

        public static string AddLog(documents_links document, string siteID, string fileName, string fileKey, eDocumentLogAction action, HttpRequest httpRequest, bool isLinkFiled = false, bool isFromUploadPage = false, CPTTEntities ceRef = null)
        {
            string returnResult = string.Empty;

            documents_changelog log = new documents_changelog();
            string currentIp = GetUserIP(httpRequest);

            log.site_uid = siteID;
            log.document_name = fileName;
            log.action = action.ToString();
            log.creator_ip = currentIp;
            string encodeKey = HttpUtility.UrlEncode(fileKey);
            log.ref_key = encodeKey;

            log.is_link_field = isLinkFiled;

            string recordType = document.record_type;
            string recordId = document.record_id.ToString();
            string field = document.linked_field;
            log.link_record = GenarateRecordLink(recordType, recordId, field);

            if (isLinkFiled)
            {
                log.link_record_type = document.record_type;
                log.link_record_id = document.record_id;
                log.link_record_field = field;
                if (!isFromUploadPage)
                {
                    log.ref_key = fileKey;
                    string Id = httpRequest.QueryString["id"];
                    log.link_record = httpRequest.Path + "?id=" + Id;
                }
            }

            returnResult = Add(log, ceRef);
            return returnResult;
        }

        public static string AddLog(string siteID, string fileName, string fileKey, eDocumentLogAction action, HttpRequest httpRequest, bool isLinkFiled = false, bool isFromUploadPage = false, CPTTEntities ceRef = null)
        {
            string returnResult = string.Empty;

            documents_changelog log = new documents_changelog();
            string currentIp = GetUserIP(httpRequest);

            log.site_uid = siteID;
            log.document_name = fileName;
            log.action = action.ToString();
            log.creator_ip = currentIp;
            string encodeKey = HttpUtility.UrlEncode(fileKey);
            log.ref_key = encodeKey;

            log.is_link_field = isLinkFiled;

            if (isLinkFiled)
            {
                string recordType = httpRequest.QueryString["type"];
                string recordId = httpRequest.QueryString["typeID"];
                string field = httpRequest.QueryString["field"];
                log.link_record = GenarateRecordLink(recordType, recordId, field);
                log.link_record_type = recordType;
                log.link_record_id = int.Parse(recordId);
                log.link_record_field = field;

                if (!isFromUploadPage)
                {
                    log.ref_key = fileKey;
                    string Id = httpRequest.QueryString["id"];
                    log.link_record = httpRequest.Path + "?id=" + Id;
                }
            }

            returnResult = Add(log, ceRef);
            return returnResult;
        }

        public static string AddLogOnCreateNewItem(Dms_Document doc, string siteID, string recirdId, eDocumentLogAction action, HttpRequest httpRequest, bool isLinkFiled = false, bool isFromUploadPage = false, CPTTEntities ceRef = null)
        {
            string returnResult = string.Empty;

            documents_changelog log = new documents_changelog();
            string currentIp = GetUserIP(httpRequest);

            log.site_uid = siteID;
            log.document_name = doc.documentName;
            log.action = action.ToString();
            log.creator_ip = currentIp;
            string encodeKey = HttpUtility.UrlEncode(doc.ref_key);
            log.ref_key = encodeKey;

            log.is_link_field = isLinkFiled;

            if (isLinkFiled)
            {
                string recordType = recordType = doc.type;
                string recordId = recirdId;
                string field = doc.ref_field;
                log.link_record = GenarateRecordLink(recordType, recordId, field);
                log.link_record_type = recordType;
                log.link_record_id = int.Parse(recordId);
                log.link_record_field = field;
            }

            returnResult = Add(log, ceRef);
            return returnResult;
        }

        public static string UpdateIsLinkRecordFlag(int link_record_id, string link_record_type, bool newValue, CPTTEntities ceRef, string link_record_field = "")
        {
            string returnResult = string.Empty;
           
            try
            {
                IQueryable<documents_changelog> log = DocumentsChangeLog.SearchWithinTransaction(link_record_id, link_record_type, ceRef);

                if (log != null)
                {
                    if (!string.IsNullOrEmpty(link_record_field))
                    {
                        log = from iQueryLog in log where iQueryLog.link_record_field == link_record_field select iQueryLog;
                    }

                    foreach (var item in log)
                    {
                        item.is_link_field = newValue;
                    }
                }
            }
            catch (Exception ex)
            {
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                returnResult = ex.ToString();
            }

            return returnResult;
        }

        public static string RemoveRefKeyOnRenameEvent(string fileKey, CPTTEntities ceRef)
        {
            string returnResult = string.Empty;

            try
            {
                IQueryable<documents_changelog> log = DocumentsChangeLog.SearchWithinTransaction(fileKey, ceRef);

                if (log != null)
                {
                    foreach (var item in log)
                    {
                        item.ref_key = string.Empty;
                        item.is_link_field = false;
                    }
                }
            }
            catch (Exception ex)
            {
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                returnResult = ex.ToString();
            }

            return returnResult;
        }
        
        private static string GetUserIP(HttpRequest httpRequest)
        {
            string ipList = httpRequest.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipList))
            {
                return ipList.Split(',')[0];
            }

            return httpRequest.ServerVariables["REMOTE_ADDR"];
        }
    }
}