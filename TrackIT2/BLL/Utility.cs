﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Reflection;
using System.Web.UI.WebControls;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Net;
using System.Text;
using System.IO;
using System.Web.Security;
using TrackIT2.DAL;
using System.Data.Metadata.Edm;
using System.Collections;
using TrackIT2.Objects;

namespace TrackIT2.BLL
{
    /// <summary>
    /// </summary>
	public static class Utility
	{

        #region Reflection / Properties

        public static EntitySet GetEntitySetFromName(CPTTEntities db, string propertyName)
        {
           PropertyInfo property = null;
           property = typeof(CPTTEntities)
                      .GetProperty(propertyName, BindingFlags.Instance | BindingFlags.Public);
           return (EntitySet)property.GetValue(db, null);
        }

            /// <summary>
            ///   Returns the property object associated with the entity and field
            ///   specified.
            /// </summary>
            /// <typeparam name="T">Entity to retrieve field from.</typeparam>
            /// <param name="field">Name of field.</param>
            /// <returns>String containing entity field.</returns>
            /// <remarks>
            ///   This function is used to provide dynamic sorting options.
            /// </remarks>
            public static Func<T, string> GetField<T>(string field)
            {
                PropertyInfo propertyInfo = typeof(T).GetProperty(field);
                return obj => Convert.ToString(propertyInfo.GetValue(obj, null));
            }

            /// <summary>
            ///   Returns the property object associated with the entity and field
            ///   specified.
            /// </summary>
            /// <typeparam name="T">Entity to retrieve field from.</typeparam>
            /// <param name="field">Name of field.</param>
            /// <returns>String containing entity field.</returns>
            /// <remarks>
            ///   This function is used to provide dynamic sorting options.
            /// </remarks>
            public static Func<T, int> GetIntField<T>(string field)
            {
                PropertyInfo propertyInfo = typeof(T).GetProperty(field);
                return obj => Convert.ToInt32(propertyInfo.GetValue(obj, null));
            }

            /// <summary>
            ///   Returns the property object associated with the entity and field
            ///   specified.
            /// </summary>
            /// <typeparam name="T">Entity to retrieve field from.</typeparam>
            /// <param name="field">Name of field.</param>
            /// <returns>String containing entity field.</returns>
            /// <remarks>
            ///   This function is used to provide dynamic sorting options.
            /// </remarks>
            public static Func<T, DateTime> GetDateField<T>(string field)
            {
                PropertyInfo propertyInfo = typeof(T).GetProperty(field);
                return obj => Convert.ToDateTime(propertyInfo.GetValue(obj, null));
            }

            /// <summary>
            /// Returns the PropertyInfo object associated with the field.
            /// </summary>
            /// <typeparam name="T">Entity to retrieve field from.</typeparam>
            /// <param name="field">Name of field.</param>
            /// <returns>PropertyInfo Object</returns>
            public static PropertyInfo GetFieldType<T>(string field)
            {
                PropertyInfo propertyInfo = typeof(T).GetProperty(field);
                return propertyInfo;
            }

        #endregion
        
        #region Control Binding

            /// <summary>
            /// Binds a List Control to a data source.
            /// </summary>
            /// <param name="ddl">Dynamic Web List Control</param>
            /// <param name="src">Data Source</param>
            /// <param name="strValueField">String Value Field</param>
            /// <param name="strTextField">String Text Field</param>
            /// <param name="strEmptyText">String Empty Text (default)</param>
            public static void BindDDLDataSource(dynamic ddl, object src, string strValueField, string strTextField, string strEmptyText)
            {
                ddl.Items.Clear();

                if (!string.IsNullOrEmpty(strEmptyText))
                {
                    //Insert Empty Value Item
                    ListItem item = new ListItem(strEmptyText, string.Empty);
                    ddl.Items.Add(item);

                    ddl.AppendDataBoundItems = true;
                }
                
                ddl.DataValueField = strValueField;
                ddl.DataTextField = strTextField;
                ddl.DataSource = src;
                ddl.DataBind();
            }

            /// <summary>
            /// Binding value to UIControl
            /// </summary>
            /// <param name="ctrlRef">Control to be binded</param>
            /// <param name="dymObjRef">Object to bind</param>
            /// <param name="sBlankText">[Text to show when object is blank](default = "")</param>
            public static void ControlValueSetter(dynamic ctrlRef, dynamic dymObjRef, String sBlankText = "")
            {
                try
                {
                    //Checking for null value from Nullable Object Type.
                    if (dymObjRef != null)
                    {
                        if (ctrlRef.GetType().Name.ToString() == "DropDownList")
                        {
                            //Temporary checking for input type and binding to dropdownlist selectedvalue.
                            if (dymObjRef.GetType().Name.ToString() == "Boolean")
                            {
                                ctrlRef.SelectedValue = (dymObjRef ? "yes" : "no");
                            }
                            else
                            {
                                //If target control is dropdownlist so set selectedvalue of control.
                                ctrlRef.SelectedValue = Utility.PrepareInt(dymObjRef.ToString()).ToString();
                            }

                        }
                        else
                        {
                            if (dymObjRef.GetType().Name.ToString().Contains("DateTime"))
                            {
                                ctrlRef.Text = Utility.DateToString(dymObjRef);
                            }
                            else if (dymObjRef.GetType().Name.ToString() == "Boolean")
                            {
                                ctrlRef.Text = (dymObjRef ? "Yes" : "No");
                            }
                            else if (dymObjRef.GetType().Name.ToString() == "Decimal" && ctrlRef.ID.ToUpper().Contains("AMOUNT"))
                            {
                                ctrlRef.Text = dymObjRef.ToString(".00");
                            }
                            else
                            {
                                ctrlRef.Text = !string.IsNullOrEmpty(dymObjRef.ToString()) ? dymObjRef.ToString() : sBlankText;
                            }
                        }
                    }
                    else
                    {
                        //Set the text for non-DropDownList controls.
                        if (ctrlRef.GetType().Name.ToString() != "DropDownList")
                        {
                            ctrlRef.Text = sBlankText;
                        }
                        //Do nothing for DropDownList.
                    }
                }
                catch (Exception)   //Checking for invalid operation for dynamic type will cause an exception at runtime.
                {
                    throw;
                }
            }

            public static void SettingErrorColorForTextbox(TextBox txtRef)
            {
                txtRef.Style.Add("background-color", "#FFBABA");
            }
            public static void ClearErrorColorForTextbox(TextBox txtRef)
            {
                txtRef.Style.Add("background-color", "#FFFFFF");
            }
        #endregion

		#region Prepare Data
            //Extension method for EntityObject to check max length by property name.
            //Can't use for MAX length declared varchar.
            public static int? GetMaxLength(this EntityObject entity, string sPropertyName)
            {
                int? result = null;
                using (CPTTEntities ce = new CPTTEntities())
                {
                    var q = from meta in ce.MetadataWorkspace.GetItems(DataSpace.CSpace)
                                       .Where(m => m.BuiltInTypeKind == BuiltInTypeKind.EntityType)
                            from p in (meta as EntityType).Properties
                               .Where(p => p.DeclaringType.Name == entity.GetType().Name
                                   && p.Name == sPropertyName
                                   && p.TypeUsage.EdmType.Name == "String")
                            select p;

                    var queryResult = from meta in ce.MetadataWorkspace.GetItems(DataSpace.CSpace)
                                       .Where(m => m.BuiltInTypeKind == BuiltInTypeKind.EntityType)
                                      from p in (meta as EntityType).Properties
                                         .Where(p => p.DeclaringType.Name == entity.GetType().Name
                                             && p.Name == sPropertyName
                                             && p.TypeUsage.EdmType.Name == "String")
                                      select p.TypeUsage.Facets["MaxLength"].Value;
                    if (queryResult.Count() > 0)
                    {
                        result = Convert.ToInt32(queryResult.First());
                    }
                }
                return result;
            }
            //Checking max length of current varchar field.
            public static int? ValidateVarCharField(EntityObject objEntity, string sPropertyName, string sValue)
            {
                int? iResult = objEntity.GetMaxLength(sPropertyName);
                if (sValue == null || (iResult.HasValue  &&iResult.Value >= sValue.ToCharArray().Count()))
                {
                    iResult = null;
                }
                return iResult;
            }
            #region Validate Varchar Fields
            public static bool ValidateVarchar(string sVarCharRef, EntityObject objEntity, Hashtable htRef, string sPropertyName, string sLabelName)
            {
                int? iVarLength = Utility.ValidateVarCharField(objEntity, sPropertyName, sVarCharRef);
                bool isValid = false;
                if (!iVarLength.HasValue)
                {
                    isValid = true;
                }
                else
                {
                    isValid = false;
                    SetInvalidText(htRef, sLabelName, iVarLength.Value);
                }
                return isValid;
            }
            public static void SetInvalidText(Hashtable htRef, string sField, int iLength)
            {
                if (htRef[sField] == null)
                {
                    htRef.Add(sField, iLength);
                }
                else
                {
                    htRef[sField] = iLength;
                }
            }


            #endregion
            
            
        public static string PrepareString(string str)
		{
         string ret = null;

         // Any data (including nulls) are acceptable for strings. However,
         // empty strings are reformatted as null values for insertion to
         // the database.
         if (!string.IsNullOrEmpty(str))
			{
            ret = str.Trim();
            SetSubmitValidator(true);
			}
         else
         {
            ret = null;
            SetSubmitValidator(true);
         }

         return ret;
		}

      public static int? PrepareInt(string str)
      {
         int? ret = null;
         int retTest;

         if (str != null && str != string.Empty)
         {
            if (int.TryParse(str, out retTest))
            {
               ret = retTest;
               SetSubmitValidator(true);
            }
            else
            {
               SetSubmitValidator(false);
            }
         }

         return ret;
      }

		public static decimal? PrepareDecimal(string str)
		{
         decimal? ret = null;
         decimal retTest;

         if (str != null && str.Contains("$"))
         {
            str = str.Replace("$", "");
         }

         if (str != null && str != string.Empty && str != "$")
         {
            if (decimal.TryParse(str, out retTest))
            {
               ret = retTest;
               SetSubmitValidator(true);
            }
            else
            {
               SetSubmitValidator(false);
            }
         }
                 
         return ret;
		}

		public static DateTime? PrepareDate(string str)
		{
         DateTime? ret = null;
         DateTime retTest;

         if (str != null && str != string.Empty)
         {
            if (DateTime.TryParse(str, out retTest))
            {
               ret = retTest;

               // Since this date is going into a SQL field, we must verify
               // that the year is between 1753 and 9999 or the database will
               // throw an exception.
               if (ret.Value.Year >= 1753 && ret.Value.Year <= 9999)
               {
                  SetSubmitValidator(true);
               }
               else
               {
                  SetSubmitValidator(false);
               }               
            }
            else
            {
               SetSubmitValidator(false);
            }
         }
         
         return ret;
		}
        
      public static Double? PrepareDouble(string str)
      {
         Double? ret = null;
         Double retTest;

         if (str != null && str != string.Empty)
         {
            if (Double.TryParse(str, out retTest))
            {
               ret = retTest;
               SetSubmitValidator(true);
            }
            else
            {
               SetSubmitValidator(false);
            }
         }         
                
         return ret;
      }

        public static Boolean? PrepareBoolean(string str)
        {
           Boolean? ret = null;
           Boolean retTest;

           if (str != null && str != string.Empty)
           {
              if (Boolean.TryParse(str, out retTest))
              {
                 ret = retTest;
                 SetSubmitValidator(true);
              }
              else
              {
                 SetSubmitValidator(false);
              }
           }
           
           return ret;
        }

        public static Boolean? ConvertYNToBoolean(String sFlag)
        {
            Boolean? blnResult = null;
            if (sFlag != null && sFlag != "")
            {
                if (sFlag.ToLower() == "yes")
                {
                    blnResult = true;
                }
                else if (sFlag.ToLower() == "no")
                {
                    blnResult = false;
                }
            }
            return blnResult;
        }

        public static string PrepareSqlSearchString(string str)
        {
            string ret = null;

            // Any data are acceptable,
            // But ' and " mustbe remove from string before select data from database.
            if (str.Contains("'") || str.Contains("\""))
            {
                ret = str.Replace("'", "");
                ret = ret.Replace("\"", "");
                ret = Uri.UnescapeDataString(ret);
                SetSubmitValidator(true);
            }
            else
            {
                ret = Uri.UnescapeDataString(str);
            }

            return ret;
        }

        public static string PrepareDMSBasePath(string str)
        {
            string ret = str;

            if (ret[0] == '/')
            {
                ret = ret.Substring(1, ret.Length - 1);
            }

            if (str.Length > 1)
            {
                if (str[str.Length - 1] == '/')
                {
                    ret = ret.Substring(0, ret.Length - 1);
                }
            }

            return ret;
        }
        #region Submit Validator Accessor
        /// <summary>
        /// Set submit validator flag from prepareing data operations.
        /// </summary>
        /// <param name="blnIsValid">Is data valid? flag.</param>
        public static void SetSubmitValidator(Boolean blnIsValid)
        {
            Boolean blnCurrent;
            Boolean? blnResult = null;
            
            if (HttpContext.Current.Session[Globals.SESSION_SUBMIT_VALIDATE] != null)
            {
                if (Boolean.TryParse(HttpContext.Current.Session[Globals.SESSION_SUBMIT_VALIDATE].ToString(), out blnCurrent))
                {
                    blnResult = blnCurrent && blnIsValid;
                }
            }
            else
            {
                blnResult = blnIsValid;
            }
            HttpContext.Current.Session[Globals.SESSION_SUBMIT_VALIDATE] = blnResult;
        }
        /// <summary>
        /// Use for clear submit validator flag when messagebox is finish its operation.
        /// </summary>
        public static void ClearSubmitValidator()
        {
            HttpContext.Current.Session[Globals.SESSION_SUBMIT_VALIDATE] = null;
        }

        /// <summary>
        /// Get status of submit validator flag from prepare data operations.
        /// </summary>
        /// <returns>Nullable boolean flag.</returns>
        public static Boolean? GetSubmitValidator()
        {
            return HttpContext.Current.Session[Globals.SESSION_SUBMIT_VALIDATE] as Boolean?;
        }
        #endregion
        #endregion

        #region Date / Time

        public static string DateToString(DateTime? dt)
        {
           return dt.HasValue ? dt.Value.ToString(Globals.SHORT_DATE_FORMAT) : string.Empty;
        }

       public static string DateTimeToString(DateTime? dt)
        {
           return dt.HasValue ? dt.Value.ToString(Globals.SHORT_DATETIME_FORMAT) : string.Empty;
        }

       #endregion

		#region Context Extention

		/// <summary>
		/// Extention for updating context with detached entity
		/// </summary>
		/// <param name="objContext"></param>
		/// <param name="objDetached">Detached Entity</param>
		public static void AttachUpdated(this ObjectContext objContext, EntityObject objDetached)
		{
			if (objDetached.EntityState == EntityState.Detached)
			{
				object original = null;
				if (objContext.TryGetObjectByKey(objDetached.EntityKey, out original))
				{
					objContext.ApplyCurrentValues(objDetached.EntityKey.EntitySetName, objDetached);
				}
				else
				{
					throw new ObjectNotFoundException();
				}
			}
		}
		
		#endregion

        #region Dynamic Sorting (Alt for Flexigrid)

            /// <summary>
            /// OrderBy - Sorts the elements of a sequence in Ascending order according to the property expression.
            /// </summary>
            /// <typeparam name="T">Entity / Type for Data Source.</typeparam>
            /// <param name="source">Data Source imput for sorting.</param>
            /// <param name="property">Property to sort the Data Source by.</param>
            /// <returns></returns>
            public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> source, string property)
            {
                return ApplyOrder<T>(source, property, "OrderBy");
            }

            /// <summary>
            /// OrderByDescending - Sorts the elements of a sequence in Descending order according to the property expression.
            /// </summary>
            /// <typeparam name="T">Entity / Type for Data Source.</typeparam>
            /// <param name="source">Data Source imput for sorting.</param>
            /// <param name="property">Property to sort the Data Source by.</param>
            /// <returns></returns>
            public static IOrderedQueryable<T> OrderByDescending<T>(this IQueryable<T> source, string property)
            {
                return ApplyOrder<T>(source, property, "OrderByDescending");
            }

            /// <summary>
            /// ThenBy - Secondary / additional sorting of a sequence in Ascending order according to the property expression.
            /// </summary>
            /// <typeparam name="T">Entity / Type for Data Source.</typeparam>
            /// <param name="source">Data Source imput for sorting.</param>
            /// <param name="property">Property to sort the Data Source by.</param>
            /// <returns></returns>
            public static IOrderedQueryable<T> ThenBy<T>(this IOrderedQueryable<T> source, string property)
            {
                return ApplyOrder<T>(source, property, "ThenBy");
            }

            /// <summary>
            /// ThenByDescending - Secondary / additional sorting of a sequence in Descending order according to the property expression.
            /// </summary>
            /// <typeparam name="T">Entity / Type for Data Source.</typeparam>
            /// <param name="source">Data Source imput for sorting.</param>
            /// <param name="property">Property to sort the Data Source by.</param>
            /// <returns></returns>
            public static IOrderedQueryable<T> ThenByDescending<T>(this IOrderedQueryable<T> source, string property)
            {
                return ApplyOrder<T>(source, property, "ThenByDescending");
            }

            /// <summary>
            /// ApplyOrder - The main ordering method which sorts a sequence according to the property Method Name argument.
            /// </summary>
            /// <typeparam name="T">Entity / Type for Data Source.</typeparam>
            /// <param name="source">Data Source imput for sorting.</param>
            /// <param name="property">Property to sort the Data Source by.</param>
            /// <param name="methodName">Method Name for the type of sorting required (OrderBy, OrderByDescending, ThenBy, ThenByDescending).</param>
            /// <returns></returns>
            static IOrderedQueryable<T> ApplyOrder<T>(IQueryable<T> source, string property, string methodName)
            {
                string[] props = property.Split('.');
                Type type = typeof(T);
                ParameterExpression arg = Expression.Parameter(type, "x");
                Expression expr = arg;

                foreach (string prop in props)
                {
                    // use reflection (not ComponentModel) to mirror LINQ
                    PropertyInfo pi = type.GetProperty(prop);
                    expr = Expression.Property(expr, pi);
                    type = pi.PropertyType;
                }

                Type delegateType = typeof(Func<,>).MakeGenericType(typeof(T), type);
                LambdaExpression lambda = Expression.Lambda(delegateType, expr, arg);

                object result = typeof(Queryable).GetMethods().Single(
                        method => method.Name == methodName
                                && method.IsGenericMethodDefinition
                                && method.GetGenericArguments().Length == 2
                                && method.GetParameters().Length == 2)
                        .MakeGenericMethod(typeof(T), type)
                        .Invoke(null, new object[] { source, lambda });

                return (IOrderedQueryable<T>)result;
            }

        #endregion

        #region Google Map
        public static string GetGoogleMapLink(string sQueryAddress)
        {
            return @"https://maps.google.com/maps?q=" + HttpUtility.UrlEncode(sQueryAddress);
        }
        #endregion

        #region Google Map (Latitude, Longitude)
        public static string GetGooleMapLatLong(string latitude, string longitude) { 
            return @"https://maps.google.com/maps?vpsrc=0&t=h&z=16&q=" + HttpUtility.UrlEncode(latitude+", "+longitude);
        }

        public static string ConvertLatLongCoorToDegree(string latlong)
        {
            string sResult = null;
            decimal dLatLong;
            if (decimal.TryParse(latlong, out dLatLong))
            {
                sResult = ConvertLatLongCoorToDegree(dLatLong);
            }
            return sResult;
        }
        public static string ConvertLatLongCoorToDegree(decimal? latlong)
        {
            string sResult = null;
            string LATLONG_FORMAT = "{0}{1}° {2}' {3}\""; // 0 = sign, 1 = degree, 2 = min, 3 = sec
            int sign;
            int degree, min;
            decimal sec;
            decimal latLongValue;
            if (latlong.HasValue)
            {
                latLongValue = latlong.Value;
                try
                {
                    sign = Math.Sign(latLongValue);
                    latLongValue = Math.Abs(latLongValue);
                    degree = int.Parse(Math.Floor(latLongValue).ToString());
                    latLongValue = (latLongValue - degree) * 60;
                    min = int.Parse(Math.Floor(latLongValue).ToString());
                    latLongValue = (latLongValue - min) * 60;
                    sec = latLongValue;
                    sResult = String.Format(LATLONG_FORMAT, (sign < 0 ? "-" : ""), degree.ToString(), min.ToString(), sec.ToString("0.00"));
                }
                catch (Exception)
                {
                    //Do nothing.
                }
            }
            return sResult;

        }
        #endregion

        #region Image Manipulation
        public static System.Drawing.Image ResizeImage (System.Drawing.Image sourceImage, int newHeight, int newWidth)
      {
         System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(newWidth, newHeight, sourceImage.PixelFormat);

         if (bitmap.PixelFormat == System.Drawing.Imaging.PixelFormat.Format1bppIndexed ||
             bitmap.PixelFormat == System.Drawing.Imaging.PixelFormat.Format4bppIndexed ||
             bitmap.PixelFormat == System.Drawing.Imaging.PixelFormat.Format8bppIndexed ||
             bitmap.PixelFormat == System.Drawing.Imaging.PixelFormat.Undefined ||
             bitmap.PixelFormat == System.Drawing.Imaging.PixelFormat.DontCare ||
             bitmap.PixelFormat == System.Drawing.Imaging.PixelFormat.Format16bppArgb1555 ||
             bitmap.PixelFormat == System.Drawing.Imaging.PixelFormat.Format16bppGrayScale)
         {
            throw new NotSupportedException("Pixel format of the image is not supported.");
         }

         System.Drawing.Graphics graphicsImage = System.Drawing.Graphics.FromImage(bitmap);

         graphicsImage.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
         graphicsImage.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
         graphicsImage.DrawImage(sourceImage, 0, 0, bitmap.Width, bitmap.Height);
         graphicsImage.Dispose();
      
         return  bitmap;
      }
      #endregion

        #region HTTP Web Request / Response

            /// <summary>
            /// Base Site URL
            /// </summary>
            public static string BaseSiteUrl
            {
                get
                {

                    HttpContext context = HttpContext.Current;
                    Uri uri = new Uri(context.Request.Url.AbsoluteUri);
                    var baseUrl = string.Format("{0}://{1}{2}{3}",
                                                uri.Scheme,
                                                    Globals.HOST_NAME,
                                                        uri.Port == 80
                                                            ? string.Empty
                                                                    : ":" + uri.Port,
                                                                                context.Request.ApplicationPath);

                    if (!baseUrl.EndsWith("/")) baseUrl += "/";

                    return baseUrl;
                }
            }
            //-->

            /// <summary>
            /// Domain
            /// </summary>
            public static string Domain
            {
                get
                {
                    HttpContext context = HttpContext.Current;
                    string domain = context.Request.Url.Host;                    
                    return Globals.HOST_NAME;
                }
            }
            //-->

            /// <summary>
            /// Web Request - POST Or GET
            /// </summary>
            /// <param name="postDataString"></param>
            /// <returns>HttpWebResponse</returns>
            public static HttpWebResponse WebRequestPostOrGet(HttpContext context, string url, string contentType, string postDataString, string requestMethod)
            {

                // Initialise
                if (url.IndexOf("http://") < 0) url = Utility.BaseSiteUrl + url;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

                // Request Method
                request.Method = requestMethod;

                // Cookies / Session
                HttpCookie cookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
                Cookie authenticationCookie = new Cookie(FormsAuthentication.FormsCookieName, cookie.Value, cookie.Path, Utility.Domain);
                request.CookieContainer = new CookieContainer();
                request.CookieContainer.Add(authenticationCookie);

                // POST Request Data?
                if (request.Method.Equals(Globals.REQUEST_METHOD_POST))
                {

                    // Content Type
                    request.ContentType = contentType;

                    // Request Data
                    byte[] bytes = null;
                    bytes = Encoding.UTF8.GetBytes(postDataString);
                    request.ContentLength = bytes.Length;

                    // Action the Request
                    Stream requestStream = request.GetRequestStream();

                    // POST Request Data?
                    if (request.Method.Equals(Globals.REQUEST_METHOD_POST))
                    {
                        requestStream.Write(bytes, 0, bytes.Length);
                    }

                    // Finish Request
                    requestStream.Close();
                }
                //>

                // get response
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                return response;

            }
            //-->

            /// <summary>
            /// Get Data From Web Response
            /// </summary>
            /// <param name="httpResponse">HttpWebResponse</param>
            /// <returns>Response String</returns>
            public static string GetDataFromWebResponse(HttpWebResponse httpResponse)
            {

                // process response
                string rawResponse = null;

                if (httpResponse.StatusCode == HttpStatusCode.OK)
                {

                    Stream responseStream = httpResponse.GetResponseStream();
                    StreamReader stream = new StreamReader(responseStream);
                    rawResponse = stream.ReadToEnd();

                    responseStream.Close();
                    responseStream = null;
                    stream = null;
                }

                httpResponse.Close();
                httpResponse = null;

                return rawResponse;

            }
            //--->

        #endregion

        #region Check User Roles for external tab

            public static bool ExternalButtonSaDelete()
            {
                string[] roles = { "Administrator", "Administrator - Tower Mod & SA" };
                foreach (string s in roles)
                {
                    if (Security.UserIsInRole(s))
                    {
                        return true;
                    }
                }
                return false;
            }

            public static bool ExternalLabelSDP()
            {
                string[] roles = { "Administrator", "Application Coordinator" };
                foreach (string s in roles)
                {
                    if (Security.UserIsInRole(s))
                    {
                        return true;
                    }
                }
                return false;
            }

            public static bool ExternalLabelTowerMod()
            {
                string[] roles = { "Administrator", "Senior PM", "Divisional Manager",
                                    "Construction PM","Tower Mod PM"};

                foreach (string s in roles)
                {
                    if (Security.UserIsInRole(s))
                    {
                        return true;
                    }
                }
                return false;
            }

        #endregion
    
      public static ObjectQuery<T> BuildObjectQuery<T>
             (ObjectQuery<T> query, NameValueCollection parameters,
              Dictionary<string, SearchParameter> searchCriteria)
      {
         var results = query;
         
         // If there are parameters in our collection, build a where clause
         // using the search criteria dictionary. Otherwise the method ends
         // and the query remains the same.
         if (parameters.Count > 0)
         {
            var whereClause = new StringBuilder();        
            var isFirstParameter = true;

            // Before processing parameters, check for from/to pairings for  
            // range queries. If a pairing exists, replace with the 
            // appropriate range key.
            var keys = parameters.AllKeys.ToList();

            foreach (var key in keys)
            {
               // Since parameters can be removed midloop, check for value
               // before processing
               if (!parameters.AllKeys.Contains(key)) continue;

               // Test for from and to index values for simplified processing
               var indexFrom = key.IndexOf("From", StringComparison.Ordinal);
               var indexTo = key.IndexOf("To", StringComparison.Ordinal);

               // Skip key if it is not a from/to parameter.
               if (indexFrom <= 0 && indexTo <= 0) continue;

               // Process parameter based on what name was found (from/to).
               var keyPrefix = indexFrom > 0 ? key.Substring(0, indexFrom) : key.Substring(0, indexTo);
               var fromKey = keyPrefix + "From";
               var toKey = keyPrefix + "To";

               // Make sure that there is a From and a To key to process.
               if (!parameters.AllKeys.Contains(fromKey) || !parameters.AllKeys.Contains(toKey)) continue;

               // Add range parameter, remove corresponding from/to 
               // parameters, and remove paired from/to key to prevent
               // duplicate processing.
               var rangeValue = parameters[fromKey] + "|" + parameters[toKey];               
               parameters.Add(keyPrefix + "Range", rangeValue);
               parameters.Remove(keyPrefix + "From");
               parameters.Remove(keyPrefix + "To");
            }

            for (var i = 0; i < parameters.Count; i++)
            {
                // Check not Global Search param
                if (parameters.GetKey(i) == Globals.QS_GLOBAL_SEARCH || parameters.GetKey(i) == Globals.QS_SORT_DIRECTION) continue;

                // Build Where clause                     
                var itemKey = parameters.GetKey(i);
                var itemValue = parameters.Get(i);
                var itemClause = string.Empty;
                SearchParameter criteria;
                ObjectParameter queryParameter;

                // Make sure search key exists
                if (!searchCriteria.TryGetValue(itemKey, out criteria))
                {
                    throw new Exception("Search Criteria Does Not Exist: " + itemKey);
                }

                // Some parameters need to be skipped from query generation.
                if (criteria.IgnoreProcessing)
                {
                    continue;
                }

                if (!isFirstParameter)
                {
                    whereClause.Append(" AND ");
                }
                else
                {
                    isFirstParameter = false;
                }

                // Null item values avoid any kind of parameter processing and
                // change the where clause slightly before appending to the query.
                if (itemValue == "null" || itemValue == "N/A")
                {
                    itemClause = criteria.WhereClause.Replace(" = {0}", " IS NULL");
                    whereClause.Append(itemClause);
                }
                else
                {
                    // Most search criteria can simply be appended to the query. Some
                    // are multi-value selectors and need to be processed accordingly.
                    if (!criteria.AllowsMultipleValues)
                    {
                        // Most parameters are single values, but if there is a range
                        // parameter we must split the values up.
                        if (!criteria.IsRangeSearch)
                        {
                            itemClause = String.Format(criteria.WhereClause, "@" + criteria.VariableName);
                            whereClause.Append(itemClause);

                            // Add parameter to query
                            var valueText = itemValue;

                            if (criteria.IsWildcardSearch)
                            {
                                valueText = "%" + valueText + "%";
                            }

                            // Most of the time we use the value specified. However, null values
                            // require the DBNull value.
                            queryParameter = new ObjectParameter
                                                 (criteria.VariableName, criteria.ParameterType) { Value = valueText };

                            query.Parameters.Add(queryParameter);
                        }
                        else
                        {
                            // Use same variable name, simply append "From/To" to its name.
                            itemClause = String.Format(criteria.WhereClause, "@" + criteria.VariableName + "From",
                                                       "@" + criteria.VariableName + "To");
                            whereClause.Append(itemClause);

                            // Add parameter to query
                            var splitValue = itemValue.Split('|');

                            if (criteria.IsWildcardSearch)
                            {
                                splitValue[0] = "%" + splitValue[0] + "%";
                                splitValue[1] = "%" + splitValue[1] + "%";
                            }

                            // Most of the time we use the value specified. However, null values
                            // require the DBNull value.
                            queryParameter = new ObjectParameter
                                             (criteria.VariableName + "From", criteria.ParameterType) { Value = splitValue[0] };

                            query.Parameters.Add(queryParameter);

                            // Most of the time we use the value specified. However, null values
                            // require the DBNull value.
                            queryParameter = new ObjectParameter
                                             (criteria.VariableName + "To", criteria.ParameterType) { Value = splitValue[1] };

                            query.Parameters.Add(queryParameter);
                        }
                    }
                    else
                    {
                        // Parameter contains multiple values, which are split into a
                        // nested OR clause.
                        var values = itemValue.Split(',').Select(Convert.ToString).ToList();
                        if (values.Count > 0)
                        {
                            for (var j = 0; j <= values.Count - 1; j++)
                            {
                                // Jump to next item in the loop if empty.
                                if (String.IsNullOrEmpty(values[j])) continue;

                                if (j != 0)
                                {
                                    whereClause.Append(" OR ");
                                }

                                else if (values.Count > 1)
                                {
                                    whereClause.Append(" (");
                                }

                                itemClause = String.Format(criteria.WhereClause, "@" + criteria.VariableName + j);
                                whereClause.Append(itemClause);

                                // Add parameter to query
                                var valueText = values[j];

                                if (criteria.IsWildcardSearch)
                                {
                                    valueText = "%" + valueText + "%";
                                }

                                // Add parameter to query                        
                                queryParameter = new ObjectParameter
                                                 (criteria.VariableName + j, criteria.ParameterType) { Value = valueText };

                                query.Parameters.Add(queryParameter);
                            }

                            if (values.Count > 1)
                            {
                                whereClause.Append(") ");
                            }
                        }
                    }
                }
            }
            // Append where clause to results. 
            if (whereClause.Length > 0)
            {
               results = query.Where(whereClause.ToString());   
            }            
         }

         return results;
      }    
   }
}