﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Linq;
using Newtonsoft.Json;
using TrackIT2.DAL;

namespace TrackIT2.BLL
{
    public class StructuralAnalysis
    {
        #region Transaction Methods
        public static structural_analyses Search(int id)
        {
            structural_analyses ret = null;

            using (var ce = new CPTTEntities())
            {
                try
                {
                    ret = ce.structural_analyses.First(sa => sa.id.Equals(id));
                }
                catch (InvalidOperationException)
                {
                    //Do nothing and return null.   
                }
            }

            return ret;
        }

        public static structural_analyses SearchWithinTransaction(int id, CPTTEntities ceRef)
        {
            structural_analyses ret = null;

            ret = ceRef.structural_analyses.FirstOrDefault(c => c.id.Equals(id));
            if (ret != null) ceRef.Detach(ret);

            return ret;
        }

        public static structural_analysis_comments SearchComment(int id)
        {
            structural_analysis_comments ret = null;

            using (var ce = new CPTTEntities())
            {
                ret = ce.structural_analysis_comments.First(c => c.id.Equals(id));
                if (ret != null) ce.Detach(ret);
            }

            return ret;
        }

        public static string Add(structural_analyses analysis)
        {
            String results = null;

            // Start transaction for add.
            using (var ce = new CPTTEntities())
            {
                var isSuccess = false;

                try
                {
                    using (var trans = TransactionUtils.CreateTransactionScope())
                    {
                        // Manually open connection to prevent EF from auto closing
                        // connection and causing issues with future queries.
                        ce.Connection.Open();

                        ce.structural_analyses.AddObject(analysis);

                        // Check to see if received date needs to be auto updated.
                        if (CheckAutoUpdate(analysis, ce, true))
                        {
                            UpdateReceivedDate(analysis, ce);
                        }

                        ce.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                        isSuccess = true;
                        trans.Complete();     // Transaction complete
                    }
                }
                catch (Exception ex)
                {
                    // Log error in ELMAH for any diagnostic needs.
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                    isSuccess = false;
                    results = "An error occurred creating the record. Please try again.";
                }
                finally
                {
                    ce.Connection.Dispose();
                }

                // Finally accept all changes from the transaction.
                if (isSuccess)
                {
                    ce.AcceptAllChanges();
                }
            }

            return results;
        }

        /// <summary>
        ///    Adds structural analysis object within an existing context.
        ///    This is needed when doing updates as part of a full lease
        ///    application update. It is assumed that the call to the method is
        ///    within a transaction block outside this method. This method will
        ///    not make a call to trans.complete or ce.SaveChanges since it will
        ///    be the responsibility of the calling method to make the final
        ///    save and allow for a full rollback if an error occurs.
        /// </summary>
        /// <param name="analysis">Structural Analysis record to add.</param>
        /// <param name="ceRef">Entity Context Referece.</param>
        /// <returns></returns>
        public static string AddWithinTransaction(structural_analyses analysis, CPTTEntities ceRef)
        {
            string results = null;

            try
            {
                ceRef.structural_analyses.AddObject(analysis);

                // Check to see if received date needs to be auto updated.
                if (CheckAutoUpdate(analysis, ceRef, true))
                {
                    UpdateReceivedDate(analysis, ceRef);
                }


            }
            catch (Exception ex)
            {
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                results = "An error occurred creating the record. Please try again.";
            }

            return results;
        }

        public static string Update(structural_analyses analysis)
        {
            String results = null;

            // Start transaction for add.
            using (var ce = new CPTTEntities())
            {
                var isSuccess = false;

                try
                {
                    using (var trans = TransactionUtils.CreateTransactionScope())
                    {
                        // Manually open connection to prevent EF from auto closing
                        // connection and causing issues with future queries.
                        ce.Connection.Open();

                        ce.AttachUpdated(analysis);

                        // Check to see if received date needs to be auto updated.
                        if (CheckAutoUpdate(analysis, ce, false))
                        {
                            UpdateReceivedDate(analysis, ce);
                        }

                        // Only add version record if modifications were detected
                        // for that entity.
                        if (ce.ObjectStateManager.GetObjectStateEntry(analysis.EntityKey).State == EntityState.Modified)
                        {
                            ce.structural_analysis_versions.AddObject(StructuralAnalysis.ToVersionFromEntityOriginal(ce, analysis));
                        }

                        ce.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                        isSuccess = true;
                        trans.Complete();     // Transaction complete
                    }
                }
                catch (OptimisticConcurrencyException oex)
                {
                    // Log error in ELMAH for any diagnostic needs.
                    Elmah.ErrorSignal.FromCurrentContext().Raise(oex);

                    isSuccess = false;
                    results = "The current record has been modifed since you have " +
                              "last retrieved it. Please reload the record and " +
                              "attempt to save it again.";
                }
                catch (Exception ex)
                {
                    // Log error in ELMAH for any diagnostic needs.
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                    isSuccess = false;
                    results = "An error occurred saving the record. Please try again.";
                }
                finally
                {
                    ce.Connection.Dispose();
                }

                // Finally accept all changes from the transaction.
                if (isSuccess)
                {
                    ce.AcceptAllChanges();
                }
            }

            return results;
        }

        /// <summary>
        ///    Adds structural analysis object within an existing context.
        ///    This is needed when doing updates as part of a full lease
        ///    application update. It is assumed that the call to the method is
        ///    within a transaction block outside this method. This method will
        ///    not make a call to trans.complete or ce.SaveChanges since it will
        ///    be the responsibility of the calling method to make the final
        ///    save and allow for a full rollback if an error occurs.
        /// </summary>
        /// <param name="analysis"></param>
        /// <param name="ceRef"></param>
        /// <returns></returns> 
        public static string UpdateWithinTransaction(structural_analyses analysis, CPTTEntities ceRef)
        {
            String results = null;

            try
            {
                ceRef.AttachUpdated(analysis);

                // Check to see if received date needs to be auto updated.
                if (CheckAutoUpdate(analysis, ceRef, false))
                {
                    UpdateReceivedDate(analysis, ceRef);
                }

                // Only add version record if modifications were detected
                // for that entity.
                if (ceRef.ObjectStateManager.GetObjectStateEntry(analysis.EntityKey).State == EntityState.Modified)
                {
                    ceRef.structural_analysis_versions.AddObject(StructuralAnalysis.ToVersionFromEntityOriginal(ceRef, analysis));
                }
            }
            catch (OptimisticConcurrencyException oex)
            {
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(oex);
                results = "The current record has been modifed since you have " +
                          "last retrieved it. Please reload the record and " +
                          "attempt to save it again.";
            }
            catch (Exception ex)
            {
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                results = "An error occurred saving the record. Please try again.";
            }

            return results;
        }

        public static bool CheckAutoUpdate(structural_analyses newSA, CPTTEntities ceRef, bool IsAddAction)
        {
            var results = false;
            var actualStatusId = GetDateStatusId("Actual", ceRef);

            // In order for the auto update to trigger, the following criteria 
            // must be met:
            // 1. There must be a SA Ordered Date and SA Type value specified.
            // 2. The SA Received Date Status value must not be "Actual" (null 
            //    is acceptable).
            // 3. The SA Ordered Date or the SA Types value must have changed or 
            //    the IsAddAction flag is set to true. When SA records are added,
            //    there are no "original" values to check against.
            if (newSA.sa_ordered_date.HasValue && newSA.structural_analysis_type_id.HasValue)
            {
                if (
                    !newSA.sa_received_date.HasValue ||
                    !newSA.sa_received_date_status_id.HasValue ||
                    newSA.sa_received_date_status_id.HasValue && newSA.sa_received_date_status_id.Value != actualStatusId
                   )
                {
                    // Add actions will not have an original state entry to
                    // retrieve, so we bypass the check immediately.
                    if (IsAddAction == true)
                    {
                        results = true;
                    }
                    else
                    {
                        var origSARecord = ceRef.ObjectStateManager
                                                .GetObjectStateEntry(newSA.EntityKey)
                                                .OriginalValues;

                        // Sometimes the previous record is null, or will have no
                        // value, but come out as an empty string, in the case of
                        // DateTime objects.
                        if (
                            (String.IsNullOrEmpty(origSARecord["sa_ordered_date"].ToString()) || DateTime.Compare((DateTime)origSARecord["sa_ordered_date"], newSA.sa_ordered_date.Value) != 0) ||
                            (String.IsNullOrEmpty(origSARecord["structural_analysis_type_id"].ToString()) || (int)origSARecord["structural_analysis_type_id"] != newSA.structural_analysis_type_id)
                           )
                        {
                            results = true;
                        }
                    }
                }
            }

            return results;
        }

        /// <summary>
        ///    This method auto updates the received date for the record
        ///    specified based on the received date status and structural
        ///    analysis type.
        /// </summary>
        /// <param name="analysis">The record to modify.</param>
        /// <param name="ceRef">Entity Context Reference for queries.</param>
        /// <remarks>
        ///    This method assumes that the conditions necessary to auto update
        ///    this field has already been met. (See CheckAutoUpdate method.)
        /// </remarks>
        public static void UpdateReceivedDate(structural_analyses analysis, CPTTEntities ceRef)
        {
            var projectedStatusId = GetDateStatusId("Projected", ceRef);

            var objState = ceRef.ObjectStateManager.GetObjectStateEntry(analysis.EntityKey);
            var newReceivedDate = CalculateReceivedDate(analysis.sa_ordered_date,
                                                        analysis.structural_analysis_type_id.Value,
                                                        ceRef);

            objState.CurrentValues.SetValue(objState.CurrentValues.GetOrdinal("sa_received_date_status_id"), projectedStatusId);
            objState.CurrentValues.SetValue(objState.CurrentValues.GetOrdinal("sa_received_date"), newReceivedDate);
        }

        /// <summary>
        ///    Calculates the received date based on the ordered date and
        ///    type.
        /// </summary>
        /// <param name="orderedDate">Ordered Date for record.</param>
        /// <param name="typeId">Structural Analysis record type.</param>
        /// <param name="context">Entity Context reference for type query.</param>
        /// <returns>DateTime value of calculated date.</returns>
        public static DateTime CalculateReceivedDate(DateTime? orderedDate, int typeId, CPTTEntities context)
        {
            var results = new DateTime();

            // Retrieve Mapping type Id for special case
            var mappingType = new structural_analysis_types();

            mappingType = context.structural_analysis_types
                                 .FirstOrDefault(type => type.structural_analysis_type.Equals("Mapping"));

            if (typeId == mappingType.id)
            {
                // Received = Current Date + 60 Days
                results = DateTime.Today.AddDays(60);
            }
            else
            {
                // Received = Ordered Date + 25 Days. If no date is specified, use current date.
                if (orderedDate.HasValue)
                {
                    results = orderedDate.Value.AddDays(25);
                }
                else
                {
                    results = DateTime.Today.AddDays(25);
                }
            }

            return results;
        }

        public static String Delete(int id, CPTTEntities ceRef = null)
        {
            String results = null;
            var isSuccess = false;
            var isRootTransaction = true;

            CPTTEntities ce;
            if (ceRef == null)
            {
                //using new entity object
                ce = new CPTTEntities();
            }
            else
            {
                //using current entity object
                ce = ceRef;
                isRootTransaction = false;
            }
            try
            {
                using (var trans = TransactionUtils.CreateTransactionScope())
                {
                    if (isRootTransaction)
                    {
                        // Manually open connection to prevent EF from auto closing
                        // connection and causing issues with future queries.
                        ce.Connection.Open();
                    }

                    // Remove any Structural Analysis Version records
                    var queryVer = from saVer in ce.structural_analysis_versions
                                   where saVer.structural_analysis_id == id
                                   select saVer;

                    if (queryVer.Any())
                    {
                        foreach (var item in queryVer.ToList())
                        {
                            ce.DeleteObject(item);
                        }
                    }

                    // Remove any related Structural Analysis Comment records
                    var queryComment = from saComment in ce.structural_analysis_comments
                                       where saComment.structural_analysis_id == id
                                       select saComment;

                    if (queryComment.Any())
                    {
                        foreach (var item in queryComment.ToList())
                        {
                            ce.DeleteObject(item);
                            DocumentsChangeLog.UpdateIsLinkRecordFlag(item.id, DMSDocumentLinkHelper.eDocumentType.structural_analyses.ToString(), false, ce
                                ,DMSDocumentLinkHelper.eDocumentField.SA_Comments.ToString());
                        }
                    }

                    //Remove any related Structural Analysis document records
                    var queryDocument = from saDoc in ce.documents_links
                                        where saDoc.record_id == id
                                        select saDoc;

                    if (queryDocument.Any())
                    {
                        foreach (var item in queryDocument.ToList())
                        {
                            ce.DeleteObject(item);
                        }
                    }

                    //Delete record
                    var querySA = from sa in ce.structural_analyses
                                  where sa.id == id
                                  select sa;

                    if (querySA.Any())
                    {
                        var item = querySA.ToList()[0];
                        ce.DeleteObject(item);
                    }
                    else
                    {
                        throw new NullReferenceException();
                    }

                    // Change DocLog
                    DocumentsChangeLog.UpdateIsLinkRecordFlag(id, DMSDocumentLinkHelper.eDocumentType.structural_analyses.ToString(), false, ce);

                    if (isRootTransaction)
                    {
                        // We tentatively save the changes so that we can finish 
                        // processing the transaction.
                        ce.SaveChanges(SaveOptions.DetectChangesBeforeSave);
                    }

                    isSuccess = true;
                    trans.Complete();     //Transaction complete.
                }
            }
            catch (Exception ex)
            {
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                isSuccess = false;
                results = ex.ToString();
            }
            finally
            {
                if (isRootTransaction)
                {
                    ce.Connection.Dispose();
                }
            }

            // Finally accept all changes from the transaction.
            if (isSuccess && isRootTransaction)
            {
                ce.AcceptAllChanges();
                ce.Dispose();
            }

            return results;
        }

        /// <summary>
        ///    Removes structural analyis objects within an existing context.
        ///    This is needed when doing updates as part of a full lease
        ///    application update. It is assumed that the call to the method is
        ///    within a transaction block outside this method. This method will
        ///    not make a call to trans.complete or ce.SaveChanges since it will
        ///    be the responsibility of the calling method to make the final
        ///    save and allow for a full rollback if an error occurs.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ceRef"></param>
        /// <returns></returns> 
        public static String DeleteWithinTransaction(int id, CPTTEntities ceRef)
        {
            String results = null;

            try
            {
                // Remove any Structural Analysis Version records
                var queryVer = from saVer in ceRef.structural_analysis_versions
                               where saVer.structural_analysis_id == id
                               select saVer;

                if (queryVer.Any())
                {
                    foreach (var item in queryVer.ToList())
                    {
                        ceRef.DeleteObject(item);
                    }
                }

                // Remove any related Structural Analysis Comment records
                var queryComment = from saComment in ceRef.structural_analysis_comments
                                   where saComment.structural_analysis_id == id
                                   select saComment;

                if (queryComment.Any())
                {
                    foreach (var item in queryComment.ToList())
                    {
                        ceRef.DeleteObject(item);
                    }
                }

                // Remove any related Structural Analysis document records
                var queryDocument = from saDoc in ceRef.documents_links
                                    where saDoc.record_id == id
                                    select saDoc;

                if (queryDocument.Any())
                {
                    foreach (var item in queryDocument.ToList())
                    {
                        ceRef.DeleteObject(item);
                    }
                }

                //Delete record
                var querySA = from sa in ceRef.structural_analyses
                              where sa.id == id
                              select sa;

                if (querySA.Any())
                {
                    var item = querySA.ToList()[0];
                    ceRef.DeleteObject(item);
                }
                else
                {
                    throw new NullReferenceException();
                }
            }
            catch (Exception ex)
            {
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                results = "Error removing structural analysis record. Please try again";
            }

            return results;
        }

        #endregion


        /// <summary>
        /// Clones the structural_analysis object into a structural_analysis_version
        /// object.
        /// </summary>
        /// <param name="analysis">The current structural analysis record.</param>
        /// <returns>Structural Analysis Version object with all values.</returns>
        public static structural_analysis_versions ToStructuralAnalysisVersion
                                                   (structural_analyses analysis)
        {
            var results = new structural_analysis_versions();

            // Use reflection to get all modifiyable properties
            var srcProperties = from p in typeof(structural_analyses).GetProperties()
                                where p.CanRead &&
                                      p.CanWrite &&
                                      p.Name != "id" &&
                                      p.Name != "EntityKey"
                                select p;

            results.structural_analysis_id = analysis.id;

            // Iterate through all properites and assign them to the version copy.
            foreach (var property in srcProperties)
            {
                var currValue = property.GetValue(analysis, null);

                if (currValue == null) continue;

                var destProperty = typeof(structural_analysis_versions).GetProperty(property.Name);

                if (destProperty == null) continue;

                // Checking for EntityReference to not create the existing reference again.
                if (currValue.GetType().BaseType.Name == "EntityReference")
                {
                    // Simply copy the EntityKey to the new entity object’s EntityReference
                    ((EntityReference)destProperty.GetValue(results, null)).EntityKey =
                       ((EntityReference)currValue).EntityKey;
                }
                else
                {
                    // Common task, just copy the simple type property into the new entity object
                    destProperty.SetValue(results, currValue, null);
                }
            }

            return results;
        }

        /// <summary>
        ///    Creates a version of an existing structural analysis record by
        ///    using the entity context's original values.
        /// </summary>
        /// <param name="context">Context in which entity resides.</param>
        /// <param name="sa">The structural analysis entity to version.</param>
        /// <returns>Structural Analysis Version object.</returns>
        public static structural_analysis_versions ToVersionFromEntityOriginal(CPTTEntities context, structural_analyses sa)
        {
            var results = new structural_analysis_versions();
            var origSA = new structural_analyses();
            var objState = context.ObjectStateManager.GetObjectStateEntry(sa.EntityKey);
            var origSARecord = objState.OriginalValues;
            var origRecordFields = new List<string>();

            // Build list of property names for validation check later.
            for (var c = 0; c <= origSARecord.FieldCount - 1; c++)
            {
                if (!origRecordFields.Contains(origSARecord.GetName(c)))
                {
                    origRecordFields.Add(origSARecord.GetName(c));
                }
            }

            origSA.id = sa.id;

            // Use reflection to get all modifiyable properties
            var srcProperties = from p in typeof(structural_analyses).GetProperties()
                                where p.CanRead &&
                                      p.CanWrite &&
                                      p.Name != "id" &&
                                      p.Name != "EntityKey"
                                select p;

            // Iterate through all properites and assign them to the version copy.
            foreach (var property in srcProperties)
            {
                // Some properties in the entity object are foreign keys, and will
                // not have a record in the original values list.
                if (!origRecordFields.Contains(property.Name)) continue;

                var currValue = origSARecord[property.Name];

                if (currValue == null || currValue.GetType().Name == "DBNull") continue;

                var destProperty = typeof(structural_analyses).GetProperty(property.Name);

                if (destProperty == null) continue;

                //Checking for EntityReference to not create the existing reference again.
                if (currValue.GetType().BaseType.Name == "EntityReference")
                {
                    //Simply copy the EntityKey to the new entity object’s EntityReference
                    ((EntityReference)destProperty.GetValue(results, null)).EntityKey =
                       ((EntityReference)currValue).EntityKey;
                }
                else
                {
                    //Common task, just copy the simple type property into the new entity object
                    destProperty.SetValue(origSA, currValue, null);
                }
            }

            results = ToStructuralAnalysisVersion(origSA);
            return results;
        }

        public static int GetJSON(Boolean isNull, out String sJson, int iAppId = 0)
        {
            var iRet = 0;
            var sRet = "";
            using (var ce = new CPTTEntities())
            {
                if (!isNull && iAppId != 0)
                {
                    var query = (from sa in
                                    (
                                        from structure in ce.structural_analyses
                                        where structure.lease_application_id == iAppId
                                        select structure).ToList()
                                select new json_structural_analyses
                                {
                                    ID = sa.id,
                                    LeaseAppID = iAppId,
                                    po_ordered_date = Utility.DateToString(sa.po_ordered_date),
                                    po_received_date = Utility.DateToString(sa.po_received_date),
                                    po_amount = sa.po_amount,
                                    po_number = sa.po_number,
                                    structural_analysis_type_id = sa.structural_analysis_type_id,
                                    sa_ordered_date = Utility.DateToString(sa.sa_ordered_date),
                                    sa_received_date = Utility.DateToString(sa.sa_received_date),
                                    sa_ordered_date_status_id = sa.sa_ordered_date_status_id,
                                    sa_tower_prcnt = sa.sa_tower_prcnt,
                                    sa_order_onhold_date = Utility.DateToString(sa.sa_order_onhold_date),
                                    sa_order_cancelled_date = Utility.DateToString(sa.sa_order_cancelled_date),
                                    sa_fee_received_date = Utility.DateToString(sa.sa_fee_received_date),
                                    sa_fee_amount = sa.sa_fee_amount,
                                    sa_check_nbr = Utility.PrepareString(sa.sa_check_nbr),
                                    structural_analysis_fee_payor_type_id = sa.structural_analysis_fee_payor_type_id,
                                    sa_vendor_id = sa.sa_vendor_id,
                                    sac_name_emp_id = sa.sac_name_emp_id,
                                    saw_to_customer_date = Utility.DateToString(sa.saw_to_customer_date),
                                    saw_approved_date = Utility.DateToString(sa.saw_approved_date),
                                    saw_to_sac_date = Utility.DateToString(sa.saw_to_sac_date),
                                    saw_request_date = Utility.DateToString(sa.saw_request_date),
                                    sac_priority_id = sa.sac_priority_id,
                                    sac_description_id = sa.sac_description_id,
                                    shopping_cart_number = Utility.PrepareString(sa.shopping_cart_number),
                                    blanket_po_month = sa.blanket_po_month,
                                    blanket_po_year = sa.blanket_po_year,
                                    po_release_status_id = sa.po_release_status_id,
                                    po_release_date = Utility.DateToString(sa.po_release_date),
                                    po_notes = Utility.PrepareString(sa.po_notes),
                                    sa_received_date_status_id = sa.sa_received_date_status_id,
                                    ps_received_date = Utility.DateToString(sa.ps_received_date),
                                    sa_updated_at = Utility.DateTimeToString(sa.updated_at),
                                    vendor_list = ((from v in ce.sa_vendors
                                                    where v.is_preferred == true
                                                    orderby v.sa_vendor
                                                    select new json_sa_vendors
                                                    {
                                                        id = v.id,
                                                        sa_vendor = v.sa_vendor,
                                                        is_preferred = v.is_preferred
                                                    }).Union(from v2 in ce.sa_vendors
                                                             where (sa.sa_vendor_id != null) &&
                                                             (v2.id == sa.sa_vendor_id.Value) &&
                                                             (v2.is_preferred == false)
                                                             select new json_sa_vendors
                                                             {
                                                                 id = v2.id,
                                                                 sa_vendor = v2.sa_vendor + " (historical)",
                                                                 is_preferred = v2.is_preferred
                                                             })).ToList(),
                                    sac_description_list = (from desc in ce.sac_descriptions
                                                            where desc.is_preferred == true
                                                            orderby desc.description
                                                            select new json_sac_descriptions
                                                            {
                                                                id = desc.id,
                                                                description = desc.description,
                                                                is_preferred = desc.is_preferred
                                                            }).Union(from desc2 in ce.sac_descriptions
                                                                     where (sa.sac_description_id != null) &&
                                                                     (desc2.id == sa.sac_description_id.Value) &&
                                                                     (desc2.is_preferred == false)
                                                                     select new json_sac_descriptions
                                                                     {
                                                                         id = desc2.id,
                                                                         description = desc2.description + " (historical)",
                                                                         is_preferred = desc2.is_preferred
                                                                     }).ToList(),
                                    comments = (from result in
                                                    (from c in ce.structural_analysis_comments
                                                     where c.structural_analysis_id == sa.id
                                                     orderby c.structural_analysis_id
                                                     select c
                                                     ).ToList()
                                                select new json_comments
                                                {
                                                    comment_id = result.id,
                                                    sa_id = sa.id,
                                                    comment = result.comments,
                                                    creator = result.creator_user != null ? result.creator_user.last_name + result.creator_user.first_name : "No employee recorded.",
                                                    created_at = Utility.DateToString(result.created_at),
                                                    creator_id = result.creator_user != null ? result.creator_user.tmo_userid : "none",
                                                    updater = result.updater_user != null ? result.updater_user.last_name + result.updater_user.first_name : "No employee recorded.",
                                                    update_at = Utility.DateToString(result.updated_at),
                                                    exist = true
                                                }).ToList(),
                                    comment_count = (from c in ce.structural_analysis_comments
                                                     where c.structural_analysis_id == sa.id
                                                     orderby c.structural_analysis_id
                                                     group c by c.structural_analysis_id into g
                                                     select g).Count(),
                                    sa_decision_date = Utility.DateToString(sa.sa_decision_date),
                                    sa_decision_type_id = sa.sa_decision_type_id,
                                    sa_decision_type_text = sa.sa_decision_type_id.HasValue ? (from decision_type in ce.struct_decision_types
                                                                                               where decision_type.id == sa.sa_decision_type_id.Value
                                                                                               select decision_type).FirstOrDefault().struct_decision_type : string.Empty,
                                    structural_analysis_type_text = sa.structural_analysis_type_id.HasValue ? (from structural_analysis_type in ce.structural_analysis_types
                                                                                                               where structural_analysis_type.id == sa.structural_analysis_type_id.Value
                                                                                                               select structural_analysis_type).FirstOrDefault().structural_analysis_type : string.Empty
                                }).ToList();

                    if (query.Any())
                    {
                        sRet = JsonConvert.SerializeObject(query);
                        iRet = query.Count();
                    }
                }
                else
                {

                    var query = new
                    {
                        ID = 0,
                        LeaseAppID = iAppId,
                        po_ordered_date = (object)null,
                        po_received_date = (object)null,
                        po_amount = (object)null,
                        po_number = (object)null,
                        structural_analysis_type_id = (object)null,
                        sa_ordered_date = (object)null,
                        sa_received_date = (object)null,
                        sa_ordered_date_status_id = (object)null,
                        sa_tower_prcnt = (object)null,
                        sa_order_onhold_date = (object)null,
                        sa_order_cancelled_date = (object)null,
                        sa_fee_received_date = (object)null,
                        sa_fee_amount = (object)null,
                        sa_check_nbr = (object)null,
                        structural_analysis_fee_payor_type_id = (object)null,
                        sa_vendor_id = (object)null,
                        sac_name_emp_id = (object)null,
                        saw_to_customer_date = (object)null,
                        saw_approved_date = (object)null,
                        saw_to_sac_date = (object)null,
                        saw_request_date = (object)null,
                        sac_priority_id = (object)null,
                        sac_description_id = (object)null,
                        shopping_cart_number = (object)null,
                        blanket_po_month = (object)null,
                        blanket_po_year = (object)null,
                        po_release_status_id = (object)null,
                        po_release_date = (object)null,
                        po_notes = (object)null,
                        sa_received_date_status_id = (object)null,
                        ps_received_date = (object)null,
                        sa_updated_at = (object)null,
                        vendor_list = (from v in ce.sa_vendors
                                       where v.is_preferred == true
                                       orderby v.sa_vendor
                                       select new
                                       {
                                           id = v.id,
                                           sa_vendor = v.sa_vendor,
                                           is_preferred = v.is_preferred
                                       }),
                        sac_description_list = (from desc in ce.sac_descriptions
                                                where desc.is_preferred == true
                                                orderby desc.description
                                                select new
                                                {
                                                    id = desc.id,
                                                    description = desc.description,
                                                    is_preferred = desc.is_preferred
                                                }),
                        comments = (object)null,
                        comment_count = 0,
                        sawToSacDocument = (object)null,
                        SA_ReceivedDocument = (object)null,
                        Structural_DecisionDocument = (object)null,
                        DelayedOrder_CancelledDocument = (object)null,
                        DelayedOrder_OnHoldDocument = (object)null,
                        PurchaseOrder_ReceivedDocument = (object)null,
                        sa_decision_date = (object)null,
                        sa_decision_type_id = (object)null
                    };

                    if (query != null)
                    {
                        sRet = JsonConvert.SerializeObject(query);
                        iRet = 1;
                    }
                }
            }
            sJson = sRet;
            return iRet;
        }

        public static List<StructuralAnalysisTemplate> ParseFromJSON(String sJson)
        {
            var lstResult = new List<StructuralAnalysisTemplate>();

            if (!string.IsNullOrEmpty(sJson))
            {
                const string jsonResultFormat = "\"result\" : {0}";
                var sTestJson = "{" + String.Format(jsonResultFormat, sJson) + "}";

                var jsonResult = JsonConvert.DeserializeObject<StructuralAnalysisResult>(sTestJson);
                lstResult = jsonResult.result;
            }
            return lstResult;
        }

        private static int GetDateStatusId(string str, CPTTEntities ceRef)
        {
            var statusId = 0;

            try
            {
                // Get Actual dateStatus
                var dateStatus = ceRef.date_statuses.FirstOrDefault(date => date.date_status.Equals(str));

                statusId = dateStatus.id;
            }
            catch (Exception ex)
            {
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return statusId;
        }
    }

    [Serializable]
    public class StructuralAnalysisResult
    {
        public List<StructuralAnalysisTemplate> result { get; set; }
    }
    public class StructuralAnalysisTemplate
    {
        public String ID { get; set; }
        public String LeaseAppID { get; set; }
        private String m_po_ordered_date;
        public String po_ordered_date
        {
            get
            {
                return m_po_ordered_date;
            }
            set
            {
                CheckIsNull(value);
                m_po_ordered_date = value;
            }
        }
        private String m_po_received_date;
        public String po_received_date
        {
            get
            {
                return m_po_received_date;
            }
            set
            {
                CheckIsNull(value);
                m_po_received_date = value;
            }
        }
        private String m_po_amount;
        public String po_amount
        {
            get
            {
                return m_po_amount;
            }
            set
            {
                CheckIsNull(value);
                m_po_amount = value;
            }
        }
        private String m_po_number;
        public String po_number
        {
            get
            {
                return m_po_number;
            }
            set
            {
                CheckIsNull(value);
                m_po_number = value;
            }
        }
        private String m_structural_analysis_type_id;
        public String structural_analysis_type_id
        {
            get
            {
                return m_structural_analysis_type_id;
            }
            set
            {
                CheckIsNull(value);
                m_structural_analysis_type_id = value;
            }
        }
        private String m_sa_ordered_date;
        public String sa_ordered_date
        {
            get
            {
                return m_sa_ordered_date;
            }
            set
            {
                CheckIsNull(value);
                m_sa_ordered_date = value;
            }
        }
        private String m_sa_received_date;
        public String sa_received_date
        {
            get
            {
                return m_sa_received_date;
            }
            set
            {
                CheckIsNull(value);
                m_sa_received_date = value;
            }
        }
        private String m_sa_ordered_date_status_id;
        public String sa_ordered_date_status_id
        {
            get
            {
                return m_sa_ordered_date_status_id;
            }
            set
            {
                CheckIsNull(value);
                m_sa_ordered_date_status_id = value;
            }
        }
        private String m_sa_tower_prcnt;
        public String sa_tower_prcnt
        {
            get
            {
                return m_sa_tower_prcnt;
            }
            set
            {
                CheckIsNull(value);
                m_sa_tower_prcnt = value;
            }
        }
        private String m_sa_order_onhold_date;
        public String sa_order_onhold_date
        {
            get
            {
                return m_sa_order_onhold_date;
            }
            set
            {
                CheckIsNull(value);
                m_sa_order_onhold_date = value;
            }
        }
        private String m_sa_order_cancelled_date;
        public String sa_order_cancelled_date
        {
            get
            {
                return m_sa_order_cancelled_date;
            }
            set
            {
                CheckIsNull(value);
                m_sa_order_cancelled_date = value;
            }
        }
        private String m_sa_fee_received_date;
        public String sa_fee_received_date
        {
            get
            {
                return m_sa_fee_received_date;
            }
            set
            {
                CheckIsNull(value);
                m_sa_fee_received_date = value;
            }
        }
        private String m_sa_fee_amount;
        public String sa_fee_amount
        {
            get
            {
                return m_sa_fee_amount;
            }
            set
            {
                CheckIsNull(value);
                m_sa_fee_amount = value;
            }
        }
        private String m_sa_check_nbr;
        public String sa_check_nbr
        {
            get
            {
                return m_sa_check_nbr;
            }
            set
            {
                CheckIsNull(value);
                m_sa_check_nbr = value;
            }
        }
        private String m_structural_analysis_fee_payor_type_id;
        public String structural_analysis_fee_payor_type_id
        {
            get
            {
                return m_structural_analysis_fee_payor_type_id;
            }
            set
            {
                CheckIsNull(value);
                m_structural_analysis_fee_payor_type_id = value;
            }
        }
        private String m_sa_vendor_id;
        public String sa_vendor_id
        {
            get
            {
                return m_sa_vendor_id;
            }
            set
            {
                CheckIsNull(value);
                m_sa_vendor_id = value;
            }
        }
        private String m_sac_name_emp_id;
        public String sac_name_emp_id
        {
            get
            {
                return m_sac_name_emp_id;
            }
            set
            {
                CheckIsNull(value);
                m_sac_name_emp_id = value;
            }
        }
        private String m_saw_to_customer_date;
        public String saw_to_customer_date
        {
            get
            {
                return m_saw_to_customer_date;
            }
            set
            {
                CheckIsNull(value);
                m_saw_to_customer_date = value;
            }
        }
        private String m_saw_approved_date;
        public String saw_approved_date
        {
            get
            {
                return m_saw_approved_date;
            }
            set
            {
                CheckIsNull(value);
                m_saw_approved_date = value;
            }
        }
        private String m_saw_to_sac_date;
        public String saw_to_sac_date
        {
            get
            {
                return m_saw_to_sac_date;
            }
            set
            {
                CheckIsNull(value);
                m_saw_to_sac_date = value;
            }
        }
        private String m_saw_request_date;
        public String saw_request_date
        {
            get
            {
                return m_saw_request_date;
            }
            set
            {
                CheckIsNull(value);
                m_saw_request_date = value;
            }
        }
        private String m_sac_priority_id;
        public String sac_priority_id
        {
            get
            {
                return m_sac_priority_id;
            }
            set
            {
                CheckIsNull(value);
                m_sac_priority_id = value;
            }
        }
        private String m_sac_description_id;
        public String sac_description_id
        {
            get
            {
                return m_sac_description_id;
            }
            set
            {
                CheckIsNull(value);
                m_sac_description_id = value;
            }
        }
        private String m_shopping_cart_number;
        public String shopping_cart_number
        {
            get
            {
                return m_shopping_cart_number;
            }
            set
            {
                CheckIsNull(value);
                m_shopping_cart_number = value;
            }
        }
        private String m_blanket_po_month;
        public String blanket_po_month
        {
            get
            {
                return m_blanket_po_month;
            }
            set
            {
                CheckIsNull(value);
                m_blanket_po_month = value;
            }
        }
        private String m_blanket_po_year;
        public String blanket_po_year
        {
            get
            {
                return m_blanket_po_year;
            }
            set
            {
                CheckIsNull(value);
                m_blanket_po_year = value;
            }
        }
        private String m_po_release_status_id;
        public String po_release_status_id
        {
            get
            {
                return m_po_release_status_id;
            }
            set
            {
                CheckIsNull(value);
                m_po_release_status_id = value;
            }
        }
        private String m_po_release_date;
        public String po_release_date
        {
            get
            {
                return m_po_release_date;
            }
            set
            {
                CheckIsNull(value);
                m_po_release_date = value;
            }
        }
        private String m_po_notes;
        public String po_notes
        {
            get
            {
                return m_po_notes;
            }
            set
            {
                CheckIsNull(value);
                m_po_notes = value;
            }
        }
        private String m_sa_received_date_status_id;
        public String sa_received_date_status_id
        {
            get
            {
                return m_sa_received_date_status_id;
            }
            set
            {
                CheckIsNull(value);
                m_sa_received_date_status_id = value;
            }
        }
        private String m_ps_received_date;
        public String ps_received_date
        {
            get
            {
                return m_ps_received_date;
            }
            set
            {
                CheckIsNull(value);
                m_ps_received_date = value;
            }
        }

        private String m_sa_updated_at;
        public String sa_updated_at
        {
            get { return m_sa_updated_at; }
            set
            {
                CheckIsNull(value);
                m_sa_updated_at = value;
            }
        }

        public Object vendor_list { get; set; }
        public Object sac_description_list { get; set; }
        public Object comments { get; set; }
        public Object comment_count { get; set; }
        public Object SA_ReceivedDocument { get; set; }
        public Object sawToSacDocument { get; set; }
        public Object Structural_DecisionDocument { get; set; }
        public Object DelayedOrder_CancelledDocument { get; set; }
        public Object DelayedOrder_OnHoldDocument { get; set; }
        public Object PurchaseOrder_ReceivedDocument { get; set; }

        private string m_new_document;
        public string sa_new_document
        {
            get
            {
                return m_new_document;
            }
            set
            {
                CheckIsNull(value);
                m_new_document = value;
            }
        }

        private Boolean m_IsNull = true;
        private void CheckIsNull(String sValue)
        {
            if (sValue != null && sValue != "")
            {
                m_IsNull = false;
            }
        }
        public Boolean CheckIsNull()
        {
            return m_IsNull & (ID == "0");
        }

        private String m_sa_decision_date;
        public String sa_decision_date
        {
            get
            {
                return m_sa_decision_date;
            }
            set
            {
                CheckIsNull(value);
                m_sa_decision_date = value;
            }
        }

        private String m_sa_decision_type_id;
        public String sa_decision_type_id
        {
            get
            {
                return m_sa_decision_type_id;
            }
            set
            {
                CheckIsNull(value);
                m_sa_decision_type_id = value;
            }
        }

    }
    public class json_structural_analyses
    {
        public int? ID { set; get; }
        public int? LeaseAppID { set; get; }
        public string po_ordered_date { set; get; }
        public string po_received_date { set; get; }
        public Decimal? po_amount { set; get; }
        public string po_number { set; get; }
        public int? structural_analysis_type_id { set; get; }
        public string structural_analysis_type_text { set; get; }
        public string sa_ordered_date { set; get; }
        public string sa_received_date { set; get; }
        public int? sa_ordered_date_status_id { set; get; }
        public Double? sa_tower_prcnt { set; get; }
        public string sa_order_onhold_date { set; get; }
        public string sa_order_cancelled_date { set; get; }
        public string sa_fee_received_date { set; get; }
        public Decimal? sa_fee_amount { set; get; }
        public string sa_check_nbr { set; get; }
        public int? structural_analysis_fee_payor_type_id { set; get; }
        public int? sa_vendor_id { set; get; }
        public int? sac_name_emp_id { set; get; }
        public string saw_to_customer_date { set; get; }
        public string saw_approved_date { set; get; }
        public string saw_to_sac_date { set; get; }
        public string saw_request_date { set; get; }
        public int? sac_priority_id { set; get; }
        public int? sac_description_id { set; get; }
        public string shopping_cart_number { set; get; }
        public int? blanket_po_month { set; get; }
        public int? blanket_po_year { set; get; }
        public int? po_release_status_id { set; get; }
        public string po_release_date { set; get; }
        public string po_notes { set; get; }
        public int? sa_received_date_status_id { set; get; }
        public string ps_received_date { set; get; }
        public string sa_updated_at { set; get; }

        public string sa_decision_date { set; get; }
        public int? sa_decision_type_id { set; get; }
        public string sa_decision_type_text { set; get; }

        private List<json_sa_vendors> m_vendor_list;
        public List<json_sa_vendors> vendor_list
        {
            get
            {
                return m_vendor_list;
            }
            set
            {
                if (value != null)
                {
                    m_vendor_list = value;
                }
                else
                {
                    m_vendor_list = new List<json_sa_vendors>();
                }
            }
        }

        private List<json_sac_descriptions> m_sac_description_list;
        public List<json_sac_descriptions> sac_description_list
        {
            get
            {
                return m_sac_description_list;
            }
            set
            {
                if (value != null)
                {
                    m_sac_description_list = value;
                }
                else
                {
                    m_sac_description_list = new List<json_sac_descriptions>();
                }
            }
        }

        private List<json_comments> m_comment;
        public List<json_comments> comments
        {
            get
            {
                return m_comment;
            }
            set
            {
                if (value != null)
                {
                    m_comment = value;
                }
                else
                {
                    m_comment = new List<json_comments>();
                }
            }
        }
        public int? comment_count { set; get; }
    }

    public class json_sa_vendors
    {
        public int? id { get; set; }
        public string sa_vendor { get; set; }
        public Boolean? is_preferred { get; set; }

    }
    public class json_sac_descriptions
    {
        public int? id { get; set; }
        public string description { get; set; }
        public Boolean? is_preferred { get; set; }

    }
    public class json_comments
    {
        public int? comment_id { set; get; }
        public int? sa_id { set; get; }
        public string comment { set; get; }
        public string creator { set; get; }
        public string created_at { set; get; }
        public string creator_id { set; get; }
        public string updater { set; get; }
        public string update_at { set; get; }
        public bool exist { set; get; }
    }
}