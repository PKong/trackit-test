﻿using System;

namespace TrackIT2.BLL
{
   public class LeaseAdminList
   {
      public int id { get; set; }
      public string SiteId { get; set; }
      public string SiteName { get; set; }
      public string CustomerName { get; set; }
      public string Phase { get; set; }
      public DateTime? LeaseExecutionDate { get; set; }
      public DateTime? CommencementDate { get; set; }
      public string RevShare { get; set; }
      public string LeaseAdminExecutionDocumentType { get; set; }
   }
}