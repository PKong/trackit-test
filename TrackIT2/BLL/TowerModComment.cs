﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TrackIT2.DAL;

namespace TrackIT2.BLL
{
    public static class TowerModComment
    {
        public static tower_mod_comments Search(int id)
        {
            tower_mod_comments ret = null;

            using (var ce = new CPTTEntities())
            {
                ret = ce.tower_mod_comments.First(c => c.id.Equals(id));
                if (ret != null) ce.Detach(ret);
            }

            return ret;
        }

        public static List<tower_mod_comments> GetComments(int? tower_mod_id)
        {
            List<tower_mod_comments> listRet = new List<tower_mod_comments>();
            if (tower_mod_id.HasValue)
            {
                using (CPTTEntities context = new CPTTEntities())
                {
                    var query = from sa in context.tower_mod_comments
                                where sa.tower_modification_id == tower_mod_id.Value
                                orderby sa.created_at descending
                                select sa;
                    listRet.AddRange(query.ToList<tower_mod_comments>());
                }
            }
            return listRet;
        }

        public static void Add(tower_mod_comments objComment)
        {
            // TODO: Add additional security/business logic in here as needed.
            // TODO: Add additional fields into database insert.
            using (CPTTEntities ce = new CPTTEntities())
            {
                HttpContext context = HttpContext.Current;
                ce.tower_mod_comments.AddObject(objComment);
                ce.SaveChanges();
            }
        }

        public static void Delete(int id)
        {
            using (CPTTEntities ce = new CPTTEntities())
            {
                //remove any relate link document
                var documents = from doc in ce.documents_links
                                where doc.record_id == id
                                select doc;

                if (documents.Count() > 0)
                {
                    foreach (documents_links item in documents.ToList<documents_links>())
                    {
                        ce.DeleteObject(item);
                    }
                }

                var query = from c in ce.tower_mod_comments
                            where c.id == id
                            select c;
                if (query.ToList().Count > 0)
                {
                    tower_mod_comments result = query.ToList()[0];
                    ce.tower_mod_comments.DeleteObject(result);

                    // Change DocLog
                    DocumentsChangeLog.UpdateIsLinkRecordFlag(id, DMSDocumentLinkHelper.eDocumentType.tower_modification.ToString(), false, ce,
                        DMSDocumentLinkHelper.eDocumentField.TMO_Comments.ToString());
                    ce.SaveChanges();
                }
            }
        }

        public static List<AllComments> GetTowerModCommentByTowerModId(int towerModId)
        {
            List<AllComments> returnResult = new List<AllComments>();
            using (CPTTEntities ce = new CPTTEntities())
            {
                var towerMod = (from modComment in ce.tower_mod_comments
                               from tower in ce.tower_modifications
                               where modComment.tower_modification_id == tower.id && tower.id == towerModId
                               select new AllComments
                               {
                                   CommentID = modComment.id,
                                   LeaseApplicationID = null,
                                   StructuralAnalysisID = null,
                                   LeaseAddminID = null,
                                   SdpID = null,
                                   TowerModID = modComment.tower_modification_id,
                                   Comments = modComment.comment,
                                   CreatorID = modComment.creator_id,
                                   CreatorName = modComment.user.last_name + ", " + modComment.user.first_name,
                                   CreateAt = modComment.created_at,
                                   CreatorUsername = modComment.user.tmo_userid,
                                   UpdaterID = modComment.updater_id,
                                   UpdaterName = modComment.user1.last_name + ", " + modComment.user1.first_name,
                                   UpdateUsername = modComment.user1.tmo_userid,
                                   UpdateAt = modComment.updated_at,
                                   SiteID = tower.site_uid,
                                   SiteName = tower.site != null ? tower.site.site_name : "",
                                   CustomerName = tower.customer.customer_name,
                                   Mode = "eTM"
                               }).OrderByDescending(Utility.GetDateField<AllComments>("CreateAt"));

                if (towerMod != null)
                {
                    returnResult = towerMod.ToList();
                }
                return returnResult;
            }
        }
        
    }
}