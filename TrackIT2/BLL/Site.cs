﻿using System;
using System.Collections.Generic;
using System.Linq;
using TrackIT2.DAL;
using TrackIT2.Objects;
using System.Transactions;
using System.Data;

namespace TrackIT2.BLL
{
	public static class Site
	{
 
		public static site Search(string siteUid)
		{

			site results = null;

			using (var ce = new CPTTEntities())
			{
            if (ce.sites.Any(site => site.site_uid.Equals(siteUid)))
            {
               results = ce.sites.First(site => site.site_uid.Equals(siteUid));
            }
			}

			return results;
		}

      public static site SearchWithinTransaction(string siteUid, CPTTEntities ceRef)
      {

         site ret = null;

         if (ceRef.sites.Any(site => site.site_uid.Equals(siteUid)))
         {
            ret = ceRef.sites.First(site => site.site_uid.Equals(siteUid));
         }         

         return ret;
      }

      public static bool HasTowerModifications(string siteUid)
      {
         var results = false;

         using (var ce = new CPTTEntities())
         {
            if (ce.tower_modifications.Any(tower => tower.site_uid.Equals(siteUid)))
            {
               results = true;
            }            
         }

         return results;
      }

      public static bool HasIssueTracker(string siteUid)
      {
         var results = false;

         using (var ce = new CPTTEntities())
         {
             if (ce.issue_tracker.Any(issue => issue.site_uid.Equals(siteUid)))
             {
                results = true;
             }             
         }

         return results;
      }

      public static bool IsNotOnAir(DateTime? site_on_air_date, string site_status_desc)
      {
         bool results = false;

          if ((!site_on_air_date.HasValue || site_on_air_date.Value > DateTime.Now) && site_status_desc != "ON-AIR")
          {
              results = true;
          }

         return results;
      }

      // All site search criteria in a dictionary for easy lookup/manipulation
      // when dynamically building the query.
      public static Dictionary<string, SearchParameter> GetSearchCriteria()
      {
         var results = new Dictionary<string, SearchParameter>
            {
                {
                  "General", new SearchParameter
                     {                        
                        VariableName = "General",
                        ParameterType = typeof(string),
                        WhereClause = "("+
                                      "    (it.site_uid LIKE {0})"+
                                      "    OR" + 
                                      "    (it.site_name LIKE {0})"+
                                      "    OR"+
                                      "    (EXISTS(SELECT VALUE leaseApp FROM [CPTTEntities].[lease_applications] AS leaseApp " +
                                           "WHERE leaseApp.site_uid = it.site_uid AND (leaseApp.customer_site_uid LIKE {0} OR leaseApp.customer_site_name LIKE {0})))"+
                                      ")",
                        IsWildcardSearch = true
                     }
               },
               {
                  "SiteID", new SearchParameter
                     {                        
                        VariableName = "site_uid",
                        ParameterType = typeof(string),
                        WhereClause = "(it.site_uid LIKE {0})",
                        IsWildcardSearch = true
                     }
               },
               {
                  "SiteName", new SearchParameter
                     {                        
                        VariableName = "site_name",
                        ParameterType = typeof(string),
                        WhereClause = "(it.site_name LIKE {0})",
                        IsWildcardSearch = true
                     }
               },
               {
                  "MarketName", new SearchParameter
                     {
                        VariableName = "market_name",
                        ParameterType = typeof(string),
                        WhereClause = "(it.market_name = {0})",
                        AllowsMultipleValues = true
                     }
               },
               {
                  "SiteClassDesc", new SearchParameter
                     {
                        VariableName = "site_class_desc",
                        ParameterType = typeof(string),
                        WhereClause = "(it.site_class_desc = {0})",
                        AllowsMultipleValues = true
                     }
               },
               {
                  "SiteStatusDesc", new SearchParameter
                     {
                        VariableName = "site_status_desc",
                        ParameterType = typeof(string),
                        WhereClause = "(it.site_class_desc = {0})",
                        AllowsMultipleValues = true
                     }
               }
               ,
               {
                  "Address", new SearchParameter
                     {
                        VariableName = "address",
                        ParameterType = typeof(string),
                        WhereClause = "(it.address LIKE {0})",
                        IsWildcardSearch = true
                     }
               }
               ,
               {
                  "City", new SearchParameter
                     {
                        VariableName = "city",
                        ParameterType = typeof(string),
                        WhereClause = "(it.city LIKE {0})",
                        IsWildcardSearch = true
                     }
               }
               ,
               {
                  "State", new SearchParameter
                     {
                        VariableName = "state",
                        ParameterType = typeof(string),
                        WhereClause = "(it.state = {0})",
                        AllowsMultipleValues = true,
                     }
               }
               ,
               {
                  "Zip", new SearchParameter
                     {
                        VariableName = "zip",
                        ParameterType = typeof(string),
                        WhereClause = "(it.zip LIKE {0})",
                        IsWildcardSearch = true
                     }
               }
               ,
               {
                  "County", new SearchParameter
                     {
                        VariableName = "county",
                        ParameterType = typeof(string),
                        WhereClause = "(it.county LIKE {0})",
                        IsWildcardSearch = true
                     }
               }
            };

         return results;
      }
        
      public static string Add(site site, CPTTEntities ceRef = null)
      {
          String results = null;
          bool isSuccess = false;
          bool isRootTransaction = true;

          CPTTEntities ce;
          if (ceRef == null)
          {
              //using new entity object
              ce = new CPTTEntities();
          }
          else
          {
              //using current entity object
              ce = ceRef;
              isRootTransaction = false;
          }

          try
          {
              if (isRootTransaction)
              {
                  using (TransactionScope trans = TransactionUtils.CreateTransactionScope())
                  {
                      ce.Connection.Open();
                      ce.sites.AddObject(site);
                      ce.SaveChanges();
                      isSuccess = true;
                      trans.Complete();     // Transaction complete
                  }
              }
              else
              {
                  ce.sites.AddObject(site);
              }
          }
          catch (OptimisticConcurrencyException oex)
          {
              // Log error in ELMAH for any diagnostic needs.
              Elmah.ErrorSignal.FromCurrentContext().Raise(oex);

              results = "The current record has been modifed since you have " +
                        "last retrieved it. Please reload the record and " +
                        "attempt to save it again.";
          }
          catch (Exception ex)
          {
              // Error occurs
              // Log error in ELMAH for any diagnostic needs.
              Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

              isSuccess = false;
              results = "An error occurred saving the record. Please try again.";
          }
          finally
          {
              if (isRootTransaction)
              {
                  ce.Connection.Dispose();
              }
          }

          // Finally accept all changes from the transaction.
          if (isSuccess && isRootTransaction)
          {
              ce.Dispose();
          }

          return results;
      }

      public static string Update(site site, CPTTEntities ceRef = null)
      {
          String results = null;
          var isSuccess = false;
          var isRootTransaction = true;

          CPTTEntities ce;
          if (ceRef == null)
          {
              //using new entity object
              ce = new CPTTEntities();
          }
          else
          {
              //using current entity object
              ce = ceRef;
              isRootTransaction = false;
          }

          // Start transaction for update.
          try
          {
              using (var trans = TransactionUtils.CreateTransactionScope())
              {
                  if (isRootTransaction)
                  {
                      // Manually open connection to prevent EF from auto closing
                      // connection and causing issues with future queries.
                      ce.Connection.Open();
                  }

                  ce.AttachUpdated(site);

                  if (isRootTransaction)
                  {
                      // We tentatively save the changes so that we can finish 
                      // processing the transaction.
                      ce.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                      trans.Complete();     // Transaction complete
                  }

                  isSuccess = true;
              }
          }
          catch (OptimisticConcurrencyException oex)
          {
              // Log error in ELMAH for any diagnostic needs.
              Elmah.ErrorSignal.FromCurrentContext().Raise(oex);

              isSuccess = false;
              results = "The current record has been modifed since you have " +
                       "last retrieved it. Please reload the record and " +
                       "attempt to save it again.";
          }
          catch (Exception ex)
          {
              // Log error in ELMAH for any diagnostic needs.
              Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

              isSuccess = false;
              results = "An error occurred saving the record. Please try again.";
          }
          finally
          {
              if (isRootTransaction)
              {
                  ce.Connection.Dispose();
              }
          }

          // Finally accept all changes from the transaction.
          if (isSuccess && isRootTransaction)
          {
              ce.AcceptAllChanges();
              ce.Dispose();
          }

          return results;
      }

      public static string Remove(site site, CPTTEntities ceRef = null)
      {
          String results = null;
          bool isSuccess = false;
          bool isRootTransaction = true;

          CPTTEntities ce;
          if (ceRef == null)
          {
              //using new entity object
              ce = new CPTTEntities();
          }
          else
          {
              //using current entity object
              ce = ceRef;
              isRootTransaction = false;
          }

          try
          {
              if (isRootTransaction)
              {
                  using (TransactionScope trans = TransactionUtils.CreateTransactionScope())
                  {
                      ce.Connection.Open();
                      ce.sites.DeleteObject(site);
                      ce.SaveChanges();
                      isSuccess = true;
                      trans.Complete();     // Transaction complete
                  }
              }
              else
              {
                  site deleteSite = new DAL.site();
                  deleteSite = BLL.Site.SearchWithinTransaction(site.site_uid, ce);
                  ce.sites.DeleteObject(deleteSite);
              }
          }
          catch (OptimisticConcurrencyException oex)
          {
              // Log error in ELMAH for any diagnostic needs.
              Elmah.ErrorSignal.FromCurrentContext().Raise(oex);

              results = "The current record has been modifed since you have " +
                        "last retrieved it. Please reload the record and " +
                        "attempt to save it again.";
          }
          catch (Exception ex)
          {
              // Error occurs
              // Log error in ELMAH for any diagnostic needs.
              Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

              isSuccess = false;
              results = "An error occurred saving the record. Please try again.";
          }
          finally
          {
              if (isRootTransaction)
              {
                  ce.Connection.Dispose();
              }
          }

          // Finally accept all changes from the transaction.
          if (isSuccess && isRootTransaction)
          {
              ce.Dispose();
          }

          return results;
      }

      public static string GetStateShortName(string itemValue)
      {
          string returnResult = string.Empty;
          var allSelectState = itemValue.Split(',');

          foreach (var state in allSelectState)
          {
              if (state.Contains('-'))
              {
                  if (!string.IsNullOrEmpty(returnResult))
                  {
                      returnResult += ",";
                  }
                  returnResult += state.Split('-').FirstOrDefault();
              }
          }

          return returnResult;
      }
	}
}