﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TrackIT2.DAL;
using System.Transactions;

namespace TrackIT2.BLL
{
    public static class TowerModPhase
    {
        public static tower_mod_pos Search(int id)
        {
            tower_mod_pos ret = null;

            using (var ce = new CPTTEntities())
            {
                ret = ce.tower_mod_pos.First(c => c.id.Equals(id));
                if (ret != null) ce.Detach(ret);
            }

            return ret;
        }

        public static List<tower_mod_pos> GetTMP(int? tower_mod_id)
        {
           List<tower_mod_pos> listRet = new List<tower_mod_pos>();
            if (tower_mod_id.HasValue)
            {
                using (CPTTEntities context = new CPTTEntities())
                {
                   var query = from tmp in context.tower_mod_pos
                                where tmp.tower_modification_id == tower_mod_id.Value
                                select tmp;
                    listRet.AddRange(query.ToList<tower_mod_pos>());
                }
            }
            return listRet;
        }
        public static void Add(tower_mod_pos objTMP)
        {
           bool isSuccess;

           using (CPTTEntities ce = new CPTTEntities())
           {
              try
              {
                 using (TransactionScope trans = TransactionUtils.CreateTransactionScope())
                 {
                    // Manually open connection to prevent EF from auto closing
                    // connection and causing issues with future queries.
                    ce.Connection.Open();

                    ce.tower_mod_pos.AddObject(objTMP);
                    ce.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                    trans.Complete();
                    isSuccess = true;
                 }
              }
              catch (Exception)
              {
                 throw;
              }
              finally
              {
                 ce.Connection.Dispose();
              }

              if (isSuccess)
              {
                 ce.AcceptAllChanges();
              }
           }
        }

        public static String Delete(int id, CPTTEntities ceRef = null)
        {
           String sResult = null;
           bool isSuccess = false;
           bool isRootTransaction = true;

           CPTTEntities ce;
           if (ceRef == null)
           {
              //using new entity object
              ce = new CPTTEntities();
           }
           else
           {
              //using current entity object
              ce = ceRef;
              isRootTransaction = false;
           }
           try
           {
              using (TransactionScope trans = TransactionUtils.CreateTransactionScope())
              {
                 if (isRootTransaction)
                 {
                    // Manually open connection to prevent EF from auto closing
                    // connection and causing issues with future queries.
                    ce.Connection.Open();
                 }

                 //Delete record
                 var queryTMPO = from tmpo in ce.tower_mod_pos
                                 where tmpo.id == id
                                 select tmpo;

                 if (queryTMPO.Count() > 0)
                 {
                    tower_mod_pos item = queryTMPO.First();
                    ce.DeleteObject(item);
                 }
                 else
                 {
                    throw new NullReferenceException();
                 }

                 if (isRootTransaction)
                 {
                    // We tentatively save the changes so that we can finish 
                    // processing the transaction.
                    ce.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                 }

                 isSuccess = true;
                 trans.Complete();     //Transaction complete.
              }
           }
           catch (Exception ex)
           {
              // Log error in ELMAH for any diagnostic needs.
              Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

              sResult = ex.ToString();
              isSuccess = false;
           }
           finally
           {
              if (isRootTransaction)
              {
                 ce.Connection.Dispose();
              }
           }

           // Finally accept all changes from the transaction.
           if (isSuccess && isRootTransaction)
           {
              ce.AcceptAllChanges();
              ce.Dispose();
           }

           return sResult;
        }

        public static int GetJSON(Boolean isNull, out String sJSON, int iAppID = 0)
        {
            int iRet = 0;
            String sRet = "";
            using (CPTTEntities ce = new CPTTEntities())
            {
                if (!isNull && iAppID != 0)
                {
                    var query = from tmp in
                                (from tower in ce.tower_mod_pos
                                 where tower.tower_modification_id == iAppID
                                 select tower).ToList()
                                select new json_tower_mod_pos
                                {
                                    //TODO: uncomment when fields are added.
                                    //tax = tmp.tax
                                    ID = tmp.id,
                                    tower_mod_phase_type_id = tmp.tower_mod_phase_type_id,
                                    ebid_req_rcvd_date = Utility.DateToString(tmp.ebid_req_rcvd_date),
                                    ebid_packet_ready_date = Utility.DateToString(tmp.ebid_packet_ready_date),
                                    ebid_scheduled_published_date = Utility.DateToString(tmp.ebid_scheduled_published_date),
                                    ebid_scheduled_close_date = Utility.DateToString(tmp.ebid_scheduled_close_date),
                                    ebid_maturity_date = Utility.DateToString(tmp.ebid_maturity_date),
                                    ebid_comment = Utility.PrepareString(tmp.ebid_comment),
                                    estimated_construction_cost_amount = tmp.estimated_construction_cost_amount,
                                    bid_amount = tmp.bid_amount,
                                    markup_amount = tmp.markup_amount,
                                    total_from_customer_amount = tmp.total_from_customer_amount,
                                    general_contractor_name = (tmp.general_contractor_id.HasValue ? Utility.PrepareString(tmp.general_contractors.general_contractor_name) : ""),
                                    sent_to_cs_pm_date = Utility.DateToString(tmp.sent_to_cs_pm_date),
                                    po_to_gc_requested_date = Utility.DateToString(tmp.po_to_gc_requested_date),
                                    shopping_cart_nbr = Utility.PrepareString(tmp.shopping_cart_nbr),
                                    po_to_gc_rcvd_date = Utility.DateToString(tmp.po_to_gc_rcvd_date),
                                    po_nbr = Utility.PrepareString(tmp.po_nbr),
                                    tax_amount = tmp.tax_amount
                                };
                    if (query.ToList().Count > 0)
                    {
                        sRet = Newtonsoft.Json.JsonConvert.SerializeObject(query);
                        iRet = query.ToList().Count;
                    }
                }
                else
                {
                    var query = new
                    {
                        ID = 0,
                        tower_mod_phase_type_id = (object)null,
                        ebid_req_rcvd_date = (object)null,
                        ebid_packet_ready_date = (object)null,
                        ebid_scheduled_published_date = (object)null,
                        ebid_scheduled_close_date = (object)null,
                        ebid_maturity_date = (object)null,
                        ebid_comment = (object)null,
                        estimated_construction_cost_amount = (object)null,
                        bid_amount = (object)null,
                        markup_amount = (object)null,
                        total_from_customer_amount = (object)null,
                        general_contractor_name = (object)null,
                        sent_to_cs_pm_date = (object)null,
                        po_to_gc_requested_date = (object)null,
                        shopping_cart_nbr = (object)null,
                        po_to_gc_rcvd_date = (object)null,
                        po_nbr = (object)null,
                        tax_amount = (object)null
                    };
                    if (query != null)
                    {
                        sRet = Newtonsoft.Json.JsonConvert.SerializeObject(query);
                        iRet = 1;
                    }
                }
            }
            sJSON = sRet;
            return iRet;
        }

        public static List<TowerModPOTemplate> ParseFromJSON(String sJSON)
        {
            List<TowerModPOTemplate> lstResult = new List<TowerModPOTemplate>();
            if (sJSON != null && sJSON != "")
            {
                String JSON_RESULT_FORMAT = "\"result\" : {0}";
                String sTestJSON = "{" + String.Format(JSON_RESULT_FORMAT, sJSON) + "}";


                TowerModPOResult JSONResult = Newtonsoft.Json.JsonConvert.DeserializeObject<TowerModPOResult>(sTestJSON);
                lstResult = JSONResult.result;
            }
            
            return lstResult;
        }
    }

    [Serializable]
    public class TowerModPOResult
    {
        public List<TowerModPOTemplate> result { get; set; }
    }
    public class TowerModPOTemplate
    {
        public String ID { get; set; }
        private String m_tower_mod_phase_type_id;
        public String tower_mod_phase_type_id
        {
            get
            {
                return m_tower_mod_phase_type_id;
            }
            set
            {
                CheckIsNull(value);
                m_tower_mod_phase_type_id = value;
            }
        }
        private String m_ebid_req_rcvd_date;
        public String ebid_req_rcvd_date
        {
            get
            {
                return m_ebid_req_rcvd_date;
            }
            set
            {
                CheckIsNull(value);
                m_ebid_req_rcvd_date = value;
            }
        }
        private String m_ebid_packet_ready_date;
        public String ebid_packet_ready_date
        {
            get
            {
                return m_ebid_packet_ready_date;
            }
            set
            {
                CheckIsNull(value);
                m_ebid_packet_ready_date = value;
            }
        }
        private String m_ebid_scheduled_published_date;
        public String ebid_scheduled_published_date
        {
            get
            {
                return m_ebid_scheduled_published_date;
            }
            set
            {
                CheckIsNull(value);
                m_ebid_scheduled_published_date = value;
            }
        }
        private String m_ebid_scheduled_close_date;
        public String ebid_scheduled_close_date
        {
            get
            {
                return m_ebid_scheduled_close_date;
            }
            set
            {
                CheckIsNull(value);
                m_ebid_scheduled_close_date = value;
            }
        }
        private String m_ebid_maturity_date;
        public String ebid_maturity_date
        {
            get
            {
                return m_ebid_maturity_date;
            }
            set
            {
                CheckIsNull(value);
                m_ebid_maturity_date = value;
            }
        }
        private String m_ebid_comment;
        public String ebid_comment
        {
            get
            {
                return m_ebid_comment;
            }
            set
            {
                CheckIsNull(value);
                m_ebid_comment = value;
            }
        }
        private String m_estimated_construction_cost_amount;
        public String estimated_construction_cost_amount
        {
            get
            {
                return m_estimated_construction_cost_amount;
            }
            set
            {
                CheckIsNull(value);
                m_estimated_construction_cost_amount = value;
            }
        }
        private String m_bid_amount;
        public String bid_amount
        {
            get
            {
                return m_bid_amount;
            }
            set
            {
                CheckIsNull(value);
                m_bid_amount = value;
            }
        }
        private String m_markup_amount;
        public String markup_amount
        {
            get
            {
                return m_markup_amount;
            }
            set
            {
                CheckIsNull(value);
                m_markup_amount = value;
            }
        }
        private String m_total_from_customer_amount;
        public String total_from_customer_amount
        {
            get
            {
                return m_total_from_customer_amount;
            }
            set
            {
                CheckIsNull(value);
                m_total_from_customer_amount = value;
            }
        }
        private String m_general_contractor_name;
        public String general_contractor_name
        {
            get
            {
                return m_general_contractor_name;
            }
            set
            {
                CheckIsNull(value);
                m_general_contractor_name = value;
            }
        }
        private String m_sent_to_cs_pm_date;
        public String sent_to_cs_pm_date
        {
            get
            {
                return m_sent_to_cs_pm_date;
            }
            set
            {
                CheckIsNull(value);
                m_sent_to_cs_pm_date = value;
            }
        }

        private String m_po_to_gc_requested_date;
        public String po_to_gc_requested_date
        {
            get
            {
                return m_po_to_gc_requested_date;
            }
            set
            {
                CheckIsNull(value);
                m_po_to_gc_requested_date = value;
            }
        }
        private String m_shopping_cart_nbr;
        public String shopping_cart_nbr
        {
            get
            {
                return m_shopping_cart_nbr;
            }
            set
            {
                CheckIsNull(value);
                m_shopping_cart_nbr = value;
            }
        }
        private String m_po_to_gc_rcvd_date;
        public String po_to_gc_rcvd_date
        {
            get
            {
                return m_po_to_gc_rcvd_date;
            }
            set
            {
                CheckIsNull(value);
                m_po_to_gc_rcvd_date = value;
            }
        }
        private String m_po_nbr;
        public String po_nbr
        {
            get
            {
                return m_po_nbr;
            }
            set
            {
                CheckIsNull(value);
                m_po_nbr = value;
            }
        }
        private String m_tax_amount;
        public String tax_amount
        {
            get
            {
                return m_tax_amount;
            }
            set
            {
                CheckIsNull(value);
                m_tax_amount = value;
            }
        }

        private Boolean m_IsNull = true;
        private void CheckIsNull(String sValue)
        {
            if (sValue != null && sValue != "")
            {
                m_IsNull = false;
            }
        }
        public Boolean CheckIsNull()
        {
            return m_IsNull & (ID == "0");
        }
    }
    public class json_tower_mod_pos
    { 
        public int? ID {set;get;}
        public int? tower_mod_phase_type_id  {set;get;}
        public string ebid_req_rcvd_date  {set;get;}
        public string ebid_packet_ready_date  {set;get;}
        public string ebid_scheduled_published_date  {set;get;}
        public string ebid_scheduled_close_date {set;get;}
        public string ebid_maturity_date {set;get;}
        public string ebid_comment {set;get;}
        public Decimal? estimated_construction_cost_amount {set;get;}
        public Decimal? bid_amount {set;get;}
        public Decimal? markup_amount {set;get;}
        public Decimal? total_from_customer_amount {set;get;}
        public string general_contractor_name {set;get;}
        public string sent_to_cs_pm_date {set;get;}
        public string po_to_gc_requested_date {set;get;}
        public string shopping_cart_nbr {set;get;}
        public string po_to_gc_rcvd_date {set;get;}
        public string po_nbr { set; get; }
        public Decimal? tax_amount {set;get;}
    }
}