﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using TrackIT2.BLL;
using TrackIT2.DAL;
using TrackIT2.Objects;
using System.Collections;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Security.Cryptography;

namespace TrackIT2.BLL
{
    public static class HMACHelper
    {
        private static String HMACKey = ConfigurationManager.AppSettings["HMACKey"];
        private static String HMACAlgorithm = ConfigurationManager.AppSettings["HMACAlgorithm"];

        public enum eAlgorithm
        {
            eHMACMD5,
            eHMACSHA1,
            eHMACSHA256,
            eHMACSHA384,
            eHMACSHA512
        }
        
        public static byte[] DoHMAC(eAlgorithm emode)
        {
            UTF8Encoding encoding = new System.Text.UTF8Encoding();
            byte[] keyBytes = encoding.GetBytes(HMACKey);
            HMACMD5 hmacmd5 = new HMACMD5(keyBytes);
            HMACSHA1 hmacsha1 = new HMACSHA1(keyBytes);
            HMACSHA256 hmacsha256 = new HMACSHA256(keyBytes);
            HMACSHA384 hmacsha384 = new HMACSHA384(keyBytes);
            HMACSHA512 hmacsha512 = new HMACSHA512(keyBytes);
            byte[] hashmessage;
            String result = String.Empty;
            switch (HMACAlgorithm)
            {
                case "MD5":
                    hmacmd5.ComputeHash(keyBytes);
                    result = ByteToString(hmacmd5.Hash);
                    hashmessage = hmacmd5.Hash;
                    break;
                case "SHA1":
                    hmacsha1.ComputeHash(keyBytes);
                    result = ByteToString(hmacsha1.Hash);
                    hashmessage = hmacsha1.Hash;
                    break;
                case "SHA256":
                    hmacsha256.ComputeHash(keyBytes);
                    result = ByteToString(hmacsha256.Hash);
                    hashmessage = hmacsha256.Hash;
                    break;
                case "SHA384":
                    hmacsha384.ComputeHash(keyBytes);
                    result = ByteToString(hmacsha384.Hash);
                    hashmessage = hmacsha384.Hash;
                    break;
                case "SHA512":
                    hmacsha512.ComputeHash(keyBytes);
                    result = ByteToString(hmacsha512.Hash);
                    hashmessage = hmacsha512.Hash;
                    break;
                default:
                    hmacsha1.ComputeHash(keyBytes);
                    result = ByteToString(hmacsha1.Hash);
                    hashmessage = hmacsha1.Hash;
                    break;
            }
            return hashmessage;
        }

        private static eAlgorithm SetAlgorithm()
        {
            eAlgorithm mode;
            switch (HMACAlgorithm)
            {
                case "MD5":
                    mode = eAlgorithm.eHMACMD5;
                    break;
                case "SHA1":
                    mode = eAlgorithm.eHMACSHA1;
                    break;
                case "SHA256":
                    mode = eAlgorithm.eHMACSHA256;
                    break;
                case "SHA384":
                    mode = eAlgorithm.eHMACSHA384;
                    break;
                case "SHA512":
                    mode = eAlgorithm.eHMACSHA512;
                    break;
                default:
                    mode = eAlgorithm.eHMACSHA1;
                    break;
            }
            return mode;
        }

        public static byte[] GenerateHMAC()
        {
            return DoHMAC(SetAlgorithm());
        }

        public static bool HMAC_Authen(byte[] signature)
        {
            string res = ByteToString(DoHMAC(SetAlgorithm()));
            string sig = ByteToString(signature);
            if (res == sig)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static String ByteToString(byte[] buff)
        {
            String sBinary = string.Empty;
            int i = 0;
            for (i = 0; i < buff.Length; i++)
            {
                sBinary += buff[i].ToString("X2");
            }
            return sBinary;
        }
    }
}