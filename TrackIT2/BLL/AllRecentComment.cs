﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TrackIT2.DAL;
using System.Threading;
using System.ComponentModel;


namespace TrackIT2.BLL
{
    public class AllRecentComment
    {
        private static EventWaitHandle MainThread;
        static List<Boolean> IsAllComplete = new List<bool>();

        static List<LatestRecentComment> AllRecentCommentList = new List<LatestRecentComment>(10);

        //Self event
        public delegate void Load_Data_OnComplete_Handler();

        private void AllRecentComment_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            AllRecentComment_Complete(sender, e);
        }

        private void LeaseAppAndLeaseAdminComment_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            LeaseAppAndLeaseAdminComment_Complete(sender, e);
        }

        private static void AllRecentComment_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            int[] args = (int[])e.Argument;
            
            try
            {
                switch (args[0])
                {
                    case 0:
                        AllRecentCommentList.AddRange(GetLatestLeaseAppComments(args[1]));
                        break;
                    case 1:
                        AllRecentCommentList.AddRange(GetLatestStructuralAnalysisComment(args[1]));
                        break;
                    case 2:
                        AllRecentCommentList.AddRange(GetLatestTowerModComments(args[1]));
                        break;
                    case 3:
                        AllRecentCommentList.AddRange(GetLastestLeaseAdminComment(args[1]));
                        break;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(null).Log(new Elmah.Error(ex));
            }
        }

        private static void LeaseAppAndLeaseAdminComment_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            int[] args = (int[])e.Argument;

            try
            {
                switch (args[0])
                {
                    case 0:
                        AllRecentCommentList.AddRange(GetLatestLeaseAppComments(args[1]));
                        break;
                    case 1:
                        AllRecentCommentList.AddRange(GetLastestLeaseAdminComment(args[1]));
                        break;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(null).Log(new Elmah.Error(ex));
            }
        }

        public static void AllRecentComment_Complete(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            ThreadUtils.HandleThreadDone(ref MainThread, 4, ref IsAllComplete);
        }

        public static void LeaseAppAndLeaseAdminComment_Complete(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            ThreadUtils.HandleThreadDone(ref MainThread, 2, ref IsAllComplete);
        }

        public static List<LatestRecentComment> GetLatestRecentComment(int userId)
        {
            MainThread = new EventWaitHandle(false, EventResetMode.AutoReset);
            AllRecentCommentList.Clear();
            Thread t;

            for (int x = 0; x < 4; x++)
            {
                switch (x)
                {
                    case 0:
                        t = new Thread(
                            delegate()
                            {
                                BackgroundWorker _objWorker = new BackgroundWorker();
                                _objWorker.DoWork += new DoWorkEventHandler(AllRecentComment_DoWork);
                                _objWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(AllRecentComment_Complete);
                                int[] args = { 0, userId };
                                _objWorker.RunWorkerAsync(args);
                            });
                        t.Start();

                        break;
                    case 1:
                        t = new Thread(
                            delegate()
                            {
                                BackgroundWorker _objWorker = new BackgroundWorker();
                                _objWorker.DoWork += new DoWorkEventHandler(AllRecentComment_DoWork);
                                _objWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(AllRecentComment_Complete);
                                int[] args = { 1, userId };
                                _objWorker.RunWorkerAsync(args);
                            });
                        t.Start();

                        break;
                    case 2:
                        t = new Thread(
                            delegate()
                            {
                                BackgroundWorker _objWorker = new BackgroundWorker();
                                _objWorker.DoWork += new DoWorkEventHandler(AllRecentComment_DoWork);
                                _objWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(AllRecentComment_Complete);
                                int[] args = { 2, userId };
                                _objWorker.RunWorkerAsync(args);
                            });
                        t.Start();

                        break;
                    case 3:
                        t = new Thread(
                            delegate()
                            {
                                BackgroundWorker _objWorker = new BackgroundWorker();
                                _objWorker.DoWork += new DoWorkEventHandler(AllRecentComment_DoWork);
                                _objWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(AllRecentComment_Complete);
                                int[] args = { 3, userId };
                                _objWorker.RunWorkerAsync(args);
                            });
                        t.Start();

                        break;
                }
            }

            MainThread.WaitOne(180000);

            var tmp = (from all in AllRecentCommentList orderby all.Timestamp.Value descending select all).Take(10);
            return tmp.ToList<LatestRecentComment>();
        }

        public static List<LatestRecentComment> GetLatestLLeaseAppAndLeaseAdminComment(int currentUserID)
        {
            List<LatestRecentComment> recentComment = new List<LatestRecentComment>();
            MainThread = new EventWaitHandle(false, EventResetMode.AutoReset);
            Thread t;
            AllRecentCommentList.Clear();

            for (int x = 0; x < 2; x++)
            {
                switch (x)
                {
                    case 0:
                        t = new Thread(
                            delegate()
                            {
                                BackgroundWorker _objWorker = new BackgroundWorker();
                                _objWorker.DoWork += new DoWorkEventHandler(LeaseAppAndLeaseAdminComment_DoWork);
                                _objWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(LeaseAppAndLeaseAdminComment_Complete);
                                int[] args = { 0, currentUserID };
                                _objWorker.RunWorkerAsync(args);
                            });
                        t.Start();

                        break;
                    case 1:
                        t = new Thread(
                            delegate()
                            {
                                BackgroundWorker _objWorker = new BackgroundWorker();
                                _objWorker.DoWork += new DoWorkEventHandler(LeaseAppAndLeaseAdminComment_DoWork);
                                _objWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(LeaseAppAndLeaseAdminComment_Complete);
                                int[] args = { 1, currentUserID };
                                _objWorker.RunWorkerAsync(args);
                            });
                        t.Start();

                        break;
                }
            }

            MainThread.WaitOne();

            var tmpComment = (from row in AllRecentCommentList
                              orderby row.CreateAt.Value descending
                              select row).ToList().Take(10);

            recentComment = tmpComment.ToList<LatestRecentComment>();
            return recentComment;
        }

        public static List<LatestRecentComment> GetLatestLeaseAppComments(int currentUserID)
        {
            CPTTEntities ce = new CPTTEntities();
            List<LatestRecentComment> lepComment = new List<LatestRecentComment>();
            try
            {
                if (currentUserID != 0)
                {
                    var leaseAppComment = (from la in ce.leaseapp_comments
                                           join lp in ce.lease_applications on la.lease_application_id equals lp.id
                                           join lt in ce.leaseapp_types on lp.leaseapp_type_id equals lt.id
                                           where la.updated_at.Value != null && la.created_at.Value != null
                                           && (lp.tmo_specialist_initial_emp_id == currentUserID
                                           || lp.tmo_specialist_current_emp_id == currentUserID
                                           || lp.tmo_pm_emp_id == currentUserID
                                           || lp.am_emp_id == currentUserID
                                           || lp.colocation_coordinator_emp_id == currentUserID)
                                           select new LatestRecentComment
                                           {
                                               CommentID = la.id,
                                               LeaseApplicationID = la.lease_application_id,
                                               StructuralAnalysisID = null,
                                               Comments = la.comments,
                                               CreatorID = la.creator_id,
                                               CreatorName = la.creator_user.last_name + ", " + la.creator_user.first_name,
                                               Timestamp = la.updated_at,
                                               CreatorUsername = la.creator_user.tmo_userid,
                                               UpdaterID = la.updater_id,
                                               UpdaterName = la.updater_user.last_name + ", " + la.updater_user.first_name,
                                               UpdateUsername = la.updater_user.tmo_userid,
                                               UpdateAt = la.updated_at,
                                               SiteID = la.lease_applications.site_uid,
                                               SiteName = la.lease_applications.site.site_name,
                                               CustomerName = la.lease_applications.customer.customer_name,
                                               AppType = lt.lease_application_type,
                                               TypeID = lt.id,
                                               TowerModID = null,
                                               CreateAt = la.created_at,
                                               LeaseAdmin = null
                                           });

                    var tmpComment = (from row in leaseAppComment
                                      orderby row.CreateAt.Value descending
                                      select row).Take(10).ToList();
                    lepComment = tmpComment.ToList<LatestRecentComment>();
                }
                else
                {
                    var leaseAppComment = (from la in ce.leaseapp_comments
                                           join lp in ce.lease_applications on la.lease_application_id equals lp.id
                                           join lt in ce.leaseapp_types on lp.leaseapp_type_id equals lt.id
                                           where la.updated_at.Value != null && la.created_at.Value != null
                                           select new LatestRecentComment
                                           {
                                               CommentID = la.id,
                                               LeaseApplicationID = la.lease_application_id,
                                               StructuralAnalysisID = null,
                                               Comments = la.comments,
                                               CreatorID = la.creator_id,
                                               CreatorName = la.creator_user.last_name + ", " + la.creator_user.first_name,
                                               Timestamp = la.updated_at,
                                               CreatorUsername = la.creator_user.tmo_userid,
                                               UpdaterID = la.updater_id,
                                               UpdaterName = la.updater_user.last_name + ", " + la.updater_user.first_name,
                                               UpdateUsername = la.updater_user.tmo_userid,
                                               UpdateAt = la.updated_at,
                                               SiteID = la.lease_applications.site_uid,
                                               SiteName = la.lease_applications.site.site_name,
                                               CustomerName = la.lease_applications.customer.customer_name,
                                               AppType = lt.lease_application_type,
                                               TypeID = lt.id,
                                               TowerModID = null,
                                               LeaseAdmin = null,
                                               CreateAt = la.created_at
                                           });

                    var tmpComment = (from row in leaseAppComment
                                      orderby row.CreateAt.Value descending
                                      select row).ToList().Take(10);
                    lepComment = tmpComment.ToList<LatestRecentComment>();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(null).Log(new Elmah.Error(ex));
            }

            return lepComment;
        }
        
        public static List<LatestRecentComment> GetLastestLeaseAdminComment(int currentUserID)
        {
            CPTTEntities ce = new CPTTEntities();
            List<LatestRecentComment> leAdminComment = new List<LatestRecentComment>();
            try
            {
                if (currentUserID != 0)
                {
                    var leaseApp = from leaseAddminComment in ce.lease_admin_comments
                                   join leaseAddminRecord in ce.lease_admin_records on leaseAddminComment.lease_admin_record_id equals leaseAddminRecord.id
                                   join lp in ce.lease_applications on leaseAddminRecord.lease_application_id equals lp.id
                                   where leaseAddminComment.updated_at.Value != null && leaseAddminComment.created_at.Value != null
                                    && (lp.tmo_specialist_initial_emp_id == currentUserID
                                           || lp.tmo_specialist_current_emp_id == currentUserID
                                           || lp.tmo_pm_emp_id == currentUserID
                                           || lp.am_emp_id == currentUserID
                                           || lp.colocation_coordinator_emp_id == currentUserID
                                           || leaseAddminRecord.document_received_by_emp_id == currentUserID)
                                   select new LatestRecentComment
                                   {
                                       CommentID = leaseAddminComment.id,
                                       LeaseApplicationID = leaseAddminRecord.lease_application_id,
                                       StructuralAnalysisID = null,
                                       Comments = leaseAddminComment.comments,
                                       CreatorID = leaseAddminComment.creator_id,
                                       CreatorName = leaseAddminComment.creator_user.last_name + ", " + leaseAddminComment.creator_user.first_name,
                                       Timestamp = leaseAddminComment.updated_at,
                                       CreatorUsername = leaseAddminComment.creator_user.tmo_userid,
                                       UpdaterID = leaseAddminComment.updater_id,
                                       UpdaterName = leaseAddminComment.updater_user.last_name + ", " + leaseAddminComment.updater_user.first_name,
                                       UpdateUsername = leaseAddminComment.updater_user.tmo_userid,
                                       UpdateAt = leaseAddminComment.updated_at,
                                       SiteID = leaseAddminRecord.lease_application_id == null ? "Various" : leaseAddminRecord.lease_applications.site.site_uid,
                                       SiteName = leaseAddminRecord.lease_application_id == null ? "Various" : leaseAddminRecord.lease_applications.site.site_name,
                                       CustomerName = leaseAddminRecord.lease_application_id == null ? "Various" : leaseAddminRecord.lease_applications.customer.customer_name,
                                       AppType = leaseAddminRecord.lease_application_id == null ? "Various" : leaseAddminRecord.lease_applications.leaseapp_types.lease_application_type,
                                       TypeID = leaseAddminRecord.lease_application_id == null ? null : leaseAddminRecord.lease_applications.leaseapp_type_id,
                                       TowerModID = null,
                                       LeaseAdmin = leaseAddminComment.lease_admin_record_id,
                                       CreateAt = leaseAddminComment.created_at

                                   };

                    var tmpComment = (from row in leaseApp
                                      orderby row.CreateAt.Value descending
                                      select row).Take(10).ToList();
                    leAdminComment = tmpComment.ToList<LatestRecentComment>();
                }
                else
                {
                    var leaseApp = from leaseAddminComment in ce.lease_admin_comments
                                   join leaseAddminRecord in ce.lease_admin_records on leaseAddminComment.lease_admin_record_id equals leaseAddminRecord.id
                                   where leaseAddminComment.updated_at.Value != null && leaseAddminComment.created_at.Value != null
                                   select new LatestRecentComment
                                   {
                                       CommentID = leaseAddminComment.id,
                                       LeaseApplicationID = leaseAddminRecord.lease_application_id,
                                       StructuralAnalysisID = null,
                                       Comments = leaseAddminComment.comments,
                                       CreatorID = leaseAddminComment.creator_id,
                                       CreatorName = leaseAddminComment.creator_user.last_name + ", " + leaseAddminComment.creator_user.first_name,
                                       Timestamp = leaseAddminComment.updated_at,
                                       CreatorUsername = leaseAddminComment.creator_user.tmo_userid,
                                       UpdaterID = leaseAddminComment.updater_id,
                                       UpdaterName = leaseAddminComment.updater_user.last_name + ", " + leaseAddminComment.updater_user.first_name,
                                       UpdateUsername = leaseAddminComment.updater_user.tmo_userid,
                                       UpdateAt = leaseAddminComment.updated_at,
                                       SiteID = leaseAddminRecord.lease_application_id == null ? "Various" : leaseAddminRecord.lease_applications.site.site_uid,
                                       SiteName = leaseAddminRecord.lease_application_id == null ? "Various" : leaseAddminRecord.lease_applications.site.site_name,
                                       CustomerName = leaseAddminRecord.lease_application_id == null ? "Various" : leaseAddminRecord.lease_applications.customer.customer_name,
                                       AppType = leaseAddminRecord.lease_application_id == null ? "Various" : leaseAddminRecord.lease_applications.leaseapp_types.lease_application_type,
                                       TypeID = leaseAddminRecord.lease_application_id == null ? null : leaseAddminRecord.lease_applications.leaseapp_type_id,
                                       TowerModID = null,
                                       LeaseAdmin = leaseAddminComment.lease_admin_record_id,
                                       CreateAt = leaseAddminComment.created_at

                                   };

                    var tmpComment = (from row in leaseApp
                                      orderby row.CreateAt.Value descending
                                      select row).ToList().Take(10);
                    leAdminComment = tmpComment.ToList<LatestRecentComment>();

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(null).Log(new Elmah.Error(ex));
            }

            return leAdminComment;
        }

        public static List<LatestRecentComment> GetLatestTowerModComments(int currentUserID)
        {
            CPTTEntities ce = new CPTTEntities();
            List<LatestRecentComment> toComment = new List<LatestRecentComment>();
            try
            {
                if (currentUserID != 0)
                {
                    var towerModComment = (from toCom in ce.tower_mod_comments
                                           join toData in ce.tower_modifications on toCom.tower_modification_id equals toData.id
                                           join siteData in ce.sites on toData.site_uid equals siteData.site_uid
                                           join lp in ce.lease_applications on siteData.site_uid equals lp.site_uid
                                           join userData in ce.users on toCom.creator_id equals userData.id
                                           where toCom.updated_at.Value != null && toCom.created_at.Value != null
                                           && (lp.tmo_specialist_initial_emp_id == currentUserID
                                           || lp.tmo_specialist_current_emp_id == currentUserID
                                           || lp.tmo_pm_emp_id == currentUserID
                                           || lp.am_emp_id == currentUserID
                                           || lp.colocation_coordinator_emp_id == currentUserID)
                                           select new LatestRecentComment
                                           {
                                               CommentID = toCom.id,
                                               StructuralAnalysisID = null,
                                               LeaseApplicationID = null,
                                               Comments = toCom.comment,
                                               CreatorID = toCom.creator_id,
                                               CreatorName = userData.last_name + ", " + userData.first_name,
                                               Timestamp = toCom.created_at,
                                               CreatorUsername = userData.tmo_userid,
                                               UpdaterID = null,
                                               UpdaterName = null,
                                               UpdateUsername = null,
                                               UpdateAt = toCom.updated_at,
                                               SiteID = siteData.site_uid,
                                               SiteName = siteData.site_name,
                                               CustomerName = toData.customer.customer_name,
                                               AppType = toData.tower_mod_type_id != null ? (from z in ce.tower_mod_types where toData.tower_mod_type_id == z.id select z.tower_mod_type).FirstOrDefault() : null,
                                               TypeID = toData.tower_mod_type_id,
                                               TowerModID = toData.id,
                                               CreateAt = toCom.created_at
                                           });

                    var tmpComment = (from row in towerModComment
                                      orderby row.CreateAt.Value descending
                                      select row).Take(10).ToList();
                    toComment = tmpComment.ToList<LatestRecentComment>();
                }
                else
                {
                    var towerModComment = (from toCom in ce.tower_mod_comments
                                           join toData in ce.tower_modifications on toCom.tower_modification_id equals toData.id
                                           join siteData in ce.sites on toData.site_uid equals siteData.site_uid
                                           join userData in ce.users on toCom.creator_id equals userData.id
                                           where toCom.updated_at.Value != null && toCom.created_at.Value != null
                                           select new LatestRecentComment
                                           {
                                               CommentID = toCom.id,
                                               StructuralAnalysisID = null,
                                               LeaseApplicationID = null,
                                               Comments = toCom.comment,
                                               CreatorID = toCom.creator_id,
                                               CreatorName = userData.last_name + ", " + userData.first_name,
                                               Timestamp = toCom.created_at,
                                               CreatorUsername = userData.tmo_userid,
                                               UpdaterID = null,
                                               UpdaterName = null,
                                               UpdateUsername = null,
                                               UpdateAt = toCom.updated_at,
                                               SiteID = siteData.site_uid,
                                               SiteName = siteData.site_name,
                                               CustomerName = toData.customer.customer_name,
                                               AppType = toData.tower_mod_type_id != null ? (from z in ce.tower_mod_types where toData.tower_mod_type_id == z.id select z.tower_mod_type).FirstOrDefault() : null,
                                               TypeID = toData.tower_mod_type_id,
                                               TowerModID = toData.id,
                                               CreateAt = toCom.created_at
                                           });

                    var tmpComment = (from row in towerModComment
                                      orderby row.CreateAt.Value descending
                                      select row).ToList().Take(10);
                    toComment = tmpComment.ToList<LatestRecentComment>();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(null).Log(new Elmah.Error(ex));
            }

            return toComment;
        }

        public static List<LatestRecentComment> GetLatestStructuralAnalysisComment(int currentUserID)
        {
            CPTTEntities ce = new CPTTEntities();
            List<LatestRecentComment> saComment = new List<LatestRecentComment>();
            try
            {
                if (currentUserID != 0)
                {
                    var saQueryComment = (from sa in ce.structural_analysis_comments
                                          join saData in ce.structural_analyses on sa.structural_analysis_id equals saData.id
                                          join leData in ce.lease_applications on saData.lease_application_id equals leData.id
                                          join siteData in ce.sites on leData.site.site_uid equals siteData.site_uid
                                          where sa.updated_at.Value != null && sa.created_at.Value != null
                                          && (leData.tmo_specialist_initial_emp_id == currentUserID
                                           || leData.tmo_specialist_current_emp_id == currentUserID
                                           || leData.tmo_pm_emp_id == currentUserID
                                           || leData.am_emp_id == currentUserID
                                           || leData.colocation_coordinator_emp_id == currentUserID)
                                          select new LatestRecentComment
                                          {
                                              CommentID = sa.id,
                                              LeaseApplicationID = leData.id,
                                              StructuralAnalysisID = sa.structural_analysis_id,
                                              Comments = sa.comments,
                                              CreatorID = sa.creator_id,
                                              CreatorName = sa.creator_user.last_name + ", " + sa.creator_user.first_name,
                                              Timestamp = sa.updated_at,
                                              CreatorUsername = sa.creator_user.tmo_userid,
                                              UpdaterID = sa.updater_id,
                                              UpdaterName = sa.updater_user.last_name + ", " + sa.updater_user.first_name,
                                              UpdateUsername = sa.updater_user.tmo_userid,
                                              UpdateAt = sa.updated_at,
                                              SiteID = leData.site_uid,
                                              SiteName = siteData.site_name,
                                              CustomerName = leData.customer.customer_name,
                                              AppType = leData.leaseapp_types.lease_application_type,
                                              TypeID = leData.leaseapp_types.id,
                                              TowerModID = null,
                                              LeaseAdmin = null,
                                              CreateAt = sa.created_at
                                          });

                    var tmpComment = (from row in saQueryComment
                                      orderby row.CreateAt.Value descending
                                      select row).Take(10).ToList();
                    saComment = tmpComment.ToList<LatestRecentComment>();
                }
                else
                {
                    var saQueryComment = (from sa in ce.structural_analysis_comments
                                          join saData in ce.structural_analyses on sa.structural_analysis_id equals saData.id
                                          join leData in ce.lease_applications on saData.lease_application_id equals leData.id
                                          join siteData in ce.sites on leData.site.site_uid equals siteData.site_uid
                                          where sa.updated_at.Value != null && sa.created_at.Value != null
                                          select new LatestRecentComment
                                          {
                                              CommentID = sa.id,
                                              LeaseApplicationID = leData.id,
                                              StructuralAnalysisID = sa.structural_analysis_id,
                                              Comments = sa.comments,
                                              CreatorID = sa.creator_id,
                                              CreatorName = sa.creator_user.last_name + ", " + sa.creator_user.first_name,
                                              Timestamp = sa.updated_at,
                                              CreatorUsername = sa.creator_user.tmo_userid,
                                              UpdaterID = sa.updater_id,
                                              UpdaterName = sa.updater_user.last_name + ", " + sa.updater_user.first_name,
                                              UpdateUsername = sa.updater_user.tmo_userid,
                                              UpdateAt = sa.updated_at,
                                              SiteID = leData.site_uid,
                                              SiteName = siteData.site_name,
                                              CustomerName = leData.customer.customer_name,
                                              AppType = leData.leaseapp_types.lease_application_type,
                                              TypeID = leData.leaseapp_types.id,
                                              TowerModID = null,
                                              LeaseAdmin = null,
                                              CreateAt = sa.created_at
                                          });

                    var tmpComment = (from row in saQueryComment
                                      orderby row.CreateAt.Value descending
                                      select row).Take(10).ToList();
                    saComment = tmpComment.ToList<LatestRecentComment>();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(null).Log(new Elmah.Error(ex));
            }

            return saComment;
        }
    }

    public class LatestRecentComment
    {
        public const string STRUCTURAL_ANALYSIS = "Al";
        public LatestRecentComment()
        { }

        public DateTime? Timestamp { get; set; }
        public int CommentID { get; set; }
        public int? LeaseApplicationID { get; set; }
        public int? StructuralAnalysisID { get; set; }
        public int? TowerModID { get; set; }
        public string Comments { get; set; }
        public int? CreatorID { get; set; }
        public string CreatorName { get; set; }
        public string CreatorUsername { get; set; }
        public int? UpdaterID { get; set; }
        public string UpdaterName { get; set; }
        public DateTime? CreateAt { get; set; }
        public DateTime? UpdateAt { get; set; }
        public string UpdateUsername { get; set; }
        public string SiteID { get; set; }
        public string SiteName { get; set; }
        public string CustomerName { get; set; }
        public string AppType { get; set; }
        public int? TypeID { get; set; }
        public int? LeaseAdmin { get; set; }
    }

}