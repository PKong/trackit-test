﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects;
using System.Linq;
using Elmah;
using TrackIT2.DAL;
using TrackIT2.Objects;

namespace TrackIT2.BLL
{
    public static class TowerMod
    {
        public static tower_modifications Search(int id)
        {
            tower_modifications results = null;

            using (var ce = new CPTTEntities())
            {
               if (ce.tower_modifications.Any(tm => tm.id.Equals(id)))
               {
                  results = ce.tower_modifications.First(tm => tm.id.Equals(id));
               }               
            }

            return results;
        }

        public static tower_modifications SearchWithinTransaction(int id, CPTTEntities ceRef)
        {
           tower_modifications results = null;

           if (ceRef.tower_modifications.Any(tm => tm.id.Equals(id)))
           {
              results = ceRef.tower_modifications.First(tm => tm.id.Equals(id));
           }           

           return results;
        }

        public static void Add(tower_modifications objTm)
        {
            try
            {
                using (var ce = new CPTTEntities())
                {
                    ce.tower_modifications.AddObject(objTm);
                    ce.SaveChanges();
                }
            }
            catch (Exception ex)
            {
               // Log error in ELMAH for any diagnostic needs before rethrow.
               ErrorSignal.FromCurrentContext().Raise(ex);
               throw;
            }
        }

        public static string AddWithinTransaction(tower_modifications objTm, CPTTEntities ceRef)
        {
           string results = null;

           try
           {
              ceRef.tower_modifications.AddObject(objTm);
           }
           catch (Exception ex)
           {
              // Log error in ELMAH for any diagnostic needs.
              ErrorSignal.FromCurrentContext().Raise(ex);
              results = "An error occurred removing tower mod record.";
           }

           return results;
        }

        public static void Update(tower_modifications objTm)
        {
            // The current application is passed in before applying edits in order
            // to do any additional validation/processing.
            try
            {
                using (var ce = new CPTTEntities())
                {
                   // Since attached objects lose their previous history context, we must manually
                   // test the updated_at field for an optimistic concurrency exception.
                   var testModification = ce.tower_modifications.First(tm => tm.id.Equals(objTm.id));

                   if (testModification.updated_at != objTm.updated_at)
                   {
                      throw new OptimisticConcurrencyException();
                   }
 
                   ce.AttachUpdated(objTm);
                   ce.SaveChanges();
                }
            }
            catch (OptimisticConcurrencyException oex)
            {
               // Log error in ELMAH for any diagnostic needs.
               ErrorSignal.FromCurrentContext().Raise(oex);
               throw;
            }
            catch (Exception ex)
            {
               // Log error in ELMAH for any diagnostic needs before rethrow.
               ErrorSignal.FromCurrentContext().Raise(ex); 
               throw;
            }            
        }

        public static string UpdateWithinTransaction(tower_modifications objTm, CPTTEntities ceRef)
        {
           string results = null;

           try
           {
              // Since attached objects lose their previous history context, we must manually
              // test the updated_at field for an optimistic concurrency exception.
              var testModification = ceRef.tower_modifications.First(tm => tm.id.Equals(objTm.id));

              if (testModification.updated_at != objTm.updated_at)
              {
                 throw new OptimisticConcurrencyException();
              }
              
              ceRef.AttachUpdated(objTm);
           }
           catch (OptimisticConcurrencyException oex)
           {
              // Log error in ELMAH for any diagnostic needs.
              ErrorSignal.FromCurrentContext().Raise(oex);

              results = "The current record has been modifed since you have " +
                        "last retrieved it. Please reload the record and " +
                        "attempt to save it again.";
           }
           catch (Exception ex)
           {
              // Log error in ELMAH for any diagnostic needs.
              ErrorSignal.FromCurrentContext().Raise(ex);
              results = "An error occurred updating the tower modification record.";
           }

           return results;
        }

        public static String Delete(int id,CPTTEntities ceRef = null)
        {
            String sResult = null;
            var isSuccess = false;
            var isRootTransaction = true;

            CPTTEntities ce;
            if (ceRef == null)
            {
                ce = new CPTTEntities();
            }
            else
            {
                ce = ceRef;
                isRootTransaction = false;
            }
               try
               {
                  using (var trans = TransactionUtils.CreateTransactionScope())
                  {
                      if (isRootTransaction)
                      {
                          // Manually open connection to prevent EF from auto closing
                          // connection and causing issues with future queries.
                          ce.Connection.Open();
                      }

                      LeaseAppTowerMod.DeleteAllByTowerModWithinTransaction(id, ce);

                      //remove any relate link document
                      var documents = from doc in ce.documents_links
                                      where doc.record_id == id
                                      select doc;

                      if (documents.Count() > 0)
                      {
                          foreach (documents_links item in documents.ToList<documents_links>())
                          {
                              ce.DeleteObject(item);
                          }
                      }

                     // Remove any related comment records.
                     var queryComment = from com in ce.tower_mod_comments
                                        where com.tower_modification_id == id
                                        select com;

                     if (queryComment.Any())
                     {
                        foreach (tower_mod_comments item in queryComment.ToList())
                        {
                           ce.DeleteObject(item);

                           // Change DocLog
                           DocumentsChangeLog.UpdateIsLinkRecordFlag(item.id, DMSDocumentLinkHelper.eDocumentType.tower_modification.ToString(), false, ce,
                               DMSDocumentLinkHelper.eDocumentField.TMO_Comments.ToString());
                        }
                     }

                     // Remove any related tower mod PO records.
                     var queryPOs = from pos in ce.tower_mod_pos
                                    where pos.tower_modification_id == id
                                    select pos;

                     if (queryPOs.Any())
                     {
                        foreach (tower_mod_pos item in queryPOs.ToList())
                        {
                           ce.DeleteObject(item);

                           // Change DocLog
                           DocumentsChangeLog.UpdateIsLinkRecordFlag(item.id, DMSDocumentLinkHelper.eDocumentType.tower_modification.ToString(), false, ce);
                        }
                     }

                     // Delete record
                     var queryTm = from tm in ce.tower_modifications
                                   where tm.id == id
                                   select tm;

                     if (queryTm.Any())
                     {
                        var item = queryTm.ToList()[0];
                        ce.DeleteObject(item);

                        // Change DocLog
                        DocumentsChangeLog.UpdateIsLinkRecordFlag(id, DMSDocumentLinkHelper.eDocumentType.tower_modification_general.ToString(), false, ce);
                     }
                     else
                     {
                        throw new NullReferenceException();
                     }

                     if (isRootTransaction)
                     {
                         // We tentatively save the changes so that we can finish 
                         // processing the transaction.
                         ce.SaveChanges(SaveOptions.DetectChangesBeforeSave);
                     }
                     isSuccess = true;
                     trans.Complete();
                  }
               }

               catch (Exception ex)
               {
                  // Log error in ELMAH for any diagnostic needs.
                  ErrorSignal.FromCurrentContext().Raise(ex);
                  sResult = ex.ToString();
               }

               finally
               {
                   if (isRootTransaction)
                   {
                       ce.Connection.Dispose();
                   }
               }

               // Finally accept all changes from the transaction.
               if (isSuccess && isRootTransaction)
               {
                   ce.AcceptAllChanges();
                   ce.Dispose();
               }
            
            return sResult;
        }

        public static String DeleteWithinTransaction(int id, CPTTEntities ceRef)
        {
           String results = null;

           try
           {
              // Remove any related lease application / tower modification records.
              LeaseAppTowerMod.DeleteAllByTowerModWithinTransaction(id, ceRef);

              // Remove any related comment records.
              var queryComment = from com in ceRef.tower_mod_comments
                                 where com.tower_modification_id == id
                                 select com;

              if (queryComment.Any())
              {
                 foreach (var item in queryComment.ToList())
                 {
                    ceRef.DeleteObject(item);

                    // Change DocLog
                    DocumentsChangeLog.UpdateIsLinkRecordFlag(item.id, DMSDocumentLinkHelper.eDocumentType.tower_modification.ToString(), false, ceRef,
                        DMSDocumentLinkHelper.eDocumentField.TMO_Comments.ToString());
                 }
              }

              // Remove any related tower mod PO records.
              var queryPOs = from pos in ceRef.tower_mod_pos
                             where pos.tower_modification_id == id
                             select pos;

              if (queryPOs.Any())
              {
                 foreach (var item in queryPOs.ToList())
                 {
                    ceRef.DeleteObject(item);

                    // Change DocLog
                    DocumentsChangeLog.UpdateIsLinkRecordFlag(item.id, DMSDocumentLinkHelper.eDocumentType.tower_modification.ToString(), false, ceRef);
                 }
              }

              // Delete record
              var queryTm = from tm in ceRef.tower_modifications
                            where tm.id == id
                            select tm;
              if (queryTm.Any())
              {
                 var item = queryTm.ToList()[0];
                 ceRef.DeleteObject(item);

                 // Change DocLog
                 DocumentsChangeLog.UpdateIsLinkRecordFlag(id, DMSDocumentLinkHelper.eDocumentType.tower_modification_general.ToString(), false, ceRef);
              }
              else
              {
                 throw new NullReferenceException();
              }
           }
           catch (Exception ex)
           {
              // Log error in ELMAH for any diagnostic needs.
              ErrorSignal.FromCurrentContext().Raise(ex);
              results = "An error occurred removing tower mod record.";
           }

           return results;
        }

      // All site search criteria in a dictionary for easy lookup/manipulation
      // when dynamically building the query.
      public static Dictionary<string, SearchParameter> GetSearchCriteria()
      {
         var results = new Dictionary<string, SearchParameter>
            {
                {
                  "General", new SearchParameter
                     {                        
                        VariableName = "General",
                        ParameterType = typeof(string),
                        WhereClause = "(it.site_uid LIKE {0})",
                        IsWildcardSearch = true
                     }
               },
               {
                  "TowerModId", new SearchParameter
                     {
                        VariableName = "TowerModId",
                        ParameterType = typeof(int),
                        WhereClause = "(it.id = {0})"
                     }
               },
               {
                  "SiteID", new SearchParameter
                     {
                        VariableName = "SiteID",
                        ParameterType = typeof(string),
                        WhereClause = "(it.site_uid LIKE {0})",
                        IsWildcardSearch = true
                     }
               },
               {
                  "Customer", new SearchParameter
                     {
                        VariableName = "Customer",
                        ParameterType = typeof(int),
                        WhereClause = "(it.customer_id = {0})",
                        AllowsMultipleValues = true
                     }
               },
               {
                  "TowerModType", new SearchParameter
                     {
                        VariableName = "TowerModType",
                        ParameterType = typeof(int),
                        WhereClause = "(it.tower_mod_type_id = {0})",
                        AllowsMultipleValues = true
                     }                  
               },
               {
                  "TowerModStatus", new SearchParameter
                     {
                        VariableName = "TowerModStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.tower_mod_status_id = {0})",
                        AllowsMultipleValues = true
                     }
               },
               {
                  "TowerModPM", new SearchParameter
                     {
                        VariableName = "TowerModPM",
                        ParameterType = typeof(int),
                        WhereClause = "(it.tower_mod_pm_emp_id = {0})",
                        AllowsMultipleValues = true
                     }
               },
               {
                  "CurrentPhase", new SearchParameter
                     {
                        VariableName = "CurrentPhase",
                        ParameterType = typeof(int),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND tmpos.tower_mod_phase_type_id = {0}))",
                        AllowsMultipleValues = true
                     }                  
               },
               {
                  "CurrentPacketRcvdFrom", new SearchParameter
                     {
                        VariableName = "CurrentPacketRcvdFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND tmpos.ebid_req_rcvd_date >= {0}))"
                     }
               },
               {
                  "CurrentPacketRcvdTo", new SearchParameter
                     {
                        VariableName = "CurrentPacketRcvdTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND tmpos.ebid_req_rcvd_date <= {0}))"
                     }
               },
               {
                  "CurrentPacketRcvdRange", new SearchParameter
                     {
                        VariableName = "CurrentPacketRcvdRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND (tmpos.ebid_req_rcvd_date >= {0} AND tmpos.ebid_req_rcvd_date <= {1})))",
                        IsRangeSearch = true
                     }
               },
               {
                  "ProjectCompleteFrom", new SearchParameter
                     {
                        VariableName = "ProjectCompleteFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.project_complete_date >= {0})"
                     }
               },
               {
                  "ProjectCompleteTo", new SearchParameter
                     {
                        VariableName = "ProjectCompleteTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.project_complete_date <= {0})"
                     }
               },
               {
                  "ProjectCompleteRange", new SearchParameter
                     {
                        VariableName = "ProjectCompleteRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.project_complete_date >= {0} AND it.project_complete_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "TMCustomer", new SearchParameter
                     {
                        VariableName = "TMCustomer",
                        ParameterType = typeof(int),
                        WhereClause = "(it.customer_id = {0})"
                     }
               },
               {
                  "TMStat", new SearchParameter
                     {
                        VariableName = "TMStat",
                        ParameterType = typeof(int),
                        WhereClause = "(it.tower_mod_status_id = {0})"
                     }                  
               },
               {
                  "TMType", new SearchParameter
                     {
                        VariableName = "TMType",
                        ParameterType = typeof(int),
                        WhereClause = "(it.tower_mod_type_id = {0})"
                     }
               },
               {
                  "TMHeightFrom", new SearchParameter
                     {
                        VariableName = "TMHeightFrom",
                        ParameterType = typeof(double),
                        WhereClause = "(it.tower_height_proposed_ft >= {0})"
                     }
               },
               {
                  "TMHeightTo", new SearchParameter
                     {
                        VariableName = "TMHeightTo",
                        ParameterType = typeof(double),
                        WhereClause = "(it.tower_height_proposed_ft <= {0})"
                     }
               },
               {
                  "TMHeightRange", new SearchParameter
                     {
                        VariableName = "TMHeightRange",
                        ParameterType = typeof(double),
                        WhereClause = "(it.tower_height_proposed_ft >= {0} AND it.tower_height_proposed_ft <= {1})",
                        IsRangeSearch = true
                     }
               },
               {
                  "TMCapacityFrom", new SearchParameter
                     {
                        VariableName = "TMCapacityFrom",
                        ParameterType = typeof(double),
                        WhereClause = "(it.postmod_structural_capacity_pct >= {0})"
                     }
               },
               {
                  "TMCapacityTo", new SearchParameter
                     {
                        VariableName = "TMCapacityTo",
                        ParameterType = typeof(double),
                        WhereClause = "(it.postmod_structural_capacity_pct <= {0})"
                     }                  
               },
               {
                  "TMCapacityRange", new SearchParameter
                     {
                        VariableName = "TMCapacityRange",
                        ParameterType = typeof(double),
                        WhereClause = "(it.postmod_structural_capacity_pct >= {0} AND it.postmod_structural_capacity_pct <= {1})"
                     }
               },
               {
                  "RegNotification", new SearchParameter
                     {
                        VariableName = "RegNotification",
                        ParameterType = typeof(bool),
                        WhereClause = "(it.regulatory_notification_yn = {0})"
                     }
               },
               {
                  "PermitCarrier", new SearchParameter
                     {
                        VariableName = "PermitCarrier",
                        ParameterType = typeof(bool),
                        WhereClause = "(it.building_permit_from_carrier_yn = {0})"
                     }                  
               },
               {
                  "PermitReceivedFrom", new SearchParameter
                     {
                        VariableName = "PermitReceivedFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.building_permit_rcvd_date >= {0})"
                     }                  
               },
               {
                  "PermitReceivedTo", new SearchParameter
                     {
                        VariableName = "ProjectCompleteTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.building_permit_rcvd_date <= {0})"
                     }                  
               },
               {
                  "PermitReceivedRange", new SearchParameter
                     {
                        VariableName = "PermitReceivedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.building_permit_rcvd_date >= {0} AND it.building_permit_rcvd_date <= {1})",
                        IsRangeSearch = true
                     }
               },
               {
                  "PermitReceivedStatus", new SearchParameter
                     {
                        VariableName = "PermitReceivedStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.building_permit_rcvd_date_status_id = {0})"
                     }                  
               },
               {
                  "TopOutNotificationRequired", new SearchParameter
                     {
                        VariableName = "TopOutNotificationRequired",
                        ParameterType = typeof(bool),
                        WhereClause = "(it.top_out_notification_required_yn = {0})"
                     }                  
               },
               {
                  "MaterialsOrderedFrom", new SearchParameter
                     {
                        VariableName = "MaterialsOrderedFrom",
                        ParameterType =  typeof(DateTime),
                        WhereClause = "(it.materials_ordered_date >= {0})"
                     }                  
               },
               {
                  "MaterialsOrderedTo", new SearchParameter
                     {
                        VariableName = "MaterialsOrderedTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.materials_ordered_date <= {0})"
                     }                  
               },
               {
                  "MaterialsOrderedRange", new SearchParameter
                     {
                        VariableName = "MaterialsOrderedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.materials_ordered_date >= {0} AND it.materials_ordered_date <= {1})",
                        IsRangeSearch = true
                     }
               },
               {
                  "MaterialReceivedFrom", new SearchParameter
                     {
                        VariableName = "MaterialReceivedFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.materials_rcvd_date >= {0})"
                     }                  
               },
               {
                  "MaterialReceivedTo", new SearchParameter
                     {
                        VariableName = "MaterialReceivedTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.materials_rcvd_date <= {0})"
                     }                  
               },
               {
                  "MaterialReceivedRange", new SearchParameter
                     {
                        VariableName = "MaterialReceivedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.materials_rcvd_date >= {0} AND it.materials_rcvd_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "MaterialReceivedStatus", new SearchParameter
                     {
                        VariableName = "MaterialReceivedStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.materials_rcvd_date_status_id = {0})"
                     }
               },
               {
                  "WorkStartFrom", new SearchParameter
                     {
                        VariableName = "WorkStartFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.work_start_date >= {0})"
                     }                  
               },
               {
                  "WorkStartTo", new SearchParameter
                     {
                        VariableName = "WorkStartTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.work_start_date <= {0})"
                     }                  
               },
               {
                  "WorkStartRange", new SearchParameter
                     {
                        VariableName = "WorkStartRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.work_start_date >= {0} AND it.work_start_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "WorkStartStatus", new SearchParameter
                     {
                        VariableName = "WorkStartStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.work_start_date_status_id = {0})"
                     }
               },
               {
                  "WorkCompletionFrom", new SearchParameter
                     {
                        VariableName = "WorkCompletionFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.work_completion_date >= {0})"
                     }                  
               },
               {
                  "WorkCompletionTo", new SearchParameter
                     {
                        VariableName = "WorkCompletionTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.work_completion_date <= {0})"
                     }                  
               },
               {
                  "WorkCompletionRange", new SearchParameter
                     {
                        VariableName = "WorkCompletionRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.work_completion_date >= {0} AND it.work_completion_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "WorkCompletionStatus", new SearchParameter
                     {
                        VariableName = "WorkCompletionStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.work_completion_date_status_id = {0})"
                     }                  
               },
               {
                  "TMPayor", new SearchParameter
                     {
                        VariableName = "TMPayor",
                        ParameterType = typeof(int),
                        WhereClause = "(it.payor_id = {0})"
                     }                  
               },
               {
                  "ThirdPartyInspectionFrom", new SearchParameter
                     {
                        VariableName = "ThirdPartyInspectionFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.third_party_inspection_date >= {0})"
                     }                  
               },
               {
                  "ThirdPartyInspectionTo", new SearchParameter
                     {
                        VariableName = "ThirdPartyInspectionTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.third_party_inspection_date <= {0})"
                     }                  
               },
               {
                  "ThirdPartyInspectionRange", new SearchParameter
                     {
                        VariableName = "ThirdPartyInspectionRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.third_party_inspection_date >= {0} AND it.third_party_inspection_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "ThirdPartyInspectionStatus", new SearchParameter
                     {
                        VariableName = "ThirdPartyInspectionStatus",
                        ParameterType = typeof(int),
                        WhereClause = "(it.third_party_inspection_date_status_id = {0})"
                     }                  
               },
               {
                  "TMPmEmp", new SearchParameter
                     {
                        VariableName = "TMPmEmp",
                        ParameterType = typeof(int),
                        WhereClause = "(it.tower_mod_pm_emp_id = {0})"
                     }                  
               },
               {
                  "PackageSentFrom", new SearchParameter
                     {
                        VariableName = "PackageSentFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.tower_mod_packet_sent_date >= {0})"
                     }                  
               },
               {
                  "PackageSentTo", new SearchParameter
                     {
                        VariableName = "PackageSentTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.tower_mod_packet_sent_date <= {0})"
                     }                  
               },
               {
                  "PackageSentRange", new SearchParameter
                     {
                        VariableName = "PackageSentRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(it.tower_mod_packet_sent_date >= {0} AND it.tower_mod_packet_sent_date <= {1})",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "POTowerModPhaseType", new SearchParameter
                     {
                        VariableName = "POTowerModPhaseType",
                        ParameterType = typeof(int),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND tmpos.tower_mod_phase_type_id = {0}))",
                     }                  
               },
               {
                  "PORequestReceivedFrom", new SearchParameter
                     {
                        VariableName = "PORequestReceivedFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND tmpos.ebid_req_rcvd_date >= {0}))",
                     }                  
               },
               {
                  "PORequestReceivedTo", new SearchParameter
                     {
                        VariableName = "PORequestReceivedTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND tmpos.ebid_req_rcvd_date <= {0}))",
                     }                  
               },
               {
                  "PORequestReceivedRange", new SearchParameter
                     {
                        VariableName = "PORequestReceivedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND (tmpos.ebid_req_rcvd_date >= {0} AND tmpos.ebid_req_rcvd_date <= {1})))",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "POPacketReadyFrom", new SearchParameter
                     {
                        VariableName = "POPacketReadyFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND tmpos.ebid_packet_ready_date >= {0}))",
                     }                  
               },
               {
                  "POPacketReadyTo", new SearchParameter
                     {
                        VariableName = "POPacketReadyTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND tmpos.ebid_packet_ready_date <= {0}))",
                     }                  
               },
               {
                  "POPacketReadyRange", new SearchParameter
                     {
                        VariableName = "POPacketReadyRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND (tmpos.ebid_packet_ready_date >= {0} AND tmpos.ebid_packet_ready_date <= {1})))",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "POScheduledPublishedFrom", new SearchParameter
                     {
                        VariableName = "POScheduledPublishedFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND tmpos.ebid_scheduled_published_date >= {0}))",
                     }                  
               },
               {
                  "POScheduledPublishedTo", new SearchParameter
                     {
                        VariableName = "POScheduledPublishedTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND tmpos.ebid_scheduled_published_date <= {0}))",
                     }                  
               },
               {
                  "POScheduledPublishedRange", new SearchParameter
                     {
                        VariableName = "POScheduledPublishedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND (tmpos.ebid_scheduled_published_date >= {0} AND tmpos.ebid_scheduled_published_date <= {1})))",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "POScheduledCloseFrom", new SearchParameter
                     {
                        VariableName = "POScheduledCloseFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND tmpos.ebid_scheduled_close_date >= {0}))",
                     }                  
               },
               {
                  "POScheduledCloseTo", new SearchParameter
                     {
                        VariableName = "POScheduledCloseTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND tmpos.ebid_scheduled_close_date <= {0}))",
                     }                  
               },
               {
                  "POScheduledCloseRange", new SearchParameter
                     {
                        VariableName = "POScheduledCloseRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND (tmpos.ebid_scheduled_close_date >= {0} AND tmpos.ebid_scheduled_close_date <= {1})))",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "POMaturityFrom", new SearchParameter
                     {
                        VariableName = "POMaturityFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND tmpos.ebid_maturity_date >= {0}))",
                     }                  
               },
               {
                  "POMaturityTo", new SearchParameter
                     {
                        VariableName = "POMaturityTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND tmpos.ebid_maturity_date <= {0}))",
                     }                  
               },
               {
                  "POMaturityRange", new SearchParameter
                     {
                        VariableName = "POMaturityRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND (tmpos.ebid_maturity_date >= {0} AND tmpos.ebid_maturity_date <= {1})))",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "POEbidComment", new SearchParameter
                     {
                        VariableName = "POEbidComment",
                        ParameterType = typeof(string),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND tmpos.ebid_comment LIKE {0}))",
                        IsWildcardSearch = true
                     }                  
               },
               {
                  "POEstConstCostFrom", new SearchParameter
                     {
                        VariableName = "POEstConstCostFrom",
                        ParameterType = typeof(decimal),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND tmpos.estimated_construction_cost_amount >= {0}))",
                     }                  
               },
               {
                  "POEstConstCostTo", new SearchParameter
                     {
                        VariableName = "POEstConstCostTo",
                        ParameterType = typeof(decimal),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND tmpos.estimated_construction_cost_amount <= {0}))",
                     }                  
               },
               {
                  "POEstConstCostRange", new SearchParameter
                     {
                        VariableName = "POEstConstCostRange",
                        ParameterType = typeof(decimal),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND (tmpos.estimated_construction_cost_amount >= {0} AND tmpos.estimated_construction_cost_amount <= {1})))",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "POBidFrom", new SearchParameter
                     {
                        VariableName = "POBidFrom",
                        ParameterType = typeof(decimal),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND tmpos.bid_amount >= {0}))",
                     }                  
               },
               {
                  "POBidTo", new SearchParameter
                     {
                        VariableName = "POBidTo",
                        ParameterType = typeof(decimal),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND tmpos.bid_amount <= {0}))",
                     }                  
               },
               {
                  "POBidRange", new SearchParameter
                     {
                        VariableName = "POBidRange",
                        ParameterType = typeof(decimal),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND (tmpos.bid_amount >= {0} AND tmpos.bid_amount <= {1})))",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "POMarkupFrom", new SearchParameter
                     {
                        VariableName = "POMarkupFrom",
                        ParameterType = typeof(decimal),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND tmpos.markup_amount >= {0}))",
                     }                  
               },
               {
                  "POMarkupTo", new SearchParameter
                     {
                        VariableName = "POMarkupTo",
                        ParameterType = typeof(decimal),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND tmpos.markup_amount <= {0}))",
                     }                  
               },
               {
                  "POMarkupRange", new SearchParameter
                     {
                        VariableName = "POMarkupRange",
                        ParameterType = typeof(decimal),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND (tmpos.markup_amount >= {0} AND tmpos.markup_amount <= {1})))",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "POTotalFromCustomerFrom", new SearchParameter
                     {
                        VariableName = "POTotalFromCustomerFrom",
                        ParameterType = typeof(decimal),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND tmpos.total_from_customer_amount >= {0}))",
                     }                  
               },
               {
                  "POTotalFromCustomerTo", new SearchParameter
                     {
                        VariableName = "POTotalFromCustomerTo",
                        ParameterType = typeof(decimal),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND tmpos.total_from_customer_amount <= {0}))",
                     }                  
               },
               {
                  "POTotalFromCustomerRange", new SearchParameter
                     {
                        VariableName = "POTotalFromCustomerRange",
                        ParameterType = typeof(decimal),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND (tmpos.total_from_customer_amount >= {0} AND tmpos.total_from_customer_amount <= {1})))",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "POSentToCsPMFrom", new SearchParameter
                     {
                        VariableName = "POSentToCsPMFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND tmpos.sent_to_cs_pm_date >= {0}))",
                     }                  
               },
               {
                  "POSentToCsPMTo", new SearchParameter
                     {
                        VariableName = "POSentToCsPMTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND tmpos.sent_to_cs_pm_date <= {0}))",
                     }                  
               },
               {
                  "POSentToCsPMRange", new SearchParameter
                     {
                        VariableName = "POSentToCsPMRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND (tmpos.sent_to_cs_pm_date >= {0} AND tmpos.sent_to_cs_pm_date <= {1})))",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "POToGcRequestedFrom", new SearchParameter
                     {
                        VariableName = "POToGcRequestedFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND tmpos.po_to_gc_requested_date >= {0}))",
                     }                  
               },
               {
                  "POToGcRequestedTo", new SearchParameter
                     {
                        VariableName = "POToGcRequestedTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND tmpos.po_to_gc_requested_date <= {0}))",
                     }                  
               },
               {
                  "POToGcRequestedRange", new SearchParameter
                     {
                        VariableName = "POToGcRequestedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND (tmpos.po_to_gc_requested_date >= {0} AND tmpos.po_to_gc_requested_date <= {1})))",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "POToGcReceivedFrom", new SearchParameter
                     {
                        VariableName = "POToGcReceivedFrom",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND tmpos.po_to_gc_rcvd_date >= {0}))",
                     }                  
               },
               {
                  "POToGcReceivedTo", new SearchParameter
                     {
                        VariableName = "POToGcReceivedTo",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND tmpos.po_to_gc_rcvd_date <= {0}))",
                     }                  
               },
               {
                  "POToGcReceivedRange", new SearchParameter
                     {
                        VariableName = "POToGcReceivedRange",
                        ParameterType = typeof(DateTime),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND (tmpos.po_to_gc_rcvd_date >= {0} AND tmpos.po_to_gc_rcvd_date <= {1})))",
                        IsRangeSearch = true
                     }                  
               },
               {
                  "POShoppingCartNumber", new SearchParameter
                     {
                        VariableName = "POShoppingCartNumber",
                        ParameterType = typeof(string),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND tmpos.shopping_cart_nbr LIKE {0}))",
                        IsWildcardSearch = true
                     }                  
               },
               {
                  "PONumber", new SearchParameter
                     {
                        VariableName = "PONumber",
                        ParameterType = typeof(string),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND tmpos.po_nbr LIKE {0}))",
                        IsWildcardSearch = true
                     }                  
               },
               {
                  "POTaxFrom", new SearchParameter
                     {
                        VariableName = "POTotalFromCustomerFrom",
                        ParameterType = typeof(decimal),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND tmpos.tax_amount >= {0}))",
                     }                  
               },
               {
                  "POTaxTo", new SearchParameter
                     {
                        VariableName = "POTotalFromCustomerTo",
                        ParameterType = typeof(decimal),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND tmpos.tax_amount <= {0}))",
                     }                  
               },
               {
                  "POTaxRange", new SearchParameter
                     {
                        VariableName = "POTotalFromCustomerRange",
                        ParameterType = typeof(decimal),
                        WhereClause = "(EXISTS(SELECT VALUE tmpos FROM [CPTTEntities].[tower_mod_pos] AS tmpos " +
                                    "WHERE tmpos.tower_modification_id = it.id AND (tmpos.tax_amount >= {0} AND tmpos.tax_amount <= {1})))",
                        IsRangeSearch = true
                     }                  
               }
            };

         return results;
      }
   }
}