﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Reflection;
using System.Web;
using TrackIT2.DAL;
using TrackIT2.Objects;
using System.Transactions;

namespace TrackIT2.BLL
{
    public class LeaseAppEquipment
    {
        public EquipmentEntities enEquip;

        public LeaseAppEquipment()
        {
            enEquip = new EquipmentEntities();
        }

        public t_Application Get_t_Application(string TMO_AppID)
        {
            t_Application currentEquip = (from eq in enEquip.t_Application
                                            where eq.TMO_AppID == TMO_AppID
                                            select eq).FirstOrDefault();
            return currentEquip;
        }

        public IQueryable<AmplifierEquipment> GetAmplifierEquipment(int ApplicationID)
        {
            IQueryable<AmplifierEquipment> queryAmp = from amp in enEquip.t_Amplifier
                                                    where amp.ApplicationID == ApplicationID
                                                      select new AmplifierEquipment
                                                       {
                                                           Quantity = amp.Quantity,
                                                           Manufacturer = amp.Manufacturer,
                                                           Model = amp.Model,
                                                           Weight = amp.Weight,
                                                           Dimensions_L = amp.DimensionL,
                                                           Dimensions_W = amp.DimensionW,
                                                           Dimensions_H = amp.DimensionH,
                                                           IsExist = amp.Existing,
                                                           EquipmentGroup = amp.AmplifierGroup
                                                       };
            return queryAmp;
        }

        public IQueryable<AntennaEquipment> GetAntennaEquipment(int ApplicationID)
        {
            IQueryable<AntennaEquipment> queryAnt = from ant in enEquip.t_ApplicationEquipment
                                                where ant.EquipmentType == "Antenna" && ant.ApplicationID == ApplicationID
                                                select new AntennaEquipment
                                               {
                                                   Quantity = ant.Quantity,
                                                   RADCenter = ant.RADCenter,
                                                   Azimuth = ant.Azimuth,
                                                   Manufacturer = ant.Manufacturer,
                                                   Model = ant.Model,
                                                   Weight = ant.Weight,
                                                   Dimensions_L = ant.DimensionL,
                                                   Dimensions_W = ant.DimensionW,
                                                   Dimensions_H = ant.DimensionH,
                                                   MountingType = ant.MountingEquipmentType,
                                                   MountingDimensions_L = ant.MountEquipDimensionL,
                                                   MountingDimensions_W = ant.MountEquipDimensionW,
                                                   MountingDimensions_H = ant.MountEquipDimensionH,
                                                   TxTransmit = ant.FrequencyTx,
                                                   RxTransmit = ant.FrequencyRx,
                                                   PowerOutput = ant.FrequencyPower,
                                                   IsExist = ant.Existing,
                                                   EquipmentGroup = ant.EquipmentGroup,
                                                   ID = ant.ID
                                               };
            return queryAnt;
        }

        public IQueryable<GPSEquipment> GetGPSEquipment(int ApplicationID, string ApplicationType)
        {
            IQueryable<GPSEquipment> queryGPS = from gps in enEquip.t_ProposedGPSUnit
                                                where gps.ApplicationID == ApplicationID
                                                select new GPSEquipment
                                                { 
                                                    IsInitial = (ApplicationType != "MOD" || !gps.Existing) ? true : false,
                                                    Quantity = gps.Quantity,
                                                    Manufacturer = gps.Manufacturer,
                                                    Model = gps.Model,
                                                    InstalledLocation = gps.InstalledLocation,
                                                    IsExist = gps.Existing,
                                                    EquipmentGroup = gps.ProposedGPSGroup,
                                                    RADCenter = gps.RADCenter,
                                                    Weight = gps.Weight,
                                                    Dimensions_L = gps.DimensionL,
                                                    Dimensions_W = gps.DimensionW,
                                                    Dimensions_H = gps.DimensionH,
                                                    TMO_AppID = gps.t_Application.TMO_AppID
                                                };
            return queryGPS;
        }

        public IQueryable<AntennaEquipmentTxLineType> GetAntennaEquipmentTxLineType(int ApplicationEquipmentID)
        {
            IQueryable<AntennaEquipmentTxLineType> antLine = from aLine in enEquip.t_AntennaTxLineType
                                                             where aLine.ApplicationEquipmentID == ApplicationEquipmentID
                                                             select new AntennaEquipmentTxLineType
                                                             {
                                                                  TxLineType = aLine.TransLineType,
                                                                  TxNumberOfLine = aLine.TransLinesCount,
                                                                  TxManufacturer = aLine.Manufacturer,
                                                                  TxModel = aLine.Model,
                                                                  TxDiameter = aLine.Diameter,
                                                                  TxLength = aLine.Length,
                                                                  IsExist = aLine.Existing,
                                                                  EquipmentGroup = aLine.TransLineTypeGroup,
                                                                  ID = aLine.ApplicationEquipmentID
                                                             };
            return antLine;
        }

        public IQueryable<MicrowaveEquipment> GetMicrowaveEquipment(int ApplicationEquipmentID)
        {
            IQueryable<MicrowaveEquipment> queryMic = from mic in enEquip.t_ApplicationEquipment
                                                      where mic.EquipmentType == "MWDish" && mic.ApplicationID == ApplicationEquipmentID
                                                      select new MicrowaveEquipment
                                                       {
                                                           Quantity = mic.Quantity,
                                                           RADCenter = mic.RADCenter,
                                                           Azimuth = mic.Azimuth,
                                                           Manufacturer = mic.Manufacturer,
                                                           Model = mic.Model,
                                                           Weight = mic.Weight,
                                                           Diameter = mic.DimensionH != null ? mic.DimensionH + "' (FEET)" : "",
                                                           Mounting = mic.MountingEquipmentType,
                                                           IsExist = mic.Existing,
                                                           EquipmentGroup = mic.EquipmentGroup
                                                       };
            return queryMic;
        }

        public IQueryable<OtherEquipment> GetOtherEquipment(int ApplicationEquipmentID)
        {
            IQueryable<OtherEquipment> queryOth = from oth in enEquip.t_ApplicationEquipment
                                                                    where oth.EquipmentType == "Other" && oth.ApplicationID == ApplicationEquipmentID
                                                                    select new OtherEquipment
                                                                    {
                                                                        Quantity = oth.Quantity,
                                                                        RADCenter = oth.RADCenter,
                                                                        Azimuth = oth.Azimuth,
                                                                        Manufacturer = oth.Manufacturer,
                                                                        Model = oth.Model,
                                                                        Weight = oth.Weight,
                                                                        Dimensions = "",
                                                                        MountingType = oth.MountingEquipmentType,
                                                                        Dimensions_L = oth.DimensionL,
                                                                        Dimensions_W = oth.DimensionW,
                                                                        Dimensions_H = oth.DimensionH,
                                                                        MountingLocation = oth.MountingLocation,
                                                                        MountEquipDimensionL = oth.MountEquipDimensionL,
                                                                        MountEquipDimensionW = oth.MountEquipDimensionW,
                                                                        MountEquipDimensionH = oth.MountEquipDimensionH,
                                                                        EquipmentPurpose = oth.EquipmentPurpose,
                                                                        EquipmentNotes = oth.EquipmentNotes,
                                                                        IsExist = oth.Existing,
                                                                        EquipmentGroup = oth.EquipmentGroup
                                                                    };
            return queryOth;
        }

        public IQueryable<GspEquipment> GetGspEquipment(int ApplicationEquipmentID)
        {
            IQueryable<GspEquipment> queryGsp = from gsp in enEquip.t_ApplicationEquipGround
                                                where gsp.ApplicationID == ApplicationEquipmentID
                                                select new GspEquipment
                                                {
                                                   Dimensions_W = gsp.DimensionW,
                                                   Dimensions_L = gsp.DimensionL,
                                                   Manufacturer = gsp.Manufacturer,
                                                   Size = gsp.Size,
                                                   TypeOfModulation = gsp.ModulationType,
                                                   ThreedB = gsp.DuplexerBandwidth_3dB,
                                                   TwentydB = gsp.DuplexerBandwidth_20dB,
                                                   IsExist = gsp.Existing,
                                                   EquipmentGroup = ""
                                                };
            return queryGsp;
        }

        public List<t_Contact> Get_t_ContactEquipment(string TMO_AppID)
        {
            List<t_Contact> relateContact = new List<t_Contact>();
            try
            {
                relateContact = (from app in enEquip.t_Application
                                      from con in enEquip.t_Contact
                                      where app.TMO_AppID == TMO_AppID
                                      && (app.ContactID_Billing == con.ID
                                      || app.ContactID_CarrierDevMgr == con.ID
                                      || app.ContactID_RFContact == con.ID
                                      || app.ContactID_SiteAcquisition == con.ID
                                      || app.ContactID_App == con.ID)
                                      select con).ToList();
            }
            catch (Exception ex)
            {
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return relateContact;
        }

        public t_Contact GetContactEquipment(int? ContactID, List<t_Contact> relateContact)
        {
            t_Contact contact = new t_Contact();
            try
            {
                contact = (from con in relateContact
                           where con.ID == ContactID
                           select con).FirstOrDefault<t_Contact>();
            }
            catch (Exception ex)
            {
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return contact;
        }

        public ContactCustomerInfomation GetcustomerInfomationEquipment(t_Application tmoApp, List<t_Contact> releteContact)
        {
            //Customer Information
            ContactCustomerInfomation customerInformationData = new ContactCustomerInfomation();
            try
            {
                customerInformationData = (from customerData in releteContact
                                           where customerData.ID == tmoApp.ContactID_App
                                           select new ContactCustomerInfomation
                                           {
                                               Submitter_Title = tmoApp.t_Applicant.SubmitterTitle != null ? tmoApp.t_Applicant.SubmitterTitle : "",
                                               Legal_Entity_Name = tmoApp.t_Applicant.LegalEntityName != null ? tmoApp.t_Applicant.LegalEntityName : "",
                                               DBA = tmoApp.t_Applicant.DBA != null ? tmoApp.t_Applicant.DBA : "",
                                               FCC_Licensed = tmoApp.t_Applicant.FCC_Licensed != null ? tmoApp.t_Applicant.FCC_Licensed : false,
                                               FCC_Licensed_Number = tmoApp.t_Applicant.FCC_LicenseNumber != null ? tmoApp.t_Applicant.FCC_LicenseNumber : "",
                                               Address = customerData.Address != null ? customerData.Address : "",
                                               Address2 = customerData.Address2 != null ? customerData.Address2 : "",
                                               City = customerData.City != null ? customerData.City : "",
                                               State = customerData.State != null ? customerData.State : "",
                                               Zip = customerData.Zip != null ? customerData.Zip : "",
                                               CountyID = customerData.CountryID != null ? customerData.CountryID : null,
                                               Customer_Site_Number = tmoApp.CustomerSiteNumber != null ? tmoApp.CustomerSiteNumber : "",
                                               County = customerData.CountryID != null ? customerData.t_Country.FullName : null
                                           }).FirstOrDefault<ContactCustomerInfomation>();
            }
            catch (Exception ex)
            {
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return customerInformationData;
        }

        public string[] GenDataAntennaEquipmentTxLineType(List<LeaseAppEquipment.AntennaEquipmentTxLineType> tmpList, int count, bool dataflag)
        {
            string[] tmpA = new string[10];
            if (dataflag)
            {
                tmpA[0] = string.Format("TxLineType_{0}_{1}!\":!\"{2}", tmpList[count].ID, count, tmpList[count].TxLineType != null ? tmpList[count].TxLineType : "null");
                tmpA[1] = string.Format("TxNumberOfLine_{0}_{1}!\":!\"{2}", tmpList[count].ID, count, tmpList[count].TxNumberOfLine != null ? tmpList[count].TxNumberOfLine.ToString() : "null");
                tmpA[2] = string.Format("TxManufacturer_{0}_{1}!\":!\"{2}", tmpList[count].ID, count, tmpList[count].TxManufacturer != null ? tmpList[count].TxManufacturer.ToString() : "null");
                tmpA[3] = string.Format("TxModel_{0}_{1}!\":!\"{2}", tmpList[count].ID, count, tmpList[count].TxModel != null ? tmpList[count].TxModel : "null");
                tmpA[4] = string.Format("TxDiameter_{0}_{1}!\":!\"{2}", tmpList[count].ID, count, tmpList[count].TxDiameter != null ? tmpList[count].TxDiameter : "null");
                tmpA[5] = string.Format("TxLength_{0}_{1}!\":!\"{2}", tmpList[count].ID, count, tmpList[count].TxLength != null ? tmpList[count].TxLength : "null");
                tmpA[6] = string.Format("IsExist_{0}_{1}!\":!\"{2}", tmpList[count].ID, count, tmpList[count].IsExist != null ? tmpList[count].IsExist.ToString() : "null");
                tmpA[7] = string.Format("EquipmentGroup_{0}_{1}!\":!\"{2}", tmpList[count].ID, count, tmpList[count].EquipmentGroup != null ? tmpList[count].EquipmentGroup.ToString() : "null");
                tmpA[8] = string.Format("AllLineCount!\":!\"{0}", tmpList.Count);
                tmpA[9] = string.Format("ID!\":!\"{0}", tmpList[count].ID);
            }
            else
            {
                tmpA[0] = string.Format("TxLineType_{0}_{1}!\":!\"null", tmpList[0].ID, 0);
                tmpA[1] = string.Format("TxNumberOfLine_{0}_{1}!\":!\"null", tmpList[0].ID, 0);
                tmpA[2] = string.Format("TxManufacturer_{0}_{1}!\":!\"null", tmpList[0].ID, 0);
                tmpA[3] = string.Format("TxModel_{0}_{1}!\":!\"null", tmpList[0].ID, 0);
                tmpA[4] = string.Format("TxDiameter_{0}_{1}!\":!\"null", tmpList[0].ID, 0);
                tmpA[5] = string.Format("TxLength_{0}_{1}!\":!\"null", tmpList[0].ID, 0);
                tmpA[6] = string.Format("IsExist_{0}_{1}!\":!\"null", tmpList[0].ID, 0);
                tmpA[7] = string.Format("EquipmentGroup_{0}_{1}!\":!\"null", tmpList[0].ID, 0);
                tmpA[8] = string.Format("AllLineCount!\":!\"{0}", 0);
                tmpA[9] = string.Format("ID!\":!\"{0}", tmpList[0].ID);
            }
            return tmpA;
        }

        #region EquipmentDataClass
        public class AntennaEquipment 
        {
            public int? Quantity { get; set; }
            public int? RADCenter { get; set; }
            public string Azimuth { get; set; }
            public string Manufacturer { get; set; }
            public string Model { get; set; }
            public decimal? Weight { get; set; }
            public string Dimensions_L { get; set; }
            public string Dimensions_W { get; set; }
            public string Dimensions_H { get; set; }
            public string MountingType { get; set; }
            public string MountingDimensions_L { get; set; }
            public string MountingDimensions_W { get; set; }
            public string MountingDimensions_H { get; set; }
            public string TxTransmit { get; set; }
            public string RxTransmit { get; set; }
            public string PowerOutput { get; set; }
            public bool IsExist { get; set; }
            public byte? EquipmentGroup { get; set; }
            public int ID { get; set; }
        }

        public class AntennaEquipmentTxLineType
        {
            public string TxLineType { get; set; }
            public int? TxNumberOfLine { get; set; }
            public string TxManufacturer { get; set; }
            public string TxModel { get; set; }
            public string TxDiameter { get; set; }
            public string TxLength { get; set; }
            public bool IsExist { get; set; }
            public byte? EquipmentGroup { get; set; }
            public int ID { get; set; }
        }

        public class AmplifierEquipment
        {
            public int? Quantity { get; set; }
            public string Manufacturer { get; set; }
            public string Model { get; set; }
            public decimal? Weight { get; set; }
            public string Dimensions_L { get; set; }
            public string Dimensions_W { get; set; }
            public string Dimensions_H { get; set; }
            public bool? IsExist { get; set; }
            public byte? EquipmentGroup { get; set; }
        }

        public class GPSEquipment
        {
            public bool IsInitial {get;set;}
            public int? Quantity {get;set;}
            public string Manufacturer {get;set;}
            public string Model {get;set;}
            public string InstalledLocation {get;set;}
            public bool IsExist {get;set;}
            public byte? EquipmentGroup {get;set;}
            public int? RADCenter {get;set;}
            public decimal? Weight {get;set;}
            public string Dimensions_L {get;set;}
            public string Dimensions_W {get;set;}
            public string Dimensions_H {get;set;}
            public string TMO_AppID { get; set; }
        }

        public class MicrowaveEquipment
        {
            public int? Quantity { get; set; }
            public int? RADCenter { get; set; }
            public string Azimuth { get; set; }
            public string Manufacturer { get; set; }
            public string Model { get; set; }
            public decimal? Weight { get; set; }
            public string Diameter { get; set; }
            public string Mounting { get; set; }
            public bool IsExist { get; set; }
            public byte? EquipmentGroup { get; set; }
        }

        public class OtherEquipment
        {
            public int? Quantity { get; set; }
            public int? RADCenter { get; set; }
            public string Azimuth { get; set; }
            public string Manufacturer { get; set; }
            public string Model { get; set; }
            public decimal? Weight { get; set; }
            public string Dimensions { get; set; }
            public string MountingType { get; set; }
            public string Dimensions_L { get; set; }
            public string Dimensions_W { get; set; }
            public string Dimensions_H { get; set; }
            public string MountingLocation { get; set; }
            public string MountEquipDimensionL { get; set; }
            public string MountEquipDimensionW { get; set; }
            public string MountEquipDimensionH { get; set; }
            public string EquipmentPurpose { get; set; }
            public string EquipmentNotes { get; set; }
            public bool IsExist { get; set; }
            public byte? EquipmentGroup { get; set; }
        }

        public class GspEquipment
        {
            public string Dimensions_W {get;set;}
            public string Dimensions_L {get;set;}
            public string Manufacturer {get;set;}
            public string Size {get;set;}
            public string TypeOfModulation {get;set;}
            public int? ThreedB {get;set;}
            public int? TwentydB {get;set;}
            public bool IsExist {get;set;}
            public string EquipmentGroup { get; set; }
        }

        public class ContactCustomerInfomation
        {
            public string Submitter_Title { get; set; }
            public string Legal_Entity_Name { get; set; }
            public string DBA { get; set; }
            public string Customer_Site_Number { get; set; }
            public bool FCC_Licensed { get; set; }
            public string FCC_Licensed_Number { get; set; }
            public string Address { get; set; }
            public string Address2 { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string Zip { get; set; }
            public int ContactTypeID { get; set; }
            public int? CountyID { get; set; }
            public string County { get; set; }
        }
        #endregion EquipmentDataClass
    }
}
