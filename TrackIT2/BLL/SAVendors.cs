﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TrackIT2.DAL;

namespace TrackIT2.BLL
{
    public static class SAVendors
    {
        public static List<SAVendorsInfo> GetVendors(int? vendorID = null)
        {
            List<SAVendorsInfo> lstVendors = new List<SAVendorsInfo>();
            using (CPTTEntities ce = new CPTTEntities())
            {
                var query = (from v in ce.sa_vendors
                            where (v.is_preferred == true) ||
                            ((vendorID != null) && (v.id == vendorID.Value) && (v.is_preferred == false))
                            orderby v.sa_vendor
                            select new SAVendorsInfo
                            {
                                ID = v.id,
                                IsPreferred = v.is_preferred,
                                SAVendor = v.sa_vendor
                            }).ToList();
                if (query.Count > 0)
                {
                    lstVendors.AddRange(query);
                }
            }
            return lstVendors;
        }

        public static List<SAVendorsInfo> GetAllVendors()
        {
            List<SAVendorsInfo> lstVendors = new List<SAVendorsInfo>();
            using (CPTTEntities ce = new CPTTEntities())
            {
                var query = (from v in ce.sa_vendors
                            orderby v.sa_vendor
                            select new SAVendorsInfo
                            {
                                ID = v.id,
                                IsPreferred = v.is_preferred,
                                SAVendor = v.sa_vendor
                            }).ToList();
                if (query.Count > 0)
                {
                    lstVendors.AddRange(query.ToList<SAVendorsInfo>());
                }
            }
            return lstVendors;
        }
    }



    public class SAVendorsInfo
    {
        public SAVendorsInfo()
        {
            
        }

        public int ID { get; set; }
        public Boolean? IsPreferred { get; set; }
        public string SAVendor { get; set; }
        public string SAVendorName
        {
            get
            {
                string ret = SAVendor;
                if (IsPreferred.HasValue && !(IsPreferred.Value))
                {
                    ret += " (historical)";
                }
                return ret;
            }
        }
    }
}