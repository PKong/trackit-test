﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.Objects;

namespace TrackIT2.Dashboard.widgets
{
    public partial class SavedLinks : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           Response.Cache.SetCacheability(HttpCacheability.NoCache); 
           StringWriter sw = new StringWriter();
            HtmlTextWriter writer = new HtmlTextWriter(sw);
            UserProfile profile = UserProfile.GetUserProfile();
            TrackIT2.Objects.SavedLinks links = profile.SavedLinks;

            Response.ContentType = "text/html";
            writer.WriteBeginTag("script");
            writer.WriteAttribute("type", "text/javascript");
            writer.WriteAttribute("src", "/Scripts/widget-script-saved-links.js");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteEndTag("script");

            writer.WriteLine();
            writer.WriteLine();

            writer.WriteBeginTag("div");
            writer.WriteAttribute("class", "widget-content-window");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteLine();
            writer.Indent++;

            writer.WriteBeginTag("ul");
            writer.WriteAttribute("class", "links-list");
            // Add 'id' attribute into element
            writer.WriteAttribute("id", "widgetSavedLinks");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteLine();
            writer.Indent++;

            // All users will have the following links always in their list:
            // T-Mobile Towers Home, Tool Shed            
            writer.WriteBeginTag("li");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteLine();
            writer.Indent++;

            writer.WriteBeginTag("a");
            writer.WriteAttribute("class", "links-list-item");
            writer.WriteAttribute("href", "http://www.t-mobiletowers.com");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteLine();
            writer.Indent++;

            writer.WriteBeginTag("span");
            writer.WriteAttribute("class", "link-title");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.Write("T-Mobile Towers Home");
            writer.WriteEndTag("span");
            writer.WriteLine();

            writer.WriteBeginTag("span");
            writer.WriteAttribute("class", "link-description");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.Write("T-Mobile Towers Home Page");
            writer.WriteEndTag("span");
            writer.WriteLine();

            writer.Indent--;
            writer.WriteEndTag("a");
            writer.WriteLine();
            writer.Indent--;
            writer.WriteEndTag("li");
            writer.WriteLine();

            writer.WriteBeginTag("li");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteLine();
            writer.Indent++;

            writer.WriteBeginTag("a");
            writer.WriteAttribute("class", "links-list-item");
            writer.WriteAttribute("href", "http://home.eng.t-mobile.com/Projects/iDoFinanceAndStrategy/ND/TMOTowers/Documents/Forms/AllItems.aspx?RootFolder=%2FProjects%2FiDoFinanceAndStrategy%2FND%2FTMOTowers%2FDocuments%2FToolShed&View=%7b41293153%2d58AB%2d48FB%2d9101%2dFA26CD47AE1E%7d");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteLine();
            writer.Indent++;

            writer.WriteBeginTag("span");
            writer.WriteAttribute("class", "link-title");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.Write("Tool Shed");
            writer.WriteEndTag("span");
            writer.WriteLine();

            writer.WriteBeginTag("span");
            writer.WriteAttribute("class", "link-description");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.Write("Tool Shed");
            writer.WriteEndTag("span");
            writer.WriteLine();

            writer.Indent--;
            writer.WriteEndTag("a");
            writer.WriteLine();
            writer.Indent--;
            writer.WriteEndTag("li");
            writer.WriteLine();

            writer.WriteBeginTag("li");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteLine();
            writer.Indent++;

            writer.WriteBeginTag("a");
            writer.WriteAttribute("class", "links-list-item");
            writer.WriteAttribute("href", "http://home.eng.t-mobile.com/Projects/iDoFinanceAndStrategy/ND/TMOTowers/Documents/Forms/AllItems.aspx?RootFolder=%2fProjects%2fiDoFinanceAndStrategy%2fND%2fTMOTowers%2fDocuments%2fToolShed%2fTool%20guides%20%28Step%20by%20Step%20how%20to%20directions%20by%20Application%29%2fTrackiT%202&FolderCTID=0x012000C10819110906C645965BC6A332B4AE4E&View=%7b41293153%2d58AB%2d48FB%2d9101%2dFA26CD47AE1E%7d");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteLine();
            writer.Indent++;

            writer.WriteBeginTag("span");
            writer.WriteAttribute("class", "link-title");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.Write("Training Guide");
            writer.WriteEndTag("span");
            writer.WriteLine();

            writer.WriteBeginTag("span");
            writer.WriteAttribute("class", "link-description");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.Write("Training Guide");
            writer.WriteEndTag("span");
            writer.WriteLine();

            writer.Indent--;
            writer.WriteEndTag("a");
            writer.WriteLine();
            writer.Indent--;
            writer.WriteEndTag("li");
            writer.WriteLine();

            if (links.LinkItems != null && links.LinkItems.Count > 0)
            {
                foreach (SavedLink currLink in profile.SavedLinks.LinkItems)
                {
                    writer.WriteBeginTag("li");
                    writer.Write(HtmlTextWriter.TagRightChar);
                    writer.WriteLine();
                    writer.Indent++;

                    writer.WriteBeginTag("a");
                    writer.WriteAttribute("class", "links-list-item");
                    writer.WriteAttribute("href", currLink.url);
                    writer.Write(HtmlTextWriter.TagRightChar);
                    writer.WriteLine();
                    writer.Indent++;

                    writer.WriteBeginTag("span");
                    writer.WriteAttribute("class", "link-title");
                    writer.Write(HtmlTextWriter.TagRightChar);
                    writer.Write(currLink.title);
                    writer.WriteEndTag("span");
                    writer.WriteLine();

                    writer.WriteBeginTag("span");
                    writer.WriteAttribute("class", "link-description");
                    writer.Write(HtmlTextWriter.TagRightChar);
                    if (!string.IsNullOrEmpty(currLink.description))
                    {
                        writer.Write(currLink.description);
                    }
                    else
                    {
                        writer.Write("No description.");
                    }
                    writer.WriteEndTag("span");
                    writer.WriteLine();

                    writer.Indent--;
                    writer.WriteEndTag("a");
                    writer.WriteLine();
                    writer.Indent--;
                    writer.WriteEndTag("li");
                    writer.WriteLine();
                }
            }

            writer.Indent--;
            writer.WriteEndTag("ul");
            writer.WriteLine();
            writer.Indent--;
            writer.WriteEndTag("div");

            writer.WriteLine();
            writer.WriteLine();

            // Insert an iframe for Edit Data from Account/SavedLink page, 20 Dec 2011

            writer.WriteBeginTag("div");
            writer.WriteAttribute("class", "edit-widget-iframe");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteLine();
            writer.Indent++;

            writer.WriteBeginTag("iframe");
            writer.WriteAttribute("id", "iframeSavedLink");
            writer.WriteAttribute("src", "/Account/SavedLinks.aspx");
            writer.WriteAttribute("width", "100%");
            writer.WriteAttribute("height", "100%");
            writer.WriteAttribute("align", "center");
            writer.WriteAttribute("frameborder", "0");

            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteEndTag("iframe");
            writer.WriteLine();
            writer.Indent--;

            writer.WriteEndTag("div");

            /// .end insert iframe

            writer.WriteBeginTag("div");
            writer.WriteAttribute("class", "widget-footer");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteLine();
            writer.Indent++;

            writer.WriteBeginTag("a");
            writer.WriteAttribute("id", "edit_mysavedlinks");
            writer.WriteAttribute("href", "javascript:void(0)");
            writer.WriteAttribute("class", "saved-links-edit-widget-button");
            writer.WriteAttribute("class", "widgetRefresh");
            //writer.WriteAttribute("href", "/Account/SavedLinks.aspx");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.Write("Edit Widget");
            writer.WriteEndTag("a");
            writer.WriteLine();
            writer.Indent--;

            writer.WriteEndTag("div");

            Response.Write(sw.ToString());

        }
    }

}