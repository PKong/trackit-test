﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using TrackIT2.BLL;
using TrackIT2.DAL;

namespace TrackIT2.Dashboard.widgets
{
    public partial class MyAssignedIssues : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            var sw = new StringWriter();
            var writer = new HtmlTextWriter(sw);

            var ce = new CPTTEntities();
            var userId = (int) Session["userId"];
            var myIssues = ce.issue_tracker.Where(i => ((i.specialist_user_id == userId) || (i.senior_pm_user_id == userId)) && (i.issue_statuses.issue_status.ToLower() == "open")).Distinct();

            writer.WriteBeginTag("div");
            writer.WriteAttribute("class", "widget-content-window");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteLine();
            writer.Indent++;

            writer.WriteBeginTag("table");
            writer.WriteAttribute("id", "widgetMyAssignedIssues");
            writer.WriteAttribute("class", "widget-table");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteLine();

            writer.Indent++;
            writer.WriteBeginTag("thead");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteLine();

            writer.Indent++;
            writer.WriteBeginTag("tr");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteLine();

            writer.Indent++;
            writer.WriteBeginTag("td");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.Write("Issue Title & Link");
            writer.WriteEndTag("td");
            writer.WriteLine();

            writer.WriteBeginTag("td");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.Write("Submitted");
            writer.WriteEndTag("td");
            writer.WriteLine();
            writer.Indent--;

            writer.WriteEndTag("tr");
            writer.WriteLine();
            writer.Indent--;

            writer.WriteEndTag("thead");
            writer.WriteLine();
            writer.Indent--;

            writer.WriteBeginTag("tbody");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteLine();

            if (myIssues.Any())
            {
                foreach (var issue in myIssues)
                {
                    writer.Indent++;
                    writer.WriteBeginTag("tr");
                    writer.Write(HtmlTextWriter.TagRightChar);
                    writer.WriteLine();

                    writer.Indent++;
                    writer.WriteBeginTag("td");
                    writer.Write(HtmlTextWriter.TagRightChar);

                    writer.WriteBeginTag("a");
                    writer.WriteAttribute("href", "/IssueTracker/Edit.aspx?id=" + issue.id);
                    writer.Write(HtmlTextWriter.TagRightChar);

                    string issueTitle = Globals.ISSUE_TRACKER_ID_PREFIX + issue.id.ToString(CultureInfo.InvariantCulture);
                    if (issue.site != null)
                    {
                        issueTitle += " / " + issue.site.site_uid + " / " + issue.site.site_name;
                    }
                    else
                    {
                        issueTitle += " / " + Globals.ISSUE_TRACKER_NO_SITE_NAME;
                    }

                    writer.Write(issueTitle);
                    writer.WriteEndTag("a");

                    writer.WriteEndTag("td");
                    writer.WriteLine();

                    writer.WriteBeginTag("td");
                    writer.Write(HtmlTextWriter.TagRightChar);

                    string submittedDate = "None";
                    DateTime dt;
                    var blHasDate = DateTime.TryParse(issue.created_at.ToString(), out dt);
                    if (blHasDate) submittedDate = dt.ToString("MM/dd/yyyy");

                    writer.Write(submittedDate);
                    writer.WriteEndTag("td");
                    writer.WriteLine();
                    writer.Indent--;

                    writer.WriteEndTag("tr");
                    writer.WriteLine();
                    writer.Indent--;
                }
            }

            writer.WriteEndTag("tbody");
            writer.WriteLine();
            writer.Indent--;

            writer.WriteEndTag("table");
            writer.WriteLine();
            writer.Indent--;

            writer.WriteEndTag("div");

            writer.WriteLine();
            writer.WriteLine();

            writer.WriteBeginTag("div");
            writer.WriteAttribute("class", "widget-footer");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteLine();
            writer.Indent++;

            writer.WriteBeginTag("p");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.Write("Click on the Issue Title to view full Issue details.");
            writer.WriteEndTag("p");
            writer.WriteLine();
            writer.Indent--;

            writer.WriteEndTag("div");

            Response.Write(sw.ToString());
        }
    }
}