﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RecentComments.aspx.cs" Inherits="TrackIT2.Dashboard.widgets.RecentComments" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <title></title>

   <!-- Styles -->
   <%: Styles.Render("~/Styles/recent_comments") %>

   <!-- Scripts -->
   <%: Scripts.Render("~/Scripts/recent_comments") %>
</head>

<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="sm" runat="server" ScriptMode="Release"/> 
    
<div class="widget-content-window no-options" style="height:auto">
    <div id="divNoData" runat="server" clientidmode="Static" class="no-chart" style="height:20px">
           No records exist for the filter specified.
        </div>
    <div id="commentDiv" style="width:auto; height:100%; margin:0px 15px 0px 15px; padding-top:20px;">   
         <asp:UpdatePanel id="upComments" updatemode="Always" runat="server">
            <ContentTemplate>
                <asp:Repeater ID="rptComments" runat="server">
                    <ItemTemplate>
                        <div class="comment">				
                                <div class="balloon">
                                    <p><asp:Label id="lblArea" runat="server" ForeColor="#008DA8" /><asp:Label id="lblComment" runat="server" /></p>
                                    <div class="balloon-bottom"></div>
                                </div>
                                <div class="author-info" style="margin-top:15px; font-size:10pt; color:#a1a1a1;">
                                    <asp:Image ID="imgEmployee"  Width="50" Height="50" ImageUrl="/images/avatars/default.png"
                                            ToolTip="Employee Profile Image" runat="server" />
                                    <div id="divCommentInfo" runat="server" class="divCommentInfo">
                                        <p><asp:Label id="lblEmployee" runat="server" />, <asp:Label id="lblDateCreated" runat="server" /></p>
                                    </div>
                                    <div id="divCommentLink" runat="server" class="divCommentLink">
                                        <asp:HyperLink ID="customerLink" runat="server" CssClass="recentCommentInfo" ></asp:HyperLink> 
                                        |
                                        <asp:HyperLink ID="modApp" runat="server" CssClass="recentCommentInfo" ></asp:HyperLink> 
                                        |
                                        <asp:HyperLink ID="SiteLink" runat="server" CssClass="recentCommentInfo" ></asp:HyperLink>
                                    </div>
                                </div>
                                <div class="relatedAppInfo"><asp:HyperLink ID="typeLink" runat="server" CssClass="recentCommentInfo" ></asp:HyperLink></div>
                        </div>   
                    </ItemTemplate>
                </asp:Repeater>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div style="clear:both"></div>
    </div>
</div> 
    </form>
</body>
</html>
