﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Xml;
using TrackIT2.DAL;
using TrackIT2.BLL;

namespace TrackIT2.Dashboard.widgets
{
    /// <summary>
    /// LeaseAppsByTypeData
    /// </summary>
    /// <remarks>
    /// TODO: Refactor and tidy the code, as some XML not required? (WL: 12/14/2011).
    /// </remarks>
   public partial class LeaseAppsByTypeData : System.Web.UI.Page
   {
      protected void Page_Load(object sender, EventArgs e)
      {
         Response.Cache.SetCacheability(HttpCacheability.NoCache);
         
         // Create a new XmlTextWriter instance that writes directly to the
         // output stream.
         Response.ContentType = "text/xml";
         XmlTextWriter writer = new XmlTextWriter(Response.OutputStream, Encoding.UTF8);
         List<lease_applications> lstCurrentUserApps = new List<lease_applications>();
         List<leaseapp_types> typeNames = new List<leaseapp_types>();
          user curUser = null;
         int appCount;
         if (Session["userId"] != null)
         {
             curUser = BLL.User.Search(int.Parse(Session["userId"].ToString()));
         }
         if (curUser != null)
         {
             using (CPTTEntities ce = new CPTTEntities())
             {
                 //Getting only lease application that assigned to current log-in user.
                 var queryLA = from la in ce.lease_applications
                               where (la.tmo_pm_emp_id == curUser.id) || (la.tmo_specialist_current_emp_id == curUser.id)
                                     || (la.tmo_specialist_initial_emp_id == curUser.id) || (la.colocation_coordinator_emp_id == curUser.id)
                               select la;
                 if (queryLA.ToList().Count > 0)
                 {
                     lstCurrentUserApps = queryLA.ToList<lease_applications>();
                 }
                typeNames = ce.leaseapp_types.ToList();

                writer.WriteStartDocument();

                // Creating the <chart> element
                writer.WriteStartElement("chart");

                // Creating the <categories> element
                writer.WriteStartElement("categories");
                if (lstCurrentUserApps.Count > 0)
                {
                    foreach (leaseapp_types currType in typeNames)
                    {
                        writer.WriteElementString("item", currType.lease_application_type);
                    }
                }
                // Close <categories> element and document
                writer.WriteEndElement();

                // Creating the <series> element
                writer.WriteStartElement("series");
                if (lstCurrentUserApps.Count > 0)
                {
                    // Creating the data elements
                    foreach (leaseapp_types currType in typeNames)
                    {
                        appCount = lstCurrentUserApps
                                    .Where(la => la.leaseapp_type_id.Value.Equals(currType.id))
                                    .Count();
                        writer.WriteStartElement("item");
                        writer.WriteElementString("name", currType.lease_application_type);
                        writer.WriteStartElement("data");
                        writer.WriteElementString("point", appCount.ToString());
                        writer.WriteEndElement();
                        writer.WriteEndElement();
                    }
                }
                

                // Close <series> element and document
                writer.WriteEndElement();

                // Close <chart> element and document
                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Close();
            }
        }
      }
   }
}