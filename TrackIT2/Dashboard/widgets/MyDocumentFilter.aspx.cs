﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.Objects;
using System.IO;

namespace TrackIT2.Dashboard.widgets
{
    public partial class MyDocumentFilter : System.Web.UI.Page
    {
        static SavedFilter le = new SavedFilter("Lease Application Fields Missing Documents Filter", "/dashboard/widgets/MissingDocuments.aspx?filter=le", "le", "LeaseApplicationsMissingDocument");
        static SavedFilter tm = new SavedFilter("Tower Modification Fields Missing Documents Filter", "/dashboard/widgets/MissingDocuments.aspx?filter=to", "to", "TowerModificationMissingDocument");
        static SavedFilter sdp = new SavedFilter("Site Data Package Fields Missing Documents Filter", "/dashboard/widgets/MissingDocuments.aspx?filter=to", "sa", "SiteDataPackageMissingDocument");
        static SavedFilter all = new SavedFilter("All Fields Missing Documents Filter", "/dashboard/widgets/MissingDocuments.aspx?filter=al", "al", "AllMissingDocument");
        static SavedFilters sf = new SavedFilters();
        static int userID = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            sf.SavedFilterItems = new List<SavedFilter>();
            sf.SavedFilterItems.Add(all);
            sf.SavedFilterItems.Add(le);
            sf.SavedFilterItems.Add(sdp);
            sf.SavedFilterItems.Add(tm);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            userID = (int)Session["userId"];

            bool hasSavedFilters = false;
            string widgetId = Request.QueryString["widgetid"];
            string title = null;
            if (!String.IsNullOrEmpty(Request.QueryString["title"]))
            {
                title = "Fields Missing Documents";
            }
            //title = "Fields Missing Documents";

            StringWriter sw = new StringWriter();
            HtmlTextWriter writer = new HtmlTextWriter(sw);

            UserProfile profile = UserProfile.GetUserProfile();
            if (sf.SavedFilterItems != null && sf.SavedFilterItems.Count > 0) hasSavedFilters = true;

            writer.WriteBeginTag("script");
            writer.WriteAttribute("type", "text/javascript");
            writer.WriteAttribute("src", "/Scripts/widget-script.my-saved-filters-list-view.js");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteEndTag("script");
            writer.WriteLine();

            writer.WriteBeginTag("script");
            writer.WriteAttribute("type", "text/javascript");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteLine();
            writer.Indent++;
            writer.Write("$(function () {");
            writer.WriteLine();
            if (!String.IsNullOrEmpty(title))
            {
                writer.Write("setupMySavedFilterListView($('#" + widgetId + "'), false, '" + title + "');");
            }
            else
            {
                writer.Write("setupMySavedFilterListView($('#" + widgetId + "'), true, null);");
            }
            writer.WriteLine();
            writer.Write("});");
            writer.WriteLine();
            writer.Indent--;
            writer.WriteEndTag("script");
            writer.WriteLine();

            writer.WriteLine();

            writer.WriteBeginTag("div");
            writer.WriteAttribute("class", "widget-options");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteLine();
            writer.Indent++;

            writer.WriteBeginTag("div");
            writer.WriteAttribute("class", "widget-options-wrapper");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteLine();
            writer.Indent++;

            writer.WriteBeginTag("p");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.Write("Please select one of the Filters that you would like to display in this widget.");
            writer.WriteEndTag("p");

            writer.WriteBeginTag("select");
            writer.WriteAttribute("id", "selectFilter");
            writer.WriteAttribute("class", "sel-my-saved-filters");
            writer.Write(HtmlTextWriter.TagRightChar);

            writer.WriteLine();
            writer.Indent++;
            writer.WriteBeginTag("option");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.Write("Select a Fields Missing Documents Filter");
            writer.WriteEndTag("option");

            if (hasSavedFilters)
            {
                foreach (SavedFilter filter in sf.SavedFilterItems)
                {
                    writer.WriteBeginTag("option");
                    writer.WriteAttribute("value", filter.listView);
                    writer.Write(HtmlTextWriter.TagRightChar);
                    writer.Write(filter.title);
                    writer.WriteEndTag("option");
                }
            }

            writer.WriteEndTag("select");
            writer.WriteLine();

            writer.WriteBeginTag("p");
            writer.WriteAttribute("class", "dashboard-inline-error");
            writer.WriteAttribute("style", "display:none;");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.Write("Select a Document Filter to display!");
            writer.WriteEndTag("p");
            writer.WriteLine();

            // TODO: may add this feature
            //  add assign to me
            //writer.WriteBeginTag("p");
            //writer.WriteAttribute("style", "margin-top:10px");
            //writer.Write(HtmlTextWriter.TagRightChar);
            //writer.Write("Only display Document, from items where I'm involved ");
            //writer.WriteBeginTag("input");
            //writer.WriteAttribute("type", "checkbox");
            //writer.WriteAttribute("class", "isAssigncheckbox");
            //writer.WriteAttribute("id", "isAssign");
            //writer.Write(HtmlTextWriter.TagRightChar);
            //writer.WriteEndTag("p");
            //  end add assign to me

            writer.Indent--;
            writer.WriteEndTag("div");

            writer.WriteLine();
            writer.Indent--;

            writer.WriteEndTag("div");

            writer.WriteLine();
            writer.WriteLine();

            writer.WriteBeginTag("div");
            writer.WriteAttribute("class", "widget-content-window");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteLine();
            writer.Indent++;

            writer.WriteBeginTag("div");
            writer.WriteAttribute("class", "my-saved-filters-list-view-container");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteEndTag("div");

            writer.WriteLine();
            writer.Indent--;

            writer.WriteEndTag("div");

            writer.WriteLine();
            writer.WriteLine();

            writer.WriteBeginTag("div");
            writer.WriteAttribute("class", "widget-footer");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteLine();
            writer.Indent++;

            writer.WriteBeginTag("div");
            writer.WriteAttribute("class", "widget-footer-nav-container");
            writer.WriteAttribute("style", "display:none;");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.Indent++;

            writer.WriteBeginTag("a");
            writer.WriteAttribute("class", "widget-nav-link widget-nav-link-first");
            writer.WriteAttribute("href", "javascript:void(0);");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.Write("«");
            writer.WriteEndTag("a");

            writer.WriteBeginTag("a");
            writer.WriteAttribute("class", "widget-nav-link widget-nav-link-previous");
            writer.WriteAttribute("href", "javascript:void(0);");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.Write("‹");
            writer.WriteEndTag("a");

            writer.WriteBeginTag("span");
            writer.WriteAttribute("class", "widget-nav-links-page-count");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteEndTag("span");

            writer.WriteBeginTag("a");
            writer.WriteAttribute("class", "widget-nav-link widget-nav-link-next");
            writer.WriteAttribute("href", "javascript:void(0);");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.Write("›");
            writer.WriteEndTag("a");

            writer.WriteBeginTag("a");
            writer.WriteAttribute("class", "widget-nav-link widget-nav-link-last");
            writer.WriteAttribute("href", "javascript:void(0);");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.Write("»");
            writer.WriteEndTag("a");

            writer.Indent--;
            writer.WriteEndTag("div");

            writer.WriteBeginTag("a");
            writer.WriteAttribute("class", "edit-widget-button");
            writer.WriteAttribute("href", "javascript:void(0);");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.Write("Edit Widget");
            writer.WriteEndTag("a");

            writer.WriteBeginTag("div");
            writer.WriteAttribute("class", "widget-options-bar");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteLine();
            writer.Indent++;

            writer.WriteBeginTag("a");
            writer.WriteAttribute("class", "widget-options-cancel");
            writer.WriteAttribute("href", "javascript:void(0);");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.Write("Cancel");
            writer.WriteEndTag("a");

            writer.WriteLine();

            writer.WriteBeginTag("a");
            writer.WriteAttribute("class", "save-widget-button");
            writer.WriteAttribute("href", "javascript:void(0);");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.Write("Save Widget");
            writer.WriteEndTag("a");

            writer.WriteLine();
            writer.Indent--;

            writer.WriteEndTag("div");
            writer.WriteLine();
            writer.Indent--;

            writer.WriteEndTag("div");

            Response.Write(sw.ToString());
        }
    }
}