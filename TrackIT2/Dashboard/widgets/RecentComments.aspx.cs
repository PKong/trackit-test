﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.DAL;
using System.IO;
using TrackIT2.BLL;

namespace TrackIT2.Dashboard.widgets
{
    public partial class RecentComments : System.Web.UI.Page
    {
        string commentType = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            if (Request.QueryString["type"] != null)
            {
                commentType = Request.QueryString["type"];
            }
            else
            {
                commentType = "al";
                this.Title = "All Recent Comment";
                this.Response.Flush();
            }
            var sw = new StringWriter();
            var writer = new HtmlTextWriter(sw);

            var userId = (int)Session["userId"];

            if (Request.QueryString["flag"] != null)
            {
                BindComments(userId);
            }
            else
            {
                BindComments(0);
            }

        }

        protected void BindComments(int userId)
        {
            List<LatestRecentComment> recentComment;

            if (commentType == "le")
            {
                recentComment = AllRecentComment.GetLatestLLeaseAppAndLeaseAdminComment(userId);
            }
            else if (commentType == "sa")
            {
                recentComment = AllRecentComment.GetLatestStructuralAnalysisComment(userId);
            }
            else if (commentType == "to")
            {
                recentComment = AllRecentComment.GetLatestTowerModComments(userId);
            }
            else
            {//get all recent comment
                recentComment = AllRecentComment.GetLatestRecentComment(userId);
            }

            if (recentComment.Count > 0)
            {
                ClientScript.RegisterStartupScript(typeof(Page), "HideDiv", "HideDivNoData('commentDiv');", true);

                rptComments.DataSource = recentComment;
                rptComments.ItemDataBound += new RepeaterItemEventHandler(rptComments_ItemDataBound_AllRecent);
                rptComments.DataBind();
            }
            else
            {
                ClientScript.RegisterStartupScript(typeof(Page), "HideDiv", "HideCommentDiv('commentDiv');", true);
            }
        }

        /// <summary>
        /// rptComments ItemDataBound - All Recent comment
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rptComments_ItemDataBound_AllRecent(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LatestRecentComment comment = (LatestRecentComment)e.Item.DataItem;

                Label lblDateCreated = (Label)e.Item.FindControl("lblDateCreated");
                lblDateCreated.Text = Utility.DateToString(comment.Timestamp);

                if (comment.LeaseAdmin == null)
                {
                    Label lblArea = (Label)e.Item.FindControl("lblArea");
                    if (comment.StructuralAnalysisID != null)
                    {
                        lblArea.Text = "SA: ";
                    }
                    else if (comment.LeaseAdmin != null)
                    {
                        lblArea.Text = "From LAdmin: ";
                    }

                    Label lblEmployee = (Label)e.Item.FindControl("lblEmployee");
                    Image imgEmployee = (Image)e.Item.FindControl("imgEmployee");

                    if (!string.IsNullOrEmpty(comment.CreatorName))
                    {
                        lblEmployee.Text = comment.CreatorName;
                    }
                    else
                    {
                        lblEmployee.Text = "No employee recorded.";
                    }

                    imgEmployee.ImageUrl = BLL.User.GetUserAvatarImage(comment.CreatorUsername);

                    Label lblComment = (Label)e.Item.FindControl("lblComment");
                    lblComment.Text = comment.Comments;

                    HyperLink customerLink = (HyperLink)e.Item.FindControl("customerLink");
                    if (comment.CustomerName != null)
                    {
                        customerLink.Text = comment.CustomerName;
                    }
                    else
                    {
                        customerLink.Text = "(No customer recorded.)";
                    }

                    HyperLink modApp = (HyperLink)e.Item.FindControl("modApp");
                    if (comment.AppType != null)
                    {
                        modApp.Text = comment.AppType;
                    }
                    else
                    {
                        modApp.Text = "(No type recorded.)";
                    }
                    HyperLink siteLink = (HyperLink)e.Item.FindControl("siteLink");
                    siteLink.Text = comment.SiteID;

                    if (comment.TowerModID != null)
                    {
                        string link = "TowerModifications/Edit.aspx?id=" + comment.TowerModID.ToString();
                        modApp.NavigateUrl = link;
                        customerLink.NavigateUrl = link;
                        siteLink.NavigateUrl = link;
                    }
                    else if (comment.StructuralAnalysisID != null)
                    {
                        string link = "LeaseApplications/Edit.aspx?id=" + comment.LeaseApplicationID.ToString() + "&saId=" + comment.StructuralAnalysisID + "";
                        modApp.NavigateUrl = link;
                        customerLink.NavigateUrl = link;
                        siteLink.NavigateUrl = link;
                    }
                    else if (comment.LeaseAdmin != null)
                    {
                        String link = "LeaseApplications/Edit.aspx?id=" + comment.LeaseApplicationID.ToString() + "&LeaseAdminId=" + comment.LeaseAdmin + "";
                        modApp.NavigateUrl = link;
                        customerLink.NavigateUrl = link;
                        siteLink.NavigateUrl = link;
                    }
                    else
                    {
                        string link = "LeaseApplications/Edit.aspx?id=" + comment.LeaseApplicationID.ToString();
                        modApp.NavigateUrl = link;
                        customerLink.NavigateUrl = link;
                        siteLink.NavigateUrl = link;
                    }
                }
                else
                {
                    Label lblArea = (Label)e.Item.FindControl("lblArea");
                    if (comment.LeaseAdmin != null)
                    {
                        lblArea.Text = "From LAdmin: ";
                    }

                    Label lblEmployee = (Label)e.Item.FindControl("lblEmployee");
                    Image imgEmployee = (Image)e.Item.FindControl("imgEmployee");

                    if (!string.IsNullOrEmpty(comment.CreatorName))
                    {
                        lblEmployee.Text = comment.CreatorName;
                    }
                    else
                    {
                        lblEmployee.Text = "No employee recorded.";
                    }

                    imgEmployee.ImageUrl = BLL.User.GetUserAvatarImage(comment.CreatorUsername);

                    Label lblComment = (Label)e.Item.FindControl("lblComment");
                    lblComment.Text = comment.Comments;

                    if (comment.LeaseApplicationID != null)
                    {
                        HyperLink customerLink = (HyperLink)e.Item.FindControl("customerLink");
                        if (comment.CustomerName != null)
                        {
                            customerLink.Text = comment.CustomerName;
                        }
                        else
                        {
                            customerLink.Text = "(No customer recorded.)";
                        }

                        HyperLink modApp = (HyperLink)e.Item.FindControl("modApp");
                        if (comment.AppType != null)
                        {
                            modApp.Text = comment.AppType;
                        }
                        else
                        {
                            modApp.Text = "(No type recorded.)";
                        }

                        HyperLink siteLink = (HyperLink)e.Item.FindControl("siteLink");
                        siteLink.Text = comment.SiteID;

                        if (comment.LeaseAdmin != null)
                        {
                            String link = "LeaseApplications/Edit.aspx?id=" + comment.LeaseApplicationID.ToString() + "&LeaseAdminId=" + comment.LeaseAdmin + "";
                            modApp.NavigateUrl = link;
                            customerLink.NavigateUrl = link;
                            siteLink.NavigateUrl = link;
                        }
                    }
                    else
                    {
                        Control div = (Control)e.Item.FindControl("divCommentLink");
                        div.Visible = false;
                    }
                }
            }
        }
        //-->
    }
}