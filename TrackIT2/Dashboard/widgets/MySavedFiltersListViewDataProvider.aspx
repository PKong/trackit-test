﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MySavedFiltersListViewDataProvider.aspx.cs" Inherits="TrackIT2.Dashboard.widgets.MySavedFiltersListViewDataProvider" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">

        <asp:Repeater ID="rptListView" runat="server">
            <HeaderTemplate>
                <table class="widget-table">
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Literal ID="litItem" runat="server" OnDataBinding="rptListView_OnDataBinding" />
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>

        <div id="divListViewNoRecords" runat="server" class="no-chart">
           No records exist for the filter specified.
        </div>

        <div id="hfWidgetNavState" style="visibility:hidden;" class="hf-widget-nav-state" runat="server"></div>
    </form>
</body>
</html>
