﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RecentDocument.aspx.cs" Inherits="TrackIT2.Dashboard.widgets.RecentDocument" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <title></title>
   <!-- Scripts -->
   <%: Scripts.Render("~/Scripts/recent_documents")%>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="sm" runat="server" ScriptMode="Release"/> 
    <div class="widget-content-window no-options recentDoc">
            <div id="divNoData" runat="server" clientidmode="Static" class="no-chart" style="height:20px">
               No records exist for the filter specified.
            </div>
            <div id="documentDiv" runat="server" style="width:auto; height:100%;">   
            <asp:UpdatePanel id="upDocuments" updatemode="Always" runat="server">
            <ContentTemplate>
                <asp:Repeater ID="rptDocuments" runat="server">
                    <HeaderTemplate>
                        <table width="100%" cellspacing="0" cellpadding="0" class="widget-table" style="border-spacing: 2px;">
	                        <thead>
		                        <tr>
			                        <td>Document Name</td>
			                        <td>Site ID</td>
			                        <td>Linked Record</td>
		                        </tr>
	                        </thead>
                            <tbody>
                    </HeaderTemplate> 
                    <ItemTemplate>
                	            <tr>
                                    <td ><asp:HyperLink id="lblDocName" runat="server" /></td>
			                        <td ><asp:HyperLink ID="lblSiteId" runat="server" /></td>
                                    <td style="text-align: center;">
                                        <asp:HyperLink id="lblLinkField" runat="server">
                                        <asp:Image id="imageLinkField" runat="server" ImageUrl="/images/icons/New document.png" Visible="false" />
                                    </asp:HyperLink>
                                    </td>
					            </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                	        </tbody>	
			            </table>
                    </FooterTemplate>
                </asp:Repeater>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div style="clear:both"></div>
        </div>
        </div>
    </form>
</body>
</html>
