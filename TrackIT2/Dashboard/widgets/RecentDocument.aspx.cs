﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.DAL;
using TrackIT2.BLL;
using System.IO;

namespace TrackIT2.Dashboard.widgets
{
    public partial class RecentDocument : System.Web.UI.Page
    {
        public const string DMS_LINK = "/DMS/#/{0}";
        public const string DMS_DOWNLOAD_HANDLER = "/DocumentDownload.axd?key={0}";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindDocument();
            }
        }

        protected void BindDocument()
        {
            List<documents_changelog> recentDocument;

            recentDocument = DocumentsChangeLog.GetDocumentsChangelog();

            if (recentDocument.Count > 0)
            {
                //ClientScript.RegisterStartupScript(typeof(Page), "HideDiv", "HideDivNoData('documentDiv');", true);
                divNoData.Visible = false;
                documentDiv.Visible = true;
                rptDocuments.DataSource = recentDocument;
                rptDocuments.ItemDataBound += new RepeaterItemEventHandler(rptDocument_ItemDataBound_AllRecent);
                rptDocuments.DataBind();
            }
            else
            {
                divNoData.Visible = true;
                documentDiv.Visible = false;
                //ClientScript.RegisterStartupScript(typeof(Page), "HideDiv", "HideCommentDiv('documentDiv');", true);
            }
        }

        protected void rptDocument_ItemDataBound_AllRecent(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                documents_changelog doc = (documents_changelog)e.Item.DataItem;

                HyperLink lblDocumentName = (HyperLink)e.Item.FindControl("lblDocName");
                lblDocumentName.Text = doc.document_name;
                if (!string.IsNullOrEmpty(doc.ref_key))
                {
                    lblDocumentName.NavigateUrl = string.Format(DMS_DOWNLOAD_HANDLER, doc.ref_key);
                }

                HyperLink lblSiteId = (HyperLink)e.Item.FindControl("lblSiteId");
                lblSiteId.Text = doc.site_uid;
                lblSiteId.NavigateUrl = string.Format(DMS_LINK, doc.site_uid);

                if (doc.is_link_field)
                {
                    HyperLink lblLinkField = (HyperLink)e.Item.FindControl("lblLinkField");
                    Image imageLinkField = (Image)e.Item.FindControl("imageLinkField");
                    imageLinkField.AlternateText = doc.link_record;
                    imageLinkField.Visible = true;
                    lblLinkField.NavigateUrl = doc.link_record;
                }
            }
        }
    }
}