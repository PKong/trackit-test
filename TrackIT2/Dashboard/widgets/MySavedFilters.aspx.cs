﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.Objects;

namespace TrackIT2.Dashboard.widgets
{
    public partial class MySavedFilters : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           Response.Cache.SetCacheability(HttpCacheability.NoCache); 
           StringWriter sw = new StringWriter();
            HtmlTextWriter writer = new HtmlTextWriter(sw);

            UserProfile profile = UserProfile.GetUserProfile();
            TrackIT2.Objects.SavedFilters filters = profile.SavedFilters;

            writer.WriteBeginTag("script");
            writer.WriteAttribute("type", "text/javascript");
            writer.WriteAttribute("src", "/Scripts/widget-script.my-saved-filters.js");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteEndTag("script");

            writer.WriteLine();
            writer.WriteLine();

            writer.WriteBeginTag("div");
            writer.WriteAttribute("class", "widget-content-window");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteLine();
            writer.Indent++;

            writer.WriteBeginTag("table");
            writer.WriteAttribute("id", "widgetMySavedFilter");
            writer.WriteAttribute("class", "widget-table deletable");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteLine();

            writer.Indent++;
            writer.WriteBeginTag("thead");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteLine();

            writer.Indent++;
            writer.WriteBeginTag("tr");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteLine();

            writer.Indent++;
            writer.WriteBeginTag("td");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.Write("Name and Link");
            writer.WriteEndTag("td");
            writer.WriteLine();

            writer.WriteBeginTag("td");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.Write("List View");
            writer.WriteEndTag("td");
            writer.WriteLine();
            writer.Indent--;

            writer.WriteEndTag("tr");
            writer.WriteLine();
            writer.Indent--;

            writer.WriteEndTag("thead");
            writer.WriteLine();
            writer.Indent--;

            writer.WriteBeginTag("tbody");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteLine();

            if (filters.SavedFilterItems != null && filters.SavedFilterItems.Count > 0)
            {
                foreach (SavedFilter filter in profile.SavedFilters.SavedFilterItems)
                {
                    writer.Indent++;
                    writer.WriteBeginTag("tr");
                    writer.Write(HtmlTextWriter.TagRightChar);
                    writer.WriteLine();

                    writer.Indent++;
                    writer.WriteBeginTag("td");
                    writer.Write(HtmlTextWriter.TagRightChar);

                    writer.WriteBeginTag("a");
                    writer.WriteAttribute("href", filter.url + "?filter=" + filter.filter);
                    writer.Write(HtmlTextWriter.TagRightChar);
                    writer.Write(filter.title);
                    writer.WriteEndTag("a");

                    writer.WriteEndTag("td");
                    writer.WriteLine();

                    writer.WriteBeginTag("td");
                    writer.Write(HtmlTextWriter.TagRightChar);
                    writer.Write(filter.listView);
                    writer.WriteEndTag("td");
                    writer.WriteLine();
                    writer.Indent--;

                    writer.WriteEndTag("tr");
                    writer.WriteLine();
                    writer.Indent--;
                }
            }

            writer.WriteEndTag("tbody");
            writer.WriteLine();
            writer.Indent--;

            writer.WriteEndTag("table");
            writer.WriteLine();
            writer.Indent--;

            writer.WriteEndTag("div");

            writer.WriteLine();
            writer.WriteLine();

            writer.WriteBeginTag("div");
            writer.WriteAttribute("class", "widget-footer");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteLine();
            writer.Indent++;

            writer.WriteBeginTag("p");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.Write("Click 'Save Filter' on list views to add a new one");
            writer.WriteEndTag("p");
            writer.WriteLine();
            writer.Indent--;

            writer.WriteEndTag("div");

            Response.Write(sw.ToString());
        }
    }
}