﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="Document Links | TrackIT2" Language="C#" MasterPageFile="~/Templates/Modal.Master" AutoEventWireup="true" CodeBehind="DocumentLinks.aspx.cs" Inherits="TrackIT2.DMS.DocumentLinks" %>
<%@ MasterType TypeName="TrackIT2.Templates.Modal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   <!-- Styles -->
   <%: Styles.Render("~/Styles/dms_document_link") %>
    
   <!-- Scripts -->
   <%: Scripts.Render("~/Scripts/dms_document_link") %>
   
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="message_alert"></div>
    <div id="DocumentLinkMain" class="create-container" style="margin: -15px auto 0 auto; text-align:left;">
        <div class="form_element">
            <div style=" width:100%; text-align:right;">
                <asp:Button ID="stepbtn_1" runat="server" ClientIDMode="Static"/>
            </div>

            <div id="header" runat="server" visible="false" style="width:100%; text-align:left;margin-top:10px">
                <h4>Field Location</h4>
                <asp:Label ID="front_docType" runat="server" Text="Location:" Font-Bold="True"></asp:Label>
                <asp:Label ID="docType" runat="server" CssClass="DocumentLinkHeader" 
                    Width="220px"></asp:Label>
                <br />
                <asp:Label ID="front_docFiled" runat="server" Text="Field:" Font-Bold="True"></asp:Label>
                <asp:Label ID="docFiled" runat="server" CssClass="DocumentLinkHeader" 
                    Width="220px"></asp:Label>
            </div>
            <div id="mainDocLink" style="width:100%;margin-top:65px">            
                <h4 style="margin-top:-45px" >Select Operation</h4>
                <ul class="menu" runat="server">
	                <li id="liUpload" runat="server" class="upload">
                        <a id="uploadDocBtn" href="javascript:void(0)">Upload Document</a>
                    </li>
	                <li id="liSelect" runat="server" class="selectlink">
                        <a id="selectDocBtn" href="javascript:void(0)">Link Document</a>
                    </li>
	                <li id="liRemove" runat="server" class="removelink">
                        <a id="removeDocBtn" href="javascript:void(0)">Unlink Document</a>
                    </li>
                </ul>
                
                <br />
                <br />
            </div>
            <div>
                
                <asp:Button ID="exitBtn" runat="server" CssClass="BtnCancleDocumentFooter" ClientIDMode="Static" Text="Cancel" OnClientClick="return false;" />
            </div>
            
        </div>
    </div>

    <!-- Modal & Hidden -->
    <div id="modelDocUpload" class="modal-container" style="overflow:hidden;"></div>
    <div id="modal-message"></div>
    <asp:HiddenField id="hidDocumentDownload" ClientIDMode="Static" runat="server"/>
    <asp:HiddenField id="hidLocationHash" ClientIDMode="Static" runat="server"/>
    <asp:HiddenField ID="hidBasePath" runat="server" Value="/" ClientIDMode="Static"/>
    <asp:HiddenField ID="hidType" runat="server" Value="" ClientIDMode="Static"/>
    <asp:HiddenField ID="hidTypeID" runat="server" Value="" ClientIDMode="Static"/>
    <asp:HiddenField ID="hidField" runat="server" Value="" ClientIDMode="Static"/>
    <asp:HiddenField ID="hidKey" runat="server" Value="" ClientIDMode="Static"/>
    <asp:HiddenField ID="hidFolder" runat="server" Value="" ClientIDMode="Static"/>
    <asp:HiddenField ID="hidTmpPath" runat="server" Value="" ClientIDMode="Static"/>
    <asp:HiddenField ID="hidStep2Path" runat="server" Value="" ClientIDMode="Static"/>
    <asp:HiddenField ID="hidUploadFlag" runat="server" Value="" ClientIDMode="Static"/>
    <asp:HiddenField ID="hidAutoRename" runat="server" Value="" ClientIDMode="Static"/>
    <asp:HiddenField ID="hidNewName" runat="server" Value="" ClientIDMode="Static"/>
    <asp:HiddenField ID="hidNewNameDescription" runat="server" Value="" ClientIDMode="Static"/>
    <asp:HiddenField ID="hidNewNameAppType" runat="server" Value="" ClientIDMode="Static"/>

    <!-- /Modal & Hidden -->

</asp:Content>
