﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Services;
using TrackIT2.DAL;
using TrackIT2.BLL;

namespace TrackIT2.DMS
{
    public partial class FolderTree : System.Web.UI.Page
    {
        bool folderSelectOnly = false;

        protected void Page_Load(object sender, EventArgs e)
        {            
            //Stop Caching in IE
            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            //Stop Caching in Firefox
            Response.Cache.SetNoStore();

            if (!Page.IsPostBack)
            {
               var folderList = BuildFolderList(); 
               
               // Use a partial HTML snippet response instead of a full document
               // since this page is only used within an AJAX call to build the
               // jqueryFileTree object.
               Response.Clear();
               Response.ClearHeaders();
               Response.AddHeader("Content-Type", "text/plain");
               Response.Write(folderList);
               Response.Flush();
               Response.End();                            
            }
        }

        private String AppendItem2UI(AWSObjectItem s3Item,String dir)
        {
            StringBuilder sb = new StringBuilder();
            string fileName = s3Item.FileName.Replace("/", "");
            string id = dir.Replace("/", "_") + fileName;
            if (s3Item.Type == AWSObjectItemType.Folder)
            {
                sb.Append(("\t" + "<li id='id_" + id + "' class=\"directory collapsed\"><a id='id_" + id + "_text' href=\"#\" rel='" + dir + fileName + "/'><span style=\"text-align:left; width:auto;\">" + fileName + "</span></a></li>" + "\n"));
            }
            else if (!folderSelectOnly)
            {
                // not create li file when this come from upload.
                string fileType = FileUtility.GetFileType(fileName).FileExtension;
                if (fileType == "?")
                {
                    fileType = "blank";
                }
                id = id.Replace(' ', '_');
                id = id.Replace('.', '_');
                var uploadDetail = string.Empty;

                if (s3Item.Metadata == null)
                {
                    // Get Object META Data
                    AWSObjectMetadata metaEdit;
                    using (AmazonDMS.S3Client = Amazon.AWSClientFactory.CreateAmazonS3Client())
                    {
                        // Get Object META
                        metaEdit = AmazonDMS.GetObjectMetadata(s3Item.Key);
                        string uploadDate = metaEdit.UploadDate.ToString(Globals.SHORT_DATE_FORMAT);
                        uploadDetail = string.Format(" ({0} - {1})", metaEdit.UploadedBy, uploadDate);
                    }

                }
                sb.Append(("\t" + "<li id=\"id_" + id + "\" class=\"file ext_" + fileType + "\"><a id=\"id_" + id + "_text\" href=\"#\" rel=\"" + dir + fileName + "\"><span style=\"text-align:left; width:auto;\">" + fileName + uploadDetail+"</span></a></li>" + "\n"));
            }

            return sb.ToString();
        }

        private List<AWSObjectItem> getNonVersionDocument(List<AWSObjectItem> lstItem)
        {
            List<AWSObjectItem> result = new List<AWSObjectItem>();
            foreach (AWSObjectItem item in lstItem)
            {
                if (item.Type == AWSObjectItemType.File)
                {
                    if (item.FileName.Split('.').Length > 1)
                    {
                        string fileRev = item.FileName.Split('.')[item.FileName.Split('.').Length - 1];
                        int intRev;
                        if (!int.TryParse(fileRev, out intRev))
                        {
                            result.Add(item);
                        }
                    }
                }
                else
                {
                    result.Add(item);
                }
            }
            return result;
        }

        [WebMethod(EnableSession = false)]
        public string FolderSearch()
        {
           var results = BuildFolderList();
           return results;
        }

       private string BuildFolderList()
       {
          var NO_DOCUMENT = true;
          var NO_DOCUMENT_LI = "\t" + "<li id=\"no-data-item-li\" class=\"no-document-li\"><span class=\"no-document-span\">No documents found.</span></a></li>" + "\n";

          var sb = new StringBuilder();
          sb.Append("<ul class=\"jqueryFileTree\" style=\"display: none;\">" + "\n");

          var siteID = String.Empty;
          var field = String.Empty;
          var dir = String.Empty;
          var tmpdir = String.Empty;
          var action = String.Empty;

          if (Request.Form["dir"] == null || Request.Form["dir"].Length <= 0)
          {
             dir = "";
          }
          else
          {
             dir = Request.Form["dir"];
          }

          // Note: arguments may reside in the form posting.
          // Get argument site from query string
          if (!String.IsNullOrEmpty(Request.QueryString["site"]) ||
              !String.IsNullOrEmpty(Request.Form["site"]))
          {
             siteID = !String.IsNullOrEmpty(Request.QueryString["site"])
                         ? Request.QueryString["site"]
                         : Request.Form["site"];

             if (siteID == "/")
             {
                siteID = "";
             }
          }

          // Get argument field from query string
          if (!String.IsNullOrEmpty(Request.QueryString["field"]) ||
              !String.IsNullOrEmpty(Request.Form["field"]))
          {
             field = !String.IsNullOrEmpty(Request.QueryString["field"])
                             ? Request.QueryString["field"]
                             : Request.Form["field"];
          }

          // Get action field from query string
          action = !String.IsNullOrEmpty(Request.QueryString["action"])
                              ? Request.QueryString["action"]
                              : Request.Form["action"];

          folderSelectOnly = !String.IsNullOrEmpty(Request.QueryString["folderSelectOnly"])
                              ? true
                              : false;

          bool bIsUpdload = false;
          if (action == "Upl")
          {
             bIsUpdload = true;
             folderSelectOnly = true;
             NO_DOCUMENT = false;
          }

          //in case folder not found will create new folder
          var queryItems = new List<AWSObjectItem>(); ;

          //check condition display tree for link document or not?
          if ((siteID != String.Empty) && ((dir == "") || (dir == siteID)))
          {
             var fileName = "";
             var id = "";

             // Create li for site folder
             if (dir == "")
             {
                fileName = siteID.Replace("/", "");
                id = dir.Replace("/", "_") + fileName;
                dir = fileName + "/";
                sb.Append(("\t" + "<li id='id_" + id + "' class=\"directory expanded\"><a id='id_" + id + "_text' href=\"javascript:void(0)\" rel='" + dir + "'><span style=\"text-align:left; width:auto;\">" + fileName + "</span></a>" + "\n"));
             }

             // Get subfolder
             DmsLinkDocumentStructure document = null;
             if (field != String.Empty)
             {
                document = DMSDocumentLinkHelper.DocumentField(field);
             }

             if (document != null || action == "move")
             {
                var objectSite = AmazonDMS.GetObjects(siteID, null, isGetAll: true);

                if (document != null && document.Location.HasValue)
                {
                   var siteFilter = siteID;

                   if (document != null)
                   {
                      siteFilter += document.Location.Value;
                   }

                   // If a a new site has been specified for the move action 
                   // There is no need to check for the stripped path for a 
                   // subfolder since we are loading the root of the site in
                   // the DMS.
                   var lstSubSite = new List<AWSObjectItem>();

                   if (document != null)
                   {
                      // Get Location inside site
                      var tmpSubSite = from item in objectSite.S3Objects
                                       where item.StrippedPath == siteFilter
                                       select item;

                      var awsObjectItems = tmpSubSite as IList<AWSObjectItem> ?? tmpSubSite.ToList();
                      lstSubSite = awsObjectItems.ToList();

                      if (!awsObjectItems.Any())
                      {
                         if (document != null)
                         {
                            AmazonDMS.CreateNewFolder(siteFilter);
                         }
                         else
                         {
                            AmazonDMS.CreateNewFolder(dir + siteFilter);
                         }

                         // Requery after create folder
                         objectSite = AmazonDMS.GetObjects(siteID, null, isGetAll: true);
                         var tmpSubObj = from item in objectSite.S3Objects
                                         where item.StrippedPath == siteFilter
                                         select item;
                         lstSubSite = tmpSubObj.ToList();
                      }
                   }

                   sb.Append("\t" + "<ul class=\"jqueryFileTree\" > ");

                   foreach (var item in objectSite.S3Objects)
                   {
                      if (item.StrippedPath == siteFilter)
                      {
                         foreach (var subFolderItem in getNonVersionDocument(lstSubSite))
                         {
                            var bIsAppend = !(bIsUpdload && subFolderItem.Type == AWSObjectItemType.File);

                            if (bIsAppend)
                            {
                               fileName = subFolderItem.FileName.Replace("/", "");
                               id = dir.Replace("/", "_") + fileName;
                               tmpdir = dir + fileName + "/";
                               sb.Append(("\t" + "<li id='id_" + id + "' class=\"directory expanded\"><a id='id_" + id + "_text' href=\"javascript:void(0)\" rel='" + tmpdir + "'><span style=\"text-align:left; width:auto;\">" + fileName + "</span></a>" + "\n"));
                               sb.Append("\t" + "<ul class=\"jqueryFileTree\" > ");
                            }

                            // Get all item inside location
                            if (subFolderItem.Type == AWSObjectItemType.Folder)
                            {
                               var objectSubSite = AmazonDMS.GetObjects(siteFilter + "/", null, isGetAll: true);
                               var nonVersionDocument = getNonVersionDocument(objectSubSite.S3Objects.ToList());

                               foreach (var subDocumentItem in nonVersionDocument)
                               {
                                  NO_DOCUMENT = false;
                                  sb.Append(AppendItem2UI(subDocumentItem, tmpdir));
                               }
                            }

                            if (NO_DOCUMENT)
                            {
                               sb.Append(NO_DOCUMENT_LI);
                            }

                            if (tmpdir.Contains(siteFilter)) continue;

                            sb.Append("\t" + "</ul>");
                            sb.Append("</li> \n");
                         }

                         sb.Append("\t" + "</ul>");

                         // Get Other folder 
                         sb.Append("</li> \n");
                      }
                      else if (item.Type == AWSObjectItemType.Folder)
                      {
                         sb.Append(AppendItem2UI(item, dir));
                      }
                   }
                   sb.Append("\t" + "</ul>");
                }
                else
                {
                   sb.Append("\t" + "<ul class=\"jqueryFileTree\" > ");
                   var subSiteList = objectSite.S3Objects.Where(x => x.Type == AWSObjectItemType.Folder).ToList();

                   foreach (var subDocumentItem in subSiteList)
                   {
                      sb.Append(AppendItem2UI(subDocumentItem, dir));
                   }
                   sb.Append("\t" + "</ul>");
                }
             }

             if (dir == "")
             {
                sb.Append("</li> \n");
             }

             if (action == "move")
             {
                NO_DOCUMENT = false;
             }

             if (NO_DOCUMENT && !folderSelectOnly)
             {
                NO_DOCUMENT = true;
             }
          }
          else
          {
             // Add condition open only unrevision file by Kantorn J. 2012-08-31
             var objectSet = AmazonDMS.GetObjects(dir, null, isGetAll: true);
             queryItems = getNonVersionDocument(objectSet.S3Objects.ToList());
             var firstRun = true;

             foreach (var s3Item in queryItems)
             {
                if (dir == "" && firstRun)
                {
                   sb.Append(("\t" + "<li id=\"id_Home\" class=\"home\">" +
                              "<a id=\"id_Home_text\" href=\"#\" rel='' ></a>" +
                              "<span style=\"text-align:left\">Home</span>" +
                              "</li>" + "\n"));
                   firstRun = false;
                }

                var bIsAppend = !(bIsUpdload && (s3Item.Type == AWSObjectItemType.File));

                if (!bIsAppend) continue;

                NO_DOCUMENT = false;
                sb.Append(AppendItem2UI(s3Item, dir));
             }

             if (NO_DOCUMENT)
             {
                sb.Append(NO_DOCUMENT_LI);
             }
          }

          sb.Append("</ul>");

          return sb.ToString();
       }
    }
}