﻿using System;
using System.Web;
using TrackIT2.BLL;
using TrackIT2.DAL;

namespace TrackIT2.DMS
{
    
    public partial class Default : System.Web.UI.Page
    {        
        
        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Security Check
            if (Security.UserIsInRole("Administrator") ||
                Security.UserIsInRole("TrackiT Docs Admin") ||
                Security.UserIsInRole("TrackiT Docs User"))
            {
                pnlNoAccess.Visible = false;
                pnlDMS.Visible = true;

                if (!Page.IsPostBack)
                {
                    // Setup FlexiGrid 
                    SetupFlexiGrid();
                }else
                {
                    // Initialise Download from Amazon
                    if (Request.Params["__EVENTARGUMENT"] == "DownloadDocument")
                    {
                        string keyName = hidDocumentDownload.Value;
                        if (!string.IsNullOrEmpty(keyName))
                        {
                            AmazonDMS.DownloadObject(HttpUtility.UrlDecode(keyName));   
                        }
                    }
                    //>
                }
            }
            else
            {
                pnlDMS.Visible = false;
                pnlNoAccess.Visible = true;
            }         
        }
        //-->

        /// <summary>
        /// Setup FlexiGrid
        /// </summary>
        protected void SetupFlexiGrid()
        {
            fgDMS.RowPerPageOptions = new int[] { 10, 20, 30, 40, 50 };
            fgDMS.NoItemMessage = "No Data";
            fgDMS.SortColumn = fgDMS.Columns.Find(col => col.Code == "FileName");
            fgDMS.SortDirection = FlexigridASPNET.GridSortOrder.Asc;
        }
        //-->
    }
}