﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="Upload File | TrackIT2" Language="C#" MasterPageFile="~/Templates/Modal.Master" AutoEventWireup="true" CodeBehind="UploadDocument.aspx.cs" Inherits="TrackIT2.DMS.UploadDocument" %>
<%@ MasterType TypeName="TrackIT2.Templates.Modal" %>
<%@ Register TagPrefix="TrackIT2" TagName="TextFieldAutoComplete" src="~/Controls/TextFieldAutoComplete.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   <!-- Styles -->
   <%: Styles.Render("~/Styles/dms_upload_document") %>

   <!-- Scripts -->
   <%: Scripts.Render("~/Scripts/dms_upload_document") %>

    <script type="text/javascript">
        // Doc Ready
        var minWidthUpload = 72;
        var maxWidthUpload = 200;

        $(function () {
            if ($.browser.mozilla) {
                minWidthUpload += 2;
            }
            setupFolderModal("Folder Select");

            activeFolderWaiting();

            $('#manualRenameTextBox').focusout(function () {
                validateRenameBeforeUpload("manualRenameTextBox");
            });

            $("#manualRenameTextBox").css('width', ($("#renameCheckBoxLable").width() - 2));

            $('#filUpload').MultiFile({
                STRING: { remove: 'X',
                    selected: 'Select: $file',
                    denied: 'Invalid file extension $ext!',
                    duplicate: '$file is already selected!'
                },
                onFileAppend: function (element, value, master_element) {
                    //check condition selected multiple file hide auto rename 
                    if ($('.MultiFile-label').length > 0 || element.files.length >1) {
                        if ($('#MainContent_divAutoRename').length > 0) {
                            $('#MainContent_divAutoRename').hide();
                            $('#renameCheckBox').prop('checked', false);
                            $('#MainContent_divManualRename').hide();
                            $('#manualRenameCheckBox').prop('checked', false);
                        }
                    }
                    else {
                        onAutoRenameUploadClick(value);
                        if ($('#message_alert').find('.error').length) {
                            $('#message_alert').find('.error').fadeOut('slow');
                        }
                    }
                    if (!$.browser.msie) {
                        $('.button-upload').each(function () {
                            $(this).width(minWidthUpload);
                        });
                    }
                },
                onFileRemove: function (element, value, master_element) {
                    if (!$.browser.msie) {
                        if ($('.MultiFile-label').length == 1) {
                            $('.button-upload').each(function () {
                                $(this).width(maxWidthUpload);
                            });
                        }
                    }
                    if ($('.MultiFile-label').length == 2) {
                        $('#MainContent_divAutoRename').show();
                        $('#MainContent_divManualRename').show();
                    }
                },
                error: function (element, value, master_element) {
                    alert(element);
                    setTimeout(function () {
                        if (!$.browser.msie) {
                            $('.button-upload').each(function () {
                                $(this).width(minWidthUpload);
                            });
                        }
                    }, 0);

                    if ($('.MultiFile-label').length == 1) {
                        $('#MainContent_divAutoRename').show();
                        $('#MainContent_divManualRename').show();
                    }
                }
            });

            $("#btnMovingFolderLocationChange").click(function () {
                $(this).parent().parent().find("div:first span.selectFileNameHeader").hide();
                $("#btnMovingFolderLocationChange").hide();
                $("#btnMovingFolderLocationSelect").show();
                $("#btnMovingFolderLocationCancel").show();
                $("#divMovingFolderSiteAutoComplete").css('display', 'inline-block');
            });

            $("#btnMovingFolderLocationCancel").click(function () {
                $("#btnMovingFolderLocationCancel").hide();
                $("#btnMovingFolderLocationSelect").hide();
                $("#divMovingFolderSiteAutoComplete").hide();
                $(this).parent().parent().find("div:first span.selectFileNameHeader").show();
                $("#btnMovingFolderLocationChange").show();
            });

            $("#btnMovingFolderLocationSelect").click(function () {
                var siteId = $(this).parent().parent().find("div:first").find("input").val();
                var locationPath = Array();
                locationPath.push(siteId + "/");

                if (siteId) {
                    $.ajax({
                        type: "POST",
                        dataType: "html",
                        url: "/Handlers/SiteIDHandler.axd",
                        async: false,
                        data: {
                            term: siteId
                        },
                        error: function (xhr, status, error) {
                            alert("Error validating site. Please try again.");
                        },
                        success: function (data) {
                            if (data != "[]") {
                                var invalidSite = jQuery.parseJSON(data)[0];
                                alert("Site Id " + invalidSite + " does not exist. Please specify an existing site.");
                            }
                            else {
                                var scriptPath = '/DMS/FolderTree.aspx?site=' + siteId + '/&action=move';

                                $.when($('#jqueryFileTree').fileTree(
                          {
                              root: '',
                              expandedFolders: locationPath,
                              script: scriptPath,
                              hiddenObj: 'hidBasePath',
                              LabelObj: 'lblMovingFolderLocation',
                              multiFolder: false
                          })
                          ).done(function () {
                              $("#btnMovingFolderLocationCancel").hide();
                              $("#btnMovingFolderLocationSelect").hide();
                              $("#divMovingFolderSiteAutoComplete").hide();
                              $('span.selectFileNameHeader').show();
                              $("#btnMovingFolderLocationChange").show();

                              $('span.selectFileNameHeader').text("/" + locationPath);
                              $('#hidBasePath').val("/" + locationPath);
                          });
                            }
                        }
                    });
                }
                else {
                    alert('Please specify a Site Id.');
                }
            });
        });
        //-->

        function renameCheckBoxClick(checkBoxObj) {
            var thisId = $(checkBoxObj).attr('id')
            if (thisId == "renameCheckBox") {
                $("#manualRenameCheckBox").attr('checked', false);
            }
            else {
                $("#renameCheckBox").attr('checked', false);
            }
        }

        function activeFolderWaiting() {
            var pageParam = getQueryString(window.location.toString());
            if (pageParam.length == 5) {
                var mainDialogHeight = parent.$('#modalDmsIframeId').height();
                var currentDialogHeight = $('#MainContent_folderLocationHeader').height();
                if (mainDialogHeight == 535) {
                    if (currentDialogHeight > 74) {
                        var count = currentDialogHeight / 74;
                        count = Math.floor(count)
                        for (x = 0; x < count; x++) {
                            mainDialogHeight = mainDialogHeight + 30;
                        }
                        parent.$('#modalDmsIframeId').height(mainDialogHeight);
                    }
                }
                $('#spinner-cont').css('margin-left', '74%');
                if (pageParam[4] == "Move") {
                    if ($('#treeLoad-spinner').length > 0) {
                        var currentBrowser = navigator.userAgent;
                        if (currentBrowser.indexOf('Firefox') > 0) {
                            $('#treeLoad-spinner').css('margin-top', '-3px');
                        }
                        $('#treeLoad-spinner').activity({ segments: 8, width: 3, space: 0, length: 3, speed: 1.5, align: 'right' });
                    }
                }
            }
            else if (pageParam[1] == "Upl" || pageParam[pageParam.length - 1] == "Sel") {
                var currentBrowser = navigator.userAgent;
                if(pageParam[pageParam.length - 1] == "Sel"){
                    $('#treeLoad-spinner').activity({ segments: 8, width: 3, space: 0, length: 3, speed: 1.5, align: 'right' });
                }
            }
            else if (pageParam.length == 1) {
                $('#spinner-cont').css('margin-left', '71%');
            }
        }

        // Setup Auto File Extension
        function setupAutoFileExtension(fileExt) {

            $('#txtFileName').bind("change", function () {
                if (fileExt != "?") {
                    var currentValue = $(this).val();
                    var newValue = '';
                    if (currentValue.indexOf('.' + fileExt) >= 0) {
                        newValue = currentValue.substring(0, currentValue.indexOf('.' + fileExt));
                    } else {
                        newValue = currentValue;
                    }
                    newValue += '.' + fileExt;
                    $(this).val(newValue);
                }
            });
        }
    //-->
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="message_alert"></div>
    <div class="create-container" style="margin: 0 auto 0 auto;">
    
        <div class="form_element">
            <div id="folderLocationHeader" runat="server">
                <h4>Folder Location</h4>
                <label id="lblUploadingTo" runat="server" clientidmode="Static">Select File</label>&nbsp;
                <asp:Label ID="lblUploadingFolderLocation" runat="server" Text="/" style="width: auto;" />&nbsp;
                <input id="btnTargetBrowser"  runat="server" clientidmode="Static" type="button" value="Browse..." style=" text-align:right;" name="btnTargetBrowser" />
            </div>
            
            <div id="documentHeader" runat="server">
                <h4>Document</h4>
            </div>
        
                <div id="divFileUpload" runat="server">
                    <label  style="float:left;" >Select File</label>
                    <asp:FileUpload ID="filUpload" class="button-upload" Multiple="multiple" ClientIDMode="Static" runat="server" style="height: 24px;" />
                </div>

                <div id="divFileName" Visible="False" runat="server">
                    <label  >File Name</label>
                    <asp:TextBox ID="txtFileName" ClientIDMode="Static" runat="server" style="width: 240px;" />
                </div>

                <div id="divAutoRename" runat="server" visible="false" style="width:auto;">
                    <input type="checkbox" ID="renameCheckBox" runat="server" clientidmode="Static" onclick="renameCheckBoxClick(this);" style="margin-left:20px;"/>
                    <span style="width: auto; padding: 0px;margin-bottom:0px;">Rename file to "<label for="MainContent_renameCheckBox" id="renameCheckBoxLable" style="margin-right:0px;width:auto;" runat="server" clientidmode="Static" >{0}{1} {2}</label>"?</span>
                </div>

                <div id="divManualRename" runat="server" visible="false" style="width:auto;">
                    <input type="checkbox" ID="manualRenameCheckBox" runat="server" clientidmode="Static" onclick="renameCheckBoxClick(this);" style="margin-left:20px;"/>
                    <span style="width: auto; padding: 0px;margin-bottom:0px;">Rename file to <asp:TextBox ID="manualRenameTextBox" runat="server" clientidmode="Static"/><label for="MainContent_manualRenameCheckBox" id="manualRenameCheckBoxLable" style="margin-right:0px;width:auto;" runat="server" clientidmode="Static" ></label>?</span>
                </div>

                <div id="divClassificationClassification" runat="server">
                    <h4>Classification</h4>
                    <div class="listbox-container">
                        <asp:ListBox ID="lbxAddClassification" SelectionMode="Multiple" ToolTip="- Select a Classification -" CssClass="multi-local-config" ClientIDMode="Static" runat="server" />
                    </div>
                </div>
            
                <div id="localtion_tree" class="location_tree" runat="server" visible="false">
			         <h4><asp:Label id="moveLocationHeader" runat="server" Text="Move Location"></asp:Label></h4>
                  <br />                                         
                  <asp:Label ID="lblMoveTo" Text="Move to:" CssClass="selectFileTargetHeader" style="width: auto;" runat="server" />&nbsp;
                  <div style="display:inline-block;">
                     <asp:Label ID="lblMovingFolderLocation" ClientIDMode="Static" runat="server" Text="/" CssClass="selectFileNameHeader" />
                     <div id="divMovingFolderSiteAutoComplete" class="move-folder-location-autocomplete">
                        <TrackIT2:TextFieldAutoComplete ID="ctlMovingFolderSiteID"
                                  LabelFieldClientID="lblSiteID"
                                  LabelFieldValue="Site ID"
                                  LabelFieldCssClass="label"
                                  TextFieldClientID="txtMovingFolderLocation"
                                  TextFieldCssClass="input-autocomplete"
                                  TextFieldWidth="130"
                                  ScriptKeyName="ScriptAutoCompleteSiteID"
                                  DataSourceUrl="SiteIdAutoComplete.axd"
                                  ClientIDMode="Static"
                                  runat="server" />
                     </div>                                                       
                  </div>

                  <div id="divChangeSite" runat="server">
                     <input type="button" id="btnMovingFolderLocationChange" class="move-folder-location-change-button" value="Change Site" />                 
                     <input type="button" id="btnMovingFolderLocationCancel" class="move-folder-location-cancel-button" style="display: none;" value="Cancel"/>
                     <input type="button" id="btnMovingFolderLocationSelect" class="move-folder-location-cancel-button" style="display: none;" value="Select"/>                     
                  </div>
                  
                  <br/>
                  
                  <asp:Panel ID="TreePanel" runat="server" CssClass="TreePanelAttribute" ScrollBars="Auto">
    			         <div id="jqueryFileTree" class="jqueryFileTree" ></div>
                  </asp:Panel>
		         </div>

                <br />

            <div id="MetaData" runat="server">
            <h4>META Data</h4>

                <label style="width: auto;">Description</label>
                <asp:TextBox ID="txtAddDescription" TextMode="MultiLine" Rows="2" ClientIDMode="Static" runat="server"></asp:TextBox>
                <br />

                <label style="width: auto;display: none;">Keywords</label>
                <asp:TextBox ID="txtAddKeywords" TextMode="MultiLine" Rows="2" ClientIDMode="Static" Visible="False" runat="server"></asp:TextBox>
             </div>    
                <div id="divUploadModelButton" style="display: block;">
                    
                    <asp:Button ID="btnStepBack"
                        Text="&laquo; Go Back"
                        ClientIDMode="Static"
                        Visible="false"
                        OnClientClick="javascript:return false;"
                        runat="server" />

                    <div id="spinner-cont" style="display: block;float: left;width: 30px;height: 50px;"></div>

                    <asp:Button ID="btnUploadDocument"
                        Text="Upload"
                        CssClass="buttonSubmit ui-button ui-widget ui-state-default ui-corner-all right"
                        OnClientClick="return CheckValidUploadDocument();"
                        OnClick="BtnUploadDocumentClick"
                        ClientIDMode="Static"
                        runat="server" />

                    <asp:Button ID="btnSaveDocument"
                        Text="Save"
                        CssClass="buttonSubmit ui-button ui-widget ui-state-default ui-corner-all right"
                        OnClientClick="return validateSaveDocument();"
                        OnClick="BtnSaveDocumentClick"
                        ClientIDMode="Static"
                        Visible="False"
                        runat="server" />

                    <asp:Button ID="btnMoveDocument"
                        Text="Move"
                        CssClass="buttonSubmit ui-button ui-widget ui-state-default ui-corner-all right"
                        OnClientClick="return validateMoveDocument();"
                        OnClick="BtnMoveDocumentClick"
                        ClientIDMode="Static"
                        Visible="False"
                        runat="server" />

                    <asp:Button ID="btnSelcetDocument"
                        Text="Select File"
                        CssClass="buttonSubmit ui-button ui-widget ui-state-default ui-corner-all right"
                        OnClientClick="return validateSelectDocument();"
                        OnClick="BtnSelectDocumentClick"
                        ClientIDMode="Static"
                        Visible="False"
                        runat="server" />

                    <div id="treeLoad-spinner" style="display: block; margin-top:-8px;margin-left:345px; float:right; width: 30px;height: 50px;"></div>
                </div>
                    
                <asp:HiddenField ID="hidBasePath" runat="server" Value="/" ClientIDMode="Static"/>
                <asp:HiddenField ID="hidTmpPath" runat="server" Value="" ClientIDMode="Static"/>
                <asp:HiddenField ID="hidNewFileName" runat="server" Value="" ClientIDMode="Static"/>
                <asp:HiddenField ID="hidExt" runat="server" Value="" ClientIDMode="Static"/>
        </div>
    </div>
    <div id="modalFolderBrowse" class="modal-container" style="overflow:hidden;"></div>
    <div id="dialog-message" style="display: none;"><h3>Classification Limit Reached</h3><br /><p>The system currently supports up-to 3 Classification per Document / File.</p></div>
</asp:Content>
