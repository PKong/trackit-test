﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using TrackIT2.BLL;
using TrackIT2.DAL;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;

namespace TrackIT2.DMS
{
    /// <summary>
    /// Upload Document
    /// </summary>
    public partial class UploadDocument : System.Web.UI.Page
    {
        // Var
        private string _basePath = "";
        private bool _isFieldLink = false;
        //>

        #region Properties

        /// <summary>
        /// Key
        /// </summary>
        public string Key
        {
            get
            {
                return (string)ViewState["Key"];
            }
            set
            {
                ViewState["Key"] = value;
            }
        }

        //>

        /// <summary>
        /// File Name
        /// </summary>
        public string FileName
        {
            get
            {
                return (string)ViewState["FileName"];
            }
            set
            {
                ViewState["FileName"] = value;
            }
        }

        //>

        /// <summary>
        /// File Type
        /// </summary>
        public string FileType
        {
            get
            {
                return (string)ViewState["FileType"];
            }
            set
            {
                ViewState["FileType"] = value;
            }
        }

        //>

        /// <summary>
        /// File Extension
        /// </summary>
        public string FileExtension
        {
            get
            {
                return (string)ViewState["FileExtension"];
            }
            set
            {
                ViewState["FileExtension"] = value;
            }
        }

        //>

        /// <summary>
        /// Uploaded By
        /// </summary>
        public string UploadedBy
        {
            get
            {
                return (string)ViewState["UploadedBy"];
            }
            set
            {
                ViewState["UploadedBy"] = value;
            }
        }
        //>

        /// <summary>
        /// Upload Date
        /// </summary>
        public DateTime UploadedDate
        {
            get
            {
                return (DateTime)ViewState["UploadedDate"];
            }
            set
            {
                ViewState["UploadedDate"] = value;
            }
        }
        //>

        /// <summary>
        /// Object Type
        /// </summary>
        public string ObjectType
        {
            get
            {
                return (string)ViewState["ObjectType"];
            }
            set
            {
                ViewState["ObjectType"] = value;
            }
        }

        //>

        #endregion

        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //Stop Caching in IE
            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            //Stop Caching in Firefox
            Response.Cache.SetNoStore();

            if (!Page.IsPostBack)
            {
                // Hidden Base Path
                if (!string.IsNullOrEmpty(Request.QueryString["basePath"]))
                {
                    _basePath = Request.QueryString["basePath"];

                    // Uploading to label for User
                    lblUploadingFolderLocation.Text = _basePath;

                    if (_basePath.Length > 1)
                    {
                        if (_basePath.StartsWith("/")) _basePath = _basePath.Substring(1, _basePath.Length - 1);
                        if (_basePath.EndsWith("/")) _basePath = _basePath.Substring(0, _basePath.Length - 1);
                    }
                    else
                    {
                        _basePath = "";
                    }
                    hidBasePath.Value = _basePath;
                }

                // Bind Lookup Data
                BindLookupData();

                if (!string.IsNullOrEmpty(Request.QueryString["type"]))
                {
                    btnTargetBrowser.Visible = true;
                }
                else
                {
                    btnTargetBrowser.Visible = false;
                }

                // Editing or Move?
                if (!string.IsNullOrEmpty(Request.QueryString["key"]) &&
                    !string.IsNullOrEmpty(Request.QueryString["action"]))
                {
                    Key = Request.QueryString["key"];
                    SetupEditMode(Request.QueryString["action"]);
                    
                }
                else if (Request.QueryString["action"] == "Sel" || Request.QueryString["action"] == "Upl")
                {
                    Key = "/";
                    hidBasePath.Value = "";
                    SetupDocMode(Request.QueryString["action"]);
                    TreePanel.CssClass = "SelectFileTreePanelAttribute";
                    divChangeSite.Visible = false;
                    if (Request.QueryString["action"] == "Sel")
                    {
                        this.TreePanel.Height = 265;
                    }
                    PageUtility.RegisterClientsideStartupScript(Page, "EnableAndSetLink", "EnableAndSetLink()");
                }
            }
        }

        //-->

        /// <summary>
        /// Setup Edit Mode
        /// </summary>
        private void SetupEditMode(string action)
        {
            // Change uploading to description label for editing mode
            lblUploadingTo.InnerText = "Current folder:";

            // Get Current Object
            var objectSet = AmazonDMS.GetObjects(_basePath, null, isGetAll: true);
            var queryItems = objectSet.S3Objects.Where(a => a.Type != AWSObjectItemType.Folder).ToList();
            var editObject = queryItems.FirstOrDefault(o => o.Key == Key);

            if (editObject != null)
            {
                // Hide File Upload Controls
                divFileUpload.Visible = false;
                btnUploadDocument.Visible = false;

                // File Name
                FileName = editObject.FileName;
                txtFileName.Text = editObject.FileName;

                // Edit or Move
                if (action == "Edit")
                {
                    // Show Edit Controls
                    divFileName.Visible = true;
                    btnSaveDocument.Visible = true;

                    // Get Object META Data
                    AWSObjectMetadata metaEdit;
                    using (AmazonDMS.S3Client = Amazon.AWSClientFactory.CreateAmazonS3Client())
                    {
                        // Get Object META
                        metaEdit = AmazonDMS.GetObjectMetadata(Key);
                    }

                    // Have META?
                    if (metaEdit != null)
                    {
                        // File Type
                        if (metaEdit.FileType != null)
                        {
                            FileType = metaEdit.FileType;
                        }
                        else
                        {
                            // Error .. no file type!
                            FileType = FileUtility.GetFileType(editObject.FileName).TypeOfFile;
                        }

                        // Classification
                        if (metaEdit.Classification != null && metaEdit.Classification.Any())
                        {
                            foreach (var item in metaEdit.Classification)
                            {
                                var lstItem = lbxAddClassification.Items.FindByValue(item);
                                if (lstItem != null)
                                {
                                    lstItem.Selected = true;
                                }
                            }
                        }

                        // Description
                        if (metaEdit.Description != null)
                        {
                            txtAddDescription.Text = metaEdit.Description;
                        }

                        // Keywords
                        if (metaEdit.Keywords != null)
                        {
                            txtAddKeywords.Text = metaEdit.Keywords;
                        }

                        // Uploaded By
                        if (metaEdit.UploadedBy != null)
                        {
                            UploadedBy = metaEdit.UploadedBy;
                        }
                        else
                        {
                            UploadedBy = ""; // User.Identity.Name;
                        }

                        // Uploaded Date
                        DateTime uploadDate;
                        if (
                            !DateTime.TryParse(metaEdit.UploadDate.ToString(CultureInfo.InvariantCulture),
                                               out uploadDate))
                        {
                            uploadDate = DateTime.Now;
                        }
                        UploadedDate = uploadDate;
                    }
                }
                else
                {
                    //Hide file edit control
                    divClassificationClassification.Visible = false;
                    MetaData.Visible = false;
                    btnSaveDocument.Visible = false;
                    txtFileName.Enabled = false;

                    lblUploadingTo.InnerText = "Current Location:";
                    //Display tree
                    divFileName.Visible = true;
                    localtion_tree.Visible = true;
                    btnMoveDocument.Visible = true;

                }
                // Setup Auto File Extension for File Name
                FileExtension = FileUtility.GetFileType(editObject.FileName).FileExtension;
            }
        }
        //-->

        /// <summary>
        /// Setup Edit Mode
        /// </summary>
        private void SetupDocMode(string action)
        {
            if (action == "Upl")
            {
                lblUploadingTo.InnerText = "Target Location:";
                
                localtion_tree.Visible = false;
                btnStepBack.CssClass = "BtnUploadStepBackDocumentFooter";

                if (Request.QueryString["siteID"] != null && Request.QueryString["siteID"] != "null")
                {
                    string field = string.Empty;
                    if (Request.QueryString["field"] != null && Request.QueryString["field"] != "null")
                    {
                        field = Request.QueryString["field"];
                    }

                    //Check SiteID and auto-create
                    string folderPath = Request.QueryString["siteID"];

                    if (!folderPath.EndsWith("/"))
                    {
                        folderPath += "/";
                    }

                    DmsLinkDocumentStructure document = DMSDocumentLinkHelper.DocumentField(field);
                    AWSResultSet objectSet = AmazonDMS.GetObjects(folderPath, null, isGetAll: false);
                    //Check folder
                    if (document.Location.HasValue)
                    {
                        String location = document.Location.ToString();
                        var tmpObj = from item in objectSet.S3Objects
                                     where item.StrippedPath == folderPath + location
                                     select item;

                        if (tmpObj.Count() == 0)
                        {
                            AmazonDMS.CreateNewFolder(Request.QueryString["siteID"] + "/" + location);
                        }

                        lblUploadingFolderLocation.Text = "/" + folderPath + location;
                    }
                    else
                    {
                        lblUploadingFolderLocation.Text = "/" + Request.QueryString["siteID"];
                    }

                    hidBasePath.Value = lblUploadingFolderLocation.Text;

                    //Auto rename field
                    if (!string.IsNullOrEmpty(Request.QueryString["autoRename"]))
                    {
                        divAutoRename.Visible = true;
                        divManualRename.Visible = true;
                        
                        string newRename = CreateAutoRename(renameCheckBoxLable.InnerText, Request.QueryString["siteID"], Request.QueryString["newName"],
                            Request.QueryString["newNameDescription"], Request.QueryString["newNameAppType"], document.Field);
                        renameCheckBoxLable.InnerText = newRename;
                        manualRenameTextBox.Text = newRename.Trim();
                    }
                    else
                    {
                        divAutoRename.Visible = false;
                        divManualRename.Visible = false;
                        renameCheckBox.Checked = false;
                        manualRenameCheckBox.Checked = false;
                    }
                }
            }
            else
            {
                //Hide file edit control
                folderLocationHeader.Visible = false;
                moveLocationHeader.Visible = false;
                lblMoveTo.Text = "Target file location :";
                btnUploadDocument.Visible = false;
                btnSelcetDocument.Visible = true;
                divFileName.Visible = false;
                documentHeader.Visible = false;

                divClassificationClassification.Visible = false;
                MetaData.Visible = false;
                btnSaveDocument.Visible = false;
                txtFileName.Enabled = false;
                lblUploadingTo.Visible = false;

                //Display tree
                divFileUpload.Visible = false;
                localtion_tree.Visible = true;

                btnStepBack.CssClass = "BtnSelectStepBackDocumentFooter";

                divAutoRename.Visible = false;
            }
            btnStepBack.Visible = true;
            btnSaveDocument.Visible = false;
        }

        /// <summary>
        /// Bind Lookup Data
        /// </summary>
        protected void BindLookupData()
        {
            using (var ce = new CPTTEntities())
            {
                var classifications = ce.dms_classifications.ToList();

                List<dms_classifications> sortClassifications = classifications;
                var sortAlgorithm = new DynamicComparer<dms_classifications>();
                sortAlgorithm.SortOrder(x => x.name);
                sortClassifications.Sort(sortAlgorithm);

                Utility.BindDDLDataSource(lbxAddClassification, sortClassifications, "name", "name", null);
            }
        }

        /// <summary>
        /// Check exist file name with case insensitive 
        /// </summary>
        private bool CheckFilesExist(string filename, ref string fileduplicate)
        {
            bool bIsFileExist = false;
            if (AmazonDMS.CurrentBucketObjectNames == null)
            {
                if (hidBasePath.Value == "")
                {
                    hidBasePath.Value = "/";
                }
                using (AmazonDMS.S3Client = Amazon.AWSClientFactory.CreateAmazonS3Client())
                {
                    string tmpBasePath = Utility.PrepareDMSBasePath(hidBasePath.Value);
                    // Get Object META
                    var objectSet = AmazonDMS.GetObjects(tmpBasePath, null, isGetAll: true);
                    AmazonDMS.CurrentBucketObjectNames = objectSet.S3Objects.Select(o => o.FileName).ToList();
                }
            }
            if (AmazonDMS.CurrentBucketObjectNames != null)
            {
                if (AmazonDMS.CurrentBucketObjectNames.Where(x => x.Trim().ToLower() == filename.Trim().ToLower()).Any())
                {
                    fileduplicate = AmazonDMS.CurrentBucketObjectNames.Where(x => x.Trim().ToLower() == filename.Trim().ToLower()).FirstOrDefault();
                    bIsFileExist = true;
                }

                /*foreach (var objBucket in AmazonDMS.CurrentBucketObjectNames)
                {
                    if (filename.Trim().ToLower().Equals(objBucket.ToLower()))
                    {
                        fileduplicate = objBucket;
                        bIsFileExist = true;
                    }
                }*/
            }
            return bIsFileExist;
        }
        //-->

        /// <summary>
        /// Check exist file name with case insensitive 
        /// </summary>
        private bool CheckFilesRevision(string filename, ref int revisionNumber)
        {
            bool bIsFileExist = false;
            if (AmazonDMS.CurrentBucketObjectNames == null)
            {
                if (hidBasePath.Value == "")
                {
                    hidBasePath.Value = "/";
                }
                using (AmazonDMS.S3Client = Amazon.AWSClientFactory.CreateAmazonS3Client())
                {
                    string tmpBasePath = Utility.PrepareDMSBasePath(hidBasePath.Value);
                    // Get Object META
                    var objectSet = AmazonDMS.GetObjects(tmpBasePath, null, isGetAll: true);
                    AmazonDMS.CurrentBucketObjectNames = objectSet.S3Objects.Select(o => o.FileName).ToList();
                }
            }
            if (AmazonDMS.CurrentBucketObjectNames != null)
            {
                var sameKeyList = AmazonDMS.CurrentBucketObjectNames.Where(x => x.Trim().ToLower().Contains(filename.Trim().ToLower()));
                if(sameKeyList.Count() > 0)
                {
                    revisionNumber = sameKeyList.Count();
                    string tmpRev = sameKeyList.LastOrDefault().Split('.').LastOrDefault();
                    if (int.TryParse(tmpRev, out revisionNumber))
                    {
                        bIsFileExist = true;
                    }
                    else
                    {
                        revisionNumber = 1;
                    }
                }
            }
            return bIsFileExist;
        }
        //-->

        private bool CheckDocumentKey(int id, DmsLinkDocumentStructure document, string key, ref string oldKey)
        {
            bool result = false;
            using (CPTTEntities ce = new CPTTEntities())
            {
                List<Dms_Document> doc = new List<Dms_Document>();
                try
                {
                    doc = DMSDocumentLinkHelper.GetDocumentLink(id, document, ceRef: ce);
                    foreach (Dms_Document d in doc)
                    {
                        if (key.ToLower() == d.ref_key.ToLower())
                        {
                            oldKey = d.ref_key;
                            result = true;
                            break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Log error in ELMAH for any diagnostic needs.
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                }
            }

            return result;
        }

        /// <summary>
        /// BtnUploadDocument Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnUploadDocumentClick(object sender, EventArgs e)
        {
            string fileName = String.Empty;
            var classification = new List<string>();
            string fileKey = String.Empty;
            string result = String.Empty;
            string mainSiteId = string.Empty;
            bool isLinkField = false;
            string recordLink = string.Empty;

            // Classification
            classification.AddRange(from ListItem listItem in lbxAddClassification.Items
                                    where listItem.Selected
                                    select listItem.Value);


            string fileDuplicateKey = "";
            string duplicateFile = "";
            string duplicateRevFile = "";


            //AmazonDMS.CurrentBucketObjectNames away be null when call outsite Dms
            if (hidBasePath.Value == "")
            {
                hidBasePath.Value = "/";
            }

            using (AmazonDMS.S3Client = Amazon.AWSClientFactory.CreateAmazonS3Client())
            {
                string tmpBasePath = Utility.PrepareDMSBasePath(hidBasePath.Value);
                // Get Object META
                var objectSet = AmazonDMS.GetObjects(tmpBasePath, null, isGetAll: true);
                AmazonDMS.CurrentBucketObjectNames = objectSet.S3Objects.Select(o => o.FileName).ToList();
            }


            //check link document properties
            bool bIsLinkDocument = false;
            string types = String.Empty;
            int typeID = 0;
            bool isValidId = false;
            string siteID = "";
            string field = Request.QueryString["field"];
            string oldKey = "";
            DmsLinkDocumentStructure document = null;
            string tmpVar = Utility.PrepareDMSBasePath(hidBasePath.Value);

            // Add doc link after upload file
            if (Request.QueryString["action"] != null)
            {
                if (Request.QueryString["action"] == "Upl")
                {
                    bIsLinkDocument = true;
                    types = Request.QueryString["type"];
                    isValidId = Int32.TryParse(Request.QueryString["typeID"], out typeID);
                    document = DMSDocumentLinkHelper.DocumentField(field);
                    if (!isValidId)
                    {
                        siteID = Request.QueryString["typeID"];
                        mainSiteId = siteID;
                    }
                }
            }
            List<Dms_Document> lstDocument = new List<Dms_Document>();
            List<Dms_Document> lstDocumentRelated = new List<Dms_Document>();
            List<Dms_Document_Site> lstDocumentSite = new List<Dms_Document_Site>();
            List<AWSObjectMetadata> lstMetadata = new List<AWSObjectMetadata>();

            //Get all upload files
            HttpFileCollection uploadFiles = Request.Files;

            //check have any lock file uploading
            for (int fileCount = 0; fileCount < uploadFiles.AllKeys.Count(); fileCount++)
            {
                HttpPostedFile file = uploadFiles[fileCount];
                fileName = file.FileName;
                fileKey = tmpVar + "/" + fileName;
                // Get Object META Data
                AWSObjectMetadata metaRename = null;
                bool bIsFileExist = CheckFilesExist(fileName, ref duplicateFile);
                if (bIsFileExist)
                {
                    using (AmazonDMS.S3Client = Amazon.AWSClientFactory.CreateAmazonS3Client())
                    {
                        // Get Object META
                        metaRename = AmazonDMS.GetObjectMetadata(fileKey);
                    }
                    //If upload new version must check current version was lock or not?
                    if (metaRename.IsCheckedOut)
                    {
                        string JS_FILE_ISLOCKED = "handleFileNameIsLockedInModal(\"" + fileKey + "\");";
                        PageUtility.RegisterClientsideStartupScript(Page, "ScriptFileExists", JS_FILE_ISLOCKED);
                        return;
                    }
                }
                lstMetadata.Add(metaRename);
            }
            //using (CPTTEntities ce = new CPTTEntities())
            //{
                try
                {
                    for (int fileCount = 0; fileCount < uploadFiles.AllKeys.Count(); fileCount++)
                    {
                        fileName = uploadFiles[fileCount].FileName;
                        var objFileType = FileUtility.GetFileType(fileName);
                        string sourcePath = System.IO.Path.Combine(HttpRuntime.CodegenDir, fileName);

                        // auto change file name *reamark not support for multiple upload
                        if (renameCheckBox.Checked)
                        {
                            fileName = hidNewFileName.Value.Trim();
                            var type = FileUtility.GetFileTypeFromExtension(hidExt.Value);
                            // mapping file exrension
                            if (type.TypeOfFile != "Unknown")
                            {
                                fileName += "." + type.FileExtension;
                            }
                            else if (!String.IsNullOrEmpty(hidExt.Value))
                            {
                                fileName += "." + hidExt.Value;
                            }
                        }
                        else if (manualRenameCheckBox.Checked)
                        {
                            fileName = manualRenameTextBox.Text.Trim();
                            var type = FileUtility.GetFileTypeFromExtension(hidExt.Value);
                            // mapping file exrension
                            if (type.TypeOfFile != "Unknown")
                            {
                                fileName += "." + type.FileExtension;
                            }
                            else if (!String.IsNullOrEmpty(hidExt.Value))
                            {
                                fileName += "." + hidExt.Value;
                            }
                        }

                        // Base Path
                        if (hidBasePath.Value != "")
                        {
                            fileKey = tmpVar + "/" + fileName;
                        }

                        string strFileVersioned = "";
                        AWSObjectItem uploadItem = new AWSObjectItem();

                        //if file is exist then matad data list is not null
                        fileDuplicateKey = "";
                        if (lstMetadata[fileCount] != null)
                        {

                            if (hidBasePath.Value != "")
                            {
                                var tmpHidBasePath = Utility.PrepareDMSBasePath(hidBasePath.Value);
                                fileDuplicateKey = tmpHidBasePath + "/" + fileName;
                                fileDuplicateKey = Utility.PrepareDMSBasePath(fileDuplicateKey);
                            }

                            // Yes: rename / version the existing File
                            bool lookForNextVersion = true;
                            int fileVersion = 1;
                            string fileNameExistingVersioned = fileName + ".";

                            // Looking
                            if (lookForNextVersion)
                            {
                                if (CheckFilesRevision(fileName.ToString(CultureInfo.InvariantCulture), ref fileVersion))
                                {
                                    fileNameExistingVersioned += (fileVersion+1).ToString(CultureInfo.InvariantCulture);
                                    strFileVersioned = fileNameExistingVersioned;
                                    lookForNextVersion = false;
                                }
                                else
                                {
                                    fileNameExistingVersioned += fileVersion.ToString(CultureInfo.InvariantCulture);
                                }
                            }

                            //Change key filename File Key Rename
                            if (hidBasePath.Value != "")
                            {
                                var tmpHidBasePath = Utility.PrepareDMSBasePath(hidBasePath.Value);
                                fileNameExistingVersioned = tmpHidBasePath + "/" + fileNameExistingVersioned;
                                fileNameExistingVersioned = Utility.PrepareDMSBasePath(fileNameExistingVersioned);
                            }


                            // Have META?
                            string uploadedBy = "";
                            DateTime uploadDate = DateTime.Now;
                            if (lstMetadata[fileCount] != null)
                            {
                                // Uploaded By
                                uploadedBy = lstMetadata[fileCount].UploadedBy ?? "";

                                // Uploaded Date
                                if (!DateTime.TryParse(lstMetadata[fileCount].UploadDate.ToString(CultureInfo.InvariantCulture), out uploadDate))
                                {
                                    uploadDate = DateTime.Now;
                                }
                            }
                            //>
                            AmazonDMS.RenameObject(fileDuplicateKey, fileNameExistingVersioned, uploadedBy,
                                                    uploadDate.ToString(CultureInfo.InvariantCulture));
                            AmazonDMS.CurrentBucketObjectNames.Remove(duplicateFile);
                            AmazonDMS.CurrentBucketObjectNames.Add(fileNameExistingVersioned);

                        }

                        //insert prime link document
                        if (hidBasePath.Value != "")
                        {
                            //split key to get siteid 
                            string[] tmpKey = hidBasePath.Value.Split('/');
                            string siteId = tmpKey[0];
                            if (String.IsNullOrEmpty(siteId) && (tmpKey.Length > 1))
                            {
                                siteId = tmpKey[1];
                            }
                            mainSiteId = siteId;
                            //check prime site to insert db
                            if (IsPrimeSiteDoc(fileName, siteId))
                            {
                                List<documents_prime_site_links> res = DMSDocumentLinkHelper.GetPrimeDocumentLink(HttpUtility.UrlEncode(fileKey), siteId);
                                if (!res.Any())
                                {
                                    result = DMSDocumentLinkHelper.AddPrimeDocumentLink(HttpUtility.UrlEncode(fileKey), siteId);
                                }
                                else
                                {
                                    result = DMSDocumentLinkHelper.UpdatePrimeDocumentLink(res.FirstOrDefault());
                                }
                            }
                        }

                        // Save & Upload to Amazon
                        uploadFiles[fileCount].SaveAs(sourcePath);
                        AmazonDMS.AddObject(sourcePath, fileKey, classification, objFileType.TypeOfFile, txtAddDescription.Text,
                                            User.Identity.Name, txtAddKeywords.Text);
                        
                        //update other
                        uploadItem = AmazonDMS.GetObjectByKey(fileKey).S3Objects.FirstOrDefault();
                        // find session with fileKey then replace with uploadItem
                        if (uploadItem.Type == AWSObjectItemType.File)
                        {
                            UpdateOtherSession(hidBasePath.Value, uploadItem, fileDuplicateKey);
                        }

                        if (bIsLinkDocument)
                        {
                            string key = Server.UrlEncode(fileKey);
                            //Check if documnet already exist then remove old key and replace with new jkey
                            if (typeID != 0)
                            {
                                if (CheckDocumentKey(typeID, document, key, ref oldKey))
                                {
                                    result = DMSDocumentLinkHelper.RemoveDocumentLink(typeID, document, oldKey);
                                }
                                result = DMSDocumentLinkHelper.AddDocumentLink(typeID, typeID, document, key, true);
                                DocumentsChangeLog.AddLog(mainSiteId, fileName, fileKey, DocumentsChangeLog.eDocumentLogAction.Upload, Request, bIsLinkDocument,true);

                                lstDocument.Add(new Dms_Document(document.Type.ToString(), typeID, document.Field.ToString(), key));
                                if (document.RelatedField.HasValue)
                                {
                                    DmsLinkDocumentStructure related_doc = DMSDocumentLinkHelper.DocumentField(document.RelatedField.ToString());
                                    lstDocumentRelated.Add(new Dms_Document(related_doc.Type.ToString(), typeID, related_doc.Field.ToString(), key));
                                }
                            }
                            else if (siteID != "")
                            {
                                List<Dms_Document_Site> oldLink = new List<Dms_Document_Site>();
                                //check old link
                                oldLink = DMSDocumentLinkHelper.GetDocumentLink(siteID, key);
                                if (oldLink.Count > 0)
                                {
                                    result = DMSDocumentLinkHelper.RemoveDocumentLink(siteID, document.Field.ToString(), key);
                                }
                                documents_links_site doc = new documents_links_site();
                                doc.record_id = siteID;
                                doc.linked_field = document.Field.ToString();
                                doc.record_type = document.Type.ToString();

                                doc.trackit_docs_folder = key;
                                result = DMSDocumentLinkHelper.AddDocumentLink(doc);
                                lstDocumentSite.Add(new Dms_Document_Site(document.Type.ToString(), siteID, document.Field.ToString(), key));
                                
                                DocumentsChangeLog.AddLog(mainSiteId, fileName, fileKey, DocumentsChangeLog.eDocumentLogAction.Upload, Request, bIsLinkDocument,true);
                            }
                            else if (typeID == 0)
                            {
                                lstDocument.Add(new Dms_Document(document.Type.ToString(), typeID, document.Field.ToString(), key));
                            }
                        }
                        else
                        { 
                             // insert to customer document link
                            string customerDocument = matchCustomer(fileName);
                            if (!String.IsNullOrEmpty(customerDocument))
                            {
                                customer objCustomer = Customer.SearchCustomerName(customerDocument);
                                if (objCustomer != null)
                                {
                                    documents_customer_links docCustomer = DMSDocumentLinkHelper.GetCustomerDocument(objCustomer.id, tmpVar.Split('/')[0], Server.UrlEncode(fileKey));
                                    if (docCustomer == null)
                                    {
                                        docCustomer = new documents_customer_links();
                                        docCustomer.site_uid = tmpVar.Split('/')[0];
                                        docCustomer.customer_id = objCustomer.id;
                                        docCustomer.trackit_docs_folder = Server.UrlEncode(fileKey);
                                        DMSDocumentLinkHelper.AddCustomerDocument(docCustomer);
                                    }
                                }
                            }

                            DocumentsChangeLog.AddLog(mainSiteId, fileName, fileKey, DocumentsChangeLog.eDocumentLogAction.Upload, Request, bIsLinkDocument);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                }

                if(!bIsLinkDocument)
                {
                    // Close & Refresh FlexiGrid
                    CloseDialogAndRefreshFlexiGrid();
                }
                else
                {
                    if (result == "")
                    {
                        String script = "closeDialogExistingWithJson('{0}','{1}');";
                        String jsonData = String.Empty;
                        if (isValidId)
                        {
                            ///closeDialogExisting(type, typeID, field, key, name, related_field);
                            StringBuilder sb = new StringBuilder();
                            jsonData = Newtonsoft.Json.JsonConvert.SerializeObject(lstDocument).Replace("'", "|");
                            sb.Append(String.Format(script,jsonData,document.RelatedField.HasValue));
                            if (document.RelatedField.HasValue)
                            {
                                sb.Append(String.Format(script, jsonData, false));
                            }
                            PageUtility.RegisterClientsideStartupScript(Page, "CloseDocumentDialogExisting",
                                                                        sb.ToString());
                        }
                        else if (siteID != "")
                        {
                            StringBuilder sb = new StringBuilder();
                            jsonData = Newtonsoft.Json.JsonConvert.SerializeObject(lstDocumentSite).Replace("'", "|");
                            sb.Append(String.Format(script, jsonData, document.RelatedField.HasValue));

                            if (document.Field == DMSDocumentLinkHelper.eDocumentField.RogueEquipment)
                            {
                                PageUtility.RegisterClientsideStartupScript(Page, "CloseDocumentDialogExisting",
                                                                        sb.ToString());
                            }
                        }
                    }
                    else
                    {
                        PageUtility.RegisterClientsideStartupScript(Page, "CloseDocumentDialogAfterError",
                                                                    "closeDialogAfterError(\"" + result + "','" +
                                                                    types + "\",\"" + typeID + "\",\"" + field +
                                                                   "\")");
                    }
                }
            //}
        }

        //-->

        /// <summary>
        /// BtnSaveDocument Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnSaveDocumentClick(object sender, EventArgs e)
        {
            string fileNameNew = txtFileName.Text;
            string fileNameOld = Key.Split('/')[Key.Split('/').Length - 1];
            string duplicateFile = "";
            string testKey = fileNameNew;
            string bufferKey = Key;
            string typeID = String.IsNullOrEmpty(Request.QueryString["typeID"]) ? "" : Request.QueryString["typeID"];
            
            string siteID = Key.Split('/').FirstOrDefault();

            bool isLinkField = false;
            bool isRename = false;
            string fileOldKey = string.Empty;

            if (fileNameNew != fileNameOld)
            {
                isRename = true;
            }

            DmsLinkDocumentStructure document = null;
            if (!String.IsNullOrEmpty(Request.QueryString["field"]))
            {
                document = DMSDocumentLinkHelper.DocumentField(Request.QueryString["field"]);
                isLinkField = true;
            }

            // Base Path
            if (hidBasePath.Value != "")
            {
                testKey = hidBasePath.Value + "/" + testKey;
            }

            // Classification
            var classification = new List<string>();
            classification.AddRange(from ListItem listItem in lbxAddClassification.Items
                                    where listItem.Selected
                                    select listItem.Value);

            // Update
            AmazonDMS.UpdateObject(Key, classification, FileType, txtAddDescription.Text, txtAddKeywords.Text,
                                   UploadedBy, UploadedDate.ToString(CultureInfo.InvariantCulture));

            // Renaming
            if (testKey != Key)
            {
                //Change to check file name with insensitive case by Katorn J. 2012-08-14
                bool bIsFileExist = CheckFilesExist(fileNameNew, ref duplicateFile);
                if (bIsFileExist)
                {
                    const string JS_FILE_EXISTS = "handleFileNameAlreadyExistsInModal('file');";
                    PageUtility.RegisterClientsideStartupScript(Page, "ScriptFileExists", JS_FILE_EXISTS);
                    return;
                }

                //Move all revsion file by Kantorn J. 2012-08-7
                var objectSet = AmazonDMS.GetObjects(hidBasePath.Value, null, isGetAll: true);
                var queryItems =
                    objectSet.S3Objects.Where(a => a.Type != AWSObjectItemType.Folder && a.Key.Contains(Key)).ToList();
                var editObject = queryItems.FirstOrDefault(o => o.Key == Key);

                List<AWSObjectItem> listrev = queryItems.ToList();
                foreach (AWSObjectItem rev in listrev)
                {
                    string fileRev = rev.FileName.Split('.')[rev.FileName.Split('.').Length - 1];
                    string fileRevName = "";
                    if (rev.FileName.Split('.').Length > 1)
                    {
                        if (rev.FileName.Length - fileRev.Length - 1 > 0)
                        {
                            fileRevName = rev.FileName.Substring(0, (rev.FileName.Length - fileRev.Length - 1));
                        }
                        else
                        {
                            fileRevName = rev.FileName;
                        }
                        if (fileRevName.Trim().ToLower().Equals(editObject.FileName.ToLower()))
                        {
                            AmazonDMS.RenameObject(rev.Key, testKey + "." + fileRev, UploadedBy,
                                                   UploadedDate.ToString(CultureInfo.InvariantCulture));
                        }
                    }
                }

                AmazonDMS.RenameObject(Key, testKey, UploadedBy, UploadedDate.ToString(CultureInfo.InvariantCulture));
                AmazonDMS.CurrentBucketObjectNames.Remove(FileName);
                AmazonDMS.CurrentBucketObjectNames.Add(fileNameNew);

                //update other
                AWSObjectItem newItem = new AWSObjectItem();
                newItem = AmazonDMS.GetObjectByKey(testKey).S3Objects.FirstOrDefault();
                if (newItem.Type == AWSObjectItemType.File)
                {
                    UpdateOtherSession(hidBasePath.Value, newItem, Key);
                }

                //change all document key after rename
                using (CPTTEntities ce = new CPTTEntities())
                {
                    try
                    {
                        string encodeoldKey = Server.UrlEncode(Key);
                        string encodeNewKey = Server.UrlEncode(testKey);

                        if (DMSDocumentLinkHelper.RenameAllDocumentLink(ce, encodeNewKey, encodeoldKey))
                        {
                            if (isRename)
                            {
                                DocumentsChangeLog.RemoveRefKeyOnRenameEvent(encodeoldKey, ce);
                            }
                            ce.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        // Log error in ELMAH for any diagnostic needs.
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    }
                }

                if (HttpContext.Current.Session["hashAllRevisonItem"] != null)
                {
                    System.Collections.Hashtable hashAllRevisonItem = new System.Collections.Hashtable();
                    System.Collections.Hashtable bufferhashAllRevisonItem = new System.Collections.Hashtable();
                    hashAllRevisonItem =
                        (System.Collections.Hashtable) HttpContext.Current.Session["hashAllRevisonItem"];
                    foreach (System.Collections.DictionaryEntry entry in hashAllRevisonItem)
                    {
                        if (((AWSObjectItem) entry.Key).Key == Key)
                        {
                            List<AWSObjectItem> bufferlstRev = new List<AWSObjectItem>();
                            List<AWSObjectItem> lstRev = (List<AWSObjectItem>) entry.Value;
                            AWSObjectItem item;
                            foreach (AWSObjectItem s3Item in lstRev)
                            {
                                item = s3Item;
                                if (s3Item.FileName.Split('.').Length > 1)
                                {
                                    string fileRev = s3Item.FileName.Split('.')[s3Item.FileName.Split('.').Length - 1];
                                    int intRev;
                                    if (int.TryParse(fileRev, out intRev))
                                    {
                                        item.Key = testKey + "." + fileRev;
                                        item.FileName = fileNameNew + "." + fileRev;
                                    }
                                }
                                bufferlstRev.Add(item);
                            }

                            //Update metadata 
                            item = (AWSObjectItem) entry.Key;
                            item.Key = testKey;
                            if (item.Metadata == null)
                            {
                                item.Metadata = new AWSObjectMetadata();
                            }
                            item.FileName = fileNameNew;
                            item.Metadata.FileType = FileType;
                            item.Metadata.Classification.AddRange(classification);
                            item.Metadata.Description = txtAddDescription.Text;
                            item.Metadata.Keywords = txtAddKeywords.Text;
                            item.Metadata.UploadedBy = UploadedBy;
                            item.Metadata.UploadDate = UploadedDate;
                            bufferhashAllRevisonItem.Add(item, bufferlstRev);
                        }
                        else
                        {
                            bufferhashAllRevisonItem.Add((AWSObjectItem) entry.Key, (List<AWSObjectItem>) entry.Value);
                        }
                    }
                    HttpContext.Current.Session["hashAllRevisonItem"] = bufferhashAllRevisonItem;
                }
                Key = testKey;
            }
            else
            {
                fileNameNew = "";
            }

            if (isLinkField)
            {
                DocumentsChangeLog.AddLog(document, siteID, txtFileName.Text, testKey, DocumentsChangeLog.eDocumentLogAction.Edit, Request, isLinkField, true);
            }
            else
            {
                var checkDocObj = DMSDocumentLinkHelper.SearchDocumentsLinksByKey(HttpUtility.UrlEncode(testKey));
                if (checkDocObj == null)
                {
                    DocumentsChangeLog.AddLog(siteID, txtFileName.Text, testKey, DocumentsChangeLog.eDocumentLogAction.Edit, Request, isLinkField, true);
                }
                else
                {
                    DocumentsChangeLog.AddLog(checkDocObj,siteID, txtFileName.Text, testKey, DocumentsChangeLog.eDocumentLogAction.Edit, Request, true, true);
                }
            }

            // Close & Refresh FlexiGrid
            if (!string.IsNullOrEmpty(Request.QueryString["fromField"]))
            {
                _isFieldLink = true;
            }
            if (!_isFieldLink)
            {
                CloseDialogAndRefreshFlexiGrid(false, fileNameNew, bufferKey);
            }
            else
            {
                CloseDialogAndUpdate(fileNameNew, fileNameOld, typeID, hidBasePath.Value , document);
            }
        }

        //-->

        /// <summary>
        /// BtnMoveDocument Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnMoveDocumentClick(object sender, EventArgs e)
        {
           var isFileExist = false;
           var fileNameNew = txtFileName.Text;
           var isValidFolder = false;

           var targetFolder = hidBasePath.Value;
           var bufftargetFolder = "";
            if (targetFolder[targetFolder.Length - 1] == '/')
            {
                targetFolder = hidBasePath.Value.Substring(0, hidBasePath.Value.Length - 1);
            }
            if (targetFolder[0] == '/')
            {
                bufftargetFolder = targetFolder.Substring(1);
            }

            // A valid site Id must be specified.
            if (targetFolder != null)
            {
               // Since target folder may contain a full path, it needs to be
               // parsed to find just the site to check.
               var testFolder = targetFolder;
               var folderSplit = new List<string>();

               // Path may optionally have it's first character be a "/"
               if (testFolder.Substring(0, 1) == "/")
               {
                  testFolder = testFolder.Substring(1);
               }

               // Split path based on folder divider ("/") and check for
               // site
               folderSplit = testFolder.Split('/').ToList();
               
               isValidFolder = BLL.Site.Search(folderSplit[0]) != null;
            }

            if (!isValidFolder)
            {
               PageUtility.RegisterClientsideStartupScript(
                              Page, "InvalidFolder",
                              "handleInvalidSiteInModal('" + targetFolder + "');"
                           );
               return;
            }

            var targetFileName = hidBasePath.Value;            
            if (targetFileName[0] == '/')
            {
               targetFileName = hidBasePath.Value.Substring(1) + fileNameNew;
            }

            var checkRevString = targetFileName.Split('.');
            var checkRevfileNameNew = fileNameNew.Split('.');
            int checkRevNumber = 0;
            if (checkRevString.Count() > 1 && int.TryParse(checkRevString[checkRevString.Count() - 1], out checkRevNumber))
            {
                targetFileName = targetFileName.Replace("."+checkRevString[checkRevString.Count() - 1], "");
                fileNameNew = fileNameNew.Replace("." + checkRevfileNameNew[checkRevfileNameNew.Count() - 1], "");
            }

            var oldLocation = Request.QueryString["basePath"];
            if (oldLocation[0] == '/')
            {
                oldLocation = Request.QueryString["basePath"].Substring(1);
            }

            if (oldLocation[oldLocation.Length - 1] == '/')
            {
                oldLocation = oldLocation.Substring(0, Request.QueryString["basePath"].Length - 2);
            }

            // Check duplicate file name with insensitive case in move folder by Katorn J. 2012-08-14
            var objectNewSet = AmazonDMS.GetObjects(bufftargetFolder, null, isGetAll: true);
            var queryNewItems = objectNewSet.S3Objects.Where(a => a.Type != AWSObjectItemType.Folder).ToList();
            foreach (var objQueryNew in queryNewItems)
            {
               if (fileNameNew.Trim().ToLower().Equals(objQueryNew.FileName.ToLower()))
               {
                  isFileExist = true;
               }
            }

            if (isFileExist)
            {
                const string JS_FILE_EXISTS = "handleFileNameAlreadyExistsInModal('file');";
                PageUtility.RegisterClientsideStartupScript(Page, "ScriptFileExists", JS_FILE_EXISTS);
                return;
            }

            var objectSet = AmazonDMS.GetObjects(oldLocation, null, isGetAll: true);
            var queryItems = objectSet.S3Objects.Where(a => a.Type != AWSObjectItemType.Folder).ToList();
            var editObject = queryItems.FirstOrDefault(o => o.Key == Key);

            // Get Object META Data
            AWSObjectMetadata metaMove;
            using (AmazonDMS.S3Client = Amazon.AWSClientFactory.CreateAmazonS3Client())
            {
                // Get Object META
                metaMove = AmazonDMS.GetObjectMetadata(Key);
            }

            // Have META?
            var uploadedBy = "";
            var uploadDate = DateTime.Now;
            if (metaMove != null)
            {
                // Uploaded By
                uploadedBy = metaMove.UploadedBy ?? "";

                // Uploaded Date
                if (!DateTime.TryParse(metaMove.UploadDate.ToString(CultureInfo.InvariantCulture), out uploadDate))
                {
                    uploadDate = DateTime.Now;
                }
            }
            //>

            if (metaMove == null)
            {
                AmazonDMS.CopyObject(editObject.Key, targetFileName, editObject.Metadata.Classification,
                                     editObject.Metadata.FileType, editObject.Metadata.Description,
                                     editObject.Metadata.Keywords, editObject.Metadata.UploadedBy,
                                     editObject.Metadata.UploadDate.ToString(CultureInfo.InvariantCulture));
            }
            else
            {
                var classification = new List<string>();
                classification.AddRange(from ListItem listItem in lbxAddClassification.Items
                                        where listItem.Selected
                                        select listItem.Value);

                //Move all revsion file by Kantorn J. 2012-08-7
                var listrev = queryItems.ToList();
                foreach (AWSObjectItem rev in listrev)
                {
                    var fileRev = rev.FileName.Split('.')[rev.FileName.Split('.').Length - 1];
                    var fileRevName = "";

                    if (rev.FileName.Split('.').Length > 1)
                    {
                        fileRevName = rev.FileName.Substring(0, (rev.FileName.Length - fileRev.Length - 1));
                    }
                    else
                    {
                        fileRevName = fileRev;
                        fileRev = "";
                    }

                    if (fileRevName.Trim().ToLower().Equals(editObject.FileName.ToLower()))
                    {
                        var metaRev = new AWSObjectMetadata();
                        using (AmazonDMS.S3Client = Amazon.AWSClientFactory.CreateAmazonS3Client())
                        {
                            // Get Object META
                            metaRev = AmazonDMS.GetObjectMetadata(rev.Key);
                        }
                        if (fileRev != "")
                        {
                            AmazonDMS.CopyObject(rev.Key, targetFileName + "." + fileRev, metaRev.Classification,
                                                 metaRev.FileType, metaRev.Description, metaRev.Keywords,
                                                 metaRev.UploadedBy,
                                                 metaRev.UploadDate.ToString(CultureInfo.InvariantCulture));
                            AmazonDMS.DeleteObject(rev.Key);
                        }
                        else
                        {
                            if (targetFileName.Split('.').Length > 1)
                            {
                                AmazonDMS.CopyObject(rev.Key, targetFileName + "." + fileRev, classification,
                                                     editObject.Type.ToString(), metaRev.Description, metaRev.Keywords,
                                                     uploadedBy, uploadDate.ToString(CultureInfo.InvariantCulture));
                            }
                            else
                            {
                                AmazonDMS.CopyObject(rev.Key, targetFileName, metaRev.Classification,
                                                     editObject.Type.ToString(), metaRev.Description, metaRev.Keywords,
                                                     uploadedBy, uploadDate.ToString(CultureInfo.InvariantCulture));
                            }
                            AmazonDMS.DeleteObject(rev.Key);
                        }
                    }
                }
                if (editObject.Key.Split('.').Length > 1)
                {
                    AmazonDMS.CopyObject(editObject.Key, targetFileName, metaMove.Classification, metaMove.FileType,
                                         metaMove.Description, metaMove.Keywords, metaMove.UploadedBy,
                                         metaMove.UploadDate.ToString(CultureInfo.InvariantCulture));
                    
                    var newItem = new AWSObjectItem();
                    newItem = AmazonDMS.GetObjectByKey(targetFileName).S3Objects.FirstOrDefault();
                    if (newItem != null)
                    {
                        UpdateOtherOnMoveSession(hidBasePath.Value, newItem, editObject.Key);
                    }
                }
            }

            //delete old file
            if (editObject.Key.Split('.').Length > 1)
            {
                AmazonDMS.DeleteObject(editObject.Key);
            }
            //change all document key after rename
            using (var ce = new CPTTEntities())
            {
                try
                {
                    var encodeoldKey = Server.UrlEncode(editObject.Key);
                    var encodeNewKey = Server.UrlEncode(targetFileName);
                    if (DMSDocumentLinkHelper.RenameAllDocumentLink(ce, encodeNewKey, encodeoldKey))
                    {
                        ce.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    // Log error in ELMAH for any diagnostic needs.
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                }
            }

            // Close & Redirect to new location
            CloseDialogAndRedirect(targetFolder);
        }

        //-->

        protected void BtnSelectDocumentClick(object sender, EventArgs e)
        {
            string result = "";
            string type = Request.QueryString["type"];

            if (Request.QueryString["action"] != null)
            {
                var action = Request.QueryString["action"];

                try
                {
                    if (action == "Sel")
                    {
                        // Save docLink to db
                        int typeID = 0;
                        string siteID = "";
                        bool isValidId = Int32.TryParse(Request.QueryString["typeID"], out typeID);
                        if (!isValidId)
                        {
                            siteID = Request.QueryString["typeID"];
                        }

                        string field = Request.QueryString["field"];
                        string tmpKey = Utility.PrepareDMSBasePath(hidBasePath.Value);
                        string key = Server.UrlEncode(tmpKey);
                        string name = "";
                        if (tmpKey.Split('/').Length > 1)
                        {
                            name = tmpKey.Split('/')[tmpKey.Split('/').Length - 1];
                        }
                        else
                        {
                            name = tmpKey.Split('/')[0];
                        }

                        DmsLinkDocumentStructure document = DMSDocumentLinkHelper.DocumentField(field);
                        if (typeID != 0)
                        {
                            List<Dms_Document> oldLink = new List<Dms_Document>();
                            //check old link
                            oldLink = DMSDocumentLinkHelper.GetDocumentLink(typeID, document, key);

                            if (oldLink.Count > 0)
                            {
                                PageUtility.RegisterClientsideStartupScript(Page, "DisplayWarringDupFile",
                                                                            "DisplayWarringDupFile()");
                                return;
                            }
                            else
                            {
                                result = DMSDocumentLinkHelper.AddDocumentLink(typeID,typeID, document, key, true);
                                // Add log
                                string siteId = key.Split('%').FirstOrDefault();
                                if(!string.IsNullOrEmpty(siteId))
                                {
                                    document.siteID = siteId;
                                    document.RecordId = typeID;
                                    DocumentsChangeLog.AddLog(document, siteId, name, key, DocumentsChangeLog.eDocumentLogAction.Link, Request,true);
                                }
                            }
                        }
                        else if (siteID != "")
                        {
                            List<Dms_Document_Site> oldLink = new List<Dms_Document_Site>();
                            //check old link
                            oldLink = DMSDocumentLinkHelper.GetDocumentLink(siteID, key);
                            if (oldLink.Count > 0)
                            {
                                PageUtility.RegisterClientsideStartupScript(Page, "DisplayWarringDupFile",
                                                                            "DisplayWarringDupFile()");
                                return;
                            }
                            else
                            {
                                documents_links_site doc = new documents_links_site();
                                doc.record_id = siteID;
                                doc.linked_field = document.Field.ToString();
                                doc.record_type = document.Type.ToString();
                                doc.trackit_docs_folder = key;
                                result = DMSDocumentLinkHelper.AddDocumentLink(doc);
                            }

                        }

                        if (result == "")
                        {
                            if (isValidId)
                            {
                                ///closeDialogExisting(type, typeID, field, key, name, related_field);
                                String script = "closeDialogExisting(\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\");";
                                StringBuilder sb = new StringBuilder();
                                sb.Append(String.Format(script, type, typeID, field, tmpKey, name,
                                                        document.RelatedField.HasValue));
                                if (document.RelatedField.HasValue)
                                {
                                    DmsLinkDocumentStructure related_doc =
                                        DMSDocumentLinkHelper.DocumentField(document.RelatedField.ToString());
                                    sb.Append(String.Format(script, related_doc.Type.ToString(), typeID,
                                                            related_doc.Field.ToString(), tmpKey, name, false));
                                }
                                PageUtility.RegisterClientsideStartupScript(Page, "CloseDocumentDialogExisting",
                                                                            sb.ToString());
                            }
                            else if (siteID != "")
                            {
                                PageUtility.RegisterClientsideStartupScript(Page, "CloseDocumentDialogExisting",
                                                                            "closeDialogExisting(\"" + type + "\",\"" +
                                                                            siteID + "\",\"" + field + "\",\"" + tmpKey +
                                                                            "\",\"" + name + "\")");
                            }
                        }
                        else
                        {
                            PageUtility.RegisterClientsideStartupScript(Page, "CloseDocumentDialogAfterError",
                                                                        "closeDialogAfterError(\"" + result + "\",\"" +
                                                                        type + "\",\"" + typeID + "\",\"" + field +
                                                                        "\")");
                        }

                    }
                }
                catch (Exception ex)
                {
                    // Log error in ELMAH for any diagnostic needs.
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                }
            }
        }

        // Close Dialog and Refresh FlexiGrid
        private void CloseDialogAndRefreshFlexiGrid(bool newRecord = true, string fileName = "", string oldfilename = "")
        {
            if (newRecord)
            {
                PageUtility.RegisterClientsideStartupScript(Page, "ScriptUploadedDocument",
                                                            "parent.ChangeDirectory(parent.CurrentDir); parent.$('#modalExternalDms').dialog('close');");
            }
            else
            {
                string rowIdAsClass = Request.QueryString["rowClassSelector"];
                string fileNameHasChangedArgument = fileName.Length > 0
                                                        ? (", true, \"" + HttpUtility.UrlEncode(fileName) + "\",\"" +
                                                           oldfilename + "\"")
                                                        : (", false, '',\"" + oldfilename + "\"");
                //change to change directory by Knatorn J. 2012-08-17
                PageUtility.RegisterClientsideStartupScript(Page, "ScriptUploadedDocument",
                                                            "parent.parent.loadDmsFlexiGridSingleFileRow(\"" +
                                                            HttpUtility.UrlEncode(Key) + "\", \"" + rowIdAsClass +
                                                            "\", true, false" + fileNameHasChangedArgument +
                                                            ");parent.$('#modalExternalDms').dialog('close');");
                
            }
        }

        //-->

        // Cloas Dialog and Refresh page
        private void CloseDialogAndUpdate(string newFileName,string oldFileName,string typeID, string basePath , DmsLinkDocumentStructure document)
        {
            String script = "closeDialogExistingWithUpdate(\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\");";
            StringBuilder sb = new StringBuilder();
            if (document.RelatedField.HasValue)
            {
                DmsLinkDocumentStructure related_doc =
                    DMSDocumentLinkHelper.DocumentField(document.RelatedField.ToString());
                sb.Append(String.Format(script, related_doc.Type.ToString(), typeID,
                                        related_doc.Field.ToString(), HttpUtility.UrlEncode(basePath +"/"+ oldFileName)
                                        , HttpUtility.UrlEncode(basePath + "/" + newFileName), newFileName, false));
            }
            sb.Append(String.Format(script, document.Type.ToString(), typeID, document.Field.ToString(),
                                    HttpUtility.UrlEncode(basePath + "/" + oldFileName),
                                    HttpUtility.UrlEncode(basePath + "/" + newFileName), newFileName, document.RelatedField.HasValue));
            PageUtility.RegisterClientsideStartupScript(Page, "CloseDocumentDialogExistingWithUpdate",
                                                        sb.ToString());
        }

        //-->

        // Close Dialog and Relocation
        private void CloseDialogAndRedirect(string targetLocation)
        {

            var fullLocation = "/DMS/#" + targetLocation;
            if (targetLocation[0] == '/')
            {
                targetLocation = targetLocation.Substring(1, targetLocation.Length - 1);
            }
            var location = targetLocation;
            string jsMoveFile = "parent.$('#modalExternalDms').dialog('close');";
            jsMoveFile = "parent.ChangeDirectory(\"" + location + "\");" + jsMoveFile;
            PageUtility.RegisterClientsideStartupScript(Page, "ScriptMoveFile", jsMoveFile);
        }

        //-->

        private bool IsPrimeSiteDoc(string docTitle, string siteId)
        {
            docTitle = docTitle.ToLower();
            siteId = siteId.ToLower();

            // strip site id and ring id to avoid matching against those
            docTitle = docTitle.Replace(siteId, "").Replace(siteId.Substring(0, siteId.Length-1), "");

            // match abbreviation followed by space, dot, integer, underscore, dash
            // match e.g. Phase1
            string acceptablePrefixChars = "[ \\-_\\.]";
            string acceptablePostfixChars = "[ \\-_\\.0-9i]";
            string[] primeSiteKeys =
                {
                    "prime", "priime", "survey", "zoning", "phase", "rl", "bp", "am", "2c", "2-c",
                    "1a", "td", "fd", "sdp"
                };

            foreach(string i in primeSiteKeys)
            {
                if (Regex.IsMatch(
                    docTitle,acceptablePrefixChars + i + acceptablePostfixChars))
                {
                    return true;
                }
            }
            if (docTitle.EndsWith(".jpg") || docTitle.EndsWith(".jpeg"))
            {
                return true;
            }
            return false;
        }

        public string CreateAutoRename(string currentText, string siteID, string newName, string newNameDescription, string newNameAppType, TrackIT2.BLL.DMSDocumentLinkHelper.eDocumentField fieldName)
        {
            string newFileName = string.Empty;
            string type = string.Empty;
            if (!string.IsNullOrEmpty(siteID))
            {
                siteID = " " + siteID;
            }

            // remove special char from new neme
            newName = RemoveSpecialCharacters(newName);

            if (!string.IsNullOrEmpty(newName))
            {
                newName = " " + newName;
            }

            var docObj = DMSDocumentLinkHelper.DocumentField(fieldName.ToString());
            // set new doc name
            if (string.IsNullOrEmpty(docObj.NewName))
            {
                type = "Unknow";
            }
            else
            {
                type = docObj.NewName;
            }
            //-->

            // set appType
            if (docObj.RenameType == DMSDocumentLinkHelper.eDocumentRenameType.ApplicationType)
            {
                type = string.Format("{0} {1}", newNameAppType, type);
            }
            else if (docObj.RenameType == DMSDocumentLinkHelper.eDocumentRenameType.SlaOrAmend)
            {
                type = newNameAppType;
            }
            //-->

            // set description
            if (!string.IsNullOrEmpty(newNameDescription))
            {
                txtAddDescription.Text = newNameDescription;
            }

            newFileName = string.Format(currentText, siteID, newName, type);
            hidNewFileName.Value = newFileName.Replace("\"", "");
            return newFileName;
        }

        public static string RemoveSpecialCharacters(string input)
        {
            input = input.Replace("//", "");
            Regex r = new Regex("[/?*:|\"<>\\\\]", RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Compiled);
            return r.Replace(input, String.Empty);
        }

        public static void UpdateOtherSession(string currentBasePath, AWSObjectItem aws, string oldName)
        {

            Hashtable hashQueryStaticItems;
            if(HttpContext.Current.Session["queryStaticItems"] != null)
            {
                hashQueryStaticItems = (Hashtable)HttpContext.Current.Session["queryStaticItems"];
            }
            else
            {
                hashQueryStaticItems = new Hashtable();
            }
            Hashtable tmpHash = new Hashtable();
            List<AWSObjectItem> queryStaticItems = new List<AWSObjectItem>();
           
            string checkName = aws.Key;
            if (!string.IsNullOrEmpty(oldName))
            {
                checkName = oldName;
            }

            if (hashQueryStaticItems != null)
            {
               foreach (string s in hashQueryStaticItems.Keys)
               {
                  tmpHash.Add(s, new List<AWSObjectItem>());
               }

               foreach (string s in hashQueryStaticItems.Keys)
               {
                  List<AWSObjectItem> list = (List<AWSObjectItem>)hashQueryStaticItems[s];
                  if (list != null)
                  {
                     var item = (from i in list
                                 where i.Key == checkName
                                 select i).FirstOrDefault();
                     if (item != null)
                     {
                        list.Remove(item);
                     }

                     if (s.Contains("AllDocument") || aws.Key.Contains(s))
                     {
                        list.Add(aws);
                     }


                     tmpHash[s] = list;
                  }
               }

               HttpContext.Current.Session["queryStaticItems"] = tmpHash;  
            }            
        }

        public static void UpdateOtherOnMoveSession(string currentBasePath, AWSObjectItem aws, string oldName)
        {
            RemoveOtherSession(oldName, aws);
            MoveSession(currentBasePath, aws, oldName);
        }

        public static void MoveSession(string currentBasePath, AWSObjectItem aws, string oldName)
        {
            Hashtable hashQueryStaticItems = (Hashtable)HttpContext.Current.Session["queryStaticItems"];
            Hashtable tmpHash = hashQueryStaticItems;
            List<AWSObjectItem> queryStaticItems = new List<AWSObjectItem>();
            string currentSiteId = string.Empty;
            if (currentBasePath.FirstOrDefault() == '/')
            {
                currentBasePath = currentBasePath.Substring(1);
            }
            if (currentBasePath.LastOrDefault() == '/')
            {
                currentBasePath = currentBasePath.Substring(0, currentBasePath.Length-1);
            }
            currentSiteId = currentBasePath.Split('/').FirstOrDefault();
            string currentAllDoc = currentSiteId + "/AllDocument";

            queryStaticItems.Add(aws);
            var tmpData = TrackIT2.Handlers.DmsFlexiGridHandler.GetCurrentVersionFile(queryStaticItems, ref tmpHash);
            if (tmpData != null && tmpData.Count > 0)
            {
                aws = tmpData.FirstOrDefault();

            }
            string checkName = aws.Key;
            foreach (string s in hashQueryStaticItems.Keys)
            {
                tmpHash.Add(s, new List<AWSObjectItem>());
            }

            foreach (string s in hashQueryStaticItems.Keys)
            {
                List<AWSObjectItem> list = (List<AWSObjectItem>)hashQueryStaticItems[s];
                if (list != null)
                {
                    var item = (from i in list
                                where i.Key == checkName
                                select i).FirstOrDefault();
                    if (item != null)
                    {
                        list.Remove(item);
                    }

                    if (s.Contains(currentAllDoc))
                    {
                        // get all aws rev
                        AWSResultSet allFile = AmazonDMS.GetObjects(currentBasePath, null, isGetAll: true);
                        
                        if (allFile.S3Objects.Count > 0)
                        {
                            var tmpList = allFile.S3Objects.ToList();
                            list = (from l in list
                                   where !(l.Key.Contains(currentBasePath))
                                   select l).ToList();

                            foreach (var i in tmpList)
                            {
                                if (i.Metadata == null)
                                {
                                    list.Add(AmazonDMS.GetObjectByKey(i.Key).S3Objects.FirstOrDefault());
                                }
                                else
                                {
                                    list.Add(i);
                                }
                            }
                            //list.AddRange(tmpList);
                        }
                    }
                    tmpHash[s] = list;
                }
            }

            HttpContext.Current.Session["queryStaticItems"] = tmpHash;
        }

        public static void RemoveOtherSession(string deleteKey, AWSObjectItem aws)
        {
            Hashtable hashQueryStaticItems = (Hashtable)HttpContext.Current.Session["queryStaticItems"];
            Hashtable tmpHash = new Hashtable();
            Hashtable tmpRevHash = new Hashtable();
            Hashtable tmp = new Hashtable();
            List<AWSObjectItem> queryStaticItems = new List<AWSObjectItem>();

            // remove rev
            System.Collections.Hashtable hashAllRevisonItem;

            if (HttpContext.Current.Session["hashAllRevisonItem"] != null)
            {
                hashAllRevisonItem = (System.Collections.Hashtable)HttpContext.Current.Session["hashAllRevisonItem"];
            }
            else
            {
                hashAllRevisonItem = new System.Collections.Hashtable();
            }

            foreach (System.Collections.DictionaryEntry entry in hashAllRevisonItem)
            {
                if (!((AWSObjectItem)entry.Key).Key.Contains(deleteKey))
                {
                    tmpRevHash.Add(entry.Key, entry.Value);
                }
            }

            HttpContext.Current.Session["hashAllRevisonItem"] = tmpRevHash;

            // remove queryStaticItems
            foreach (string s in hashQueryStaticItems.Keys)
            {
                tmpHash.Add(s, new List<AWSObjectItem>());
            }

            foreach (string s in hashQueryStaticItems.Keys)
            {
                List<AWSObjectItem> list = (List<AWSObjectItem>)hashQueryStaticItems[s];
                if (list != null)
                {
                    var item = from i in list
                               where !(i.Key.Contains(deleteKey))
                               select i;
                    if (item.Count() == 0)
                    {
                        tmpHash.Remove(s);
                    }
                    else
                    {
                        tmpHash[s] = item.ToList();
                    }
                }
            }

            HttpContext.Current.Session["queryStaticItems"] = tmpHash;
        }

        public static AWSResultSet GetAllDocument(string key)
        {
            AWSResultSet returnResult = new AWSResultSet();
            returnResult.S3Objects = new List<AWSObjectItem>();

            AWSResultSet tmpObj = AmazonDMS.GetObjects(key, null, isGetAll: true);
            returnResult.CurrentPage = tmpObj.CurrentPage;
            returnResult.IsPartialResult = tmpObj.IsPartialResult;
            returnResult.NextMarker = tmpObj.NextMarker;
            returnResult.PreviousMarker = tmpObj.PreviousMarker;

            foreach (AWSObjectItem obj in tmpObj.S3Objects)
            {
                if (obj.Type == AWSObjectItemType.Folder)
                {
                    returnResult.S3Objects.AddRange(GetAllDocument(obj.Key).S3Objects);
                }
                else
                {
                    returnResult.S3Objects.Add(obj);
                }
            }

            return returnResult;
        }


        #region Customer association
            private string matchCustomer(string docTitle)
            {
                string matched = String.Empty;
                matched = checkCustomerOnSite(docTitle);
                if (String.IsNullOrEmpty(matched))
                {
                    matched = checkAliasCustomer(docTitle);
                }

                return matched;
            }

            private string checkCustomerOnSite(string docTitle)
            {
                string matched = String.Empty;
                using (CPTTEntities ce = new CPTTEntities())
                {
                   List<customer> cusomerList = ce.customers.ToList();
                    foreach(customer cus in cusomerList)
                    {
                        if (docIsForCustomer(docTitle, cus.customer_name))
                        {
                            matched = cus.customer_name;
                        }
                    }
                }
                return matched;
            }

            private string checkAliasCustomer(string docTitle)
            {
                string matched = String.Empty;
                string[][] customerAliasMappings = new string[][]{
                     new string[] {" Mobi " ,  "Mobi PCS"},
                     new string[] {" PSE " ,  "Puget Sound Energy"},
                     new string[] {"Alltel" ,  "Verizon"},
                     new string[] {"CBW" ,  "Cincinnati Bell"},
                     new string[] {"CI Bell" ,  "US Cellular"},
                     new string[] {"CLW" ,  "Clearwire"},
                     new string[] {"Cingular" ,  "AT&T"},
                     new string[] {"Clear" ,  "Clearwire"},
                     new string[] {"Cleawire" ,  "Clearwire"},
                     new string[] {"Cleawrwire" ,  "Clearwire"},
                     new string[] {"Clear" ,  "Clearwire"},
                     new string[] {"Commnet" ,  "Commnet Four Corners"},
                     new string[] {"Conterra" ,  "Conterra Ultra Broadband"},
                     new string[] {"Denali" ,  "Cricket Communications"},
                     new string[] {"Dobson" ,  "AT&T"},
                     new string[] {"Fiber Tech" ,  "Fiber Technologies Networks"},
                     new string[] {"FiberTech" ,  "Fiber Technologies Networks"},
                     new string[] {"Light Squared" ,  "LightSquared"},
                     new string[] {"LightSq" ,  "LightSquared"},
                     new string[] {"Louisville Metro" ,  "Louisville/Jefferson County Metro Government"},
                     new string[] {"Metro" ,  "Metro PCS"},
                     new string[] {"NeXXcom" ,  "NeXXcom Wireless LLC"},
                     new string[] {"New York State Thruway Authority" ,  "NYSTA"},
                     new string[] {"Nextel" ,  "Sprint/Nextel"},
                     new string[] {"Pac Bell" ,  "AT&T"},
                     new string[] {"Pocket" ,  "Youghiogheny"},
                     new string[] {"Royal Street" ,  "Metro PCS"},
                     new string[] {"Sprint" ,  "Sprint/Nextel"},
                     new string[] {"Sunman" ,  "Sunman Telecommunications Corp"},
                     new string[] {"Stelera" ,  "Stelera Wireless"},
                     new string[] {"TMO" ,  "T-Mobile"},
                     new string[] {"TTM" ,  "TTMI"},
                     new string[] {"US Cell" ,  "US Cellular"},
                     new string[] {"USCC" ,  "US Cellular"},
                     new string[] {"VZW" ,  "Verizon"},
                     new string[] {"Verison" ,  "Verizon"},
                     new string[] {"Verzion" ,  "Verizon"},
                     new string[] {"Vista" ,  "Vista PCS LLC"},
                     new string[] {"VoiceStream" ,  "T-Mobile"},
                     new string[] {"Western PCS" ,  "T-Mobile"}
                };

               foreach(string[] alias in customerAliasMappings)
                {
                    if (docIsForCustomer(docTitle, alias[0]))
                    {
                        matched = alias[1];
                    }
                }
                return matched;
            }

            //maps alias to TrackiT customer name
            public bool docIsForCustomer(string docTitle, string customerName)
            {

                docTitle = docTitle.ToLower();
                customerName = customerName.ToLower();

                //try matching customer name as given
                if (docTitle.Contains(customerName))
                {
                    return true;
                }
                // try matching with some customer name modifications
                // note customer_name has been lowercased
                customerName = customerName.Replace(" communications", "").Replace(" llc", "");
                if (docTitle.Contains(customerName))
                {
                    return true;
                }
                // try matching with *more* customer name modifications
                customerName = customerName.Replace("&", "");
                // strip spaces from inside customer names, but don't strip leading or trailing spaces
                if (!customerName.StartsWith(" "))
                {
                    customerName = customerName.Replace(" ", "");
                }
                if (docTitle.Contains(customerName))
                {
                    return true;
                }
                return false;
            }
        #endregion
    }

    //>
}