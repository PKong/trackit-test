﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using TrackIT2.BLL;
using TrackIT2.DAL;

namespace TrackIT2.DMS
{
    /// <summary>
    /// Create Folder
    /// </summary>
    public partial class CreateFolder : System.Web.UI.Page
    {
        // Var
        private string _basePath = "";
        //>

        #region Properties

        /// <summary>
        /// Key
        /// </summary>
        public string Key
        {
            get
            {
                return (string)ViewState["Key"];
            }
            set
            {
                ViewState["Key"] = value;
            }
        }
        //>

        /// <summary>
        /// Folder Name Current
        /// </summary>
        public string FolderNameCurrent
        {
            get
            {
                return (string)ViewState["FolderNameCurrent"];
            }
            set
            {
                ViewState["FolderNameCurrent"] = value;
            }
        }
        //>

        #endregion

        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                // Hidden Base Path
                if ((Request.QueryString["basePath"] != null) && (Request.QueryString["basePath"] != ""))
                {
                    _basePath = Request.QueryString["basePath"];

                    // Current folder location label for User
                    lblCurrentFolderLocation.Text = _basePath;

                    if (_basePath.Length > 1)
                    {
                        if (_basePath.StartsWith("/")) _basePath = _basePath.Substring(1, _basePath.Length -1);
                        if (_basePath.EndsWith("/")) _basePath = _basePath.Substring(0, _basePath.Length - 1);   
                    }else
                    {
                        _basePath = "";
                    }
                    hidBasePath.Value = _basePath;
                }
                //>

                // Editing?
                if (!string.IsNullOrEmpty(Request.QueryString["key"])  && !string.IsNullOrEmpty(Request.QueryString["action"]))
                {
                    Key = Request.QueryString["key"];
                    SetupEditMode(Request.QueryString["action"]);
                }
            }
        }
        //-->

        /// <summary>
        /// Setup Edit Mode
        /// </summary>
        private void SetupEditMode(string action)
        {
            // Setup UI
            btnCreateFolder.Visible = false;

            // Get Current Object
            var objectSet = AmazonDMS.GetObjects(_basePath, null, isGetAll: true);
            var queryItems = objectSet.S3Objects.Where(a => a.Type == AWSObjectItemType.Folder).ToList();
            var editObject = queryItems.FirstOrDefault(o => o.Key == Key);

            if (editObject != null)
            {
                // Folder Name
                string folderName = editObject.FileName;
                if (folderName.EndsWith("/")) folderName = folderName.Substring(0, folderName.Length - 1);
                FolderNameCurrent = folderName;
                txtFolderName.Text = folderName;

                if (action == "Edit")
                {
                    btnSaveFolder.Visible = true;
                }
                else
                {
                    btnMoveFolder.Visible = true;
                    localtion_tree.Visible = true;
                    txtFolderName.Enabled = false;
                }
            }
        }
        //-->

        /// <summary>
        /// BtnCreateFolder Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnCreateFolderClick(object sender, EventArgs e)
        {
            string folderName = txtFolderName.Text;
            if (hidBasePath.Value != "") folderName = hidBasePath.Value + "/" + folderName;

            // Create Folder on Amazon
            AmazonDMS.CreateNewFolder(folderName);

            // Close & Refresh FlexiGrid
            CloseDialogAndRefreshFlexiGrid();
        }
        //-->

        /// <summary>
        /// BtnSaveFolder Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnSaveFolderClick(object sender, EventArgs e)
        {
            string folderNameCurrent = FolderNameCurrent;
            string folderNameNew = txtFolderName.Text;

            // Renaming check
            if (folderNameNew != folderNameCurrent)
            {
                // Trying to rename to an existing folder?
                if (AmazonDMS.CurrentBucketObjectNames.Contains(folderNameNew + "/"))
                {
                    const string JS_FOLDER_EXISTS = "handleFileNameAlreadyExistsInModal('folder');";
                    PageUtility.RegisterClientsideStartupScript(Page, "ScriptFolderExists", JS_FOLDER_EXISTS);
                    return;
                }


                // Setup
                string currentFolderPath = folderNameCurrent;
                if (hidBasePath.Value != "") currentFolderPath = hidBasePath.Value + "/" + currentFolderPath;
                String newFolderPath = folderNameNew;
                if (hidBasePath.Value != "") newFolderPath = hidBasePath.Value + "/" + newFolderPath;
                
                // Rename
                AmazonDMS.RenameFolder(currentFolderPath, newFolderPath);
                AmazonDMS.CurrentBucketObjectNames.Remove(FolderNameCurrent + "/");
                AmazonDMS.CurrentBucketObjectNames.Add(folderNameNew + "/");
                Key = folderNameNew + "/";

                //Change doc link
                using (CPTTEntities ce = new CPTTEntities())
                {
                    try
                    {
                        string encodeoldKey = Server.UrlEncode(currentFolderPath);
                        string encodeNewKey = Server.UrlEncode(newFolderPath);
                        if (DMSDocumentLinkHelper.RenameFolder(ce, encodeNewKey, encodeoldKey))
                        {
                            ce.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        // Log error in ELMAH for any diagnostic needs.
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    }
                }
                //update session list revision value if file name change
                // TODO: change to jQuery update of changed Folder name (not whole grid refresh).
                CloseDialogAndRefreshFlexiGrid(false, true, folderNameNew);
            }else
            {
                // Close no refresh
                CloseDialogAndRefreshFlexiGrid(false);
            }
        }
        //-->

        /// <summary>
        /// BtnMoveFolder Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnMoveFolderClick(object sender, EventArgs e)
        {
            string folderNameNew = txtFolderName.Text;
            string targetParentFolder = hidBasePath.Value;

            targetParentFolder = Utility.PrepareDMSBasePath(hidBasePath.Value);

            string targetFolderName = hidBasePath.Value;
            if (targetFolderName[0] == '/')
            {
                targetFolderName = hidBasePath.Value.Substring(1) + folderNameNew;
            }

            //get current folder 
            var sourceFolder = Utility.PrepareDMSBasePath(Key);

            if (CheckDestinationFolder(targetParentFolder, sourceFolder))
            {
                var objectSet = AmazonDMS.GetObjects(sourceFolder, null, isGetAll: true);
                var queryItems = objectSet.S3Objects.ToList();

                //create new folder
                AmazonDMS.CreateNewFolder(targetFolderName);

                //move all item in folder
                queryItems = objectSet.S3Objects.ToList();
                if (targetFolderName[targetFolderName.Length - 1] != '/')
                {
                    targetFolderName += "/";
                }
                AmazonDMS.MoveFileObjectsToFolderRecursive(queryItems, targetFolderName);

                //delete old folder
                if (sourceFolder[sourceFolder.Length - 1] != '/')
                {
                    sourceFolder = sourceFolder + "/";
                }
                AmazonDMS.DeleteObjectsRecursively(sourceFolder, true);

                using (CPTTEntities ce = new CPTTEntities())
                {
                    try
                    {
                        string encodeoldKey = Server.UrlEncode(sourceFolder);
                        string encodeNewKey = Server.UrlEncode(targetFolderName);
                        if (DMSDocumentLinkHelper.RenameFolder(ce, encodeNewKey, encodeoldKey))
                        {
                            ce.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        // Log error in ELMAH for any diagnostic needs.
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    }
                }

                // Close & Redirect to new location
                CloseDialogAndRedirect(targetParentFolder);
            }
            else
            {
                //display msg warring
                PageUtility.RegisterClientsideStartupScript(Page, "DisplayWarringMoveFolder", "DisplayMoveFolderWarring()");
            }
        }
        //-->

        // Close Dialog and Refresh FlexiGrid
        private void CloseDialogAndRefreshFlexiGrid(bool refresh = true, bool isRenamedFolder = false, string folderNameNew = null)
        {
            string jsCreatedFolder = "parent.$('#modalExternal-create-folder').dialog('close');";

            if (refresh)
            {
                //jsCreatedFolder = "parent.parent.reloadDataGrid();" + jsCreatedFolder;
                jsCreatedFolder = "parent.ChangeDirectory(parent.CurrentDir);" + jsCreatedFolder;
            }

            if (isRenamedFolder)
            {
                string rowIdAsClass = Request.QueryString["rowClassSelector"];
                string jsFolderNameNew = !string.IsNullOrEmpty(folderNameNew) ? (",\"" + folderNameNew + "\"") : "";
                jsCreatedFolder += "parent.parent.loadDmsFlexiGridSingleFolderRow(\"" + Key + "\", \"" + rowIdAsClass + "\"" + jsFolderNameNew + ");";
            }
            PageUtility.RegisterClientsideStartupScript(Page, "ScriptCreatedFolder", jsCreatedFolder);
        }
        //-->

        // Close Dialog and Relocation
        private void CloseDialogAndRedirect(string targetLocation)
        {
            var fullLocation = "/DMS/#/" + targetLocation;
            var location = targetLocation;
            string jsMoveFolder = "parent.$('#modalExternal-create-folder').dialog('close');";
            jsMoveFolder = "redirectPage(\"" + fullLocation + "\",\"" + location + "\");" + jsMoveFolder;
            PageUtility.RegisterClientsideStartupScript(Page, "ScriptMoveFolder", jsMoveFolder);
        }
        //-->

        public bool CheckDestinationFolder(string target, string source)
        {
            if (target.Split('/').Length > 1)
            {
                var sliptTarget = target.Split('/');
                string tmpTarget = "";

                foreach (string s in target.Split('/'))
                {
                    tmpTarget += s;
                    if (source == tmpTarget)
                    {
                        return false;
                    }
                    tmpTarget += "/";
                }
            }
            return true;
        }
    }
    //>
}