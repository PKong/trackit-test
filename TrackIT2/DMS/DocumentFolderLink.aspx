﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="Document Links | TrackIT2" Language="C#" MasterPageFile="~/Templates/Modal.Master" AutoEventWireup="true" CodeBehind="DocumentFolderLink.aspx.cs" Inherits="TrackIT2.DMS.DocumentFolderLink" %>
<%@ MasterType TypeName="TrackIT2.Templates.Modal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   <!-- Styles -->
   <%: Styles.Render("~/Styles/dms_document_folder") %>
    
   <!-- Scripts -->
   <%: Scripts.Render("~/Scripts/dms_document_folder") %>
    <script type="text/javascript">

        // On Folder browse click
        function folderSelectClick() {
            var pageParam = getQueryString(window.location.toString());

            parent.$("#MainContent_lblUploadingFolderLocation").text($("#lblUploadingFolderLocation").text());
            parent.$("#hidBasePath").val($("#lblUploadingFolderLocation").text());
            if (pageParam[pageParam.length - 1] == "Upl") {
                parent.$('#modalFolderBrowse').dialog('close');
            }
            else {
                parent.parent.$('#modalDocument').dialog('close');
            }
            return true;
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="message_alert"></div>
    <div id="DocumentLinkMain" class="create-container" style="margin: 0 auto 0 auto; text-align:left;">
        <div class="form_element">
            <div id="localtion_tree" class="location_tree" runat="server">
                <br />
                    <asp:Label ID="lblMoveTo" Text="Upload to:" style="width: auto; vertical-align:top;" runat="server" />&nbsp;
                    <asp:Label ID="lblUploadingFolderLocation" ClientIDMode="Static" runat="server" Text="/" style=" width:200px; text-align:left; vertical-align:top;" />
                <br />
                <asp:Panel ID="TreePanel" runat="server" CssClass="TreePanelAttribute" ScrollBars="Auto">
    			    <div id="jqueryFileTree" class="jqueryFileTree" ></div>
                </asp:Panel>
		    </div>
            <br />
            <div style="width:100%; text-align:right;">
                <asp:Button ID="folderSelect" runat="server" Text="Select" OnClientClick="folderSelectClick()" />
            </div>
        </div>
    </div>
    
    <!-- Modal & Hidden -->
    <asp:HiddenField id="hidDocumentDownload" ClientIDMode="Static" runat="server"/>
    <asp:HiddenField id="hidLocationHash" ClientIDMode="Static" runat="server"/>
    <asp:HiddenField ID="hidBasePath" runat="server" Value="" ClientIDMode="Static"/>
    <asp:HiddenField ID="hidTmpPath" runat="server" Value="" ClientIDMode="Static"/>
    <asp:HiddenField ID="hiddSiteFolderExist" ClientIDMode="Static" runat="server" />
    <!-- /Modal & Hidden -->

</asp:Content>
