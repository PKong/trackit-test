﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using TrackIT2.Objects;
using TrackIT2.Objects.JSONWrapper;
using TrackIT2.BLL;

namespace TrackIT2
{
    public partial class _Default : System.Web.UI.Page
    {
        private const string PATH_MYWIDGET = @"~\Dashboard\jsonfeed\mywidgets.json";

        protected override void OnPreInit(EventArgs e)
        {
           base.OnPreInit(e);
           if (Context != null && Context.Request.UserAgent != null)
           {             
                 Response.Cache.SetNoStore();           
           }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ////DEBUG Purpose.
            //Test();
            LoadWidgetLayout();
        }


        protected void LoadWidgetLayout()
        {
            UserProfile profile = UserProfile.GetUserProfile();
            MyWidgets objMyWidgets = profile.UserWidgets;
            DashboardJSONHelper objJSONHelper;
            //Try to load user widgets layout.
            if (objMyWidgets.MyWidgetsResult == null)
            {
                //Use default.
                objJSONHelper = new DashboardJSONHelper(GetDefaultMyWidgetJSONString());
                profile.UserWidgets = objJSONHelper.GetMyWidgets();
                profile.Save();
            }
            else
            {
                //User custom.
                objJSONHelper = new DashboardJSONHelper(objMyWidgets);
            }
            //Set Helper to Session.
            this.Session[Globals.SESSION_JSON_HELPER] = objJSONHelper;
            //Sending data to UI.
            hidMyWidgetJSON.Value = objJSONHelper.GetMyWidgetJSON();
        }

        protected string GetDefaultMyWidgetJSONString()
        {
            string MyWidgetPath = Server.MapPath(PATH_MYWIDGET);
            string ret = null;
            if (File.Exists(MyWidgetPath))
            {
                StreamReader reader = new StreamReader(MyWidgetPath);
                try
                {
                    ret = reader.ReadToEnd();
                }
                finally
                {
                    reader.Close();
                }
            }
            return ret;
        }



        #region DEBUG test
        //TODO : Delete this region when all JSON method is working correctly.
        protected void Test()
        {
            string MyWidgetPath = Server.MapPath(PATH_MYWIDGET);
            //string WidgetCategoriesPath = Server.MapPath(PATH_WIDGET_CATEGORIES);
            if (File.Exists(MyWidgetPath))
            {
                StreamReader reader = new StreamReader(MyWidgetPath);
                //StreamReader readerCate = new StreamReader(WidgetCategoriesPath);
                try
                {
                    string sMyWidget = reader.ReadToEnd();
                    //string sWidgetCategories = readerCate.ReadToEnd();
                    var obj = Newtonsoft.Json.JsonConvert.DeserializeObject(sMyWidget);
                    string json = Newtonsoft.Json.JsonConvert.SerializeObject(new Test());

                    var obj2 = Newtonsoft.Json.JsonConvert.DeserializeObject<Test>(json);
                    var obj3 = Newtonsoft.Json.JsonConvert.DeserializeObject(json);
                    var obj4 = Newtonsoft.Json.JsonConvert.DeserializeObject<JSONMyWidgetResult>(sMyWidget);


                    var obj5 = Newtonsoft.Json.JsonConvert.DeserializeXmlNode(json, "result");
                    string json2 = Newtonsoft.Json.JsonConvert.SerializeXmlNode(obj5);
                    //var objCategories = Newtonsoft.Json.JsonConvert.DeserializeObject<JSONWidgetCategoriesResult>(sWidgetCategories);

                    hidMyWidgetJSON.Value = Newtonsoft.Json.JsonConvert.SerializeObject(obj4).ToString();

                }
                finally
                {
                    reader.Close();
                    //readerCate.Close();
                }
            }

        }
        #endregion
    }


    #region Test Resource
    //TODO : Delete this region and DEBUG TEST after all method for JSON is working.
    //For MyWidget JSON
    [Serializable]
    public class JSONMyWidgetResult
    {
        public MyWidgets result { get; set; }
        public class MyWidgets
        {
            public string layout { get; set; }
            public List<WidgetInfo> data { get; set; }

            public class WidgetInfo
            {
                public string title { get; set; }
                public string column { get; set; }
                public string id { get; set; }
                public string url { get; set; }
                public string editurl { get; set; }
                public Boolean open { get; set; }
                public WidgetMetadataInfo metadata { get; set; }


            }
        }
    }
    [Serializable]
    public class WidgetMetadataInfo
    {
        public string defaults { get; set; }
    }

    //For Widget Categories JSON
    [Serializable]
    public class JSONWidgetCategoriesResult
    {
        public WidgetCategories categories { get; set; }
        public class WidgetCategories
        {
            public List<WidgetCategoriesInfo> category { get; set; }
            public class WidgetCategoriesInfo
            {
                public int id { get; set; }
                public string title { get; set; }
                public int amount { get; set; }
                public string url { get; set; }
            }
        }
    }


    //test class
    [Serializable]
    public class Test
    {
        public int MyProperty { get; set; }
        public int MyProperty2 { get; set; }
        public InnerTest MyClass { get; set; }
        public List<InnerTest> MyList { get; set; }
        public Test()
        {
            MyClass = new InnerTest();
            MyList = new List<InnerTest>();
            MyList.Add(new InnerTest());
            MyList[0].MyInnerProperty = 1;
            MyList[0].MyInnerProperty2 = 1;
            MyList.Add(new InnerTest());
            MyList[1].MyInnerProperty = 2;
            MyList[1].MyInnerProperty2 = 2;
        }
        public class InnerTest
        {
            public int MyInnerProperty { get; set; }
            public int MyInnerProperty2 { get; set; }
        }
    }
    #endregion
    
}