﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.IO;

namespace TrackIT2.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ITrackiT_API" in both code and config file together.
    [ServiceContract]
    public interface ITrackiT_API
    {
        [OperationContract]
        string InsertLeaseAppRevision(byte[] signature, String TMOAppId);
        [OperationContract]
        int CheckExistedTrakItTMOApp(byte[] signature, String TMOAppId);
        [OperationContract]
        bool CanShowSDPDocs(byte[] signature, String TMO_AppID);
        [OperationContract]
        List<string[]> GetSDPDocuments(byte[] signature, String TMO_AppID);
        [OperationContract]
        byte[] DowloadDocument(byte[] signature, String keyName,int sdpId);
        [OperationContract]
        byte[] DowloadAllDocument(byte[] signature, List<string> listFileKey, String TMO_AppID, string zipFileName, ref bool isRefreshData);
    }
}
